#! /bin/sh

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 DevelperName(signing)"
  exit 1
fi

cd ./installer/ios
if [ $? -ne 0 ]; then
  exit $?
fi

./make.sh $1
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

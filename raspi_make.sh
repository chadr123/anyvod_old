#! /bin/sh

cd ./installer/raspi
if [ $? -ne 0 ]; then
  exit $?
fi

./make.sh
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

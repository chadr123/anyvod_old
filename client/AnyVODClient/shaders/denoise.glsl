﻿// entry denoise

void denoise(inout vec4 texel, inout vec2 coord)
{
  const float effectWidth = 0.1;
  const float val0 = 1.0;
  const float val1 = 0.125;
  float tap = effectWidth;
  vec4 accum = texel * val0;
  int i = 0;

  for (; i < 16; i++)
  {
    float dx = tap / AnyVOD_Width;
    float dy = tap / AnyVOD_Height;

    accum += AnyVOD_getTexel(coord + vec2(-dx, -dy)) * val1;
    accum += AnyVOD_getTexel(coord + vec2(0.0, -dy)) * val1;
    accum += AnyVOD_getTexel(coord + vec2(-dx, 0.0)) * val1;
    accum += AnyVOD_getTexel(coord + vec2(dx, 0.0)) * val1;
    accum += AnyVOD_getTexel(coord + vec2(0.0, dy)) * val1;
    accum += AnyVOD_getTexel(coord + vec2(dx, dy)) * val1;
    accum += AnyVOD_getTexel(coord + vec2(-dx, dy)) * val1;
    accum += AnyVOD_getTexel(coord + vec2(dx, -dy)) * val1;

    tap += effectWidth;
  }

  texel = accum / 16.0;
}

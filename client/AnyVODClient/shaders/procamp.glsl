﻿// entry procamp

// Brightness: -1.0 to 1.0, default 0.0
// Contrast: 0.0 to 10.0, default 1.0
// Hue: -180.0 to +180.0, default 0.0
// Saturation: 0.0 to 10.0, default 1.0

void procamp(inout vec4 texel, inout vec2 coord)
{
  const float Brightness = 0.0;
  const float Contrast = 1.0;
  const float Hue = 0.0;
  const float Saturation = 1.0;
  const float ymin = 16.0 / 255.0;
  const float ymax = 235.0 / 255.0;
  float PI = acos(-1.0);

  mat2 HueMatrix = mat2
  (
     cos(Hue * PI / 180.0), sin(Hue * PI / 180.0),
    -sin(Hue * PI / 180.0), cos(Hue * PI / 180.0)
  );

  const mat4 r2y = mat4
  (
     0.299,  0.587,  0.114, 0.0,
    -0.147, -0.289,  0.437, 0.0,
     0.615, -0.515, -0.100, 0.0,
     0.0,    0.0,    0.0,   0.0
  );

  const mat4 y2r = mat4
  (
    1.0,  0.0,    1.140, 0.0,
    1.0, -0.394, -0.581, 0.0,
    1.0,  2.028,  0.0,   0.0,
    0.0,  0.0,    0.0,   0.0
  );

  texel = r2y * texel;
  texel.r = Contrast * (texel.r - ymin) + ymin + Brightness;
  texel.gb = HueMatrix * texel.gb * Saturation;
  texel = y2r * texel;
}

﻿// entry contour

void contour(inout vec4 texel, inout vec2 coord)
{
  float dx = 4.0 / AnyVOD_Width;
  float dy = 4.0 / AnyVOD_Height;

  vec4 c2 = AnyVOD_getTexel(coord + vec2(0.0, -dy));
  vec4 c4 = AnyVOD_getTexel(coord + vec2(-dx, 0.0));
  vec4 c5 = AnyVOD_getTexel(coord + vec2(0.0, 0.0));
  vec4 c6 = AnyVOD_getTexel(coord + vec2(dx, 0.0));
  vec4 c8 = AnyVOD_getTexel(coord + vec2(0.0, dy));

  vec4 c0 = (-c2 - c4 + c5 * 4.0 - c6 - c8);

  if (length(c0) < 1.0)
    c0 = vec4(0.0, 0.0, 0.0, 0.0);
  else
    c0 = vec4(1.0, 1.0, 1.0, 0.0);

  texel = c0;
}

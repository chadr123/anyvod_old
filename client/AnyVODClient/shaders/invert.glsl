﻿// entry invert

void invert(inout vec4 texel, inout vec2 coord)
{
  texel = vec4(1.0, 1.0, 1.0, 1.0) - texel;
}

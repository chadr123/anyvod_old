﻿<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>CustomShortcut</name>
    <message>
        <location filename="../forms/customshortcut.ui" line="14"/>
        <source>단축 키 설정</source>
        <translation>단축 키 설정</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="105"/>
        <source>메인</source>
        <translation>메인</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="134"/>
        <source>DTV</source>
        <translation>DTV</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="163"/>
        <source>화면</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="192"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="221"/>
        <source>자막 / 가사</source>
        <translation>자막 / 가사</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="250"/>
        <source>소리</source>
        <translation>소리</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="31"/>
        <source>초기화</source>
        <translation>초기화</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="57"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../forms/customshortcut.ui" line="73"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../src/ui/CustomShortcut.cpp" line="70"/>
        <source>&quot;%1&quot;와 중복 되는 단축키 입니다</source>
        <translation>&quot;%1&quot;와 중복 되는 단축키 입니다</translation>
    </message>
    <message>
        <location filename="../src/ui/CustomShortcut.cpp" line="384"/>
        <source>단축키를 초기화 하시겠습니까?</source>
        <translation>단축키를 초기화 하시겠습니까?</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../forms/equalizer.ui" line="19"/>
        <source>이퀄라이저</source>
        <translation>이퀄라이저</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="33"/>
        <location filename="../src/ui/Equalizer.cpp" line="269"/>
        <source>프리셋 추가</source>
        <translation>프리셋 추가</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="49"/>
        <source>프리셋 삭제</source>
        <translation>프리셋 삭제</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="65"/>
        <source>프리셋 저장</source>
        <translation>프리셋 저장</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="97"/>
        <source>초기화</source>
        <translation>초기화</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="113"/>
        <source>프리셋</source>
        <translation>프리셋</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="2085"/>
        <source>이퀄라이저 사용</source>
        <translation>이퀄라이저 사용</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="2111"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../forms/equalizer.ui" line="2127"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="99"/>
        <source>기본값</source>
        <translation>기본값</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="100"/>
        <source>클래식</source>
        <translation>클래식</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="101"/>
        <source>베이스</source>
        <translation>베이스</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="102"/>
        <source>베이스 &amp; 트레블</source>
        <translation>베이스 &amp; 트레블</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="103"/>
        <source>트레블</source>
        <translation>트레블</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="104"/>
        <source>헤드폰</source>
        <translation>헤드폰</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="105"/>
        <source>홀</source>
        <translation>홀</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="106"/>
        <source>소프트 락</source>
        <translation>소프트 락</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="107"/>
        <source>클럽</source>
        <translation>클럽</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="108"/>
        <source>댄스</source>
        <translation>댄스</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="109"/>
        <source>라이브</source>
        <translation>라이브</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="110"/>
        <source>파티</source>
        <translation>파티</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="111"/>
        <source>팝</source>
        <translation>팝</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="112"/>
        <source>레게</source>
        <translation>레게</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="113"/>
        <source>락</source>
        <translation>락</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="114"/>
        <source>스카</source>
        <translation>스카</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="115"/>
        <source>소프트</source>
        <translation>소프트</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="116"/>
        <source>테크노</source>
        <translation>테크노</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="117"/>
        <source>보컬</source>
        <translation>보컬</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="118"/>
        <source>재즈</source>
        <translation>재즈</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="269"/>
        <source>프리셋 이름 :</source>
        <translation>프리셋 이름 :</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="364"/>
        <source>프리셋이 없습니다</source>
        <translation>프리셋이 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/Equalizer.cpp" line="440"/>
        <source>삭제 하시겠습니까?</source>
        <translation>삭제 하시겠습니까?</translation>
    </message>
</context>
<context>
    <name>FileAssociation</name>
    <message>
        <location filename="../forms/fileassociation.ui" line="14"/>
        <source>확장자 연결</source>
        <translation>확장자 연결</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="22"/>
        <source>비디오</source>
        <translation>비디오</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="29"/>
        <source>오디오</source>
        <translation>오디오</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="42"/>
        <source>자막</source>
        <translation>자막</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="58"/>
        <location filename="../forms/fileassociation.ui" line="71"/>
        <location filename="../forms/fileassociation.ui" line="84"/>
        <location filename="../forms/fileassociation.ui" line="100"/>
        <source>기본 값</source>
        <translation>기본 값</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="107"/>
        <source>재생 목록</source>
        <translation>재생 목록</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="156"/>
        <source>적용 하기</source>
        <translation>적용 하기</translation>
    </message>
    <message>
        <location filename="../forms/fileassociation.ui" line="169"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../src/ui/FileAssociation.cpp" line="117"/>
        <source>관리자 권한으로 실행하지 않았을 경우 정상적으로 적용 되지 않을 수 있습니다</source>
        <translation>관리자 권한으로 실행하지 않았을 경우 정상적으로 적용 되지 않을 수 있습니다</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../forms/login.ui" line="31"/>
        <location filename="../forms/login.ui" line="136"/>
        <source>로그인</source>
        <translation>로그인</translation>
    </message>
    <message>
        <location filename="../forms/login.ui" line="46"/>
        <source>아이디와 비밀번호를 입력해주세요</source>
        <translation>아이디와 비밀번호를 입력해주세요</translation>
    </message>
    <message>
        <location filename="../forms/login.ui" line="70"/>
        <source>아이디</source>
        <translation>아이디</translation>
    </message>
    <message>
        <location filename="../forms/login.ui" line="80"/>
        <source>비밀번호</source>
        <translation>비밀번호</translation>
    </message>
    <message>
        <location filename="../forms/login.ui" line="165"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../src/ui/Login.cpp" line="76"/>
        <source>아이디를 입력해 주세요</source>
        <translation>아이디를 입력해 주세요</translation>
    </message>
    <message>
        <location filename="../src/ui/Login.cpp" line="80"/>
        <source>비밀번호를 입력해 주세요</source>
        <translation>비밀번호를 입력해 주세요</translation>
    </message>
    <message>
        <location filename="../src/ui/Login.cpp" line="89"/>
        <source>서버에 접속 할 수 없습니다</source>
        <translation>서버에 접속 할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/Login.cpp" line="105"/>
        <source>스트림에 접속 할 수 없습니다</source>
        <translation>스트림에 접속 할 수 없습니다</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../forms/mainwindow.ui" line="22"/>
        <source>AnyVOD</source>
        <translation>AnyVOD</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="238"/>
        <source>재생 위치</source>
        <translation>재생 위치</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="334"/>
        <source>음소거 켜기 / 끄기</source>
        <translation>음소거 켜기 / 끄기</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="367"/>
        <source>소리</source>
        <translation>소리</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="476"/>
        <location filename="../src/ui/MainWindow.cpp" line="2277"/>
        <source>가사</source>
        <translation>가사</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="600"/>
        <location filename="../src/ui/MainWindow.cpp" line="660"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="623"/>
        <location filename="../src/ui/MainWindow.cpp" line="667"/>
        <source>일시정지</source>
        <translation>일시정지</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="652"/>
        <location filename="../src/ui/MainWindow.cpp" line="480"/>
        <location filename="../src/ui/MainWindow.cpp" line="676"/>
        <source>정지</source>
        <translation>정지</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="681"/>
        <location filename="../src/ui/MainWindow.cpp" line="340"/>
        <source>5초 뒤로</source>
        <translation>5초 뒤로</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="710"/>
        <location filename="../src/ui/MainWindow.cpp" line="341"/>
        <source>5초 앞으로</source>
        <translation>5초 앞으로</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="739"/>
        <location filename="../src/ui/MainWindow.cpp" line="322"/>
        <source>열기</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="787"/>
        <source>스펙트럼</source>
        <translation>스펙트럼</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="943"/>
        <location filename="../src/ui/MainWindow.cpp" line="2801"/>
        <source>버퍼링 중입니다</source>
        <translation>버퍼링 중입니다</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="969"/>
        <location filename="../src/ui/MainWindow.cpp" line="347"/>
        <location filename="../src/ui/MainWindow.cpp" line="677"/>
        <source>이전 파일</source>
        <translation>이전 파일</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="992"/>
        <location filename="../src/ui/MainWindow.cpp" line="349"/>
        <location filename="../src/ui/MainWindow.cpp" line="678"/>
        <source>다음 파일</source>
        <translation>다음 파일</translation>
    </message>
    <message>
        <location filename="../forms/mainwindow.ui" line="1023"/>
        <source>투명도</source>
        <translation>투명도</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="281"/>
        <source>스킨을 읽을 수 없습니다 (%1)</source>
        <translation>스킨을 읽을 수 없습니다 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="317"/>
        <source>로그인</source>
        <translation>로그인</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="318"/>
        <source>로그아웃</source>
        <translation>로그아웃</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="319"/>
        <source>항상 위</source>
        <translation>항상 위</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="320"/>
        <source>원격 파일 목록 열기</source>
        <translation>원격 파일 목록 열기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="321"/>
        <source>외부 열기</source>
        <translation>외부 열기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="323"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="324"/>
        <source>종료</source>
        <translation>종료</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="325"/>
        <source>노멀라이저 사용</source>
        <translation>노멀라이저 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="326"/>
        <source>이퀄라이저 사용</source>
        <translation>이퀄라이저 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="327"/>
        <source>이퀄라이저 설정</source>
        <translation>이퀄라이저 설정</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="328"/>
        <source>재생 목록 열기</source>
        <translation>재생 목록 열기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="329"/>
        <source>자막 기본 위치</source>
        <translation>자막 기본 위치</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="330"/>
        <source>자막 위치 위로</source>
        <translation>자막 위치 위로</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="331"/>
        <source>자막 위치 아래로</source>
        <translation>자막 위치 아래로</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="332"/>
        <source>자막 위치 왼쪽으로</source>
        <translation>자막 위치 왼쪽으로</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="333"/>
        <source>자막 위치 오른쪽으로</source>
        <translation>자막 위치 오른쪽으로</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="334"/>
        <source>재생 / 일시정지</source>
        <translation>재생 / 일시정지</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="335"/>
        <source>재생 / 일시정지(재생)</source>
        <translation>재생 / 일시정지(재생)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="336"/>
        <source>재생 / 일시정지(일시정지)</source>
        <translation>재생 / 일시정지(일시정지)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="337"/>
        <source>재생 / 일시정지(토글)</source>
        <translation>재생 / 일시정지(토글)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="338"/>
        <location filename="../src/ui/MainWindow.cpp" line="3921"/>
        <source>전체 화면</source>
        <translation>전체 화면</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="339"/>
        <source>전체 화면(추가)</source>
        <translation>전체 화면(추가)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="342"/>
        <source>30초 뒤로</source>
        <translation>30초 뒤로</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="343"/>
        <source>30초 앞으로</source>
        <translation>30초 앞으로</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="344"/>
        <source>1분 뒤로</source>
        <translation>1분 뒤로</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="345"/>
        <source>1분 앞으로</source>
        <translation>1분 앞으로</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="346"/>
        <location filename="../src/ui/MainWindow.cpp" line="3963"/>
        <source>처음으로 이동</source>
        <translation>처음으로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="348"/>
        <source>이전 파일(추가)</source>
        <translation>이전 파일(추가)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="350"/>
        <source>다음 파일(추가)</source>
        <translation>다음 파일(추가)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="351"/>
        <source>재생 순서 순차 선택</source>
        <translation>재생 순서 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="352"/>
        <source>보이기</source>
        <translation>보이기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="353"/>
        <location filename="../src/ui/MainWindow.cpp" line="373"/>
        <source>싱크 느리게</source>
        <translation>싱크 느리게</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="354"/>
        <location filename="../src/ui/MainWindow.cpp" line="374"/>
        <source>싱크 빠르게</source>
        <translation>싱크 빠르게</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="355"/>
        <location filename="../src/ui/MainWindow.cpp" line="375"/>
        <source>싱크 초기화</source>
        <translation>싱크 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="356"/>
        <source>자막 언어 순차 선택</source>
        <translation>자막 언어 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="357"/>
        <source>소리 크게</source>
        <translation>소리 크게</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="358"/>
        <source>소리 크게(추가)</source>
        <translation>소리 크게(추가)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="359"/>
        <source>소리 작게</source>
        <translation>소리 작게</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="360"/>
        <source>소리 작게(추가)</source>
        <translation>소리 작게(추가)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="361"/>
        <source>음소거</source>
        <translation>음소거</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="362"/>
        <source>음소거(추가)</source>
        <translation>음소거(추가)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="363"/>
        <source>음성 순차 선택</source>
        <translation>음성 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="364"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="365"/>
        <source>재생 정보</source>
        <translation>재생 정보</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="366"/>
        <source>컨트롤바 보이기</source>
        <translation>컨트롤바 보이기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="367"/>
        <location filename="../src/ui/MainWindow.cpp" line="412"/>
        <source>투명도 증가</source>
        <translation>투명도 증가</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="368"/>
        <location filename="../src/ui/MainWindow.cpp" line="413"/>
        <source>투명도 감소</source>
        <translation>투명도 감소</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="369"/>
        <source>최대 투명도</source>
        <translation>최대 투명도</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="370"/>
        <source>최소 투명도</source>
        <translation>최소 투명도</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="371"/>
        <source>디인터레이스 순차 선택</source>
        <translation>디인터레이스 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="372"/>
        <source>디인터레이스 알고리즘 순차 선택</source>
        <translation>디인터레이스 알고리즘 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="376"/>
        <source>자막 가로 정렬 방법 순차 선택</source>
        <translation>자막 가로 정렬 방법 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="377"/>
        <source>구간 반복 시작</source>
        <translation>구간 반복 시작</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="378"/>
        <source>구간 반복 끝</source>
        <translation>구간 반복 끝</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="379"/>
        <source>구간 반복 활성화</source>
        <translation>구간 반복 활성화</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="380"/>
        <source>키프레임 단위로 이동</source>
        <translation>키프레임 단위로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="381"/>
        <source>오프닝 스킵 사용</source>
        <translation>오프닝 스킵 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="382"/>
        <source>엔딩 스킵 사용</source>
        <translation>엔딩 스킵 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="383"/>
        <source>재생 스킵 사용</source>
        <translation>재생 스킵 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="384"/>
        <source>재생 스킵 설정</source>
        <translation>재생 스킵 설정</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="385"/>
        <source>캡쳐 확장자 순차 선택</source>
        <translation>캡쳐 확장자 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="386"/>
        <source>한 장 캡쳐</source>
        <translation>한 장 캡쳐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="387"/>
        <source>여러 장 캡쳐</source>
        <translation>여러 장 캡쳐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="388"/>
        <source>이전 프레임으로 이동</source>
        <translation>이전 프레임으로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="389"/>
        <source>다음 프레임으로 이동</source>
        <translation>다음 프레임으로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="390"/>
        <source>재생 속도 느리게</source>
        <translation>재생 속도 느리게</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="391"/>
        <source>재생 속도 빠르게</source>
        <translation>재생 속도 빠르게</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="392"/>
        <source>재생 속도 초기화</source>
        <translation>재생 속도 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="393"/>
        <location filename="../src/ui/MainWindow.cpp" line="4516"/>
        <source>영상 속성 초기화</source>
        <translation>영상 속성 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="394"/>
        <source>영상 밝기 감소</source>
        <translation>영상 밝기 감소</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="395"/>
        <source>영상 밝기 증가</source>
        <translation>영상 밝기 증가</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="396"/>
        <source>영상 채도 감소</source>
        <translation>영상 채도 감소</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="397"/>
        <source>영상 채도 증가</source>
        <translation>영상 채도 증가</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="398"/>
        <source>영상 색상 감소</source>
        <translation>영상 색상 감소</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="399"/>
        <source>영상 색상 증가</source>
        <translation>영상 색상 증가</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="400"/>
        <source>영상 대비 감소</source>
        <translation>영상 대비 감소</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="401"/>
        <source>영상 대비 증가</source>
        <translation>영상 대비 증가</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="402"/>
        <source>셰이더 조합</source>
        <translation>셰이더 조합</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="403"/>
        <source>음악 줄임</source>
        <translation>음악 줄임</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="404"/>
        <source>음성 줄임</source>
        <translation>음성 줄임</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="405"/>
        <source>음성 강조</source>
        <translation>음성 강조</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="406"/>
        <source>날카롭게</source>
        <translation>날카롭게</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="407"/>
        <source>선명하게</source>
        <translation>선명하게</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="408"/>
        <source>부드럽게</source>
        <translation>부드럽게</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="409"/>
        <source>좌우 반전</source>
        <translation>좌우 반전</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="410"/>
        <source>상하 반전</source>
        <translation>상하 반전</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="411"/>
        <location filename="../src/ui/MainWindow.cpp" line="4767"/>
        <source>재생 목록에 추가</source>
        <translation>재생 목록에 추가</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="414"/>
        <source>저장 디렉토리 열기</source>
        <translation>저장 디렉토리 열기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="415"/>
        <source>저장 디렉토리 설정</source>
        <translation>저장 디렉토리 설정</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="416"/>
        <source>단축 키 설정</source>
        <translation>단축 키 설정</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="417"/>
        <source>소리 출력 장치 순차 선택</source>
        <translation>소리 출력 장치 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="418"/>
        <source>확장자 연결</source>
        <translation>확장자 연결</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="419"/>
        <source>자막 찾기 켜기</source>
        <translation>자막 찾기 켜기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="420"/>
        <source>가사 찾기 켜기</source>
        <translation>가사 찾기 켜기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="421"/>
        <source>자막 세로 정렬 방법 순차 선택</source>
        <translation>자막 세로 정렬 방법 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="422"/>
        <source>자막 크기 증가</source>
        <translation>자막 크기 증가</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="423"/>
        <source>자막 크기 감소</source>
        <translation>자막 크기 감소</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="424"/>
        <source>자막 크기 초기화</source>
        <translation>자막 크기 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="425"/>
        <source>투명도 초기화</source>
        <translation>투명도 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="426"/>
        <source>화면 비율 순차 선택</source>
        <translation>화면 비율 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="427"/>
        <source>화면 비율 사용자 지정</source>
        <translation>화면 비율 사용자 지정</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="428"/>
        <source>화면 크기 0.5배</source>
        <translation>화면 크기 0.5배</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="429"/>
        <source>화면 크기 1배</source>
        <translation>화면 크기 1배</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="430"/>
        <source>화면 크기 1.5배</source>
        <translation>화면 크기 1.5배</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="431"/>
        <source>화면 크기 2배</source>
        <translation>화면 크기 2배</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="432"/>
        <source>하드웨어 디코더 사용</source>
        <translation>하드웨어 디코더 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="433"/>
        <source>S/PDIF 출력 사용</source>
        <translation>S/PDIF 출력 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="434"/>
        <source>S/PDIF 샘플 속도 순차 선택</source>
        <translation>S/PDIF 샘플 속도 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="435"/>
        <source>고속 렌더링 사용</source>
        <translation>고속 렌더링 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="436"/>
        <source>3D 영상 출력 방법 순차 선택</source>
        <translation>3D 영상 출력 방법 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="437"/>
        <source>수직 동기화 사용</source>
        <translation>수직 동기화 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="438"/>
        <source>이전 챕터로 이동</source>
        <translation>이전 챕터로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="439"/>
        <source>다음 챕터로 이동</source>
        <translation>다음 챕터로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="441"/>
        <source>종료 시 재생 목록 비움</source>
        <translation>종료 시 재생 목록 비움</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="442"/>
        <source>앨범 자켓 보기</source>
        <translation>앨범 자켓 보기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="443"/>
        <source>재생 위치 기억</source>
        <translation>재생 위치 기억</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="444"/>
        <source>인코딩 설정</source>
        <translation>인코딩 설정</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="445"/>
        <source>서버 설정</source>
        <translation>서버 설정</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="448"/>
        <source>S/PDIF 소리 출력 장치 순차 선택</source>
        <translation>S/PDIF 소리 출력 장치 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="449"/>
        <source>장면 탐색</source>
        <translation>장면 탐색</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="450"/>
        <source>시작 위치 0.1초 뒤로 이동</source>
        <translation>시작 위치 0.1초 뒤로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="451"/>
        <source>시작 위치 0.1초 앞으로 이동</source>
        <translation>시작 위치 0.1초 앞으로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="452"/>
        <source>끝 위치 0.1초 뒤로 이동</source>
        <translation>끝 위치 0.1초 뒤로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="453"/>
        <source>끝 위치 0.1초 앞으로 이동</source>
        <translation>끝 위치 0.1초 앞으로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="454"/>
        <source>구간 반복 0.1초 뒤로 이동</source>
        <translation>구간 반복 0.1초 뒤로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="455"/>
        <source>구간 반복 0.1초 앞으로 이동</source>
        <translation>구간 반복 0.1초 앞으로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="456"/>
        <source>프레임 드랍 사용</source>
        <translation>프레임 드랍 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="458"/>
        <source>애너글리프 알고리즘 순차 선택</source>
        <translation>애너글리프 알고리즘 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="459"/>
        <source>3D 자막 출력 방법 순차 선택</source>
        <translation>3D 자막 출력 방법 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="460"/>
        <source>3D 전체 해상도 사용</source>
        <translation>3D 전체 해상도 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="461"/>
        <source>3D 자막 기본 위치</source>
        <translation>3D 자막 기본 위치</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="462"/>
        <source>3D 자막 위치 가깝게(세로)</source>
        <translation>3D 자막 위치 가깝게(세로)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="463"/>
        <source>3D 자막 위치 멀게(세로)</source>
        <translation>3D 자막 위치 멀게(세로)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="464"/>
        <source>3D 자막 위치 가깝게(가로)</source>
        <translation>3D 자막 위치 가깝게(가로)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="465"/>
        <source>3D 자막 위치 멀게(가로)</source>
        <translation>3D 자막 위치 멀게(가로)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="467"/>
        <location filename="../src/ui/MainWindow.cpp" line="1640"/>
        <source>자막 폰트 가져오기</source>
        <translation>자막 폰트 가져오기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="468"/>
        <source>화면 회전 각도 순차 선택</source>
        <translation>화면 회전 각도 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="469"/>
        <source>장치 열기</source>
        <translation>장치 열기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="470"/>
        <source>재생 목록에 DTV 채널 추가</source>
        <translation>재생 목록에 DTV 채널 추가</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="471"/>
        <source>DTV 채널 검색</source>
        <translation>DTV 채널 검색</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="472"/>
        <source>히스토그램 이퀄라이저</source>
        <translation>히스토그램 이퀄라이저</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="473"/>
        <source>3D 노이즈 제거</source>
        <translation>3D 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="474"/>
        <source>채널 편성표</source>
        <translation>채널 편성표</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="476"/>
        <source>디밴드</source>
        <translation>디밴드</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="477"/>
        <source>적응 시간 평균 노이즈 제거</source>
        <translation>적응 시간 평균 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="478"/>
        <source>Overcomplete Wavelet 노이즈 제거</source>
        <translation>Overcomplete Wavelet 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="479"/>
        <source>버퍼링 모드 사용</source>
        <translation>버퍼링 모드 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="481"/>
        <source>정지(추가)</source>
        <translation>정지(추가)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="503"/>
        <location filename="../src/ui/MainWindow.cpp" line="550"/>
        <source>사용 안 함</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="504"/>
        <source>왼쪽 영상 사용</source>
        <translation>왼쪽 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="505"/>
        <source>오른쪽 영상 사용</source>
        <translation>오른쪽 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="506"/>
        <source>상단 영상 사용</source>
        <translation>상단 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="507"/>
        <source>하단 영상 사용</source>
        <translation>하단 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="508"/>
        <source>좌우 영상 사용</source>
        <translation>좌우 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="509"/>
        <source>상하 영상 사용</source>
        <translation>상하 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="514"/>
        <source>왼쪽 영상 우선 사용 (Row Interlaced)</source>
        <translation>왼쪽 영상 우선 사용 (Row Interlaced)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="515"/>
        <source>오른쪽 영상 우선 사용 (Row Interlaced)</source>
        <translation>오른쪽 영상 우선 사용 (Row Interlaced)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="516"/>
        <source>상단 영상 우선 사용 (Row Interlaced)</source>
        <translation>상단 영상 우선 사용 (Row Interlaced)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="517"/>
        <source>하단 영상 우선 사용 (Row Interlaced)</source>
        <translation>하단 영상 우선 사용 (Row Interlaced)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="518"/>
        <source>왼쪽 영상 우선 사용 (Column Interlaced)</source>
        <translation>왼쪽 영상 우선 사용 (Column Interlaced)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="519"/>
        <source>오른쪽 영상 우선 사용 (Column Interlaced)</source>
        <translation>오른쪽 영상 우선 사용 (Column Interlaced)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="520"/>
        <source>상단 영상 우선 사용 (Column Interlaced)</source>
        <translation>상단 영상 우선 사용 (Column Interlaced)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="521"/>
        <source>하단 영상 우선 사용 (Column Interlaced)</source>
        <translation>하단 영상 우선 사용 (Column Interlaced)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="522"/>
        <source>왼쪽 영상 우선 사용 (Red-Cyan Anaglyph)</source>
        <translation>왼쪽 영상 우선 사용 (Red-Cyan Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="523"/>
        <source>오른쪽 영상 우선 사용 (Red-Cyan Anaglyph)</source>
        <translation>오른쪽 영상 우선 사용 (Red-Cyan Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="524"/>
        <source>상단 영상 우선 사용 (Red-Cyan Anaglyph)</source>
        <translation>상단 영상 우선 사용 (Red-Cyan Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="525"/>
        <source>하단 영상 우선 사용 (Red-Cyan Anaglyph)</source>
        <translation>하단 영상 우선 사용 (Red-Cyan Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="526"/>
        <source>왼쪽 영상 우선 사용 (Green-Magenta Anaglyph)</source>
        <translation>왼쪽 영상 우선 사용 (Green-Magenta Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="527"/>
        <source>오른쪽 영상 우선 사용 (Green-Magenta Anaglyph)</source>
        <translation>오른쪽 영상 우선 사용 (Green-Magenta Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="528"/>
        <source>상단 영상 우선 사용 (Green-Magenta Anaglyph)</source>
        <translation>상단 영상 우선 사용 (Green-Magenta Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="529"/>
        <source>하단 영상 우선 사용 (Green-Magenta Anaglyph)</source>
        <translation>하단 영상 우선 사용 (Green-Magenta Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="534"/>
        <source>왼쪽 영상 우선 사용 (Red-Blue Anaglyph)</source>
        <translation>왼쪽 영상 우선 사용 (Red-Blue Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="535"/>
        <source>오른쪽 영상 우선 사용 (Red-Blue Anaglyph)</source>
        <translation>오른쪽 영상 우선 사용 (Red-Blue Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="536"/>
        <source>상단 영상 우선 사용 (Red-Blue Anaglyph)</source>
        <translation>상단 영상 우선 사용 (Red-Blue Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="537"/>
        <source>하단 영상 우선 사용 (Red-Blue Anaglyph)</source>
        <translation>하단 영상 우선 사용 (Red-Blue Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="538"/>
        <source>왼쪽 영상 우선 사용 (Red-Green Anaglyph)</source>
        <translation>왼쪽 영상 우선 사용 (Red-Green Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="539"/>
        <source>오른쪽 영상 우선 사용 (Red-Green Anaglyph)</source>
        <translation>오른쪽 영상 우선 사용 (Red-Green Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="540"/>
        <source>상단 영상 우선 사용 (Red-Green Anaglyph)</source>
        <translation>상단 영상 우선 사용 (Red-Green Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="541"/>
        <source>하단 영상 우선 사용 (Red-Green Anaglyph)</source>
        <translation>하단 영상 우선 사용 (Red-Green Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="542"/>
        <source>왼쪽 영상 우선 사용 (Checker Board)</source>
        <translation>왼쪽 영상 우선 사용 (Checker Board)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="543"/>
        <source>오른쪽 영상 우선 사용 (Checker Board)</source>
        <translation>오른쪽 영상 우선 사용 (Checker Board)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="544"/>
        <source>상단 영상 우선 사용 (Checker Board)</source>
        <translation>상단 영상 우선 사용 (Checker Board)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="545"/>
        <source>하단 영상 우선 사용 (Checker Board)</source>
        <translation>하단 영상 우선 사용 (Checker Board)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="551"/>
        <source>상/하</source>
        <translation>상/하</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="552"/>
        <source>좌/우</source>
        <translation>좌/우</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="553"/>
        <source>페이지 플리핑</source>
        <translation>페이지 플리핑</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="554"/>
        <source>인터레이스</source>
        <translation>인터레이스</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="555"/>
        <source>애너글리프</source>
        <translation>애너글리프</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="556"/>
        <source>체커 보드</source>
        <translation>체커 보드</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1166"/>
        <source>전체 순차 재생</source>
        <translation>전체 순차 재생</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1169"/>
        <source>전체 반복 재생</source>
        <translation>전체 반복 재생</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1172"/>
        <source>한 개 재생</source>
        <translation>한 개 재생</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1175"/>
        <source>한 개 반복 재생</source>
        <translation>한 개 반복 재생</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1178"/>
        <source>무작위 재생</source>
        <translation>무작위 재생</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1184"/>
        <source>재생 순서 (%1)</source>
        <translation>재생 순서 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1220"/>
        <source>탐색 중입니다</source>
        <translation>탐색 중입니다</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1549"/>
        <source>애너글리프 알고리즘 (%1)</source>
        <translation>애너글리프 알고리즘 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1633"/>
        <source>관리자 권한으로 실행되지 않을 경우 정상적으로 가져오기가 안될 수 있습니다.
만약에 관리자 권한이 아닐 경우 아래 경로에 폰트를 수동으로 복사 하세요.
계속 하시겠습니까?</source>
        <translation>관리자 권한으로 실행되지 않을 경우 정상적으로 가져오기가 안될 수 있습니다.
만약에 관리자 권한이 아닐 경우 아래 경로에 폰트를 수동으로 복사 하세요.
계속 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1639"/>
        <source>폰트 (*.%2);;모든 파일 (*.*)</source>
        <translation>폰트 (*.%2);;모든 파일 (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1657"/>
        <source>복사가 완료 되었습니다.</source>
        <translation>복사가 완료 되었습니다.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1659"/>
        <source>일부 파일이 복사되지 않았습니다.</source>
        <translation>일부 파일이 복사되지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1816"/>
        <source>히스토그램 이퀄라이저 켜짐</source>
        <translation>히스토그램 이퀄라이저 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1818"/>
        <source>히스토그램 이퀄라이저 꺼짐</source>
        <translation>히스토그램 이퀄라이저 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1837"/>
        <source>3D 노이즈 제거 켜짐</source>
        <translation>3D 노이즈 제거 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1839"/>
        <source>3D 노이즈 제거 꺼짐</source>
        <translation>3D 노이즈 제거 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1858"/>
        <source>적응 시간 평균 노이즈 제거 켜짐</source>
        <translation>적응 시간 평균 노이즈 제거 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1860"/>
        <source>적응 시간 평균 노이즈 제거 꺼짐</source>
        <translation>적응 시간 평균 노이즈 제거 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1879"/>
        <source>Overcomplete Wavelet 노이즈 제거 켜짐</source>
        <translation>Overcomplete Wavelet 노이즈 제거 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1881"/>
        <source>Overcomplete Wavelet 노이즈 제거 꺼짐</source>
        <translation>Overcomplete Wavelet 노이즈 제거 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1900"/>
        <source>디밴드 켜짐</source>
        <translation>디밴드 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1902"/>
        <source>디밴드 꺼짐</source>
        <translation>디밴드 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2177"/>
        <source>이전 챕터로 이동 (%1)</source>
        <translation>이전 챕터로 이동 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2189"/>
        <source>다음 챕터로 이동 (%1)</source>
        <translation>다음 챕터로 이동 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2658"/>
        <source>가사가 없습니다</source>
        <translation>가사가 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2876"/>
        <source>다음 에러로 중지 되었습니다 : %1</source>
        <translation>다음 에러로 중지 되었습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2928"/>
        <source>모노</source>
        <translation>모노</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2930"/>
        <source>스테레오</source>
        <translation>스테레오</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2932"/>
        <source>서라운드</source>
        <translation>서라운드</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2991"/>
        <source>외부 서버에 자막이 존재합니다.
이동하시려면 여기를 클릭하세요.</source>
        <translation>외부 서버에 자막이 존재합니다.
이동하시려면 여기를 클릭하세요.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3030"/>
        <location filename="../src/ui/MainWindow.cpp" line="3050"/>
        <source>파일을 열 수 없습니다</source>
        <translation>파일을 열 수 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3200"/>
        <source>재생 목록에서 삭제 하시겠습니까?</source>
        <translation>재생 목록에서 삭제 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3417"/>
        <source>화면 크기 %1배</source>
        <translation>화면 크기 %1배</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3620"/>
        <source>투명도 (%1%)</source>
        <translation>투명도 (%1%)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3702"/>
        <source>항상 위 켜짐</source>
        <translation>항상 위 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3704"/>
        <source>항상 위 꺼짐</source>
        <translation>항상 위 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3711"/>
        <source>지원하는 모든 파일 (*.%1);;동영상 (*.%2);;음악 (*.%3);;자막 / 가사 (*.%4);;재생 목록 (*.%5);;모든 파일 (*.*)</source>
        <translation>지원하는 모든 파일 (*.%1);;동영상 (*.%2);;음악 (*.%3);;자막 / 가사 (*.%4);;재생 목록 (*.%5);;모든 파일 (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4762"/>
        <source>지원하는 모든 파일 (*.%1);;동영상 (*.%2);;음악 (*.%3);;재생 목록 (*.%4);;모든 파일 (*.*)</source>
        <translation>지원하는 모든 파일 (*.%1);;동영상 (*.%2);;음악 (*.%3);;재생 목록 (*.%4);;모든 파일 (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3809"/>
        <source>자막을 변경 할 수 없습니다</source>
        <translation>자막을 변경 할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="440"/>
        <source>외부 닫기</source>
        <translation>외부 닫기</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="446"/>
        <source>저장</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="447"/>
        <source>다른 이름으로 저장</source>
        <translation>다른 이름으로 저장</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="457"/>
        <source>인코딩 순차 선택</source>
        <translation>인코딩 순차 선택</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="466"/>
        <source>검색 디렉토리</source>
        <translation>검색 디렉토리</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="475"/>
        <source>고급 검색</source>
        <translation>고급 검색</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="510"/>
        <source>왼쪽 영상 우선 사용 (Page Flipping)</source>
        <translation>왼쪽 영상 우선 사용 (Page Flipping)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="511"/>
        <source>오른쪽 영상 우선 사용 (Page Flipping)</source>
        <translation>오른쪽 영상 우선 사용 (Page Flipping)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="512"/>
        <source>상단 영상 우선 사용 (Page Flipping)</source>
        <translation>상단 영상 우선 사용 (Page Flipping)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="513"/>
        <source>하단 영상 우선 사용 (Page Flipping)</source>
        <translation>하단 영상 우선 사용 (Page Flipping)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="530"/>
        <source>왼쪽 영상 우선 사용 (Yellow-Blue Anaglyph)</source>
        <translation>왼쪽 영상 우선 사용 (Yellow-Blue Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="531"/>
        <source>오른쪽 영상 우선 사용 (Yellow-Blue Anaglyph)</source>
        <translation>오른쪽 영상 우선 사용 (Yellow-Blue Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="532"/>
        <source>상단 영상 우선 사용 (Yellow-Blue Anaglyph)</source>
        <translation>상단 영상 우선 사용 (Yellow-Blue Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="533"/>
        <source>하단 영상 우선 사용 (Yellow-Blue Anaglyph)</source>
        <translation>하단 영상 우선 사용 (Yellow-Blue Anaglyph)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="1786"/>
        <source>DTV 수신 장치가 없습니다.</source>
        <translation>DTV 수신 장치가 없습니다.</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2277"/>
        <source>자막</source>
        <translation>자막</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="2278"/>
        <source>%1 (*.%2);;모든 파일 (*.*)</source>
        <translation>%1 (*.%2);;모든 파일 (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3811"/>
        <source>가사를 변경 할 수 없습니다</source>
        <translation>가사를 변경 할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3823"/>
        <source>음성을 변경 할 수 없습니다</source>
        <translation>음성을 변경 할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="3907"/>
        <source>일반 화면</source>
        <translation>일반 화면</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4206"/>
        <source>컨트롤바 보임</source>
        <translation>컨트롤바 보임</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4208"/>
        <source>컨트롤바 숨김</source>
        <translation>컨트롤바 숨김</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4544"/>
        <source>영상 밝기 %1배</source>
        <translation>영상 밝기 %1배</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4572"/>
        <source>영상 채도 %1배</source>
        <translation>영상 채도 %1배</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4603"/>
        <source>영상 색상 각도 : %1°</source>
        <translation>영상 색상 각도 : %1°</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4631"/>
        <source>영상 대비 %1배</source>
        <translation>영상 대비 %1배</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4672"/>
        <source>영상 날카롭게 켜짐</source>
        <translation>영상 날카롭게 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4674"/>
        <source>영상 날카롭게 꺼짐</source>
        <translation>영상 날카롭게 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4692"/>
        <source>영상 선명하게 켜짐</source>
        <translation>영상 선명하게 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4694"/>
        <source>영상 선명하게 꺼짐</source>
        <translation>영상 선명하게 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4712"/>
        <source>영상 부드럽게 켜짐</source>
        <translation>영상 부드럽게 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4714"/>
        <source>영상 부드럽게 꺼짐</source>
        <translation>영상 부드럽게 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4732"/>
        <source>영상 좌우 반전 켜짐</source>
        <translation>영상 좌우 반전 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4734"/>
        <source>영상 좌우 반전 꺼짐</source>
        <translation>영상 좌우 반전 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4752"/>
        <source>영상 상하 반전 켜짐</source>
        <translation>영상 상하 반전 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/MainWindow.cpp" line="4754"/>
        <source>영상 상하 반전 꺼짐</source>
        <translation>영상 상하 반전 꺼짐</translation>
    </message>
</context>
<context>
    <name>MediaPlayer</name>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="131"/>
        <source>자막 열기 : %1</source>
        <translation>자막 열기 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="133"/>
        <source>가사 열기 : %1</source>
        <translation>가사 열기 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="148"/>
        <source>자막이 저장 되었습니다 : %1</source>
        <translation>자막이 저장 되었습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="150"/>
        <source>가사가 저장 되었습니다 : %1</source>
        <translation>가사가 저장 되었습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="155"/>
        <source>자막이 저장 되지 않았습니다 : %1</source>
        <translation>자막이 저장 되지 않았습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="157"/>
        <source>가사가 저장 되지 않았습니다 : %1</source>
        <translation>가사가 저장 되지 않았습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="175"/>
        <source>외부 자막 닫기</source>
        <translation>외부 자막 닫기</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="177"/>
        <source>외부 가사 닫기</source>
        <translation>외부 가사 닫기</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="413"/>
        <source>자막 보이기</source>
        <translation>자막 보이기</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="415"/>
        <source>자막 숨기기</source>
        <translation>자막 숨기기</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="420"/>
        <source>가사 보이기</source>
        <translation>가사 보이기</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="422"/>
        <source>가사 숨기기</source>
        <translation>가사 숨기기</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="442"/>
        <source>고급 자막 검색 사용</source>
        <translation>고급 자막 검색 사용</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="444"/>
        <source>고급 자막 검색 사용 안 함</source>
        <translation>고급 자막 검색 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="449"/>
        <source>고급 가사 검색 사용</source>
        <translation>고급 가사 검색 사용</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="451"/>
        <source>고급 가사 검색 사용 안 함</source>
        <translation>고급 가사 검색 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="502"/>
        <source>자막 언어 변경</source>
        <translation>자막 언어 변경</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="504"/>
        <source>가사 언어 변경</source>
        <translation>가사 언어 변경</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="518"/>
        <source>자막 위치 초기화</source>
        <translation>자막 위치 초기화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="562"/>
        <source>3D 자막 위치 초기화</source>
        <translation>3D 자막 위치 초기화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="572"/>
        <source>3D 자막 위치 가깝게(세로) : %1</source>
        <translation>3D 자막 위치 가깝게(세로) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="574"/>
        <source>3D 자막 위치 멀게(세로) : %1</source>
        <translation>3D 자막 위치 멀게(세로) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="586"/>
        <source>3D 자막 위치 가깝게(가로) : %1</source>
        <translation>3D 자막 위치 가깝게(가로) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="588"/>
        <source>3D 자막 위치 멀게(가로) : %1</source>
        <translation>3D 자막 위치 멀게(가로) : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="610"/>
        <source>구간 반복 시작 : %1</source>
        <translation>구간 반복 시작 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="623"/>
        <source>구간 반복 끝 : %1</source>
        <translation>구간 반복 끝 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="639"/>
        <source>구간 반복 활성화</source>
        <translation>구간 반복 활성화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="641"/>
        <source>구간 반복 비활성화</source>
        <translation>구간 반복 비활성화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="647"/>
        <source>시작과 끝 시각이 같으므로 활성화 되지 않습니다</source>
        <translation>시작과 끝 시각이 같으므로 활성화 되지 않습니다</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="685"/>
        <source>구간 반복 시작 위치 %1초 뒤로 이동 (%2)</source>
        <translation>구간 반복 시작 위치 %1초 뒤로 이동 (%2)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="687"/>
        <source>구간 반복 시작 위치 %1초 앞으로 이동 (%2)</source>
        <translation>구간 반복 시작 위치 %1초 앞으로 이동 (%2)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="693"/>
        <source>구간 반복 시작 위치가 범위를 벗어났습니다</source>
        <translation>구간 반복 시작 위치가 범위를 벗어났습니다</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="698"/>
        <location filename="../src/media/MediaPlayer.cpp" line="732"/>
        <location filename="../src/media/MediaPlayer.cpp" line="771"/>
        <source>구간 반복이 설정 되지 않았습니다</source>
        <translation>구간 반복이 설정 되지 않았습니다</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="719"/>
        <source>구간 반복 끝 위치 %1초 뒤로 이동 (%2)</source>
        <translation>구간 반복 끝 위치 %1초 뒤로 이동 (%2)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="721"/>
        <source>구간 반복 끝 위치 %1초 앞으로 이동 (%2)</source>
        <translation>구간 반복 끝 위치 %1초 앞으로 이동 (%2)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="727"/>
        <source>구간 반복 끝 위치가 범위를 벗어났습니다</source>
        <translation>구간 반복 끝 위치가 범위를 벗어났습니다</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="757"/>
        <source>구간 반복 위치 %1초 뒤로 이동 (%2 ~ %3)</source>
        <translation>구간 반복 위치 %1초 뒤로 이동 (%2 ~ %3)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="759"/>
        <source>구간 반복 위치 %1초 앞으로 이동 (%2 ~ %3)</source>
        <translation>구간 반복 위치 %1초 앞으로 이동 (%2 ~ %3)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="766"/>
        <source>구간 반복 위치가 범위를 벗어났습니다</source>
        <translation>구간 반복 위치가 범위를 벗어났습니다</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="782"/>
        <source>키프레임 단위로 이동 함</source>
        <translation>키프레임 단위로 이동 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="784"/>
        <source>키프레임 단위로 이동 안 함</source>
        <translation>키프레임 단위로 이동 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="825"/>
        <source>3D 자막 (%1)</source>
        <translation>3D 자막 (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="852"/>
        <source>오프닝 스킵 사용 함</source>
        <translation>오프닝 스킵 사용 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="854"/>
        <source>오프닝 스킵 사용 안 함</source>
        <translation>오프닝 스킵 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="876"/>
        <source>엔딩 스킵 사용 함</source>
        <translation>엔딩 스킵 사용 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="878"/>
        <source>엔딩 스킵 사용 안 함</source>
        <translation>엔딩 스킵 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="919"/>
        <source>노멀라이저 켜짐</source>
        <translation>노멀라이저 켜짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="921"/>
        <source>노멀라이저 꺼짐</source>
        <translation>노멀라이저 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="941"/>
        <source>이퀄라이저 켜짐</source>
        <translation>이퀄라이저 켜짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="943"/>
        <source>이퀄라이저 꺼짐</source>
        <translation>이퀄라이저 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="963"/>
        <source>음악 줄임 켜짐</source>
        <translation>음악 줄임 켜짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="965"/>
        <source>음악 줄임 꺼짐</source>
        <translation>음악 줄임 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="981"/>
        <source>자막 투명도</source>
        <translation>자막 투명도</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="983"/>
        <source>가사 투명도</source>
        <translation>가사 투명도</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1007"/>
        <source>자막 투명도 초기화</source>
        <translation>자막 투명도 초기화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1009"/>
        <source>가사 투명도 초기화</source>
        <translation>가사 투명도 초기화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1018"/>
        <source>자막 크기</source>
        <translation>자막 크기</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1041"/>
        <source>자막 크기 초기화</source>
        <translation>자막 크기 초기화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1055"/>
        <source>하드웨어 디코더 사용 함</source>
        <translation>하드웨어 디코더 사용 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1062"/>
        <source>하드웨어 디코더 사용 안 함</source>
        <translation>하드웨어 디코더 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1080"/>
        <source>프레임 드랍 사용 함</source>
        <translation>프레임 드랍 사용 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1082"/>
        <source>프레임 드랍 사용 안 함</source>
        <translation>프레임 드랍 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1099"/>
        <source>버퍼링 모드 사용 함</source>
        <translation>버퍼링 모드 사용 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1101"/>
        <source>버퍼링 모드 사용 안 함</source>
        <translation>버퍼링 모드 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1122"/>
        <source>S/PDIF 출력 사용 안 함</source>
        <translation>S/PDIF 출력 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1158"/>
        <source>S/PDIF 출력 시 인코딩 사용 안 함</source>
        <translation>S/PDIF 출력 시 인코딩 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1161"/>
        <source>S/PDIF 출력 시 AC3 인코딩 사용</source>
        <translation>S/PDIF 출력 시 AC3 인코딩 사용</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1190"/>
        <source>3D 전체 해상도 사용</source>
        <translation>3D 전체 해상도 사용</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1192"/>
        <source>3D 전체 해상도 사용 안 함</source>
        <translation>3D 전체 해상도 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1209"/>
        <source>고속 렌더링 사용</source>
        <translation>고속 렌더링 사용</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1211"/>
        <source>고속 렌더링 사용 안 함</source>
        <translation>고속 렌더링 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1265"/>
        <source>음성 줄임 켜짐</source>
        <translation>음성 줄임 켜짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1267"/>
        <source>음성 줄임 꺼짐</source>
        <translation>음성 줄임 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1287"/>
        <source>음성 강조 켜짐</source>
        <translation>음성 강조 켜짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1289"/>
        <source>음성 강조 꺼짐</source>
        <translation>음성 강조 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1372"/>
        <source>자막 있음</source>
        <translation>자막 있음</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1374"/>
        <source>자막 없음</source>
        <translation>자막 없음</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1379"/>
        <source>가사 있음</source>
        <translation>가사 있음</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1381"/>
        <source>가사 없음</source>
        <translation>가사 없음</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1578"/>
        <source>화면 비율 사용 함</source>
        <translation>화면 비율 사용 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1581"/>
        <source> (화면 채우기)</source>
        <translation> (화면 채우기)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1587"/>
        <source>화면 비율 사용 안 함</source>
        <translation>화면 비율 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1695"/>
        <source>자막 가로 정렬 변경</source>
        <translation>자막 가로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1697"/>
        <source>가사 가로 정렬 변경</source>
        <translation>가사 가로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1702"/>
        <source>자동 정렬</source>
        <translation>자동 정렬</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1705"/>
        <source>왼쪽 정렬</source>
        <translation>왼쪽 정렬</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1708"/>
        <source>오른쪽 정렬</source>
        <translation>오른쪽 정렬</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1711"/>
        <source>가운데 정렬</source>
        <translation>가운데 정렬</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1714"/>
        <location filename="../src/media/MediaPlayer.cpp" line="1750"/>
        <source>기본 정렬</source>
        <translation>기본 정렬</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1737"/>
        <source>자막 세로 정렬 변경</source>
        <translation>자막 세로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1739"/>
        <source>가사 세로 정렬 변경</source>
        <translation>가사 세로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1744"/>
        <source>상단 정렬</source>
        <translation>상단 정렬</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1747"/>
        <source>하단 정렬</source>
        <translation>하단 정렬</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1782"/>
        <source>자막 싱크 초기화</source>
        <translation>자막 싱크 초기화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1784"/>
        <source>가사 싱크 초기화</source>
        <translation>가사 싱크 초기화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1802"/>
        <source>자막 싱크 %1초 빠르게</source>
        <translation>자막 싱크 %1초 빠르게</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1804"/>
        <source>자막 싱크 %1초 느리게</source>
        <translation>자막 싱크 %1초 느리게</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1809"/>
        <source>가사 싱크 %1초 빠르게</source>
        <translation>가사 싱크 %1초 빠르게</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1811"/>
        <source>가사 싱크 %1초 느리게</source>
        <translation>가사 싱크 %1초 느리게</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1817"/>
        <location filename="../src/media/MediaPlayer.cpp" line="1895"/>
        <source> (%1초)</source>
        <translation> (%1초)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1857"/>
        <source>자막 찾기 켜짐</source>
        <translation>자막 찾기 켜짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1859"/>
        <source>자막 찾기 꺼짐</source>
        <translation>자막 찾기 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1870"/>
        <source>가사 찾기 켜짐</source>
        <translation>가사 찾기 켜짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1872"/>
        <source>가사 찾기 꺼짐</source>
        <translation>가사 찾기 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1888"/>
        <source>소리 싱크 %1초 빠르게</source>
        <translation>소리 싱크 %1초 빠르게</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1890"/>
        <source>소리 싱크 %1초 느리게</source>
        <translation>소리 싱크 %1초 느리게</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1950"/>
        <source>앞으로 %1초 키프레임 이동</source>
        <translation>앞으로 %1초 키프레임 이동</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1952"/>
        <source>뒤로 %1초 키프레임 이동</source>
        <translation>뒤로 %1초 키프레임 이동</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1957"/>
        <source>앞으로 %1초 이동</source>
        <translation>앞으로 %1초 이동</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1959"/>
        <source>뒤로 %1초 이동</source>
        <translation>뒤로 %1초 이동</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1997"/>
        <source>소리 꺼짐</source>
        <translation>소리 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1999"/>
        <source>소리 켜짐</source>
        <translation>소리 켜짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2090"/>
        <source>앨범 자켓 숨기기</source>
        <translation>앨범 자켓 숨기기</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2226"/>
        <source>S/PDIF 소리 출력 장치 변경 (%1)</source>
        <translation>S/PDIF 소리 출력 장치 변경 (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2250"/>
        <source>90도</source>
        <translation>90도</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2253"/>
        <source>180도</source>
        <translation>180도</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2256"/>
        <source>270도</source>
        <translation>270도</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2262"/>
        <source>화면 회전 각도 (%1)</source>
        <translation>화면 회전 각도 (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1662"/>
        <location filename="../src/media/MediaPlayer.cpp" line="2247"/>
        <source>사용 안 함</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="528"/>
        <source>자막 위치 위로 : %1</source>
        <translation>자막 위치 위로 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="530"/>
        <source>자막 위치 아래로 : %1</source>
        <translation>자막 위치 아래로 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="542"/>
        <source>자막 위치 왼쪽으로 : %1</source>
        <translation>자막 위치 왼쪽으로 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="544"/>
        <source>자막 위치 오른쪽으로 : %1</source>
        <translation>자막 위치 오른쪽으로 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="812"/>
        <source>3D 영상 (%1)</source>
        <translation>3D 영상 (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="896"/>
        <source>재생 스킵 사용 함</source>
        <translation>재생 스킵 사용 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="898"/>
        <source>재생 스킵 사용 안 함</source>
        <translation>재생 스킵 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1058"/>
        <source> (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)</source>
        <translation> (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1120"/>
        <source>S/PDIF 출력 사용</source>
        <translation>S/PDIF 출력 사용</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1126"/>
        <location filename="../src/media/MediaPlayer.cpp" line="1172"/>
        <location filename="../src/media/MediaPlayer.cpp" line="1244"/>
        <location filename="../src/media/MediaPlayer.cpp" line="2228"/>
        <source>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1164"/>
        <source>S/PDIF 출력 시 DTS 인코딩 사용</source>
        <translation>S/PDIF 출력 시 DTS 인코딩 사용</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1235"/>
        <source>S/PDIF 샘플 속도 (%1)</source>
        <translation>S/PDIF 샘플 속도 (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1238"/>
        <source>기본 속도</source>
        <translation>기본 속도</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1430"/>
        <source>일시정지</source>
        <translation>일시정지</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1454"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1514"/>
        <source>이전으로 %1 프레임 이동</source>
        <translation>이전으로 %1 프레임 이동</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1529"/>
        <source>다음으로 %1 프레임 이동</source>
        <translation>다음으로 %1 프레임 이동</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1561"/>
        <source>재생 속도 초기화</source>
        <translation>재생 속도 초기화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1565"/>
        <source>재생 속도 : %1배</source>
        <translation>재생 속도 : %1배</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1637"/>
        <source>음성 변경 (%1)</source>
        <translation>음성 변경 (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1656"/>
        <source>자동 판단</source>
        <translation>자동 판단</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1659"/>
        <source>항상 사용</source>
        <translation>항상 사용</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1668"/>
        <source>디인터레이스 (%1)</source>
        <translation>디인터레이스 (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1676"/>
        <source>디인터레이스 알고리즘 (%1)</source>
        <translation>디인터레이스 알고리즘 (%1)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1836"/>
        <source>소리 싱크 초기화</source>
        <translation>소리 싱크 초기화</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="1983"/>
        <source>소리 (%1%)</source>
        <translation>소리 (%1%)</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2088"/>
        <source>앨범 자켓 보이기</source>
        <translation>앨범 자켓 보이기</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2121"/>
        <source>재생 위치 기억 켜짐</source>
        <translation>재생 위치 기억 켜짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2123"/>
        <source>재생 위치 기억 꺼짐</source>
        <translation>재생 위치 기억 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2167"/>
        <location filename="../src/media/MediaPlayer.cpp" line="2207"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPlayer.cpp" line="2183"/>
        <source>소리 출력 장치 변경 (%1)</source>
        <translation>소리 출력 장치 변경 (%1)</translation>
    </message>
</context>
<context>
    <name>MediaPresenter</name>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2522"/>
        <source>가사 있음</source>
        <translation>가사 있음</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2524"/>
        <source>자막 있음</source>
        <translation>자막 있음</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2529"/>
        <source>가사 없음</source>
        <translation>가사 없음</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2531"/>
        <source>자막 없음</source>
        <translation>자막 없음</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2537"/>
        <source>파일 이름 : </source>
        <translation>파일 이름 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2550"/>
        <source>재생 위치 : </source>
        <translation>재생 위치 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2564"/>
        <source>비디오 코덱 : </source>
        <translation>비디오 코덱 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2575"/>
        <source>하드웨어 디코더 : </source>
        <translation>하드웨어 디코더 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2583"/>
        <location filename="../src/media/MediaPresenter.cpp" line="2720"/>
        <source>입력 : </source>
        <translation>입력 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2598"/>
        <location filename="../src/media/MediaPresenter.cpp" line="2734"/>
        <source>출력 : </source>
        <translation>출력 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2619"/>
        <location filename="../src/media/MediaPresenter.cpp" line="2630"/>
        <source>프레임 : </source>
        <translation>프레임 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2657"/>
        <source>DTV 신호 감도 : </source>
        <translation>DTV 신호 감도 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2666"/>
        <source>CPU 사용률 : </source>
        <translation>CPU 사용률 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2676"/>
        <source>오디오 코덱 : </source>
        <translation>오디오 코덱 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2687"/>
        <source>S/PDIF 오디오 장치 : </source>
        <translation>S/PDIF 오디오 장치 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2694"/>
        <source>인코딩 사용</source>
        <translation>인코딩 사용</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2752"/>
        <source>가사 코덱 : </source>
        <translation>가사 코덱 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="2754"/>
        <source>자막 코덱 : </source>
        <translation>자막 코덱 : </translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="4758"/>
        <source>프레임 저하가 일어나고 있습니다. 성능에 영향을 미치는 옵션 또는 수직 동기화를 꺼주세요.</source>
        <translation>프레임 저하가 일어나고 있습니다. 성능에 영향을 미치는 옵션 또는 수직 동기화를 꺼주세요.</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="5700"/>
        <source>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="7158"/>
        <source>오프닝 스킵 : %1</source>
        <translation>오프닝 스킵 : %1</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="7179"/>
        <source>재생 스킵 : %1 ~ %2</source>
        <translation>재생 스킵 : %1 ~ %2</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="7287"/>
        <source>구간 반복 : %1 ~ %2</source>
        <translation>구간 반복 : %1 ~ %2</translation>
    </message>
    <message>
        <location filename="../src/media/MediaPresenter.cpp" line="7295"/>
        <source>S/PDIF 출력을 지원하지 않은 포맷이므로 PCM 출력으로 전환합니다</source>
        <translation>S/PDIF 출력을 지원하지 않은 포맷이므로 PCM 출력으로 전환합니다</translation>
    </message>
</context>
<context>
    <name>MultipleCapture</name>
    <message>
        <location filename="../forms/multiplecapture.ui" line="19"/>
        <source>여러 장 캡쳐</source>
        <translation>여러 장 캡쳐</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="33"/>
        <source>캡쳐 시작</source>
        <translation>캡쳐 시작</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="52"/>
        <source>캡쳐 중지</source>
        <translation>캡쳐 중지</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="81"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="93"/>
        <source>프레임</source>
        <translation>프레임</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="162"/>
        <source>매</source>
        <translation>매</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="177"/>
        <source>지정 시간까지 캡쳐</source>
        <translation>지정 시간까지 캡쳐</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="190"/>
        <source>지정 개수까지 캡쳐</source>
        <translation>지정 개수까지 캡쳐</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="219"/>
        <location filename="../forms/multiplecapture.ui" line="234"/>
        <source>확장자</source>
        <translation>확장자</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="257"/>
        <source>품질</source>
        <translation>품질</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="292"/>
        <source>기본 품질</source>
        <translation>기본 품질</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="302"/>
        <source>캡쳐 현황</source>
        <translation>캡쳐 현황</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="316"/>
        <source>현재 개수</source>
        <translation>현재 개수</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="343"/>
        <source>총 개수</source>
        <translation>총 개수</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="365"/>
        <source>크기</source>
        <translation>크기</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="371"/>
        <source>원본 크기로 캡쳐</source>
        <translation>원본 크기로 캡쳐</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="384"/>
        <source>현재 크기로 캡쳐</source>
        <translation>현재 크기로 캡쳐</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="394"/>
        <source>사용자 정의 크기로 캡쳐</source>
        <translation>사용자 정의 크기로 캡쳐</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="481"/>
        <source>비율</source>
        <translation>비율</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="496"/>
        <source>기타</source>
        <translation>기타</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="502"/>
        <source>자막도 같이 캡쳐</source>
        <translation>자막도 같이 캡쳐</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="509"/>
        <source>세부 정보도 같이 캡쳐</source>
        <translation>세부 정보도 같이 캡쳐</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="525"/>
        <source>저장 위치</source>
        <translation>저장 위치</translation>
    </message>
    <message>
        <location filename="../forms/multiplecapture.ui" line="546"/>
        <source>변경</source>
        <translation>변경</translation>
    </message>
    <message>
        <location filename="../src/ui/MultipleCapture.cpp" line="147"/>
        <source>캡쳐를 할 수 없습니다. 지정 시간 또는 지정 개수를 확인 해 주세요.</source>
        <translation>캡쳐를 할 수 없습니다. 지정 시간 또는 지정 개수를 확인 해 주세요.</translation>
    </message>
</context>
<context>
    <name>OpenDTVScanChannel</name>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="14"/>
        <source>DTV 채널 검색</source>
        <translation>DTV 채널 검색</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="20"/>
        <source>채널 검색</source>
        <translation>채널 검색</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="26"/>
        <source>장치</source>
        <translation>장치</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="48"/>
        <source>어댑터</source>
        <translation>어댑터</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="61"/>
        <source>형식</source>
        <translation>형식</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="81"/>
        <source>상태</source>
        <translation>상태</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="89"/>
        <source>현재 채널 :</source>
        <translation>현재 채널 :</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="103"/>
        <source>찾은 채널 개수 :</source>
        <translation>찾은 채널 개수 :</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="119"/>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="103"/>
        <source>신호 감도</source>
        <translation>신호 감도</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="162"/>
        <source>채널 범위</source>
        <translation>채널 범위</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="215"/>
        <source>검색</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="231"/>
        <source>중지</source>
        <translation>중지</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="248"/>
        <source>국가 선택</source>
        <translation>국가 선택</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="273"/>
        <source>채널 정보</source>
        <translation>채널 정보</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="102"/>
        <source>채널</source>
        <translation>채널</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="343"/>
        <source>저장</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../forms/opendtvscanchannel.ui" line="372"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="104"/>
        <source>이름</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="215"/>
        <source>채널 정보가 없습니다.</source>
        <translation>채널 정보가 없습니다.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="224"/>
        <source>시작 채널이 종료 채널보다 큽니다.</source>
        <translation>시작 채널이 종료 채널보다 큽니다.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDTVScanChannel.cpp" line="233"/>
        <source>채널 범위가 맞지 않습니다.</source>
        <translation>채널 범위가 맞지 않습니다.</translation>
    </message>
</context>
<context>
    <name>OpenDevice</name>
    <message>
        <location filename="../forms/opendevice.ui" line="20"/>
        <source>장치 열기</source>
        <translation>장치 열기</translation>
    </message>
    <message>
        <location filename="../forms/opendevice.ui" line="34"/>
        <source>비디오 장치</source>
        <translation>비디오 장치</translation>
    </message>
    <message>
        <location filename="../forms/opendevice.ui" line="53"/>
        <source>오디오 장치</source>
        <translation>오디오 장치</translation>
    </message>
    <message>
        <location filename="../forms/opendevice.ui" line="107"/>
        <source>열기</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../forms/opendevice.ui" line="136"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDevice.cpp" line="46"/>
        <location filename="../src/ui/OpenDevice.cpp" line="49"/>
        <source>선택 안 함</source>
        <translation>선택 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDevice.cpp" line="157"/>
        <source>비디오 장치를 선택 해 주세요.</source>
        <translation>비디오 장치를 선택 해 주세요.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDevice.cpp" line="161"/>
        <source>오디오 장치를 선택 해 주세요.</source>
        <translation>오디오 장치를 선택 해 주세요.</translation>
    </message>
    <message>
        <location filename="../src/ui/OpenDevice.cpp" line="171"/>
        <source>비디오 장치 또는 오디오 장치를 선택 해야 합니다.</source>
        <translation>비디오 장치 또는 오디오 장치를 선택 해야 합니다.</translation>
    </message>
</context>
<context>
    <name>OpenExternal</name>
    <message>
        <location filename="../forms/openexternal.ui" line="19"/>
        <source>외부 열기</source>
        <translation>외부 열기</translation>
    </message>
    <message>
        <location filename="../forms/openexternal.ui" line="31"/>
        <source>주소를 입력 해 주세요</source>
        <translation>주소를 입력 해 주세요</translation>
    </message>
    <message>
        <location filename="../forms/openexternal.ui" line="55"/>
        <source>주소</source>
        <translation>주소</translation>
    </message>
    <message>
        <location filename="../forms/openexternal.ui" line="110"/>
        <source>예) http://somehost.com/movie.mp4</source>
        <translation>예) http://somehost.com/movie.mp4</translation>
    </message>
    <message>
        <location filename="../forms/openexternal.ui" line="160"/>
        <source>열기</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../forms/openexternal.ui" line="189"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>PlayList</name>
    <message>
        <location filename="../forms/playlist.ui" line="19"/>
        <source>재생 목록</source>
        <translation>재생 목록</translation>
    </message>
    <message>
        <location filename="../forms/playlist.ui" line="71"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="403"/>
        <source>삭제</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="410"/>
        <source>맨 위로</source>
        <translation>맨 위로</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="415"/>
        <source>맨 아래로</source>
        <translation>맨 아래로</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="422"/>
        <source>위로</source>
        <translation>위로</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="427"/>
        <source>아래로</source>
        <translation>아래로</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="434"/>
        <source>파일 경로 보기</source>
        <translation>파일 경로 보기</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="439"/>
        <source>파일 경로 복사</source>
        <translation>파일 경로 복사</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="448"/>
        <source>다른 화질</source>
        <translation>다른 화질</translation>
    </message>
    <message>
        <location filename="../src/ui/PlayList.cpp" line="488"/>
        <source>장면 탐색</source>
        <translation>장면 탐색</translation>
    </message>
</context>
<context>
    <name>Popup</name>
    <message>
        <location filename="../forms/popup.ui" line="19"/>
        <source>알림</source>
        <translation>알림</translation>
    </message>
</context>
<context>
    <name>RemoteFileList</name>
    <message>
        <location filename="../forms/remotefilelist.ui" line="19"/>
        <source>원격 파일 목록</source>
        <translation>원격 파일 목록</translation>
    </message>
    <message>
        <location filename="../forms/remotefilelist.ui" line="67"/>
        <source>폴더,</source>
        <translation>폴더,</translation>
    </message>
    <message>
        <location filename="../forms/remotefilelist.ui" line="87"/>
        <source>파일</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="../forms/remotefilelist.ui" line="119"/>
        <source>새로 고침</source>
        <translation>새로 고침</translation>
    </message>
    <message>
        <location filename="../forms/remotefilelist.ui" line="141"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="62"/>
        <source>이름</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="63"/>
        <source>확장자</source>
        <translation>확장자</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="64"/>
        <source>재생 시간</source>
        <translation>재생 시간</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="65"/>
        <source>전송 속도</source>
        <translation>전송 속도</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="140"/>
        <source>&lt;DIR&gt;</source>
        <translation>&lt;DIR&gt;</translation>
    </message>
    <message>
        <location filename="../src/ui/RemoteFileList.cpp" line="259"/>
        <source>재생 목록에 추가</source>
        <translation>재생 목록에 추가</translation>
    </message>
</context>
<context>
    <name>SPDIFInterface</name>
    <message>
        <location filename="../src/audio/SPDIFInterface.cpp" line="92"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
</context>
<context>
    <name>Screen</name>
    <message>
        <location filename="../src/ui/Screen.cpp" line="377"/>
        <source>캡쳐 성공</source>
        <translation>캡쳐 성공</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="379"/>
        <source>캡쳐 실패</source>
        <translation>캡쳐 실패</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="876"/>
        <source>3D 영상 설정</source>
        <translation>3D 영상 설정</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1163"/>
        <source>애너글리프</source>
        <translation>애너글리프</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="843"/>
        <location filename="../src/ui/Screen.cpp" line="1378"/>
        <location filename="../src/ui/Screen.cpp" line="1427"/>
        <location filename="../src/ui/Screen.cpp" line="2394"/>
        <source>사용 안 함</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1335"/>
        <source>화면 크기</source>
        <translation>화면 크기</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1366"/>
        <source>화면 비율</source>
        <translation>화면 비율</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1380"/>
        <source>화면 채우기</source>
        <translation>화면 채우기</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1382"/>
        <source>사용자 지정 (%1:%2)</source>
        <translation>사용자 지정 (%1:%2)</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1409"/>
        <source>디인터레이스</source>
        <translation>디인터레이스</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1415"/>
        <source>자동 판단</source>
        <translation>자동 판단</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1421"/>
        <source>항상 사용</source>
        <translation>항상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1437"/>
        <source>알고리즘</source>
        <translation>알고리즘</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1494"/>
        <source>캡쳐</source>
        <translation>캡쳐</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1543"/>
        <source>영상 속성</source>
        <translation>영상 속성</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1596"/>
        <source>영상 효과</source>
        <translation>영상 효과</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1820"/>
        <source>한 개 재생</source>
        <translation>한 개 재생</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1826"/>
        <source>한 개 반복 재생</source>
        <translation>한 개 반복 재생</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2194"/>
        <source>언어</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1707"/>
        <source>화면</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="260"/>
        <source>캡쳐를 시작하지 못했습니다</source>
        <translation>캡쳐를 시작하지 못했습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="416"/>
        <source>캡쳐 확장자 변경 : %1</source>
        <translation>캡쳐 확장자 변경 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="419"/>
        <source>캡쳐 저장 경로 변경 : %1</source>
        <translation>캡쳐 저장 경로 변경 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="450"/>
        <source>수직 동기화 상태를 변경 후 적용 하려면 프로그램을 재 시작 해야 합니다</source>
        <translation>수직 동기화 상태를 변경 후 적용 하려면 프로그램을 재 시작 해야 합니다</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="837"/>
        <source>화면 회전 각도</source>
        <translation>화면 회전 각도</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="849"/>
        <source>90도</source>
        <translation>90도</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="855"/>
        <source>180도</source>
        <translation>180도</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="861"/>
        <source>270도</source>
        <translation>270도</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="953"/>
        <source>체커 보드</source>
        <translation>체커 보드</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="986"/>
        <source>페이지 플리핑</source>
        <translation>페이지 플리핑</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1019"/>
        <source>3D 자막 설정</source>
        <translation>3D 자막 설정</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1103"/>
        <source>인터레이스</source>
        <translation>인터레이스</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1677"/>
        <source>Language</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1802"/>
        <source>재생 순서</source>
        <translation>재생 순서</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1808"/>
        <source>전체 순차 재생</source>
        <translation>전체 순차 재생</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1814"/>
        <source>전체 반복 재생</source>
        <translation>전체 반복 재생</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1832"/>
        <source>무작위 재생</source>
        <translation>무작위 재생</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1846"/>
        <source>재생 스킵</source>
        <translation>재생 스킵</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1881"/>
        <source>구간 반복</source>
        <translation>구간 반복</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="1943"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2109"/>
        <source>정렬</source>
        <translation>정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2116"/>
        <location filename="../src/ui/Screen.cpp" line="2160"/>
        <source>기본 정렬</source>
        <translation>기본 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2123"/>
        <source>자동 정렬</source>
        <translation>자동 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2130"/>
        <source>왼쪽 정렬</source>
        <translation>왼쪽 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2137"/>
        <source>가운데 정렬</source>
        <translation>가운데 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2144"/>
        <source>오른쪽 정렬</source>
        <translation>오른쪽 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2167"/>
        <source>상단 정렬</source>
        <translation>상단 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2174"/>
        <source>하단 정렬</source>
        <translation>하단 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2227"/>
        <source>위치</source>
        <translation>위치</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2261"/>
        <source>자막 / 가사</source>
        <translation>자막 / 가사</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2388"/>
        <source>인코딩 사용</source>
        <translation>인코딩 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2406"/>
        <source>DTS</source>
        <translation>DTS</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2421"/>
        <source>DTV 열기</source>
        <translation>DTV 열기</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2485"/>
        <source>S/PDIF 설정</source>
        <translation>S/PDIF 설정</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2509"/>
        <source>기본 속도</source>
        <translation>기본 속도</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2529"/>
        <source>소리 효과</source>
        <translation>소리 효과</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2558"/>
        <source>음성</source>
        <translation>음성</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2451"/>
        <location filename="../src/ui/Screen.cpp" line="2601"/>
        <source>소리 출력 장치</source>
        <translation>소리 출력 장치</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2457"/>
        <location filename="../src/ui/Screen.cpp" line="2607"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
    <message>
        <location filename="../src/ui/Screen.cpp" line="2634"/>
        <source>소리</source>
        <translation>소리</translation>
    </message>
</context>
<context>
    <name>ScreenExplorer</name>
    <message>
        <location filename="../forms/screenexplorer.ui" line="14"/>
        <source>장면 탐색</source>
        <translation>장면 탐색</translation>
    </message>
    <message>
        <location filename="../forms/screenexplorer.ui" line="20"/>
        <source>설정</source>
        <translation>설정</translation>
    </message>
    <message>
        <location filename="../forms/screenexplorer.ui" line="39"/>
        <source>간격 :</source>
        <translation>간격 :</translation>
    </message>
    <message>
        <location filename="../forms/screenexplorer.ui" line="87"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>ScreenExplorerItem</name>
    <message>
        <location filename="../forms/screenexploreritem.ui" line="17"/>
        <source>screen</source>
        <translation>screen</translation>
    </message>
    <message>
        <location filename="../forms/screenexploreritem.ui" line="27"/>
        <source>time</source>
        <translation>time</translation>
    </message>
</context>
<context>
    <name>ServerSetting</name>
    <message>
        <location filename="../forms/serversetting.ui" line="20"/>
        <source>서버 설정</source>
        <translation>서버 설정</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="32"/>
        <source>설정</source>
        <translation>설정</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="38"/>
        <source>주소</source>
        <translation>주소</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="48"/>
        <source>커맨드 포트</source>
        <translation>커맨드 포트</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="55"/>
        <source>스트림 포트</source>
        <translation>스트림 포트</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="128"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../forms/serversetting.ui" line="144"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>Shader</name>
    <message>
        <location filename="../forms/shader.ui" line="25"/>
        <source>셰이더</source>
        <translation>셰이더</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="31"/>
        <source>목록</source>
        <translation>목록</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="80"/>
        <source>목록에 등록 된 순서대로 적용 됩니다</source>
        <translation>목록에 등록 된 순서대로 적용 됩니다</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="104"/>
        <source>추가</source>
        <translation>추가</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="120"/>
        <source>선택 삭제</source>
        <translation>선택 삭제</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="136"/>
        <source>모두 삭제</source>
        <translation>모두 삭제</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="165"/>
        <source>선택 위로</source>
        <translation>선택 위로</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="181"/>
        <source>선택 아래로</source>
        <translation>선택 아래로</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="210"/>
        <source>적용</source>
        <translation>적용</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="226"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../forms/shader.ui" line="243"/>
        <source>적용 결과</source>
        <translation>적용 결과</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="111"/>
        <source>삭제</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="118"/>
        <source>맨 위로</source>
        <translation>맨 위로</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="123"/>
        <source>맨 아래로</source>
        <translation>맨 아래로</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="130"/>
        <source>위로</source>
        <translation>위로</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="135"/>
        <source>아래로</source>
        <translation>아래로</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="318"/>
        <source>GLSL (*.glsl);;텍스트 (*.txt);;모든 파일 (*.*)</source>
        <translation>GLSL (*.glsl);;텍스트 (*.txt);;모든 파일 (*.*)</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="341"/>
        <source>성공</source>
        <translation>성공</translation>
    </message>
    <message>
        <location filename="../src/ui/Shader.cpp" line="349"/>
        <source>실패</source>
        <translation>실패</translation>
    </message>
</context>
<context>
    <name>SkipRange</name>
    <message>
        <location filename="../forms/skiprange.ui" line="19"/>
        <source>재생 스킵 설정</source>
        <translation>재생 스킵 설정</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="25"/>
        <source>설정</source>
        <translation>설정</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="33"/>
        <location filename="../forms/skiprange.ui" line="47"/>
        <source>건너 뛰기</source>
        <translation>건너 뛰기</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="40"/>
        <source>오프닝 (앞에서)</source>
        <translation>오프닝 (앞에서)</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="67"/>
        <source>엔딩 (뒤에서)</source>
        <translation>엔딩 (뒤에서)</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="93"/>
        <source>삭제</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="123"/>
        <source>구간</source>
        <translation>구간</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="139"/>
        <location filename="../src/ui/SkipRange.cpp" line="88"/>
        <source>시작</source>
        <translation>시작</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="169"/>
        <location filename="../src/ui/SkipRange.cpp" line="90"/>
        <source>범위</source>
        <translation>범위</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="197"/>
        <source>추가</source>
        <translation>추가</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="212"/>
        <location filename="../src/ui/SkipRange.cpp" line="89"/>
        <source>길이</source>
        <translation>길이</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="234"/>
        <source>초</source>
        <translation>초</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="288"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../forms/skiprange.ui" line="304"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../src/ui/SkipRange.cpp" line="91"/>
        <source>사용</source>
        <translation>사용</translation>
    </message>
    <message>
        <location filename="../src/ui/SkipRange.cpp" line="120"/>
        <source>%1초</source>
        <translation>%1초</translation>
    </message>
    <message>
        <location filename="../src/ui/SkipRange.cpp" line="232"/>
        <source>이미 해당 범위가 존재합니다</source>
        <translation>이미 해당 범위가 존재합니다</translation>
    </message>
</context>
<context>
    <name>Socket</name>
    <message>
        <location filename="../src/net/Socket.cpp" line="296"/>
        <source>접속이 끊겼습니다</source>
        <translation>접속이 끊겼습니다</translation>
    </message>
</context>
<context>
    <name>SubtitleDirectory</name>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="14"/>
        <source>자막 / 가사 검색 디렉토리</source>
        <translation>자막 / 가사 검색 디렉토리</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="20"/>
        <source>목록</source>
        <translation>목록</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="69"/>
        <source>등록된 디렉토리에서도 자막 / 가사를 검색합니다</source>
        <translation>등록된 디렉토리에서도 자막 / 가사를 검색합니다</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="93"/>
        <source>추가</source>
        <translation>추가</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="109"/>
        <source>선택 삭제</source>
        <translation>선택 삭제</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="125"/>
        <source>모두 삭제</source>
        <translation>모두 삭제</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="154"/>
        <source>선택 위로</source>
        <translation>선택 위로</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="170"/>
        <source>선택 아래로</source>
        <translation>선택 아래로</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="205"/>
        <source>등록된 디렉토리 우선 검색</source>
        <translation>등록된 디렉토리 우선 검색</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="231"/>
        <source>적용</source>
        <translation>적용</translation>
    </message>
    <message>
        <location filename="../forms/subtitledirectory.ui" line="247"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
    <message>
        <location filename="../src/ui/SubtitleDirectory.cpp" line="118"/>
        <source>삭제</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../src/ui/SubtitleDirectory.cpp" line="125"/>
        <source>맨 위로</source>
        <translation>맨 위로</translation>
    </message>
    <message>
        <location filename="../src/ui/SubtitleDirectory.cpp" line="130"/>
        <source>맨 아래로</source>
        <translation>맨 아래로</translation>
    </message>
    <message>
        <location filename="../src/ui/SubtitleDirectory.cpp" line="137"/>
        <source>위로</source>
        <translation>위로</translation>
    </message>
    <message>
        <location filename="../src/ui/SubtitleDirectory.cpp" line="142"/>
        <source>아래로</source>
        <translation>아래로</translation>
    </message>
</context>
<context>
    <name>TextEncodeSetting</name>
    <message>
        <location filename="../forms/textencodesetting.ui" line="19"/>
        <source>인코딩 설정</source>
        <translation>인코딩 설정</translation>
    </message>
    <message>
        <location filename="../forms/textencodesetting.ui" line="25"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../forms/textencodesetting.ui" line="74"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../forms/textencodesetting.ui" line="90"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>Trackbar</name>
    <message>
        <location filename="../src/ui/Trackbar.cpp" line="86"/>
        <source>재생 위치</source>
        <translation>재생 위치</translation>
    </message>
</context>
<context>
    <name>UserAspectRatio</name>
    <message>
        <location filename="../forms/useraspectratio.ui" line="14"/>
        <source>화면 비율 사용자 지정</source>
        <translation>화면 비율 사용자 지정</translation>
    </message>
    <message>
        <location filename="../forms/useraspectratio.ui" line="30"/>
        <source>넓이</source>
        <translation>넓이</translation>
    </message>
    <message>
        <location filename="../forms/useraspectratio.ui" line="70"/>
        <source>높이</source>
        <translation>높이</translation>
    </message>
    <message>
        <location filename="../forms/useraspectratio.ui" line="115"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../forms/useraspectratio.ui" line="144"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../src/core/Utils.cpp" line="254"/>
        <source>오류</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../src/core/Utils.cpp" line="259"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../src/core/Utils.cpp" line="264"/>
        <source>질문</source>
        <translation>질문</translation>
    </message>
    <message>
        <location filename="../src/core/Utils.cpp" line="604"/>
        <source>%1로 열기</source>
        <translation>%1로 열기</translation>
    </message>
</context>
<context>
    <name>ViewEPG</name>
    <message>
        <location filename="../forms/viewepg.ui" line="14"/>
        <source>채널 편성표</source>
        <translation>채널 편성표</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="20"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="26"/>
        <source>채널</source>
        <translation>채널</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="40"/>
        <source>제목</source>
        <translation>제목</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="54"/>
        <source>방송 시간</source>
        <translation>방송 시간</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="80"/>
        <source>현재 시각 :</source>
        <translation>현재 시각 :</translation>
    </message>
    <message>
        <location filename="../forms/viewepg.ui" line="113"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../src/ui/ViewEPG.cpp" line="41"/>
        <location filename="../src/ui/ViewEPG.cpp" line="42"/>
        <location filename="../src/ui/ViewEPG.cpp" line="43"/>
        <source>업데이트 중...</source>
        <translation>업데이트 중...</translation>
    </message>
</context>
</TS>

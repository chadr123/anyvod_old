﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "SPDIFInterface.h"

#include <QVector>

#include <AudioToolBox/AudioToolbox.h>

class SPDIFCoreAudio : public SPDIFInterface
{
public:
    SPDIFCoreAudio();
    virtual ~SPDIFCoreAudio();

    virtual bool open();
    virtual void close();

    virtual bool play();
    virtual bool pause();
    virtual bool resume();
    virtual bool stop();

    virtual bool fillBuffer(void *buffer, int size);
    virtual unsigned int remainBufferSize();

    virtual unsigned int getActualBufferSize() const;

    virtual void getDeviceList(QStringList *ret);
    virtual int getDeviceCount() const;

    virtual double getLatency();
    virtual bool isSupportPull() const;
    virtual int getAlignSize() const;

    virtual bool canFillBufferBeforeStart() const;

private:
    struct DEVICE_ITEM
    {
        AudioDeviceID id;
        QString name;
    };

private:
    bool updateDeviceList();

    bool setStreamFormat(AudioStreamID id, const AudioStreamBasicDescription &format);
    bool setAutoHogMode(bool enable);
    bool getAutoHogMode();
    bool setMixing(bool enable);
    bool getMixing();
    bool setHogMode(bool enable);
    bool isSPDIFFormat(const AudioStreamBasicDescription &format) const;
    void resetDevices();
    void resetStream(AudioStreamID id);
    bool getDeviceName(AudioDeviceID id, QString *ret);
    bool getDefaultDevice(AudioDeviceID *ret);
    bool getStreamList(AudioDeviceID id, QVector<AudioStreamID> *ret);
    bool getFormatList(AudioStreamID id, QVector<AudioStreamBasicDescription> *ret);

private:
    static char* UInt32ToFourCC(UInt32* val);
    static OSStatus SPDIFCallback(AudioDeviceID inDevice, const AudioTimeStamp *inNow,
                                        const void *inInputData, const AudioTimeStamp *inInputTime,
                                        AudioBufferList *outOutputData, const AudioTimeStamp *inOutputTime,
                                        void *threadGlobals);

private:
    QVector<DEVICE_ITEM> m_deviceList;
    int m_streamIndex;
    AudioStreamID m_streamID;
    pid_t m_hogPID;
    int m_mixing;
    UInt32 m_bytesPerPacket;
    AudioStreamBasicDescription m_oriFormat;
    AudioDeviceID m_deviceID;
    AudioDeviceIOProcID m_procID;
};

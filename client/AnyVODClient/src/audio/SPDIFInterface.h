﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "SPDIF.h"

#include <QString>
#include <QCoreApplication>

class SPDIFInterface
{
    Q_DECLARE_TR_FUNCTIONS(SPDIFInterface)
public:
    SPDIFInterface();
    virtual ~SPDIFInterface();

    virtual bool open() = 0;
    virtual void close() = 0;

    virtual bool play() = 0;
    virtual bool pause() = 0;
    virtual bool resume() = 0;
    virtual bool stop() = 0;

    virtual bool fillBuffer(void *buffer, int size) = 0;
    virtual unsigned int remainBufferSize() = 0;

    virtual unsigned int getActualBufferSize() const = 0;

    virtual void getDeviceList(QStringList *ret) = 0;
    virtual int getDeviceCount() const = 0;

    virtual double getLatency() = 0;
    virtual bool isSupportPull() const = 0;
    virtual int getAlignSize() const = 0;

    virtual bool canFillBufferBeforeStart() const = 0;

    void setParams(int sampleRate, int channelCount);
    int getSampleRate() const;
    int getChannelCount() const;

    void setBufferLength(unsigned int msec);
    unsigned int getBufferLength() const;

    bool setDevice(int device);
    int getDevice() const;

    bool getDeviceName(int device, QString *ret);
    void setCallback(SPDIF::Callback callback, void *user);
    void getErrorString(QString *ret) const;
    unsigned int getBytesPerSecond() const;

public:
    static const double PUSH_MULTIPLIER;

protected:
    int m_sampleRate;
    int m_channelCount;
    int m_bytesPerSample;
    QString m_errorString;
    unsigned int m_bufferLength;
    int m_device;
    SPDIF::Callback m_callback;
    void *m_user;
};

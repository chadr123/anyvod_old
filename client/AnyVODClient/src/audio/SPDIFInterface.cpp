﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "SPDIFInterface.h"

#include <QObject>
#include <QStringList>

const double SPDIFInterface::PUSH_MULTIPLIER = 1.1;

SPDIFInterface::SPDIFInterface() :
    m_sampleRate(0),
    m_channelCount(0),
    m_bytesPerSample(0),
    m_bufferLength(50),
    m_device(-1),
    m_callback(NULL),
    m_user(NULL)
{

}

SPDIFInterface::~SPDIFInterface()
{

}

void SPDIFInterface::setParams(int sampleRate, int channelCount)
{
    this->m_sampleRate = sampleRate;
    this->m_channelCount = channelCount;
}

int SPDIFInterface::getSampleRate() const
{
    return this->m_sampleRate;
}

int SPDIFInterface::getChannelCount() const
{
    return this->m_channelCount;
}

void SPDIFInterface::setBufferLength(unsigned int msec)
{
    this->m_bufferLength = msec;
}

unsigned int SPDIFInterface::getBufferLength() const
{
    return this->m_bufferLength;
}

bool SPDIFInterface::setDevice(int device)
{
    if (device >= -1 && device < this->getDeviceCount())
    {
        this->m_device = device;
        return true;
    }
    else
    {
        this->m_device = -1;
        return false;
    }
}

int SPDIFInterface::getDevice() const
{
    return this->m_device;
}

bool SPDIFInterface::getDeviceName(int device, QString *ret)
{
    if (device == -1)
    {
        *ret = trUtf8("기본 장치");
        return true;
    }

    if (device >= 0 && device < this->getDeviceCount())
    {
        QStringList list;

        this->getDeviceList(&list);

        *ret = list[device];

        return true;
    }

    return false;
}

void SPDIFInterface::setCallback(SPDIF::Callback callback, void *user)
{
    this->m_callback = callback;
    this->m_user = user;
}

void SPDIFInterface::getErrorString(QString *ret) const
{
    *ret = this->m_errorString;
}

unsigned int SPDIFInterface::getBytesPerSecond() const
{
    return this->m_sampleRate * this->m_bytesPerSample;
}

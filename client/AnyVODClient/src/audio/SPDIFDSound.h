﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "SPDIFInterface.h"

#include <QVector>

#include <dsound.h>
#include <winnt.h>

class SPDIFDSound : public SPDIFInterface
{
public:
    SPDIFDSound();
    virtual ~SPDIFDSound();

    virtual bool open();
    virtual void close();

    virtual bool play();
    virtual bool pause();
    virtual bool resume();
    virtual bool stop();

    virtual bool fillBuffer(void *buffer, int size);
    virtual unsigned int remainBufferSize();

    virtual unsigned int getActualBufferSize() const;

    virtual void getDeviceList(QStringList *ret);
    virtual int getDeviceCount() const;

    virtual double getLatency();
    virtual bool isSupportPull() const;
    virtual int getAlignSize() const;

    virtual bool canFillBufferBeforeStart() const;

private:
    struct DEVICE_ITEM
    {
        GUID guid;
        QString name;
    };

private:
    bool createDSound();
    void deleteDSound();

    bool createDSoundBuffer();
    void deleteDSoundBuffer();

    bool updateDeviceList();

    void errorToString(const HRESULT error);

private:
    static BOOL CALLBACK deviceEnumCallback(LPGUID guid, LPCWSTR desc, LPCWSTR module, LPVOID context);

private:
    LPDIRECTSOUND m_dsound;
    LPDIRECTSOUNDBUFFER m_dsoundBuffer;
    DWORD m_dsoundBufferSize;
    DWORD m_curBufferPos;
    QVector<DEVICE_ITEM> m_deviceList;
};

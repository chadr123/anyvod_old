﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include <QGlobalStatic>

#include "SPDIF.h"
#include "SPDIFInterface.h"

#ifdef Q_OS_WIN
#include "SPDIFDSound.h"
#elif defined Q_OS_MAC && !defined Q_OS_IOS
#include "SPDIFCoreAudio.h"
#elif defined Q_OS_LINUX && !defined Q_OS_ANDROID
#include "SPDIFAlsa.h"
#endif

#include <QDebug>

SPDIF::SPDIF() :
    m_output(NULL),
    m_callback(NULL),
    m_isOpened(false),
    m_interval(30),
    m_quit(false),
    m_user(NULL),
    m_codecID(AV_CODEC_ID_NONE)
{
#ifdef Q_OS_WIN
    this->m_output = new SPDIFDSound;
#elif defined Q_OS_MAC && !defined Q_OS_IOS
    this->m_output = new SPDIFCoreAudio;
#elif defined Q_OS_LINUX && !defined Q_OS_ANDROID
    this->m_output = new SPDIFAlsa;
#endif
}

SPDIF::~SPDIF()
{
    if (this->m_output)
    {
        this->close();

        delete this->m_output;
        this->m_output = NULL;
    }
}

bool SPDIF::open(AVCodecID codecID, Callback callback, int sampleRate, int channelCount, unsigned int bufferLen, void *user)
{
    if (this->m_isOpened)
        return false;

    if (this->m_output && callback)
    {
        this->m_output->setParams(sampleRate, channelCount);
        this->m_output->setBufferLength(bufferLen);

        if (!this->m_spdifEncoder.open(codecID))
        {
            this->close();
            return false;
        }

        if (!this->m_output->open())
        {
            this->close();
            return false;
        }

        if (this->m_output->isSupportPull())
            this->m_output->setCallback(callback, user);
        else
            this->m_quit = false;

        this->m_isOpened = true;
        this->m_callback = callback;
        this->m_user = user;
        this->m_codecID = codecID;

        return true;
    }

    return false;
}

void SPDIF::close()
{
    this->stop();

    if (this->m_output)
    {
        this->m_output->close();
        this->m_spdifEncoder.close();
    }

    this->m_codecID = AV_CODEC_ID_NONE;
    this->m_isOpened = false;
    this->m_callback = NULL;
}

bool SPDIF::isOpened() const
{
    return this->m_isOpened;
}

bool SPDIF::isAvailable() const
{
    return this->m_output != NULL;
}

bool SPDIF::play()
{
    if (this->m_output)
    {
        if (!this->m_output->isSupportPull())
        {
            if (this->m_output->canFillBufferBeforeStart())
            {
                const int maxCount = 20;

                for (int i = 0; i < maxCount; i++)
                {
                    if (this->requestFillBuffer())
                        break;
                }
            }

            this->m_quit = false;
        }

        if (this->m_output->play())
        {
            if (!this->m_output->isSupportPull())
                this->start();

            return true;
        }
    }

    return false;
}

bool SPDIF::pause()
{
    if (this->m_output)
        return this->m_output->pause();

    return false;
}

bool SPDIF::resume()
{
    if (this->m_output)
        return this->m_output->resume();

    return false;
}

bool SPDIF::stop()
{
    if (this->m_output)
    {
        if (!this->m_output->isSupportPull())
        {
            this->m_quit = true;

            if (this->isRunning())
                this->wait();
        }

        return this->m_output->stop();
    }

    return false;
}

void SPDIF::setInterval(unsigned int msec)
{
    this->m_interval = msec;
}

unsigned int SPDIF::getInterval() const
{
    return this->m_interval;
}

bool SPDIF::setBufferLength(unsigned int msec)
{
    if (this->m_output)
    {
        if (this->m_isOpened)
        {
            this->close();

            return this->open(this->m_codecID, this->m_callback,
                              this->m_output->getSampleRate(), this->m_output->getChannelCount(),
                              msec, this->m_user);
        }

        return true;
    }

    return false;
}

unsigned int SPDIF::getBufferLength() const
{
    if (this->m_output)
        return this->m_output->getBufferLength();
    else
        return 0;
}

void SPDIF::getErrorString(QString *ret) const
{
    if (this->m_output)
        this->m_output->getErrorString(ret);
}

void SPDIF::getParams(const AVCodecContext *context, int *sampleRate, int *channelCount, AVSampleFormat *format) const
{
    this->m_spdifEncoder.getParams(context, sampleRate, channelCount, format);
}

void SPDIF::setAudioBuffer(uint8_t *buf, int size)
{
    this->m_spdifEncoder.setAudioBuffer(buf, size);
}

void SPDIF::setHDRate(int rate)
{
    this->m_spdifEncoder.setHDRate(rate);
}

int SPDIF::writePacket(AVPacket &packet)
{
    return this->m_spdifEncoder.writePacket(packet);
}

int SPDIF::getFailCount() const
{
    return this->m_spdifEncoder.getFailCount();
}

void SPDIF::getDeviceList(QStringList *ret)
{
    if (this->m_output)
        this->m_output->getDeviceList(ret);
}

int SPDIF::getDeviceCount() const
{
    if (this->m_output)
        return this->m_output->getDeviceCount();

    return 0;
}

bool SPDIF::setDevice(int device)
{
    if (this->m_output)
        return this->m_output->setDevice(device);

    return false;
}

int SPDIF::getDevice() const
{
    if (this->m_output)
        return this->m_output->getDevice();

    return -1;
}

bool SPDIF::getDeviceName(int device, QString *ret)
{
    if (this->m_output)
        return this->m_output->getDeviceName(device, ret);

    return false;
}

double SPDIF::getLatency()
{
    if (this->m_output)
    {
        double latency = this->m_output->getLatency();

        if (!this->m_output->isSupportPull())
            latency += this->m_output->getBufferLength() / 1000.0;

        return latency;
    }

    return 0.0;
}

bool SPDIF::requestFillBuffer()
{
    int size = this->alignSize(this->m_output->getBytesPerSecond() * (this->m_interval / 1000.0) * SPDIFInterface::PUSH_MULTIPLIER);
    uint8_t buffer[size];
    int wrote = this->m_callback(buffer, size, this->m_user);

    if (wrote > 0)
        return this->m_output->fillBuffer(buffer, wrote);

    return false;
}

int SPDIF::alignSize(int size) const
{
    if (this->m_output)
    {
        int align = this->m_output->getAlignSize();

        if (align)
        {
            align--;
            return (size + align) & ~align;
        }
    }

    return size;
}

void SPDIF::run()
{
    while (!this->m_quit)
    {
        this->requestFillBuffer();

        QThread::msleep(this->m_interval);
    }
}

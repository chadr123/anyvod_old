﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "SPDIFDSound.h"
#include "core/Utils.h"
#include "ui/MainWindow.h"

#include <QDebug>

#include <mmreg.h>
#include <ksmedia.h>

extern "C"
{
    #include <libavutil/time.h>
}

static const GUID _KSDATAFORMAT_SUBTYPE_DOLBY_AC3_SPDIF = {WAVE_FORMAT_DOLBY_AC3_SPDIF, 0x0000, 0x0010, {0x80, 0x00, 0x00, 0xaa, 0x00, 0x38, 0x9b, 0x71}};

SPDIFDSound::SPDIFDSound() :
    m_dsound(NULL),
    m_dsoundBuffer(NULL),
    m_dsoundBufferSize(0),
    m_curBufferPos(0)
{

}

SPDIFDSound::~SPDIFDSound()
{
    this->close();
}

bool SPDIFDSound::createDSound()
{
    GUID *guid = NULL;

    if (this->m_device != -1)
        guid = &this->m_deviceList[this->m_device].guid;

    if (DirectSoundCreate(guid, &this->m_dsound, NULL) != DS_OK)
        return false;

    if (IDirectSound_SetCooperativeLevel(this->m_dsound, (HWND)Utils::getMainWindow()->winId(), DSSCL_EXCLUSIVE) != DS_OK)
        return false;

    return true;
}

void SPDIFDSound::deleteDSound()
{
    if (this->m_dsound)
    {
        IDirectSound_Release(this->m_dsound);
        this->m_dsound = NULL;
    }
}

bool SPDIFDSound::createDSoundBuffer()
{
    WAVEFORMATEXTENSIBLE waveformat;
    DSBUFFERDESC dsbdesc;

    waveformat.dwChannelMask = SPEAKER_FRONT_LEFT | SPEAKER_FRONT_RIGHT;
    waveformat.Format.wBitsPerSample = 16;
    waveformat.Samples.wValidBitsPerSample = waveformat.Format.wBitsPerSample;
    waveformat.Format.wFormatTag = WAVE_FORMAT_DOLBY_AC3_SPDIF;
    waveformat.SubFormat = _KSDATAFORMAT_SUBTYPE_DOLBY_AC3_SPDIF;

    waveformat.Format.nChannels = this->m_channelCount;
    waveformat.Format.nSamplesPerSec = this->m_sampleRate;
    waveformat.Format.nBlockAlign = waveformat.Format.wBitsPerSample / 8 * this->m_channelCount;
    waveformat.Format.nAvgBytesPerSec = waveformat.Format.nSamplesPerSec * waveformat.Format.nBlockAlign;

    this->m_bytesPerSample = waveformat.Format.nBlockAlign;

    memset(&dsbdesc, 0, sizeof(DSBUFFERDESC));

    dsbdesc.dwSize = sizeof(DSBUFFERDESC);
    dsbdesc.dwFlags = DSBCAPS_GETCURRENTPOSITION2 | DSBCAPS_GLOBALFOCUS | DSBCAPS_CTRLPOSITIONNOTIFY;

    if (this->m_channelCount <= 2)
    {
        waveformat.Format.cbSize = 0;
    }
    else
    {
        waveformat.Format.wFormatTag = WAVE_FORMAT_EXTENSIBLE;
        waveformat.Format.cbSize = sizeof(WAVEFORMATEXTENSIBLE) - sizeof(WAVEFORMATEX);

        dsbdesc.dwFlags |= DSBCAPS_LOCHARDWARE;
    }

    dsbdesc.dwBufferBytes = this->m_bufferLength * waveformat.Format.nBlockAlign * this->m_sampleRate / 1000;
    dsbdesc.lpwfxFormat = (WAVEFORMATEX*)&waveformat;

    this->m_dsoundBufferSize = dsbdesc.dwBufferBytes;
    this->m_curBufferPos = 0;

    HRESULT hr;

    if ((hr = IDirectSound_CreateSoundBuffer(this->m_dsound, &dsbdesc, &this->m_dsoundBuffer, NULL)) != DS_OK)
    {
        this->errorToString(hr);

        if (dsbdesc.dwFlags & DSBCAPS_LOCHARDWARE)
        {
            dsbdesc.dwFlags &= ~DSBCAPS_LOCHARDWARE;

            if (IDirectSound_CreateSoundBuffer(this->m_dsound, &dsbdesc, &this->m_dsoundBuffer, NULL) != DS_OK)
                return false;
        }
        else
        {
            return false;
        }
    }

    return true;
}

void SPDIFDSound::deleteDSoundBuffer()
{
    if (this->m_dsoundBuffer)
    {
        IDirectSoundBuffer_Release(this->m_dsoundBuffer);
        this->m_dsoundBuffer = NULL;
    }
}

bool SPDIFDSound::updateDeviceList()
{
    this->m_deviceList.clear();

    return DirectSoundEnumerateW(SPDIFDSound::deviceEnumCallback, this) == DS_OK;
}

void SPDIFDSound::errorToString(const HRESULT error)
{
    switch (error)
    {
        case DSERR_ALLOCATED:
        {
            this->m_errorString = "DSERR_ALLOCATED";
            break;
        }
        case DSERR_BADFORMAT:
        {
            this->m_errorString = "DSERR_BADFORMAT";
            break;
        }
        case DSERR_BUFFERTOOSMALL:
        {
            this->m_errorString = "DSERR_BUFFERTOOSMALL";
            break;
        }
        case DSERR_CONTROLUNAVAIL:
        {
            this->m_errorString = "DSERR_CONTROLUNAVAIL";
            break;
        }
        case DSERR_DS8_REQUIRED:
        {
            this->m_errorString = "DSERR_DS8_REQUIRED";
            break;
        }
        case DSERR_INVALIDCALL:
        {
            this->m_errorString = "DSERR_INVALIDCALL";
            break;
        }
        case DSERR_INVALIDPARAM:
        {
            this->m_errorString = "DSERR_INVALIDPARAM";
            break;
        }
        case DSERR_NOAGGREGATION:
        {
            this->m_errorString = "DSERR_NOAGGREGATION";
            break;
        }
        case DSERR_OUTOFMEMORY:
        {
            this->m_errorString = "DSERR_OUTOFMEMORY";
            break;
        }
        case DSERR_UNINITIALIZED:
        {
            this->m_errorString = "DSERR_UNINITIALIZED";
            break;
        }
        case DSERR_UNSUPPORTED:
        {
            this->m_errorString = "DSERR_UNSUPPORTED";
            break;
        }
        case DS_OK:
        {
            this->m_errorString = "DS_OK";
            break;
        }
        default:
        {
            this->m_errorString = "unknown";
            break;
        }
    }
}

BOOL SPDIFDSound::deviceEnumCallback(LPGUID guid, LPCWSTR desc, LPCWSTR module, LPVOID context)
{
    (void)module;

    if (guid)
    {
        DEVICE_ITEM item;
        SPDIFDSound *parent = (SPDIFDSound*)context;

        item.guid = *guid;
        item.name = QString::fromWCharArray(desc);

        parent->m_deviceList.append(item);
    }

    return TRUE;
}

bool SPDIFDSound::open()
{
    if (!this->updateDeviceList())
    {
        this->close();
        return false;
    }

    if (!this->createDSound())
    {
        this->close();
        return false;
    }

    if (!this->createDSoundBuffer())
    {
        this->close();
        return false;
    }

    return true;
}

void SPDIFDSound::close()
{
    this->deleteDSoundBuffer();
    this->deleteDSound();
}

bool SPDIFDSound::play()
{
    if (!this->m_dsoundBuffer)
        return false;

    HRESULT hr = IDirectSoundBuffer_Play(this->m_dsoundBuffer, 0, 0, DSBPLAY_LOOPING);

    if (hr == DSERR_BUFFERLOST)
    {
        IDirectSoundBuffer_Restore(this->m_dsoundBuffer);
        hr = IDirectSoundBuffer_Play(this->m_dsoundBuffer, 0, 0, DSBPLAY_LOOPING);
    }

    return hr == DS_OK;
}

bool SPDIFDSound::pause()
{
    if (this->m_dsoundBuffer && IDirectSoundBuffer_Stop(this->m_dsoundBuffer) == DS_OK)
        return true;
    else
        return false;
}

bool SPDIFDSound::resume()
{
    if (this->m_dsoundBuffer && IDirectSoundBuffer_Play(this->m_dsoundBuffer, 0, 0, DSBPLAY_LOOPING) == DS_OK)
        return true;
    else
        return false;
}

bool SPDIFDSound::stop()
{
    return this->pause();
}

bool SPDIFDSound::fillBuffer(void *buffer, int size)
{
    void *writeBuffer = NULL;
    void *wrapAroundBuffer = NULL;
    DWORD beforeWrapSize = 0;
    DWORD afterWrapSize = 0;
    DWORD playPos = 0;
    DWORD writePos = 0;

    if (!this->m_dsoundBuffer)
        return false;

    while (true)
    {
        if (IDirectSoundBuffer_GetCurrentPosition(this->m_dsoundBuffer, &playPos, &writePos) != DS_OK)
            return false;

        while ((this->m_curBufferPos < writePos &&
                ((this->m_curBufferPos >= playPos && writePos >= playPos) ||
                 (this->m_curBufferPos < playPos && writePos < playPos))) ||
               (this->m_curBufferPos >= playPos && writePos < playPos))
        {
            this->m_curBufferPos += size;

            while (this->m_curBufferPos >= this->m_dsoundBufferSize)
                this->m_curBufferPos -= this->m_dsoundBufferSize;
        }

        if ((this->m_curBufferPos < playPos  && this->m_curBufferPos + size >= playPos) ||
                (this->m_curBufferPos >= playPos &&
                 this->m_curBufferPos + size >= playPos + this->m_dsoundBufferSize))
        {
            av_usleep(10000);
            continue;
        }

        break;
    }

    if (IDirectSoundBuffer_Lock(this->m_dsoundBuffer, this->m_curBufferPos, size,
                                &writeBuffer, &beforeWrapSize, &wrapAroundBuffer, &afterWrapSize, 0) == DSERR_BUFFERLOST)
    {
        IDirectSoundBuffer_Restore(this->m_dsoundBuffer);

        if (IDirectSoundBuffer_Lock(this->m_dsoundBuffer, this->m_curBufferPos, size,
                                    &writeBuffer, &beforeWrapSize, &wrapAroundBuffer, &afterWrapSize, 0) != DS_OK)
        {
            return false;
        }
    }

    memcpy(writeBuffer, buffer, beforeWrapSize);

    if (wrapAroundBuffer)
        memcpy(wrapAroundBuffer, ((uint8_t*)buffer) + beforeWrapSize, afterWrapSize);

    this->m_curBufferPos += beforeWrapSize + afterWrapSize;

    while (this->m_curBufferPos >= this->m_dsoundBufferSize)
        this->m_curBufferPos -= this->m_dsoundBufferSize;

    IDirectSoundBuffer_Unlock(this->m_dsoundBuffer, writeBuffer, beforeWrapSize, wrapAroundBuffer, afterWrapSize);

    return true;
}

unsigned int SPDIFDSound::remainBufferSize()
{
    DWORD playPos = 0;
    int size = 0;

    if (IDirectSoundBuffer_GetCurrentPosition(this->m_dsoundBuffer, &playPos, NULL) != DS_OK)
        return 0;

    if ((this->m_curBufferPos + 1) % this->m_dsoundBufferSize == playPos)
        return 0;

    if (playPos == this->m_curBufferPos)
        return this->m_dsoundBufferSize;

    if (playPos < this->m_curBufferPos)
        size = (this->m_dsoundBufferSize-1) - this->m_curBufferPos + playPos;
    else
        size = playPos - this->m_curBufferPos - 1;

    return (unsigned int)size;
}

unsigned int SPDIFDSound::getActualBufferSize() const
{
    return (unsigned int)this->m_dsoundBufferSize;
}

void SPDIFDSound::getDeviceList(QStringList *ret)
{
    QStringList list;

    this->updateDeviceList();

    foreach (const DEVICE_ITEM &item, this->m_deviceList)
        list.append(item.name);

    *ret = list;
}

int SPDIFDSound::getDeviceCount() const
{
    return this->m_deviceList.count();
}

double SPDIFDSound::getLatency()
{
    return 0.0;
}

bool SPDIFDSound::isSupportPull() const
{
    return false;
}

int SPDIFDSound::getAlignSize() const
{
    return 0;
}

bool SPDIFDSound::canFillBufferBeforeStart() const
{
    return true;
}

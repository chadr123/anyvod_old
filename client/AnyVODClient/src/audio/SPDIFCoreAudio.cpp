﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "SPDIFCoreAudio.h"

#include <QStringList>
#include <QDebug>

#include <CoreAudio/CoreAudio.h>
#include <CoreServices/CoreServices.h>
#include <AudioUnit/AudioUnit.h>

#define OSS_STATUS(x) SPDIFCoreAudio::UInt32ToFourCC((UInt32*)&x)

SPDIFCoreAudio::SPDIFCoreAudio() :
    m_streamIndex(-1),
    m_streamID(0),
    m_hogPID(-1),
    m_mixing(-1),
    m_bytesPerPacket(0),
    m_deviceID(0),
    m_procID(0)
{
    memset(&this->m_oriFormat, 0, sizeof(this->m_oriFormat));
}

SPDIFCoreAudio::~SPDIFCoreAudio()
{
    this->close();
}

bool SPDIFCoreAudio::open()
{
    QVector<AudioStreamID> streams;
    AudioStreamBasicDescription outputFormat;

    memset(&outputFormat, 0, sizeof(outputFormat));

    this->updateDeviceList();
    this->resetDevices();

    if (this->m_device == -1)
    {
        if (!this->getDefaultDevice(&this->m_deviceID))
        {
            this->close();
            return false;
        }
    }
    else
    {
        this->m_deviceID = this->m_deviceList[this->m_device].id;
    }

    if (!this->getStreamList(this->m_deviceID, &streams))
    {
        this->close();
        return false;
    }

    for (int i = 0; i < streams.count(); i++)
    {
        QVector<AudioStreamBasicDescription> formats;
        AudioStreamID stream = streams[i];

        if (!this->getFormatList(stream, &formats))
            continue;

        foreach (AudioStreamBasicDescription format, formats)
        {
            if (this->isSPDIFFormat(format) && format.mSampleRate == this->m_sampleRate)
            {
                this->m_streamID = stream;
                this->m_streamIndex = i;
                outputFormat = format;

                break;
            }
        }

        if (outputFormat.mFormatID)
            break;
    }

    if (!outputFormat.mFormatID)
    {
        this->close();
        return false;
    }

    UInt32 paramSize = sizeof(this->m_oriFormat);
    AudioObjectPropertyAddress address =
    {
        kAudioStreamPropertyPhysicalFormat,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    if (AudioObjectGetPropertyData(this->m_streamID, &address, 0, NULL, &paramSize, &this->m_oriFormat) != noErr)
    {
        this->close();
        return false;
    }

    this->setAutoHogMode(false);

    if (!this->getAutoHogMode())
    {
        this->setHogMode(true);
        this->setMixing(false);
    }

    if (!this->setStreamFormat(this->m_streamID, outputFormat))
    {
        this->close();
        return false;
    }

    this->m_bytesPerPacket = outputFormat.mBytesPerPacket;
    this->m_bytesPerSample = outputFormat.mBitsPerChannel * outputFormat.mChannelsPerFrame / 8;

    if (AudioDeviceCreateIOProcID(this->m_deviceID, (AudioDeviceIOProc)SPDIFCallback, this, &this->m_procID) != noErr)
    {
        this->close();
        return false;
    }

    return true;
}

void SPDIFCoreAudio::close()
{
    AudioDeviceDestroyIOProcID(this->m_deviceID, this->m_procID);

    this->setHogMode(false);

    if (this->m_mixing > -1)
        this->setMixing(this->m_mixing ? true : false);

    this->updateDeviceList();
    this->resetDevices();

    this->setStreamFormat(this->m_streamID, this->m_oriFormat);

    AudioHardwareUnload();

    memset(&this->m_oriFormat, 0, sizeof(this->m_oriFormat));
    this->m_streamIndex = -1;
    this->m_streamID = 0;
    this->m_mixing = -1;
    this->m_bytesPerPacket = 0;
    this->m_deviceID = 0;
    this->m_procID = 0;
}

bool SPDIFCoreAudio::play()
{
    return AudioDeviceStart(this->m_deviceID, this->m_procID) == noErr;
}

bool SPDIFCoreAudio::pause()
{
    return AudioDeviceStop(this->m_deviceID, this->m_procID) == noErr;
}

bool SPDIFCoreAudio::resume()
{
    return this->play();
}

bool SPDIFCoreAudio::stop()
{
    return this->pause();
}

bool SPDIFCoreAudio::fillBuffer(void *buffer, int size)
{
    (void)buffer;
    (void)size;

    return false;
}

unsigned int SPDIFCoreAudio::remainBufferSize()
{
    return 0;
}

unsigned int SPDIFCoreAudio::getActualBufferSize() const
{
    return 0;
}

void SPDIFCoreAudio::getDeviceList(QStringList *ret)
{
    QStringList list;

    this->updateDeviceList();

    foreach (const DEVICE_ITEM &item, this->m_deviceList)
        list.append(item.name);

    *ret = list;
}

int SPDIFCoreAudio::getDeviceCount() const
{
    return this->m_deviceList.count();
}

double SPDIFCoreAudio::getLatency()
{
    return 0.0;
}

bool SPDIFCoreAudio::isSupportPull() const
{
    return true;
}

int SPDIFCoreAudio::getAlignSize() const
{
    return 0;
}

bool SPDIFCoreAudio::canFillBufferBeforeStart() const
{
    return false;
}

bool SPDIFCoreAudio::setAutoHogMode(bool enable)
{
    UInt32 value = enable ? 1 : 0;
    AudioObjectPropertyAddress address =
    {
        kAudioHardwarePropertyHogModeIsAllowed,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    return AudioObjectSetPropertyData(this->m_streamID, &address, 0, NULL, sizeof(value), &value) == noErr;
}

bool SPDIFCoreAudio::getAutoHogMode()
{
    UInt32 value = 0;
    UInt32 size = sizeof(value);
    AudioObjectPropertyAddress address =
    {
        kAudioHardwarePropertyHogModeIsAllowed,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    if (AudioObjectGetPropertyData(this->m_streamID, &address, 0, NULL, &size, &value) != noErr)
        return false;

    return value == 1;
}

bool SPDIFCoreAudio::setMixing(bool enable)
{
    int restore = -1;
    UInt32 value = enable ? 1 : 0;
    AudioObjectPropertyAddress address =
    {
        kAudioDevicePropertySupportsMixing,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    if (this->m_mixing == -1)
        restore = this->getMixing() ? 1 : 0;

    if (AudioObjectSetPropertyData(this->m_streamID, &address, 0, NULL, sizeof(value), &value) != noErr)
        return false;

    if (this->m_mixing == -1)
        this->m_mixing = restore;

    return true;
}

bool SPDIFCoreAudio::getMixing()
{
    UInt32 value = 0;
    UInt32 size = sizeof(value);
    AudioObjectPropertyAddress address =
    {
        kAudioDevicePropertySupportsMixing,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    if (AudioObjectGetPropertyData(this->m_streamID, &address, 0, NULL, &size, &value) != noErr)
        return false;

    return value > 0;
}

bool SPDIFCoreAudio::setStreamFormat(AudioStreamID id, const AudioStreamBasicDescription &format)
{
    AudioObjectPropertyAddress address =
    {
        kAudioStreamPropertyPhysicalFormat,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    return AudioObjectSetPropertyData(id, &address, 0, NULL, sizeof(format), &format) == noErr;
}

bool SPDIFCoreAudio::setHogMode(bool enable)
{
    if (!this->m_streamID)
        return false;

    OSStatus err;
    AudioObjectPropertyAddress address =
    {
        kAudioDevicePropertyHogMode,
        kAudioObjectPropertyScopeOutput,
        kAudioObjectPropertyElementMaster
    };

    if (enable)
    {
        if (this->m_hogPID == -1)
        {
            err = AudioObjectSetPropertyData(this->m_streamID, &address, 0, NULL, sizeof(this->m_hogPID), &this->m_hogPID);

            if (err != noErr || this->m_hogPID != getpid())
                return false;
        }
    }
    else
    {
        if (this->m_hogPID > -1)
        {
            pid_t pid = -1;

            err = AudioObjectSetPropertyData(this->m_streamID, &address, 0, NULL, sizeof(pid), &pid);

            if (err != noErr || this->m_hogPID == getpid())
                return false;

            this->m_hogPID = pid;
        }
    }

    return true;
}

bool SPDIFCoreAudio::updateDeviceList()
{
    UInt32 listSize = 0;
    AudioStreamID *list = NULL;
    AudioObjectPropertyAddress address =
    {
        kAudioHardwarePropertyDevices,
        kAudioObjectPropertyScopeOutput,
        kAudioObjectPropertyElementMaster
    };

    if (AudioObjectGetPropertyDataSize(kAudioObjectSystemObject, &address, 0, NULL, &listSize) != noErr)
        return false;

    list = new AudioStreamID[listSize];

    if (AudioObjectGetPropertyData(kAudioObjectSystemObject, &address, 0, NULL, &listSize, list) != noErr)
    {
        delete[] list;
        return false;
    }

    this->m_deviceList.clear();

    for (size_t i = 0; i < listSize / sizeof(AudioStreamID); i++)
    {
        QString name;

        if (this->getDeviceName(list[i], &name))
        {
            DEVICE_ITEM item;

            item.id = list[i];
            item.name = name;

            this->m_deviceList.append(item);
        }
    }

    delete[] list;

    return true;
}

void SPDIFCoreAudio::resetDevices()
{
    this->updateDeviceList();

    foreach (const DEVICE_ITEM &item, this->m_deviceList)
    {
        QVector<AudioStreamID> streams;

        if (!this->getStreamList(item.id, &streams))
            continue;

        foreach (AudioStreamID stream, streams)
            this->resetStream(stream);
    }
}

void SPDIFCoreAudio::resetStream(AudioStreamID id)
{
    AudioStreamBasicDescription format;
    UInt32 paramSize = sizeof(format);
    AudioObjectPropertyAddress address =
    {
        kAudioStreamPropertyPhysicalFormat,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    if (AudioObjectGetPropertyData(id, &address, 0, NULL, &paramSize, &format) != noErr)
        return;

    if (!this->isSPDIFFormat(format))
        return;

    QVector<AudioStreamBasicDescription> formats;

    if (!this->getFormatList(id, &formats))
        return;

    foreach (AudioStreamBasicDescription f, formats)
    {
        if (f.mFormatID != kAudioFormatLinearPCM)
            continue;

        if (AudioObjectSetPropertyData(id, &address, 0, NULL, paramSize, &f) == noErr)
        {
            sleep(1);
            break;
        }
    }
}

bool SPDIFCoreAudio::isSPDIFFormat(const AudioStreamBasicDescription &format) const
{
    return format.mFormatID == 'IAC3' || format.mFormatID == kAudioFormat60958AC3;
}

bool SPDIFCoreAudio::getDeviceName(AudioDeviceID id, QString *ret)
{
    CFStringRef name;
    UInt32 paramSize = sizeof(CFStringRef);
    AudioObjectPropertyAddress address =
    {
        kAudioObjectPropertyName,
        kAudioObjectPropertyScopeOutput,
        kAudioObjectPropertyElementMaster
    };

    if (AudioObjectGetPropertyData(id, &address, 0, NULL, &paramSize, &name) != noErr)
        return false;

    CFIndex length = CFStringGetLength(name);
    CFIndex stringSize = CFStringGetMaximumSizeForEncoding(length, kCFStringEncodingUTF8);
    char *cName = new char[stringSize];

    CFStringGetCString(name, cName, stringSize, kCFStringEncodingUTF8);

    *ret = QString::fromUtf8(cName);
    delete[] cName;

    return true;
}

bool SPDIFCoreAudio::getDefaultDevice(AudioDeviceID *ret)
{
    UInt32 paramSize = sizeof(AudioDeviceID);
    AudioObjectPropertyAddress address =
    {
        kAudioHardwarePropertyDefaultOutputDevice,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    *ret = kAudioObjectUnknown;

    return AudioObjectGetPropertyData(kAudioObjectSystemObject,
                                      &address, 0, NULL, &paramSize, ret) == noErr;
}

bool SPDIFCoreAudio::getStreamList(AudioDeviceID id, QVector<AudioStreamID> *ret)
{
    QVector<AudioStreamID> ids;
    UInt32 listSize = 0;
    AudioStreamID *list = NULL;
    AudioObjectPropertyAddress address =
    {
        kAudioDevicePropertyStreams,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    if (AudioObjectGetPropertyDataSize(id, &address, 0, NULL, &listSize) != noErr)
        return false;

    list = new AudioStreamID[listSize];

    if (AudioObjectGetPropertyData(id, &address, 0, NULL, &listSize, list) != noErr)
    {
        delete[] list;
        return false;
    }

    for (size_t i = 0; i < listSize / sizeof(AudioStreamID); i++)
        ids.append(list[i]);

    delete[] list;
    *ret = ids;

    return true;
}

bool SPDIFCoreAudio::getFormatList(AudioStreamID id, QVector<AudioStreamBasicDescription> *ret)
{
    QVector<AudioStreamBasicDescription> ids;
    UInt32 listSize = 0;
    AudioStreamBasicDescription *list = NULL;
    AudioObjectPropertyAddress address =
    {
        kAudioStreamPropertyPhysicalFormats,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    if (AudioObjectGetPropertyDataSize(id, &address, 0, NULL, &listSize) != noErr)
        return false;

    list = new AudioStreamBasicDescription[listSize];

    if (AudioObjectGetPropertyData(id, &address, 0, NULL, &listSize, list) != noErr)
    {
        delete[] list;
        return false;
    }

    for (size_t i = 0; i < listSize / sizeof(AudioStreamBasicDescription); i++)
        ids.append(list[i]);

    delete[] list;
    *ret = ids;

    return true;
}

char* SPDIFCoreAudio::UInt32ToFourCC(UInt32* val)
{
   static char fourCC[5];
   UInt32 inVal = *val;
   char* in = (char*)&inVal;

   fourCC[4] = 0;
   fourCC[3] = in[0];
   fourCC[2] = in[1];
   fourCC[1] = in[2];
   fourCC[0] = in[3];

   return fourCC;
}

OSStatus SPDIFCoreAudio::SPDIFCallback(AudioDeviceID inDevice, const AudioTimeStamp *inNow,
                                       const void *inInputData, const AudioTimeStamp *inInputTime,
                                       AudioBufferList *outOutputData, const AudioTimeStamp *inOutputTime,
                                       void *threadGlobals)
{
    (void) inDevice;
    (void) inNow;
    (void) inInputData;
    (void) inInputTime;
    (void) inOutputTime;
    SPDIFCoreAudio *parent = (SPDIFCoreAudio*)threadGlobals;
    AudioBuffer &buffer = outOutputData->mBuffers[parent->m_streamIndex];

    if (parent->m_bytesPerPacket > 0 && buffer.mDataByteSize > parent->m_bytesPerPacket)
        buffer.mDataByteSize = parent->m_bytesPerPacket;

    buffer.mDataByteSize = parent->m_callback(buffer.mData, buffer.mDataByteSize, parent->m_user);

    return noErr;
}

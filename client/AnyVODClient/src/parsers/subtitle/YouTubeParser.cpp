﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "YouTubeParser.h"
#include "net/HttpDownloader.h"
#include "net/YouTubeURLPicker.h"
#include "core/Utils.h"

#include <QTemporaryFile>
#include <QXmlStreamReader>
#include <QBuffer>
#include <QUrlQuery>
#include <QDir>
#include <QDebug>

const QString YouTubeParser::SUBTITLE_INFO_URL = "https://video.google.com/timedtext?hl=en&type=list&v=";
const QString YouTubeParser::SUBTITLE_URL = "https://www.youtube.com/api/timedtext?";

YouTubeParser::YouTubeParser()
{

}

YouTubeParser::~YouTubeParser()
{
    this->close();
}

bool YouTubeParser::open(const QString &id)
{
    bool ok = false;

    this->close();

    ok = this->getManualSubtitles(id);

    if (!ok)
    {
        this->close();
        ok = this->getAutoSubtitles(id);
    }

    if (ok)
        this->setDefaultLanguage(this->m_curLanguage);

    return this->isExist();
}

void YouTubeParser::close()
{
    QMutexLocker locker(&this->m_lock);

    foreach (Item item, this->m_subtitles)
    {
        if (item.parser)
        {
            item.parser->close();
            delete item.parser;
        }
    }

    this->m_subtitles.clear();
    this->m_curLanguage.clear();
}

bool YouTubeParser::save(const QString &path, double sync)
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_subtitles.contains(this->m_curLanguage))
    {
        Item item = this->m_subtitles[this->m_curLanguage];

        if (item.parser)
            return item.parser->save(path, sync);
        else
            return false;
    }

    return false;
}

bool YouTubeParser::get(int32_t millisecond, SRTParser::Item *ret)
{
    QMutexLocker locker(&this->m_lock);

    if (this->m_subtitles.contains(this->m_curLanguage))
    {
        Item item = this->m_subtitles[this->m_curLanguage];

        if (item.parser)
            return item.parser->get(millisecond, ret);
        else
            return false;
    }

    return false;
}

bool YouTubeParser::isExist()
{
    QMutexLocker locker(&this->m_lock);

    return !this->m_subtitles.isEmpty();
}

void YouTubeParser::getDefaultLanguage(QString *ret)
{
    QMutexLocker locker(&this->m_lock);

    *ret = this->m_curLanguage;
}

void YouTubeParser::setDefaultLanguage(const QString &lang)
{
    QMutexLocker locker(&this->m_lock);
    QBuffer subtitle;
    HttpDownloader d;
    Item item = this->m_subtitles[lang];

    this->m_curLanguage = lang;

    if (!item.parser)
    {
        if (!d.download(item.url, subtitle))
            return;

        item.parser = this->getSRTParser(subtitle);

        if (item.parser)
            this->m_subtitles[lang] = item;
    }
}

void YouTubeParser::getLanguages(QStringList *ret)
{
    QMutexLocker locker(&this->m_lock);

    *ret = this->m_subtitles.keys();
}

bool YouTubeParser::getTTSUrl(const QString &id, QString *url, QString *listUrl, QString *timestamp) const
{
    HttpDownloader d;
    QBuffer data;
    QString ret;

    if (!d.download(YouTubeURLPicker::BASE_URL + id, data))
        return false;

    QString content(data.buffer());
    QRegExp match;
    QStringList patterns;

    patterns.append("[\"']?TTS_URL[\"']?\\s*:\\s*[\"']?([\\w\\d\\/\\\\\\.\\-:%&\\?=]+)[\"']?,?");
    patterns.append("[\"']?ttsurl[\"']?\\s*:\\s*[\"']?([\\w\\d\\/\\\\\\.\\-:%&\\?=]+)[\"']?,?");

    foreach (const QString &pattern, patterns)
    {
        match.setPattern(pattern);

        if (match.indexIn(content) < 0)
            continue;

        ret = match.cap(1);

        break;
    }

    ret = ret.replace("\\/", "/").replace("\\u0026", "&");

    *url = ret;

    ret += "&type=list&tlangs=1&asrs=1";

    *listUrl = ret;

    match.setPattern("\"timestamp\"\\s*:\\s*\"?(\\d+)\"?,?");

    if (match.indexIn(content) < 0)
        return false;

    ret = match.cap(1);

    *timestamp = ret;

    return true;
}

bool YouTubeParser::getAutoSubtitles(const QString &id)
{
    HttpDownloader d;
    QBuffer data;
    QXmlStreamReader xml;
    QString url;
    QString listUrl;
    QString timestamp;
    QString firstLang;

    if (!this->getTTSUrl(id, &url, &listUrl, &timestamp))
        return false;

    if (!data.open(QIODevice::ReadWrite))
        return false;

    if (!d.download(listUrl, data))
        return false;

    if (!data.reset())
        return false;

    xml.setDevice(&data);

    //<target id="9" urlfrag="&lang=ko" lang_code="ko" lang_original="한국어" lang_translated="한국어" lang_default="true"/>
    while (!xml.atEnd())
    {
        if (!xml.readNextStartElement())
            continue;

        QString elem = xml.name().toString();

        if (elem != "track")
            continue;

        QXmlStreamAttributes attr = xml.attributes();
        QString langCode = attr.value("lang_code").toString();
        QString kind = attr.value("kind").toString();

        while (!xml.atEnd())
        {
            if (!xml.readNextStartElement())
                continue;

            elem = xml.name().toString();

            if (elem != "target")
                continue;

            QXmlStreamAttributes attrSub = xml.attributes();
            QString langSubCode = attrSub.value("lang_code").toString();
            QString langOriginal = attrSub.value("lang_original").toString();
            QString langDefault = attrSub.value("lang_default").toString();
            QUrlQuery query;

            if (this->m_subtitles.contains(langOriginal))
                continue;

            query.addQueryItem("lang", langCode);
            query.addQueryItem("tlang", langSubCode);
            query.addQueryItem("fmt", "vtt");
            query.addQueryItem("ts", timestamp);
            query.addQueryItem("kind", kind);

            Item item;

            item.url = url + "&" + query.query();
            item.parser = NULL;

            this->m_subtitles[langOriginal] = item;

            if (langDefault.toLower() == "true")
                this->m_curLanguage = langOriginal;

            if (firstLang.isEmpty())
                firstLang = langOriginal;
        }
    }

    if (this->m_curLanguage.isEmpty())
        this->m_curLanguage = firstLang;

    data.close();

    return this->isExist();
}

bool YouTubeParser::getManualSubtitles(const QString &id)
{
    //https://www.youtube.com/watch?v=XJGiS83eQLk
    HttpDownloader d;
    QBuffer data;
    QXmlStreamReader xml;
    QString firstLang;

    if (!data.open(QIODevice::ReadWrite))
        return false;

    if (!d.download(SUBTITLE_INFO_URL + id, data))
        return false;

    if (!data.reset())
        return false;

    xml.setDevice(&data);

    //<track id="0" name="" lang_code="en" lang_original="English" lang_translated="English" lang_default="true"/>
    while (!xml.atEnd())
    {
        if (!xml.readNextStartElement())
            continue;

        QString elem = xml.name().toString();

        if (elem != "track")
            continue;

        QXmlStreamAttributes attr = xml.attributes();
        QString name = attr.value("name").toString();
        QString langCode = attr.value("lang_code").toString();
        QString langOriginal = attr.value("lang_original").toString();
        QString langDefault = attr.value("lang_default").toString();
        QUrlQuery query;

        if (this->m_subtitles.contains(langOriginal))
            continue;

        query.addQueryItem("lang", langCode);
        query.addQueryItem("v", id);
        query.addQueryItem("fmt", "srt");
        query.addQueryItem("name", name);

        Item item;

        item.url = SUBTITLE_URL + query.query();
        item.parser = NULL;

        this->m_subtitles[langOriginal] = item;

        if (langDefault.toLower() == "true")
            this->m_curLanguage = langOriginal;

        if (firstLang.isEmpty())
            firstLang = langOriginal;
    }

    if (this->m_curLanguage.isEmpty())
        this->m_curLanguage = firstLang;

    data.close();

    return this->isExist();
}

SRTParser* YouTubeParser::getSRTParser(QBuffer &subtitle) const
{
    QString tmpFilePath = QDir::tempPath();

    Utils::appendDirSeparator(&tmpFilePath);
    tmpFilePath += "XXXXXX";
    tmpFilePath += ".srt";

    QTemporaryFile tmpFile(tmpFilePath);

    if (!tmpFile.open())
        return NULL;

    QFile file(tmpFile.fileName());

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return NULL;

    QTextStream stream(&file);

    stream.setCodec("UTF-8");
    stream.setGenerateByteOrderMark(true);

    if (!subtitle.open(QIODevice::ReadWrite))
        return NULL;

    stream << subtitle.buffer();

    subtitle.close();
    file.close();
    tmpFile.close();

    SRTParser *parser = new SRTParser();

    if (!parser->open(tmpFile.fileName()))
    {
        delete parser;
        return NULL;
    }

    return parser;
}

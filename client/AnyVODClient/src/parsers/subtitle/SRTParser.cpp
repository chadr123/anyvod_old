﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "SRTParser.h"
#include "core/Common.h"
#include "core/Utils.h"
#include "core/TextEncodingDetector.h"
#include "../../../../common/types.h"

#include <QFile>
#include <QMutexLocker>
#include <QRegExp>
#include <QTextStream>
#include <QTime>
#include <QDebug>

SRTParser::SRTParser() :
    m_curIndex(0)
{

}

bool SRTParser::open(const QString &path)
{
    this->close();

    QFile file(path);
    QString codecName;
    TextEncodingDetector detector;

    if (!detector.getCodecName(path, &codecName))
        return false;

    if (!file.open(QIODevice::ReadOnly))
        return false;

    this->parse(file, codecName);
    qStableSort(this->m_items);

    return this->isExist();
}

bool SRTParser::save(const QString &path, double sync)
{
    QMutexLocker locker(&this->m_lock);
    QFile file(path);

    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QTextStream stream(&file);

    stream.setCodec("UTF-8");
    stream.setGenerateByteOrderMark(true);

    for (int i = 0; i < this->m_items.count(); i++)
    {
        const Item &item = this->m_items[i];

        stream << i + 1 << endl;

        QTime startTime = QTime::fromMSecsSinceStartOfDay(item.start + int(sync * 1000));
        QTime endTime = QTime::fromMSecsSinceStartOfDay(item.end + int(sync * 1000));

        stream << startTime.toString(Utils::TIME_HH_MM_SS_ZZZ_COMMA);
        stream << " --> ";
        stream << endTime.toString(Utils::TIME_HH_MM_SS_ZZZ_COMMA);

        if (!item.rect.isEmpty())
        {
            const QRect &rect = item.rect;

            stream << " X1:" << rect.left() << " X2:" << rect.right()
                   << " Y1:" << rect.top() << " Y2:" << rect.bottom();
        }

        stream << endl;

        for (int j = 0; j < item.texts.count(); j++)
            stream << item.texts[j] << endl;

        stream << endl;

        if (stream.status() != QTextStream::Ok)
            return false;
    }

    stream.flush();

    if (stream.status() != QTextStream::Ok)
        return false;

    return true;
}

void SRTParser::close()
{
    QMutexLocker locker(&this->m_lock);

    this->m_items.clear();
}

bool SRTParser::get(int32_t millisecond, Item *ret)
{
    QMutexLocker locker(&this->m_lock);
    int count = this->m_items.count();
    int oldIndex = this->m_curIndex;

    if (count > 0)
    {
        for (int i = 0; i < count; i++)
        {
            Item &item = this->m_items[this->m_curIndex];

            if (item.start <= millisecond && millisecond < item.end)
            {
                *ret = item;
                return true;
            }
            else
            {
                this->m_curIndex++;

                if (this->m_curIndex >= count)
                    this->m_curIndex = 0;
            }
        }

        this->m_curIndex = oldIndex;
    }

    return false;
}

bool SRTParser::isExist()
{
    QMutexLocker locker(&this->m_lock);

    return this->m_items.count() > 0;
}

bool SRTParser::parseTime(const QString &time, int *ret) const
{
    QTime tmp = QTime::fromString(time, Utils::TIME_HH_MM_SS_ZZZ_COMMA);

    if (!tmp.isValid())
    {
        tmp = QTime::fromString(time, Utils::TIME_HH_MM_SS_ZZZ);

        if (!tmp.isValid())
            return false;
    }

    *ret = -tmp.msecsTo(Utils::ZERO_TIME);

    return true;
}

bool SRTParser::parseRect(const QString rect[4], QRect *ret) const
{
    for (int i = 0; i < 4; i++)
    {
        QStringList elements = rect[i].split(":", QString::SkipEmptyParts);

        if (elements.count() < 2)
            return false;

        QString elem = elements[0].toLower();
        bool ok = false;
        int integer = elements[1].toInt(&ok);

        if (!ok)
            return false;

        if (elem == "x1")
            ret->setLeft(integer);
        else if (elem == "x2")
            ret->setRight(integer);
        else if (elem == "y1")
            ret->setTop(integer);
        else if (elem == "y2")
            ret->setBottom(integer);
        else
            return false;
    }

    return true;
}

bool SRTParser::parseTimeline(const QString &line, Item *ret) const
{
    QString time;
    QString tmp = line;
    QTextStream parser(&tmp);

    parser >> time;

    if (!this->parseTime(time, &ret->start))
        return false;

    parser >> time;
    parser >> time;

    if (!this->parseTime(time, &ret->end))
        return false;

    QString rect[4];

    for (int i = 0; i < 4; i++)
        parser >> rect[i];

    if (rect[0].isEmpty())
        return true;

    if (!this->parseRect(rect, &ret->rect))
        return false;

    return true;
}

void SRTParser::parseText(const QString &line, QString *ret) const
{
    QString removed = line;

    removed.remove(QRegExp("<[^>]*>"));
    removed.replace('\t', TABS);

    *ret = removed;
}

void SRTParser::parse(QFile &file, const QString &codecName)
{
    QMutexLocker locker(&this->m_lock);
    QTextStream stream(&file);
    bool vttMode = false;

    stream.setCodec(codecName.toLatin1());

    while (true)
    {
        QString line = stream.readLine().trimmed();

        if (line.isNull())
            break;

        if (line.isEmpty())
            continue;

        if (line == "WEBVTT")
        {
            vttMode = true;
            continue;
        }

        Item item;

        if (!vttMode)
            line = stream.readLine().trimmed();

        if (!this->parseTimeline(line, &item))
            continue;

        while (true)
        {
            QString text;

            line = stream.readLine().trimmed();

            if (line.isEmpty())
                break;

            this->parseText(line, &text);

            item.texts.append(text);
        }

        this->m_items.append(item);
    }
}

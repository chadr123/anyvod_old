﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "SRTParser.h"

#include <QString>
#include <QMutex>
#include <QMap>

class QBuffer;

class YouTubeParser
{
public:
    YouTubeParser();
    ~YouTubeParser();

    bool open(const QString &id);
    void close();

    bool save(const QString &path, double sync);

    bool get(int32_t millisecond, SRTParser::Item *ret);
    bool isExist();

    void getDefaultLanguage(QString *ret);
    void setDefaultLanguage(const QString &lang);
    void getLanguages(QStringList *ret);

private:
    bool getTTSUrl(const QString &id, QString *url, QString *listUrl, QString *timestamp) const;
    bool getAutoSubtitles(const QString &id);
    bool getManualSubtitles(const QString &id);
    SRTParser* getSRTParser(QBuffer &subtitle) const;

private:
    struct Item
    {
        QString url;
        SRTParser* parser;
    };

private:
    static const QString SUBTITLE_INFO_URL;
    static const QString SUBTITLE_URL;

private:
    QString m_curLanguage;
    QMap<QString, Item> m_subtitles;
    QMutex m_lock;
};

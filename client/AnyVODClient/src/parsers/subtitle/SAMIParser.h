﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <stdint.h>
#include <QStringList>
#include <QVector>
#include <QMutex>
#include <QColor>
#include <QStack>

class QFile;

class SAMIParser
{
public:
    struct Text
    {
        bool bold;
        bool underline;
        bool italic;
        bool strike;
        QColor color;
        QString text;
    };

    struct Line
    {
        QVector<Text> subtitles;
    };

    struct Paragraph
    {
        QString className;
        QVector<Line> lines;
    };

    struct Sync
    {
        int32_t start;
        int32_t end;
        QVector<Paragraph> paragraphs;
    };

    struct SAMI
    {
        QStringList classNames;
        QString defaultClassName;
        QVector<Sync> syncs;
    };

    SAMIParser();

    bool open(const QString &path);
    void close();

    bool save(const QString &path, double sync);

    bool get(const QString &className, int32_t millisecond, Paragraph *ret);

    void getDefaultClassName(QString *ret);
    void setDefaultClassName(const QString &className);
    void getClassNames(QStringList *ret);

    bool isExist();

private:
    struct Context
    {
        bool bold;
        bool underline;
        bool italic;
        bool strike;
        QColor color;

        QStack<bool> boldStack;
        QStack<bool> underlineStack;
        QStack<bool> italicStack;
        QStack<bool> strikeStack;
        QStack<QColor> colorStack;
    };

    void removeComment(QString *ret) const;

    void parse(QFile &file, const QString &codecName);
    void parseClassName(const QString &data);
    void parseSync(const QString &data, QVector<Paragraph> *ret);
    void parseParagraph(const QString &data, QVector<Line> *ret) const;
    void parseLine(const QString &data, Context *context, QVector<Text> *ret) const;

    void determinDefaultClassName();

    bool getColor(const QString &color, QColor *ret) const;

    void removeTags(QString *ret) const;
    int getTag(const QString &text, int pos, QString *ret) const;

    void fillEndTime();
    bool findSecond(const QString &className, int index, Sync **ret);

    void addTimeToContent(const QString &content, int sync, QString *ret);

private:
    int m_curIndex;
    QString m_contents;
    SAMI m_sami;
    QMutex m_lock;
};

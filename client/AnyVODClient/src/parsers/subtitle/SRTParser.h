﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <stdint.h>
#include <QVector>
#include <QMutex>
#include <QRect>
#include <QStringList>

class QFile;

class SRTParser
{
public:
    struct Item
    {
        QRect rect;
        int start;
        int end;
        QStringList texts;

        bool operator < (const Item &rhs) const
        {
            return start < rhs.start;
        }
    };

    SRTParser();

    bool open(const QString &path);
    void close();

    bool save(const QString &path, double sync);

    bool get(int32_t millisecond, Item *ret);
    bool isExist();

private:
    void parse(QFile &file, const QString &codecName);
    bool parseRect(const QString rect[4], QRect *ret) const;
    bool parseTime(const QString &time, int *ret) const;
    bool parseTimeline(const QString &line, Item *ret) const;
    void parseText(const QString &line, QString *ret) const;

private:
    int m_curIndex;
    QVector<Item> m_items;
    QMutex m_lock;
};

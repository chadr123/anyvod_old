﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "ASSParser.h"
#include "core/Common.h"
#include "media/MediaState.h"

#include <stdint.h>
#include <QVector>
#include <QHash>
#include <QSize>

class QString;

struct AVSubtitle;
struct AVCodecContext;

class AVParser
{
public:
    AVParser();

    bool open(const QString &path, const QString &fontPath, const QString &fontFamily);
    void close();

    void setFrameSize(const QSize &size);

    bool get(const uint32_t millisecond, AVSubtitle **ret);
    bool getASSImage(const uint32_t millisecond, ASS_Image **ret, bool *changed);

    void getStreamInfos(QVector<SubtitleStreamInfo> *ret) const;

    unsigned int getCurrentIndex() const;
    bool setCurrentIndex(unsigned int index);

    bool getCurrentName(QString *ret) const;
    bool getDesc(QString *ret) const;

    bool isExist();

private:
    void fillStreamInfos(const AVFormatContext *format);
    bool fillItems(AVFormatContext *format);

    void adjustSubtitleTimes();
    bool rebuildSubtitleTime(const SubtitleElement &second, SubtitleElement *first) const;

    void closeStream(AVFormatContext *format);
    void addToASSParser(unsigned int index);

private:
    QVector<SubtitleStreamInfo> m_streamInfos;
    QHash<unsigned int, QVector<SubtitleElement> > m_items;
    ASSParser m_assParser;
    unsigned int m_curIndex;
    int m_searchIndex;
    bool m_isUTF8;
    QString m_assHeader;
    QSize m_frameSize;
    QHash<unsigned int, AVCodecContext*> m_ctxs;
};

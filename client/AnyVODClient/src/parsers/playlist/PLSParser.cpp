﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "PLSParser.h"
#include "core/TextEncodingDetector.h"

#include <QFile>
#include <QTextStream>
#include <QMap>
#include <QList>

PLSParser::PLSParser()
{

}

bool PLSParser::parse(QFile &file)
{
    QString codecName;
    TextEncodingDetector detector;

    if (!detector.getCodecName(file.fileName(), &codecName))
        return false;

    QTextStream stream(&file);

    stream.setCodec(codecName.toLatin1());

    while (true)
    {
        QString line = stream.readLine().trimmed();

        if (line.isNull())
            break;

        if (line.isEmpty())
            continue;

        if (line == "[playlist]")
            break;
        else
            return false;
    }

    QMap<int, PlayListParserInterface::PlayListItem> directory;

    while (true)
    {
        QString line = stream.readLine().trimmed();

        if (line.isNull())
            break;

        if (line.isEmpty())
            continue;

        if (line.startsWith("File", Qt::CaseInsensitive))
        {
            int pos = line.indexOf('=');

            if (pos == -1)
                continue;

            int index = this->getIndex("File", line);

            if (index >= 0)
                directory[index].path = line.mid(pos + 1).trimmed().replace("shout:", "http:", Qt::CaseInsensitive);
        }
        else if (line.startsWith("Title", Qt::CaseInsensitive))
        {
            int pos = line.indexOf('=');

            if (pos == -1)
                continue;

            int index = this->getIndex("Title", line);

            if (index >= 0)
                directory[index].title = line.mid(pos + 1).trimmed();
        }
    }

    QList<PlayListParserInterface::PlayListItem> list = directory.values();

    foreach (const PlayListParserInterface::PlayListItem &item, list)
        this->appendPlayList(item);

    return true;
}

int PLSParser::getIndex(const QString &key, const QString &line) const
{
    int pos = line.indexOf('=');

    if (pos == -1)
        return pos;

    QString num = line.mid(key.count(), pos - key.count());
    bool ok = false;
    int value = num.toInt(&ok);

    if (ok)
        return value;
    else
        return -1;
}

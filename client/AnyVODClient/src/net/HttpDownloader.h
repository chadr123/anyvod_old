﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <QThread>
#include <QNetworkReply>
#include <QMutex>

class HttpDownloader : private QThread
{
    Q_OBJECT
public:
    HttpDownloader();
    ~HttpDownloader();

    bool download(const QString &path, QIODevice &to);
    QNetworkReply::NetworkError error() const;

private slots:
    void received();

protected:
    virtual void run();

private:
    void request(QIODevice *to);
    bool isRedirect() const;
    bool downloadInternal(const QString &path, QIODevice &to);

private:
    QNetworkRequest m_header;
    QIODevice *m_to;
    QNetworkReply::NetworkError m_error;
    QString m_redirectURL;
    QMutex m_lock;
};

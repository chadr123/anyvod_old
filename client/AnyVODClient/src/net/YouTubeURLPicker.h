﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <QString>
#include <QVector>

class QBuffer;
class QJsonObject;
class QUrlQuery;

class YouTubeURLPicker
{
public:
    struct Item
    {
        QString title;
        QString url;
        QString id;
        QString orgUrl;
        QString desc;
        QString shortDesc;
        QString mimeType;
        QString quality;
        QString dashmpd;
    };

public:
    YouTubeURLPicker();
    ~YouTubeURLPicker();

    QVector<YouTubeURLPicker::Item> pickURL(const QString &url);
    bool getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const;
    bool isPlayList() const;

public:
    static const QString BASE_URL;
    static const QString INFO_URL;
    static const QString EMBED_URL;
    static const QString LIST_URL;
    static const QString API_URL;

private:
    QString adjustURL(const QString &url) const;
    QVector<YouTubeURLPicker::Item> parseVideoContent(const QString &content, const QString &id);
    QVector<YouTubeURLPicker::Item> parsePlayListContent(const QString &url);
    QString decryptSignature(const QString &sig, const QString &js) const;
    bool getVideoInfo(const QString &content, const QString &id, QString *ret) const;
    QString getOrgURL(const QString &url) const;
    bool getJS(const QString &content, QBuffer *js) const;
    bool getJSon(const QString &content, QJsonObject *obj) const;
    void toUrlQuery(const QStringList &list, QVector<QUrlQuery> *ret) const;
    void getFormatListFromM3U8(const QString &url, YouTubeURLPicker::Item &refItem, QVector<YouTubeURLPicker::Item> *ret);

private:
    bool m_isPlayList;
};

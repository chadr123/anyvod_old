﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "YouTubeURLPicker.h"
#include "net/HttpDownloader.h"
#include "core/Utils.h"

#include <QBuffer>
#include <QUrl>
#include <QUrlQuery>
#include <QJSEngine>
#include <QTextDocument>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QXmlStreamReader>
#include <QDebug>

const QString YouTubeURLPicker::BASE_URL = "https://www.youtube.com/watch?gl=US&hl=en&has_verified=1&bpctr=9999999999&v=";
const QString YouTubeURLPicker::EMBED_URL = "https://www.youtube.com/embed/";
const QString YouTubeURLPicker::INFO_URL = "https://www.youtube.com/get_video_info?";
const QString YouTubeURLPicker::LIST_URL = "https://www.youtube.com/list_ajax?action_get_list=1&style=xml&list=";
const QString YouTubeURLPicker::API_URL = "https://youtube.googleapis.com/v/";

/*
 http://www.youtube.com/watch?v=BaW_jenozKc
 http://www.youtube.com/watch?v=UxxajLWwzqY
 https://www.youtube.com/watch?v=07FYdnEawAQ
 https://www.YouTube.com/watch?v=yZIXLfi8CZQ
 http://www.youtube.com/watch?v=a9LDPn-MO4I
 https://www.youtube.com/watch?v=IB3lcPjvWLA
 https://www.youtube.com/watch?v=nfWlot6h_JM
 https://www.youtube.com/watch?v=T4XJQO3qol8
 http://youtube.com/watch?v=HtVdAasjOgU
 http://www.youtube.com/watch?v=6kLq3WMV1nU
 http://www.youtube.com/watch?v=__2ABJjxzNo
 http://www.youtube.com/watch?v=lqQg6PlCWgI
 https://www.youtube.com/watch?v=_b-2C3KPAM0
*/

YouTubeURLPicker::YouTubeURLPicker() :
    m_isPlayList(false)
{

}

YouTubeURLPicker::~YouTubeURLPicker()
{

}

QVector<YouTubeURLPicker::Item> YouTubeURLPicker::pickURL(const QString &url)
{
    QString to = url;
    QVector<YouTubeURLPicker::Item> ret;

    to = this->adjustURL(to);

    if (to.isEmpty())
        return ret;

    if (to.contains("/watch", Qt::CaseInsensitive))
    {
        HttpDownloader downloader;
        QBuffer buffer;

        if (!downloader.download(to, buffer))
        {
            to = to.replace("https", "http", Qt::CaseInsensitive);

            downloader.download(to, buffer);
        }

        QString org = buffer.buffer();
        QString content;
        QUrlQuery query(QUrl(to).query());
        QString id = query.queryItemValue("v");

        if (this->getVideoInfo(org, id, &content))
            ret = this->parseVideoContent(content, id);
    }
    else
    {
        ret = this->parsePlayListContent(to);
    }

    return ret;
}

bool YouTubeURLPicker::isPlayList() const
{
    return this->m_isPlayList;
}

QString YouTubeURLPicker::adjustURL(const QString &url) const
{
    QString id;

    if (!url.contains("youtu", Qt::CaseInsensitive))
        return id;

    if (url.contains("/watch", Qt::CaseInsensitive) ||
            url.contains("/playlist", Qt::CaseInsensitive))
    {
        return url;
    }
    else if (url.contains("/v/", Qt::CaseInsensitive) ||
             url.contains("/embed/", Qt::CaseInsensitive) ||
             url.contains("youtu.be", Qt::CaseInsensitive))
    {
        int index = url.lastIndexOf("/") + 1;

        id = url.mid(index);
    }
    else
    {
        return id;
    }

    return BASE_URL + id;
}

QVector<YouTubeURLPicker::Item> YouTubeURLPicker::parseVideoContent(const QString &content, const QString &id)
{
    QVector<YouTubeURLPicker::Item> ret;
    int index;
    YouTubeURLPicker::Item item;
    QRegExp match;
    QString title;
    QStringList patterns;

    patterns.append("<meta\\s+name\\s*=\\s*[\"]?title[\"]?\\s*content\\s*=\\s*[\"]?(.*[^\"])[\"]?\\s*>");
    patterns.append("<meta\\s+property\\s*=\\s*[\"]?og:title[\"]?\\s*content\\s*=\\s*[\"]?(.*[^\"])[\"]?\\s*>");

    foreach (const QString &pattern, patterns)
    {
        match.setPattern(pattern);
        match.setMinimal(true);

        if (match.indexIn(content) >= 0)
        {
            QTextDocument escape;

            title = match.cap(1);
            title = title.remove("\"");

            escape.setHtml(title);
            title = escape.toPlainText();
            escape.setHtml(title);
            title = escape.toPlainText();

            break;
        }
    }

    index = content.indexOf("hlsvp");

    if (index >= 0)
    {
        QString url;

        match.setPattern("[\"]?hlsvp[\"]?\\s*:\\s*[\"]?([\\w\\d\\/\\\\\\.\\-:%&\\?=]+)[\"]?,?");
        match.setMinimal(false);

        if (match.indexIn(content) < 0)
            return ret;

        url = match.cap(1);
        url = url.replace("\\/", "/").replace("\\u0026", "&");

        item.title = title;
        item.orgUrl = this->getOrgURL(BASE_URL + id);
        item.id = id;

        this->getFormatListFromM3U8(url, item, &ret);
    }
    else
    {
        QJsonObject json;
        QJsonObject args;
        QVector<QUrlQuery> fmtMap;
        QVector<QUrlQuery> adaptiveFmt;
        QBuffer js;

        if (!this->getJS(content, &js))
            return ret;

        if (!this->getJSon(content, &json))
            return ret;

        args = json["args"].toObject();

        this->toUrlQuery(args["url_encoded_fmt_stream_map"].toString().split(",", QString::SkipEmptyParts), &fmtMap);
        this->toUrlQuery(args["adaptive_fmts"].toString().split(",", QString::SkipEmptyParts), &adaptiveFmt);

        foreach (const QUrlQuery &q, fmtMap)
        {
            QString url = QUrl::fromPercentEncoding(q.queryItemValue("url").toLatin1());
            QString quality = q.queryItemValue("quality");
            QString type = QUrl::fromPercentEncoding(q.queryItemValue("type").toLatin1());
            QString encryptedSig = q.queryItemValue("s");
            QStringList typeList = type.split(";", QString::SkipEmptyParts);

            if (typeList.length() == 2)
            {
                QString mime = QUrl::fromPercentEncoding(typeList[0].toLatin1());
                QString codec = typeList[1].remove("codecs").remove("+").remove("=").remove("\"").replace(",", "/");

                item.desc = quality + "(" + mime.split("/", QString::SkipEmptyParts).at(1) + ", " + codec + ")";
                item.shortDesc = item.desc;
                item.mimeType = mime;
            }

            if (encryptedSig.isEmpty())
                encryptedSig = q.queryItemValue("sig");

            encryptedSig = this->decryptSignature(encryptedSig, js.buffer());

            if (!encryptedSig.isEmpty())
                url += "&signature=" + encryptedSig;

            item.title = title;
            item.orgUrl = this->getOrgURL(BASE_URL + id);
            item.url = url;
            item.id = id;
            item.quality = quality;

            ret.append(item);
        }

        QString dashmpd;

        if (!adaptiveFmt.isEmpty())
        {
            QString sig;
            QString pattern = "/s/([\\w\\.]+)";
            QRegExp match(pattern);

            dashmpd = args["dashmpd"].toString();

            if (dashmpd.isEmpty())
            {
                HttpDownloader d;
                QBuffer data;
                QUrlQuery query;

                if (d.download(EMBED_URL + id, data))
                {
                    QString embed = data.buffer();
                    QRegExp stsMatch("\"sts\"\\s*:\\s*(\\d+)");

                    if (stsMatch.indexIn(embed) >= 0)
                        query.addQueryItem("sts", stsMatch.cap(1));
                }

                query.addQueryItem("video_id", id);
                query.addQueryItem("eurl", API_URL + id);

                if (d.download(INFO_URL + query.query(), data))
                {
                    query.setQuery(data.buffer());

                    dashmpd = QUrl::fromPercentEncoding(query.queryItemValue("dashmpd").toLatin1());
                }
            }

            if (match.indexIn(dashmpd) >= 0)
            {
                sig = match.cap(1);
                sig = this->decryptSignature(sig, js.buffer());

                dashmpd = dashmpd.replace(QRegExp(pattern), "/signature/" + sig);
            }
        }

        foreach (const QUrlQuery &q, adaptiveFmt)
        {
            QString url = QUrl::fromPercentEncoding(q.queryItemValue("url").toLatin1());
            QString size = q.queryItemValue("size");
            QString quality = q.queryItemValue("quality_label");
            QString fps = q.queryItemValue("fps") + "fps";
            QString type = QUrl::fromPercentEncoding(q.queryItemValue("type").toLatin1());
            QString encryptedSig = q.queryItemValue("s");
            QStringList typeList = type.split(";", QString::SkipEmptyParts);
            unsigned long long bitrate = q.queryItemValue("bitrate").toULongLong();

            if (typeList.length() == 2)
            {
                QString mime = QUrl::fromPercentEncoding(typeList[0].toLatin1());
                QString codec = typeList[1].remove("codecs").remove("+").remove("=").remove("\"").replace(",", "/");
                QStringList types = mime.split("/", QString::SkipEmptyParts);

                if (types[0] == "video")
                {
                    item.desc = QString("%1 %2 %3 %4 (%5, %6)")
                            .arg(types[0])
                            .arg(Utils::sizeToString(bitrate))
                            .arg(size)
                            .arg(fps)
                            .arg(types[1])
                            .arg(codec);
                    item.shortDesc = QString("%1 %2 %3 %4 (%5)")
                            .arg(types[0])
                            .arg(Utils::sizeToString(bitrate))
                            .arg(size)
                            .arg(fps)
                            .arg(types[1]);
                }
                else
                {
                    item.desc = QString("%1 %2 (%3, %4)")
                            .arg(types[0])
                            .arg(Utils::sizeToString(bitrate))
                            .arg(types[1])
                            .arg(codec);
                    item.shortDesc = QString("%1 %2 (%3)")
                            .arg(types[0])
                            .arg(Utils::sizeToString(bitrate))
                            .arg(types[1]);
                }

                item.mimeType = mime;
            }

            if (encryptedSig.isEmpty())
                encryptedSig = q.queryItemValue("sig");

            encryptedSig = this->decryptSignature(encryptedSig, js.buffer());

            if (!encryptedSig.isEmpty())
                url += "&signature=" + encryptedSig;

            item.title = title;
            item.orgUrl = this->getOrgURL(BASE_URL + id);
            item.url = url;
            item.id = id;
            item.dashmpd = dashmpd;
            item.quality = quality;

            ret.append(item);
        }
    }

    this->m_isPlayList = false;

    return ret;
}

QVector<YouTubeURLPicker::Item> YouTubeURLPicker::parsePlayListContent(const QString &url)
{
    QVector<YouTubeURLPicker::Item> ret;
    QStringList tmp;
    QUrl orgUrl(url);
    QUrlQuery query(orgUrl.query());
    QString id = query.queryItemValue("list");
    int index = 0;
    bool added = true;

    while (added)
    {
        HttpDownloader downloader;
        QXmlStreamReader xml;
        QBuffer buffer;
        QString xmlUrl = LIST_URL + id + "&index=" + QString::number(index);

        if (!buffer.open(QIODevice::ReadWrite))
            return ret;

        if (!downloader.download(xmlUrl, buffer))
            return ret;

        if (!buffer.reset())
            return ret;

        xml.setDevice(&buffer);

        added = false;

        while (!xml.atEnd())
        {
            if (!xml.readNextStartElement())
                continue;

            QString elem = xml.name().toString();

            if (elem != "encrypted_id")
                continue;

            tmp.append(xml.readElementText());
            added = true;
        }

        buffer.close();

        index += 100;
    }

    tmp.removeDuplicates();

    foreach (const QString vid, tmp)
    {
        YouTubeURLPicker::Item item;

        item.url = BASE_URL + vid;
        item.orgUrl = this->getOrgURL(item.url);
        item.id = vid;

        ret.append(item);
    }

    this->m_isPlayList = true;

    return ret;
}

QString YouTubeURLPicker::decryptSignature(const QString &sig, const QString &js) const
{
    QString ret;
    QString funcName;
    QRegExp match("\\.set\\(\\\"signature\\\",([a-zA-Z0-9$]+)\\(");
    QJSEngine script;

    if (match.indexIn(js) < 0)
        return ret;

    funcName = match.cap(1);

    QJSValue func;
    QJSValueList v;
    QString decCode;
    //"var Go=function(a){a=a.split(\"\");Fo.sH(a,2);Fo.TU(a,28);Fo.TU(a,44);Fo.TU(a,26);Fo.TU(a,40);Fo.TU(a,64);Fo.TR(a,26);Fo.sH(a,1);return a.join(\"\")};";
    QStringList funcPatterns;
    QString funcCode;
    //var Fo={TR:function(a){a.reverse()},TU:function(a,b){var c=a[0];a[0]=a[b%a.length];a[b]=c},sH:function(a,b){a.splice(0,b)}};
    QString helperPattern = "[ ,]%1=\\{[\\s\\w\\d;`~!@#$%^&*_,\\.\\-:=\\\"\\{\\}\\[\\]\\(\\)\\?]+\\};";
    QString helperCode;
    QString helperName;

    funcPatterns.append("[ ,]%1=function\\([\\w,]+\\)\\{[\\s\\w\\d;`~!@#$%^&*_,\\.\\-:=\\\"\\{\\}\\[\\]\\(\\)\\?]+\\}[ ,]");
    funcPatterns.append("%1=function\\([\\w,]+\\)\\{[\\s\\w\\d;`~!@#$%^&*_,\\.\\-:=\\\"\\{\\}\\[\\]\\(\\)\\?]+\\};");
    funcPatterns.append("function %1\\([\\w]+\\)\\{[\\s\\w\\d;`~!@#$%^&*_,\\.\\-:=\\\\\"\\{\\}\\[\\]\\(\\)\\?]+\\};");

    foreach (const QString &pattern, funcPatterns)
    {
        match.setPattern(pattern.arg(QRegExp::escape(funcName)));

        if (match.indexIn(js) < 0)
            continue;

        funcCode = match.cap();
        funcCode = funcCode.mid(funcCode.indexOf(funcName));

        if (funcCode.endsWith(','))
        {
            funcCode = funcCode.mid(0, funcCode.length() - 1);
            funcCode.append(";");
        }

        break;
    }

    match.setPattern(";(..)\\...\\(");

    if (match.indexIn(funcCode) < 0)
        return ret;

    helperName = match.cap(1);

    match.setPattern(helperPattern.arg(QRegExp::escape(helperName)));
    match.setMinimal(true);

    if (match.indexIn(js) < 0)
        return ret;

    helperCode = match.cap();

    decCode = funcCode + helperCode;

    v << sig;

    script.evaluate(decCode);
    func = script.evaluate(funcName);

    ret = func.call(v).toString();

    return ret;
}

bool YouTubeURLPicker::getVideoInfo(const QString &content, const QString &id, QString *ret) const
{
    bool ok = true;
    HttpDownloader d;
    QBuffer data;
    QUrlQuery query;
    bool isAgeGate = content.indexOf("player-age-gate-content") >= 0;
    bool isControversy = content.indexOf("verify_controversy") >= 0;

    if (isControversy || isAgeGate)
    {
        if (!d.download(EMBED_URL + id, data))
            return false;

        QString embed = data.buffer();
        QString js;
        QRegExp match("[\"]?js[\"]?\\s*:\\s*[\"]?[\\w\\d\\/\\\\\\.\\-]+[\"]?");

        if (match.indexIn(embed) >= 0)
            js = match.cap();

        match.setPattern("\"sts\"\\s*:\\s*(\\d+)");

        if (match.indexIn(embed) >= 0)
            query.addQueryItem("sts", match.cap(1));

        query.addQueryItem("video_id", id);
        query.addQueryItem("eurl", API_URL + id);

        if (!d.download(INFO_URL + query.query(), data))
            return false;

        query.setQuery(data.buffer());

        QString map = QUrl::fromPercentEncoding(query.queryItemValue("url_encoded_fmt_stream_map").toLatin1());
        QString fmts = QUrl::fromPercentEncoding(query.queryItemValue("adaptive_fmts").toLatin1());
        QString dashmpd = QUrl::fromPercentEncoding(query.queryItemValue("dashmpd").toLatin1());
        QString title;
        QString titlePattern = "<meta\\s+name\\s*=\\s*[\"]?title[\"]?\\s*content\\s*=\\s*[\"]?.*[^\"][\"]?\\s*>";

        if (isControversy)
        {
            QString redirected;
            QString titleUrl;

            match.setPattern("window.location\\s*=\\s*[\"]?(https?:.+[^\"])[\"]?<");

            if (match.indexIn(content) < 0)
                return false;

            titleUrl = match.cap(1);
            titleUrl = titleUrl.replace("\\/", "/");

            if (!d.download(titleUrl, data))
                return false;

            redirected = data.buffer();

            match.setPattern(titlePattern);
            match.setMinimal(true);

            if (match.indexIn(redirected) >= 0)
                title = match.cap();
        }
        else
        {
            match.setPattern(titlePattern);
            match.setMinimal(true);

            if (match.indexIn(content) >= 0)
                title = match.cap();
        }

        QString total;

        total = "<script>"
                "var ytplayer = ytplayer || {};ytplayer.config = "
                "{\"args\":{"
                "\"url_encoded_fmt_stream_map\":\"" + map + "\","
                "\"adaptive_fmts\":\"" + fmts + "\","
                "\"dashmpd\":\"" + dashmpd + "\""
                "}};"
                "</script>";

        *ret = total + js + title;
    }
    else
    {
       *ret = content;
    }

    return ok;
}

QString YouTubeURLPicker::getOrgURL(const QString &url) const
{
    QUrl orgUrl(url);
    QUrlQuery query(orgUrl.query());
    QString v = query.queryItemValue("v");

    query.clear();
    query.addQueryItem("v", v);

    orgUrl.setQuery(query);

    return orgUrl.toString();
}

bool YouTubeURLPicker::getJS(const QString &content, QBuffer *js) const
{
    QString jsUrl;
    HttpDownloader d;
    QRegExp jsMatch("[\"]?js[\"]?\\s*:\\s*[\"]?([\\w\\d\\/\\\\\\.\\-]+)[\"]?");

    if (jsMatch.indexIn(content) < 0)
        return false;

    jsUrl = jsMatch.cap(1);
    jsUrl = "https:" + jsUrl.replace("\\/", "/");

    return d.download(jsUrl, *js);
}

bool YouTubeURLPicker::getAudioStreamByMimeType(const QString &url, const QString &mimeType, QString *ret) const
{
    HttpDownloader d;
    QXmlStreamReader xml;
    QBuffer dash;
    QStringList videoMime = mimeType.split("/", QString::SkipEmptyParts);

    if (videoMime.length() != 2)
        return false;

    if (!dash.open(QIODevice::ReadWrite))
        return false;

    if (!d.download(url, dash))
        return false;

    if (!dash.reset())
        return false;

    xml.setDevice(&dash);

    QStringList audioUrls;

    while (!xml.atEnd())
    {
        if (!xml.readNextStartElement())
            continue;

        QString elem = xml.name().toString();

        if (elem != "AdaptationSet")
            continue;

        QXmlStreamAttributes attr = xml.attributes();
        QString mime = attr.value("mimeType").toString();
        QStringList audioMime = mime.split("/", QString::SkipEmptyParts);

        if (audioMime.length() != 2)
            continue;

        if (audioMime[0].toLower() != "audio" || videoMime[1].toLower() != audioMime[1].toLower())
            continue;

        while (!xml.atEnd())
        {
            if (!xml.readNextStartElement())
                continue;

            elem = xml.name().toString();

            if (elem == "AdaptationSet")
                break;

            if (elem != "BaseURL")
                continue;

            audioUrls.append(xml.readElementText());
        }
    }

    dash.close();

    *ret = audioUrls.last();

    return true;
}

bool YouTubeURLPicker::getJSon(const QString &content, QJsonObject *obj) const
{
    int index;
    int end;

    index = content.indexOf("var ytplayer");

    if (index < 0)
        return false;

    index = content.lastIndexOf("<script>", index);

    if (index < 0)
        return false;

    index = content.indexOf(">", index) + 1;

    if (index < 0)
        return false;

    end = content.indexOf("</script>", index);

    if (end < 0)
        return false;

    QString js = content.mid(index, end - index);
    QJSEngine script;
    QJSValue ytplayer;

    script.evaluate(js);
    ytplayer = script.evaluate("ytplayer.config");

    QVariant json = ytplayer.toVariant();
    QJsonDocument doc = QJsonDocument::fromVariant(json);

    if (!doc.isObject())
        return false;

    *obj = doc.object();

    return true;
}

void YouTubeURLPicker::toUrlQuery(const QStringList &list, QVector<QUrlQuery> *ret) const
{
    foreach (const QString &item, list)
        ret->append(QUrlQuery(item));
}

void YouTubeURLPicker::getFormatListFromM3U8(const QString &url, YouTubeURLPicker::Item &refItem, QVector<YouTubeURLPicker::Item> *ret)
{
    HttpDownloader d;
    QBuffer m3u8;
    QString m3u8Data;

    if (!d.download(url, m3u8))
        return;

    m3u8Data = m3u8.buffer();

    QTextStream stream(&m3u8Data);

    while (true)
    {
        QString line = stream.readLine().trimmed();

        if (line.isNull())
            break;

        if (line.isEmpty())
            continue;

        if (!line.startsWith("#EXT-X-STREAM-INF"))
            continue;

        int index = line.indexOf(":");
        int end;

        if (index < 0)
            continue;

        index++;

        QString bitrate;
        QString size;
        QString codec;
        QString videoUrl;
        QString data = line.mid(index);

        index = 0;

        while (true)
        {
            QString key;
            QString value;
            bool quota = false;

            end = data.indexOf("=", index);

            if (end < 0)
                break;

            key = data.mid(index, end - index).toUpper();

            end++;

            if (data[end] == '"' || data[end] == '\'')
            {
                QChar c = data[end];

                end++;

                index = end;
                end = data.indexOf(c, index);

                if (end < 0)
                {
                    end = data.indexOf(',', index);

                    if (end < 0)
                        end = data.length();
                }

                quota = true;
            }
            else
            {
                index = end;
                end = data.indexOf(',', index);

                if (end < 0)
                    end = data.length();
            }

            value = data.mid(index, end - index);
            index = end + 1;

            if (quota)
                index++;

            if (key == "BANDWIDTH")
                bitrate = value;
            else if (key == "CODECS")
                codec = value;
            else if (key == "RESOLUTION")
                size = value;
        }

        videoUrl = stream.readLine().trimmed();

        refItem.desc = QString("%1 %2 (%3)")
                .arg(Utils::sizeToString(bitrate.toInt()))
                .arg(size)
                .arg(codec);
        refItem.shortDesc = refItem.desc;

        refItem.quality = bitrate;
        refItem.url = videoUrl;

        ret->append(refItem);
    }
}

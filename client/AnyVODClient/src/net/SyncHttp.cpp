﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "SyncHttp.h"

#include <QMutexLocker>
#include <QNetworkAccessManager>
#include <QNetworkCookie>
#include <QDebug>

const QString SyncHttp::PROTOCOL = "http://";

SyncHttp::SyncHttp() :
    m_header(NULL),
    m_data(NULL),
    m_to(NULL),
    m_error(QNetworkReply::NoError)
{

}

SyncHttp::~SyncHttp()
{

}

QNetworkReply::NetworkError SyncHttp::error() const
{
    return this->m_error;
}

void SyncHttp::request(QNetworkRequest *header, const QByteArray *data, QIODevice *to)
{
    this->m_header = header;
    this->m_data = data;
    this->m_to = to;

    this->start();
    this->wait();

    this->m_header = NULL;
    this->m_data = NULL;
    this->m_to = NULL;
}

QUrl SyncHttp::getUrl(const QString &host, const QString &path) const
{
    QString url;

    if (!host.startsWith(PROTOCOL, Qt::CaseInsensitive))
        url = PROTOCOL;

    if (host.endsWith('/'))
        url += host.mid(0, host.length() - 1);
    else
        url += host;

    if (path.startsWith('/'))
        url += path;
    else
        url += '/' + path;

    return QUrl(url);
}

void SyncHttp::syncPost(const QString &host, const QString &path, const QByteArray &data, QIODevice *to)
{
    QMutexLocker locker(&this->m_lock);
    QNetworkRequest header(this->getUrl(host, path));

    header.setHeader(QNetworkRequest::ContentTypeHeader, "application/soap+xml; charset=utf-8");
    header.setRawHeader("User-Agent", "gSOAP/2.7");

    this->request(&header, &data, to);
}

void SyncHttp::syncGet(const QString &host, const QString &path, const QString &md5, QIODevice *to)
{
    QMutexLocker locker(&this->m_lock);
    QNetworkRequest header(this->getUrl(host, path));
    QList<QNetworkCookie> cookies;

    cookies.append(QNetworkCookie("moviekey", md5.toLatin1()));

    header.setHeader(QNetworkRequest::CookieHeader, QVariant::fromValue(cookies));
    header.setRawHeader("User-Agent", "GomPlayer 2, 1, 41, 5109 (KOR)");

    header.setAttribute(QNetworkRequest::CacheLoadControlAttribute, QNetworkRequest::AlwaysNetwork);

    this->request(&header, NULL, to);
}

void SyncHttp::received(QNetworkReply*)
{
    this->exit();
}

void SyncHttp::run()
{
    QNetworkAccessManager http;
    QNetworkReply *reply;

    connect(&http, SIGNAL(finished(QNetworkReply*)), SLOT(received(QNetworkReply*)), Qt::DirectConnection);

    if (this->m_data)
        reply = http.post(*this->m_header, *this->m_data);
    else
        reply = http.get(*this->m_header);

    this->exec();

    bool opened = this->m_to->isOpen();

    if (!opened)
        this->m_to->open(QIODevice::WriteOnly);

    this->m_to->write(reply->readAll());

    if (!opened)
        this->m_to->close();

    this->m_error = reply->error();

    delete reply;
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "VirtualFile.h"
#include "core/Utils.h"
#include "media/MediaPresenter.h"

#include <QDebug>

VirtualFile::VirtualFile(MediaPresenter *parent) :
    m_parent(parent)
{
    memset(&this->m_intCallback, 0, sizeof(this->m_intCallback));
    memset(&this->m_protocol, 0, sizeof(this->m_protocol));

    this->m_protocol.name = ANYVOD_PROTOCOL_NAME;
    this->m_protocol.url_open = VirtualFile::movieOpen;
    this->m_protocol.url_read = VirtualFile::movieRead;
    this->m_protocol.url_seek = VirtualFile::movieSeek;
    this->m_protocol.url_close = VirtualFile::movieClose;
}

VirtualFile::~VirtualFile()
{

}

URLProtocol* VirtualFile::getProtocol()
{
    return &this->m_protocol;
}

void VirtualFile::loadSubtitle(const QString &filePath)
{
    this->m_file.loadSubtitle(filePath, this->m_parent);
}

int VirtualFile::movieOpen(URLContext *h, const char *uri, int)
{
    QString realUri = QString::fromUtf8(uri);
    int index = realUri.lastIndexOf(Utils::FFMPEG_SEPARATOR);

    if (index != -1)
    {
        VirtualFile *me = NULL;
        uint64_t id = 0;
        QString parentID;

        parentID = realUri.mid(index + 1);
        id = parentID.toULongLong();
        me = (VirtualFile*)id;

        h->priv_data = me;

        realUri = realUri.mid(0, index);

        char proto[strlen(ANYVOD_PROTOCOL_NAME) + 1];
        char path[MAX_FILEPATH_CHAR_SIZE];

        av_url_split(proto, sizeof(proto), NULL, 0, NULL, 0, NULL, path, sizeof(path), realUri.toUtf8().constData());

        if (strcmp(proto, ANYVOD_PROTOCOL_NAME))
            return AVERROR(EINVAL);

        if (me->m_file.open(QString::fromUtf8(path), NULL))
        {
            me->m_intCallback = h->interrupt_callback;

            return 0;
        }
        else
        {
            return AVERROR(ENOENT);
        }
    }
    else
    {
        return AVERROR(EINVAL);
    }
}

int VirtualFile::movieRead(URLContext *h, uint8_t *buf, int size)
{
    VirtualFile *me = (VirtualFile*)h->priv_data;

    if (me->m_intCallback.callback && me->m_intCallback.callback(me->m_intCallback.opaque))
        return AVERROR_EXIT;

    return me->m_file.read(buf, size);
}

int64_t VirtualFile::movieSeek(URLContext *h, int64_t pos, int whence)
{
    VirtualFile *me = (VirtualFile*)h->priv_data;

    return me->m_file.seek(pos, whence);
}

int VirtualFile::movieClose(URLContext *h)
{
    VirtualFile *me = (VirtualFile*)h->priv_data;

    me->m_file.close();

    return 0;
}

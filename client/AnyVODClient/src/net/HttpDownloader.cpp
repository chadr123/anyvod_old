﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "HttpDownloader.h"

#include <QMutexLocker>
#include <QNetworkAccessManager>
#include <QDebug>

HttpDownloader::HttpDownloader() :
    m_to(NULL),
    m_error(QNetworkReply::NoError)
{

}

HttpDownloader::~HttpDownloader()
{

}

QNetworkReply::NetworkError HttpDownloader::error() const
{
    return this->m_error;
}

bool HttpDownloader::isRedirect() const
{
    return !this->m_redirectURL.isEmpty();
}

bool HttpDownloader::downloadInternal(const QString &path, QIODevice &to)
{
    QSslConfiguration sslConfig;

    this->m_header = QNetworkRequest(path);

    sslConfig = this->m_header.sslConfiguration();
    sslConfig.setPeerVerifyMode(QSslSocket::VerifyNone);

    this->m_header.setSslConfiguration(sslConfig);

    this->request(&to);

    if (this->isRedirect())
        return this->downloadInternal(this->m_redirectURL, to);

    return this->error() == QNetworkReply::NoError;
}

void HttpDownloader::request(QIODevice *to)
{
    this->m_to = to;

    this->start();
    this->wait();

    this->m_to = NULL;
}

bool HttpDownloader::download(const QString &path, QIODevice &to)
{
    QMutexLocker locker(&this->m_lock);

    return this->downloadInternal(path, to);
}

void HttpDownloader::received()
{
    this->exit();
}

void HttpDownloader::run()
{
    QNetworkAccessManager http;
    QNetworkReply *reply;

    reply = http.get(this->m_header);

    connect(reply, SIGNAL(finished()), SLOT(received()), Qt::DirectConnection);

    this->exec();

    bool opened = this->m_to->isOpen();

    if (!opened)
        this->m_to->open(QIODevice::WriteOnly);

    this->m_to->write(reply->readAll());

    if (!opened)
        this->m_to->close();

    this->m_redirectURL = reply->header(QNetworkRequest::LocationHeader).toString();
    this->m_error = reply->error();

    delete reply;
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

extern "C"
{
    #include <libavformat/avio.h>
    #include <libavformat/url.h>
}

#include "NetFile.h"

class MediaPresenter;

class VirtualFile
{
public:
    VirtualFile(MediaPresenter *parent);
    ~VirtualFile();

    URLProtocol* getProtocol();
    void loadSubtitle(const QString &filePath);

private:
    static int movieOpen(URLContext *h, const char *uri, int);
    static int movieRead(URLContext *h, uint8_t *buf, int size);
    static int64_t movieSeek(URLContext *h, int64_t pos, int whence);
    static int movieClose(URLContext *h);

private:
    URLProtocol m_protocol;
    NetFile m_file;
    MediaPresenter *m_parent;
    AVIOInterruptCB m_intCallback;
};

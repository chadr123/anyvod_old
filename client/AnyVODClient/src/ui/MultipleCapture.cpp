﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "MultipleCapture.h"
#include "ui_multiplecapture.h"
#include "Screen.h"
#include "MainWindow.h"
#include "media/MediaPlayer.h"
#include "core/Utils.h"

#include <QRect>
#include <QSize>
#include <QFileDialog>
#include <QDir>

const int MultipleCapture::CAPTURE_CHECK_TIME = 40;

MultipleCapture::MultipleCapture(Screen *screen, MediaPlayer *player, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MultipleCapture),
    m_screen(screen),
    m_player(player),
    m_captureWidthPrio(true)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    Ui::MultipleCapture *ctrl = this->ui;
    Screen::CaptureInfo info;
    QStringList exts = MainWindow::CAPTURE_FORMAT_LIST;
    int index = 0;

    this->m_screen->getCaptureInfo(&info);

    for (int i = 0; i < exts.count(); i++)
    {
        exts[i] = exts.at(i).toUpper();

        if (exts[i].toUpper() == info.ext.toUpper())
            index = i;
    }

    ctrl->savePath->setText(QDir::toNativeSeparators(info.savePath));
    ctrl->exts->addItems(exts);

    ctrl->exts->setCurrentIndex(index);

    if (info.quality == -1)
        ctrl->defaultQuality->setChecked(true);
    else
        ctrl->quality->setValue(info.quality);

    QRect picRect;
    QSize picSize;

    this->m_player->getPictureRect(&picRect);

    picSize = picRect.size();
    picSize.setHeight(picSize.height() - 1);
    picSize.setWidth(picSize.width() - 1);

    if (picSize.width() < 1)
        picSize.setWidth(1);

    if (picSize.height() < 1)
        picSize.setHeight(1);

    ctrl->captureWidth->setValue(picSize.width());
    ctrl->captureHeight->setValue(picSize.height());

    double curTime = this->m_player->getCurrentPosition();

    curTime += 60.0;

    ctrl->targetTime->setCurrentSection(QTimeEdit::SecondSection);
    ctrl->targetTime->setTime(Utils::getTime(curTime).time());

    ctrl->captureSubtitle->setChecked(this->m_player->isShowSubtitle());
    ctrl->captureDetail->setChecked(this->m_player->isShowDetail());

    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

MultipleCapture::~MultipleCapture()
{
    delete ui;
}

void MultipleCapture::on_changePath_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this, QString(), this->ui->savePath->text());

    if (!dir.isEmpty())
        this->ui->savePath->setText(dir);
}

void MultipleCapture::on_adjustResolution_clicked()
{
    Ui::MultipleCapture *ctrl = this->ui;
    double ratio = this->m_player->getAspectRatio(this->m_captureWidthPrio);
    int height;
    int width;

    if (this->m_captureWidthPrio)
    {
        width = ctrl->captureWidth->value();
        height = (int)rint(width * ratio);
    }
    else
    {
        height = ctrl->captureHeight->value();
        width = (int)rint(height * ratio);
    }

    ctrl->captureWidth->setValue(width);
    ctrl->captureHeight->setValue(height);
}

void MultipleCapture::on_startCapture_clicked()
{
    Screen::CaptureInfo info;
    Ui::MultipleCapture *ctrl = this->ui;

    info.captureCount = this->getTotalCount();

    if (info.captureCount <= 0)
    {
        Utils::informationMessageBox(this, trUtf8("캡쳐를 할 수 없습니다. 지정 시간 또는 지정 개수를 확인 해 주세요."));

        ctrl->startCapture->setEnabled(true);
        ctrl->stopCapture->setEnabled(false);

        return;
    }

    if (ctrl->sizeGroup->checkedButton() == ctrl->captureOrg)
    {
        info.captureOrg = true;
    }
    else if (ctrl->sizeGroup->checkedButton() == ctrl->captureCurrent)
    {
        QRect rect;
        QSize size;

        this->m_player->getPictureRect(&rect);

        size = rect.size();

        size.setWidth(size.width() - 1);
        size.setHeight(size.height() - 1);

        info.captureSize = size;
        info.captureOrg = false;
    }
    else if (ctrl->sizeGroup->checkedButton() == ctrl->captureUser)
    {
        QSize size(ctrl->captureWidth->value(), ctrl->captureHeight->value());

        info.captureSize = size;
        info.captureOrg = false;
    }
    else
    {
        info.captureOrg = true;
    }

    info.totalCount = info.captureCount;
    info.ext = ctrl->exts->currentText().toLower();

    if (ctrl->defaultQuality->isChecked())
        info.quality = -1;
    else
        info.quality = ctrl->quality->value();

    info.savePath = ctrl->savePath->text();

    this->m_player->showSubtitle(ctrl->captureSubtitle->isChecked());
    this->m_player->showDetail(ctrl->captureDetail->isChecked());

    this->m_screen->setCaptureInfo(info);
    this->m_screen->startCapture();

    this->startTimer(CAPTURE_CHECK_TIME);
}

void MultipleCapture::on_stopCapture_clicked()
{
    Screen::CaptureInfo info;

    this->m_screen->stopCapture();
    this->m_screen->getCaptureInfo(&info);

    if (info.paused)
        this->m_player->pause(true);
}

void MultipleCapture::on_captureWidth_editingFinished()
{
    this->m_captureWidthPrio = true;
}

void MultipleCapture::on_captureHeight_editingFinished()
{
    this->m_captureWidthPrio = false;
}

void MultipleCapture::timerEvent(QTimerEvent *event)
{
    Screen::CaptureInfo info;
    int count;

    this->m_screen->getCaptureInfo(&info);
    count = info.totalCount - info.captureCount;

    this->ui->currentStat->setText(QString::number(count));
    this->ui->totalStat->setText(QString::number(info.totalCount));

    if (info.captureCount <= 0)
    {
        this->killTimer(event->timerId());
        this->ui->stopCapture->click();
    }
}

void MultipleCapture::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

int MultipleCapture::getTotalCount() const
{
    Ui::MultipleCapture *ctrl = this->ui;

    if (ctrl->frameGroup->checkedButton() == ctrl->captureTime)
    {
        double targetTime = -ctrl->targetTime->time().msecsTo(Utils::ZERO_TIME) / 1000.0;
        double time = targetTime - this->m_player->getCurrentPosition();

        if (time < 0.0)
            return 0;

        MediaPresenter::Detail detail = this->m_player->getDetail();
        double frameRatio = detail.videoTotalFrame / detail.totalTime;

        return (int)time * frameRatio;
    }
    else
    {
        return ctrl->targetCount->value();
    }
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "MainWindow.h"
#include "ui_mainwindow.h"
#include "Screen.h"
#include "Login.h"
#include "Equalizer.h"
#include "OpenExternal.h"
#include "SkipRange.h"
#include "MultipleCapture.h"
#include "Shader.h"
#include "CustomShortcut.h"
#include "FileAssociation.h"
#include "UserAspectRatio.h"
#include "TextEncodeSetting.h"
#include "ServerSetting.h"
#include "ScreenExplorer.h"
#include "SubtitleDirectory.h"
#include "OpenDevice.h"
#include "OpenDTVScanChannel.h"
#include "ViewEPG.h"
#include "core/Settings.h"
#include "core/BuildNumber.h"
#include "core/Utils.h"
#include "media/MediaPlayer.h"
#include "net/Socket.h"
#include "net/YouTubeURLPicker.h"
#include "../../../../common/types.h"

#ifndef UNICODE
#define UNICODE
#endif

#ifndef _UNICODE
#define _UNICODE
#endif

#include <MediaInfoDLL/MediaInfoDLL.h>
#include <qmath.h>
#include <QUrl>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QMap>
#include <QList>
#include <QMouseEvent>
#include <QFontDatabase>
#include <QFileDialog>
#include <QShortcut>
#include <QApplication>
#include <QDesktopServices>
#include <QLocale>
#include <QMimeData>
#include <QTextCodec>

#ifdef Q_OS_WIN
#include <QWinTaskbarProgress>
#endif

#include <QDebug>

#include <zlib.h>
#include <bass/bass_fx.h>
#include <bass/bassmix.h>
#include <ass/ass.h>

extern "C"
{
    #include <libswscale/swscale.h>
    #include <libswresample/swresample.h>
    #include <libavdevice/avdevice.h>
    #include <libavfilter/avfilter.h>
}

using namespace std;

const QString MainWindow::APP_NAME = "AnyVOD";
const QString MainWindow::VERSION_TEMPLATE = "%1.%2.%3.%4";
const QString MainWindow::MOVIE_EXTS = QString::fromStdWString(VIDEO_EXTENSION);
const QString MainWindow::AUDIO_EXTS = QString::fromStdWString(AUDIO_EXTENSION);
const QString MainWindow::SUBTITLE_EXTS = QString::fromStdWString(SUBTITLE_EXTENSION);
const QString MainWindow::PLAYLIST_EXTS = QString::fromStdWString(PLAYLIST_EXTENSION);
const QString MainWindow::FONT_EXTS = QString::fromStdWString(FONT_EXTENSION);
const QStringList MainWindow::MOVIE_EXTS_LIST = MOVIE_EXTS.split(" ", QString::SkipEmptyParts);
const QStringList MainWindow::AUDIO_EXTS_LIST = AUDIO_EXTS.split(" ", QString::SkipEmptyParts);
const QStringList MainWindow::SUBTITLE_EXTS_LIST = SUBTITLE_EXTS.split(" ", QString::SkipEmptyParts);
const QStringList MainWindow::PLAYLIST_EXTS_LIST = PLAYLIST_EXTS.split(" ", QString::SkipEmptyParts);
const QStringList MainWindow::MEDIA_EXTS_LIST = MOVIE_EXTS_LIST + AUDIO_EXTS_LIST + PLAYLIST_EXTS_LIST;
const QStringList MainWindow::ALL_EXTS_LIST = MOVIE_EXTS_LIST + AUDIO_EXTS_LIST + SUBTITLE_EXTS_LIST + PLAYLIST_EXTS_LIST;
const QString MainWindow::CAPTURE_FORMAT = QString::fromStdWString(CAPTURE_FORMAT_EXTENSION);
const QStringList MainWindow::CAPTURE_FORMAT_LIST = CAPTURE_FORMAT.split(" ", QString::SkipEmptyParts);
const QStringList MainWindow::FONT_EXTS_LIST = FONT_EXTS.split(" ", QString::SkipEmptyParts);
const QSize MainWindow::DEFAULT_SCREEN_SIZE = QSize(640, 480);
const QString MainWindow::ORG_NAME = COMPANY;
const QString MainWindow::SHARE_MEM_KEY = APP_NAME + "__ShareMem__";
const QString MainWindow::LANG_PREFIX = "anyvod";
const QString MainWindow::LANG_DIR = "./languages";

QString MainWindow::OBJECT_NAME;

#ifdef Q_OS_MAC
const int MainWindow::CONTROL_BAR_HEIGHT = 216;
const int MainWindow::SCREEN_MINIMUM_SIZE = 16;
#else
const int MainWindow::CONTROL_BAR_HEIGHT = 196;
const int MainWindow::SCREEN_MINIMUM_SIZE = 10;
#endif
const QPoint MainWindow::DEFAULT_POSITION = QPoint(200, 100);
const int MainWindow::LOGIN_TIME = 500;
const int MainWindow::SHARE_MEM_TIME = 500;
const int MainWindow::SHARE_MEM_SIZE = 2048;
const int MainWindow::INIT_COMPLETE_TIME = 10;

#ifdef Q_OS_LINUX
const char MainWindow::DBUS_SERVICE[][40] =
{
    "org.freedesktop.ScreenSaver",
    "org.freedesktop.PowerManagement.Inhibit",
    "org.mate.SessionManager",
    "org.gnome.SessionManager",
};

const char MainWindow::DBUS_PATH[][33] =
{
    "/ScreenSaver",
    "/org/freedesktop/PowerManagement",
    "/org/mate/SessionManager",
    "/org/gnome/SessionManager",
};
#endif

MainWindow::MainWindow(const QStringList starting, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_player(NULL),
    m_buttonPressed(false),
    m_prevMaximized(false),
    m_currentPlayingIndex(-1),
    m_settings(QSettings::IniFormat, QSettings::UserScope, MainWindow::ORG_NAME, MainWindow::APP_NAME),
    m_lastScreenVisible(true),
    m_remoteFileList(this),
    m_prevRemoteFileListVisible(false),
    m_playList(this),
    m_prevPlayListVisible(false),
    m_playingMethod(AnyVODEnums::PM_TOTAL),
    m_spectrumTimerID(0),
    m_nonePlayingDescTimerID(0),
    m_loginTimerID(0),
    m_shareMemTimerID(0),
    m_initCompleteTimerID(0),
    m_isReady(false),
    m_isFullScreening(false),
    m_clearPlayList(false),
    m_popup(this),
    m_shareMem(SHARE_MEM_KEY),
    m_startingPaths(starting),
    m_remoteAddress(QString::fromWCharArray(DEFAULT_SERVER_ADDRESS)),
    m_remoteCommandPort(DEFAULT_NETWORK_PORT),
    m_remoteStreamPort(DEFAULT_NETWORK_STREAM_PORT)
{
    Ui::MainWindow *ctrl = this->ui;

    ctrl->setupUi(this);

    MainWindow::OBJECT_NAME = this->objectName();

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);

    this->m_systemSleepAssertionID = 0;
#endif

#ifdef Q_OS_LINUX
    if (!this->debusInit())
        this->debusDeinit();
#endif

    this->setupKey();
    this->setup3DMethod();
    this->setupSubtitle3DMethod();
    this->setupAnaglyphAlgorithm();
    this->setupDeinterlaceMethod();

    this->m_player = ctrl->screen->getPlayer();
    this->m_shareMem.create(SHARE_MEM_SIZE);
    this->m_shareMemTimerID = this->startTimer(SHARE_MEM_TIME);

    this->loadSettings();

    ctrl->stop->setEnabled(false);
    ctrl->back->setEnabled(false);
    ctrl->forw->setEnabled(false);
    ctrl->pause->setEnabled(false);
    ctrl->prev->setEnabled(false);
    ctrl->next->setEnabled(false);
    ctrl->mute->setEnabled(false);

    if (this->m_playList.exist())
    {
        this->updateNavigationButtons();
        ctrl->resume->setEnabled(true);
        ctrl->mute->setEnabled(true);
    }
    else
    {
        ctrl->resume->setEnabled(false);
        ctrl->mute->setEnabled(false);
    }

    ctrl->pause->setVisible(false);

    ctrl->volume->setRange(0, this->m_player->getMaxVolume());
    ctrl->volume->setValue(this->m_player->getVolume());

    ctrl->trackBar->setMinimum(0);
    ctrl->trackBar->setMaximum(0);

    this->resetWindowTitle();
    this->setAcceptDrops(true);

    QMap<QString, QString> list;

    if (Utils::parsePair(ENV_FILENAME, &list))
    {
        QMap<QString, QString>::iterator i = list.begin();

        for (; i != list.end(); i++)
        {
            QString key = i.key().toLower();

            if (key == ENV_FONT_PATH_NAME)
                this->m_env.fontPath = i.value();
            else if (key == ENV_FONT_SIZE_NAME)
                this->m_env.fontSize = i.value().toUShort();
            else if (key == ENV_FONT_SUBTITLE_OUTLINE_SIZE_NAME)
                this->m_env.subtitleOutlineSize = i.value().toUShort();
            else if (key == ENV_SKIN_NAME)
                this->m_env.skin = i.value();
            else if (key == ENV_SHADER_NAME)
                this->m_env.shader = i.value();
        }
    }

    int id = QFontDatabase::addApplicationFont(this->m_env.fontPath);

    if (id >= 0)
    {
        QStringList families = QFontDatabase::applicationFontFamilies(id);

        if (families.count() > 0)
            this->m_env.fontFamily = families[0];
    }

    this->m_player->setASSFontFamily(this->m_env.fontFamily);

    QFile skin(this->m_env.skin);

    if (skin.open(QFile::ReadOnly))
    {
        QTextStream stream(&skin);

        qApp->setStyleSheet(stream.readAll());
    }
    else
    {
        Utils::informationMessageBox(this, trUtf8("스킨을 읽을 수 없습니다 (%1)").arg(this->m_env.skin));
    }

    ctrl->audioSpec->setText(QString());
    ctrl->audioSpecWidget->setVisible(false);

    ctrl->audioOptionDesc->setVisible(false);
    ctrl->audioBuffering->setText(QString());

    ctrl->audioSubtitle->setVisible(!this->m_lastScreenVisible);
    ctrl->audioSubtitle1->setText(MainWindow::APP_NAME + " " + this->getVersion());
    ctrl->audioSubtitle2->setText(QString());
    ctrl->audioSubtitle3->setText(QString());

    ctrl->screen->initTextures(false);

    this->setMouseTracking(true);
    this->setFocus();
    this->setEnglishIME();

    this->m_player->setASSFontPath(this->m_env.fontPath);

    this->m_initCompleteTimerID = this->startTimer(INIT_COMPLETE_TIME);
}

MainWindow::~MainWindow()
{
#ifdef Q_OS_LINUX
    this->debusDeinit();
#endif
    this->deleteKey();
    delete ui;
}

void MainWindow::setupKey()
{
    this->m_shortcut[AnyVODEnums::SK_LOGIN] = ShortcutItem(trUtf8("로그인"), new QShortcut(QKeySequence(Qt::Key_I), this, SLOT(login()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_LOGOUT] = ShortcutItem(trUtf8("로그아웃"), new QShortcut(QKeySequence(Qt::Key_O | Qt::AltModifier), this, SLOT(logout()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_MOST_TOP] = ShortcutItem(trUtf8("항상 위"), new QShortcut(QKeySequence(Qt::Key_T), this, SLOT(mostTop()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_OPEN_REMOTE_FILE_LIST] = ShortcutItem(trUtf8("원격 파일 목록 열기"), new QShortcut(QKeySequence(Qt::Key_R), this, SLOT(openRemoteFileList()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_OPEN_EXTERNAL] = ShortcutItem(trUtf8("외부 열기"), new QShortcut(QKeySequence(Qt::Key_E), this, SLOT(openExternal()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_OPEN] = ShortcutItem(trUtf8("열기"), new QShortcut(QKeySequence(Qt::Key_P), this, SLOT(open()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_CLOSE] = ShortcutItem(trUtf8("닫기"), new QShortcut(QKeySequence(Qt::Key_C | Qt::AltModifier), this, SLOT(close()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_EXIT] = ShortcutItem(trUtf8("종료"), new QShortcut(QKeySequence(Qt::Key_F4 | Qt::AltModifier), this, SLOT(exit()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_AUDIO_NORMALIZE] = ShortcutItem(trUtf8("노멀라이저 사용"), new QShortcut(QKeySequence(Qt::Key_N), this, SLOT(audioNormalize()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_AUDIO_EQUALIZER] = ShortcutItem(trUtf8("이퀄라이저 사용"), new QShortcut(QKeySequence(Qt::Key_Q), this, SLOT(audioEqualizer()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_EQUALIZER_SETTING] = ShortcutItem(trUtf8("이퀄라이저 설정"), new QShortcut(QKeySequence(Qt::Key_E | Qt::AltModifier), this, SLOT(equalizerSetting()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_OPEN_PLAY_LIST] = ShortcutItem(trUtf8("재생 목록 열기"), new QShortcut(QKeySequence(Qt::Key_L), this, SLOT(openPlayList()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_RESET_SUBTITLE_POSITION] = ShortcutItem(trUtf8("자막 기본 위치"), new QShortcut(QKeySequence(Qt::Key_Home), this, SLOT(resetSubtitlePosition()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_UP_SUBTITLE_POSITION] = ShortcutItem(trUtf8("자막 위치 위로"), new QShortcut(QKeySequence(Qt::Key_Up | Qt::AltModifier), this, SLOT(upSubtitlePosition()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_DOWN_SUBTITLE_POSITION] = ShortcutItem(trUtf8("자막 위치 아래로"), new QShortcut(QKeySequence(Qt::Key_Down | Qt::AltModifier), this, SLOT(downSubtitlePosition()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_LEFT_SUBTITLE_POSITION] = ShortcutItem(trUtf8("자막 위치 왼쪽으로"), new QShortcut(QKeySequence(Qt::Key_Left | Qt::AltModifier), this, SLOT(leftSubtitlePosition()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_RIGHT_SUBTITLE_POSITION] = ShortcutItem(trUtf8("자막 위치 오른쪽으로"), new QShortcut(QKeySequence(Qt::Key_Right | Qt::AltModifier), this, SLOT(rightSubtitlePosition()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_TOGGLE] = ShortcutItem(trUtf8("재생 / 일시정지"), new QShortcut(QKeySequence(Qt::Key_Space), this, SLOT(toggle()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_TOGGLE_PLAY_MEDIA] = ShortcutItem(trUtf8("재생 / 일시정지(재생)"), new QShortcut(QKeySequence(Qt::Key_MediaPlay), this, SLOT(toggle()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_TOGGLE_PAUSE_MEDIA] = ShortcutItem(trUtf8("재생 / 일시정지(일시정지)"), new QShortcut(QKeySequence(Qt::Key_MediaPause), this, SLOT(toggle()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_TOGGLE_PLAY_PAUSE_MEDIA] = ShortcutItem(trUtf8("재생 / 일시정지(토글)"), new QShortcut(QKeySequence(Qt::Key_MediaTogglePlayPause), this, SLOT(toggle()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_FULL_SCREEN_RETURN] = ShortcutItem(trUtf8("전체 화면"), new QShortcut(QKeySequence(Qt::Key_Return), this, SLOT(fullScreen()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_FULL_SCREEN_ENTER] = ShortcutItem(trUtf8("전체 화면(추가)"), new QShortcut(QKeySequence(Qt::Key_Enter), this, SLOT(fullScreen()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REWIND_5] = ShortcutItem(trUtf8("5초 뒤로"), new QShortcut(QKeySequence(Qt::Key_Left), this, SLOT(rewind5()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_FORWARD_5] = ShortcutItem(trUtf8("5초 앞으로"), new QShortcut(QKeySequence(Qt::Key_Right), this, SLOT(forward5()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REWIND_30] = ShortcutItem(trUtf8("30초 뒤로"), new QShortcut(QKeySequence(Qt::Key_Left | Qt::ControlModifier), this, SLOT(rewind30()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_FORWARD_30] = ShortcutItem(trUtf8("30초 앞으로"), new QShortcut(QKeySequence(Qt::Key_Right | Qt::ControlModifier), this, SLOT(forward30()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REWIND_60] = ShortcutItem(trUtf8("1분 뒤로"), new QShortcut(QKeySequence(Qt::Key_Left | Qt::ShiftModifier), this, SLOT(rewind60()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_FORWARD_60] = ShortcutItem(trUtf8("1분 앞으로"), new QShortcut(QKeySequence(Qt::Key_Right | Qt::ShiftModifier), this, SLOT(forward60()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_GOTO_BEGIN] = ShortcutItem(trUtf8("처음으로 이동"), new QShortcut(QKeySequence(Qt::Key_Backspace), this, SLOT(gotoBegin()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_PREV] = ShortcutItem(trUtf8("이전 파일"), new QShortcut(QKeySequence(Qt::Key_PageUp), this, SLOT(prev()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_PREV_MEDIA] = ShortcutItem(trUtf8("이전 파일(추가)"), new QShortcut(QKeySequence(Qt::Key_MediaPrevious), this, SLOT(prev()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_NEXT] = ShortcutItem(trUtf8("다음 파일"), new QShortcut(QKeySequence(Qt::Key_PageDown), this, SLOT(next()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_NEXT_MEDIA] = ShortcutItem(trUtf8("다음 파일(추가)"), new QShortcut(QKeySequence(Qt::Key_MediaNext), this, SLOT(next()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_PLAY_ORDER] = ShortcutItem(trUtf8("재생 순서 순차 선택"), new QShortcut(QKeySequence(Qt::Key_P | Qt::AltModifier), this, SLOT(playOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SUBTITLE_TOGGLE] = ShortcutItem(trUtf8("보이기"), new QShortcut(QKeySequence(Qt::Key_H | Qt::AltModifier), this, SLOT(subtitleToggle()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_PREV_SUBTITLE_SYNC] = ShortcutItem(trUtf8("싱크 느리게"), new QShortcut(QKeySequence(Qt::Key_Comma), this, SLOT(prevSubtitleSync()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_NEXT_SUBTITLE_SYNC] = ShortcutItem(trUtf8("싱크 빠르게"), new QShortcut(QKeySequence(Qt::Key_Period), this, SLOT(nextSubtitleSync()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_RESET_SUBTITLE_SYNC] = ShortcutItem(trUtf8("싱크 초기화"), new QShortcut(QKeySequence(Qt::Key_Slash), this, SLOT(resetSubtitleSync()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SUBTITLE_LANGUAGE_ORDER] = ShortcutItem(trUtf8("자막 언어 순차 선택"), new QShortcut(QKeySequence(Qt::Key_L | Qt::AltModifier), this, SLOT(subtitleLanguageOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_VOLUME_UP] = ShortcutItem(trUtf8("소리 크게"), new QShortcut(QKeySequence(Qt::Key_Up), this, SLOT(volumeUp()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_VOLUME_UP_MEDIA] = ShortcutItem(trUtf8("소리 크게(추가)"), new QShortcut(QKeySequence(Qt::Key_VolumeUp), this, SLOT(volumeUp()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_VOLUME_DOWN] = ShortcutItem(trUtf8("소리 작게"), new QShortcut(QKeySequence(Qt::Key_Down), this, SLOT(volumeDown()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_VOLUME_DOWN_MEDIA] = ShortcutItem(trUtf8("소리 작게(추가)"), new QShortcut(QKeySequence(Qt::Key_VolumeDown), this, SLOT(volumeDown()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_MUTE] = ShortcutItem(trUtf8("음소거"), new QShortcut(QKeySequence(Qt::Key_M), this, SLOT(mute()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_MUTE_MEDIA] = ShortcutItem(trUtf8("음소거(추가)"), new QShortcut(QKeySequence(Qt::Key_VolumeMute), this, SLOT(mute()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_AUDIO_ORDER] = ShortcutItem(trUtf8("음성 순차 선택"), new QShortcut(QKeySequence(Qt::Key_V | Qt::AltModifier), this, SLOT(audioOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_INFO] = ShortcutItem(trUtf8("정보"), new QShortcut(QKeySequence(Qt::Key_F1), this, SLOT(info()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_DETAIL] = ShortcutItem(trUtf8("재생 정보"), new QShortcut(QKeySequence(Qt::Key_F2), this, SLOT(detail()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SHOW_CONTROL_BAR] = ShortcutItem(trUtf8("컨트롤바 보이기"), new QShortcut(QKeySequence(Qt::Key_Tab), this, SLOT(showControlBar()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_INC_OPAQUE] = ShortcutItem(trUtf8("투명도 증가"), new QShortcut(QKeySequence(Qt::Key_X), this, SLOT(incOpaque()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_DEC_OPAQUE] = ShortcutItem(trUtf8("투명도 감소"), new QShortcut(QKeySequence(Qt::Key_Z), this, SLOT(decOpaque()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_MAX_OPAQUE] = ShortcutItem(trUtf8("최대 투명도"), new QShortcut(QKeySequence(Qt::Key_X | Qt::AltModifier), this, SLOT(maxOpaque()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_MIN_OPAQUE] = ShortcutItem(trUtf8("최소 투명도"), new QShortcut(QKeySequence(Qt::Key_Z | Qt::AltModifier), this, SLOT(minOpaque()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER] = ShortcutItem(trUtf8("디인터레이스 순차 선택"), new QShortcut(QKeySequence(Qt::Key_D | Qt::AltModifier), this, SLOT(deinterlaceMethodOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER] = ShortcutItem(trUtf8("디인터레이스 알고리즘 순차 선택"), new QShortcut(QKeySequence(Qt::Key_A | Qt::AltModifier), this, SLOT(deinterlaceAlgorithmOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_PREV_AUDIO_SYNC] = ShortcutItem(trUtf8("싱크 느리게"), new QShortcut(QKeySequence(Qt::Key_A), this, SLOT(prevAudioSync()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_NEXT_AUDIO_SYNC] = ShortcutItem(trUtf8("싱크 빠르게"), new QShortcut(QKeySequence(Qt::Key_S), this, SLOT(nextAudioSync()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_RESET_AUDIO_SYNC] = ShortcutItem(trUtf8("싱크 초기화"), new QShortcut(QKeySequence(Qt::Key_D), this, SLOT(resetAudioSync()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER] = ShortcutItem(trUtf8("자막 가로 정렬 방법 순차 선택"), new QShortcut(QKeySequence(Qt::Key_N | Qt::AltModifier), this, SLOT(subtitleHAlignOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REPEAT_RANGE_START] = ShortcutItem(trUtf8("구간 반복 시작"), new QShortcut(QKeySequence(Qt::Key_BracketLeft), this, SLOT(repeatRangeStart()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REPEAT_RANGE_END] = ShortcutItem(trUtf8("구간 반복 끝"), new QShortcut(QKeySequence(Qt::Key_BracketRight), this, SLOT(repeatRangeEnd()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REPEAT_RANGE_ENABLE] = ShortcutItem(trUtf8("구간 반복 활성화"), new QShortcut(QKeySequence(Qt::Key_Backslash), this, SLOT(repeatRangeEnable()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SEEK_KEYFRAME] = ShortcutItem(trUtf8("키프레임 단위로 이동"), new QShortcut(QKeySequence(Qt::Key_K | Qt::AltModifier), this, SLOT(seekKeyFrame()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SKIP_OPENING] = ShortcutItem(trUtf8("오프닝 스킵 사용"), new QShortcut(QKeySequence(Qt::Key_R | Qt::AltModifier), this, SLOT(skipOpening()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SKIP_ENDING] = ShortcutItem(trUtf8("엔딩 스킵 사용"), new QShortcut(QKeySequence(Qt::Key_T | Qt::AltModifier), this, SLOT(skipEnding()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USE_SKIP_RANGE] = ShortcutItem(trUtf8("재생 스킵 사용"), new QShortcut(QKeySequence(Qt::Key_Y | Qt::AltModifier), this, SLOT(useSkipRange()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SKIP_RANGE_SETTING] = ShortcutItem(trUtf8("재생 스킵 설정"), new QShortcut(QKeySequence(Qt::Key_Y), this, SLOT(openSkipRange()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER] = ShortcutItem(trUtf8("캡쳐 확장자 순차 선택"), new QShortcut(QKeySequence(Qt::Key_E | Qt::ControlModifier), this, SLOT(captureExtOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_CAPTURE_SINGLE] = ShortcutItem(trUtf8("한 장 캡쳐"), new QShortcut(QKeySequence(Qt::Key_C | Qt::ControlModifier), this, SLOT(captureSingle()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_CAPTURE_MULTIPLE] = ShortcutItem(trUtf8("여러 장 캡쳐"), new QShortcut(QKeySequence(Qt::Key_C | Qt::ControlModifier | Qt::AltModifier), this, SLOT(captureMultiple()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_PREV_FRAME] = ShortcutItem(trUtf8("이전 프레임으로 이동"), new QShortcut(QKeySequence(Qt::Key_F), this, SLOT(prevFrame()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_NEXT_FRAME] = ShortcutItem(trUtf8("다음 프레임으로 이동"), new QShortcut(QKeySequence(Qt::Key_G), this, SLOT(nextFrame()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SLOWER_PLAYBACK] = ShortcutItem(trUtf8("재생 속도 느리게"), new QShortcut(QKeySequence(Qt::Key_C), this, SLOT(slowerPlayback()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_FASTER_PLAYBACK] = ShortcutItem(trUtf8("재생 속도 빠르게"), new QShortcut(QKeySequence(Qt::Key_V), this, SLOT(fasterPlayback()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_NORMAL_PLAYBACK] = ShortcutItem(trUtf8("재생 속도 초기화"), new QShortcut(QKeySequence(Qt::Key_B), this, SLOT(normalPlayback()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_RESET_VIDEO_ATTRIBUTE] = ShortcutItem(trUtf8("영상 속성 초기화"), new QShortcut(QKeySequence(Qt::Key_1), this, SLOT(resetVideoAttribute()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_BRIGHTNESS_DOWN] = ShortcutItem(trUtf8("영상 밝기 감소"), new QShortcut(QKeySequence(Qt::Key_2), this, SLOT(brightnessDown()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_BRIGHTNESS_UP] = ShortcutItem(trUtf8("영상 밝기 증가"), new QShortcut(QKeySequence(Qt::Key_3), this, SLOT(brightnessUp()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SATURATION_DOWN] = ShortcutItem(trUtf8("영상 채도 감소"), new QShortcut(QKeySequence(Qt::Key_4), this, SLOT(saturationDown()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SATURATION_UP] = ShortcutItem(trUtf8("영상 채도 증가"), new QShortcut(QKeySequence(Qt::Key_5), this, SLOT(saturationUp()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_HUE_DOWN] = ShortcutItem(trUtf8("영상 색상 감소"), new QShortcut(QKeySequence(Qt::Key_6), this, SLOT(hueDown()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_HUE_UP] = ShortcutItem(trUtf8("영상 색상 증가"), new QShortcut(QKeySequence(Qt::Key_7), this, SLOT(hueUp()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_CONTRAST_DOWN] = ShortcutItem(trUtf8("영상 대비 감소"), new QShortcut(QKeySequence(Qt::Key_8), this, SLOT(contrastDown()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_CONTRAST_UP] = ShortcutItem(trUtf8("영상 대비 증가"), new QShortcut(QKeySequence(Qt::Key_9), this, SLOT(contrastUp()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_OPEN_SHADER_COMPOSITER] = ShortcutItem(trUtf8("셰이더 조합"), new QShortcut(QKeySequence(Qt::Key_K), this, SLOT(openShaderCompositer()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_LOWER_MUSIC] = ShortcutItem(trUtf8("음악 줄임"), new QShortcut(QKeySequence(Qt::Key_M | Qt::ShiftModifier), this, SLOT(lowerMusic()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_LOWER_VOICE] = ShortcutItem(trUtf8("음성 줄임"), new QShortcut(QKeySequence(Qt::Key_V | Qt::ShiftModifier), this, SLOT(lowerVoice()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_HIGHER_VOICE] = ShortcutItem(trUtf8("음성 강조"), new QShortcut(QKeySequence(Qt::Key_H | Qt::ShiftModifier), this, SLOT(higherVoice()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SHARPLY] = ShortcutItem(trUtf8("날카롭게"), new QShortcut(QKeySequence(Qt::Key_A | Qt::ControlModifier), this, SLOT(sharply()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SHARPEN] = ShortcutItem(trUtf8("선명하게"), new QShortcut(QKeySequence(Qt::Key_S | Qt::ControlModifier), this, SLOT(sharpen()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SOFTEN] = ShortcutItem(trUtf8("부드럽게"), new QShortcut(QKeySequence(Qt::Key_D | Qt::ControlModifier), this, SLOT(soften()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_LEFT_RIGHT_INVERT] = ShortcutItem(trUtf8("좌우 반전"), new QShortcut(QKeySequence(Qt::Key_F | Qt::ControlModifier), this, SLOT(leftRightInvert()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_TOP_BOTTOM_INVERT] = ShortcutItem(trUtf8("상하 반전"), new QShortcut(QKeySequence(Qt::Key_G | Qt::ControlModifier), this, SLOT(topBottomInvert()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_ADD_TO_PLATLIST] = ShortcutItem(trUtf8("재생 목록에 추가"), new QShortcut(QKeySequence(Qt::Key_P | Qt::ControlModifier), this, SLOT(addToPlayList()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_INC_SUBTITLE_OPAQUE] = ShortcutItem(trUtf8("투명도 증가"), new QShortcut(QKeySequence(Qt::Key_X | Qt::ControlModifier), this, SLOT(incSubtitleOpaque()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_DEC_SUBTITLE_OPAQUE] = ShortcutItem(trUtf8("투명도 감소"), new QShortcut(QKeySequence(Qt::Key_Z | Qt::ControlModifier), this, SLOT(decSubtitleOpaque()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_OPEN_CAPTURE_DIRECTORY] = ShortcutItem(trUtf8("저장 디렉토리 열기"), new QShortcut(QKeySequence(), this, SLOT(openCaptureSaveDir()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY] = ShortcutItem(trUtf8("저장 디렉토리 설정"), new QShortcut(QKeySequence(), this, SLOT(selectCaptureSaveDir()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_OPEN_CUSTOM_SHORTCUTS] = ShortcutItem(trUtf8("단축 키 설정"), new QShortcut(QKeySequence(), this, SLOT(customShortcut()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_AUDIO_DEVICE_ORDER] = ShortcutItem(trUtf8("소리 출력 장치 순차 선택"), new QShortcut(QKeySequence(), this, SLOT(audioDeviceOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_FILE_ASSOCIATION] = ShortcutItem(trUtf8("확장자 연결"), new QShortcut(QKeySequence(), this, SLOT(fileAssociation()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_ENABLE_SEARCH_SUBTITLE] = ShortcutItem(trUtf8("자막 찾기 켜기"), new QShortcut(QKeySequence(Qt::Key_S | Qt::ControlModifier | Qt::AltModifier), this, SLOT(enableSearchSubtitle()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_ENABLE_SEARCH_LYRICS] = ShortcutItem(trUtf8("가사 찾기 켜기"), new QShortcut(QKeySequence(Qt::Key_L | Qt::ControlModifier | Qt::AltModifier), this, SLOT(enableSearchLyrics()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER] = ShortcutItem(trUtf8("자막 세로 정렬 방법 순차 선택"), new QShortcut(QKeySequence(Qt::Key_N | Qt::ControlModifier | Qt::AltModifier), this, SLOT(subtitleVAlignOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_INC_SUBTITLE_SIZE] = ShortcutItem(trUtf8("자막 크기 증가"), new QShortcut(QKeySequence(Qt::Key_Up | Qt::ControlModifier), this, SLOT(incSubtitleSize()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_DEC_SUBTITLE_SIZE] = ShortcutItem(trUtf8("자막 크기 감소"), new QShortcut(QKeySequence(Qt::Key_Down | Qt::ControlModifier), this, SLOT(decSubtitleSize()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_RESET_SUBTITLE_SIZE] = ShortcutItem(trUtf8("자막 크기 초기화"), new QShortcut(QKeySequence(Qt::Key_Home | Qt::ControlModifier), this, SLOT(resetSubtitleSize()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_RESET_SUBTITLE_OPAQUE] = ShortcutItem(trUtf8("투명도 초기화"), new QShortcut(QKeySequence(Qt::Key_C | Qt::ShiftModifier), this, SLOT(resetSubtitleOpaque()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USER_ASPECT_RATIO_ORDER] = ShortcutItem(trUtf8("화면 비율 순차 선택"), new QShortcut(QKeySequence(Qt::Key_R | Qt::ControlModifier), this, SLOT(userAspectRatioOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USER_ASPECT_RATIO] = ShortcutItem(trUtf8("화면 비율 사용자 지정"), new QShortcut(QKeySequence(), this, SLOT(userAspectRatio()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SCREEN_SIZE_HALF] = ShortcutItem(trUtf8("화면 크기 0.5배"), new QShortcut(QKeySequence(Qt::Key_1 | Qt::AltModifier), this, SLOT(screenSizeHalf()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SCREEN_SIZE_NORMAL] = ShortcutItem(trUtf8("화면 크기 1배"), new QShortcut(QKeySequence(Qt::Key_2 | Qt::AltModifier), this, SLOT(screenSizeNormal()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SCREEN_SIZE_NORMAL_HALF] = ShortcutItem(trUtf8("화면 크기 1.5배"), new QShortcut(QKeySequence(Qt::Key_3 | Qt::AltModifier), this, SLOT(screenSizeNormalHalf()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SCREEN_SIZE_DOUBLE] = ShortcutItem(trUtf8("화면 크기 2배"), new QShortcut(QKeySequence(Qt::Key_4 | Qt::AltModifier), this, SLOT(screenSizeDouble()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USE_HW_DECODER] = ShortcutItem(trUtf8("하드웨어 디코더 사용"), new QShortcut(QKeySequence(), this, SLOT(useHWDecoder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USE_SPDIF] = ShortcutItem(trUtf8("S/PDIF 출력 사용"), new QShortcut(QKeySequence(Qt::Key_S | Qt::AltModifier | Qt::ShiftModifier), this, SLOT(useSPDIF()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SPDIF_USER_SAMPLE_RATE_ORDER] = ShortcutItem(trUtf8("S/PDIF 샘플 속도 순차 선택"), new QShortcut(QKeySequence(), this, SLOT(userSPDIFSampleRateOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USE_PBO] = ShortcutItem(trUtf8("고속 렌더링 사용"), new QShortcut(QKeySequence(), this, SLOT(usePBO()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_3D_VIDEO_METHOD_ORDER] = ShortcutItem(trUtf8("3D 영상 출력 방법 순차 선택"), new QShortcut(QKeySequence(), this, SLOT(method3DOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USE_VSYNC] = ShortcutItem(trUtf8("수직 동기화 사용"), new QShortcut(QKeySequence(), this, SLOT(useVSync()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_PREV_CHAPTER] = ShortcutItem(trUtf8("이전 챕터로 이동"), new QShortcut(QKeySequence(Qt::Key_PageUp | Qt::ControlModifier), this, SLOT(prevChapter()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_NEXT_CHAPTER] = ShortcutItem(trUtf8("다음 챕터로 이동"), new QShortcut(QKeySequence(Qt::Key_PageDown | Qt::ControlModifier), this, SLOT(nextChapter()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_CLOSE_EXTERNAL_SUBTITLE] = ShortcutItem(trUtf8("외부 닫기"), new QShortcut(QKeySequence(), this, SLOT(closeExternalSubtitle()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_CLEAR_PLAYLIST] = ShortcutItem(trUtf8("종료 시 재생 목록 비움"), new QShortcut(QKeySequence(), this, SLOT(clearPlayList()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SHOW_ALBUM_JACKET] = ShortcutItem(trUtf8("앨범 자켓 보기"), new QShortcut(QKeySequence(Qt::Key_J | Qt::ControlModifier), this, SLOT(showAlbumJacket()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_LAST_PLAY] = ShortcutItem(trUtf8("재생 위치 기억"), new QShortcut(QKeySequence(), this, SLOT(lastPlay()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_TEXT_ENCODING] = ShortcutItem(trUtf8("인코딩 설정"), new QShortcut(QKeySequence(), this, SLOT(textEncoding()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SERVER_SETTING] = ShortcutItem(trUtf8("서버 설정"), new QShortcut(QKeySequence(), this, SLOT(serverSetting()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SAVE_SUBTITLE] = ShortcutItem(trUtf8("저장"), new QShortcut(QKeySequence(), this, SLOT(saveSubtitle()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SAVE_AS_SUBTITLE] = ShortcutItem(trUtf8("다른 이름으로 저장"), new QShortcut(QKeySequence(), this, SLOT(saveAsSubtitle()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER] = ShortcutItem(trUtf8("S/PDIF 소리 출력 장치 순차 선택"), new QShortcut(QKeySequence(), this, SLOT(audioSPDIFDeviceOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SCREEN_EXPLORER] = ShortcutItem(trUtf8("장면 탐색"), new QShortcut(QKeySequence(Qt::Key_E | Qt::ControlModifier | Qt::AltModifier), this, SLOT(openScreenExplorer()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REPEAT_RANGE_START_BACK_100MS] = ShortcutItem(trUtf8("시작 위치 0.1초 뒤로 이동"), new QShortcut(QKeySequence(Qt::Key_BracketLeft | Qt::ControlModifier), this, SLOT(repeatRangeStartBack100MS()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REPEAT_RANGE_START_FORW_100MS] = ShortcutItem(trUtf8("시작 위치 0.1초 앞으로 이동"), new QShortcut(QKeySequence(Qt::Key_BracketRight | Qt::ControlModifier), this, SLOT(repeatRangeStartForw100MS()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REPEAT_RANGE_END_BACK_100MS] = ShortcutItem(trUtf8("끝 위치 0.1초 뒤로 이동"), new QShortcut(QKeySequence(Qt::Key_BracketLeft | Qt::AltModifier), this, SLOT(repeatRangeEndBack100MS()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REPEAT_RANGE_END_FORW_100MS] = ShortcutItem(trUtf8("끝 위치 0.1초 앞으로 이동"), new QShortcut(QKeySequence(Qt::Key_BracketRight | Qt::AltModifier), this, SLOT(repeatRangeEndForw100MS()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REPEAT_RANGE_BACK_100MS] = ShortcutItem(trUtf8("구간 반복 0.1초 뒤로 이동"), new QShortcut(QKeySequence(Qt::Key_BracketLeft | Qt::AltModifier | Qt::ControlModifier), this, SLOT(repeatRangeBack100MS()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_REPEAT_RANGE_FORW_100MS] = ShortcutItem(trUtf8("구간 반복 0.1초 앞으로 이동"), new QShortcut(QKeySequence(Qt::Key_BracketRight | Qt::AltModifier | Qt::ControlModifier), this, SLOT(repeatRangeForw100MS()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USE_FRAME_DROP] = ShortcutItem(trUtf8("프레임 드랍 사용"), new QShortcut(QKeySequence(), this, SLOT(useFrameDrop()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER] = ShortcutItem(trUtf8("인코딩 순차 선택"), new QShortcut(QKeySequence(Qt::Key_A | Qt::AltModifier | Qt::ShiftModifier), this, SLOT(useSPDIFEncodingOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_ANAGLYPH_ALGORITHM_ORDER] = ShortcutItem(trUtf8("애너글리프 알고리즘 순차 선택"), new QShortcut(QKeySequence(), this, SLOT(anaglyphAlgorithmOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_3D_SUBTITLE_METHOD_ORDER] = ShortcutItem(trUtf8("3D 자막 출력 방법 순차 선택"), new QShortcut(QKeySequence(), this, SLOT(method3DSubtitleOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USE_3D_FULL] = ShortcutItem(trUtf8("3D 전체 해상도 사용"), new QShortcut(QKeySequence(), this, SLOT(use3DFull()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_RESET_3D_SUBTITLE_OFFSET] = ShortcutItem(trUtf8("3D 자막 기본 위치"), new QShortcut(QKeySequence(Qt::Key_Home | Qt::ShiftModifier | Qt::ControlModifier), this, SLOT(reset3DSubtitleOffset()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_UP_3D_SUBTITLE_OFFSET] = ShortcutItem(trUtf8("3D 자막 위치 가깝게(세로)"), new QShortcut(QKeySequence(Qt::Key_Up | Qt::ShiftModifier | Qt::ControlModifier), this, SLOT(up3DSubtitleOffset()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_DOWN_3D_SUBTITLE_OFFSET] = ShortcutItem(trUtf8("3D 자막 위치 멀게(세로)"), new QShortcut(QKeySequence(Qt::Key_Down | Qt::ShiftModifier | Qt::ControlModifier), this, SLOT(down3DSubtitleOffset()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_LEFT_3D_SUBTITLE_OFFSET] = ShortcutItem(trUtf8("3D 자막 위치 가깝게(가로)"), new QShortcut(QKeySequence(Qt::Key_Left | Qt::ShiftModifier | Qt::ControlModifier), this, SLOT(left3DSubtitleOffset()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_RIGHT_3D_SUBTITLE_OFFSET] = ShortcutItem(trUtf8("3D 자막 위치 멀게(가로)"), new QShortcut(QKeySequence(Qt::Key_Right | Qt::ShiftModifier | Qt::ControlModifier), this, SLOT(right3DSubtitleOffset()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SUBTITLE_DIRECTORY] = ShortcutItem(trUtf8("검색 디렉토리"), new QShortcut(QKeySequence(), this, SLOT(openSubtitleDirectory()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_IMPORT_FONTS] = ShortcutItem(trUtf8("자막 폰트 가져오기"), new QShortcut(QKeySequence(), this, SLOT(openImportFonts()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SCREEN_ROTATION_DEGREE_ORDER] = ShortcutItem(trUtf8("화면 회전 각도 순차 선택"), new QShortcut(QKeySequence(Qt::Key_R | Qt::AltModifier | Qt::ControlModifier), this, SLOT(screenRotationDegreeOrder()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_OPEN_DEVICE] = ShortcutItem(trUtf8("장치 열기"), new QShortcut(QKeySequence(), this, SLOT(openDevice()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_ADD_DTV_CHANNEL_TO_PLAYLIST] = ShortcutItem(trUtf8("재생 목록에 DTV 채널 추가"), new QShortcut(QKeySequence(), this, SLOT(addDTVChannelToPlaylist()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_OPEN_DTV_SCAN_CHANNEL] = ShortcutItem(trUtf8("DTV 채널 검색"), new QShortcut(QKeySequence(), this, SLOT(openScanDTVChannel()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_HISTOGRAM_EQ] = ShortcutItem(trUtf8("히스토그램 이퀄라이저"), new QShortcut(QKeySequence(Qt::Key_H | Qt::ControlModifier), this, SLOT(histEQ()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_HIGH_QUALITY_3D_DENOISE] = ShortcutItem(trUtf8("3D 노이즈 제거"), new QShortcut(QKeySequence(Qt::Key_N | Qt::ControlModifier), this, SLOT(hq3DDenoise()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_VIEW_EPG] = ShortcutItem(trUtf8("채널 편성표"), new QShortcut(QKeySequence(), this, SLOT(viewEPG()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX] = ShortcutItem(trUtf8("고급 검색"), new QShortcut(QKeySequence(), this, SLOT(useSearchSubtitleComplex()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_DEBAND] = ShortcutItem(trUtf8("디밴드"), new QShortcut(QKeySequence(Qt::Key_D | Qt::ShiftModifier | Qt::ControlModifier), this, SLOT(deband()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_ATA_DENOISE] = ShortcutItem(trUtf8("적응 시간 평균 노이즈 제거"), new QShortcut(QKeySequence(Qt::Key_A | Qt::AltModifier | Qt::ControlModifier), this, SLOT(ataDenoise()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_OW_DENOISE] = ShortcutItem(trUtf8("Overcomplete Wavelet 노이즈 제거"), new QShortcut(QKeySequence(Qt::Key_O | Qt::ControlModifier), this, SLOT(owDenoise()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_USE_BUFFERING_MODE] = ShortcutItem(trUtf8("버퍼링 모드 사용"), new QShortcut(QKeySequence(Qt::Key_B | Qt::ControlModifier), this, SLOT(useBufferingMode()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_STOP] = ShortcutItem(trUtf8("정지"), new QShortcut(QKeySequence(Qt::Key_S | Qt::AltModifier), this, SLOT(stop()), NULL, Qt::WidgetShortcut));
    this->m_shortcut[AnyVODEnums::SK_STOP_MEDIA] = ShortcutItem(trUtf8("정지(추가)"), new QShortcut(QKeySequence(Qt::Key_MediaStop), this, SLOT(stop()), NULL, Qt::WidgetShortcut));
}

void MainWindow::deleteKey()
{
    for (int i = 0; i < AnyVODEnums::SK_COUNT; i++)
    {
        delete this->m_shortcut[i].shortcut;
        this->m_shortcut[i].shortcut = NULL;
    }
}

void MainWindow::reloadKey()
{
    this->saveShortcutKey();
    this->deleteKey();
    this->setupKey();
    this->loadShortcutKey();
}

void MainWindow::setup3DMethod()
{
    this->m_3DMethodDesc[AnyVODEnums::V3M_NONE] = trUtf8("사용 안 함");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_LEFT] = trUtf8("왼쪽 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_RIGHT] = trUtf8("오른쪽 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_TOP] = trUtf8("상단 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_BOTTOM] = trUtf8("하단 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_FULL_LEFT_RIGHT] = trUtf8("좌우 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_FULL_TOP_BOTTOM] = trUtf8("상하 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용 (Page Flipping)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용 (Page Flipping)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용 (Page Flipping)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용 (Page Flipping)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용 (Row Interlaced)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용 (Row Interlaced)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용 (Row Interlaced)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용 (Row Interlaced)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용 (Column Interlaced)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용 (Column Interlaced)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용 (Column Interlaced)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용 (Column Interlaced)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용 (Red-Cyan Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용 (Red-Cyan Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용 (Red-Cyan Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용 (Red-Cyan Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용 (Green-Magenta Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용 (Green-Magenta Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용 (Green-Magenta Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용 (Green-Magenta Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용 (Yellow-Blue Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용 (Yellow-Blue Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용 (Yellow-Blue Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용 (Yellow-Blue Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용 (Red-Blue Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용 (Red-Blue Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용 (Red-Blue Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용 (Red-Blue Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용 (Red-Green Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용 (Red-Green Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용 (Red-Green Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용 (Red-Green Anaglyph)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용 (Checker Board)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용 (Checker Board)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용 (Checker Board)");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용 (Checker Board)");
}

void MainWindow::setupSubtitle3DMethod()
{
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_NONE] = trUtf8("사용 안 함");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_TOP_BOTTOM] = trUtf8("상/하");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_LEFT_RIGHT] = trUtf8("좌/우");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_PAGE_FLIP] = trUtf8("페이지 플리핑");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_INTERLACED] = trUtf8("인터레이스");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_ANAGLYPH] = trUtf8("애너글리프");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_CHECKER_BOARD] = trUtf8("체커 보드");
}

void MainWindow::setupAnaglyphAlgorithm()
{
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_GRAY] = "Gray";
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_COLORED] = "Colored";
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_HALF_COLORED] = "Half Colored";
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_DUBOIS] = "Dubois";
}

void MainWindow::setupDeinterlaceMethod()
{
    this->m_deinterlaceDesc[AnyVODEnums::DA_BLEND] = "Blend";
    this->m_deinterlaceDesc[AnyVODEnums::DA_BOB] = "BOB";
    this->m_deinterlaceDesc[AnyVODEnums::DA_YADIF] = "YADIF";
    this->m_deinterlaceDesc[AnyVODEnums::DA_YADIF_BOB] = "YADIF(BOB)";
    this->m_deinterlaceDesc[AnyVODEnums::DA_W3FDIF] = "Weston 3 Field";
    this->m_deinterlaceDesc[AnyVODEnums::DA_KERNDEINT] = "Adaptive Kernel";
    this->m_deinterlaceDesc[AnyVODEnums::DA_MCDEINT] = "Motion Compensation";
    this->m_deinterlaceDesc[AnyVODEnums::DA_BWDIF] = "Bob Weaver";
    this->m_deinterlaceDesc[AnyVODEnums::DA_BWDIF_BOB] = "Bob Weaver(BOB)";
}

#ifdef Q_OS_LINUX
bool MainWindow::debusInit()
{
    DBusError err;

    this->m_dbusPending = NULL;
    this->m_dbusCookie = 0;

    dbus_error_init(&err);

    this->m_dbusConn = dbus_bus_get_private(DBUS_BUS_SESSION, &err);

    if (this->m_dbusConn == NULL)
    {
        dbus_error_free(&err);
        return false;
    }

    for (int i = 0; i < SSAPI_COUNT; i++)
    {
        if (dbus_bus_name_has_owner(this->m_dbusConn, DBUS_SERVICE[i], NULL))
        {
            this->m_screenSaverAPI = (ScreenSaverAPI)i;
            return true;
        }
    }

    return false;
}

void MainWindow::debusDeinit()
{
    if (this->m_dbusPending != NULL)
    {
        dbus_pending_call_cancel(this->m_dbusPending);
        dbus_pending_call_unref(this->m_dbusPending);

        this->m_dbusPending = NULL;
    }

    if (this->m_dbusConn)
    {
        dbus_connection_close(this->m_dbusConn);
        dbus_connection_unref(this->m_dbusConn);

        this->m_dbusConn = NULL;
    }
}
#endif

#ifdef Q_OS_WIN
void MainWindow::initThumbnailBar()
{
    this->m_thumbnailToolBar.setParent(this);
    this->m_thumbnailToolBar.setWindow(this->windowHandle());

    this->m_thumbnailToolResumePause.setParent(&this->m_thumbnailToolBar);

    this->m_thumbnailToolStop.setParent(&this->m_thumbnailToolBar);
    this->m_thumbnailToolStop.setEnabled(false);
    this->m_thumbnailToolStop.setIcon(QIcon(":/icons/18_64x64.png"));

    this->m_thumbnailToolBackward.setParent(&this->m_thumbnailToolBar);
    this->m_thumbnailToolBackward.setIcon(QIcon(":/icons/26_64x64.png"));

    this->m_thumbnailToolForward.setParent(&this->m_thumbnailToolBar);
    this->m_thumbnailToolForward.setIcon(QIcon(":/icons/27_64x64.png"));

    connect(&this->m_thumbnailToolResumePause, SIGNAL(clicked()), this, SLOT(on_resume_clicked()));
    connect(&this->m_thumbnailToolStop, SIGNAL(clicked()), this, SLOT(on_stop_clicked()));
    connect(&this->m_thumbnailToolBackward, SIGNAL(clicked()), this, SLOT(prev()));
    connect(&this->m_thumbnailToolForward, SIGNAL(clicked()), this, SLOT(next()));

    this->m_thumbnailToolBar.addButton(&this->m_thumbnailToolResumePause);
    this->m_thumbnailToolBar.addButton(&this->m_thumbnailToolStop);
    this->m_thumbnailToolBar.addButton(&this->m_thumbnailToolBackward);
    this->m_thumbnailToolBar.addButton(&this->m_thumbnailToolForward);

    this->translateThumbnailText();
}

void MainWindow::setThumbnailButtonType(bool resume)
{
    if (resume)
    {
        this->m_thumbnailToolResumePause.setToolTip(trUtf8("재생"));
        this->m_thumbnailToolResumePause.setIcon(QIcon(":/icons/31_64x64.png"));

        connect(&this->m_thumbnailToolResumePause, SIGNAL(clicked()), this, SLOT(on_resume_clicked()));
    }
    else
    {
        this->m_thumbnailToolResumePause.setToolTip(trUtf8("일시정지"));
        this->m_thumbnailToolResumePause.setIcon(QIcon(":/icons/33_64x64.png"));

        connect(&this->m_thumbnailToolResumePause, SIGNAL(clicked()), this, SLOT(on_pause_clicked()));
    }
}

void MainWindow::translateThumbnailText()
{
    this->m_thumbnailToolStop.setToolTip(trUtf8("정지"));
    this->m_thumbnailToolBackward.setToolTip(trUtf8("이전 파일"));
    this->m_thumbnailToolForward.setToolTip(trUtf8("다음 파일"));

    this->setThumbnailButtonType(this->ui->resume->isEnabled());
}
#endif

QString MainWindow::getLanguage() const
{
    return this->m_lang;
}

void MainWindow::loadPlayList()
{
    QVariantList list = this->m_settings.value(SETTING_PLAYLIST, QVariantList()).toList();
    QVector<PlayItem> playList;

    for (int i = 0; i < list.count(); i++)
        playList.append(list[i].value<PlayItem>());

    this->m_playList.setPlayList(playList);

    bool show = this->m_settings.value(SETTING_PLAYLIST_SHOW, this->m_playList.isVisible()).toBool();

#ifdef Q_OS_WIN
    this->m_playList.setVisible(true); // workaround for weird screen
#endif
    this->m_playList.setVisible(show);

    QRect geometry = this->m_settings.value(SETTING_PLAYLIST_GEOMETRY, this->m_playList.geometry()).toRect();
    this->m_playList.setGeometry(geometry);

    int playing = this->m_settings.value(SETTING_PLAYLIST_PLAYING, -1).toInt();
    this->m_playList.selectItem(playing);
    this->m_currentPlayingIndex = playing;
    this->adjustPlayIndex();
    this->updateWindowTitle();
}

void MainWindow::savePlayList()
{
    QVariantList list;
    QVector<PlayItem> playList;

    this->m_playList.stop();
    this->m_playList.getPlayList(&playList);

    for (int i = 0; i < playList.count(); i++)
        list.append(qVariantFromValue(playList[i]));

    this->m_settings.setValue(SETTING_PLAYLIST, list);
    this->m_settings.setValue(SETTING_PLAYLIST_SHOW, this->m_playList.isVisible());
    this->m_settings.setValue(SETTING_PLAYLIST_GEOMETRY, this->m_playList.geometry());
    this->m_settings.setValue(SETTING_PLAYLIST_PLAYING, this->m_currentPlayingIndex);
}

void MainWindow::loadSettings()
{
    QSize minSize = this->m_settings.value(SETTING_MAINWINDOW_MIN_SIZE, this->minimumSize()).toSize();
    this->setMinimumSize(minSize);

    QSize maxSize = this->m_settings.value(SETTING_MAINWINDOW_MAX_SIZE, this->maximumSize()).toSize();
    this->setMaximumSize(maxSize);

    QRect geometry = this->m_settings.value(SETTING_MAINWINDOW_GEOMETRY, QRect(DEFAULT_POSITION, DEFAULT_SCREEN_SIZE)).toRect();

    if (geometry.height() < CONTROL_BAR_HEIGHT)
        geometry.setHeight(CONTROL_BAR_HEIGHT);

    this->setGeometry(geometry);

    bool isMostTop = this->m_settings.value(SETTING_MAINWINDOW_MOSTTOP, false).toBool();

    if (isMostTop)
        this->mostTop();

    bool screenVisible = this->m_settings.value(SETTING_SCREEN_VISIBLE, this->m_lastScreenVisible).toBool();
    this->ui->screen->setVisible(screenVisible);
    this->m_lastScreenVisible = screenVisible;

    this->m_lastSelectedDir = this->m_settings.value(SETTING_SCREEN_LAST_DIR, QString()).toString();

    QSize screenSize = this->ui->screen->size();

    if (screenVisible)
    {
        if (screenSize.height() <= 0)
            screenSize.setHeight(DEFAULT_SCREEN_SIZE.height());

        if (screenSize.width() <= 0)
            screenSize.setWidth(DEFAULT_SCREEN_SIZE.width());
    }
    else
    {
        if (screenSize.height() <= DEFAULT_SCREEN_SIZE.height())
            screenSize.setHeight(DEFAULT_SCREEN_SIZE.height());

        if (screenSize.width() <= DEFAULT_SCREEN_SIZE.width())
            screenSize.setWidth(DEFAULT_SCREEN_SIZE.width());
    }

    this->m_lastScreenSize = screenSize;
    this->ui->screen->resize(screenSize);

    uint8_t volume = this->m_settings.value(SETTING_VOLUME, this->m_player->getVolume()).toInt();
    this->m_player->volume(volume);

    bool showSubtitle = this->m_settings.value(SETTING_SHOW_SUBTITLE, this->m_player->isShowSubtitle()).toBool();
    this->m_player->showSubtitle(showSubtitle);

    bool useNormalizer = this->m_settings.value(SETTING_USE_NORMALIZER, this->m_player->isUsingNormalizer()).toBool();
    this->m_player->useNormalizer(useNormalizer);

    AnyVODEnums::PlayingMethod method = (AnyVODEnums::PlayingMethod)this->m_settings.value(SETTING_MAINWINDOW_PLAYING_METHOD, (int)this->getPlayingMethod()).toInt();
    this->setPlayingMethod(method);

    Spectrum::SpectrumType type = (Spectrum::SpectrumType)this->m_settings.value(SETTING_SPECTRUM_TYPE, (int)this->ui->spectrum->getType()).toInt();
    this->ui->spectrum->setType(type);

    qreal opaque = this->m_settings.value(SETTING_MAINWINDOW_OPAQUE, this->windowOpacity()).toReal();
    this->setWindowOpacity(opaque);
    this->m_remoteFileList.setWindowOpacity(opaque);
    this->m_playList.setWindowOpacity(opaque);
    this->ui->opaque->setValue(this->ui->opaque->maximum() * opaque);

    AnyVODEnums::DeinterlaceMethod deMethod = (AnyVODEnums::DeinterlaceMethod)this->m_settings.value(SETTING_DEINTERLACER_METHOD, this->m_player->getDeinterlaceMethod()).toInt();
    this->m_player->setDeinterlaceMethod(deMethod);

    AnyVODEnums::DeinterlaceAlgorithm deAlgorithm = (AnyVODEnums::DeinterlaceAlgorithm)this->m_settings.value(SETTING_DEINTERLACER_ALGORITHM, this->m_player->getDeinterlaceAlgorithm()).toInt();
    this->m_player->setDeinterlaceAlgorithm(deAlgorithm);

    AnyVODEnums::HAlignMethod halign = (AnyVODEnums::HAlignMethod)this->m_settings.value(SETTING_HALIGN_SUBTITLE, this->m_player->getHAlign()).toInt();
    this->m_player->setHAlign(halign);

    bool seekKeyFrame = this->m_settings.value(SETTING_SEEK_KEYFRAME, this->m_player->isSeekKeyFrame()).toBool();
    this->m_player->setSeekKeyFrame(seekKeyFrame);

    Screen::CaptureInfo capinfo;
    this->ui->screen->getCaptureInfo(&capinfo);
    capinfo.savePath = this->m_settings.value(SETTING_SCREEN_CAPTURE_DIR, capinfo.savePath).toString();
    capinfo.ext = this->m_settings.value(SETTING_SCREEN_CAPTURE_EXT, capinfo.ext).toString();
    this->ui->screen->setCaptureInfo(capinfo);

    bool searchSubtitle = this->m_settings.value(SETTING_ENABLE_SEARCH_SUBTITLE, this->m_player->isEnableSearchSubtitle()).toBool();
    this->m_player->enableSearchSubtitle(searchSubtitle);

    bool searchLyrics = this->m_settings.value(SETTING_ENABLE_SEARCH_LYRICS, this->m_player->isEnableSearchLyrics()).toBool();
    this->m_player->enableSearchLyrics(searchLyrics);

    AnyVODEnums::VAlignMethod valign = (AnyVODEnums::VAlignMethod)this->m_settings.value(SETTING_VALIGN_SUBTITLE, this->m_player->getVAlign()).toInt();
    this->m_player->setVAlign(valign);

    float subtitleSize = (float)this->m_settings.value(SETTING_SUBTITLE_SIZE, this->m_player->getSubtitleSize()).toFloat();
    this->m_player->setSubtitleSize(subtitleSize);

    UserAspectRatioItem &item = this->m_userAspectRatioList.list.last();
    QSizeF aspectSize(item.width, item.height);
    QSizeF aspectRatio = this->m_settings.value(SETTING_USER_ASPECT_RATIO, aspectSize).toSizeF();
    item.width = aspectRatio.width();
    item.height = aspectRatio.height();

    bool useHWDecoder = this->m_settings.value(SETTING_USE_HW_DECODER, this->m_player->isUseHWDecoder()).toBool();
    this->m_player->useHWDecoder(useHWDecoder);

    bool usePBO = this->m_settings.value(SETTING_USE_PBO, this->m_player->isUsePBO()).toBool();
    this->m_player->usePBO(usePBO);

    bool useVSync = this->m_settings.value(SETTING_USE_VSYNC, this->ui->screen->isUseVSync()).toBool();
    this->ui->screen->setUseVSync(useVSync);

    this->loadShortcutKey();

    QString lang = this->m_settings.value(SETTING_LANGUAGE, QString()).toString();
    this->setLanguage(lang);

    this->m_clearPlayList = this->m_settings.value(SETTING_CLEAR_PLAYLIST, this->m_clearPlayList).toBool();

    bool showAlbumJacket = this->m_settings.value(SETTING_SHOW_ALBUM_JACKET, this->m_player->isShowAlbumJacket()).toBool();
    this->m_player->showAlbumJacket(showAlbumJacket);

    bool enableLastPlay = this->m_settings.value(SETTING_ENABLE_LAST_PLAY, this->m_player->isGotoLastPos()).toBool();
    this->m_player->enableGotoLastPos(enableLastPlay, false);

    Utils::setSubtitleCodecName(this->m_settings.value(SETTING_TEXT_ENCODING, QTextCodec::codecForLocale()->name()).toString());

    this->m_remoteAddress = this->m_settings.value(SETTING_ADDRESS, this->m_remoteAddress).toString();
    this->m_remoteCommandPort = this->m_settings.value(SETTING_COMMAND_PORT, this->m_remoteCommandPort).toUInt();
    this->m_remoteStreamPort = this->m_settings.value(SETTING_STREAM_PORT, this->m_remoteStreamPort).toUInt();

    bool useFrameDrop = this->m_settings.value(SETTING_USE_FRAME_DROP, this->m_player->isUseFrameDrop()).toBool();
    this->m_player->useFrameDrop(useFrameDrop);

    QStringList subtitleDirectory;
    bool priorSubtitleDirectory;

    this->m_player->getSubtitleDirectory(&subtitleDirectory, &priorSubtitleDirectory);

    subtitleDirectory = this->m_settings.value(SETTING_SUBTITLE_DIRECTORY, subtitleDirectory).toStringList();
    priorSubtitleDirectory = this->m_settings.value(SETTING_PRIOR_SUBTITLE_DIRECTORY, priorSubtitleDirectory).toBool();

    this->m_player->setSubtitleDirectory(subtitleDirectory, priorSubtitleDirectory);

    bool searchSubtitleComplex = this->m_settings.value(SETTING_SEARCH_SUBTITLE_COMPLEX, this->m_player->getSearchSubtitleComplex()).toBool();
    this->m_player->setSearchSubtitleComplex(searchSubtitleComplex);

    int spdifDevice = this->m_settings.value(SETTING_SPDIF_DEVICE, this->m_player->getCurrentSPDIFAudioDevice()).toInt();
    this->m_player->setSPDIFAudioDevice(spdifDevice);

    bool useBufferingMode = this->m_settings.value(SETTING_USE_BUFFERING_MODE, this->m_player->isUseBufferingMode()).toBool();
    this->m_player->useBufferingMode(useBufferingMode);

    this->loadPlayList();
    this->loadEqualizer();
    this->loadSkipRange();
    this->loadDTVScannedChannels();
}

void MainWindow::saveSettings()
{
    this->m_settings.setValue(SETTING_MAINWINDOW_MIN_SIZE, this->minimumSize());
    this->m_settings.setValue(SETTING_MAINWINDOW_MAX_SIZE, this->maximumSize());
    this->m_settings.setValue(SETTING_MAINWINDOW_GEOMETRY, this->geometry());
    this->m_settings.setValue(SETTING_MAINWINDOW_MOSTTOP, IS_BIT_SET(this->windowFlags(), Qt::WindowStaysOnTopHint));
    this->m_settings.setValue(SETTING_MAINWINDOW_OPAQUE, this->windowOpacity());
    this->m_settings.setValue(SETTING_SCREEN_VISIBLE, this->m_lastScreenVisible);
    this->m_settings.setValue(SETTING_SCREEN_LAST_DIR, this->m_lastSelectedDir);
    this->m_settings.setValue(SETTING_VOLUME, this->m_player->getVolume());
    this->m_settings.setValue(SETTING_SHOW_SUBTITLE, this->m_player->isShowSubtitle());
    this->m_settings.setValue(SETTING_USE_NORMALIZER, this->m_player->isUsingNormalizer());
    this->m_settings.setValue(SETTING_MAINWINDOW_PLAYING_METHOD, (int)this->getPlayingMethod());
    this->m_settings.setValue(SETTING_SPECTRUM_TYPE, (int)this->ui->spectrum->getType());
    this->m_settings.setValue(SETTING_DEINTERLACER_METHOD, (int)this->m_player->getDeinterlaceMethod());
    this->m_settings.setValue(SETTING_DEINTERLACER_ALGORITHM, (int)this->m_player->getDeinterlaceAlgorithm());
    this->m_settings.setValue(SETTING_HALIGN_SUBTITLE, (int)this->m_player->getHAlign());
    this->m_settings.setValue(SETTING_SEEK_KEYFRAME, this->m_player->isSeekKeyFrame());

    Screen::CaptureInfo capinfo;

    this->ui->screen->getCaptureInfo(&capinfo);
    this->m_settings.setValue(SETTING_SCREEN_CAPTURE_DIR, capinfo.savePath);
    this->m_settings.setValue(SETTING_SCREEN_CAPTURE_EXT, capinfo.ext);

    this->m_settings.setValue(SETTING_ENABLE_SEARCH_SUBTITLE, this->m_player->isEnableSearchSubtitle());
    this->m_settings.setValue(SETTING_ENABLE_SEARCH_LYRICS, this->m_player->isEnableSearchLyrics());

    this->m_settings.setValue(SETTING_VALIGN_SUBTITLE, (int)this->m_player->getVAlign());
    this->m_settings.setValue(SETTING_SUBTITLE_SIZE, this->m_player->getSubtitleSize());

    UserAspectRatioItem &item = this->m_userAspectRatioList.list.last();
    QSizeF aspectSize(item.width, item.height);
    this->m_settings.setValue(SETTING_USER_ASPECT_RATIO, aspectSize);

    this->m_settings.setValue(SETTING_USE_HW_DECODER, this->m_player->isUseHWDecoder());
    this->m_settings.setValue(SETTING_USE_PBO, this->m_player->isUsePBO());
    this->m_settings.setValue(SETTING_USE_VSYNC, this->ui->screen->isUseVSync());
    this->m_settings.setValue(SETTING_LANGUAGE, this->m_lang);
    this->m_settings.setValue(SETTING_CLEAR_PLAYLIST, this->m_clearPlayList);
    this->m_settings.setValue(SETTING_SHOW_ALBUM_JACKET, this->m_player->isShowAlbumJacket());
    this->m_settings.setValue(SETTING_ENABLE_LAST_PLAY, this->m_player->isGotoLastPos());
    this->m_settings.setValue(SETTING_TEXT_ENCODING, Utils::getSubtitleCodecName());
    this->m_settings.setValue(SETTING_ADDRESS, this->m_remoteAddress);
    this->m_settings.setValue(SETTING_COMMAND_PORT, this->m_remoteCommandPort);
    this->m_settings.setValue(SETTING_STREAM_PORT, this->m_remoteStreamPort);
    this->m_settings.setValue(SETTING_USE_FRAME_DROP, this->m_player->isUseFrameDrop());

    QStringList subtitleDirectory;
    bool priorSubtitleDirectory;

    this->m_player->getSubtitleDirectory(&subtitleDirectory, &priorSubtitleDirectory);
    this->m_settings.setValue(SETTING_SUBTITLE_DIRECTORY, subtitleDirectory);
    this->m_settings.setValue(SETTING_PRIOR_SUBTITLE_DIRECTORY, priorSubtitleDirectory);

    if (this->m_clearPlayList)
        this->m_playList.clearPlayList();

    this->m_settings.setValue(SETTING_SEARCH_SUBTITLE_COMPLEX, this->m_player->getSearchSubtitleComplex());
    this->m_settings.setValue(SETTING_SPDIF_DEVICE, this->m_player->getCurrentSPDIFAudioDevice());
    this->m_settings.setValue(SETTING_USE_BUFFERING_MODE, this->m_player->isUseBufferingMode());

    this->savePlayList();
    this->saveEqualizer();
    this->saveSkipRange();
    this->saveShortcutKey();
    this->saveDTVScannedChannels();
}

void MainWindow::loadEqualizer()
{
    bool useEqualizer = this->m_settings.value(SETTING_USE_EQUALIZER, this->m_player->isUsingEqualizer()).toBool();
    this->m_player->useEqualizer(useEqualizer);

    float preamp = this->m_settings.value(SETTING_EQUALIZER_PREAMP, this->m_player->getPreAmp()).toFloat();
    this->m_player->setPreAmp(preamp);

    QVariantList list = this->m_settings.value(SETTING_EQUALIZER_GAINS, QVariantList()).toList();

    for (int i = 0; i < list.count(); i++)
        this->m_player->setEqualizerGain(i, list[i].value<Equalizer::EqualizerItem>().gain);
}

void MainWindow::saveEqualizer()
{
    this->m_settings.setValue(SETTING_USE_EQUALIZER, this->m_player->isUsingEqualizer());
    this->m_settings.setValue(SETTING_EQUALIZER_PREAMP, this->m_player->getPreAmp());

    QVariantList list;
    QVector<Equalizer::EqualizerItem> eqGains;

    for (int i = 0; i < this->m_player->getBandCount(); i++)
    {
        Equalizer::EqualizerItem item;

        item.gain = this->m_player->getEqualizerGain(i);

        eqGains.append(item);
    }

    for (int i = 0; i < eqGains.count(); i++)
        list.append(qVariantFromValue(eqGains[i]));

    this->m_settings.setValue(SETTING_EQUALIZER_GAINS, list);
}

void MainWindow::loadSkipRange()
{
    bool skipOpening = this->m_settings.value(SETTING_SKIP_OPENING, this->m_player->getSkipOpening()).toBool();
    this->m_player->setSkipOpening(skipOpening);

    bool skipEnding = this->m_settings.value(SETTING_SKIP_ENDING, this->m_player->getSkipEnding()).toBool();
    this->m_player->setSkipEnding(skipEnding);

    bool useSkipRange = this->m_settings.value(SETTING_SKIP_USING, this->m_player->getUseSkipRange()).toBool();
    this->m_player->setUseSkipRange(useSkipRange);

    QVariantList list = this->m_settings.value(SETTING_SKIP_RANGE, QVariantList()).toList();
    QVector<MediaPresenter::Range> ranges;

    for (int i = 0; i < list.count(); i++)
        ranges.append(list[i].value<MediaPresenter::Range>());

    this->m_player->setSkipRanges(ranges);
}

void MainWindow::saveSkipRange()
{
    this->m_settings.setValue(SETTING_SKIP_OPENING, this->m_player->getSkipOpening());
    this->m_settings.setValue(SETTING_SKIP_ENDING, this->m_player->getSkipEnding());
    this->m_settings.setValue(SETTING_SKIP_USING, this->m_player->getUseSkipRange());

    QVariantList list;
    QVector<MediaPresenter::Range> ranges;

    this->m_player->getSkipRanges(&ranges);

    for (int i = 0; i < ranges.count(); i++)
        list.append(qVariantFromValue(ranges[i]));

    this->m_settings.setValue(SETTING_SKIP_RANGE, list);
}

void MainWindow::loadShortcutKey()
{
    QVariantList shortcutTmp;

    for (int i = 0; i < AnyVODEnums::SK_COUNT; i++)
    {
        QString key;

        if (this->m_shortcut[i].shortcut)
            key = this->m_shortcut[i].shortcut->key().toString();

        shortcutTmp.append(key);
    }

    shortcutTmp = this->m_settings.value(SETTING_SHORTCUTS, shortcutTmp).toList();

    for (int i = 0; i < shortcutTmp.count() && i < AnyVODEnums::SK_COUNT; i++)
    {
        if (this->m_shortcut[i].shortcut)
            this->m_shortcut[i].shortcut->setKey(QKeySequence::fromString(shortcutTmp[i].value<QString>()));
    }
}

void MainWindow::saveShortcutKey()
{
    QVariantList list;

    for (int i = 0; i < AnyVODEnums::SK_COUNT; i++)
    {
        QString key;

        if (this->m_shortcut[i].shortcut)
            key = this->m_shortcut[i].shortcut->key().toString();

        list.append(key);
    }

    this->m_settings.setValue(SETTING_SHORTCUTS, list);
}

void MainWindow::loadDTVScannedChannels()
{
    QVariantList list = this->m_settings.value(SETTING_DTV_SCANNED_CHANNELS, QVariantList()).toList();
    QVector<DTVReader::ChannelInfo> channels;

    for (int i = 0; i < list.count(); i++)
        channels.append(list[i].value<DTVReader::ChannelInfo>());

    this->m_player->getDTVReader().setScannedChannels(channels);

    QLocale::Country country = (QLocale::Country)this->m_settings.value(SETTING_DTV_CURRENT_COUNTRY, QLocale::SouthKorea).toInt();
    DTVReaderInterface::SystemType type = (DTVReaderInterface::SystemType)this->m_settings.value(SETTING_DTV_CURRENT_TYPE, DTVReaderInterface::ST_ATSC_T).toInt();

    this->m_player->getDTVReader().setChannelCountry(country, type);
}

void MainWindow::saveDTVScannedChannels()
{
    QVariantList list;
    QVector<DTVReader::ChannelInfo> channels;

    this->m_player->getDTVReader().getScannedChannels(&channels);

    for (int i = 0; i < channels.length(); i++)
        list.append(qVariantFromValue(channels[i]));

    this->m_settings.setValue(SETTING_DTV_SCANNED_CHANNELS, list);

    QLocale::Country country;
    DTVReaderInterface::SystemType type;

    this->m_player->getDTVReader().getChannelCountry(&country, &type);

    this->m_settings.setValue(SETTING_DTV_CURRENT_COUNTRY, country);
    this->m_settings.setValue(SETTING_DTV_CURRENT_TYPE, type);
}

void MainWindow::resetWindowTitle()
{
    this->setWindowTitle(MainWindow::APP_NAME);
    this->setFileNameIndicator(MainWindow::APP_NAME + " " + this->getVersion());
}

int MainWindow::getCurrentPlayingIndex() const
{
    return this->m_currentPlayingIndex;
}

void MainWindow::resetCurrentPlayingIndex()
{
    this->m_currentPlayingIndex = -1;
}

void MainWindow::setCurrentPlayingIndex(int index)
{
    this->m_currentPlayingIndex = index;
}

bool MainWindow::setCurrentPlayingIndexByUnique()
{
    QUuid unique = this->m_player->getUnique();

    if (unique == QUuid())
    {
        this->resetCurrentPlayingIndex();
        return false;
    }
    else
    {
        this->m_currentPlayingIndex = this->m_playList.findIndex(unique);
        return true;
    }
}

void MainWindow::setFileNameIndicator(const QString &fileName)
{
    this->ui->currentFileName->setText(fileName);
    this->ui->currentFileName->setToolTip(fileName);
}

void MainWindow::setPlayingMethod(AnyVODEnums::PlayingMethod method)
{
    QString methodDesc;

    this->m_playingMethod = method;

    switch (method)
    {
        case AnyVODEnums::PM_TOTAL:
            methodDesc = trUtf8("전체 순차 재생");
            break;
        case AnyVODEnums::PM_TOTAL_REPEAT:
            methodDesc = trUtf8("전체 반복 재생");
            break;
        case AnyVODEnums::PM_SINGLE:
            methodDesc = trUtf8("한 개 재생");
            break;
        case AnyVODEnums::PM_SINGLE_REPEAT:
            methodDesc = trUtf8("한 개 반복 재생");
            break;
        case AnyVODEnums::PM_RANDOM:
            methodDesc = trUtf8("무작위 재생");
            break;
        default:
            break;
    }

    this->m_player->showOptionDesc(trUtf8("재생 순서 (%1)").arg(methodDesc));
}

AnyVODEnums::PlayingMethod MainWindow::getPlayingMethod() const
{
    return this->m_playingMethod;
}

bool MainWindow::isPrevEnabled() const
{
    return this->ui->prev->isEnabled();
}

bool MainWindow::isNextEnabled() const
{
    return this->ui->next->isEnabled();
}

bool MainWindow::isMostTop() const
{
    return IS_BIT_SET(this->windowFlags(), Qt::WindowStaysOnTopHint);
}

bool MainWindow::isMute() const
{
    return this->ui->mute->isChecked();
}

bool MainWindow::isReady() const
{
    return this->m_isReady;
}

void MainWindow::setSeekingText()
{
    if (this->m_player->isAudio() && this->m_player->isRemoteFile())
        this->ui->audioBuffering->setText(trUtf8("탐색 중입니다"));
}

ShaderCompositer& MainWindow::getShader()
{
    return this->ui->screen->getShader();
}

QWidget& MainWindow::getControlBar()
{
    return *this->ui->controlBar;
}

void MainWindow::updateWindowTitle()
{
    QString current = QString().setNum(this->m_currentPlayingIndex + 1);
    QString totalCount = QString().setNum(this->m_playList.getCount());

    if (this->m_playList.exist())
    {
        QString fileName = this->m_playList.getFileName(this->m_currentPlayingIndex);
        QString desc = QString("%1 - [%2 / %3] %4").arg(MainWindow::APP_NAME, current, totalCount, fileName);

        this->setWindowTitle(desc);
        this->setFileNameIndicator(fileName);
    }
    else if (this->m_player->isPlayOrPause())
    {
        QString fileName = this->m_player->getFileName();
        QString desc = QString("%1 - %2").arg(MainWindow::APP_NAME, fileName);

        this->setWindowTitle(desc);
        this->setFileNameIndicator(fileName);
    }
    else
    {
        this->resetWindowTitle();
    }
}

const MainWindow::ENV* MainWindow::getEnv() const
{
    return &this->m_env;
}

QString MainWindow::getShortcutKeyDesc(AnyVODEnums::ShortcutKey key) const
{
    return this->m_shortcut[key].desc;
}

QKeySequence MainWindow::getShortcutKey(AnyVODEnums::ShortcutKey key) const
{
    if (this->m_shortcut[key].shortcut)
        return this->m_shortcut[key].shortcut->key();
    else
        return QKeySequence();
}

void MainWindow::setShortcutKey(AnyVODEnums::ShortcutKey key, QKeySequence seq)
{
    if (this->m_shortcut[key].shortcut)
        this->m_shortcut[key].shortcut->setKey(seq);
}

void MainWindow::moveChildWindows()
{
    this->m_remoteFileList.move(QPoint(this->x(), this->y() + this->frameGeometry().height()));
    this->m_playList.move(QPoint(this->x() + this->frameGeometry().width(), this->y()));
    this->movePopup();
}

void MainWindow::movePopup()
{
    int xFrame = (this->frameGeometry().width() - this->width()) / 2;
    int x = this->frameGeometry().width() - this->m_popup.frameGeometry().width() - xFrame;

    int yFrame = (this->frameGeometry().height() - this->height());
    int y = this->ui->screen->height() - this->m_popup.frameGeometry().height() + yFrame - xFrame;

    this->m_popup.move(QPoint(this->x() + x, this->y() + y));
}

bool MainWindow::isFullScreening() const
{
    return this->m_isFullScreening;
}

void MainWindow::getUserAspectRatioList(UserAspectRatioList *ret) const
{
    *ret = this->m_userAspectRatioList;
}

void MainWindow::getUserSPDIFSampleRateList(UserSPDIFSampleRateList *ret) const
{
    *ret = this->m_userSPDIFSampleRateList;
}

bool MainWindow::canShowControlBar() const
{
    return this->m_player->isEnabledVideo() && !this->isFullScreen();
}

bool MainWindow::isLogined() const
{
    return Socket::getInstance().isLogined() && Socket::getStreamInstance().isLogined();
}

bool MainWindow::isClearPlayList() const
{
    return this->m_clearPlayList;
}

QString MainWindow::get3DMethodDesc(AnyVODEnums::Video3DMethod method) const
{
    return this->m_3DMethodDesc[method];
}

QString MainWindow::getSubtitle3DMethodDesc(AnyVODEnums::Subtitle3DMethod method) const
{
    return this->m_subtitle3DMethodDesc[method];
}

QString MainWindow::getAnaglyphAlgoritmDesc(AnyVODEnums::AnaglyphAlgorithm algorithm) const
{
    return this->m_anaglyphAlgorithmDesc[algorithm];
}

QString MainWindow::getDeinterlaceDesc(AnyVODEnums::DeinterlaceAlgorithm algorithm) const
{
    return this->m_deinterlaceDesc[algorithm];
}

void MainWindow::moveEvent(QMoveEvent *)
{
    this->moveChildWindows();
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    this->moveChildWindows();
}

void MainWindow::closeEvent(QCloseEvent *)
{
    if (this->isFullScreen())
        this->fullScreen();

    this->saveSettings();
}

void MainWindow::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        case QEvent::LocaleChange:
        {
            QString lang = QLocale::system().name().left(2).toLower();

            this->setLanguage(lang);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void MainWindow::contextMenuEvent(QContextMenuEvent *)
{
    this->ui->screen->showContextMenu();
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
        this->processMouseDown();
}

void MainWindow::mouseReleaseEvent(QMouseEvent *)
{
    this->processMouseUp();
}

void MainWindow::mouseMoveEvent(QMouseEvent *)
{
    this->processMouseMoving();
}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    const QMimeData *mime = event->mimeData();

    if (mime->hasUrls())
    {
        const QList<QUrl> urls = mime->urls();
        QStringList filePathList;
        bool ignoreExts = IS_BIT_SET(event->keyboardModifiers(), Qt::AltModifier | Qt::ControlModifier);

        if (urls.count() == 1)
        {
            const QUrl &url = urls[0];

            if (Utils::determinRemoteProtocol(url.toString()))
            {
                event->accept();
                return;
            }
            else
            {
                QFileInfo info(url.toLocalFile());

                if (SUBTITLE_EXTS_LIST.contains(info.suffix().toLower()))
                {
                    event->accept();
                    return;
                }
            }
        }

        for (int i = 0; i < urls.count(); i++)
            Utils::getLocalFileListOnlyMedia(urls[i].toLocalFile(), MEDIA_EXTS_LIST, ignoreExts, &filePathList);

        if (!filePathList.isEmpty())
            event->accept();
    }
}

void MainWindow::setEnglishIME() const
{
#ifdef Q_OS_WIN
    ImmAssociateContext((HWND)this->winId(), NULL);
#endif
}

void MainWindow::selectAudioDevice(int device)
{
    this->m_player->setAudioDevice(device);
}

void MainWindow::audioDeviceOrder()
{
    QStringList list;
    int curDevice = this->m_player->getCurrentAudioDevice() + 1;

    this->m_player->getAudioDevices(&list);

    if (curDevice >= list.count())
        curDevice = -1;

    this->selectAudioDevice(curDevice);
}

void MainWindow::selectSPDIFAudioDevice(int device)
{
    this->m_player->setSPDIFAudioDevice(device);
    this->adjustSPDIFAudioUI();
}

void MainWindow::audioSPDIFDeviceOrder()
{
    QStringList list;
    int curDevice = this->m_player->getCurrentSPDIFAudioDevice() + 1;

    this->m_player->getSPDIFAudioDevices(&list);

    if (curDevice >= list.count())
        curDevice = -1;

    this->selectSPDIFAudioDevice(curDevice);
}

void MainWindow::openScreenExplorer()
{
    if (this->m_player->hasDuration() && !this->m_player->isAudio() && !this->m_player->isRemoteFile())
    {
        ScreenExplorer dlg(this);

        dlg.exec();
    }
}

void MainWindow::useFrameDrop()
{
    this->m_player->useFrameDrop(!this->m_player->isUseFrameDrop());
}

void MainWindow::useSPDIFEncodingOrder()
{
    AnyVODEnums::SPDIFEncodingMethod method = this->m_player->getSPDIFEncodingMethod();

    method = (AnyVODEnums::SPDIFEncodingMethod)(method + 1);

    if (method >= AnyVODEnums::SEM_COUNT)
        method = AnyVODEnums::SEM_NONE;

    this->selectSPDIFEncodingMethod(method);
}

void MainWindow::selectSPDIFEncodingMethod(int method)
{
    this->m_player->setSPDIFEncodingMethod((AnyVODEnums::SPDIFEncodingMethod)method);
    this->adjustSPDIFAudioUI();
}

void MainWindow::anaglyphAlgorithmOrder()
{
    ShaderCompositer &shader = this->ui->screen->getShader();

    if (shader.canUseAnaglyphAlgorithm())
    {
        AnyVODEnums::AnaglyphAlgorithm algorithm = shader.getAnaglyphAlgorithm();

        algorithm = (AnyVODEnums::AnaglyphAlgorithm)(algorithm + 1);

        if (algorithm >= AnyVODEnums::AGA_COUNT)
            algorithm = AnyVODEnums::AGA_DEFAULT;

        this->selectAnaglyphAlgorithm(algorithm);

        this->m_player->showOptionDesc(trUtf8("애너글리프 알고리즘 (%1)").arg(this->getAnaglyphAlgoritmDesc(algorithm)));
    }
}

void MainWindow::selectAnaglyphAlgorithm(int algorithm)
{
    this->ui->screen->getShader().setAnaglyphAlgorithm((AnyVODEnums::AnaglyphAlgorithm)algorithm);
}

void MainWindow::method3DSubtitleOrder()
{
    AnyVODEnums::Subtitle3DMethod method = this->m_player->getSubtitle3DMethod();

    method = (AnyVODEnums::Subtitle3DMethod)(method + 1);

    if (method >= AnyVODEnums::S3M_COUNT)
        method = AnyVODEnums::S3M_NONE;

    this->m_player->setSubtitle3DMethod(method);
}

void MainWindow::select3DSubtitleMethod(int method)
{
    this->m_player->setSubtitle3DMethod((AnyVODEnums::Subtitle3DMethod)method);
}

void MainWindow::use3DFull()
{
    if (this->m_player->get3DMethod() != AnyVODEnums::V3M_NONE)
        this->m_player->use3DFull(!this->m_player->isUse3DFull());
}


void MainWindow::reset3DSubtitleOffset()
{
    if (this->m_player->is3DSubtitleMoveable())
        this->m_player->reset3DSubtitleOffset();
}

void MainWindow::up3DSubtitleOffset()
{
    if (this->m_player->is3DSubtitleMoveable())
        this->m_player->setVertical3DSubtitleOffset(1);
}

void MainWindow::down3DSubtitleOffset()
{
    if (this->m_player->is3DSubtitleMoveable())
        this->m_player->setVertical3DSubtitleOffset(-1);
}

void MainWindow::left3DSubtitleOffset()
{
    if (this->m_player->is3DSubtitleMoveable())
        this->m_player->setHorizontal3DSubtitleOffset(1);
}

void MainWindow::right3DSubtitleOffset()
{
    if (this->m_player->is3DSubtitleMoveable())
        this->m_player->setHorizontal3DSubtitleOffset(-1);
}

void MainWindow::openSubtitleDirectory()
{
    QStringList paths;
    bool prior;

    this->m_player->getSubtitleDirectory(&paths, &prior);

    SubtitleDirectory dlg(paths, prior, this);

    if (dlg.exec())
    {
        dlg.getResult(&paths, &prior);
        this->m_player->setSubtitleDirectory(paths, prior);
    }
}

void MainWindow::openImportFonts()
{
    QString fontDir(QFileInfo(this->m_env.fontPath).absolutePath());
    QString msg;

    msg = trUtf8("관리자 권한으로 실행되지 않을 경우 정상적으로 가져오기가 안될 수 있습니다.\n만약에 관리자 권한이 아닐 경우 아래 경로에 폰트를 수동으로 복사 하세요.\n계속 하시겠습니까?");
    msg += "\n\n";
    msg += fontDir;

    if (Utils::questionMessageBox(this, msg))
    {
        QString filter = trUtf8("폰트 (*.%2);;모든 파일 (*.*)").arg(FONT_EXTS_LIST.join(" *."));
        QStringList files = QFileDialog::getOpenFileNames(this, trUtf8("자막 폰트 가져오기"), this->m_lastSelectedDir, filter, NULL, QFileDialog::HideNameFilterDetails);

        if (!files.isEmpty())
        {
            bool copied = true;

            Utils::appendDirSeparator(&fontDir);

            foreach (QString filePath, files)
            {
                QFile file(filePath);
                QString dest = fontDir + QFileInfo(file).fileName();

                copied &= file.copy(dest);
            }

            if (copied)
                this->m_player->showOptionDesc(trUtf8("복사가 완료 되었습니다."));
            else
                this->m_player->showOptionDesc(trUtf8("일부 파일이 복사되지 않았습니다."));

            this->m_lastSelectedDir = fontDir;
        }
    }
}

void MainWindow::screenRotationDegreeOrder()
{
    AnyVODEnums::ScreenRotationDegree degree = this->m_player->getScreenRotationDegree();

    degree = (AnyVODEnums::ScreenRotationDegree)(degree + 1);

    if (degree >= AnyVODEnums::SRD_COUNT)
        degree = AnyVODEnums::SRD_NONE;

    this->selectScreenRotationDegree(degree);
}

void MainWindow::selectScreenRotationDegree(int degree)
{
    this->m_player->setScreenRotationDegree((AnyVODEnums::ScreenRotationDegree)degree);
}

void MainWindow::openDevice()
{
    OpenDevice dlg(this);

    if (dlg.exec() == QDialog::Accepted)
    {
        QString path;
        QString desc;
        QVector<PlayItem> itemVector;
        QStringList list;

        dlg.getDevicePath(&path);
        dlg.getDesc(&desc);

        list.append(path);
        Utils::getPlayItemFromFileNames(list, &itemVector);

        if (!itemVector.isEmpty())
        {
            itemVector[0].title = desc;
            itemVector[0].extraData.userData = path.toUtf8();
        }

        this->addToPlayList(itemVector, true, true);
    }
}

void MainWindow::addDTVChannelToPlaylist()
{
    DTVReader &reader = this->m_player->getDTVReader();
    QVector<DTVReader::ChannelInfo> scanned;

    if (!reader.isSupport())
        return;

    reader.getScannedChannels(&scanned);

    if (scanned.isEmpty())
    {
        this->openScanDTVChannel();
    }
    else
    {
        QVector<PlayItem> itemVector;

        foreach (const DTVReader::ChannelInfo &channelInfo, scanned)
        {
            QStringList urlList;
            QVector<PlayItem> single;
            QString url = Utils::makeDTVPath(channelInfo);

            urlList.append(url);

            Utils::getPlayItemFromFileNames(urlList, &single);

            if (!single.isEmpty())
            {
                DTVReaderInterface::Info info;

                if (reader.getAdapterInfo(channelInfo.adapter, &info))
                {
                    if (channelInfo.name.isEmpty())
                    {
                        single[0].title = QString("%1 (%2)")
                                .arg(channelInfo.channel)
                                .arg(info.name);
                    }
                    else
                    {
                        single[0].title = QString("%1 (%2, %3)")
                                .arg(channelInfo.name)
                                .arg(channelInfo.channel)
                                .arg(info.name);
                    }

                    single[0].extraData.userData = url.toUtf8();
                }

                itemVector += single;
            }
        }

        this->addToPlayList(itemVector, true, true);
    }
}

void MainWindow::openScanDTVChannel()
{
    DTVReader &reader = this->m_player->getDTVReader();
    QVector<DTVReaderInterface::Info> adapters;

    reader.getAdapterList(&adapters);

    if (!reader.isSupport())
        return;

    if (adapters.isEmpty())
    {
        Utils::informationMessageBox(this, trUtf8("DTV 수신 장치가 없습니다."));
        return;
    }

    OpenDTVScanChannel dlg(reader, this);

    if (dlg.exec() == QDialog::Accepted)
    {
        DTVReader &reader = this->m_player->getDTVReader();
        QVector<DTVReader::ChannelInfo> scanned;

        reader.getScannedChannels(&scanned);

        if (!scanned.isEmpty())
            this->addDTVChannelToPlaylist();
    }
}

void MainWindow::histEQ()
{
    if (this->m_player->isEnabledVideo())
    {
        FilterGraph &graph = this->m_player->getFilterGraph();
        bool use = !graph.getEnableHistEQ();

        graph.setEnableHistEQ(use);

        QString desc;

        if (use)
            desc = trUtf8("히스토그램 이퀄라이저 켜짐");
        else
            desc = trUtf8("히스토그램 이퀄라이저 꺼짐");

        this->m_player->showOptionDesc(desc);
        this->m_player->configFilterGraph();
    }
}

void MainWindow::hq3DDenoise()
{
    if (this->m_player->isEnabledVideo())
    {
        FilterGraph &graph = this->m_player->getFilterGraph();
        bool use = !graph.getEnableHighQuality3DDenoise();

        graph.setEnableHighQuality3DDenoise(use);

        QString desc;

        if (use)
            desc = trUtf8("3D 노이즈 제거 켜짐");
        else
            desc = trUtf8("3D 노이즈 제거 꺼짐");

        this->m_player->showOptionDesc(desc);
        this->m_player->configFilterGraph();
    }
}

void MainWindow::ataDenoise()
{
    if (this->m_player->isEnabledVideo())
    {
        FilterGraph &graph = this->m_player->getFilterGraph();
        bool use = !graph.getEnableATADenoise();

        graph.setEnableATADenoise(use);

        QString desc;

        if (use)
            desc = trUtf8("적응 시간 평균 노이즈 제거 켜짐");
        else
            desc = trUtf8("적응 시간 평균 노이즈 제거 꺼짐");

        this->m_player->showOptionDesc(desc);
        this->m_player->configFilterGraph();
    }
}

void MainWindow::owDenoise()
{
    if (this->m_player->isEnabledVideo())
    {
        FilterGraph &graph = this->m_player->getFilterGraph();
        bool use = !graph.getEnableOWDenoise();

        graph.setEnableOWDenoise(use);

        QString desc;

        if (use)
            desc = trUtf8("Overcomplete Wavelet 노이즈 제거 켜짐");
        else
            desc = trUtf8("Overcomplete Wavelet 노이즈 제거 꺼짐");

        this->m_player->showOptionDesc(desc);
        this->m_player->configFilterGraph();
    }
}

void MainWindow::deband()
{
    if (this->m_player->isEnabledVideo())
    {
        FilterGraph &graph = this->m_player->getFilterGraph();
        bool use = !graph.getEnableDeBand();

        graph.setEnableDeBand(use);

        QString desc;

        if (use)
            desc = trUtf8("디밴드 켜짐");
        else
            desc = trUtf8("디밴드 꺼짐");

        this->m_player->showOptionDesc(desc);
        this->m_player->configFilterGraph();
    }
}

void MainWindow::viewEPG()
{
    DTVReader &reader = this->m_player->getDTVReader();

    if (!reader.isSupport() || !reader.isOpened())
        return;

    QString channelName = this->m_playList.getPlayItem(this->m_currentPlayingIndex).title;
    ViewEPG dlg(reader, channelName, this);

    dlg.exec();
}

void MainWindow::useSearchSubtitleComplex()
{
    this->m_player->setSearchSubtitleComplex(!this->m_player->getSearchSubtitleComplex());
}

void MainWindow::useBufferingMode()
{
    this->m_player->useBufferingMode(!this->m_player->isUseBufferingMode());
}

void MainWindow::customShortcut()
{
    CustomShortcut dlg(this);

    dlg.exec();
}

void MainWindow::selectCaptureSaveDir()
{
    Screen *screen = this->ui->screen;
    QString dir = QFileDialog::getExistingDirectory(this, QString(), screen->getCaptureSavePath());

    if (!dir.isEmpty())
        screen->setCaptureSavePath(dir);
}

void MainWindow::openCaptureSaveDir()
{
    QString path = this->ui->screen->getCaptureSavePath();

    Utils::createDirectory(path);

    if (!QDesktopServices::openUrl(QUrl("file:///" + path)))
        QDesktopServices::openUrl(QUrl(QDir::toNativeSeparators("./")));
}

void MainWindow::fileAssociation()
{
    FileAssociation dlg(this->m_settings, this);

    dlg.exec();
}

void MainWindow::enableSearchSubtitle()
{
    this->m_player->enableSearchSubtitle(!this->m_player->isEnableSearchSubtitle());

    if (this->m_player->isEnableSearchSubtitle())
    {
        this->m_player->retreiveExternalSubtitle(this->m_player->getFilePath());
        this->checkGOMSubtitle();
    }
}

void MainWindow::enableSearchLyrics()
{
    this->m_player->enableSearchLyrics(!this->m_player->isEnableSearchLyrics());

    if (this->m_player->isEnableSearchLyrics())
    {
        this->m_player->retreiveExternalSubtitle(this->m_player->getFilePath());

        if (this->m_player->existAudioSubtitle())
            this->ui->audioSubtitle1->setText(QString());
    }
}

void MainWindow::subtitleVAlignOrder()
{
    if (this->m_player->isAlignable())
    {
        AnyVODEnums::VAlignMethod align = this->m_player->getVAlign();

        align = (AnyVODEnums::VAlignMethod)(align + 1);

        if (align >= AnyVODEnums::VAM_COUNT)
            align = AnyVODEnums::VAM_NONE;

        this->m_player->setVAlign(align);
    }
}

void MainWindow::selectSubtitleVAlignMethod(int method)
{
    if (this->m_player->isAlignable())
        this->m_player->setVAlign((AnyVODEnums::VAlignMethod)method);
}

void MainWindow::incSubtitleSize()
{
    if (this->m_player->isAlignable())
        this->m_player->addSubtitleSize(0.01f);
}

void MainWindow::decSubtitleSize()
{
    if (this->m_player->isAlignable())
        this->m_player->addSubtitleSize(-0.01f);
}

void MainWindow::resetSubtitleSize()
{
    if (this->m_player->isAlignable())
        this->m_player->resetSubtitleSize();
}

void MainWindow::userAspectRatioOrder()
{
    int index = this->m_userAspectRatioList.curIndex + 1;

    if (index >= this->m_userAspectRatioList.list.count())
        index = 0;

    this->selectUserAspectRatio(index);
}

void MainWindow::selectUserAspectRatio(int index)
{
    MediaPresenter::UserAspectRatio ratio;
    UserAspectRatioItem &item = this->m_userAspectRatioList.list[index];

    if (item.isInvalid())
    {
        ratio.use = false;
    }
    else if (item.isFullScreen())
    {
        ratio.use = true;
        ratio.fullscreen = true;
    }
    else
    {
        ratio.use = true;

        ratio.width = item.width;
        ratio.height = item.height;
    }

    this->m_userAspectRatioList.curIndex = index;
    this->m_player->setUserAspectRatio(ratio);
}

void MainWindow::userAspectRatio()
{
    UserAspectRatioItem &item = this->m_userAspectRatioList.list.last();
    UserAspectRatio dlg(item.width, item.height, this);

    if (dlg.exec() == QDialog::Accepted)
    {
        QSizeF size;

        dlg.getSize(&size);

        item.width = size.width();
        item.height = size.height();

        this->selectUserAspectRatio(this->m_userAspectRatioList.list.count() - 1);
    }
}

void MainWindow::screenSizeHalf()
{
    this->setScreenSizeRatio(0.5);
}

void MainWindow::screenSizeNormal()
{
    this->setScreenSizeRatio(1.0);
}

void MainWindow::screenSizeNormalHalf()
{
    this->setScreenSizeRatio(1.5);
}

void MainWindow::screenSizeDouble()
{
    this->setScreenSizeRatio(2.0);
}

void MainWindow::useHWDecoder()
{
    this->m_player->useHWDecoder(!this->m_player->isUseHWDecoder());
}

void MainWindow::useSPDIF()
{
    this->m_player->useSPDIF(!this->m_player->isUseSPDIF());
    this->adjustSPDIFAudioUI();
}

void MainWindow::userSPDIFSampleRateOrder()
{
    int index = this->m_userSPDIFSampleRateList.curIndex + 1;

    if (index >= this->m_userSPDIFSampleRateList.list.count())
        index = 0;

    this->selectUserSPDIFSampleRate(index);
}

void MainWindow::adjustSPDIFAudioUI()
{
    Ui::MainWindow *ctrl = this->ui;
    bool toggle = this->m_player->isUseSPDIF();

    ctrl->volume->setEnabled(!toggle);
    ctrl->mute->setEnabled(!toggle);
}

void MainWindow::selectUserSPDIFSampleRate(int index)
{
    this->m_userSPDIFSampleRateList.curIndex = index;
    this->m_player->setUserSPDIFSampleRate(this->m_userSPDIFSampleRateList.list[index]);

    this->adjustSPDIFAudioUI();
}

void MainWindow::usePBO()
{
    if (this->m_player->isUsablePBO())
        this->m_player->usePBO(!this->m_player->isUsePBO());
}

void MainWindow::method3DOrder()
{
    AnyVODEnums::Video3DMethod method = this->m_player->get3DMethod();

    method = (AnyVODEnums::Video3DMethod)(method + 1);

    if (method >= AnyVODEnums::V3M_COUNT)
        method = AnyVODEnums::V3M_NONE;

    this->m_player->set3DMethod(method);
    this->ui->screen->getShader().set3DMethod(method);
}

void MainWindow::useVSync()
{
    this->ui->screen->toggleUseVSync();
}

void MainWindow::selectLanguage(const QString &lang)
{
    this->setLanguage(lang);
}

void MainWindow::prevChapter()
{
    if (this->m_player->canMoveChapter())
    {
        double pos = this->m_player->getCurrentPosition();
        QString desc;

        if (this->moveChapter(this->getChapterIndex(pos) - 1, &desc))
            this->m_player->showOptionDesc(trUtf8("이전 챕터로 이동 (%1)").arg(desc));
    }
}

void MainWindow::nextChapter()
{
    if (this->m_player->canMoveChapter())
    {
        double pos = this->m_player->getCurrentPosition();
        QString desc;

        if (this->moveChapter(this->getChapterIndex(pos) + 1, &desc))
            this->m_player->showOptionDesc(trUtf8("다음 챕터로 이동 (%1)").arg(desc));
    }
}

bool MainWindow::moveChapter(int index, QString *desc)
{
    const QVector<ChapterInfo> &list = this->m_player->getChapters();

    if (index < 0 || index >= list.count())
        return false;

    this->m_player->seek(list[index].start, true);
    *desc = list[index].desc;

    return true;
}

int MainWindow::getChapterIndex(double pos)
{
    return Utils::findChapter(pos, this->m_player->getChapters());
}

void MainWindow::closeExternalSubtitle()
{
    if (this->m_player->existExternalSubtitle())
    {
        this->m_player->closeAllExternalSubtitles();
        this->resetAudioSubtitle();
    }
}

void MainWindow::clearPlayList()
{
    this->m_clearPlayList = !this->m_clearPlayList;
}

void MainWindow::showAlbumJacket()
{
    this->m_player->showAlbumJacket(!this->m_player->isShowAlbumJacket());
}

void MainWindow::lastPlay()
{
    this->m_player->enableGotoLastPos(!this->m_player->isGotoLastPos(), false);
}

void MainWindow::textEncoding()
{
    TextEncodeSetting dlg(this);
    int ret = dlg.exec();

    if (ret == QDialog::Accepted)
    {
        QString encoding;

        dlg.getEncoding(&encoding);

        if (encoding == TextEncodeSetting::DEFAULT_NAME)
            encoding = "System";

        Utils::setSubtitleCodecName(encoding);
    }
}

void MainWindow::serverSetting()
{
    ServerSetting dlg(this->m_remoteAddress, this->m_remoteCommandPort, this->m_remoteStreamPort, this);
    int ret = dlg.exec();

    if (ret == QDialog::Accepted)
    {
        dlg.getAddress(&this->m_remoteAddress);
        dlg.getCommandPort(&this->m_remoteCommandPort);
        dlg.getStreamPort(&this->m_remoteStreamPort);
    }
}

void MainWindow::saveSubtitle()
{
    if (this->m_player->existFileSubtitle())
        this->m_player->saveSubtitle();
}

void MainWindow::saveAsSubtitle()
{
    if (this->m_player->existFileSubtitle())
    {
        QFileInfo info(this->m_player->getSubtitlePath());
        QString type = this->m_player->isMovieSubtitleVisiable() ? trUtf8("자막") : trUtf8("가사");
        QString path = QFileDialog::getSaveFileName(this, QString(), info.absoluteFilePath(), trUtf8("%1 (*.%2);;모든 파일 (*.*)").arg(type).arg(info.suffix()));

        if (!path.isEmpty())
            this->m_player->saveSubtitleAs(path);
    }
}

void MainWindow::select3DMethod(int method)
{
    this->m_player->set3DMethod((AnyVODEnums::Video3DMethod)method);
    this->ui->screen->getShader().set3DMethod((AnyVODEnums::Video3DMethod)method);
}

MediaPlayer* MainWindow::getPlayer()
{
    return this->m_player;
}

QString MainWindow::getVersion() const
{
    return VERSION_TEMPLATE.arg(MAJOR).arg(MINOR).arg(PATCH).arg(BUILDNUM) + " " + STRFILEVER;
}

RemoteFileList& MainWindow::getRemoteFileListWindow()
{
    return this->m_remoteFileList;
}

PlayList& MainWindow::getPlayListWindow()
{
    return this->m_playList;
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    if (this->m_spectrumTimerID == event->timerId())
    {
        this->ui->spectrum->changeHandle(this->m_player->getAudioHandle());
        this->ui->spectrum->updateSpectrum();
    }
    else if (this->m_nonePlayingDescTimerID == event->timerId())
    {
        this->ui->audioOptionDesc->setVisible(false);
        this->ui->audioOptionDesc->setText(QString());
        this->killTimer(event->timerId());
        this->m_nonePlayingDescTimerID = 0;
    }
    else if (this->m_loginTimerID == event->timerId())
    {
        if (!this->isLogined())
        {
            this->m_remoteFileList.hide();
            this->m_remoteFileList.clearFileList();

            if (this->m_player->isRemoteFile())
                this->m_player->close();

            this->killTimer(event->timerId());
        }
    }
    else if (this->m_shareMemTimerID == event->timerId())
    {
        if (!this->m_startingPaths.isEmpty())
        {
            QVector<PlayItem> items;
            QVector<PlayItem> filterdItems;
            QString subtitle;

            Utils::getPlayItemFromFileNames(this->m_startingPaths, &items);

            foreach (const PlayItem &item, items)
            {
                QFileInfo info(item.path);

                if (SUBTITLE_EXTS_LIST.contains(info.suffix().toLower()))
                    subtitle = info.filePath();
                else
                    filterdItems.append(item);
            }

            this->addToPlayList(filterdItems, true, false);

            if (!subtitle.isEmpty())
                this->m_player->openSubtitle(Utils::adjustNetworkPath(subtitle), true);

            this->m_startingPaths.clear();
        }
        else if (this->m_shareMem.lock())
        {
            char *data = (char*)this->m_shareMem.data();
            QString path = QString::fromUtf8(data).trimmed();

            if (!path.isEmpty())
            {
                QStringList list = path.split('\n', QString::SkipEmptyParts);
                QVector<PlayItem> playList;
                QVector<PlayItem> filterdItems;
                QString subtitle;

                Utils::getPlayItemFromFileNames(list, &playList);

                foreach (const PlayItem &item, playList)
                {
                    QFileInfo info(item.path);

                    if (SUBTITLE_EXTS_LIST.contains(info.suffix().toLower()))
                        subtitle = info.filePath();
                    else
                        filterdItems.append(item);
                }

                this->addToPlayList(filterdItems, true, true);

                if (!subtitle.isEmpty())
                    this->m_player->openSubtitle(Utils::adjustNetworkPath(subtitle), true);

                memset(data, 0, SHARE_MEM_SIZE);

                QApplication::alert(this);
            }

            this->m_shareMem.unlock();
        }
    }
    else if (this->m_initCompleteTimerID == event->timerId())
    {
        this->m_isReady = true;

#ifdef Q_OS_WIN
        this->m_taskbarButton.setParent(this);
        this->m_taskbarButton.setWindow(this->windowHandle());

        this->initThumbnailBar();
#endif

        this->killTimer(this->m_initCompleteTimerID);
        this->m_initCompleteTimerID = 0;

        this->setFocus();
    }
}

void MainWindow::dropEvent(QDropEvent *event)
{
    const QMimeData *mime = event->mimeData();

    if (mime->hasUrls())
    {
        const QList<QUrl> urls = mime->urls();

        if (urls.count() == 1)
        {
            const QUrl &url = urls[0];

            if (Utils::determinRemoteProtocol(url.toString()))
            {
                QVector<PlayItem> itemVector;
                QString address = url.toString();

                this->getYouTubePlayItems(address, &itemVector);

                if (IS_BIT_SET(event->keyboardModifiers(), Qt::ControlModifier))
                    this->addToPlayList(itemVector, true, false);
                else
                    this->setPlayList(itemVector);

                this->activateWindow();
            }
            else
            {
                QFileInfo info(url.toLocalFile());

                if (SUBTITLE_EXTS_LIST.contains(info.suffix().toLower()))
                {
                    this->m_player->openSubtitle(Utils::adjustNetworkPath(info.filePath()), true);
                    return;
                }
            }
        }

        QStringList filePathList;
        bool ignoreExts = IS_BIT_SET(event->keyboardModifiers(), Qt::AltModifier | Qt::ControlModifier);

        for (int i = 0; i < urls.count(); i++)
            Utils::getLocalFileListOnlyMedia(urls[i].toLocalFile(), MEDIA_EXTS_LIST, ignoreExts, &filePathList);

        if (!filePathList.empty())
        {
            QVector<PlayItem> itemVector;

            Utils::getPlayItemFromFileNames(filePathList, &itemVector);

            if (IS_BIT_SET(event->keyboardModifiers(), Qt::ControlModifier))
                this->addToPlayList(itemVector, true, false);
            else
                this->setPlayList(itemVector);

            this->activateWindow();
        }
    }
}

void MainWindow::resetAudioSubtitle()
{
    Ui::MainWindow *ctrl = this->ui;
    QPalette pal;

    pal = ctrl->audioSubtitle1->palette();
    pal.setColor(QPalette::WindowText, QColor(0, 0, 0));

    ctrl->audioSubtitle1->setPalette(pal);
    ctrl->audioSubtitle2->setPalette(pal);
    ctrl->audioSubtitle3->setPalette(pal);

    ctrl->audioSubtitle1->setText(QString());
    ctrl->audioSubtitle2->setText(QString());
    ctrl->audioSubtitle3->setText(QString());
}

void MainWindow::customEvent(QEvent *event)
{
    Ui::MainWindow *ctrl = this->ui;

    if (event->type() == Screen::STATUS_CHANGE_EVENT)
    {
        Screen::StatusChangeEvent *e = (Screen::StatusChangeEvent*)event;
        MediaPlayer::Status status = e->getStatus();

        if (status == MediaPlayer::Playing)
        {
            ctrl->stop->setEnabled(true);
            ctrl->resume->setEnabled(false);

            if (this->m_player->hasDuration())
            {
                ctrl->back->setEnabled(true);
                ctrl->forw->setEnabled(true);
            }
            else
            {
                ctrl->back->setEnabled(false);
                ctrl->forw->setEnabled(false);
            }

            if (Utils::determinDevice(this->m_player->getFilePath()))
                ctrl->pause->setEnabled(false);
            else
                ctrl->pause->setEnabled(true);

            ctrl->volume->setEnabled(!this->m_player->isUseSPDIF());
            ctrl->mute->setEnabled(!this->m_player->isUseSPDIF());

            ctrl->pause->setVisible(true);
            ctrl->resume->setVisible(false);

            ctrl->trackBar->setMinimum(0);
            ctrl->trackBar->setMaximum(this->m_player->getDuration());

#ifdef Q_OS_WIN
            QWinTaskbarProgress *progress = this->m_taskbarButton.progress();

            progress->resume();

            this->m_thumbnailToolStop.setEnabled(true);
#endif

            if (this->m_player->isEnabledVideo())
            {
                QSize mainSize = ctrl->screen->size();
                int controlBarHeight = ctrl->controlBar->height();

                if (mainSize.height() <= SCREEN_MINIMUM_SIZE)
                    mainSize.setHeight(this->m_lastScreenSize.height() - mainSize.height());

                if (this->m_player->isAudio())
                {
                    mainSize.setWidth(this->width());
                }
                else
                {
                    if (mainSize.width() <= 0)
                        mainSize.setWidth(this->m_lastScreenSize.width());
                }

                this->setMinimumHeight(controlBarHeight);
                this->setMaximumHeight(QWIDGETSIZE_MAX);

                if (!this->isFullScreen())
                {
                    int centerHeight = ctrl->centralWidget->height();

                    if (ctrl->controlBar->isVisible())
                        mainSize.rheight() += controlBarHeight;

                    if (centerHeight > mainSize.height())
                        mainSize.rheight() += centerHeight - mainSize.height();

                    this->resize(mainSize);
                }

                this->m_lastScreenVisible = true;
            }
            else
            {
                if (this->isFullScreen())
                {
                    this->m_remoteFileList.setVisible(this->m_prevRemoteFileListVisible);
                    this->m_playList.setVisible(this->m_prevPlayListVisible);

                    this->setFullScreenMode(false);
                    this->showNormal();
                }

                if (this->m_lastScreenVisible)
                {
                    if (ctrl->screen->size().height() > 0)
                        this->m_lastScreenSize.setHeight(ctrl->screen->size().height());

                    if (ctrl->screen->size().width() > 0)
                        this->m_lastScreenSize.setWidth(ctrl->screen->size().width());

                    if (!ctrl->controlBar->isVisible())
                        this->m_lastScreenSize.rheight() -= CONTROL_BAR_HEIGHT - SCREEN_MINIMUM_SIZE;
                }

                this->m_lastScreenVisible = false;
                ctrl->controlBar->show();

                QSize controlBarSize = ctrl->controlBar->size();

                controlBarSize.setHeight(CONTROL_BAR_HEIGHT);
                this->resize(controlBarSize);
                this->setMinimumHeight(CONTROL_BAR_HEIGHT);
                this->setMaximumHeight(CONTROL_BAR_HEIGHT);
            }

            ctrl->screen->setVisible(this->m_lastScreenVisible);

            if (ctrl->spectrum->isPaused())
                this->resumeSpectrum();
        }
        else if (status == MediaPlayer::Paused)
        {
            ctrl->stop->setEnabled(true);
            ctrl->back->setEnabled(true);
            ctrl->forw->setEnabled(true);
            ctrl->pause->setEnabled(false);
            ctrl->resume->setEnabled(true);

            ctrl->pause->setVisible(false);
            ctrl->resume->setVisible(true);

            this->pauseSpectrum();

#ifdef Q_OS_WIN
            QWinTaskbarProgress *progress = this->m_taskbarButton.progress();

            progress->show();
            progress->pause();

            this->m_thumbnailToolStop.setEnabled(true);
#endif
        }
        else if (status == MediaPlayer::Started)
        {
            ctrl->trackBar->setChapters(this->m_player->getChapters());
            ctrl->trackBar->setEnabled(this->m_player->getDuration() > 0.0);

            this->startSpectrum();

            if (this->m_player->isMovieSubtitleVisiable())
            {
                ctrl->audioSubtitle->setVisible(false);
            }
            else
            {
                ctrl->audioSpecWidget->setVisible(true);
                ctrl->audioSubtitle->setVisible(true);

                if (!this->m_player->existAudioSubtitle())
                    ctrl->audioSubtitle1->setText(trUtf8("가사가 없습니다"));
            }

            this->setScreenSaverActivity(false);

#ifdef Q_OS_WIN
            QWinTaskbarProgress *progress = this->m_taskbarButton.progress();

            progress->setMinimum(0);
            progress->setMaximum(this->m_player->getDuration());

            progress->show();
            progress->resume();
#endif
        }
        else if (status == MediaPlayer::Stopped)
        {
            bool resume = this->m_player->isOpened() || this->m_playList.exist();

            ctrl->stop->setEnabled(false);
            ctrl->back->setEnabled(false);
            ctrl->forw->setEnabled(false);
            ctrl->pause->setEnabled(false);
            ctrl->resume->setEnabled(resume);
            ctrl->mute->setEnabled(resume);

            ctrl->pause->setVisible(false);
            ctrl->resume->setVisible(true);

            ctrl->trackBar->setMaximum(0);
            ctrl->trackBar->clearChapters();
            ctrl->trackBar->setEnabled(true);

            this->updateTrackBar();
            this->stopSpectrum();
            this->setScreenSaverActivity(true);

            ctrl->audioSpec->setText(QString());
            ctrl->audioSpecWidget->setVisible(false);
            ctrl->audioBuffering->setText(QString());
            ctrl->audioSubtitle->setVisible(!this->m_lastScreenVisible);
            ctrl->audioOptionDesc->setVisible(false);

            this->resetAudioSubtitle();

#ifdef Q_OS_WIN
            QWinTaskbarProgress *progress = this->m_taskbarButton.progress();

            progress->stop();
            progress->hide();

            this->m_thumbnailToolStop.setEnabled(false);
#endif
        }
        else if (status == MediaPlayer::Ended)
        {
            ctrl->stop->setEnabled(false);
            ctrl->back->setEnabled(true);
            ctrl->forw->setEnabled(true);
            ctrl->pause->setEnabled(false);
            ctrl->resume->setEnabled(true);

            ctrl->pause->setVisible(false);
            ctrl->resume->setVisible(true);

#ifdef Q_OS_WIN
            this->m_thumbnailToolStop.setEnabled(false);
#endif

            this->stopSpectrum();

            switch (this->m_playingMethod)
            {
                case AnyVODEnums::PM_TOTAL:
                {
                    if (this->m_currentPlayingIndex + 1 < this->m_playList.getCount())
                        this->playNext();
                    else
                        this->close();

                    break;
                }
                case AnyVODEnums::PM_TOTAL_REPEAT:
                {
                    if (this->m_currentPlayingIndex + 1 >= this->m_playList.getCount())
                        this->m_currentPlayingIndex = -1;

                    this->playNext();

                    break;
                }
                case AnyVODEnums::PM_SINGLE:
                {
                    this->close();
                    break;
                }
                case AnyVODEnums::PM_SINGLE_REPEAT:
                {
                    this->playCurrent();
                    break;
                }
                case AnyVODEnums::PM_RANDOM:
                {
                    this->m_currentPlayingIndex = rand() % this->m_playList.getCount();
                    this->playCurrent();

                    break;
                }
                default:
                {
                    break;
                }
            }
        }

#ifdef Q_OS_WIN
        this->setThumbnailButtonType(ctrl->resume->isEnabled());
#endif
    }
    else if (event->type() == Screen::PLAYING_EVENT)
    {
        this->updateTrackBar();
        this->updateAudioSpec();

#ifdef Q_OS_WIN
        QWinTaskbarProgress *progress = this->m_taskbarButton.progress();

        progress->setValue(this->m_player->getCurrentPosition());
#endif
        ctrl->audioBuffering->setText(QString());
    }
    else if (event->type() == Screen::AUDIO_OPTION_DESC_EVENT)
    {
        Screen::ShowAudioOptionDescEvent *e = (Screen::ShowAudioOptionDescEvent*)event;

        ctrl->audioOptionDesc->setVisible(e->getShow());
        ctrl->audioOptionDesc->setText(e->getDesc());
    }
    else if (event->type() == Screen::EMPTY_BUFFER_EVENT)
    {
        Screen::EmptyBufferEvent *e = (Screen::EmptyBufferEvent*)event;

        if (e->isEmpty())
            ctrl->audioBuffering->setText(trUtf8("버퍼링 중입니다"));
        else
            ctrl->audioBuffering->setText(QString());
    }
    else if (event->type() == Screen::AUDIO_SUBTITLE_EVENT)
    {
        if (!this->m_player->existExternalSubtitle())
            return;

        Screen::AudioSubtitleEvent *e = (Screen::AudioSubtitleEvent*)event;
        QVector<Lyrics> lines = e->getLines();
        QVector<Lyrics> mergedLines;
        QVector<Lyrics> *realLine = NULL;
        QVector<QLabel*> outputs;
        QPalette pal;
        int i;

        pal = ctrl->audioSubtitle1->palette();
        outputs.append(ctrl->audioSubtitle1);
        outputs.append(ctrl->audioSubtitle2);
        outputs.append(ctrl->audioSubtitle3);

        if (lines.count() > 0 && lines[0].text.indexOf('\n') >= 0)
        {
            QStringList texts = lines[0].text.split('\n');
            Lyrics lyrics;

            for (i = 0; i < texts.count(); i++)
            {
                lyrics.color = lines[0].color;
                lyrics.text = texts[i];

                mergedLines.append(lyrics);
            }

            realLine = &mergedLines;
        }
        else
        {
            realLine = &lines;
        }

        for (i = 0; i < outputs.size() && i < realLine->size(); i++)
        {
            QColor color;

            if (this->m_player->existAudioSubtitleGender())
            {
                color = (*realLine)[i].color;
            }
            else
            {
                if (i == 0)
                    color = QColor(0x40, 0x40, 0x40);
                else
                    color = Qt::darkGray;
            }

            color.setAlphaF(this->m_player->getSubtitleOpaque());

            pal.setColor(QPalette::WindowText, color);

            outputs[i]->setPalette(pal);
            outputs[i]->setText((*realLine)[i].text.trimmed().remove(QRegExp("\\r|\\n")));
        }

        for (; i < outputs.size(); i++)
            outputs[i]->setText(QString());
    }
    else if (event->type() == Screen::ABORT_EVENT)
    {
        char buf[2048] = {0, };
        Screen::AbortEvent *e = (Screen::AbortEvent*)event;

        av_strerror(e->getReason(), buf, sizeof(buf));
        Utils::criticalMessageBox(this, trUtf8("다음 에러로 중지 되었습니다 : %1").arg(buf));

        this->m_player->close();
    }
    else if (event->type() == Screen::NONE_PLAYING_DESC_EVENT)
    {
        Screen::NonePlayingDescEvent *e = (Screen::NonePlayingDescEvent*)event;

        ctrl->audioOptionDesc->setVisible(true);
        ctrl->audioOptionDesc->setText(e->getDesc());

        if (this->m_nonePlayingDescTimerID != 0)
            this->killTimer(this->m_nonePlayingDescTimerID);

        this->m_nonePlayingDescTimerID = this->startTimer(MediaPresenter::OPTION_DESC_TIME);
    }
    else
    {
        QWidget::customEvent(event);
    }
}

void MainWindow::updateTrackBar()
{
    Ui::MainWindow *ctrl = this->ui;
    double currentPosition = this->m_player->getCurrentPosition();

    if (currentPosition < 0.0)
        return;

    if (!ctrl->trackBar->isSliderDown())
        ctrl->trackBar->setValue(currentPosition);

    QString current;
    QString duration;
    QString format = Utils::TIME_HH_MM_SS;

    ctrl->currentTime->setText(*Utils::getTimeString(currentPosition, format, &current));
    ctrl->totalTime->setText(*Utils::getTimeString(this->m_player->getDuration(), format, &duration));
}

void MainWindow::updateAudioSpec()
{
    QString title;

    if (this->m_player->isAudio() || !this->m_player->isEnabledVideo())
    {
        QString temp("%L1kbps %2bits %3kHz (%4 %5)");
        const MediaPresenter::Detail &detail = this->m_player->getDetail();
        QString channel;

        if (detail.audioInputChannels == 1)
            channel = trUtf8("모노");
        else if (detail.audioInputChannels <= 3)
            channel = trUtf8("스테레오");
        else
            channel = trUtf8("서라운드");

        title = temp
                .arg(detail.audioInputByteRate / 1000 * 8)
                .arg(detail.audioInputBits)
                .arg(detail.audioInputSampleRate / 1000.0f, 0, 'f', 1)
                .arg(channel)
                .arg(detail.audioCodecSimple);
    }

    this->ui->audioSpec->setText(title);

    if (title.isEmpty())
        this->ui->audioSpecWidget->setVisible(false);
}

void MainWindow::updatePlayUI()
{
    this->m_player->mute(this->ui->mute->isChecked(), false);
}

void MainWindow::startSpectrum()
{
    if (this->m_spectrumTimerID == 0)
    {
        this->ui->spectrum->init(this->m_player->getAudioHandle());
        this->m_spectrumTimerID = this->startTimer(Spectrum::SPECTRUM_ELAPSE);
    }
}

void MainWindow::stopSpectrum()
{
    if (this->m_spectrumTimerID != 0)
    {
        this->killTimer(this->m_spectrumTimerID);
        this->m_spectrumTimerID = 0;

        this->ui->spectrum->update();
        this->ui->spectrum->deInit();
    }
}

void MainWindow::pauseSpectrum()
{
    this->ui->spectrum->pause();
}

void MainWindow::resumeSpectrum()
{
    this->ui->spectrum->resume();
}

void MainWindow::checkGOMSubtitle()
{
    QString url;

    this->m_player->getGOMSubtitleURL(&url);

    if (url.count() > 0)
        this->m_popup.showText(trUtf8("외부 서버에 자막이 존재합니다.\n이동하시려면 여기를 클릭하세요."), url);
}

bool MainWindow::open(const QString &filePath, ExtraPlayData &data, const QUuid &unique)
{
    this->m_player->close();
    this->m_popup.hide();

    PlayItem item = this->m_playList.getPlayItem(this->m_playList.findIndex(unique));
    QString title = item.title;
    QString audioPath;

    if (!item.youtubeData.isEmpty())
    {
        foreach (const YouTubeURLPicker::Item &yItem, item.youtubeData)
        {
            if (!yItem.dashmpd.isEmpty() && yItem.url == filePath)
            {
                YouTubeURLPicker picker;

                picker.getAudioStreamByMimeType(yItem.dashmpd, yItem.mimeType, &audioPath);

                break;
            }
        }
    }

    if (this->m_player->open(filePath, title, data, unique, this->m_env.fontFamily, this->m_env.fontSize,
                             this->m_env.subtitleOutlineSize, audioPath))
    {
        if (this->m_player->play())
        {
            this->updatePlayUI();
            this->checkGOMSubtitle();

            return true;
        }
    }

    Utils::criticalMessageBox(this, trUtf8("파일을 열 수 없습니다"));

    return false;
}

void MainWindow::close()
{
    this->resetWindowTitle();
    this->resetCurrentPlayingIndex();
    this->m_player->close();
}

void MainWindow::restart()
{
    if (this->m_player->isOpened())
    {
        this->m_player->stop();

        if (!(this->m_player->reOpen() && this->m_player->play()))
        {
            Utils::criticalMessageBox(this, trUtf8("파일을 열 수 없습니다"));
        }
        else
        {
            this->updatePlayUI();
            this->checkGOMSubtitle();
        }
    }
    else if (this->m_playList.exist())
    {
        this->playCurrent();
    }
    else
    {
        this->open();
    }
}

bool MainWindow::isAutoStart() const
{
    return !this->m_playList.exist() || !this->m_player->isPlayOrPause();
}

void MainWindow::adjustPlayIndex()
{
    if (this->m_currentPlayingIndex >= this->m_playList.getCount())
        this->m_currentPlayingIndex = this->m_playList.getCount() - 1;
    else if (this->m_currentPlayingIndex < 0)
        this->m_currentPlayingIndex = 0;
    else if (this->m_playList.getCount() <= 0)
        this->resetCurrentPlayingIndex();
}

Screen* MainWindow::getScreen()
{
    return this->ui->screen;
}

bool MainWindow::playAt(int index)
{
    this->m_currentPlayingIndex = index;
    return this->playCurrent();
}

void MainWindow::updateNavigationButtons()
{
    Ui::MainWindow *ctrl = this->ui;

    if (this->m_playList.getCount() <= 1)
    {
        ctrl->prev->setEnabled(false);
        ctrl->next->setEnabled(false);

#ifdef Q_OS_WIN
        this->m_thumbnailToolBackward.setEnabled(false);
        this->m_thumbnailToolForward.setEnabled(false);
#endif
    }
    else if (this->m_currentPlayingIndex - 1 < 0)
    {
        ctrl->prev->setEnabled(false);
        ctrl->next->setEnabled(true);

#ifdef Q_OS_WIN
        this->m_thumbnailToolBackward.setEnabled(false);
        this->m_thumbnailToolForward.setEnabled(true);
#endif
    }
    else if (this->m_currentPlayingIndex + 1 >= this->m_playList.getCount())
    {
        ctrl->prev->setEnabled(true);
        ctrl->next->setEnabled(false);

#ifdef Q_OS_WIN
        this->m_thumbnailToolBackward.setEnabled(true);
        this->m_thumbnailToolForward.setEnabled(false);
#endif
    }
    else
    {
        ctrl->prev->setEnabled(true);
        ctrl->next->setEnabled(true);

#ifdef Q_OS_WIN
        this->m_thumbnailToolBackward.setEnabled(true);
        this->m_thumbnailToolForward.setEnabled(true);
#endif
    }
}

bool MainWindow::playCurrent()
{
    bool success = false;

    this->adjustPlayIndex();
    this->updateNavigationButtons();

    if (this->m_playList.exist())
    {
        PlayItem item = this->m_playList.getPlayItem(this->m_currentPlayingIndex);

        if (item.path.startsWith(Utils::ANYVOD_PROTOCOL))
        {
            item.path += Utils::FFMPEG_SEPARATOR;
            item.path += QString().setNum((qulonglong)&this->m_player->getVirtualFile());

            if (!this->isLogined())
            {
                this->logout();
                this->login();

                if (!this->isLogined())
                {
                    this->m_player->close();
                    return false;
                }
            }
        }
        else if (item.path.startsWith(Utils::DTV_PROTOCOL))
        {
            item.path += Utils::FFMPEG_SEPARATOR;
            item.path += QString().setNum((qulonglong)&this->m_player->getDTVReader());
        }

        QVector<YouTubeURLPicker::Item> youtube;
        YouTubeURLPicker picker;
        QString path;

        youtube = picker.pickURL(item.path);

        if (youtube.isEmpty())
        {
            path = item.path;
        }
        else
        {
            int index = this->m_playList.updateYouTubeData(item.unique, youtube);

            path = youtube[index].url;
        }

        success = this->open(path, item.extraData, item.unique);

        if (success)
        {
            this->setCurrentPlayingIndexByUnique();
            this->updateNavigationButtons();
        }
        else
        {
            if (Utils::questionMessageBox(this, trUtf8("재생 목록에서 삭제 하시겠습니까?")))
            {
                int oldIndex = this->m_currentPlayingIndex;

                this->m_playList.deleteItem(this->m_currentPlayingIndex);
                this->close();
                this->m_currentPlayingIndex = oldIndex;
                this->adjustPlayIndex();
                this->setFocus();
            }
        }

        this->updateWindowTitle();
        this->m_playList.selectItem(this->m_currentPlayingIndex);
    }
    else
    {
        this->resetWindowTitle();
    }

    return success;
}

bool MainWindow::playPrev()
{
    this->m_currentPlayingIndex--;
    return this->playCurrent();
}


bool MainWindow::playNext()
{
    this->m_currentPlayingIndex++;
    return this->playCurrent();
}

bool MainWindow::addToPlayList(const QVector<PlayItem> &list, bool enableAuto, bool forceStart)
{
    if (list.count() <= 0)
        return false;

    bool autoStart = (this->isAutoStart() && enableAuto) || forceStart;
    int curIndex = this->m_playList.getCount();
    bool added = this->m_playList.addPlayList(list);

    if (autoStart && added)
    {
        this->m_currentPlayingIndex = curIndex;
        this->playCurrent();
    }
    else
    {
        if (this->m_player->isOpened())
            this->updateWindowTitle();
    }

    return autoStart;
}

void MainWindow::setPlayList(const QVector<PlayItem> &list)
{
    Ui::MainWindow *ctrl = this->ui;

    this->m_playList.setPlayList(list);
    this->resetCurrentPlayingIndex();
    this->playNext();

    ctrl->prev->setEnabled(false);

    if (list.count() == 1)
        ctrl->next->setEnabled(false);
}

void MainWindow::processMouseUp()
{
    this->m_buttonPressed = false;
}

void MainWindow::processMouseDown()
{
    this->m_buttonPressed = true;
    this->m_prevPos = QCursor::pos();
}

void MainWindow::processMouseMoving()
{
    QPoint mousePos = QCursor::pos();

    if (this->m_buttonPressed && !this->isFullScreen())
    {
        QPoint currentPos = this->pos();
        QPoint delta = mousePos - this->m_prevPos;

        if (delta.manhattanLength() >= QApplication::startDragDistance())
        {
            this->move(currentPos + delta);
            this->m_prevPos = mousePos;
        }
    }

    if (this->isFullScreen())
    {
        QWidget *ctlBar = this->ui->controlBar;
        int ctlHeight = ctlBar->height();
        int screenHeight = this->height();
        int mouseYPosInScreen = this->mapFromGlobal(mousePos).y();
        int mousePosHeight = screenHeight - mouseYPosInScreen;

        if (mousePosHeight <= ctlHeight && !ctlBar->isVisible())
            ctlBar->setVisible(true);
        else if (mousePosHeight > ctlHeight && ctlBar->isVisible())
            ctlBar->setVisible(false);
    }
}

void MainWindow::volume(int maxVolume, int dir)
{
    if (this->m_player->isUseSPDIF())
        return;

    Ui::MainWindow *ctrl = this->ui;
    int volume = maxVolume * dir;

    volume = qCeil(volume * 0.05);
    volume += ctrl->volume->value();

    ctrl->volume->setValue(volume);

    if (volume < 0)
        this->m_player->volume(0);
    else if (volume > maxVolume)
        this->m_player->volume(maxVolume);

    if (ctrl->mute->isChecked())
        ctrl->mute->setChecked(false);
}

void MainWindow::opaque(int dir)
{
    QSlider *ctrl = this->ui->opaque;
    int maxValue = ctrl->maximum();
    int opaque = maxValue * dir;

    opaque = qCeil(opaque * 0.05);
    opaque += ctrl->value();

    this->changeOpaque(opaque);
}

void MainWindow::changeOpaque(int value)
{
    this->ui->opaque->setValue(value);
}

void MainWindow::rewind(double distance)
{
    if (this->m_player->hasDuration())
    {
        this->m_player->rewind(distance);

        this->updateTrackBar();
        this->setSeekingText();
    }
}

void MainWindow::forward(double distance)
{
    if (this->m_player->hasDuration())
    {
        this->m_player->forward(distance);

        this->updateTrackBar();
        this->setSeekingText();
    }
}

void MainWindow::setFullScreenMode(bool enable)
{
    this->ui->centralWidget->layout()->setContentsMargins(0, enable ? SCREEN_TOP_OFFSET : 0, 0, 0);
    this->m_isFullScreening = enable;
}

void MainWindow::startMedia(const QString &path)
{
    QFileInfo info(path);

    if (SUBTITLE_EXTS_LIST.contains(info.suffix().toLower()))
    {
        this->m_player->openSubtitle(Utils::adjustNetworkPath(info.filePath()), true);
    }
    else
    {
        QVector<PlayItem> items;

        Utils::getPlayItemFromFileNames(QStringList(path), &items);
        this->addToPlayList(items, true, true);
    }
}

void MainWindow::setScreenSizeRatio(double ratio)
{
    int width;
    int height;

    if (this->m_player->getFrameSize(&width, &height))
    {
        QSize size(width, height);

        size *= ratio;

        if (this->isFullScreen())
            this->fullScreen();

        if (this->ui->controlBar->isVisible())
            size.rheight() += this->ui->controlBar->height();

        this->resize(size);
        this->m_player->showOptionDesc(trUtf8("화면 크기 %1배").arg(ratio));
    }
}

void MainWindow::setLanguage(const QString &lang)
{
    QString localName = QLocale::system().name();
    QString localLang = localName.split('_').first();

    QApplication::removeTranslator(&this->m_langQt);
    QApplication::removeTranslator(&this->m_langQtBase);
    QApplication::removeTranslator(&this->m_langAnyVOD);

    QString temp;
    bool loaded;
    QLocale english(QLocale::English);

    if (english.name().split('_').first() != lang)
    {
        temp = "qt_%1";

        loaded = this->m_langQt.load(temp.arg(lang), LANG_DIR);

        if (!loaded)
            loaded = this->m_langQt.load(temp.arg(localLang), LANG_DIR);

        if (!loaded)
            loaded = this->m_langQt.load(temp.arg(localName), LANG_DIR);

        if (loaded)
            QApplication::installTranslator(&this->m_langQt);

        temp = "qtbase_%1";

        loaded = this->m_langQtBase.load(temp.arg(lang), LANG_DIR);

        if (!loaded)
            loaded = this->m_langQtBase.load(temp.arg(localLang), LANG_DIR);

        if (!loaded)
            loaded = this->m_langQtBase.load(temp.arg(localName), LANG_DIR);

        if (loaded)
            QApplication::installTranslator(&this->m_langQtBase);
    }

    temp = "%1_%2";

    loaded = this->m_langAnyVOD.load(temp.arg(LANG_PREFIX).arg(lang), LANG_DIR);

    if (!loaded)
        loaded = this->m_langAnyVOD.load(temp.arg(LANG_PREFIX).arg(localLang), LANG_DIR);

    if (!loaded)
        loaded = this->m_langAnyVOD.load(temp.arg(LANG_PREFIX).arg(localName), LANG_DIR);

    if (loaded)
        QApplication::installTranslator(&this->m_langAnyVOD);

    this->reloadKey();
    this->setup3DMethod();
    this->setupSubtitle3DMethod();
    this->setupAnaglyphAlgorithm();

#ifdef Q_OS_WIN
    this->translateThumbnailText();
#endif

    this->m_lang = lang;
}

void MainWindow::setScreenSaverActivity(bool enable)
{
#ifdef Q_OS_MAC
    if (enable)
    {
        IOPMAssertionRelease(this->m_systemSleepAssertionID);
        this->m_systemSleepAssertionID = 0;
    }
    else
    {
        QString name = APP_NAME + " playback";
        CFStringRef key = CFStringCreateWithCString(kCFAllocatorDefault, name.toUtf8(), kCFStringEncodingUTF8);

        IOPMAssertionCreateWithName(kIOPMAssertionTypeNoDisplaySleep, kIOPMAssertionLevelOn, key, &this->m_systemSleepAssertionID);
        CFRelease(key);
    }
#elif defined Q_OS_LINUX
    if (this->m_dbusConn == NULL)
        return;

    if (this->m_dbusPending != NULL)
    {
        DBusMessage *reply;

        dbus_pending_call_block(this->m_dbusPending);
        reply = dbus_pending_call_steal_reply(this->m_dbusPending);
        dbus_pending_call_unref(this->m_dbusPending);
        this->m_dbusPending = NULL;

        if (reply != NULL)
        {
            if (!dbus_message_get_args(reply, NULL, DBUS_TYPE_UINT32, &this->m_dbusCookie, DBUS_TYPE_INVALID))
                this->m_dbusCookie = 0;

            dbus_message_unref(reply);
        }
    }

    const char *method = enable ? "UnInhibit" : "Inhibit";
    ScreenSaverAPI api = this->m_screenSaverAPI;
    DBusMessage *msg = dbus_message_new_method_call(DBUS_SERVICE[api], DBUS_PATH[api], DBUS_SERVICE[api], method);

    if (msg == NULL)
        return;

    if (enable)
    {
        if (!dbus_message_append_args(msg, DBUS_TYPE_UINT32, &this->m_dbusCookie, DBUS_TYPE_INVALID) ||
                !dbus_connection_send(this->m_dbusConn, msg, NULL))
            this->m_dbusCookie = 0;
    }
    else
    {
        const char *app = APP_NAME.toLatin1();
        const char *reason = "playback";
        dbus_bool_t ret;

        switch (api)
        {
            case SSAPI_MATE:
            case SSAPI_GNOME:
            {
                dbus_uint32_t xid = 0;
                dbus_uint32_t gflags = 0xC;

                ret = dbus_message_append_args(msg, DBUS_TYPE_STRING, &app, DBUS_TYPE_UINT32, &xid,
                                               DBUS_TYPE_STRING, &reason, DBUS_TYPE_UINT32, &gflags,
                                               DBUS_TYPE_INVALID);
                break;
            }
            default:
            {
                ret = dbus_message_append_args(msg, DBUS_TYPE_STRING, &app, DBUS_TYPE_STRING, &reason,
                                               DBUS_TYPE_INVALID);
                break;
            }
        }

        if (!ret || !dbus_connection_send_with_reply(this->m_dbusConn, msg, &this->m_dbusPending, -1))
            this->m_dbusPending = NULL;
    }

    dbus_connection_flush(this->m_dbusConn);
    dbus_message_unref(msg);
#else
    (void)enable;
#endif
}

void MainWindow::on_pause_clicked()
{
    this->m_player->pause();
}

void MainWindow::on_stop_clicked()
{
    this->stop();
}

void MainWindow::on_resume_clicked()
{
    if (!this->m_player->isPlayOrPause())
        this->restart();
    else
        this->m_player->resume();
}

void MainWindow::on_volume_valueChanged(int value)
{
    this->m_player->volume(value);
}

void MainWindow::on_trackBar_sliderMoved(int position)
{
    if (!this->m_player->isRemoteFile())
        this->m_player->seek(position, !this->ui->trackBar->isDragging());
}

void MainWindow::on_trackBar_sliderReleased()
{
    if (this->m_player->isRemoteFile())
    {
        this->setSeekingText();
        this->m_player->seek(this->ui->trackBar->value(), true);
    }
}

void MainWindow::on_opaque_valueChanged(int value)
{
    qreal opaque = value / (qreal)this->ui->opaque->maximum();

    this->setWindowOpacity(opaque);
    this->m_remoteFileList.setWindowOpacity(opaque);
    this->m_playList.setWindowOpacity(opaque);

    QString desc = trUtf8("투명도 (%1%)");

    this->m_player->showOptionDesc(desc.arg((int)(ceil(opaque * 100))));
}

void MainWindow::on_mute_toggled(bool checked)
{
    this->m_player->mute(checked, true);
}

void MainWindow::login()
{
    if (!this->isLogined())
    {
        Login login(this->m_remoteAddress, this->m_remoteCommandPort, this->m_remoteStreamPort, this);
        int ret = login.exec();

        if (ret == QDialog::Accepted)
        {
            QString error;
            QList<ANYVOD_FILE_ITEM> fileList;

            this->m_loginTimerID = this->startTimer(LOGIN_TIME);

            if (Socket::getInstance().requestFilelist(RemoteFileList::ROOT_DIR, &error, &fileList))
            {
                this->m_remoteFileList.setFileList(fileList, RemoteFileList::ROOT_DIR);
                this->m_remoteFileList.show();
            }
            else
            {
                Utils::criticalMessageBox(this, error);
            }
        }
    }
}

void MainWindow::logout()
{
    QString error;

    Socket::getInstance().logout(&error);
    this->m_remoteFileList.clearFileList();

    if (this->m_player->isRemoteFile())
        this->close();

    Socket::getStreamInstance().logout(&error);
}

void MainWindow::mostTop()
{
    bool fullscreen = this->isFullScreen();
    unsigned int flags = this->windowFlags();
    unsigned int topFlag = Qt::WindowStaysOnTopHint | Qt::X11BypassWindowManagerHint;

    if (IS_BIT_SET(flags, topFlag))
        flags &= ~topFlag;
    else
        flags |= topFlag;

    if (fullscreen)
        this->fullScreen();

#ifdef Q_OS_WIN
    HWND topMost;

    topMost = IS_BIT_SET(flags, Qt::WindowStaysOnTopHint) ? HWND_TOPMOST : HWND_NOTOPMOST;

    SetWindowPos((HWND)this->winId(), topMost, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOACTIVATE);
#endif

    this->setWindowFlags((Qt::WindowFlags)flags);
    this->show();
    this->activateWindow();

    if (fullscreen)
        this->fullScreen();

    QString desc;

    if (this->isMostTop())
        desc = trUtf8("항상 위 켜짐");
    else
        desc = trUtf8("항상 위 꺼짐");

    this->m_player->showOptionDesc(desc);
}

void MainWindow::open()
{
    QString filter = trUtf8("지원하는 모든 파일 (*.%1);;동영상 (*.%2);;음악 (*.%3);;자막 / 가사 (*.%4);;재생 목록 (*.%5);;모든 파일 (*.*)").arg(
                ALL_EXTS_LIST.join(" *."),
                MOVIE_EXTS_LIST.join(" *."),
                AUDIO_EXTS_LIST.join(" *."),
                SUBTITLE_EXTS_LIST.join(" *."),
                PLAYLIST_EXTS_LIST.join(" *."));
    QString file = QFileDialog::getOpenFileName(this, QString(), this->m_lastSelectedDir, filter, NULL, QFileDialog::HideNameFilterDetails);

    if (!file.isEmpty())
    {
        this->startMedia(file);
        this->m_lastSelectedDir = QFileInfo(file).absolutePath();
    }
}

void MainWindow::getYouTubePlayItems(const QString &address, QVector<PlayItem> *itemVector)
{
    QVector<YouTubeURLPicker::Item> youtube;
    YouTubeURLPicker picker;

    youtube = picker.pickURL(address);

    if (youtube.isEmpty())
    {
        QStringList list;

        list.append(address);
        Utils::getPlayItemFromFileNames(list, itemVector);
    }
    else
    {
        bool isPlayList = picker.isPlayList();

        if (isPlayList)
        {
            QVector<YouTubeURLPicker::Item> playlist;

            if (youtube.length() > 0)
                playlist = picker.pickURL(youtube.first().url);

            youtube = playlist.mid(0, 1) + youtube.mid(1);
        }

        Utils::getPlayItemFromYouTube(youtube, isPlayList, itemVector);
    }
}

void MainWindow::openExternal()
{
    OpenExternal dlg(this);
    int ret = dlg.exec();

    if (ret == QDialog::Accepted)
    {
        QVector<PlayItem> itemVector;
        QString address;

        dlg.getAddress(&address);

        this->getYouTubePlayItems(address, &itemVector);
        this->addToPlayList(itemVector, true, true);
    }
}

void MainWindow::openRemoteFileList()
{
    if (this->isLogined())
        this->m_remoteFileList.setVisible(!this->m_remoteFileList.isVisible());
}

void MainWindow::openPlayList()
{
    this->m_playList.setVisible(!this->m_playList.isVisible());
}

void MainWindow::audioNormalize()
{
    this->m_player->useNormalizer(!this->m_player->isUsingNormalizer());
}

void MainWindow::audioEqualizer()
{
    this->m_player->useEqualizer(!this->m_player->isUsingEqualizer());
}

void MainWindow::selectSubtitleClass(const QString &className)
{
    QString currentName;

    this->m_player->getCurrentSubtitleClass(&currentName);

    if (currentName != className)
    {
        if (!this->m_player->setCurrentSubtitleClass(className))
        {
            QString desc;

            if (this->m_player->isMovieSubtitleVisiable())
                desc = trUtf8("자막을 변경 할 수 없습니다");
            else
                desc = trUtf8("가사를 변경 할 수 없습니다");

            Utils::informationMessageBox(this, desc);
        }
    }
}

void MainWindow::selectAudioStream(int index)
{
    if (this->m_player->getCurrentAudioStreamIndex() != index)
    {
        if (!this->m_player->changeAudioStream(index))
            Utils::informationMessageBox(this, trUtf8("음성을 변경 할 수 없습니다"));
    }
}

void MainWindow::equalizerSetting()
{
    Equalizer dlg(this->m_player, this);

    dlg.exec();
}

void MainWindow::resetSubtitlePosition()
{
    if (this->m_player->isSubtitleMoveable())
        this->m_player->resetSubtitlePosition();
}

void MainWindow::upSubtitlePosition()
{
    if (this->m_player->isSubtitleMoveable())
        this->m_player->setVerticalSubtitlePosition(1);
}

void MainWindow::downSubtitlePosition()
{
    if (this->m_player->isSubtitleMoveable())
        this->m_player->setVerticalSubtitlePosition(-1);
}

void MainWindow::leftSubtitlePosition()
{
    if (this->m_player->isSubtitleMoveable())
        this->m_player->setHorizontalSubtitlePosition(1);
}

void MainWindow::rightSubtitlePosition()
{
    if (this->m_player->isSubtitleMoveable())
        this->m_player->setHorizontalSubtitlePosition(-1);
}

void MainWindow::selectPlayingMethod(int method)
{
    this->setPlayingMethod((AnyVODEnums::PlayingMethod)method);
}

void MainWindow::toggle()
{
    if (!this->m_player->isPlayOrPause())
    {
        this->restart();
    }
    else
    {
        if (!Utils::determinDevice(this->m_player->getFilePath()))
            this->m_player->toggle();
    }
}

void MainWindow::stop()
{
    this->m_player->stop();
}

void MainWindow::fullScreen()
{
    Ui::MainWindow *ctrl = this->ui;

    if ((this->m_player->isEnabledVideo() || !this->m_player->isPlayOrPause()) &&
            ctrl->screen->size().height() > 0 && ctrl->screen->isVisible())
    {
        if (this->isFullScreen())
        {
            this->m_remoteFileList.setVisible(this->m_prevRemoteFileListVisible);
            this->m_playList.setVisible(this->m_prevPlayListVisible);
            ctrl->controlBar->show();

            this->setFullScreenMode(false);

            if (this->m_prevMaximized)
                this->showMaximized();
            else
                this->showNormal();

            this->m_player->showOptionDesc(trUtf8("일반 화면"));
        }
        else
        {
            ctrl->controlBar->hide();
            this->m_prevMaximized = this->isMaximized();
            this->m_prevRemoteFileListVisible = this->m_remoteFileList.isVisible();
            this->m_prevPlayListVisible = this->m_playList.isVisible();
            this->m_remoteFileList.hide();
            this->m_playList.hide();

            this->setFullScreenMode(true);
            this->showFullScreen();

            this->m_player->showOptionDesc(trUtf8("전체 화면"));
        }

        this->activateWindow();
    }
}

void MainWindow::rewind5()
{
    this->rewind(5.0);
}

void MainWindow::forward5()
{
    this->forward(5.0);
}

void MainWindow::rewind30()
{
    this->rewind(30.0);
}

void MainWindow::forward30()
{
    this->forward(30.0);
}

void MainWindow::rewind60()
{
    this->rewind(60.0);
}

void MainWindow::forward60()
{
    this->forward(60.0);
}

void MainWindow::gotoBegin()
{
    if (this->m_player->hasDuration())
    {
        this->m_player->seek(0.0, false);
        this->m_player->showOptionDesc(trUtf8("처음으로 이동"));
    }
}

void MainWindow::prev()
{
    if (this->isPrevEnabled())
        this->playPrev();
}

void MainWindow::next()
{
    if (this->isNextEnabled())
        this->playNext();
}

void MainWindow::playOrder()
{
    AnyVODEnums::PlayingMethod method = this->getPlayingMethod();

    method = (AnyVODEnums::PlayingMethod)(method + 1);

    if (method >= AnyVODEnums::PM_COUNT)
        method = AnyVODEnums::PM_TOTAL;

    this->setPlayingMethod(method);
}

void MainWindow::subtitleToggle()
{
    this->m_player->showSubtitle(!this->m_player->isShowSubtitle());
}

void MainWindow::prevSubtitleSync()
{
    if (this->m_player->existSubtitle())
        this->m_player->prevSubtitleSync(0.5);
}

void MainWindow::nextSubtitleSync()
{
    if (this->m_player->existSubtitle())
        this->m_player->nextSubtitleSync(0.5);
}

void MainWindow::resetSubtitleSync()
{
    if (this->m_player->existSubtitle())
        this->m_player->resetSubtitleSync();
}

void MainWindow::subtitleLanguageOrder()
{
    QStringList classes;
    QString curClass;

    this->m_player->getSubtitleClasses(&classes);
    this->m_player->getCurrentSubtitleClass(&curClass);

    if (curClass.isEmpty() && classes.count() > 0)
    {
        this->selectSubtitleClass(classes[0]);
        return;
    }

    for (int i = 0; i < classes.count(); i++)
    {
        if (curClass == classes[i])
        {
            QString selClass;

            if (i + 1 >= classes.count())
                selClass = classes[0];
            else
                selClass = classes[i + 1];

            this->selectSubtitleClass(selClass);

            break;
        }
    }
}

void MainWindow::volumeUp()
{
    this->volume(this->m_player->getMaxVolume(), 1);
}

void MainWindow::volumeDown()
{
    this->volume(this->m_player->getMaxVolume(), -1);
}

void MainWindow::mute()
{
    if (!this->m_player->isUseSPDIF())
        this->ui->mute->setChecked(!this->ui->mute->isChecked());
}

void MainWindow::audioOrder()
{
    QVector<AudioStreamInfo> info;
    int index = this->m_player->getCurrentAudioStreamIndex();

    this->m_player->getAudioStreamInfo(&info);

    if (index < 0 && info.count() > 0)
    {
        this->selectAudioStream(info[0].index);
        return;
    }

    for (int i = 0; i < info.count(); i++)
    {
        if (index == info[i].index)
        {
            int selIndex;

            if (i + 1 >= info.count())
                selIndex = info[0].index;
            else
                selIndex = info[i + 1].index;

            this->selectAudioStream(selIndex);

            break;
        }
    }
}

void MainWindow::info()
{
    QString desc;
    QString version;
    uint versionInt;

    version = this->getVersion();
    desc += MainWindow::APP_NAME + " : " + version;
    desc += "\n";

    desc += "Built on " __DATE__ " at " __TIME__;
    desc += "\n";

    desc += "Author : DongRyeol Cha (chadr@dcple.com)";
    desc += "\n\n";

    desc += "Qt - ";
    desc += qVersion();
    desc += "\n";

    desc += "ass - ";
    versionInt = ass_library_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 28).arg(((versionInt >> 24 & 0x0f) * 10) + (versionInt >> 20 & 0x0f)).arg(versionInt >> 12 & 0x0f).arg(0);
    desc += version;
    desc += "\n";

    desc += "avcodec - ";
    versionInt = avcodec_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "avformat - ";
    versionInt = avformat_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "avdevice - ";
    versionInt = avdevice_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "avfilter - ";
    versionInt = avfilter_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "avutil - ";
    versionInt = avutil_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "swscale - ";
    versionInt = swscale_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "swresample - ";
    versionInt = swresample_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "bass - ";
    versionInt = BASS_GetVersion();
    version = VERSION_TEMPLATE.arg(versionInt >> 24).arg(versionInt >> 16 & 0xff).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff);
    desc += version;
    desc += "\n";

    desc += "bass_fx - ";
    versionInt = BASS_FX_GetVersion();
    version = VERSION_TEMPLATE.arg(versionInt >> 24).arg(versionInt >> 16 & 0xff).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff);
    desc += version;
    desc += "\n";

    desc += "bassmix - ";
    versionInt = BASS_Mixer_GetVersion();
    version = VERSION_TEMPLATE.arg(versionInt >> 24).arg(versionInt >> 16 & 0xff).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff);
    desc += version;
    desc += "\n";

    desc += QString::fromStdWString(MediaInfoDLL::MediaInfo::Option_Static(__T("Info_Version"), __T(""))).remove('v');
    desc += "\n";

    desc += "zlib - ";
    desc += zlibVersion();
    desc += "\n";

    Utils::informationMessageBox(this, desc);
}

void MainWindow::detail()
{
    if (this->m_player->isEnabledVideo())
        this->m_player->showDetail(!this->m_player->isShowDetail());
}

void MainWindow::showControlBar()
{
    Ui::MainWindow *ctrl = this->ui;

    if (this->canShowControlBar())
    {
        ctrl->controlBar->setVisible(!ctrl->controlBar->isVisible());

        QString desc;

        if (ctrl->controlBar->isVisible())
            desc = trUtf8("컨트롤바 보임");
        else
            desc = trUtf8("컨트롤바 숨김");

        this->m_player->showOptionDesc(desc);
    }
}

void MainWindow::incOpaque()
{
    this->opaque(1);
}

void MainWindow::decOpaque()
{
    this->opaque(-1);
}

void MainWindow::maxOpaque()
{
    this->changeOpaque(this->ui->opaque->maximum());
}

void MainWindow::minOpaque()
{
    this->changeOpaque(this->ui->opaque->minimum());
}

void MainWindow::deinterlaceMethodOrder()
{
    AnyVODEnums::DeinterlaceMethod method = this->m_player->getDeinterlaceMethod();

    method = (AnyVODEnums::DeinterlaceMethod)(method + 1);

    if (method >= AnyVODEnums::DM_COUNT)
        method = AnyVODEnums::DM_AUTO;

    this->m_player->setDeinterlaceMethod(method);
}

void MainWindow::deinterlaceAlgorithmOrder()
{
    AnyVODEnums::DeinterlaceAlgorithm algorithm = this->m_player->getDeinterlaceAlgorithm();

    algorithm = (AnyVODEnums::DeinterlaceAlgorithm)(algorithm + 1);

    if (algorithm >= AnyVODEnums::DA_COUNT)
        algorithm = AnyVODEnums::DA_BLEND;

    this->m_player->setDeinterlaceAlgorithm(algorithm);
}

void MainWindow::selectDeinterlacerMethod(int method)
{
    this->m_player->setDeinterlaceMethod((AnyVODEnums::DeinterlaceMethod)method);
}

void MainWindow::selectDeinterlacerAlgorithem(int algorithm)
{
    this->m_player->setDeinterlaceAlgorithm((AnyVODEnums::DeinterlaceAlgorithm)algorithm);
}

void MainWindow::prevAudioSync()
{
    this->m_player->prevAudioSync(0.05);
}

void MainWindow::nextAudioSync()
{
    this->m_player->nextAudioSync(0.05);
}

void MainWindow::resetAudioSync()
{
    this->m_player->resetAudioSync();
}

void MainWindow::subtitleHAlignOrder()
{
    if (this->m_player->isAlignable())
    {
        AnyVODEnums::HAlignMethod align = this->m_player->getHAlign();

        align = (AnyVODEnums::HAlignMethod)(align + 1);

        if (align >= AnyVODEnums::HAM_COUNT)
            align = AnyVODEnums::HAM_NONE;

        this->m_player->setHAlign(align);
    }
}

void MainWindow::selectSubtitleHAlignMethod(int method)
{
    if (this->m_player->isAlignable())
        this->m_player->setHAlign((AnyVODEnums::HAlignMethod)method);
}

void MainWindow::repeatRangeStart()
{
    if (this->m_player->hasDuration())
    {
        double pos = this->m_player->getCurrentPosition();

        this->m_player->setRepeatStart(pos);
    }
}

void MainWindow::repeatRangeEnd()
{
    if (this->m_player->hasDuration())
    {
        double pos = this->m_player->getCurrentPosition();

        this->m_player->setRepeatEnd(pos);
    }
}

void MainWindow::repeatRangeEnable()
{
    this->m_player->setRepeatEnable(!this->m_player->getRepeatEnable());
}

void MainWindow::repeatRangeStartBack100MS()
{
    if (this->m_player->hasDuration())
        this->m_player->setRepeatStartMove(-0.1);
}

void MainWindow::repeatRangeStartForw100MS()
{
    if (this->m_player->hasDuration())
        this->m_player->setRepeatStartMove(0.1);
}

void MainWindow::repeatRangeEndBack100MS()
{
    if (this->m_player->hasDuration())
        this->m_player->setRepeatEndMove(-0.1);
}

void MainWindow::repeatRangeEndForw100MS()
{
    if (this->m_player->hasDuration())
        this->m_player->setRepeatEndMove(0.1);
}

void MainWindow::repeatRangeBack100MS()
{
    if (this->m_player->hasDuration())
        this->m_player->setRepeatMove(-0.1);
}

void MainWindow::repeatRangeForw100MS()
{
    if (this->m_player->hasDuration())
        this->m_player->setRepeatMove(0.1);
}

void MainWindow::seekKeyFrame()
{
    this->m_player->setSeekKeyFrame(!this->m_player->isSeekKeyFrame());
}

void MainWindow::skipOpening()
{
    this->m_player->setSkipOpening(!this->m_player->getSkipOpening());
}

void MainWindow::skipEnding()
{
    this->m_player->setSkipEnding(!this->m_player->getSkipEnding());
}

void MainWindow::useSkipRange()
{
    this->m_player->setUseSkipRange(!this->m_player->getUseSkipRange());
}

void MainWindow::openSkipRange()
{
    QVector<MediaPresenter::Range> ranges;
    SkipRange dlg(this->m_player, &ranges, this);

    if (dlg.exec() == QDialog::Accepted)
        this->m_player->setSkipRanges(ranges);
}

void MainWindow::captureExtOrder()
{
    Screen::CaptureInfo info;
    int index = 0;

    this->ui->screen->getCaptureInfo(&info);

    for (; index < CAPTURE_FORMAT_LIST.count(); index++)
    {
        if (CAPTURE_FORMAT_LIST.at(index).toLower() == info.ext)
            break;
    }

    index++;

    if (index >= CAPTURE_FORMAT_LIST.count())
        index = 0;

    info.ext = CAPTURE_FORMAT_LIST.at(index);

    this->ui->screen->setCaptureInfo(info);
}

void MainWindow::captureSingle()
{
    if (this->m_player->isEnabledVideo())
    {
        Screen::CaptureInfo info;

        this->ui->screen->getCaptureInfo(&info);

        info.capture = true;
        info.captureOrg = true;
        info.captureCount = 1;
        info.totalCount = 1;

        this->ui->screen->setCaptureInfo(info);
    }
}

void MainWindow::captureMultiple()
{
    if (this->m_player->isEnabledVideo())
    {
        Screen *screen = this->ui->screen;
        MultipleCapture dlg(screen, this->m_player, this);
        bool showingSubtitle = this->m_player->isShowSubtitle();
        bool showingDetail = this->m_player->isShowDetail();
        Screen::CaptureInfo info;

        dlg.exec();

        this->m_player->showSubtitle(showingSubtitle, true);
        this->m_player->showDetail(showingDetail);

        screen->getCaptureInfo(&info);
        info.capture = false;
        info.captureCount = 0;
        info.totalCount = 0;
        screen->setCaptureInfo(info, true);
    }
}

void MainWindow::selectCaptureExt(const QString &ext)
{
    Screen::CaptureInfo info;

    this->ui->screen->getCaptureInfo(&info);
    info.ext = ext.toLower();
    this->ui->screen->setCaptureInfo(info);
}

void MainWindow::prevFrame()
{
    if (this->m_player->canMoveFrame())
        this->m_player->prevFrame(1);
}

void MainWindow::nextFrame()
{
    if (this->m_player->canMoveFrame())
        this->m_player->nextFrame(1);
}

void MainWindow::slowerPlayback()
{
    if (this->m_player->isTempoUsable())
    {
        int cur = (int)this->m_player->getTempo();

        if (cur > -90)
            this->m_player->setTempo(cur - 10.0f);
    }
}

void MainWindow::fasterPlayback()
{
    if (this->m_player->isTempoUsable())
    {
        int cur = (int)this->m_player->getTempo();

        this->m_player->setTempo(cur + 10.0f);
    }
}

void MainWindow::normalPlayback()
{
    if (this->m_player->isTempoUsable())
        this->m_player->setTempo(0.0f);
}

void MainWindow::resetVideoAttribute()
{
    if (this->m_player->isEnabledVideo())
    {
        ShaderCompositer &shader = this->ui->screen->getShader();

        shader.setBrightness(1.0);
        shader.setContrast(1.0);
        shader.setHue(0.0);
        shader.setSaturation(1.0);

        this->m_player->showOptionDesc(trUtf8("영상 속성 초기화"));
    }
}

void MainWindow::brightnessDown()
{
    this->brightness(-0.01);
}

void MainWindow::brightnessUp()
{
    this->brightness(0.01);
}

void MainWindow::brightness(double step)
{
    if (this->m_player->isEnabledVideo())
    {
        ShaderCompositer &shader = this->ui->screen->getShader();
        double value = shader.getBrightness();

        value += step;
        value = Utils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        shader.setBrightness(value);
        this->m_player->showOptionDesc(trUtf8("영상 밝기 %1배").arg(value));
    }
}

void MainWindow::saturationDown()
{
   this->saturation(-0.01);
}

void MainWindow::saturationUp()
{
   this->saturation(0.01);
}

void MainWindow::saturation(double step)
{
    if (this->m_player->isEnabledVideo())
    {
        ShaderCompositer &shader = this->ui->screen->getShader();
        double value = shader.getSaturation();

        value += step;
        value = Utils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        shader.setSaturation(value);
        this->m_player->showOptionDesc(trUtf8("영상 채도 %1배").arg(value));
    }
}

void MainWindow::hueDown()
{
    this->hue(-1.0 / 360.0);
}

void MainWindow::hueUp()
{
    this->hue(1.0 / 360.0);
}

void MainWindow::hue(double step)
{
    if (this->m_player->isEnabledVideo())
    {
        ShaderCompositer &shader = this->ui->screen->getShader();
        double value = shader.getHue();

        value += step;
        value = Utils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        if (value > 1.0)
            value = 1.0;

        shader.setHue(value);
        this->m_player->showOptionDesc(trUtf8("영상 색상 각도 : %1°").arg((int)(value * 360.0)));
    }
}

void MainWindow::contrastDown()
{
    this->contrast(-0.01);
}

void MainWindow::contrastUp()
{
   this->contrast(0.01);
}

void MainWindow::contrast(double step)
{
    if (this->m_player->isEnabledVideo())
    {
        ShaderCompositer &shader = this->ui->screen->getShader();
        double value = shader.getContrast();

        value += step;
        value = Utils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        shader.setContrast(value);
        this->m_player->showOptionDesc(trUtf8("영상 대비 %1배").arg(value));
    }
}

void MainWindow::openShaderCompositer()
{
    if (this->m_player->isEnabledVideo())
    {
        Shader dlg(&this->ui->screen->getShader(), this);

        dlg.exec();
    }
}

void MainWindow::lowerMusic()
{
    this->m_player->useLowerMusic(!this->m_player->isUsingLowerMusic());
}

void MainWindow::lowerVoice()
{
    this->m_player->useLowerVoice(!this->m_player->isUsingLowerVoice());
}

void MainWindow::higherVoice()
{
    this->m_player->useHigherVoice(!this->m_player->isUsingHigherVoice());
}

void MainWindow::sharply()
{
    if (this->m_player->isEnabledVideo())
    {
        ShaderCompositer &shader = this->ui->screen->getShader();
        bool use = !shader.isUsingSharply();

        shader.useSharply(use);

        QString desc;

        if (use)
            desc = trUtf8("영상 날카롭게 켜짐");
        else
            desc = trUtf8("영상 날카롭게 꺼짐");

        this->m_player->showOptionDesc(desc);
    }
}

void MainWindow::sharpen()
{
    if (this->m_player->isEnabledVideo())
    {
        ShaderCompositer &shader = this->ui->screen->getShader();
        bool use = !shader.isUsingSharpen();

        shader.useSharpen(use);

        QString desc;

        if (use)
            desc = trUtf8("영상 선명하게 켜짐");
        else
            desc = trUtf8("영상 선명하게 꺼짐");

        this->m_player->showOptionDesc(desc);
    }
}

void MainWindow::soften()
{
    if (this->m_player->isEnabledVideo())
    {
        ShaderCompositer &shader = this->ui->screen->getShader();
        bool use = !shader.isUsingSoften();

        shader.useSoften(use);

        QString desc;

        if (use)
            desc = trUtf8("영상 부드럽게 켜짐");
        else
            desc = trUtf8("영상 부드럽게 꺼짐");

        this->m_player->showOptionDesc(desc);
    }
}

void MainWindow::leftRightInvert()
{
    if (this->m_player->isEnabledVideo())
    {
        ShaderCompositer &shader = this->ui->screen->getShader();
        bool use = !shader.isUsingLeftRightInvert();

        shader.useLeftRightInvert(use);

        QString desc;

        if (use)
            desc = trUtf8("영상 좌우 반전 켜짐");
        else
            desc = trUtf8("영상 좌우 반전 꺼짐");

        this->m_player->showOptionDesc(desc);
    }
}

void MainWindow::topBottomInvert()
{
    if (this->m_player->isEnabledVideo())
    {
        ShaderCompositer &shader = this->ui->screen->getShader();
        bool use = !shader.isUsingTopBottomInvert();

        shader.useTopBottomInvert(use);

        QString desc;

        if (use)
            desc = trUtf8("영상 상하 반전 켜짐");
        else
            desc = trUtf8("영상 상하 반전 꺼짐");

        this->m_player->showOptionDesc(desc);
    }
}

void MainWindow::addToPlayList()
{
    QString filter = trUtf8("지원하는 모든 파일 (*.%1);;동영상 (*.%2);;음악 (*.%3);;재생 목록 (*.%4);;모든 파일 (*.*)").arg(
                ALL_EXTS_LIST.join(" *."),
                MOVIE_EXTS_LIST.join(" *."),
                AUDIO_EXTS_LIST.join(" *."),
                PLAYLIST_EXTS_LIST.join(" *."));
    QStringList files = QFileDialog::getOpenFileNames(this, trUtf8("재생 목록에 추가"), this->m_lastSelectedDir, filter, NULL, QFileDialog::HideNameFilterDetails);

    if (!files.isEmpty())
    {
        QVector<PlayItem> itemVector;

        Utils::getPlayItemFromFileNames(files, &itemVector);
        this->addToPlayList(itemVector, false, false);
        this->m_lastSelectedDir = QFileInfo(files[0]).absolutePath();
    }
}

void MainWindow::incSubtitleOpaque()
{
    if (this->m_player->existSubtitle())
        this->m_player->addSubtitleOpaque(0.05f);
}

void MainWindow::decSubtitleOpaque()
{
    if (this->m_player->existSubtitle())
        this->m_player->addSubtitleOpaque(-0.05f);
}

void MainWindow::resetSubtitleOpaque()
{
    if (this->m_player->existSubtitle())
        this->m_player->resetSubtitleOpaque();
}

void MainWindow::exit()
{
    QWidget::close();
}

bool MainWindow::nativeEvent(const QByteArray &, void *message, long *result)
{
#ifdef Q_OS_WIN
    MSG *msg = (MSG*)message;

    if (msg->message == WM_SYSCOMMAND)
    {
        if (msg->wParam == SC_MONITORPOWER || msg->wParam == SC_SCREENSAVE)
        {
            if (this->m_player->isPlayOrPause() || this->isActiveWindow())
            {
                *result = 0;
                return true;
            }
        }
    }
#else
    (void)message;
    (void)result;
#endif

    return false;
}

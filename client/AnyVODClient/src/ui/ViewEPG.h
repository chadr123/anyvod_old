﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <QDialog>
#include <QDateTime>

class QListWidgetItem;

namespace Ui
{
    class ViewEPG;
}

class DTVReader;

class ViewEPG : public QDialog
{
    Q_OBJECT

public:
    explicit ViewEPG(DTVReader &reader, const QString channelName, QWidget *parent = 0);
    ~ViewEPG();

private:
    void updateCurrentDateTime();

private slots:
    void on_items_itemClicked(QListWidgetItem *item);

private:
    virtual void changeEvent(QEvent *event);
    virtual void timerEvent(QTimerEvent *event);

private:
    Ui::ViewEPG *ui;
    DTVReader &m_reader;
    int m_updateTimer;
    int m_timeTimer;
    QDateTime m_curDateTime;
    QDateTime m_startDateTime;
};

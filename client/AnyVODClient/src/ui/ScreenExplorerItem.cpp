﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "ScreenExplorerItem.h"
#include "ui_screenexploreritem.h"
#include "core/Utils.h"

#include <QLabel>
#include <QPushButton>
#include <QDebug>

ScreenExplorerItem::ScreenExplorerItem(double time, double rotation, const QImage &screen, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ScreenExplorerItem)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    QString strTime;
    int height = parent->height() - this->ui->time->height() * 2;
    QImage img = screen.scaledToHeight(height, Qt::SmoothTransformation);

    Utils::getTimeString(time, Utils::TIME_HH_MM_SS, &strTime);

    QTransform trans;

    trans.translate((img.width() / 2.0f), (img.height() / 2.0f));
    trans.rotate(rotation);
    trans.translate((-img.width() / 2.0f), (-img.height() / 2.0f));

    this->ui->time->setText(strTime);
    this->ui->screen->setPixmap(QPixmap::fromImage(img.transformed(trans)));
}

ScreenExplorerItem::~ScreenExplorerItem()
{
    delete ui;
}

void ScreenExplorerItem::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

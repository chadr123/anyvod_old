﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "Trackbar.h"
#include "core/Utils.h"

#include <limits>
#include <QMouseEvent>
#include <QPainter>
#include <QDebug>

using namespace std;

Trackbar::Trackbar(QWidget *parent) :
    Slider(parent)
{
    this->setMouseTracking(true);
}

void Trackbar::setChapters(const QVector<ChapterInfo> &chapters)
{
    this->m_chapters = chapters;
}

void Trackbar::clearChapters()
{
    this->m_chapters.clear();
    this->update();
}

void Trackbar::mousePressEvent(QMouseEvent *ev)
{
    if (ev->button() == Qt::LeftButton)
    {
        int pos;

        this->getPosition(ev->pos(), &pos);

        this->setSliderDown(true);
        this->setSliderPosition(pos);

        this->setDrag(true);

        ev->accept();
    }

    QSlider::mousePressEvent(ev);
}

void Trackbar::mouseReleaseEvent(QMouseEvent *ev)
{
    QSlider::mouseReleaseEvent(ev);

    if (ev->button() == Qt::LeftButton)
    {
        this->setSliderDown(false);
        this->setDrag(false);

        ev->accept();
    }
}

void Trackbar::mouseMoveEvent(QMouseEvent *ev)
{
    QString time;
    QString chapter;
    QString length;

    if (this->maximum() <= 0)
    {
        time = trUtf8("재생 위치");
    }
    else
    {
        int pos = 0;
        ChapterInfo info;

        this->getPosition(ev->pos(), &pos);
        Utils::getTimeString(pos, Utils::TIME_HH_MM_SS, &time);

        if (this->findChapter(pos, &info))
        {
            int len;

            if (info.end == numeric_limits<int>::max())
                info.end = this->maximum();

            len = info.end - info.start;
            Utils::getTimeString(len, Utils::TIME_HH_MM_SS, &length);

            length = " (" + length + ")";
            chapter = info.desc;
        }
    }

    if (!chapter.isEmpty())
        chapter = " - " + chapter;

    this->setToolTip(time + chapter + length);

    QSlider::mouseMoveEvent(ev);
}

void Trackbar::paintEvent(QPaintEvent *ev)
{
    QPainter painter;
    const int penSize = 2;
    const int lineHeight = 5;
    const int offset = penSize / 2;

    painter.begin(this);

    painter.setPen(QPen(this->palette().foreground().color(), penSize));

    for (int i = 0; i < this->m_chapters.count(); i++)
    {
        const ChapterInfo &info = this->m_chapters[i];
        int pixel = this->getPixelPosition(info.start);
        QPoint start;
        QPoint end;

        start.setX(pixel + offset);
        start.setY(this->height() - lineHeight);

        end.setX(pixel + offset);
        end.setY(this->height());

        painter.drawLine(start, end);
    }

    painter.end();

    Slider::paintEvent(ev);
}

bool Trackbar::findChapter(double pos, ChapterInfo *ret) const
{
    int index = Utils::findChapter(pos, this->m_chapters);

    if (index >= 0)
    {
        *ret = this->m_chapters[index];
        return true;
    }

    return false;
}

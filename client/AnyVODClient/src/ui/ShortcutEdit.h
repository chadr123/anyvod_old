﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <QLineEdit>
#include <QKeySequence>
#include <QEvent>

class ShortcutEdit : public QLineEdit
{
    Q_OBJECT
public:
    static const QEvent::Type COMPLETE_EVENT;
    static const QEvent::Type CLEAR_EVENT;

public:
    class CompleteEvent : public QEvent
    {
    public:
        CompleteEvent(const QKeySequence &seq, ShortcutEdit *sender) :
            QEvent(COMPLETE_EVENT),
            m_seq(seq),
            m_sender(sender)
        {

        }

        QKeySequence getkey() const { return this->m_seq; }
        ShortcutEdit* getSender() const { return this->m_sender; }

    private:
        QKeySequence m_seq;
        ShortcutEdit *m_sender;
    };

    class ClearEvent : public QEvent
    {
    public:
        ClearEvent(ShortcutEdit *sender) :
            QEvent(CLEAR_EVENT),
            m_sender(sender)
        {

        }

        ShortcutEdit* getSender() { return this->m_sender; }

    private:
        ShortcutEdit *m_sender;
    };

    explicit ShortcutEdit(QWidget *parent = 0);

private:
    virtual void keyPressEvent(QKeyEvent *event);
    virtual void keyReleaseEvent(QKeyEvent *event);
    virtual bool event(QEvent *e);

private:
    bool isVaildKey(int key) const;

private:
    bool m_validKey;
};

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "Equalizer.h"
#include "ui_equalizer.h"
#include "media/MediaPlayer.h"
#include "core/Utils.h"

#include <QMenu>
#include <QSignalMapper>
#include <QFile>
#include <QTextStream>
#include <QInputDialog>

const QString Equalizer::EQUALIZER_FILENAME = "equalizer.ini";
const QString Equalizer::EQUALIZER_ITEM_DELIMITER = "/";

QDataStream& operator << (QDataStream &out, const Equalizer::EqualizerItem &item)
{
    out << item.gain;

    return out;
}

QDataStream& operator >> (QDataStream &in, Equalizer::EqualizerItem &item)
{
    in >> item.gain;

    return in;
}

Equalizer::Equalizer(MediaPlayer *player, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Equalizer),
    m_player(player),
    m_preAmp(0.0f),
    m_prevUseEqualizer(false)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    Ui::Equalizer *ctrl = this->ui;

    this->m_preAmp = player->getPreAmp();
    ctrl->preamp->setValue(this->dBToValue(this->m_preAmp));
    ctrl->preamp_value->setText(QString().sprintf("%.1f", this->m_preAmp));

    for (int i = 0; i < player->getBandCount(); i++)
    {
        EqualizerItem gain;

        gain.gain = player->getEqualizerGain(i);

        this->m_gains.append(gain);

        QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(i));
        QLabel *label = (QLabel*)this->findChild<QLabel*>(QString("value%1").arg(i));

        slider->setValue(this->dBToValue(gain.gain));
        label->setText(QString().sprintf("%.1f", gain.gain));
    }

    ctrl->useEqualizer->setChecked(player->isUsingEqualizer() && !player->isUseSPDIF());
    ctrl->useEqualizer->setEnabled(!player->isUseSPDIF());

    this->m_prevUseEqualizer = ctrl->useEqualizer->isChecked();

    this->initDefaultNames();
    this->initPreset();
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

Equalizer::~Equalizer()
{
    delete ui;
}

void Equalizer::initDefaultNames() const
{
    trUtf8("기본값");
    trUtf8("클래식");
    trUtf8("베이스");
    trUtf8("베이스 & 트레블");
    trUtf8("트레블");
    trUtf8("헤드폰");
    trUtf8("홀");
    trUtf8("소프트 락");
    trUtf8("클럽");
    trUtf8("댄스");
    trUtf8("라이브");
    trUtf8("파티");
    trUtf8("팝");
    trUtf8("레게");
    trUtf8("락");
    trUtf8("스카");
    trUtf8("소프트");
    trUtf8("테크노");
    trUtf8("보컬");
    trUtf8("재즈");
}

float Equalizer::valueTodB(int value)
{
    return (float)value / 10.0f;
}

int Equalizer::dBToValue(float dB)
{
    return (int)(dB * 10.0f);
}

void Equalizer::canceled()
{
    this->m_player->setPreAmp(this->m_preAmp);

    for (int i = 0; i < this->m_gains.count(); i++)
        this->m_player->setEqualizerGain(i, this->m_gains[i].gain);

    if (this->ui->useEqualizer->isChecked() != this->m_prevUseEqualizer)
        this->m_player->useEqualizer(this->m_prevUseEqualizer);
}

void Equalizer::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void Equalizer::on_reset_clicked()
{
    Ui::Equalizer *ctrl = this->ui;

    ctrl->preamp->setValue(0);
    ctrl->preamp_value->setText(QString().sprintf("%.1f", 0.0f));

    this->on_preamp_valueChanged(0);

    for (int i = 0; i < this->m_player->getBandCount(); i++)
    {
        QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(i));
        QLabel *label = (QLabel*)this->findChild<QLabel*>(QString("value%1").arg(i));

        slider->setValue(0);
        label->setText(QString().sprintf("%.1f", 0.0f));
    }

    this->on_band0_valueChanged(0);
    this->on_band1_valueChanged(0);
    this->on_band2_valueChanged(0);
    this->on_band3_valueChanged(0);
    this->on_band4_valueChanged(0);
    this->on_band5_valueChanged(0);
    this->on_band6_valueChanged(0);
    this->on_band7_valueChanged(0);
    this->on_band8_valueChanged(0);
    this->on_band9_valueChanged(0);
}

void Equalizer::on_preamp_valueChanged(int value)
{
    this->ui->preamp_value->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setPreAmp(this->valueTodB(value));
}

void Equalizer::on_band0_valueChanged(int value)
{
    this->ui->value0->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setEqualizerGain(0, this->valueTodB(value));
}

void Equalizer::on_band1_valueChanged(int value)
{
    this->ui->value1->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setEqualizerGain(1, this->valueTodB(value));
}

void Equalizer::on_band2_valueChanged(int value)
{
    this->ui->value2->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setEqualizerGain(2, this->valueTodB(value));
}

void Equalizer::on_band3_valueChanged(int value)
{
    this->ui->value3->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setEqualizerGain(3, this->valueTodB(value));
}

void Equalizer::on_band4_valueChanged(int value)
{
    this->ui->value4->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setEqualizerGain(4, this->valueTodB(value));
}

void Equalizer::on_band5_valueChanged(int value)
{
    this->ui->value5->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setEqualizerGain(5, this->valueTodB(value));
}

void Equalizer::on_band6_valueChanged(int value)
{
    this->ui->value6->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setEqualizerGain(6, this->valueTodB(value));
}

void Equalizer::on_band7_valueChanged(int value)
{
    this->ui->value7->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setEqualizerGain(7, this->valueTodB(value));
}

void Equalizer::on_band8_valueChanged(int value)
{
    this->ui->value8->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setEqualizerGain(8, this->valueTodB(value));
}

void Equalizer::on_band9_valueChanged(int value)
{
    this->ui->value9->setText(QString().sprintf("%.1f", this->valueTodB(value)));
    this->m_player->setEqualizerGain(9, this->valueTodB(value));
}

void Equalizer::on_preset_clicked()
{
    this->presetMenu(SLOT(selectPreset(int)));
}

void Equalizer::on_del_preset_clicked()
{
    this->presetMenu(SLOT(deletePreset(int)));
}

void Equalizer::on_add_preset_clicked()
{
    bool ok;
    QString name = QInputDialog::getText(this, trUtf8("프리셋 추가"), trUtf8("프리셋 이름 :"), QLineEdit::Normal, QString(), &ok);

    if (ok && !name.isEmpty())
    {
        Preset preset;
        EqualizerItem gain;

        preset.desc = name;

        for (int i = 0; i < this->m_player->getBandCount(); i++)
        {
            QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(i));

            gain.gain = this->valueTodB(slider->value());
            preset.gains.append(gain);
        }

        this->m_presets.append(preset);
    }
}

void Equalizer::on_save_preset_clicked()
{
    this->presetMenu(SLOT(savePreset(int)));
}

void Equalizer::on_useEqualizer_clicked(bool checked)
{
    this->m_player->useEqualizer(checked);
}

void Equalizer::savePresetToFile()
{
    QFile file(EQUALIZER_FILENAME);

    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        stream.setCodec("UTF-8");
        stream.setGenerateByteOrderMark(true);

        for (int i = 0; i < this->m_presets.count(); i++)
        {
            Preset &preset = this->m_presets[i];

            stream << preset.desc;

            for (int i = 0; i < preset.gains.count(); i++)
                stream << EQUALIZER_ITEM_DELIMITER << preset.gains[i].gain;

            stream << endl;
        }
    }
}

void Equalizer::presetMenu(const char *slot)
{
    QMenu menu;
    QSignalMapper subtitleMapper;
    int count = this->m_presets.count();
    bool check = QString(SLOT(selectPreset(int))) == slot;

    if (count > 0)
    {
        for (int i = 0; i < count; i++)
        {
            Preset &preset = this->m_presets[i];
            QString name = preset.desc;
            QAction *subAction = menu.addAction(name.replace("&", "&&"));

            if (check)
            {
                bool same = true;

                for (int j = 0; j < preset.gains.count() && j < this->m_player->getBandCount(); j++)
                {
                    QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(j));
                    float gain = this->valueTodB(slider->value());

                    same &= preset.gains[j].gain == gain;
                }

                subAction->setCheckable(true);
                subAction->setChecked(same);
            }

            connect(subAction, SIGNAL(triggered()), &subtitleMapper, SLOT(map()));
            subtitleMapper.setMapping(subAction, i);
        }

        connect(&subtitleMapper, SIGNAL(mapped(int)), this, slot);
    }
    else
    {
        menu.addAction(trUtf8("프리셋이 없습니다"));
    }

    menu.exec(QCursor::pos());
}

void Equalizer::initPreset()
{
    this->parsePreset(&this->m_presets);
}

void Equalizer::parsePreset(QVector<Preset> *ret)
{
    QFile file(EQUALIZER_FILENAME);
    int maxBand = this->m_player->getBandCount();

    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&file);

        while (!stream.atEnd())
        {
            QString line = stream.readLine();
            Preset preset;
            EqualizerItem gain;

            line = line.trimmed();

            if (line.length() == 0)
                continue;

            if (line.startsWith(Utils::COMMENT_PREFIX))
                continue;

            QStringList pair = line.split(EQUALIZER_ITEM_DELIMITER);

            if (pair.length() < maxBand + 1)
                continue;

            preset.desc = trUtf8(pair[0].toUtf8());

            for (int i = 0; i < maxBand; i++)
            {
                gain.gain = pair[i+1].toFloat();
                preset.gains.append(gain);
            }

            ret->append(preset);
        }
    }
}

void Equalizer::selectPreset(int index)
{
    if (index < 0 || index >= this->m_presets.count())
        return;

    Preset &preset = this->m_presets[index];

    for (int i = 0; i < this->m_player->getBandCount(); i++)
    {
        QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(i));
        QLabel *label = (QLabel*)this->findChild<QLabel*>(QString("value%1").arg(i));
        float gain = preset.gains[i].gain;

        slider->setValue(this->dBToValue(gain));
        label->setText(QString().sprintf("%.1f", gain));
        this->m_player->setEqualizerGain(i, gain);
    }
}

void Equalizer::deletePreset(int index)
{
    if (index < 0 || index >= this->m_presets.count())
        return;

    if (Utils::questionMessageBox(this, trUtf8("삭제 하시겠습니까?")))
        this->m_presets.remove(index);
}

void Equalizer::savePreset(int index)
{
    if (index < 0 || index >= this->m_presets.count())
        return;

    Preset &preset = this->m_presets[index];

    for (int i = 0; i < this->m_player->getBandCount(); i++)
    {
        QSlider *slider = (QSlider*)this->findChild<QSlider*>(QString("band%1").arg(i));

        preset.gains[i].gain = this->valueTodB(slider->value());
    }
}

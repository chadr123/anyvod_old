﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "OpenDevice.h"
#include "ui_opendevice.h"
#include "core/Utils.h"
#include "device/CaptureInfo.h"

const QString OpenDevice::INVALID_VALUE = "invalid";

OpenDevice::OpenDevice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenDevice)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

#ifdef Q_OS_LINUX
    this->ui->audioList->setEnabled(false);
#endif

    QListWidgetItem *videoDefault = new QListWidgetItem;
    QListWidgetItem *audioDefault = new QListWidgetItem;

    videoDefault->setText(trUtf8("선택 안 함"));
    videoDefault->setData(Qt::UserRole, INVALID_VALUE);

    audioDefault->setText(trUtf8("선택 안 함"));
    audioDefault->setData(Qt::UserRole, INVALID_VALUE);

    this->ui->videoList->addItem(videoDefault);
    this->ui->audioList->addItem(audioDefault);

    videoDefault->setSelected(true);
    audioDefault->setSelected(true);

    QVector<CaptureInfo::Info> videoInfo;
    QVector<CaptureInfo::Info> audioInfo;
    CaptureInfo info;

    info.getVideoDevices(&videoInfo);
    info.getAudioDevices(&audioInfo);

    foreach (const CaptureInfo::Info &info, videoInfo)
    {
        QListWidgetItem *item = new QListWidgetItem;

        item->setText(info.desc);
        item->setData(Qt::UserRole, info.name);

        this->ui->videoList->addItem(item);
    }

    foreach (const CaptureInfo::Info &info, audioInfo)
    {
        QListWidgetItem *item = new QListWidgetItem;

        item->setText(info.desc);
        item->setData(Qt::UserRole, info.name);

        this->ui->audioList->addItem(item);
    }
}

OpenDevice::~OpenDevice()
{
    delete ui;
}

void OpenDevice::getDevicePath(QString *ret) const
{
    QString video = this->ui->videoList->selectedItems().at(0)->data(Qt::UserRole).toString();
    QString audio = this->ui->audioList->selectedItems().at(0)->data(Qt::UserRole).toString();
    QString path;

    if (video == INVALID_VALUE)
        video.clear();

    if (audio == INVALID_VALUE)
        audio.clear();

#ifdef Q_OS_MAC
    path = QString("%1:%2").arg(video).arg(audio);
#elif defined Q_OS_WIN
    path = QString("video=%1:audio=%2").arg(video).arg(audio);
#elif defined Q_OS_LINUX
    path = video;
#endif

    *ret = Utils::makeDevicePath(path);
}

void OpenDevice::getDesc(QString *ret) const
{
    QString videoDesc = this->ui->videoList->selectedItems().at(0)->text();
    QString audioDesc = this->ui->audioList->selectedItems().at(0)->text();
    QString videoValue = this->ui->videoList->selectedItems().at(0)->data(Qt::UserRole).toString();
    QString audioValue = this->ui->audioList->selectedItems().at(0)->data(Qt::UserRole).toString();
    QString first = videoDesc;
    QString second = audioDesc;

    if (videoValue == INVALID_VALUE)
        first.clear();

    if (audioValue == INVALID_VALUE)
        second.clear();

    if (first.isEmpty())
        *ret = second;
    else
        *ret = first + (second.isEmpty() ? "" : ", " + second);
}

void OpenDevice::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void OpenDevice::on_open_clicked()
{
    if (this->ui->videoList->selectedItems().isEmpty())
    {
        Utils::informationMessageBox(this, trUtf8("비디오 장치를 선택 해 주세요."));
    }
    else if (this->ui->audioList->selectedItems().isEmpty())
    {
        Utils::informationMessageBox(this, trUtf8("오디오 장치를 선택 해 주세요."));
    }
    else
    {
        QListWidgetItem *video = this->ui->videoList->selectedItems().at(0);
        QListWidgetItem *audio = this->ui->audioList->selectedItems().at(0);

        if (video->data(Qt::UserRole).toString() == INVALID_VALUE &&
                audio->data(Qt::UserRole).toString() == INVALID_VALUE)
        {
            Utils::informationMessageBox(this, trUtf8("비디오 장치 또는 오디오 장치를 선택 해야 합니다."));
        }
        else
        {
            this->accept();
        }
    }
}

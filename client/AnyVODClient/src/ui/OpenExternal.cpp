﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "OpenExternal.h"
#include "ui_openexternal.h"

OpenExternal::OpenExternal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenExternal)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

OpenExternal::~OpenExternal()
{
    delete ui;
}

void OpenExternal::getAddress(QString *ret) const
{
    *ret = this->m_address;
}

void OpenExternal::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void OpenExternal::on_open_clicked()
{
    this->m_address = this->ui->address->text().trimmed();
    this->accept();
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "Shader.h"
#include "ui_shader.h"
#include "MainWindow.h"
#include "video/ShaderCompositer.h"
#include "core/Utils.h"

#include <QScrollBar>
#include <QFileInfo>
#include <QFileDialog>
#include <QUrl>
#include <QMenu>
#include <QMimeData>

const QString Shader::SHADER_EXTS = "glsl txt";
const QStringList Shader::SHADER_EXTS_LIST = SHADER_EXTS.split(" ", QString::SkipEmptyParts);

Shader::Shader(ShaderCompositer *compositer, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Shader),
    m_compositer(compositer),
    m_delShort(QKeySequence(Qt::Key_Delete), this, SLOT(deleteSelection()), NULL),
    m_moveUpShort(QKeySequence(Qt::Key_Up | Qt::AltModifier), this, SLOT(moveUp()), NULL),
    m_moveDownShort(QKeySequence(Qt::Key_Down | Qt::AltModifier), this, SLOT(moveDown()), NULL),
    m_moveToTopShort(QKeySequence(Qt::Key_Home | Qt::AltModifier), this, SLOT(moveToTop()), NULL),
    m_moveToBottomShort(QKeySequence(Qt::Key_End | Qt::AltModifier), this, SLOT(moveToBottom()), NULL)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    QStringList shaders;

    this->m_compositer->getShaderList(&shaders);
    this->addShaderList(shaders);

    this->setAcceptDrops(true);
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

Shader::~Shader()
{
    delete ui;
}

void Shader::dragEnterEvent(QDragEnterEvent *event)
{
    const QMimeData *mime = event->mimeData();

    if (mime->hasUrls())
        event->accept();
}

void Shader::dropEvent(QDropEvent *event)
{
    const QMimeData *mime = event->mimeData();

    if (mime->hasUrls())
    {
        const QList<QUrl> urls = mime->urls();
        QStringList paths;

        for (int i = 0; i < urls.count(); i++)
            Utils::getLocalFileList(urls[i].toLocalFile(), SHADER_EXTS_LIST, false, &paths);

        this->addShaderList(paths);
    }
}

void Shader::contextMenuEvent(QContextMenuEvent *)
{
    QMenu menu;
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = Utils::sortByRow(listWidget->selectedItems(), *listWidget);
    int startRow;
    int endRow;

    if (list.empty())
    {
        startRow = -1;
        endRow = 1;
    }
    else
    {
        startRow = listWidget->row(list.first()) - 1;
        endRow = listWidget->row(list.last()) + 1;
    }

    QAction *action = NULL;

    action = menu.addAction(trUtf8("삭제"));
    action->setShortcut(this->m_delShort.key());
    action->setEnabled(list.count() > 0);
    connect(action, SIGNAL(triggered()), this, SLOT(deleteSelection()));

    menu.addSeparator();

    action = menu.addAction(trUtf8("맨 위로"));
    action->setShortcut(this->m_moveToTopShort.key());
    action->setEnabled(startRow >= 0);
    connect(action, SIGNAL(triggered()), this, SLOT(moveToTop()));

    action = menu.addAction(trUtf8("맨 아래로"));
    action->setShortcut(this->m_moveToBottomShort.key());
    action->setEnabled(endRow < listWidget->count());
    connect(action, SIGNAL(triggered()), this, SLOT(moveToBottom()));

    menu.addSeparator();

    action = menu.addAction(trUtf8("위로"));
    action->setShortcut(this->m_moveUpShort.key());
    action->setEnabled(startRow >= 0);
    connect(action, SIGNAL(triggered()), this, SLOT(moveUp()));

    action = menu.addAction(trUtf8("아래로"));
    action->setShortcut(this->m_moveDownShort.key());
    action->setEnabled(endRow < listWidget->count());
    connect(action, SIGNAL(triggered()), this, SLOT(moveDown()));

    menu.exec(QCursor::pos());
}

void Shader::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void Shader::selectItemOption(int index, QItemSelectionModel::SelectionFlags option)
{
    this->ui->list->setCurrentRow(index, option);
}

void Shader::selectItem(int index)
{
    this->selectItemOption(index, QItemSelectionModel::ClearAndSelect);
}

void Shader::addShaderList(const QStringList &paths)
{
    for (int i = 0; i < paths.count(); i++)
    {
        QString path = paths[i];
        QFileInfo info(path);
        QListWidgetItem *item = new QListWidgetItem;

        item->setText(info.fileName());
        item->setData(Qt::UserRole, path);
        item->setToolTip(info.fileName());

        this->ui->list->addItem(item);
    }
}

void Shader::moveToTop()
{
    this->moveItems(this->ui->list->selectedItems(), 0, false);
}

void Shader::moveToBottom()
{
    this->moveItems(this->ui->list->selectedItems(), this->ui->list->count() - 1, true);
}

void Shader::moveUp()
{
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = Utils::sortByRow(listWidget->selectedItems(), *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();

    if (list.count() > 0)
    {
        for (int i = 0; i < list.count(); i++)
        {
            int select = listWidget->row(list[i]);

            if (select <= 0)
                return;

            QListWidgetItem *item = listWidget->takeItem(select);

            listWidget->insertItem(select - 1, item);
            item->setSelected(true);
        }
    }

    listWidget->verticalScrollBar()->setValue(scroll);
    this->selectItemOption(listWidget->row(list.first()), QItemSelectionModel::Current);
}

void Shader::moveDown()
{
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = Utils::sortByRow(listWidget->selectedItems(), *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();

    if (list.count() > 0)
    {
        for (int i = list.count() - 1; i >= 0; i--)
        {
            int select = listWidget->row(list[i]);

            if (select >= listWidget->count() - 1)
                return;

            QListWidgetItem *item = listWidget->takeItem(select);

            listWidget->insertItem(select + 1, item);
            item->setSelected(true);
        }
    }

    listWidget->verticalScrollBar()->setValue(scroll);
    this->selectItemOption(listWidget->row(list.last()), QItemSelectionModel::Current);
}

void Shader::moveItems(const QList<QListWidgetItem*> &list, int rowTo, bool dirBottom)
{
    QListWidget *listWidget = this->ui->list;
    QList<QListWidgetItem*> sorted = Utils::sortByRow(list, *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();
    int curIndex = -1;

    if (sorted.count() > 0)
    {
        if (dirBottom)
            curIndex = listWidget->row(sorted.first());
        else
            curIndex = listWidget->row(sorted.last());
    }

    for (int i = 0; i < sorted.count(); i++)
    {
        if (rowTo >= 0 && rowTo <= listWidget->count())
        {
            QListWidgetItem *item = sorted[i];
            int row = listWidget->row(item);

            item = listWidget->takeItem(row);
            listWidget->insertItem(rowTo, item);
        }

        if (!dirBottom)
            rowTo++;
    }

    listWidget->verticalScrollBar()->setValue(scroll);

    if (curIndex >= listWidget->count())
        curIndex = listWidget->count() - 1;

    if (curIndex >= 0)
        this->selectItem(curIndex);
}

void Shader::deleteSelection()
{
    QListWidget *list = this->ui->list;
    const QList<QListWidgetItem*> &selectedItems = Utils::sortByRow(list->selectedItems(), *list);
    int scroll = list->verticalScrollBar()->value();
    int curIndex = -1;

    if (selectedItems.count() > 0)
        curIndex = list->row(selectedItems.first());

    for (int i = 0; i < selectedItems.count(); i++)
    {
        QListWidgetItem *item = selectedItems[i];

        delete item;
    }

    list->verticalScrollBar()->setValue(scroll);

    if (curIndex >= list->count())
        curIndex = list->count() - 1;

    if (curIndex >= 0)
        this->selectItem(curIndex);
}

void Shader::on_add_clicked()
{
    MainWindow *parent = Utils::getMainWindow();
    QString startPath = QDir(parent->getEnv()->shader).absolutePath();
    QString filter = trUtf8("GLSL (*.glsl);;텍스트 (*.txt);;모든 파일 (*.*)");
    QStringList files = QFileDialog::getOpenFileNames(this, QString(), startPath, filter, NULL, QFileDialog::HideNameFilterDetails);

    this->addShaderList(files);
}

void Shader::on_apply_clicked()
{
    Ui::Shader *ctrl = this->ui;

    this->m_compositer->clearShaders();

    for (int i = 0; i < ctrl->list->count(); i++)
    {
        QString path = ctrl->list->item(i)->data(Qt::UserRole).toString();

        this->m_compositer->addShader(path);
    }

    QString log;

    if (this->m_compositer->build())
    {
        log = trUtf8("성공");
    }
    else
    {
        this->m_compositer->clearShaders();
        this->m_compositer->getLog(&log);

        log + "\n";
        log += trUtf8("실패");
    }

    QString src;

    this->m_compositer->getFragmentSource(&src);

    ctrl->log->setPlainText(src + log);
    ctrl->log->verticalScrollBar()->setValue(ctrl->log->verticalScrollBar()->maximum());
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "TextEncodeSetting.h"
#include "ui_textencodesetting.h"
#include "core/Utils.h"

#include <QTextCodec>
#include <QList>

const QString TextEncodeSetting::DEFAULT_NAME = "Default";

TextEncodeSetting::TextEncodeSetting(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextEncodeSetting)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    QList<int> mibList = QTextCodec::availableMibs();
    QStringList codecList;
    QListWidget *list = this->ui->encoding;

    foreach (const int mib, mibList)
        codecList.append(QTextCodec::codecForMib(mib)->name());

    qStableSort(codecList);

    codecList.removeDuplicates();

    list->addItem(DEFAULT_NAME);

    foreach (const QString &codec, codecList)
        list->addItem(codec);

    QList<QListWidgetItem*> currentItem = list->findItems(Utils::getSubtitleCodecName(), Qt::MatchExactly);

    if (currentItem.size() > 0)
        list->setCurrentItem(currentItem.first());
    else
        list->setCurrentRow(0);
}

TextEncodeSetting::~TextEncodeSetting()
{
    delete ui;
}

void TextEncodeSetting::getEncoding(QString *ret) const
{
    QListWidgetItem *item = this->ui->encoding->currentItem();

    *ret = item->text();
}

void TextEncodeSetting::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

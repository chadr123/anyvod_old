﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <QDialog>
#include <QSizeF>

namespace Ui
{
    class UserAspectRatio;
}

class UserAspectRatio : public QDialog
{
    Q_OBJECT
public:
    explicit UserAspectRatio(double width, double height, QWidget *parent = 0);
    ~UserAspectRatio();

    void getSize(QSizeF *ret) const;

private:
    virtual void changeEvent(QEvent *event);

private slots:
    void on_ok_clicked();

private:
    Ui::UserAspectRatio *ui;
    QSizeF m_size;
};

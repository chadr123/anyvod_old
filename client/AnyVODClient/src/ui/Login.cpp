﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "Login.h"
#include "ui_login.h"
#include "core/Utils.h"
#include "net/Socket.h"
#include "../../../../common/size.h"

Login::Login(const QString &address, uint16_t port, uint16_t streamPort, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login),
    m_address(address),
    m_port(port),
    m_streamPort(streamPort)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    Ui::Login *ctrl = this->ui;

    ctrl->id->setMaxLength(MAX_ID_CHAR_SIZE);
    ctrl->password->setMaxLength(MAX_PASS_CHAR_SIZE);

    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

Login::~Login()
{
    delete ui;
}

void Login::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void Login::on_login_clicked()
{
    Ui::Login *ctrl = this->ui;

    if (ctrl->id->text().isEmpty())
    {
        Utils::informationMessageBox(this, trUtf8("아이디를 입력해 주세요"));
    }
    else if (ctrl->password->text().isEmpty())
    {
        Utils::informationMessageBox(this, trUtf8("비밀번호를 입력해 주세요"));
    }
    else
    {
        Socket &socket = Socket::getInstance();
        QString error;

        if (!socket.connect(this->m_address, this->m_port))
        {
            Utils::criticalMessageBox(this, trUtf8("서버에 접속 할 수 없습니다"));
            return;
        }

        if (!socket.login(ctrl->id->text(), ctrl->password->text(), &error))
        {
            Utils::criticalMessageBox(this, error);
            socket.disconnect();

            return;
        }

        Socket &streamSocket = Socket::getStreamInstance();

        if (!streamSocket.connect(this->m_address, this->m_streamPort))
        {
            Utils::criticalMessageBox(this, trUtf8("스트림에 접속 할 수 없습니다"));
            socket.disconnect();

            return;
        }

        QString ticket;

        socket.getTicket(&ticket);

        if (!streamSocket.join(ticket, &error))
        {
            Utils::criticalMessageBox(this, error);
            socket.disconnect();
            streamSocket.disconnect();

            return;
        }

        this->accept();
    }
}

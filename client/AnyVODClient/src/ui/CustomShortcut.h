﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "core/Common.h"

#include <QDialog>
#include <QKeySequence>

namespace Ui
{
    class CustomShortcut;
}

class QFormLayout;

class CustomShortcut : public QDialog
{
    Q_OBJECT
public:
    explicit CustomShortcut(QWidget *parent = 0);
    ~CustomShortcut();

private slots:
    void on_ok_clicked();
    void on_reset_clicked();

private:
    struct Item
    {
        Item() {}

        Item(const QString &d, const QKeySequence &s)
        {
            desc = d;
            seq = s;
        }

        QString desc;
        QKeySequence seq;
    };

private:
    virtual void customEvent(QEvent *event);
    virtual void changeEvent(QEvent *event);

private:
    void initKey();

    void addShortcutItem(QFormLayout *parent, AnyVODEnums::ShortcutKey key);
    void clearLayout(QFormLayout *layout);

    void createMainTab();
    void createDTVTab();
    void createScreenTab();
    void createPlayTab();
    void createSubtitleTab();
    void createAudioTab();

private:
    Ui::CustomShortcut *ui;
    Item m_table[AnyVODEnums::SK_COUNT];

private:
    static const QString PREFIX;
};

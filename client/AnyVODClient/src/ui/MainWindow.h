﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "RemoteFileList.h"
#include "PlayList.h"
#include "Popup.h"
#include "../../../../common/network.h"

#ifdef Q_OS_MAC
#include <IOKit/pwr_mgt/IOPMLib.h>
#endif

#ifdef Q_OS_WIN
#include <QWinTaskbarButton>
#include <QWinThumbnailToolBar>
#include <QWinThumbnailToolButton>
#endif

#ifdef Q_OS_LINUX
#include <dbus/dbus.h>
#endif

#include <QMainWindow>
#include <QSettings>
#include <QSharedMemory>
#include <QTranslator>

class QShortcut;
class MediaPlayer;
class ShaderCompositer;
class Screen;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    struct ENV
    {
        ENV()
        {
            fontPath = QString::fromUtf8("./fonts/NanumGothicBold.ttf");
            skin = QString::fromUtf8("./skins/default.skin");
            shader = QString::fromUtf8("./shaders");
            fontSize = 20;
            subtitleOutlineSize = 2;
        }

        QString fontPath;
        QString fontFamily;
        QString skin;
        QString shader;
        uint8_t fontSize;
        uint8_t subtitleOutlineSize;
    };

    struct UserAspectRatioItem
    {
        UserAspectRatioItem()
        {
            width = 1.0;
            height = 1.0;
        }

        UserAspectRatioItem(double _width, double _height)
        {
            width = _width;
            height = _height;
        }

        bool isInvalid() const
        {
            return width == 0.0 || height == 0.0;
        }

        bool isFullScreen() const
        {
            return width == -1.0 || height == -1.0;
        }

        double width;
        double height;
    };

    struct UserAspectRatioList
    {
        UserAspectRatioList()
        {
            curIndex = 0;

            list.append(UserAspectRatioItem(0.0, 0.0));
            list.append(UserAspectRatioItem(-1.0, -1.0));
            list.append(UserAspectRatioItem(4.0, 3.0));
            list.append(UserAspectRatioItem(16.0, 9.0));
            list.append(UserAspectRatioItem(16.0, 10.0));
            list.append(UserAspectRatioItem(1.85, 1.0));
            list.append(UserAspectRatioItem(2.35, 1.0));
            list.append(UserAspectRatioItem(4.0, 3.0));
        }

        int curIndex;
        QVector<UserAspectRatioItem> list;
    };

    struct UserSPDIFSampleRateList
    {
        UserSPDIFSampleRateList()
        {
            curIndex = 0;

            list.append(0);
            list.append(44100);
            list.append(48000);
            list.append(88200);
            list.append(96000);
            list.append(176400);
            list.append(192000);
        }

        int curIndex;
        QVector<int> list;
    };

public:
    explicit MainWindow(const QStringList starting, QWidget *parent = 0);
    ~MainWindow();

    virtual void moveEvent(QMoveEvent *);
    virtual void customEvent(QEvent *event);
    virtual void contextMenuEvent(QContextMenuEvent *);
    virtual void mouseReleaseEvent(QMouseEvent *);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *);
    virtual void dragEnterEvent(QDragEnterEvent *event);
    virtual void dropEvent(QDropEvent *event);
    virtual void timerEvent(QTimerEvent *event);
    virtual void resizeEvent(QResizeEvent *);
    virtual void closeEvent(QCloseEvent *);
    virtual void changeEvent(QEvent *event);

    void resetWindowTitle();

    bool addToPlayList(const QVector<PlayItem> &list, bool enableAuto, bool forceStart);
    void setPlayList(const QVector<PlayItem> &list);

    RemoteFileList& getRemoteFileListWindow();
    PlayList& getPlayListWindow();
    const ENV* getEnv() const;

    QString getShortcutKeyDesc(AnyVODEnums::ShortcutKey key) const;
    QKeySequence getShortcutKey(AnyVODEnums::ShortcutKey key) const;
    void setShortcutKey(AnyVODEnums::ShortcutKey key, QKeySequence seq);

    bool open(const QString &filePath, ExtraPlayData &data, const QUuid &unique);
    bool playAt(int index);

    void updateWindowTitle();
    void updateNavigationButtons();

    int getCurrentPlayingIndex() const;
    void setCurrentPlayingIndex(int index);
    void resetCurrentPlayingIndex();
    bool setCurrentPlayingIndexByUnique();
    void adjustPlayIndex();

    Screen* getScreen();
    MediaPlayer* getPlayer();
    QString getVersion() const;

    void setFileNameIndicator(const QString &fileName);

    void setPlayingMethod(AnyVODEnums::PlayingMethod method);
    AnyVODEnums::PlayingMethod getPlayingMethod() const;

    bool isPrevEnabled() const;
    bool isNextEnabled() const;
    bool isMostTop() const;
    bool isMute() const;
    bool isReady() const;
    bool isFullScreening() const;
    bool isLogined() const;
    bool isClearPlayList() const;

    void setSeekingText();

    QWidget& getControlBar();
    ShaderCompositer& getShader();
    QString getLanguage() const;

    void movePopup();

    void setupKey();
    void deleteKey();
    void reloadKey();

    void getUserAspectRatioList(UserAspectRatioList *ret) const;
    void getUserSPDIFSampleRateList(UserSPDIFSampleRateList *ret) const;

    bool canShowControlBar() const;

    QString get3DMethodDesc(AnyVODEnums::Video3DMethod method) const;
    QString getSubtitle3DMethodDesc(AnyVODEnums::Subtitle3DMethod method) const;
    QString getAnaglyphAlgoritmDesc(AnyVODEnums::AnaglyphAlgorithm algorithm) const;
    QString getDeinterlaceDesc(AnyVODEnums::DeinterlaceAlgorithm algorithm) const;

public:
    static const QString APP_NAME;
    static const QString VERSION_TEMPLATE;
    static const QString MOVIE_EXTS;
    static const QString AUDIO_EXTS;
    static const QString SUBTITLE_EXTS;
    static const QString PLAYLIST_EXTS;
    static const QString FONT_EXTS;
    static const QStringList MOVIE_EXTS_LIST;
    static const QStringList AUDIO_EXTS_LIST;
    static const QStringList SUBTITLE_EXTS_LIST;
    static const QStringList PLAYLIST_EXTS_LIST;
    static const QStringList MEDIA_EXTS_LIST;
    static const QStringList ALL_EXTS_LIST;
    static const QString CAPTURE_FORMAT;
    static const QStringList CAPTURE_FORMAT_LIST;
    static const QStringList FONT_EXTS_LIST;
    static const QSize DEFAULT_SCREEN_SIZE;
    static const QString ORG_NAME;
    static const QString SHARE_MEM_KEY;
    static const int SHARE_MEM_SIZE;
    static const QString LANG_PREFIX;
    static const QString LANG_DIR;

public:
    static QString OBJECT_NAME;

private:
    static const int CONTROL_BAR_HEIGHT;
    static const int SCREEN_MINIMUM_SIZE;
    static const QPoint DEFAULT_POSITION;
    static const int LOGIN_TIME;
    static const int SHARE_MEM_TIME;
    static const int INIT_COMPLETE_TIME;
#ifdef Q_OS_LINUX
    static const char DBUS_SERVICE[][40];
    static const char DBUS_PATH[][33];
#endif

private:
    void updateTrackBar();
    bool loadEnv();

    void restart();
    bool playNext();
    bool playPrev();
    bool playCurrent();

    bool isAutoStart() const;

    void loadSettings();
    void saveSettings();

    void loadPlayList();
    void savePlayList();

    void loadEqualizer();
    void saveEqualizer();

    void loadSkipRange();
    void saveSkipRange();

    void loadShortcutKey();
    void saveShortcutKey();

    void loadDTVScannedChannels();
    void saveDTVScannedChannels();

    void processMouseUp();
    void processMouseDown();
    void processMouseMoving();

    void moveChildWindows();

    void updateAudioSpec();
    void updatePlayUI();

    void startSpectrum();
    void stopSpectrum();
    void pauseSpectrum();
    void resumeSpectrum();

    void checkGOMSubtitle();

    void volume(int maxVolume, int dir);
    void opaque(int dir);
    void changeOpaque(int value);

    void rewind(double distance);
    void forward(double distance);

    void startMedia(const QString &path);

    void setFullScreenMode(bool enable);
    void setScreenSizeRatio(double ratio);
    void setLanguage(const QString &lang);
    void setScreenSaverActivity(bool enable);

    void adjustSPDIFAudioUI();

    void setup3DMethod();
    void setupSubtitle3DMethod();
    void setupAnaglyphAlgorithm();
    void setupDeinterlaceMethod();

    void resetAudioSubtitle();

#ifdef Q_OS_WIN
    void initThumbnailBar();
    void setThumbnailButtonType(bool resume);
    void translateThumbnailText();
#endif

#ifdef Q_OS_LINUX
    bool debusInit();
    void debusDeinit();
#endif

    void getYouTubePlayItems(const QString &address, QVector<PlayItem> *itemVector);

private:
    static void audioOptionDescCallback(void *userdata, QKeyEvent *event);

private:
    virtual bool nativeEvent(const QByteArray &, void *message, long *result);

private slots:
    void on_pause_clicked();
    void on_stop_clicked();
    void on_resume_clicked();
    void on_volume_valueChanged(int value);
    void on_trackBar_sliderMoved(int position);
    void on_trackBar_sliderReleased();
    void on_mute_toggled(bool checked);
    void on_opaque_valueChanged(int value);

private slots:
    void login();
    void logout();
    void mostTop();
    void openRemoteFileList();
    void openExternal();
    void open();
    void close();
    void exit();
    void selectSubtitleClass(const QString &className);
    void selectAudioStream(int index);
    void audioNormalize();
    void audioEqualizer();
    void equalizerSetting();
    void openPlayList();
    void resetSubtitlePosition();
    void upSubtitlePosition();
    void downSubtitlePosition();
    void leftSubtitlePosition();
    void rightSubtitlePosition();
    void selectPlayingMethod(int method);
    void toggle();
    void stop();
    void fullScreen();
    void rewind5();
    void forward5();
    void rewind30();
    void forward30();
    void rewind60();
    void forward60();
    void gotoBegin();
    void prev();
    void next();
    void playOrder();
    void subtitleToggle();
    void prevSubtitleSync();
    void nextSubtitleSync();
    void resetSubtitleSync();
    void subtitleLanguageOrder();
    void volumeUp();
    void volumeDown();
    void mute();
    void audioOrder();
    void info();
    void detail();
    void showControlBar();
    void incOpaque();
    void decOpaque();
    void maxOpaque();
    void minOpaque();
    void deinterlaceMethodOrder();
    void deinterlaceAlgorithmOrder();
    void selectDeinterlacerMethod(int method);
    void selectDeinterlacerAlgorithem(int algorithm);
    void prevAudioSync();
    void nextAudioSync();
    void resetAudioSync();
    void subtitleHAlignOrder();
    void selectSubtitleHAlignMethod(int method);
    void repeatRangeStart();
    void repeatRangeEnd();
    void repeatRangeEnable();
    void repeatRangeStartBack100MS();
    void repeatRangeStartForw100MS();
    void repeatRangeEndBack100MS();
    void repeatRangeEndForw100MS();
    void repeatRangeBack100MS();
    void repeatRangeForw100MS();
    void seekKeyFrame();
    void skipOpening();
    void skipEnding();
    void useSkipRange();
    void openSkipRange();
    void captureExtOrder();
    void captureSingle();
    void captureMultiple();
    void selectCaptureExt(const QString &ext);
    void prevFrame();
    void nextFrame();
    void slowerPlayback();
    void fasterPlayback();
    void normalPlayback();
    void resetVideoAttribute();
    void brightnessDown();
    void brightnessUp();
    void saturationDown();
    void saturationUp();
    void hueDown();
    void hueUp();
    void contrastDown();
    void contrastUp();
    void brightness(double step);
    void saturation(double step);
    void hue(double step);
    void contrast(double step);
    void openShaderCompositer();
    void lowerMusic();
    void lowerVoice();
    void higherVoice();
    void sharply();
    void sharpen();
    void soften();
    void leftRightInvert();
    void topBottomInvert();
    void addToPlayList();
    void incSubtitleOpaque();
    void decSubtitleOpaque();
    void resetSubtitleOpaque();
    void setEnglishIME() const;
    void selectAudioDevice(int device);
    void audioDeviceOrder();
    void customShortcut();
    void selectCaptureSaveDir();
    void openCaptureSaveDir();
    void fileAssociation();
    void enableSearchSubtitle();
    void enableSearchLyrics();
    void subtitleVAlignOrder();
    void selectSubtitleVAlignMethod(int method);
    void incSubtitleSize();
    void decSubtitleSize();
    void resetSubtitleSize();
    void userAspectRatioOrder();
    void selectUserAspectRatio(int index);
    void userAspectRatio();
    void screenSizeHalf();
    void screenSizeNormal();
    void screenSizeNormalHalf();
    void screenSizeDouble();
    void useHWDecoder();
    void useSPDIF();
    void userSPDIFSampleRateOrder();
    void selectUserSPDIFSampleRate(int sampleRate);
    void usePBO();
    void method3DOrder();
    void select3DMethod(int method);
    void useVSync();
    void selectLanguage(const QString &lang);
    void prevChapter();
    void nextChapter();
    bool moveChapter(int index, QString *desc);
    int getChapterIndex(double pos);
    void closeExternalSubtitle();
    void clearPlayList();
    void showAlbumJacket();
    void lastPlay();
    void textEncoding();
    void serverSetting();
    void saveSubtitle();
    void saveAsSubtitle();
    void selectSPDIFAudioDevice(int device);
    void audioSPDIFDeviceOrder();
    void openScreenExplorer();
    void useFrameDrop();
    void useSPDIFEncodingOrder();
    void selectSPDIFEncodingMethod(int method);
    void anaglyphAlgorithmOrder();
    void selectAnaglyphAlgorithm(int algorithm);
    void method3DSubtitleOrder();
    void select3DSubtitleMethod(int method);
    void use3DFull();
    void reset3DSubtitleOffset();
    void up3DSubtitleOffset();
    void down3DSubtitleOffset();
    void left3DSubtitleOffset();
    void right3DSubtitleOffset();
    void openSubtitleDirectory();
    void openImportFonts();
    void screenRotationDegreeOrder();
    void selectScreenRotationDegree(int degree);
    void openDevice();
    void addDTVChannelToPlaylist();
    void openScanDTVChannel();
    void histEQ();
    void hq3DDenoise();
    void ataDenoise();
    void owDenoise();
    void deband();
    void viewEPG();
    void useSearchSubtitleComplex();
    void useBufferingMode();

private:
    struct ShortcutItem
    {
        ShortcutItem()
        {
            shortcut = NULL;
        }

        ShortcutItem(const QString &d, QShortcut *s)
        {
            desc = d;
            shortcut = s;
        }

        QString desc;
        QShortcut *shortcut;
    };

#ifdef Q_OS_LINUX
    enum ScreenSaverAPI
    {
        SSAPI_FDO_SS,
        SSAPI_FDO_PM,
        SSAPI_MATE,
        SSAPI_GNOME,
        SSAPI_COUNT
    };
#endif

private:
    Ui::MainWindow *ui;
    MediaPlayer *m_player;
    bool m_buttonPressed;
    QPoint m_prevPos;
    bool m_prevMaximized;
    int m_currentPlayingIndex;
    ENV m_env;
    QSettings m_settings;
    bool m_lastScreenVisible;
    QSize m_lastScreenSize;
    RemoteFileList m_remoteFileList;
    bool m_prevRemoteFileListVisible;
    PlayList m_playList;
    bool m_prevPlayListVisible;
    AnyVODEnums::PlayingMethod m_playingMethod;
    int m_spectrumTimerID;
    int m_nonePlayingDescTimerID;
    int m_loginTimerID;
    int m_shareMemTimerID;
    int m_initCompleteTimerID;
    bool m_isReady;
    bool m_isFullScreening;
    bool m_clearPlayList;
    Popup m_popup;
    QSharedMemory m_shareMem;
    QStringList m_startingPaths;
    QString m_remoteAddress;
    uint16_t m_remoteCommandPort;
    uint16_t m_remoteStreamPort;
    QString m_lastSelectedDir;
    QString m_lang;
    QTranslator m_langQt;
    QTranslator m_langQtBase;
    QTranslator m_langAnyVOD;
    ShortcutItem m_shortcut[AnyVODEnums::SK_COUNT];
    QString m_3DMethodDesc[AnyVODEnums::V3M_COUNT];
    QString m_subtitle3DMethodDesc[AnyVODEnums::S3M_COUNT];
    QString m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_COUNT];
    QString m_deinterlaceDesc[AnyVODEnums::DA_COUNT];
    UserAspectRatioList m_userAspectRatioList;
    UserSPDIFSampleRateList m_userSPDIFSampleRateList;

#ifdef Q_OS_MAC
    IOPMAssertionID m_systemSleepAssertionID;
#endif

#ifdef Q_OS_WIN
    QWinTaskbarButton m_taskbarButton;
    QWinThumbnailToolBar m_thumbnailToolBar;
    QWinThumbnailToolButton m_thumbnailToolResumePause;
    QWinThumbnailToolButton m_thumbnailToolStop;
    QWinThumbnailToolButton m_thumbnailToolBackward;
    QWinThumbnailToolButton m_thumbnailToolForward;
#endif

#ifdef Q_OS_LINUX
    DBusConnection *m_dbusConn;
    DBusPendingCall *m_dbusPending;
    dbus_uint32_t m_dbusCookie;
    ScreenSaverAPI m_screenSaverAPI;
#endif
};

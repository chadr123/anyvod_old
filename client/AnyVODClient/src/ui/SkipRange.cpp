﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "SkipRange.h"
#include "ui_skiprange.h"
#include "media/MediaPlayer.h"
#include "core/Utils.h"

#include <QTime>

SkipRange::SkipRange(MediaPlayer *player, QVector<MediaPresenter::Range> *ret, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SkipRange),
    m_player(player),
    m_ranges(ret),
    m_delShort(QKeySequence(Qt::Key_Delete), this, SLOT(on_del_clicked()), NULL)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->initHeader();

    this->ui->opening->setCurrentSection(QTimeEdit::SecondSection);
    this->ui->ending->setCurrentSection(QTimeEdit::SecondSection);
    this->ui->startTime->setCurrentSection(QTimeEdit::SecondSection);

    QVector<MediaPresenter::Range> ranges;
    int opening = -1;
    int ending = -1;

    this->m_player->getSkipRanges(&ranges);

    for (int i = 0; i < ranges.count(); i++)
    {
        if (ranges[i].start < 0.0)
        {
            opening = i;
            continue;
        }

        if (ranges[i].end < 0.0)
        {
            ending = i;
            continue;
        }

        this->pushBack(ranges[i]);
    }

    if (opening != -1)
        this->ui->opening->setTime(Utils::getTime(ranges[opening].end).time());

    if (ending != -1)
        this->ui->ending->setTime(Utils::getTime(ranges[ending].start).time());

    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

SkipRange::~SkipRange()
{
    delete ui;
}

void SkipRange::initHeader()
{
    QTreeWidget *list = this->ui->skipRanges;
    QStringList headers;

    headers.append(trUtf8("시작"));
    headers.append(trUtf8("길이"));
    headers.append(trUtf8("범위"));
    headers.append(trUtf8("사용"));

    list->setColumnCount(headers.count());
    list->setHeaderLabels(headers);

    list->setColumnWidth(0, 70);
    list->setColumnWidth(1, 80);
    list->setColumnWidth(2, 140);
    list->setColumnWidth(3, 45);

    QFont headerFont(list->header()->font());

    headerFont.setPointSize(9);

    list->header()->setFont(headerFont);
    list->header()->setSectionsMovable(false);
    list->header()->setDefaultAlignment(Qt::AlignCenter);
}

void SkipRange::pushBack(const MediaPresenter::Range &range)
{
    QStringList row;
    QString startTime;
    QString endTime;

    Utils::getTimeString(range.start, Utils::TIME_HH_MM_SS, &startTime);
    Utils::getTimeString(range.end, Utils::TIME_HH_MM_SS, &endTime);

    row.append(QString("%1").arg(startTime));
    row.append(trUtf8("%1초").arg(int(range.end - range.start)));
    row.append(QString("%1 ~ %2").arg(startTime).arg(endTime));
    row.append(QString());

    QTreeWidgetItem *item = new QTreeWidgetItem((QTreeWidgetItem*)NULL, row);

    item->setTextAlignment(0, Qt::AlignCenter);
    item->setTextAlignment(1, Qt::AlignRight);
    item->setTextAlignment(2, Qt::AlignCenter);

    item->setData(0, Qt::UserRole, int(range.start));
    item->setData(1, Qt::UserRole, int(range.end));
    item->setCheckState(3, range.enable ? Qt::Checked : Qt::Unchecked);

    this->ui->skipRanges->addTopLevelItem(item);
}

void SkipRange::changeRange(const QTime &time, int length)
{
    QString startTime;
    QString endTime;
    QTime range = time;

    Utils::getTimeString(-range.secsTo(Utils::ZERO_TIME), Utils::TIME_HH_MM_SS, &startTime);
    range = range.addSecs(length);
    Utils::getTimeString(-range.secsTo(Utils::ZERO_TIME), Utils::TIME_HH_MM_SS, &endTime);

    this->ui->range->setText(QString("%1 ~ %2").arg(startTime).arg(endTime));
}

bool SkipRange::existRange(const MediaPresenter::Range &range) const
{
    QTreeWidget *list = this->ui->skipRanges;

    for (int i = 0; i < list->topLevelItemCount(); i++)
    {
        QTreeWidgetItem *item = list->topLevelItem(i);
        MediaPresenter::Range value;

        value.start = item->data(0, Qt::UserRole).toInt();
        value.end = item->data(1, Qt::UserRole).toInt();

        if (value.start == range.start && value.end == range.end)
            return true;
    }

    return false;
}

void SkipRange::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            this->initHeader();

            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void SkipRange::on_ok_clicked()
{
    MediaPresenter::Range range;
    Ui::SkipRange *ctrl = this->ui;

    range.start = -1.0;
    range.end = -ctrl->opening->time().secsTo(Utils::ZERO_TIME);

    this->m_ranges->push_back(range);

    range.start = -ctrl->ending->time().secsTo(Utils::ZERO_TIME);
    range.end = -1.0;

    this->m_ranges->push_back(range);

    for (int i = 0; i < ctrl->skipRanges->topLevelItemCount(); i++)
    {
        QTreeWidgetItem *item = ctrl->skipRanges->topLevelItem(i);

        range.start = item->data(0, Qt::UserRole).toInt();
        range.end = item->data(1, Qt::UserRole).toInt();
        range.enable = item->checkState(3) == Qt::Checked ? true : false;

        this->m_ranges->append(range);
    }

    this->accept();
}

void SkipRange::on_add_clicked()
{
    MediaPresenter::Range range;
    Ui::SkipRange *ctrl = this->ui;
    QTime time = ctrl->startTime->time();

    range.start = -time.secsTo(Utils::ZERO_TIME);
    time = time.addSecs(ctrl->length->value());
    range.end = -time.secsTo(Utils::ZERO_TIME);
    range.enable = true;

    if (!this->existRange(range))
        this->pushBack(range);
    else
        Utils::informationMessageBox(this, trUtf8("이미 해당 범위가 존재합니다"));
}

void SkipRange::on_del_clicked()
{
    QTreeWidget *list = this->ui->skipRanges;

    for (int i = 0; i < list->topLevelItemCount(); i++)
    {
        QTreeWidgetItem *item = list->topLevelItem(i);

        if (item->isSelected())
        {
            list->takeTopLevelItem(i);
            delete item;
            --i;
        }
    }
}

void SkipRange::on_startTime_timeChanged(const QTime &date)
{
   this->changeRange(date, this->ui->length->value());
}

void SkipRange::on_length_valueChanged(int value)
{
   this->changeRange(this->ui->startTime->time(), value);
}

void SkipRange::on_skipRanges_itemClicked(QTreeWidgetItem *item, int)
{
    if (item->isSelected())
    {
        Ui::SkipRange *ctrl = this->ui;
        MediaPresenter::Range range;
        QString time;

        range.start = item->data(0, Qt::UserRole).toInt();
        range.end = item->data(1, Qt::UserRole).toInt();

        Utils::getTimeString(range.start, Utils::TIME_HH_MM_SS, &time);

        ctrl->startTime->setTime(QTime::fromString(time, Utils::TIME_HH_MM_SS));
        ctrl->length->setValue(int(range.end - range.start));
    }
}

void SkipRange::on_skipRanges_itemActivated(QTreeWidgetItem *item, int)
{
    item->setCheckState(3, item->checkState(3) == Qt::Unchecked ? Qt::Checked : Qt::Unchecked);
}

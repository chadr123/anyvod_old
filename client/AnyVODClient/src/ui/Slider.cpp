﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "Slider.h"

#include <QMouseEvent>
#include <QPoint>
#include <QRect>
#include <QStyle>
#include <QStyleOptionSlider>
#include <QDebug>

Slider::Slider(QWidget *parent) :
    QSlider(parent),
    m_dragging(false)
{

}

bool Slider::getPosition(const QPoint &pos, int *ret) const
{
    QStyleOptionSlider opt;

    this->initStyleOption(&opt);

    QRect handleRect = this->style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);
    QPoint center = handleRect.center() - handleRect.topLeft();

    *ret = this->pixelPosToRangeValue(this->pick(pos - center));

    return this->style()->hitTestComplexControl(QStyle::CC_Slider, &opt, pos, this) == QStyle::SC_SliderHandle;
}

int Slider::getPixelPosition(const int pos) const
{
    QStyleOptionSlider opt;

    this->initStyleOption(&opt);

    QRect handleRect = this->style()->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);
    QPoint center = handleRect.center() - handleRect.topLeft();

    return this->rangeValueToPixelPos(pos) + center.x();
}

int Slider::pick(const QPoint &pt) const
{
    return this->orientation() == Qt::Horizontal ? pt.x() : pt.y();
}

int Slider::pixelPosToRangeValue(int pos) const
{
    int sliderMin;
    int sliderMax;
    bool upsideDown;

    this->getStyleOption(&sliderMin, &sliderMax, &upsideDown);

    return QStyle::sliderValueFromPosition(this->minimum(), this->maximum(), pos - sliderMin, sliderMax - sliderMin, upsideDown);
}

int Slider::rangeValueToPixelPos(int pos) const
{
    int sliderMin;
    int sliderMax;
    bool upsideDown;

    this->getStyleOption(&sliderMin, &sliderMax, &upsideDown);

    return QStyle::sliderPositionFromValue(this->minimum(), this->maximum(), pos - sliderMin, sliderMax - sliderMin, upsideDown);
}

void Slider::getStyleOption(int *sliderMin, int *sliderMax, bool *upsideDown) const
{
    QStyleOptionSlider opt;

    this->initStyleOption(&opt);

    QStyle *style = this->style();
    QRect gr = style->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderGroove, this);
    QRect hr = style->subControlRect(QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this);
    int handleLen;

    if (this->orientation() == Qt::Horizontal)
    {
        handleLen = hr.width();
        *sliderMin = gr.x();
        *sliderMax = gr.right();
    }
    else
    {
        handleLen = hr.height();
        *sliderMin = gr.y();
        *sliderMax = gr.bottom();
    }

    *sliderMax -= handleLen;
    *sliderMax += 1;

    *upsideDown = opt.upsideDown;
}

bool Slider::isDragging() const
{
    return this->m_dragging;
}

void Slider::setDrag(bool drag)
{
    this->m_dragging = drag;
}

void Slider::mousePressEvent(QMouseEvent *ev)
{
    if (ev->button() == Qt::LeftButton)
    {
        int pos;

        if (!this->getPosition(ev->pos(), &pos))
        {
            this->setSliderDown(true);
            this->setSliderPosition(pos);

            ev->accept();
        }
        else
        {
            this->m_dragging = true;
        }
    }

    QSlider::mousePressEvent(ev);
}

void Slider::mouseReleaseEvent(QMouseEvent *ev)
{
    QSlider::mouseReleaseEvent(ev);

    if (ev->button() == Qt::LeftButton)
    {
        int pos;

        if (!this->getPosition(ev->pos(), &pos))
        {
            ev->accept();
            this->setSliderDown(false);
        }

        this->m_dragging = false;
    }
}

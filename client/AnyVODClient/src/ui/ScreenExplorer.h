﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "ScreenExplorerUpdater.h"

#include <QDialog>
#include <QEvent>

namespace Ui
{
    class ScreenExplorer;
}

class MainWindow;
class MediaPlayer;
class QListWidgetItem;

class ScreenExplorer : public QDialog
{
    Q_OBJECT
public:
    static const QEvent::Type UPDATE_ITEM_EVENT;
    static const QEvent::Type UPDATE_PARENT_EVENT;

public:
    class UpdateItemEvent : public QEvent
    {
    public:
        UpdateItemEvent(double time, double rotation, const QImage &screen) :
            QEvent(UPDATE_ITEM_EVENT),
            m_time(time),
            m_rotation(rotation),
            m_screen(screen)
        {

        }

        double getTime() const { return this->m_time; }
        double getRotation() const { return this->m_rotation; }
        QImage getScreen() const { return this->m_screen; }

    private:
        double m_time;
        double m_rotation;
        QImage m_screen;
    };

    class UpdateParentEvent : public QEvent
    {
    public:
        UpdateParentEvent() :
            QEvent(UPDATE_PARENT_EVENT)
        {

        }
    };

public:
    explicit ScreenExplorer(QWidget *parent = 0);
    explicit ScreenExplorer(const QString &filePath, int currentIndex, QWidget *parent = 0);
    ~ScreenExplorer();

private slots:
    void on_screens_itemActivated(QListWidgetItem *item);
    void on_ScreenExplorer_accepted();
    void on_ScreenExplorer_rejected();
    void on_step_timeChanged(const QTime &time);

private:
    virtual void customEvent(QEvent *event);
    virtual void resizeEvent(QResizeEvent *event);
    virtual void changeEvent(QEvent *event);

private:
    void updateScreen();

private:
    Ui::ScreenExplorer *ui;
    MediaPlayer *m_player;
    ScreenExplorerUpdater m_updater;
    bool m_stopAddingItem;
    QString m_filePath;
    int m_currentPlayingIndex;
};

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "ui_playlist.h"
#include "PlayList.h"
#include "MainWindow.h"
#include "ScreenExplorer.h"
#include "Screen.h"
#include "core/Utils.h"
#include "media/MediaPlayer.h"
#include "parsers/playlist/PlayListParserInterface.h"
#include "parsers/playlist/PlayListParserGenerator.h"

#include <QFileInfo>
#include <QIcon>
#include <QMenu>
#include <QDropEvent>
#include <QScrollBar>
#include <QDir>
#include <QApplication>
#include <QClipboard>
#include <QTextStream>
#include <QMutexLocker>
#include <QSignalMapper>

const QEvent::Type PlayList::UPDATE_ITEM_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type PlayList::UPDATE_PARENT_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type PlayList::UPDATE_INC_COUNT = (QEvent::Type)QEvent::registerEventType();
const QSize PlayList::DEFAULT_PLAYLIST_SIZE = QSize(400, 290);
const int PlayList::UPDATE_PLAYLIST_TIME = 5000;
const int PlayList::MAX_RETRY_COUNT = 5;

QDataStream& operator << (QDataStream &out, const PlayItem &item)
{
    out << item.path;
    out << item.extraData.duration;
    out << item.extraData.totalFrame;
    out << item.extraData.valid;
    out << item.unique;
    out << item.title;
    out << item.totalTime;
    out << item.itemUpdated;
    out << item.extraData.userData;
    out << item.retry;

    return out;
}

QDataStream& operator >> (QDataStream &in, PlayItem &item)
{
    in >> item.path;
    in >> item.extraData.duration;
    in >> item.extraData.totalFrame;
    in >> item.extraData.valid;
    in >> item.unique;
    in >> item.title;
    in >> item.totalTime;
    in >> item.itemUpdated;
    in >> item.extraData.userData;
    in >> item.retry;

    if (item.unique.isNull())
        item.unique = QUuid::createUuid();

    return in;
}

PlayList::PlayList(QWidget *parent) :
    QDialog(parent, Qt::Tool),
    ui(new Ui::PlayList),
    m_isInit(false),
    m_updater(this),
    m_updateTimerID(0),
    m_delShort(QKeySequence(Qt::Key_Delete), this, SLOT(deleteSelection()), NULL),
    m_moveUpShort(QKeySequence(Qt::Key_Up | Qt::AltModifier), this, SLOT(moveUp()), NULL),
    m_moveDownShort(QKeySequence(Qt::Key_Down | Qt::AltModifier), this, SLOT(moveDown()), NULL),
    m_moveToTopShort(QKeySequence(Qt::Key_Home | Qt::AltModifier), this, SLOT(moveToTop()), NULL),
    m_moveToBottomShort(QKeySequence(Qt::Key_End | Qt::AltModifier), this, SLOT(moveToBottom()), NULL),
    m_viewPathShort(QKeySequence(Qt::Key_V | Qt::AltModifier), this, SLOT(viewPath()), NULL),
    m_copyPathShort(QKeySequence(Qt::Key_C | Qt::ControlModifier), this, SLOT(copyPath()), NULL),
    m_screenExplorerShort(QKeySequence(Qt::Key_E | Qt::AltModifier), this, SLOT(screenExplorer()), NULL)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->setAcceptDrops(true);
    this->m_updateTimerID = this->startTimer(UPDATE_PLAYLIST_TIME);
}

PlayList::~PlayList()
{
    delete ui;
}

QListWidgetItem* PlayList::createItem(QFileInfo &info, PlayItem &playItem) const
{
    QListWidgetItem *item = new QListWidgetItem;
    QIcon icon;
    QString fileName = info.fileName();
    QString filePath = info.filePath();

    if (Utils::determinDevice(filePath))
        playItem.path = QString::fromUtf8(playItem.extraData.userData);
    else
        playItem.path = Utils::adjustNetworkPath(filePath);

    playItem.unique = QUuid::createUuid();

    if (Utils::determinRemoteProtocol(playItem.path) && !Utils::determinRemoteFile(playItem.path))
        Utils::getExtentionIcon("htm", &icon, false);
    else
        Utils::getExtentionIcon(info.suffix(), &icon, false);

    if (playItem.title.isEmpty())
        item->setText(fileName);
    else
        item->setText(playItem.title);

    item->setIcon(icon);
    item->setData(Qt::UserRole, qVariantFromValue(playItem));

    bool diplayFileName = true;

    if (Utils::determinRemoteProtocol(playItem.path) && !Utils::determinDevice(playItem.path))
    {
        char path[MAX_FILEPATH_CHAR_SIZE];
        QString pathPart;

        av_url_split(NULL, 0, NULL, 0, NULL, 0, NULL, path, sizeof(path), playItem.path.toUtf8().constData());
        pathPart = QString::fromUtf8(path);

        diplayFileName = !QFileInfo(pathPart).suffix().isEmpty();
    }

    QString path;

    if (Utils::determinDevice(playItem.path))
        path = playItem.title;
    else if (fileName.isEmpty() || !diplayFileName)
        path = playItem.path;
    else
        path = fileName;

    this->setItemToolTip(item, path, playItem.totalTime);

    return item;
}

void PlayList::startUpdateThread()
{
    if (!this->m_updater.isRunning())
    {
        this->m_updater.setStop(false);
        this->m_updater.start();
    }
}

void PlayList::setItemToolTip(QListWidgetItem *item, const QString &text, double totalTime) const
{
    QString time;

    if (totalTime > 0.0)
    {
        Utils::getTimeString(totalTime, Utils::TIME_HH_MM_SS, &time);

        time = " (" + time + ")";
    }

    item->setToolTip(text + time);
}

int PlayList::findYouTubeIndex(const QString &quality, const QString &mime, const QVector<YouTubeURLPicker::Item> &items) const
{
    for (int i = 0; i < items.count(); i++)
    {
        const YouTubeURLPicker::Item &item = items[i];

        if (item.quality == quality && item.mimeType == mime)
            return i;
    }

    return 0;
}

void PlayList::setPlayList(const QVector<PlayItem> &list)
{
    this->clearPlayList();
    this->addPlayList(list);
}

bool PlayList::addPlayList(const QVector<PlayItem> &list)
{
    bool added = false;

    this->disbleUpdates();

    for (int i = 0; i < list.count(); i++)
    {
        PlayItem playItem = list[i];
        QFileInfo info(playItem.path);
        QString suffix = info.suffix();
        PlayListParserGenerator gen;
        PlayListParserInterface *parser = gen.getParser(suffix);

        if (parser)
        {
            QVector<PlayListParserInterface::PlayListItem> fileList;

            parser->setRoot(info.absolutePath() + QDir::separator());

            if (parser->open(playItem.path))
            {
                parser->getFileList(&fileList);
                parser->close();

                foreach (const PlayListParserInterface::PlayListItem &item, fileList)
                {
                    QFileInfo fileInfo(item.path);
                    PlayItem fileItem;

                    fileItem.title = item.title;
                    fileItem.itemUpdated = true;

                    this->ui->list->addItem(this->createItem(fileInfo, fileItem));
                    added = true;
                }
            }
            else if (!parser->isSuccess())
            {
                playItem.itemUpdated = true;

                this->ui->list->addItem(this->createItem(info, playItem));
                added = true;
            }

            delete parser;
        }
        else
        {
            this->ui->list->addItem(this->createItem(info, playItem));
            added = true;
        }
    }

    this->enableUpdates();
    this->updateParentWindow(true);

    this->startUpdateThread();

    return added;
}

void PlayList::getPlayList(QVector<PlayItem> *ret)
{
    QMutexLocker locker(&this->m_getPlayListMutex);

    for (int i = 0; i < this->getCount(); i++)
        ret->append(this->getPlayItem(i));
}

int PlayList::getCount() const
{
    return this->ui->list->count();
}

bool PlayList::exist() const
{
    return this->getCount() > 0;
}

QString PlayList::getFileName(int index) const
{
    if (this->exist() && index >= 0 && index < this->getCount())
        return this->ui->list->item(index)->text();
    else
        return QString();
}

PlayItem PlayList::getPlayItem(int index) const
{
    return this->ui->list->item(index)->data(Qt::UserRole).value<PlayItem>();
}

int PlayList::findIndex(const QUuid &unique) const
{
    for (int i = 0; i < this->getCount(); i++)
    {
        PlayItem item = this->getPlayItem(i);

        if (item.unique == unique)
            return i;
    }

    return -1;
}

void PlayList::clearPlayList()
{
    this->stop();
    this->ui->list->clear();
}

void PlayList::stop()
{
    this->m_updater.setStop(true);
    this->m_updater.wait();
}

int PlayList::updateYouTubeData(const QUuid &unique, const QVector<YouTubeURLPicker::Item> &items)
{
    int index = this->findIndex(unique);

    if (index >= 0)
    {
        QListWidgetItem *item = this->ui->list->item(index);
        PlayItem data = item->data(Qt::UserRole).value<PlayItem>();

        data.youtubeData = items;
        data.youtubeIndex = this->findYouTubeIndex(this->m_lastYouTubeQuality, this->m_lastYouTubeMime, items);

        if (data.youtubeIndex == 0)
        {
            this->m_lastYouTubeMime.clear();
            this->m_lastYouTubeQuality.clear();
        }

        item->setData(Qt::UserRole, qVariantFromValue(data));

        return data.youtubeIndex;
    }

    return 0;
}

void PlayList::selectItemOption(int index, QItemSelectionModel::SelectionFlags option)
{
    this->ui->list->setCurrentRow(index, option);
}

void PlayList::selectItem(int index)
{
    this->selectItemOption(index, QItemSelectionModel::ClearAndSelect);
}

void PlayList::dragEnterEvent(QDragEnterEvent *event)
{
    MainWindow *window = Utils::getMainWindow();

    window->dragEnterEvent(event);
}

void PlayList::dropEvent(QDropEvent *event)
{
    MainWindow *window = Utils::getMainWindow();
    QDropEvent e(event->pos(), event->dropAction(),
                 event->mimeData(), event->mouseButtons(),
                 event->keyboardModifiers() | Qt::ControlModifier,
                 event->type());

    window->dropEvent(&e);
}

void PlayList::contextMenuEvent(QContextMenuEvent *)
{
    QMenu menu;
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = Utils::sortByRow(listWidget->selectedItems(), *listWidget);
    int startRow;
    int endRow;
    QSignalMapper otherQualityMapper;

    if (list.empty())
    {
        startRow = -1;
        endRow = 1;
    }
    else
    {
        startRow = listWidget->row(list.first()) - 1;
        endRow = listWidget->row(list.last()) + 1;
    }

    QAction *action = NULL;

    action = menu.addAction(trUtf8("삭제"));
    action->setShortcut(this->m_delShort.key());
    action->setEnabled(list.count() > 0);
    connect(action, SIGNAL(triggered()), this, SLOT(deleteSelection()));

    menu.addSeparator();

    action = menu.addAction(trUtf8("맨 위로"));
    action->setShortcut(this->m_moveToTopShort.key());
    action->setEnabled(startRow >= 0);
    connect(action, SIGNAL(triggered()), this, SLOT(moveToTop()));

    action = menu.addAction(trUtf8("맨 아래로"));
    action->setShortcut(this->m_moveToBottomShort.key());
    action->setEnabled(endRow < listWidget->count());
    connect(action, SIGNAL(triggered()), this, SLOT(moveToBottom()));

    menu.addSeparator();

    action = menu.addAction(trUtf8("위로"));
    action->setShortcut(this->m_moveUpShort.key());
    action->setEnabled(startRow >= 0);
    connect(action, SIGNAL(triggered()), this, SLOT(moveUp()));

    action = menu.addAction(trUtf8("아래로"));
    action->setShortcut(this->m_moveDownShort.key());
    action->setEnabled(endRow < listWidget->count());
    connect(action, SIGNAL(triggered()), this, SLOT(moveDown()));

    menu.addSeparator();

    action = menu.addAction(trUtf8("파일 경로 보기"));
    action->setShortcut(this->m_viewPathShort.key());
    action->setEnabled(listWidget->selectedItems().count() == 1);
    connect(action, SIGNAL(triggered()), this, SLOT(viewPath()));

    action = menu.addAction(trUtf8("파일 경로 복사"));
    action->setShortcut(this->m_copyPathShort.key());
    action->setEnabled(listWidget->selectedItems().count() == 1);
    connect(action, SIGNAL(triggered()), this, SLOT(copyPath()));

    bool enable = false;
    QAction *subAction = NULL;
    QMenu *subMenu = NULL;

    subMenu = menu.addMenu(trUtf8("다른 화질"));

    if (listWidget->selectedItems().count() == 1)
    {
        const QList<QListWidgetItem*> &list = listWidget->selectedItems();
        const PlayItem &playItem = list.first()->data(Qt::UserRole).value<PlayItem>();

        enable = !playItem.youtubeData.isEmpty();

        for (int i = 0; i < playItem.youtubeData.count(); i++)
        {
            const YouTubeURLPicker::Item &item = playItem.youtubeData[i];

            subAction = subMenu->addAction(item.desc);
            subAction->setCheckable(true);
            subAction->setChecked(i == playItem.youtubeIndex);
            connect(subAction, SIGNAL(triggered()), &otherQualityMapper, SLOT(map()));

            otherQualityMapper.setMapping(subAction, i);
        }

        connect(&otherQualityMapper, SIGNAL(mapped(int)), this, SLOT(selectOtherQuality(int)));
    }

    subMenu->setEnabled(enable);

    menu.addSeparator();

    enable = false;

    if (listWidget->selectedItems().count() == 1)
    {
        PlayItem data = list[0]->data(Qt::UserRole).value<PlayItem>();
        bool movieExt = MainWindow::MOVIE_EXTS_LIST.contains(QFileInfo(data.path).suffix(), Qt::CaseInsensitive);

        if ((!Utils::determinRemoteFile(data.path) && !Utils::determinDevice(data.path) && movieExt) ||
                (!Utils::determinRemoteFile(data.path) && Utils::determinRemoteProtocol(data.path)))
            enable = true;
    }

    action = menu.addAction(trUtf8("장면 탐색"));
    action->setShortcut(this->m_screenExplorerShort.key());
    action->setEnabled(enable);
    connect(action, SIGNAL(triggered()), this, SLOT(screenExplorer()));

    Screen *screen = Utils::getMainWindow()->getScreen();

    screen->setDisableHideCusor(true);
    menu.exec(QCursor::pos());
    screen->setDisableHideCusor(false);
}

void PlayList::resizeEvent(QResizeEvent *)
{
    if (!this->m_isInit)
    {
        QWidget *parent = this->parentWidget();
        QPoint pos(parent->x() + parent->frameGeometry().width(), parent->y());
        int height = parent->size().height();

        if (height < DEFAULT_PLAYLIST_SIZE.height())
            height = DEFAULT_PLAYLIST_SIZE.height();

        this->move(pos);
        this->resize(QSize(this->size().width(), height));

        this->m_isInit = true;
    }
}

void PlayList::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void PlayList::customEvent(QEvent *event)
{
    if (event->type() == UPDATE_ITEM_EVENT)
    {
        UpdateItemEvent *e = (UpdateItemEvent*)event;
        int index = this->findIndex(e->getUnique());

        if (index >= 0)
        {
            QListWidgetItem *item = this->ui->list->item(index);
            PlayItem data = item->data(Qt::UserRole).value<PlayItem>();
            QString title = e->getTitle();
            QString toolTip = item->toolTip();

            data.title = title;
            data.totalTime = e->getTotalTime();
            data.itemUpdated = true;

            if (!title.isEmpty())
                item->setText(data.title);

            if (data.totalTime > 0.0)
                this->setItemToolTip(item, toolTip, data.totalTime);

            item->setData(Qt::UserRole, qVariantFromValue(data));
        }
    }
    else if (event->type() == UPDATE_INC_COUNT)
    {
        UpdateIncCountEvent *e = (UpdateIncCountEvent*)event;
        int index = this->findIndex(e->getUnique());

        if (index >= 0)
        {
            QListWidgetItem *item = this->ui->list->item(index);
            PlayItem data = item->data(Qt::UserRole).value<PlayItem>();

            data.retry++;

            if (data.retry > MAX_RETRY_COUNT)
                data.itemUpdated = true;

            item->setData(Qt::UserRole, qVariantFromValue(data));
        }
    }
    else if (event->type() == UPDATE_PARENT_EVENT)
    {
        this->updateParentWindow(true);
    }
    else
    {
        QDialog::customEvent(event);
    }
}

void PlayList::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == this->m_updateTimerID)
        this->startUpdateThread();
}

void PlayList::deleteItem(int index)
{
    QListWidget *list = this->ui->list;
    int scroll = list->verticalScrollBar()->value();
    QListWidgetItem *item = list->takeItem(index);

    this->stop();

    delete item;
    list->verticalScrollBar()->setValue(scroll);
    this->updateParentWindow(true);

    if (index >= list->count())
        index = list->count() - 1;
    else if (index < 0)
        index = 0;

    this->selectItem(index);
}

void PlayList::setFixItemSize(bool fix)
{
    this->ui->list->setUniformItemSizes(fix);
}

void PlayList::moveItems(const QList<QListWidgetItem*> &list, int rowTo, bool dirBottom)
{
    QListWidget *listWidget = this->ui->list;
    QList<QListWidgetItem*> sorted = Utils::sortByRow(list, *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();
    int curIndex = -1;

    if (sorted.count() > 0)
    {
        if (dirBottom)
            curIndex = listWidget->row(sorted.first());
        else
            curIndex = listWidget->row(sorted.last());
    }

    this->disbleUpdates();

    for (int i = 0; i < sorted.count(); i++)
    {
        if (rowTo >= 0 && rowTo <= listWidget->count())
        {
            QListWidgetItem *item = sorted[i];
            int row = listWidget->row(item);

            item = listWidget->takeItem(row);
            listWidget->insertItem(rowTo, item);
        }

        if (!dirBottom)
            rowTo++;
    }

    this->enableUpdates();

    listWidget->verticalScrollBar()->setValue(scroll);
    this->updateParentWindow(true);

    if (curIndex >= listWidget->count())
        curIndex = listWidget->count() - 1;

    if (curIndex >= 0)
        this->selectItem(curIndex);
}

void PlayList::updateParentWindow(bool updateTitle)
{
    MainWindow *window = Utils::getMainWindow();
    int curIndex = window->getCurrentPlayingIndex();

    if (window->setCurrentPlayingIndexByUnique())
    {
        if (window->getCurrentPlayingIndex() < 0)
            window->setCurrentPlayingIndex(curIndex - 1);
    }

    window->adjustPlayIndex();
    window->updateNavigationButtons();

    if (updateTitle && window->getPlayer()->isOpened())
        window->updateWindowTitle();
}

void PlayList::disbleUpdates()
{
    QListWidget *listWidget = this->ui->list;

    listWidget->setUpdatesEnabled(false);
}

void PlayList::enableUpdates()
{
    QListWidget *listWidget = this->ui->list;

    listWidget->setUpdatesEnabled(true);
}

void PlayList::deleteSelection()
{
    MainWindow *window = Utils::getMainWindow();
    QListWidget *list = this->ui->list;
    const QList<QListWidgetItem*> &selectedItems = Utils::sortByRow(list->selectedItems(), *list);
    int scroll = list->verticalScrollBar()->value();
    QListWidgetItem *curItem = list->item(window->getCurrentPlayingIndex());
    bool updateTitle = true;
    int curIndex = -1;

    this->stop();

    if (selectedItems.count() > 0)
        curIndex = list->row(selectedItems.first());

    this->disbleUpdates();

    for (int i = 0; i < selectedItems.count(); i++)
    {
        QListWidgetItem *item = selectedItems[i];

        if (item == curItem)
            updateTitle = false;

        delete item;
    }

    this->enableUpdates();

    list->verticalScrollBar()->setValue(scroll);
    this->updateParentWindow(updateTitle);

    if (curIndex >= list->count())
        curIndex = list->count() - 1;

    if (curIndex >= 0)
        this->selectItem(curIndex);
}

void PlayList::moveToTop()
{
    this->moveItems(this->ui->list->selectedItems(), 0, false);
}

void PlayList::moveToBottom()
{
    this->moveItems(this->ui->list->selectedItems(), this->ui->list->count() - 1, true);
}

void PlayList::moveUp()
{
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = Utils::sortByRow(listWidget->selectedItems(), *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();

    this->disbleUpdates();

    if (list.count() > 0)
    {
        for (int i = 0; i < list.count(); i++)
        {
            int select = listWidget->row(list[i]);

            if (select <= 0)
            {
                this->enableUpdates();
                return;
            }

            QListWidgetItem *item = listWidget->takeItem(select);

            listWidget->insertItem(select - 1, item);
            item->setSelected(true);
        }
    }

    this->enableUpdates();

    listWidget->verticalScrollBar()->setValue(scroll);
    this->updateParentWindow(true);
    this->selectItemOption(listWidget->row(list.first()), QItemSelectionModel::Current);
}

void PlayList::moveDown()
{
    QListWidget *listWidget = this->ui->list;
    const QList<QListWidgetItem*> &list = Utils::sortByRow(listWidget->selectedItems(), *listWidget);
    int scroll = listWidget->verticalScrollBar()->value();

    this->disbleUpdates();

    if (list.count() > 0)
    {
        for (int i = list.count() - 1; i >= 0; i--)
        {
            int select = listWidget->row(list[i]);

            if (select >= listWidget->count() - 1)
            {
                this->enableUpdates();
                return;
            }

            QListWidgetItem *item = listWidget->takeItem(select);

            listWidget->insertItem(select + 1, item);
            item->setSelected(true);
        }
    }

    this->enableUpdates();

    listWidget->verticalScrollBar()->setValue(scroll);
    this->updateParentWindow(true);
    this->selectItemOption(listWidget->row(list.last()), QItemSelectionModel::Current);
}

void PlayList::viewPath()
{
    const QList<QListWidgetItem*> &list = this->ui->list->selectedItems();

    if (list.count() == 1)
    {
        const PlayItem &playItem = list[0]->data(Qt::UserRole).value<PlayItem>();
        QString path = playItem.path;

        if (!Utils::determinRemoteProtocol(path))
            path = QDir::toNativeSeparators(path);

        Utils::informationMessageBox(this, path);
    }
}

void PlayList::copyPath()
{
    QClipboard *clipboard = QApplication::clipboard();
    const QList<QListWidgetItem*> &list = this->ui->list->selectedItems();
    QString pathList;
    QTextStream stream(&pathList);

    foreach (QListWidgetItem *item, list)
    {
        const PlayItem &data = item->data(Qt::UserRole).value<PlayItem>();

        if (Utils::determinRemoteProtocol(data.path))
            stream << data.path << endl;
        else
            stream << QDir::toNativeSeparators(data.path) << endl;
    }

    clipboard->setText(pathList);
}

void PlayList::screenExplorer()
{
    const QList<QListWidgetItem*> &list = this->ui->list->selectedItems();

    if (list.count() == 1)
    {
        const PlayItem &playItem = list[0]->data(Qt::UserRole).value<PlayItem>();
        QString path;
        bool movieExt = MainWindow::MOVIE_EXTS_LIST.contains(QFileInfo(playItem.path).suffix(), Qt::CaseInsensitive);
        bool enable = false;

        if ((!Utils::determinRemoteFile(playItem.path) && movieExt) ||
                Utils::determinRemoteProtocol(playItem.path) ||
                !Utils::determinDevice(playItem.path))
            enable = true;

        if (!enable)
            return;

        QVector<YouTubeURLPicker::Item> youtube;

        if (playItem.youtubeData.isEmpty())
        {
            YouTubeURLPicker picker;

            youtube = picker.pickURL(playItem.path);
        }
        else
        {
            youtube = playItem.youtubeData;
        }

        if (youtube.isEmpty())
            path = playItem.path;
        else
            path = youtube[playItem.youtubeIndex].url;

        ScreenExplorer dlg(path, this->ui->list->currentRow(), Utils::getMainWindow());

        dlg.exec();
    }
}

void PlayList::selectOtherQuality(int index)
{
    const QList<QListWidgetItem*> &list = this->ui->list->selectedItems();

    if (list.count() == 1)
    {
        PlayItem playItem = list[0]->data(Qt::UserRole).value<PlayItem>();
        MainWindow *window = Utils::getMainWindow();

        if (playItem.youtubeData.isEmpty() || playItem.youtubeIndex == index)
            return;

        playItem.youtubeIndex = index;

        this->m_lastYouTubeMime = playItem.youtubeData[index].mimeType;
        this->m_lastYouTubeQuality = playItem.youtubeData[index].quality;

        list[0]->setData(Qt::UserRole, qVariantFromValue(playItem));

        window->playAt(this->ui->list->currentRow());
        window->activateWindow();
    }
}

void PlayList::on_list_itemActivated(QListWidgetItem *)
{
    MainWindow *window = Utils::getMainWindow();

    window->playAt(this->ui->list->currentRow());
    window->activateWindow();
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include <QGlobalStatic>

#define GL_NV_geometry_program4

#ifndef Q_OS_WIN
#define GL_GLEXT_PROTOTYPES
#endif

#include "Screen.h"
#include "MainWindow.h"
#include "net/Socket.h"
#include "core/Utils.h"

#include <qmath.h>
#include <QEvent>
#include <QMouseEvent>
#include <QCoreApplication>
#include <QMenu>
#include <QVector>
#include <QSignalMapper>
#include <QPainter>
#include <QOpenGLFramebufferObject>
#include <QFileInfo>
#include <QDir>
#include <QStandardPaths>
#include <QLocale>
#include <QDebug>

#ifdef Q_OS_WIN
PFNGLGENBUFFERSARBPROC glGenBuffersARB = NULL;
PFNGLBINDBUFFERARBPROC glBindBufferARB = NULL;
PFNGLBUFFERDATAARBPROC glBufferDataARB = NULL;
PFNGLDELETEBUFFERSARBPROC glDeleteBuffersARB = NULL;
PFNGLMAPBUFFERARBPROC glMapBufferARB = NULL;
PFNGLUNMAPBUFFERARBPROC glUnmapBufferARB = NULL;
PFNGLACTIVETEXTUREPROC glActiveTextureARB = NULL;
#endif

const QEvent::Type Screen::PLAYING_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::EMPTY_BUFFER_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::AUDIO_OPTION_DESC_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::AUDIO_SUBTITLE_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::ABORT_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::NONE_PLAYING_DESC_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type Screen::STATUS_CHANGE_EVENT = (QEvent::Type)QEvent::registerEventType();

const int Screen::CURSOR_TIME = 2000;

Screen::Screen(QWidget *parent, Qt::WindowFlags flags) :
    QGLWidget(parent, NULL, flags),
    m_player(new MediaPlayer(0, 0)),
    m_cursorTimer(0),
    m_waitCounter(0),
    m_visibleWait(false),
    m_disableHideCursor(false),
    m_useVSync(false),
    m_render(false),
    m_offScreen(NULL)
{
    QPalette p(this->palette());

    p.setColor(QPalette::Background, Qt::black);
    this->setPalette(p);

    this->setWaitVisible(false);

    MediaPresenter::EventCallback cb;
    cb.callback = Screen::statusChangedCallback;
    cb.userData = this;
    this->m_player->setStatusChangedCallback(cb);

    cb.callback = Screen::playingCallback;
    cb.userData = this;
    this->m_player->setPlayingCallback(cb);

    MediaPresenter::EmptyBufferCallback ecb;

    ecb.callback = Screen::emptyBufferCallback;
    ecb.userData = this;
    this->m_player->setEmptyBufferCallback(ecb);

    MediaPresenter::ShowAudioOptionDescCallback acb;

    acb.callback = Screen::showAudioOptionDescCallback;
    acb.userData = this;
    this->m_player->setShowAudioOptionDescCallback(acb);

    MediaPresenter::AudioSubtitleCallback asc;

    asc.callback = Screen::audioSubtitleCallback;
    asc.userData = this;
    this->m_player->setAudioSubtitleCallback(asc);

    MediaPresenter::PaintCallback pcb;

    pcb.callback = Screen::paintCallback;
    pcb.userData = this;
    this->m_player->setPaintCallback(pcb);

    MediaPresenter::AbortCallback atcb;

    atcb.callback = Screen::abortCallback;
    atcb.userData = this;
    this->m_player->setAbortCallback(atcb);

    MediaPresenter::NonePlayingDescCallback npcb;

    npcb.callback = Screen::nonePlayingDescCallback;
    npcb.userData = this;
    this->m_player->setNonePlayingDescCallback(npcb);

    this->setMouseTracking(true);
    this->setAttribute(Qt::WA_OpaquePaintEvent);
    this->setAttribute(Qt::WA_NoSystemBackground);
    this->setAutoFillBackground(false);
    this->setAutoBufferSwap(false);

    connect(this, SIGNAL(screenUpdated()), this, SLOT(update()));

    this->m_player->resetScreen(this->width(), this->height(), this->m_texInfo, false);

    this->m_captureInfo.savePath = QString("%1/capture")
            .arg(QStandardPaths::standardLocations(QStandardPaths::DataLocation).first());

    Utils::createDirectory(this->m_captureInfo.savePath);
}

Screen::~Screen()
{
    this->m_player->close();
    delete this->m_player;

    this->makeCurrent();

    for (int i = 0; i < TEX_COUNT; i++)
        this->deleteTexture((TextureID)i);

    if (this->m_offScreen)
        delete this->m_offScreen;

    this->doneCurrent();
}

void Screen::setWaitVisible(bool visible)
{
    this->m_visibleWait = visible;
    this->m_waitCounter = 0;
    this->m_waitTimer.restart();
}

void Screen::renderWait()
{
    TextureInfo &texInfo = this->m_texInfo[TEX_MOVIE_FRAME];

    glClear(GL_COLOR_BUFFER_BIT);

    int width;
    int height;
    QRect rect;

    this->m_player->getFrameSize(&width, &height);
    this->m_player->getPictureRect(&rect);

    GLfloat top = 0.0;
    GLfloat left = 0.0;
    GLfloat right = width / (GLfloat)texInfo.maxSize;
    GLfloat bottom = height / (GLfloat)texInfo.maxSize;

    glBindTexture(GL_TEXTURE_2D, texInfo.id[texInfo.index]);

    glBegin(GL_QUADS);
    {
        glTexCoord2f(right, top);
        glVertex2f(rect.right(), rect.top());

        glTexCoord2f(right, bottom);
        glVertex2f(rect.right(), rect.bottom());

        glTexCoord2f(left, bottom);
        glVertex2f(rect.left(), rect.bottom());

        glTexCoord2f(left, top);
        glVertex2f(rect.left(), rect.top());
    }
    glEnd();

    QPainter painter(this);

    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::NoPen);

    painter.setBrush(Qt::SolidPattern);

    qreal maxY = 0.0;

    for (int i = 0; i < 6; i++)
    {
        if ((this->m_waitCounter / 5) % 6 == i)
            painter.setBrush(QBrush(QColor(127 + (this->m_waitCounter % 5) * 32, 127, 127)));
        else
            painter.setBrush(QBrush(QColor(127, 127, 127)));

        qreal y = this->height() / 2 + 30 * qFastSin(2 * M_PI * i / 6.0) - 10;

        maxY = max(maxY, y);
        painter.drawEllipse(this->width() / 2 + 30 * qFastCos(2 * M_PI * i / 6.0) - 10, y, 20, 20);
    }

    if (this->m_waitTimer.elapsed() >= 50)
    {
        this->m_waitCounter++;
        this->m_waitTimer.restart();
    }

    texInfo.index = (texInfo.index + 1) % texInfo.textureCount;

    this->swapBuffers();
}

void Screen::updateMovie(bool visible)
{
    bool rendered = false;

    if (this->m_player->isPlayOrPause())
    {
        bool captureBound = false;
        bool capture = this->m_captureInfo.capture;
        bool captureOrg = this->m_captureInfo.captureOrg;

        if (capture)
        {
            this->m_player->setCaptureMode(true);

            this->capturePrologue(captureOrg);
            captureBound = this->m_offScreen->bind();
        }

        rendered = this->m_player->render(this->m_shader);

        if (capture)
        {
            this->m_offScreen->release();
            this->captureEpilogue(captureOrg, captureBound);

            this->m_player->setCaptureMode(false);
        }

        if (capture && !captureBound)
            this->m_player->showOptionDesc(trUtf8("캡쳐를 시작하지 못했습니다"));
    }
    else
    {
        QPainter painter(this);
        QSize size(this->width(), this->height());
        QImage icon(ICON_PATH);
        QSize iconSize(256, 256);
        QPoint pos((size.width() - iconSize.width()) / 2, (size.height() - iconSize.height()) / 2);
        QRect rect(pos, iconSize);
        QLinearGradient gradient(QPoint(0, 0), QPoint(size.width() / 2, size.height() / 2));

        gradient.setColorAt(0, QColor(244, 244, 244));
        gradient.setColorAt(1, QColor(214, 214, 214));

        painter.save();

        painter.setRenderHint(QPainter::Antialiasing);
        painter.setRenderHint(QPainter::SmoothPixmapTransform);

        painter.fillRect(QRect(QPoint(0, 0), QPoint(size.width(), size.height())), QBrush(gradient));
        painter.drawImage(rect, icon);

        painter.restore();

        rendered = true;
    }

    if (rendered && visible)
        this->swapBuffers();
}

void Screen::capturePrologue(bool captureOrg)
{
    int width;
    int height;

    if (this->m_captureInfo.totalCount - this->m_captureInfo.captureCount <= 0)
    {
        this->m_captureInfo.paused = this->m_player->getStatus() == MediaPlayer::Paused;

        if (this->m_captureInfo.paused)
            this->m_player->resume(true);
    }

    if (captureOrg)
    {
        if (!this->m_player->getFrameSize(&width, &height))
        {
            QSize size = this->size();

            width = size.width();
            height = size.height();
        }
    }
    else
    {
        width = this->m_captureInfo.captureSize.width();
        height = this->m_captureInfo.captureSize.height();
    }

    this->resizeGL(width, height);
}

void Screen::captureEpilogue(bool captureOrg, bool captureBound)
{
    QSize orgSize = this->size();
    QSize offSize = this->m_offScreen->size();
    QRect rect;
    QSize targetSize;
    QRect captureRect;
    int targetWidth;
    int targetHeight;

    this->m_player->pause(true);

    if (captureOrg)
    {
        targetWidth = orgSize.width();
        targetHeight = orgSize.height();

        this->m_player->getFrameSize(&targetWidth, &targetHeight);
    }
    else
    {
        targetWidth = this->m_captureInfo.captureSize.width();
        targetHeight = this->m_captureInfo.captureSize.height();
    }

    targetSize = QSize(targetWidth, targetHeight);
    captureRect = QRect(0, offSize.height() - targetHeight, targetWidth, targetHeight);

    this->resizeGL(orgSize.width(), orgSize.height());
    this->m_player->getPictureRect(&rect);

    GLfloat width = targetSize.width() / (GLfloat)this->m_offScreen->width();
    GLfloat height = targetSize.height() / (GLfloat)this->m_offScreen->height();

    glClear(GL_COLOR_BUFFER_BIT);

    glBindTexture(GL_TEXTURE_2D, this->m_offScreen->texture());

    glBegin(GL_QUADS);
    {
        glTexCoord2f(width, height);
        glVertex2f(rect.right(), rect.top());

        glTexCoord2f(width, 0.0f);
        glVertex2f(rect.right(), rect.bottom());

        glTexCoord2f(0.0f, 0.0f);
        glVertex2f(rect.left(), rect.bottom());

        glTexCoord2f(0.0f, height);
        glVertex2f(rect.left(), rect.top());
    }
    glEnd();

    glBindTexture(GL_TEXTURE_2D, 0);

    QImage captured = this->m_offScreen->toImage();
    QString path("%1%2_%3_%4.%5");
    QString time;
    QString savePath = this->m_captureInfo.savePath;

    Utils::createDirectory(savePath);
    Utils::appendDirSeparator(&savePath);
    Utils::getTimeString(this->m_player->getCurrentPosition(), Utils::TIME_HH_MM_SS_ZZZ_DIR, &time);

    QString fileName;

    if (this->m_player->isPlayUserDataEmpty())
        fileName = QFileInfo(this->m_player->getFileName()).baseName();
    else
        fileName = this->m_player->getTitle();

    path = path.arg(savePath)
            .arg(fileName)
            .arg(time)
            .arg(this->m_captureInfo.totalCount - this->m_captureInfo.captureCount)
            .arg(this->m_captureInfo.ext);

    path = QDir::toNativeSeparators(path);
    captured = captured.copy(captureRect);

    QString desc;

    if (captureBound && captured.save(path, NULL, this->m_captureInfo.quality))
        desc = trUtf8("캡쳐 성공");
    else
        desc = trUtf8("캡쳐 실패");

    desc += " : " + QFileInfo(path).fileName();

    this->m_player->resume(true);
    this->m_player->showOptionDesc(desc);

    if (--this->m_captureInfo.captureCount <= 0)
    {
        this->m_captureInfo.capture = false;

        if (this->m_captureInfo.paused)
            this->m_player->pause(true);
    }

    if (!this->m_player->isPlayOrPause())
    {
        this->m_captureInfo.capture = false;
        this->m_captureInfo.captureCount = 0;
    }
}

MediaPlayer* Screen::getPlayer()
{
    return this->m_player;
}

void Screen::setCaptureInfo(const CaptureInfo &captureInfo)
{
    this->setCaptureInfo(captureInfo, false);
}

void Screen::setCaptureInfo(const CaptureInfo &captureInfo, bool raw)
{
    QString desc;

    if (captureInfo.ext != this->m_captureInfo.ext)
        desc = trUtf8("캡쳐 확장자 변경 : %1").arg(captureInfo.ext.toUpper());

    if (captureInfo.savePath != this->m_captureInfo.savePath)
        desc = trUtf8("캡쳐 저장 경로 변경 : %1").arg(captureInfo.savePath);

    if (!raw && !desc.isEmpty())
        this->m_player->showOptionDesc(desc);

    this->m_captureInfo = captureInfo;
}

void Screen::getCaptureInfo(CaptureInfo *ret) const
{
    *ret = this->m_captureInfo;
}

void Screen::startCapture()
{
    this->m_captureInfo.capture = true;
}

void Screen::stopCapture()
{
    this->m_captureInfo.capture = false;
}

void Screen::setCaptureSavePath(const QString &dir)
{
    this->m_captureInfo.savePath = dir;
}

void Screen::toggleUseVSync()
{
    this->setUseVSync(!this->isUseVSync());
    Utils::informationMessageBox(this, trUtf8("수직 동기화 상태를 변경 후 적용 하려면 프로그램을 재 시작 해야 합니다"));
}

void Screen::setUseVSync(bool use)
{
    this->m_useVSync = use;
}

bool Screen::isUseVSync() const
{
    return this->m_useVSync;
}

QString Screen::getCaptureSavePath() const
{
    return this->m_captureInfo.savePath;
}

ShaderCompositer& Screen::getShader()
{
    return this->m_shader;
}

void Screen::setDisableHideCusor(bool disable)
{
    this->m_disableHideCursor = disable;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Waddress"

void Screen::deleteTexture(TextureID type)
{
    TextureInfo &info = this->m_texInfo[type];

    for (unsigned int i = 0; i < info.textureCount; i++)
    {
        if (info.id[i])
        {
            glDeleteTextures(1, &info.id[i]);
            info.id[i] = 0;
        }
    }

    if (glDeleteBuffersARB != NULL)
    {
        glDeleteBuffersARB(MAX_PBO_COUNT, info.idPBO);

        memset(info.idPBO, 0, sizeof(info.idPBO));
        info.indexPBO = 0;
    }
}

void Screen::genOffScreen(int size)
{
    if (this->m_offScreen)
        delete this->m_offScreen;

    this->m_offScreen = new QOpenGLFramebufferObject(size, size);

    glBindTexture(GL_TEXTURE_2D, this->m_offScreen->texture());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, 0);

    this->m_offScreen->release();
}

void Screen::genTexture(TextureID type, int size)
{
    TextureInfo &info = this->m_texInfo[type];

    info.textureCount = 1;

    for (unsigned int i = 0; i < info.textureCount; i++)
    {
        glGenTextures(1, &info.id[i]);

        glBindTexture(GL_TEXTURE_2D, info.id[i]);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size, size, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    }

    if (glGenBuffersARB != NULL)
        glGenBuffersARB(MAX_PBO_COUNT, info.idPBO);

    glBindTexture(GL_TEXTURE_2D, 0);

    info.maxSize = size;

    this->clearTexture(type);
}

#pragma GCC diagnostic pop

void Screen::clearTexture(TextureID type)
{
    TextureInfo &info = this->m_texInfo[type];
    QImage clear(info.maxSize, info.maxSize, QImage::Format_ARGB32);

    clear.fill(Qt::transparent);

    for (unsigned int i = 0; i < info.textureCount; i++)
    {
        glBindTexture(GL_TEXTURE_2D, info.id[i]);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, info.maxSize, info.maxSize, GL_RGBA, GL_UNSIGNED_BYTE, clear.constBits());
    }

    glBindTexture(GL_TEXTURE_2D, 0);
}

void Screen::initTextures(bool autoSize)
{
    GLint maxSize;

    this->makeCurrent();

    if (autoSize)
    {
        glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxSize);

        if (maxSize > DEFAULT_MAX_TEXTURE_SIZE)
            maxSize = DEFAULT_MAX_TEXTURE_SIZE;
    }
    else
    {
        int width;
        int height;

        if (this->m_player->getFrameSize(&width, &height))
        {
            maxSize = max(width, height);

            int powValue;

            frexp(maxSize, &powValue);
            maxSize = ::pow(2, powValue);
        }
        else
        {
            maxSize = DEFAULT_MIN_TEXTURE_SIZE;
        }
    }

    glEnable(GL_TEXTURE_2D);

    for (int i = 0; i < TEX_COUNT; i++)
        this->deleteTexture((TextureID)i);

    this->genTexture(TEX_MOVIE_FRAME, maxSize);
    this->genTexture(TEX_FFMPEG_SUBTITLE, maxSize);
    this->genTexture(TEX_ASS_SUBTITLE, maxSize);
    this->genOffScreen(maxSize);

    this->m_player->setMaxTextureSize(maxSize);

    this->doneCurrent();
}

void Screen::initializeGL()
{
#ifdef Q_OS_WIN
    glGenBuffersARB = (PFNGLGENBUFFERSARBPROC)wglGetProcAddress("glGenBuffersARB");
    glBindBufferARB = (PFNGLBINDBUFFERARBPROC)wglGetProcAddress("glBindBufferARB");
    glBufferDataARB = (PFNGLBUFFERDATAARBPROC)wglGetProcAddress("glBufferDataARB");
    glDeleteBuffersARB = (PFNGLDELETEBUFFERSARBPROC)wglGetProcAddress("glDeleteBuffersARB");
    glBindBufferARB = (PFNGLBINDBUFFERARBPROC)wglGetProcAddress("glBindBufferARB");
    glBufferDataARB = (PFNGLBUFFERDATAARBPROC)wglGetProcAddress("glBufferDataARB");
    glMapBufferARB = (PFNGLMAPBUFFERARBPROC)wglGetProcAddress("glMapBufferARB");
    glUnmapBufferARB = (PFNGLUNMAPBUFFERARBPROC)wglGetProcAddress("glUnmapBufferARB");
    glActiveTextureARB = (PFNGLACTIVETEXTUREPROC)wglGetProcAddress("glActiveTextureARB");
#endif

    glShadeModel(GL_FLAT);
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    this->m_shader.build();
}

void Screen::resizeGL(int width, int height)
{
    MainWindow *window = Utils::getMainWindow();

    if (window->isFullScreening())
        height += SCREEN_TOP_OFFSET;

    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, width, height, 0.0, -1.0, 1.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    QSize size(width, height);

    if (size.width() <= 0)
        size.setWidth(MainWindow::DEFAULT_SCREEN_SIZE.width());

    if (size.height() <= 0)
        size.setHeight(MainWindow::DEFAULT_SCREEN_SIZE.height());

    this->m_player->resetScreen(size.width(), size.height(), this->m_texInfo, true);
}

void Screen::paintGL()
{
    bool render;

    if (!this->m_player->isPlayOrPause())
    {
        render = true;
    }
    else if (this->m_render)
    {
        render = true;
        this->m_render = false;
    }
    else
    {
        render = false;
    }

    if (render)
    {
        if (this->m_visibleWait)
            this->renderWait();

        this->updateMovie(!this->m_visibleWait);
    }
}

void Screen::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == this->m_cursorTimer && !this->m_disableHideCursor)
    {
        if (!this->m_player->isPlayOrPause() || !this->m_player->isEnabledVideo())
            return;

        QPoint localPos = this->mapFromGlobal(QCursor::pos());

        if (localPos.x() < 0 || localPos.y() < 0)
            return;

        QSize size = this->size();

        if (localPos.y() < size.height() && localPos.x() < size.width())
            Utils::getMainWindow()->setCursor(Qt::BlankCursor);
    }
}

void Screen::mouseMoveEvent(QMouseEvent *event)
{
    MainWindow *parent = Utils::getMainWindow();

    Utils::getMainWindow()->unsetCursor();
    parent->mouseMoveEvent(event);
    event->accept();
}

void Screen::mousePressEvent(QMouseEvent *event)
{
    Utils::getMainWindow()->unsetCursor();
    event->ignore();
}

void Screen::mouseReleaseEvent(QMouseEvent *event)
{
    Utils::getMainWindow()->unsetCursor();
    event->ignore();
}

void Screen::contextMenuEvent(QContextMenuEvent *)
{
    this->showContextMenu();
}

void Screen::customEvent(QEvent *event)
{
    if (event->type() == STATUS_CHANGE_EVENT)
    {
        StatusChangeEvent *e = (StatusChangeEvent*)event;
        MediaPlayer::Status status = e->getStatus();

        if (status == MediaPlayer::Started ||
                status == MediaPlayer::Playing ||
                status == MediaPlayer::Paused ||
                status == MediaPlayer::Stopped ||
                status == MediaPlayer::Ended)
        {
            if (status == MediaPlayer::Stopped)
            {
                this->update();
                this->setWaitVisible(false);
                Utils::getMainWindow()->unsetCursor();

                if (this->m_cursorTimer != 0)
                {
                    this->killTimer(this->m_cursorTimer);
                    this->m_cursorTimer = 0;
                }
            }
            else if (status == MediaPlayer::Started)
            {
                this->m_cursorTimer = this->startTimer(CURSOR_TIME);
                this->initTextures(false);
            }

            if (!this->getPlayer()->isEnabledVideo())
                this->setWaitVisible(false);
        }
    }
    else if (event->type() == EMPTY_BUFFER_EVENT)
    {
        EmptyBufferEvent *e = (EmptyBufferEvent*)event;

        if (this->m_player->isAudio())
            QCoreApplication::postEvent(this->window(), new EmptyBufferEvent(e->isEmpty()));
        else
            this->setWaitVisible(e->isEmpty());
    }
    else
    {
        QWidget::customEvent(event);
    }
}

void Screen::playingCallback(void *userData)
{
    Screen *parent = (Screen*)userData;

    QCoreApplication::postEvent(parent->window(), new PlayingEvent());
}

void Screen::statusChangedCallback(void *userData)
{
    Screen *parent = (Screen*)userData;
    MediaPlayer::Status status = parent->getPlayer()->getStatus();

    QCoreApplication::postEvent(parent, new StatusChangeEvent(status));
    QCoreApplication::postEvent(parent->window(), new StatusChangeEvent(status));
}

void Screen::emptyBufferCallback(void *userData, bool empty)
{
    QCoreApplication::postEvent((Screen*)userData, new EmptyBufferEvent(empty));
}

void Screen::showAudioOptionDescCallback(void *userData, const QString &desc, bool show)
{
    Screen *parent = (Screen*)userData;

    QCoreApplication::postEvent(parent->window(), new ShowAudioOptionDescEvent(desc, show));
}

void Screen::audioSubtitleCallback(void *userData, const QVector<Lyrics> &lines)
{
    Screen *parent = (Screen*)userData;

    QCoreApplication::postEvent(parent->window(), new AudioSubtitleEvent(lines));
}

void Screen::paintCallback(void *userData)
{
    Screen *parent = (Screen*)userData;

    parent->m_render = true;
    emit parent->screenUpdated();
}

void Screen::abortCallback(void *userData, int reason)
{
    Screen *parent = (Screen*)userData;

    QCoreApplication::postEvent(parent->window(), new AbortEvent(reason));
}

void Screen::nonePlayingDescCallback(void *userData, const QString &desc)
{
    Screen *parent = (Screen*)userData;
    MainWindow *window = Utils::getMainWindow();

    if (window->isReady())
        QCoreApplication::postEvent(parent->window(), new NonePlayingDescEvent(desc));
}

void Screen::createScreenRotationDegreeMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    AnyVODEnums::ScreenRotationDegree degree = this->m_player->getScreenRotationDegree();

    subMenu = menu->addMenu(trUtf8("화면 회전 각도"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SCREEN_ROTATION_DEGREE_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SCREEN_ROTATION_DEGREE_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(screenRotationDegreeOrder()));

    subAction = subMenu->addAction(trUtf8("사용 안 함"));
    subAction->setCheckable(true);
    subAction->setChecked(degree == AnyVODEnums::SRD_NONE);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::SRD_NONE);

    subAction = subMenu->addAction(trUtf8("90도"));
    subAction->setCheckable(true);
    subAction->setChecked(degree == AnyVODEnums::SRD_90);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::SRD_90);

    subAction = subMenu->addAction(trUtf8("180도"));
    subAction->setCheckable(true);
    subAction->setChecked(degree == AnyVODEnums::SRD_180);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::SRD_180);

    subAction = subMenu->addAction(trUtf8("270도"));
    subAction->setCheckable(true);
    subAction->setChecked(degree == AnyVODEnums::SRD_270);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::SRD_270);

    connect(mapper, SIGNAL(mapped(int)), window, SLOT(selectScreenRotationDegree(int)));
}

void Screen::create3DMenu(QMenu *menu, MainWindow *window, QSignalMapper *typeMapper, QSignalMapper *anaAlgorithmMapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    AnyVODEnums::Video3DMethod method = this->m_player->get3DMethod();

    subMenu = menu->addMenu(trUtf8("3D 영상 설정"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USE_3D_FULL));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USE_3D_FULL));
    subAction->setEnabled(method != AnyVODEnums::V3M_NONE);
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUse3DFull());
    connect(subAction, SIGNAL(triggered()), window, SLOT(use3DFull()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_3D_VIDEO_METHOD_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_3D_VIDEO_METHOD_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(method3DOrder()));

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_NONE));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_NONE);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_NONE);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_HALF_LEFT));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_HALF_LEFT);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_HALF_LEFT);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_HALF_RIGHT));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_HALF_RIGHT);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_HALF_RIGHT);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_HALF_TOP));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_HALF_TOP);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_HALF_TOP);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_HALF_BOTTOM));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_HALF_BOTTOM);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_HALF_BOTTOM);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_FULL_LEFT_RIGHT));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_FULL_LEFT_RIGHT);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_FULL_LEFT_RIGHT);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_FULL_TOP_BOTTOM));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_FULL_TOP_BOTTOM);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_FULL_TOP_BOTTOM);

    subMenu->addSeparator();

    this->create3DPageFlipMenu(subMenu, window, typeMapper);
    this->create3DInterlaceMenu(subMenu, window, typeMapper);
    this->create3DCheckerBoardMenu(subMenu, window, typeMapper);
    this->create3DAnaglyphMenu(subMenu, window, typeMapper, anaAlgorithmMapper);

    connect(typeMapper, SIGNAL(mapped(int)), window, SLOT(select3DMethod(int)));
}

void Screen::create3DCheckerBoardMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    AnyVODEnums::Video3DMethod method = this->m_player->get3DMethod();

    subMenu = menu->addMenu(trUtf8("체커 보드"));

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR);
}

void Screen::create3DPageFlipMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    AnyVODEnums::Video3DMethod method = this->m_player->get3DMethod();

    subMenu = menu->addMenu(trUtf8("페이지 플리핑"));

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR);
}

void Screen::create3DSubtitleMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    AnyVODEnums::Subtitle3DMethod method = this->m_player->getSubtitle3DMethod();

    subMenu = menu->addMenu(trUtf8("3D 자막 설정"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_RESET_3D_SUBTITLE_OFFSET));
    subAction->setEnabled(this->m_player->is3DSubtitleMoveable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_RESET_3D_SUBTITLE_OFFSET));
    connect(subAction, SIGNAL(triggered()), window, SLOT(reset3DSubtitleOffset()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_UP_3D_SUBTITLE_OFFSET));
    subAction->setEnabled(this->m_player->is3DSubtitleMoveable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_UP_3D_SUBTITLE_OFFSET));
    connect(subAction, SIGNAL(triggered()), window, SLOT(up3DSubtitleOffset()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_DOWN_3D_SUBTITLE_OFFSET));
    subAction->setEnabled(this->m_player->is3DSubtitleMoveable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_DOWN_3D_SUBTITLE_OFFSET));
    connect(subAction, SIGNAL(triggered()), window, SLOT(down3DSubtitleOffset()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_LEFT_3D_SUBTITLE_OFFSET));
    subAction->setEnabled(this->m_player->is3DSubtitleMoveable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_LEFT_3D_SUBTITLE_OFFSET));
    connect(subAction, SIGNAL(triggered()), window, SLOT(left3DSubtitleOffset()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_RIGHT_3D_SUBTITLE_OFFSET));
    subAction->setEnabled(this->m_player->is3DSubtitleMoveable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_RIGHT_3D_SUBTITLE_OFFSET));
    connect(subAction, SIGNAL(triggered()), window, SLOT(right3DSubtitleOffset()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_3D_SUBTITLE_METHOD_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_3D_SUBTITLE_METHOD_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(method3DSubtitleOrder()));

    subAction = subMenu->addAction(window->getSubtitle3DMethodDesc(AnyVODEnums::S3M_NONE));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::S3M_NONE);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::S3M_NONE);

    subAction = subMenu->addAction(window->getSubtitle3DMethodDesc(AnyVODEnums::S3M_LEFT_RIGHT));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::S3M_LEFT_RIGHT);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::S3M_LEFT_RIGHT);

    subAction = subMenu->addAction(window->getSubtitle3DMethodDesc(AnyVODEnums::S3M_TOP_BOTTOM));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::S3M_TOP_BOTTOM);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::S3M_TOP_BOTTOM);

    subAction = subMenu->addAction(window->getSubtitle3DMethodDesc(AnyVODEnums::S3M_PAGE_FLIP));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::S3M_PAGE_FLIP);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::S3M_PAGE_FLIP);

    subAction = subMenu->addAction(window->getSubtitle3DMethodDesc(AnyVODEnums::S3M_INTERLACED));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::S3M_INTERLACED);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::S3M_INTERLACED);

    subAction = subMenu->addAction(window->getSubtitle3DMethodDesc(AnyVODEnums::S3M_CHECKER_BOARD));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::S3M_CHECKER_BOARD);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::S3M_CHECKER_BOARD);

    subAction = subMenu->addAction(window->getSubtitle3DMethodDesc(AnyVODEnums::S3M_ANAGLYPH));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::S3M_ANAGLYPH);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::S3M_ANAGLYPH);

    connect(mapper, SIGNAL(mapped(int)), window, SLOT(select3DSubtitleMethod(int)));
}

void Screen::create3DInterlaceMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    AnyVODEnums::Video3DMethod method = this->m_player->get3DMethod();

    subMenu = menu->addMenu(trUtf8("인터레이스"));

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR);
}

void Screen::create3DAnaglyphMenu(QMenu *menu, MainWindow *window, QSignalMapper *typeMapper, QSignalMapper *anaAlgorithmMapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    AnyVODEnums::Video3DMethod method = this->m_player->get3DMethod();
    AnyVODEnums::AnaglyphAlgorithm anaAlgorithm = this->m_shader.getAnaglyphAlgorithm();

    subMenu = menu->addMenu(trUtf8("애너글리프"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_ANAGLYPH_ALGORITHM_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_ANAGLYPH_ALGORITHM_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(anaglyphAlgorithmOrder()));

    subAction = subMenu->addAction(window->getAnaglyphAlgoritmDesc(AnyVODEnums::AGA_DUBOIS));
    subAction->setEnabled(this->m_shader.canUseAnaglyphAlgorithm());
    subAction->setCheckable(true);
    subAction->setChecked(anaAlgorithm == AnyVODEnums::AGA_DUBOIS);
    connect(subAction, SIGNAL(triggered()), anaAlgorithmMapper, SLOT(map()));
    anaAlgorithmMapper->setMapping(subAction, AnyVODEnums::AGA_DUBOIS);

    subAction = subMenu->addAction(window->getAnaglyphAlgoritmDesc(AnyVODEnums::AGA_COLORED));
    subAction->setEnabled(this->m_shader.canUseAnaglyphAlgorithm());
    subAction->setCheckable(true);
    subAction->setChecked(anaAlgorithm == AnyVODEnums::AGA_COLORED);
    connect(subAction, SIGNAL(triggered()), anaAlgorithmMapper, SLOT(map()));
    anaAlgorithmMapper->setMapping(subAction, AnyVODEnums::AGA_COLORED);

    subAction = subMenu->addAction(window->getAnaglyphAlgoritmDesc(AnyVODEnums::AGA_HALF_COLORED));
    subAction->setEnabled(this->m_shader.canUseAnaglyphAlgorithm());
    subAction->setCheckable(true);
    subAction->setChecked(anaAlgorithm == AnyVODEnums::AGA_HALF_COLORED);
    connect(subAction, SIGNAL(triggered()), anaAlgorithmMapper, SLOT(map()));
    anaAlgorithmMapper->setMapping(subAction, AnyVODEnums::AGA_HALF_COLORED);

    subAction = subMenu->addAction(window->getAnaglyphAlgoritmDesc(AnyVODEnums::AGA_GRAY));
    subAction->setEnabled(this->m_shader.canUseAnaglyphAlgorithm());
    subAction->setCheckable(true);
    subAction->setChecked(anaAlgorithm == AnyVODEnums::AGA_GRAY);
    connect(subAction, SIGNAL(triggered()), anaAlgorithmMapper, SLOT(map()));
    anaAlgorithmMapper->setMapping(subAction, AnyVODEnums::AGA_GRAY);

    connect(anaAlgorithmMapper, SIGNAL(mapped(int)), window, SLOT(selectAnaglyphAlgorithm(int)));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR);

    subAction = subMenu->addAction(window->get3DMethodDesc(AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR);
    connect(subAction, SIGNAL(triggered()), typeMapper, SLOT(map()));
    typeMapper->setMapping(subAction, AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR);
}

void Screen::createScreenRatioMenu(QMenu *menu, MainWindow *window)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("화면 크기"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SCREEN_SIZE_HALF));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SCREEN_SIZE_HALF));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(screenSizeHalf()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SCREEN_SIZE_NORMAL));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SCREEN_SIZE_NORMAL));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(screenSizeNormal()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SCREEN_SIZE_NORMAL_HALF));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SCREEN_SIZE_NORMAL_HALF));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(screenSizeNormalHalf()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SCREEN_SIZE_DOUBLE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SCREEN_SIZE_DOUBLE));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(screenSizeDouble()));
}

void Screen::createUserAspectRatioMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    MainWindow::UserAspectRatioList list;

    window->getUserAspectRatioList(&list);

    subMenu = menu->addMenu(trUtf8("화면 비율"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USER_ASPECT_RATIO_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USER_ASPECT_RATIO_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(userAspectRatioOrder()));

    for (int i = 0; i < list.list.count(); i++)
    {
        MainWindow::UserAspectRatioItem &item = list.list[i];
        QString title;

        if (item.isInvalid())
            title = trUtf8("사용 안 함");
        else if (item.isFullScreen())
            title = trUtf8("화면 채우기");
        else if (i == list.list.count() - 1)
            title = trUtf8("사용자 지정 (%1:%2)").arg(item.width).arg(item.height);
        else
            title = QString("%1:%2").arg(item.width).arg(item.height);

        subAction = subMenu->addAction(title);
        subAction->setCheckable(true);
        subAction->setChecked(i == list.curIndex);
        connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));

        mapper->setMapping(subAction, i);
    }

    connect(mapper, SIGNAL(mapped(int)), window, SLOT(selectUserAspectRatio(int)));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USER_ASPECT_RATIO));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USER_ASPECT_RATIO));
    connect(subAction, SIGNAL(triggered()), window, SLOT(userAspectRatio()));
}

void Screen::createDeinterlaceMenu(QMenu *menu, MainWindow *window, QSignalMapper *methodMapper, QSignalMapper *algorithmMapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    AnyVODEnums::DeinterlaceMethod deMethod = this->m_player->getDeinterlaceMethod();

    subMenu = menu->addMenu(trUtf8("디인터레이스"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(deinterlaceMethodOrder()));

    subAction = subMenu->addAction(trUtf8("자동 판단"));
    subAction->setCheckable(true);
    subAction->setChecked(deMethod == AnyVODEnums::DM_AUTO);
    connect(subAction, SIGNAL(triggered()), methodMapper, SLOT(map()));
    methodMapper->setMapping(subAction, AnyVODEnums::DM_AUTO);

    subAction = subMenu->addAction(trUtf8("항상 사용"));
    subAction->setCheckable(true);
    subAction->setChecked(deMethod == AnyVODEnums::DM_USE);
    connect(subAction, SIGNAL(triggered()), methodMapper, SLOT(map()));
    methodMapper->setMapping(subAction, AnyVODEnums::DM_USE);

    subAction = subMenu->addAction(trUtf8("사용 안 함"));
    subAction->setCheckable(true);
    subAction->setChecked(deMethod == AnyVODEnums::DM_NOUSE);
    connect(subAction, SIGNAL(triggered()), methodMapper, SLOT(map()));
    methodMapper->setMapping(subAction, AnyVODEnums::DM_NOUSE);

    connect(methodMapper, SIGNAL(mapped(int)), window, SLOT(selectDeinterlacerMethod(int)));

    AnyVODEnums::DeinterlaceAlgorithm deAlgorithm = this->m_player->getDeinterlaceAlgorithm();

    subMenu = subMenu->addMenu(trUtf8("알고리즘"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(deinterlaceAlgorithmOrder()));

    subAction = subMenu->addAction(window->getDeinterlaceDesc(AnyVODEnums::DA_BLEND));
    subAction->setCheckable(true);
    subAction->setChecked(deAlgorithm == AnyVODEnums::DA_BLEND);
    connect(subAction, SIGNAL(triggered()), algorithmMapper, SLOT(map()));
    algorithmMapper->setMapping(subAction, AnyVODEnums::DA_BLEND);

    subAction = subMenu->addAction(window->getDeinterlaceDesc(AnyVODEnums::DA_BOB));
    subAction->setCheckable(true);
    subAction->setChecked(deAlgorithm == AnyVODEnums::DA_BOB);
    connect(subAction, SIGNAL(triggered()), algorithmMapper, SLOT(map()));
    algorithmMapper->setMapping(subAction, AnyVODEnums::DA_BOB);

    subAction = subMenu->addAction(window->getDeinterlaceDesc(AnyVODEnums::DA_YADIF));
    subAction->setCheckable(true);
    subAction->setChecked(deAlgorithm == AnyVODEnums::DA_YADIF);
    connect(subAction, SIGNAL(triggered()), algorithmMapper, SLOT(map()));
    algorithmMapper->setMapping(subAction, AnyVODEnums::DA_YADIF);

    subAction = subMenu->addAction(window->getDeinterlaceDesc(AnyVODEnums::DA_YADIF_BOB));
    subAction->setCheckable(true);
    subAction->setChecked(deAlgorithm == AnyVODEnums::DA_YADIF_BOB);
    connect(subAction, SIGNAL(triggered()), algorithmMapper, SLOT(map()));
    algorithmMapper->setMapping(subAction, AnyVODEnums::DA_YADIF_BOB);

    subAction = subMenu->addAction(window->getDeinterlaceDesc(AnyVODEnums::DA_W3FDIF));
    subAction->setCheckable(true);
    subAction->setChecked(deAlgorithm == AnyVODEnums::DA_W3FDIF);
    connect(subAction, SIGNAL(triggered()), algorithmMapper, SLOT(map()));
    algorithmMapper->setMapping(subAction, AnyVODEnums::DA_W3FDIF);

    subAction = subMenu->addAction(window->getDeinterlaceDesc(AnyVODEnums::DA_KERNDEINT));
    subAction->setCheckable(true);
    subAction->setChecked(deAlgorithm == AnyVODEnums::DA_KERNDEINT);
    connect(subAction, SIGNAL(triggered()), algorithmMapper, SLOT(map()));
    algorithmMapper->setMapping(subAction, AnyVODEnums::DA_KERNDEINT);

    subAction = subMenu->addAction(window->getDeinterlaceDesc(AnyVODEnums::DA_MCDEINT));
    subAction->setCheckable(true);
    subAction->setChecked(deAlgorithm == AnyVODEnums::DA_MCDEINT);
    connect(subAction, SIGNAL(triggered()), algorithmMapper, SLOT(map()));
    algorithmMapper->setMapping(subAction, AnyVODEnums::DA_MCDEINT);

    subAction = subMenu->addAction(window->getDeinterlaceDesc(AnyVODEnums::DA_BWDIF));
    subAction->setCheckable(true);
    subAction->setChecked(deAlgorithm == AnyVODEnums::DA_BWDIF);
    connect(subAction, SIGNAL(triggered()), algorithmMapper, SLOT(map()));
    algorithmMapper->setMapping(subAction, AnyVODEnums::DA_BWDIF);

    subAction = subMenu->addAction(window->getDeinterlaceDesc(AnyVODEnums::DA_BWDIF_BOB));
    subAction->setCheckable(true);
    subAction->setChecked(deAlgorithm == AnyVODEnums::DA_BWDIF_BOB);
    connect(subAction, SIGNAL(triggered()), algorithmMapper, SLOT(map()));
    algorithmMapper->setMapping(subAction, AnyVODEnums::DA_BWDIF_BOB);

    connect(algorithmMapper, SIGNAL(mapped(int)), window, SLOT(selectDeinterlacerAlgorithem(int)));
}

void Screen::createCaptureMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    QString curExt = this->m_captureInfo.ext.toUpper();

    subMenu = menu->addMenu(trUtf8("캡쳐"));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(captureExtOrder()));

    for (int i = 0; i < MainWindow::CAPTURE_FORMAT_LIST.count(); i++)
    {
        QString ext = MainWindow::CAPTURE_FORMAT_LIST.at(i).toUpper();

        subAction = subMenu->addAction(ext);
        subAction->setCheckable(true);
        subAction->setChecked(curExt == ext);
        connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));

        mapper->setMapping(subAction, ext);
    }

    connect(mapper, SIGNAL(mapped(QString)), window, SLOT(selectCaptureExt(const QString&)));
    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_CAPTURE_SINGLE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_CAPTURE_SINGLE));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(captureSingle()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_CAPTURE_MULTIPLE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_CAPTURE_MULTIPLE));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(captureMultiple()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY));
    connect(subAction, SIGNAL(triggered()), window, SLOT(selectCaptureSaveDir()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_OPEN_CAPTURE_DIRECTORY));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_OPEN_CAPTURE_DIRECTORY));
    connect(subAction, SIGNAL(triggered()), window, SLOT(openCaptureSaveDir()));
}

void Screen::createVideoAttributeMenu(QMenu *menu, MainWindow *window)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("영상 속성"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_RESET_VIDEO_ATTRIBUTE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_RESET_VIDEO_ATTRIBUTE));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(resetVideoAttribute()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_BRIGHTNESS_DOWN));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_BRIGHTNESS_DOWN));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(brightnessDown()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_BRIGHTNESS_UP));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_BRIGHTNESS_UP));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(brightnessUp()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SATURATION_DOWN));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SATURATION_DOWN));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(saturationDown()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SATURATION_UP));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SATURATION_UP));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(saturationUp()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_HUE_DOWN));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_HUE_DOWN));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(hueDown()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_HUE_UP));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_HUE_UP));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(hueUp()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_CONTRAST_DOWN));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_CONTRAST_DOWN));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(contrastDown()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_CONTRAST_UP));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_CONTRAST_UP));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(contrastUp()));
}

void Screen::createVideoEffectMenu(QMenu *menu, MainWindow *window)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("영상 효과"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SHARPLY));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_shader.isUsingSharply());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SHARPLY));
    connect(subAction, SIGNAL(triggered()), window, SLOT(sharply()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SHARPEN));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_shader.isUsingSharpen());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SHARPEN));
    connect(subAction, SIGNAL(triggered()), window, SLOT(sharpen()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SOFTEN));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_shader.isUsingSoften());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SOFTEN));
    connect(subAction, SIGNAL(triggered()), window, SLOT(soften()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_HISTOGRAM_EQ));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->getFilterGraph().getEnableHistEQ());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_HISTOGRAM_EQ));
    connect(subAction, SIGNAL(triggered()), window, SLOT(histEQ()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_DEBAND));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->getFilterGraph().getEnableDeBand());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_DEBAND));
    connect(subAction, SIGNAL(triggered()), window, SLOT(deband()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_HIGH_QUALITY_3D_DENOISE));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->getFilterGraph().getEnableHighQuality3DDenoise());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_HIGH_QUALITY_3D_DENOISE));
    connect(subAction, SIGNAL(triggered()), window, SLOT(hq3DDenoise()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_ATA_DENOISE));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->getFilterGraph().getEnableATADenoise());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_ATA_DENOISE));
    connect(subAction, SIGNAL(triggered()), window, SLOT(ataDenoise()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_OW_DENOISE));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->getFilterGraph().getEnableOWDenoise());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_OW_DENOISE));
    connect(subAction, SIGNAL(triggered()), window, SLOT(owDenoise()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_LEFT_RIGHT_INVERT));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_shader.isUsingLeftRightInvert());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_LEFT_RIGHT_INVERT));
    connect(subAction, SIGNAL(triggered()), window, SLOT(leftRightInvert()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_TOP_BOTTOM_INVERT));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_shader.isUsingTopBottomInvert());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_TOP_BOTTOM_INVERT));
    connect(subAction, SIGNAL(triggered()), window, SLOT(topBottomInvert()));
}

void Screen::createDefaultLanguageMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    QString curLang = window->getLanguage();
    QDir dir(MainWindow::LANG_DIR);
    QStringList fileNames = dir.entryList(QStringList(QString("%1_*.qm").arg(MainWindow::LANG_PREFIX)));

    subMenu = menu->addMenu(trUtf8("Language"));

    for (int i = 0; i < fileNames.count(); i++)
    {
        QString lang = fileNames[i];

        lang.truncate(lang.lastIndexOf('.'));
        lang.remove(0, lang.indexOf('_') + 1);

        QLocale locale(lang);

        subAction = subMenu->addAction(QString("%1 (%2)").arg(locale.nativeLanguageName()).arg(locale.nativeCountryName()));
        subAction->setCheckable(true);
        subAction->setChecked(curLang == lang);

        connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));

        mapper->setMapping(subAction, lang);
    }

    connect(mapper, SIGNAL(mapped(QString)), window, SLOT(selectLanguage(QString)));
}

void Screen::createScreenMenu(QMenu *menu, MainWindow *window, QSignalMapper *methodMapper, QSignalMapper *algorithmMapper,
                              QSignalMapper *captureExtMapper, QSignalMapper *aspectMapper, QSignalMapper *mapper3D,
                              QSignalMapper *anaglyphMapper, QSignalMapper *screenRotDegreeMapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("화면"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_DEC_OPAQUE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_DEC_OPAQUE));
    connect(subAction, SIGNAL(triggered()), window, SLOT(decOpaque()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_INC_OPAQUE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_INC_OPAQUE));
    connect(subAction, SIGNAL(triggered()), window, SLOT(incOpaque()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_MIN_OPAQUE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_MIN_OPAQUE));
    connect(subAction, SIGNAL(triggered()), window, SLOT(minOpaque()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_MAX_OPAQUE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_MAX_OPAQUE));
    connect(subAction, SIGNAL(triggered()), window, SLOT(maxOpaque()));

    subMenu->addSeparator();

    QList<QKeySequence> fullScreenShortcuts;

    fullScreenShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_FULL_SCREEN_RETURN));
    fullScreenShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_FULL_SCREEN_ENTER));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_FULL_SCREEN_RETURN));
    subAction->setShortcuts(fullScreenShortcuts);
    subAction->setCheckable(true);
    subAction->setChecked(window->isFullScreen());
    connect(subAction, SIGNAL(triggered()), window, SLOT(fullScreen()));

    this->createScreenRatioMenu(subMenu, window);
    this->createUserAspectRatioMenu(subMenu, window, aspectMapper);
    this->createScreenRotationDegreeMenu(subMenu, window, screenRotDegreeMapper);

    subMenu->addSeparator();

    this->createVideoAttributeMenu(subMenu, window);
    this->createVideoEffectMenu(subMenu, window);

    subMenu->addSeparator();

    this->createCaptureMenu(subMenu, window, captureExtMapper);

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_OPEN_SHADER_COMPOSITER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_OPEN_SHADER_COMPOSITER));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    connect(subAction, SIGNAL(triggered()), window, SLOT(openShaderCompositer()));

    this->createDeinterlaceMenu(subMenu, window, methodMapper, algorithmMapper);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USE_HW_DECODER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USE_HW_DECODER));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUseHWDecoder());
    connect(subAction, SIGNAL(triggered()), window, SLOT(useHWDecoder()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USE_PBO));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USE_PBO));
    subAction->setEnabled(this->m_player->isUsablePBO());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUsePBO());
    connect(subAction, SIGNAL(triggered()), window, SLOT(usePBO()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USE_VSYNC));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USE_VSYNC));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_useVSync);
    connect(subAction, SIGNAL(triggered()), window, SLOT(useVSync()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USE_FRAME_DROP));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USE_FRAME_DROP));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUseFrameDrop());
    connect(subAction, SIGNAL(triggered()), window, SLOT(useFrameDrop()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SHOW_ALBUM_JACKET));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SHOW_ALBUM_JACKET));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isShowAlbumJacket());
    connect(subAction, SIGNAL(triggered()), window, SLOT(showAlbumJacket()));

    this->create3DMenu(subMenu, window, mapper3D, anaglyphMapper);
}

void Screen::createPlayOrderMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    AnyVODEnums::PlayingMethod method = window->getPlayingMethod();

    subMenu = menu->addMenu(trUtf8("재생 순서"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_PLAY_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_PLAY_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(playOrder()));

    subAction = subMenu->addAction(trUtf8("전체 순차 재생"));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::PM_TOTAL);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::PM_TOTAL);

    subAction = subMenu->addAction(trUtf8("전체 반복 재생"));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::PM_TOTAL_REPEAT);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::PM_TOTAL_REPEAT);

    subAction = subMenu->addAction(trUtf8("한 개 재생"));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::PM_SINGLE);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::PM_SINGLE);

    subAction = subMenu->addAction(trUtf8("한 개 반복 재생"));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::PM_SINGLE_REPEAT);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::PM_SINGLE_REPEAT);

    subAction = subMenu->addAction(trUtf8("무작위 재생"));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::PM_RANDOM);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::PM_RANDOM);

    connect(mapper, SIGNAL(mapped(int)), window, SLOT(selectPlayingMethod(int)));
}

void Screen::createSkipRange(QMenu *menu, MainWindow *window)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("재생 스킵"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SKIP_OPENING));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SKIP_OPENING));
    subAction->setEnabled(this->m_player->hasDuration());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->getSkipOpening());
    connect(subAction, SIGNAL(triggered()), window, SLOT(skipOpening()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SKIP_ENDING));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SKIP_ENDING));
    subAction->setEnabled(this->m_player->hasDuration());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->getSkipEnding());
    connect(subAction, SIGNAL(triggered()), window, SLOT(skipEnding()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USE_SKIP_RANGE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USE_SKIP_RANGE));
    subAction->setEnabled(this->m_player->hasDuration());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->getUseSkipRange());
    connect(subAction, SIGNAL(triggered()), window, SLOT(useSkipRange()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SKIP_RANGE_SETTING));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SKIP_RANGE_SETTING));
    connect(subAction, SIGNAL(triggered()), window, SLOT(openSkipRange()));
}

void Screen::createRepeatRange(QMenu *menu, MainWindow *window)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("구간 반복"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REPEAT_RANGE_START));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REPEAT_RANGE_START));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(repeatRangeStart()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REPEAT_RANGE_END));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REPEAT_RANGE_END));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(repeatRangeEnd()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REPEAT_RANGE_START_BACK_100MS));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REPEAT_RANGE_START_BACK_100MS));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(repeatRangeStartBack100MS()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REPEAT_RANGE_START_FORW_100MS));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REPEAT_RANGE_START_FORW_100MS));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(repeatRangeStartForw100MS()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REPEAT_RANGE_END_BACK_100MS));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REPEAT_RANGE_END_BACK_100MS));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(repeatRangeEndBack100MS()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REPEAT_RANGE_END_FORW_100MS));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REPEAT_RANGE_END_FORW_100MS));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(repeatRangeEndForw100MS()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REPEAT_RANGE_BACK_100MS));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REPEAT_RANGE_BACK_100MS));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(repeatRangeBack100MS()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REPEAT_RANGE_FORW_100MS));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REPEAT_RANGE_FORW_100MS));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(repeatRangeForw100MS()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REPEAT_RANGE_ENABLE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REPEAT_RANGE_ENABLE));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->getRepeatEnable());
    connect(subAction, SIGNAL(triggered()), window, SLOT(repeatRangeEnable()));
}

void Screen::createPlaybackMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("재생"));

    QList<QKeySequence> toggleShortcuts;

    toggleShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_TOGGLE));
    toggleShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_TOGGLE_PLAY_MEDIA));
    toggleShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_TOGGLE_PAUSE_MEDIA));
    toggleShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_TOGGLE_PLAY_PAUSE_MEDIA));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_TOGGLE));
    subAction->setShortcuts(toggleShortcuts);
    subAction->setEnabled(!Utils::determinDevice(this->m_player->getFilePath()));
    connect(subAction, SIGNAL(triggered()), window, SLOT(toggle()));

    QList<QKeySequence> stopShortcuts;

    stopShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_STOP));
    stopShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_STOP_MEDIA));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_STOP));
    subAction->setShortcuts(stopShortcuts);
    subAction->setEnabled(this->m_player->isPlayOrPause());
    connect(subAction, SIGNAL(triggered()), window, SLOT(stop()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_LAST_PLAY));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_LAST_PLAY));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isGotoLastPos());
    connect(subAction, SIGNAL(triggered()), window, SLOT(lastPlay()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USE_BUFFERING_MODE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USE_BUFFERING_MODE));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUseBufferingMode());
    connect(subAction, SIGNAL(triggered()), window, SLOT(useBufferingMode()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SEEK_KEYFRAME));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SEEK_KEYFRAME));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isSeekKeyFrame());
    connect(subAction, SIGNAL(triggered()), window, SLOT(seekKeyFrame()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_PREV_FRAME));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_PREV_FRAME));
    subAction->setEnabled(this->m_player->canMoveFrame());
    connect(subAction, SIGNAL(triggered()), window, SLOT(prevFrame()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_NEXT_FRAME));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_NEXT_FRAME));
    subAction->setEnabled(this->m_player->canMoveFrame());
    connect(subAction, SIGNAL(triggered()), window, SLOT(nextFrame()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REWIND_5));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REWIND_5));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(rewind5()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_FORWARD_5));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_FORWARD_5));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(forward5()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REWIND_30));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REWIND_30));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(rewind30()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_FORWARD_30));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_FORWARD_30));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(forward30()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_REWIND_60));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_REWIND_60));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(rewind60()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_FORWARD_60));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_FORWARD_60));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(forward60()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_GOTO_BEGIN));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_GOTO_BEGIN));
    subAction->setEnabled(this->m_player->hasDuration());
    connect(subAction, SIGNAL(triggered()), window, SLOT(gotoBegin()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SCREEN_EXPLORER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SCREEN_EXPLORER));
    subAction->setEnabled(this->m_player->hasDuration() && !this->m_player->isAudio() && !this->m_player->isRemoteFile());
    connect(subAction, SIGNAL(triggered()), window, SLOT(openScreenExplorer()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_PREV_CHAPTER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_PREV_CHAPTER));
    subAction->setEnabled(this->m_player->canMoveChapter());
    connect(subAction, SIGNAL(triggered()), window, SLOT(prevChapter()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_NEXT_CHAPTER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_NEXT_CHAPTER));
    subAction->setEnabled(this->m_player->canMoveChapter());
    connect(subAction, SIGNAL(triggered()), window, SLOT(nextChapter()));

    subMenu->addSeparator();

    QList<QKeySequence> prevShortcuts;

    prevShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_PREV));
    prevShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_PREV_MEDIA));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_PREV));
    subAction->setShortcuts(prevShortcuts);
    subAction->setEnabled(window->isPrevEnabled());
    connect(subAction, SIGNAL(triggered()), window, SLOT(prev()));

    QList<QKeySequence> nextShortcuts;

    nextShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_NEXT));
    nextShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_NEXT_MEDIA));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_NEXT));
    subAction->setShortcuts(nextShortcuts);
    subAction->setEnabled(window->isNextEnabled());
    connect(subAction, SIGNAL(triggered()), window, SLOT(next()));

    subMenu->addSeparator();

    this->createRepeatRange(subMenu, window);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_NORMAL_PLAYBACK));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_NORMAL_PLAYBACK));
    subAction->setEnabled(this->m_player->isTempoUsable());
    connect(subAction, SIGNAL(triggered()), window, SLOT(normalPlayback()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SLOWER_PLAYBACK));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SLOWER_PLAYBACK));
    subAction->setEnabled(this->m_player->isTempoUsable());
    connect(subAction, SIGNAL(triggered()), window, SLOT(slowerPlayback()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_FASTER_PLAYBACK));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_FASTER_PLAYBACK));
    subAction->setEnabled(this->m_player->isTempoUsable());
    connect(subAction, SIGNAL(triggered()), window, SLOT(fasterPlayback()));

    subMenu->addSeparator();

    this->createPlayOrderMenu(subMenu, window, mapper);
    this->createSkipRange(subMenu, window);
}

void Screen::createAlignMenu(QMenu *menu, MainWindow *window, QSignalMapper *halignMapper, QSignalMapper *valignMapper)
{
    QAction *subAction = NULL;
    QMenu *subMenu = NULL;
    AnyVODEnums::HAlignMethod halign = this->m_player->getHAlign();
    AnyVODEnums::VAlignMethod valign = this->m_player->getVAlign();

    subMenu = menu->addMenu(trUtf8("정렬"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER));
    subAction->setEnabled(this->m_player->isAlignable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(subtitleHAlignOrder()));

    subAction = subMenu->addAction(trUtf8("기본 정렬"));
    subAction->setEnabled(this->m_player->isAlignable());
    subAction->setCheckable(true);
    subAction->setChecked(halign == AnyVODEnums::HAM_NONE);
    connect(subAction, SIGNAL(triggered()), halignMapper, SLOT(map()));
    halignMapper->setMapping(subAction, AnyVODEnums::HAM_NONE);

    subAction = subMenu->addAction(trUtf8("자동 정렬"));
    subAction->setEnabled(this->m_player->isAlignable());
    subAction->setCheckable(true);
    subAction->setChecked(halign == AnyVODEnums::HAM_AUTO);
    connect(subAction, SIGNAL(triggered()), halignMapper, SLOT(map()));
    halignMapper->setMapping(subAction, AnyVODEnums::HAM_AUTO);

    subAction = subMenu->addAction(trUtf8("왼쪽 정렬"));
    subAction->setEnabled(this->m_player->isAlignable());
    subAction->setCheckable(true);
    subAction->setChecked(halign == AnyVODEnums::HAM_LEFT);
    connect(subAction, SIGNAL(triggered()), halignMapper, SLOT(map()));
    halignMapper->setMapping(subAction, AnyVODEnums::HAM_LEFT);

    subAction = subMenu->addAction(trUtf8("가운데 정렬"));
    subAction->setEnabled(this->m_player->isAlignable());
    subAction->setCheckable(true);
    subAction->setChecked(halign == AnyVODEnums::HAM_MIDDLE);
    connect(subAction, SIGNAL(triggered()), halignMapper, SLOT(map()));
    halignMapper->setMapping(subAction, AnyVODEnums::HAM_MIDDLE);

    subAction = subMenu->addAction(trUtf8("오른쪽 정렬"));
    subAction->setEnabled(this->m_player->isAlignable());
    subAction->setCheckable(true);
    subAction->setChecked(halign == AnyVODEnums::HAM_RIGHT);
    connect(subAction, SIGNAL(triggered()), halignMapper, SLOT(map()));
    halignMapper->setMapping(subAction, AnyVODEnums::HAM_RIGHT);

    connect(halignMapper, SIGNAL(mapped(int)), window, SLOT(selectSubtitleHAlignMethod(int)));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER));
    subAction->setEnabled(this->m_player->isAlignable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(subtitleVAlignOrder()));

    subAction = subMenu->addAction(trUtf8("기본 정렬"));
    subAction->setEnabled(this->m_player->isAlignable());
    subAction->setCheckable(true);
    subAction->setChecked(valign == AnyVODEnums::VAM_NONE);
    connect(subAction, SIGNAL(triggered()), valignMapper, SLOT(map()));
    valignMapper->setMapping(subAction, AnyVODEnums::VAM_NONE);

    subAction = subMenu->addAction(trUtf8("상단 정렬"));
    subAction->setEnabled(this->m_player->isAlignable());
    subAction->setCheckable(true);
    subAction->setChecked(valign == AnyVODEnums::VAM_TOP);
    connect(subAction, SIGNAL(triggered()), valignMapper, SLOT(map()));
    valignMapper->setMapping(subAction, AnyVODEnums::VAM_TOP);

    subAction = subMenu->addAction(trUtf8("하단 정렬"));
    subAction->setEnabled(this->m_player->isAlignable());
    subAction->setCheckable(true);
    subAction->setChecked(valign == AnyVODEnums::VAM_BOTTOM);
    connect(subAction, SIGNAL(triggered()), valignMapper, SLOT(map()));
    valignMapper->setMapping(subAction, AnyVODEnums::VAM_BOTTOM);

    connect(valignMapper, SIGNAL(mapped(int)), window, SLOT(selectSubtitleVAlignMethod(int)));
}

void Screen::createLanguageMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QAction *subAction = NULL;
    QMenu *subMenu = NULL;
    QStringList subtitleClasses;
    QString currentClassName;

    this->m_player->getSubtitleClasses(&subtitleClasses);
    this->m_player->getCurrentSubtitleClass(&currentClassName);

    subMenu = menu->addMenu(trUtf8("언어"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SUBTITLE_LANGUAGE_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SUBTITLE_LANGUAGE_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(subtitleLanguageOrder()));

    if (subtitleClasses.count() > 0)
    {
        for (int i = 0; i < subtitleClasses.count(); i++)
        {
            QString &className = subtitleClasses[i];

            subAction = subMenu->addAction(subtitleClasses[i]);
            subAction->setCheckable(true);
            subAction->setChecked(currentClassName == className);
            connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));

            mapper->setMapping(subAction, className);
        }

        connect(mapper, SIGNAL(mapped(QString)), window, SLOT(selectSubtitleClass(const QString&)));
    }
    else
    {
       subMenu->setDisabled(true);
    }
}

void Screen::createPositionMenu(QMenu *menu, MainWindow *window)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("위치"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_RESET_SUBTITLE_POSITION));
    subAction->setEnabled(this->m_player->isSubtitleMoveable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_RESET_SUBTITLE_POSITION));
    connect(subAction, SIGNAL(triggered()), window, SLOT(resetSubtitlePosition()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_UP_SUBTITLE_POSITION));
    subAction->setEnabled(this->m_player->isSubtitleMoveable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_UP_SUBTITLE_POSITION));
    connect(subAction, SIGNAL(triggered()), window, SLOT(upSubtitlePosition()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_DOWN_SUBTITLE_POSITION));
    subAction->setEnabled(this->m_player->isSubtitleMoveable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_DOWN_SUBTITLE_POSITION));
    connect(subAction, SIGNAL(triggered()), window, SLOT(downSubtitlePosition()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_LEFT_SUBTITLE_POSITION));
    subAction->setEnabled(this->m_player->isSubtitleMoveable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_LEFT_SUBTITLE_POSITION));
    connect(subAction, SIGNAL(triggered()), window, SLOT(leftSubtitlePosition()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_RIGHT_SUBTITLE_POSITION));
    subAction->setEnabled(this->m_player->isSubtitleMoveable());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_RIGHT_SUBTITLE_POSITION));
    connect(subAction, SIGNAL(triggered()), window, SLOT(rightSubtitlePosition()));
}

void Screen::createSubtitleMenu(QMenu *menu, MainWindow *window, QSignalMapper *halignMapper, QSignalMapper *valignMapper, QSignalMapper *languageMapper,
                                QSignalMapper *subtitle3DMapper)
{
    QAction *subAction = NULL;
    QMenu *subMenu = NULL;

    subMenu = menu->addMenu(trUtf8("자막 / 가사"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SUBTITLE_TOGGLE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SUBTITLE_TOGGLE));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isShowSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(subtitleToggle()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_CLOSE_EXTERNAL_SUBTITLE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_CLOSE_EXTERNAL_SUBTITLE));
    subAction->setEnabled(this->m_player->existExternalSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(closeExternalSubtitle()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_IMPORT_FONTS));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_IMPORT_FONTS));
    connect(subAction, SIGNAL(triggered()), window, SLOT(openImportFonts()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->getSearchSubtitleComplex());
    connect(subAction, SIGNAL(triggered()), window, SLOT(useSearchSubtitleComplex()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SUBTITLE_DIRECTORY));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SUBTITLE_DIRECTORY));
    connect(subAction, SIGNAL(triggered()), window, SLOT(openSubtitleDirectory()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_INC_SUBTITLE_OPAQUE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_INC_SUBTITLE_OPAQUE));
    subAction->setEnabled(this->m_player->existSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(incSubtitleOpaque()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_DEC_SUBTITLE_OPAQUE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_DEC_SUBTITLE_OPAQUE));
    subAction->setEnabled(this->m_player->existSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(decSubtitleOpaque()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_RESET_SUBTITLE_OPAQUE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_RESET_SUBTITLE_OPAQUE));
    subAction->setEnabled(this->m_player->existSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(resetSubtitleOpaque()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_INC_SUBTITLE_SIZE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_INC_SUBTITLE_SIZE));
    subAction->setEnabled(this->m_player->isAlignable());
    connect(subAction, SIGNAL(triggered()), window, SLOT(incSubtitleSize()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_DEC_SUBTITLE_SIZE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_DEC_SUBTITLE_SIZE));
    subAction->setEnabled(this->m_player->isAlignable());
    connect(subAction, SIGNAL(triggered()), window, SLOT(decSubtitleSize()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_RESET_SUBTITLE_SIZE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_RESET_SUBTITLE_SIZE));
    subAction->setEnabled(this->m_player->isAlignable());
    connect(subAction, SIGNAL(triggered()), window, SLOT(resetSubtitleSize()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_PREV_SUBTITLE_SYNC));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_PREV_SUBTITLE_SYNC));
    subAction->setEnabled(this->m_player->existSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(prevSubtitleSync()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_NEXT_SUBTITLE_SYNC));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_NEXT_SUBTITLE_SYNC));
    subAction->setEnabled(this->m_player->existSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(nextSubtitleSync()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_RESET_SUBTITLE_SYNC));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_RESET_SUBTITLE_SYNC));
    subAction->setEnabled(this->m_player->existSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(resetSubtitleSync()));

    subMenu->addSeparator();

    this->createPositionMenu(subMenu, window);
    this->createAlignMenu(subMenu, window, halignMapper, valignMapper);
    this->createLanguageMenu(subMenu, window, languageMapper);

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_TEXT_ENCODING));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_TEXT_ENCODING));
    connect(subAction, SIGNAL(triggered()), window, SLOT(textEncoding()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_ENABLE_SEARCH_SUBTITLE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_ENABLE_SEARCH_SUBTITLE));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isEnableSearchSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(enableSearchSubtitle()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_ENABLE_SEARCH_LYRICS));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_ENABLE_SEARCH_LYRICS));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isEnableSearchLyrics());
    connect(subAction, SIGNAL(triggered()), window, SLOT(enableSearchLyrics()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SAVE_SUBTITLE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SAVE_SUBTITLE));
    subAction->setEnabled(this->m_player->existFileSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(saveSubtitle()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SAVE_AS_SUBTITLE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SAVE_AS_SUBTITLE));
    subAction->setEnabled(this->m_player->existFileSubtitle());
    connect(subAction, SIGNAL(triggered()), window, SLOT(saveAsSubtitle()));

    subMenu->addSeparator();

    this->create3DSubtitleMenu(subMenu, window, subtitle3DMapper);
}

void Screen::createSPDIFEncodingMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    AnyVODEnums::SPDIFEncodingMethod method = this->m_player->getSPDIFEncodingMethod();

    subMenu = menu->addMenu(trUtf8("인코딩 사용"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(useSPDIFEncodingOrder()));

    subAction = subMenu->addAction(trUtf8("사용 안 함"));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::SEM_NONE);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::SEM_NONE);

    subAction = subMenu->addAction("AC3");
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::SEM_AC3);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::SEM_AC3);

    subAction = subMenu->addAction(trUtf8("DTS"));
    subAction->setCheckable(true);
    subAction->setChecked(method == AnyVODEnums::SEM_DTS);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(subAction, AnyVODEnums::SEM_DTS);

    connect(mapper, SIGNAL(mapped(int)), window, SLOT(selectSPDIFEncodingMethod(int)));
}

void Screen::createDTVMenu(QMenu *menu, MainWindow *window)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    DTVReader &reader = this->m_player->getDTVReader();

    subMenu = menu->addMenu(trUtf8("DTV 열기"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_ADD_DTV_CHANNEL_TO_PLAYLIST));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_ADD_DTV_CHANNEL_TO_PLAYLIST));
    subAction->setEnabled(reader.isSupport());
    connect(subAction, SIGNAL(triggered()), window, SLOT(addDTVChannelToPlaylist()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_OPEN_DTV_SCAN_CHANNEL));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_OPEN_DTV_SCAN_CHANNEL));
    subAction->setEnabled(reader.isSupport());
    connect(subAction, SIGNAL(triggered()), window, SLOT(openScanDTVChannel()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_VIEW_EPG));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_VIEW_EPG));
    subAction->setEnabled(reader.isSupport() && reader.isOpened());
    connect(subAction, SIGNAL(triggered()), window, SLOT(viewEPG()));
}

void Screen::createSPDIFAuidoDeviceMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    QStringList audioDevices;
    int curDevice;

    this->m_player->getSPDIFAudioDevices(&audioDevices);
    curDevice = this->m_player->getCurrentSPDIFAudioDevice();

    subMenu = menu->addMenu(trUtf8("소리 출력 장치"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(audioSPDIFDeviceOrder()));

    subAction = subMenu->addAction(trUtf8("기본 장치"));
    subAction->setCheckable(true);
    subAction->setChecked(curDevice == -1);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));

    mapper->setMapping(subAction, -1);

    for (int i = 0; i < audioDevices.count(); i++)
    {
        subAction = subMenu->addAction(audioDevices[i]);
        subAction->setCheckable(true);
        subAction->setChecked(curDevice == i);
        connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));

        mapper->setMapping(subAction, i);
    }

    connect(mapper, SIGNAL(mapped(int)), window, SLOT(selectSPDIFAudioDevice(int)));
}

void Screen::createSPDIFMenu(QMenu *menu, MainWindow *window, QSignalMapper *spdifMapper, QSignalMapper *spdifDeviceMapper, QSignalMapper *spdifEncodingMapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    MainWindow::UserSPDIFSampleRateList list;

    window->getUserSPDIFSampleRateList(&list);

    subMenu = menu->addMenu(trUtf8("S/PDIF 설정"));
    subMenu->setEnabled(this->m_player->isSPDIFAvailable());

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_USE_SPDIF));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_USE_SPDIF));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUseSPDIF());
    connect(subAction, SIGNAL(triggered()), window, SLOT(useSPDIF()));

    this->createSPDIFEncodingMenu(subMenu, window, spdifEncodingMapper);
    this->createSPDIFAuidoDeviceMenu(subMenu, window, spdifDeviceMapper);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SPDIF_USER_SAMPLE_RATE_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SPDIF_USER_SAMPLE_RATE_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(userSPDIFSampleRateOrder()));

    for (int i = 0; i < list.list.count(); i++)
    {
        int item = list.list[i];
        QString title;

        if (item == 0)
            title = trUtf8("기본 속도");
        else
            title = QString("%1kHz").arg(item / 1000.0f, 0, 'f', 1);

        subAction = subMenu->addAction(title);
        subAction->setCheckable(true);
        subAction->setChecked(i == list.curIndex);
        connect(subAction, SIGNAL(triggered()), spdifMapper, SLOT(map()));

        spdifMapper->setMapping(subAction, i);
    }

    connect(spdifMapper, SIGNAL(mapped(int)), window, SLOT(selectUserSPDIFSampleRate(int)));
}

void Screen::createAudioEffectMenu(QMenu *menu, MainWindow *window)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("소리 효과"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_LOWER_VOICE));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUsingLowerVoice());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_LOWER_VOICE));
    subAction->setEnabled(!this->m_player->isUseSPDIF());
    connect(subAction, SIGNAL(triggered()), window, SLOT(lowerVoice()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_HIGHER_VOICE));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUsingHigherVoice());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_HIGHER_VOICE));
    subAction->setEnabled(!this->m_player->isUseSPDIF());
    connect(subAction, SIGNAL(triggered()), window, SLOT(higherVoice()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_LOWER_MUSIC));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUsingLowerMusic());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_LOWER_MUSIC));
    subAction->setEnabled(!this->m_player->isUseSPDIF());
    connect(subAction, SIGNAL(triggered()), window, SLOT(lowerMusic()));
}

void Screen::createAudioMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("음성"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_AUDIO_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_AUDIO_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(audioOrder()));

    QVector<AudioStreamInfo> audioInfo;
    int curAudioIndex = this->m_player->getCurrentAudioStreamIndex();

    this->m_player->getAudioStreamInfo(&audioInfo);

    if (audioInfo.count() > 0)
    {
        for (int i = 0; i < audioInfo.count(); i++)
        {
            AudioStreamInfo &info = audioInfo[i];

            subAction = subMenu->addAction(audioInfo[i].name);
            subAction->setCheckable(true);
            subAction->setChecked(curAudioIndex == info.index);
            connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));

            mapper->setMapping(subAction, info.index);
        }

        connect(mapper, SIGNAL(mapped(int)), window, SLOT(selectAudioStream(int)));
    }
    else
    {
       subMenu->setDisabled(true);
    }
}

void Screen::createAuidoDeviceMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;
    QStringList audioDevices;
    int curDevice;

    this->m_player->getAudioDevices(&audioDevices);
    curDevice = this->m_player->getCurrentAudioDevice();

    subMenu = menu->addMenu(trUtf8("소리 출력 장치"));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_AUDIO_DEVICE_ORDER));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_AUDIO_DEVICE_ORDER));
    connect(subAction, SIGNAL(triggered()), window, SLOT(audioDeviceOrder()));

    subAction = subMenu->addAction(trUtf8("기본 장치"));
    subAction->setCheckable(true);
    subAction->setChecked(curDevice == -1);
    connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));

    mapper->setMapping(subAction, -1);

    for (int i = 0; i < audioDevices.count(); i++)
    {
        subAction = subMenu->addAction(audioDevices[i]);
        subAction->setCheckable(true);
        subAction->setChecked(curDevice == i);
        connect(subAction, SIGNAL(triggered()), mapper, SLOT(map()));

        mapper->setMapping(subAction, i);
    }

    connect(mapper, SIGNAL(mapped(int)), window, SLOT(selectAudioDevice(int)));
}

void Screen::createSoundMenu(QMenu *menu, MainWindow *window, QSignalMapper *audioMapper,
                             QSignalMapper *deviceMapper, QSignalMapper *spdifMapper,
                             QSignalMapper *spdifDeviceMapper, QSignalMapper *spdifEncodingMapper)
{
    QMenu *subMenu = NULL;
    QAction *subAction = NULL;

    subMenu = menu->addMenu(trUtf8("소리"));

    this->createAuidoDeviceMenu(subMenu, window, deviceMapper);
    this->createSPDIFMenu(subMenu, window, spdifMapper, spdifDeviceMapper, spdifEncodingMapper);

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_AUDIO_NORMALIZE));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUsingNormalizer());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_AUDIO_NORMALIZE));
    subAction->setEnabled(!this->m_player->isUseSPDIF());
    connect(subAction, SIGNAL(triggered()), window, SLOT(audioNormalize()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_AUDIO_EQUALIZER));
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isUsingEqualizer());
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_AUDIO_EQUALIZER));
    subAction->setEnabled(!this->m_player->isUseSPDIF());
    connect(subAction, SIGNAL(triggered()), window, SLOT(audioEqualizer()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_EQUALIZER_SETTING));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_EQUALIZER_SETTING));
    subAction->setEnabled(!this->m_player->isUseSPDIF());
    connect(subAction, SIGNAL(triggered()), window, SLOT(equalizerSetting()));

    subMenu->addSeparator();

    QList<QKeySequence> volUpShortcuts;

    volUpShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_VOLUME_UP));
    volUpShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_VOLUME_UP_MEDIA));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_VOLUME_UP));
    subAction->setShortcuts(volUpShortcuts);
    subAction->setEnabled(!this->m_player->isUseSPDIF());
    connect(subAction, SIGNAL(triggered()), window, SLOT(volumeUp()));

    QList<QKeySequence> volDownShortcuts;

    volDownShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_VOLUME_DOWN));
    volDownShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_VOLUME_DOWN_MEDIA));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_VOLUME_DOWN));
    subAction->setShortcuts(volDownShortcuts);
    subAction->setEnabled(!this->m_player->isUseSPDIF());
    connect(subAction, SIGNAL(triggered()), window, SLOT(volumeDown()));

    QList<QKeySequence> muteShortcuts;

    muteShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_MUTE));
    muteShortcuts.append(window->getShortcutKey(AnyVODEnums::SK_MUTE_MEDIA));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_MUTE));
    subAction->setShortcuts(muteShortcuts);
    subAction->setCheckable(true);
    subAction->setChecked(window->isMute());
    subAction->setEnabled(!this->m_player->isUseSPDIF());
    connect(subAction, SIGNAL(triggered()), window, SLOT(mute()));

    subMenu->addSeparator();

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_PREV_AUDIO_SYNC));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_PREV_AUDIO_SYNC));
    connect(subAction, SIGNAL(triggered()), window, SLOT(prevAudioSync()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_NEXT_AUDIO_SYNC));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_NEXT_AUDIO_SYNC));
    connect(subAction, SIGNAL(triggered()), window, SLOT(nextAudioSync()));

    subAction = subMenu->addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_RESET_AUDIO_SYNC));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_RESET_AUDIO_SYNC));
    connect(subAction, SIGNAL(triggered()), window, SLOT(resetAudioSync()));

    subMenu->addSeparator();

    this->createAudioMenu(subMenu, window, audioMapper);
    this->createAudioEffectMenu(subMenu, window);
}

void Screen::showContextMenu()
{
    QMenu menu;
    MainWindow *window = Utils::getMainWindow();
    bool mostTop = window->isMostTop();
    QAction *subAction = NULL;

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_MOST_TOP));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_MOST_TOP));
    subAction->setCheckable(true);
    subAction->setChecked(mostTop);
    connect(subAction, SIGNAL(triggered()), window, SLOT(mostTop()));

    menu.addSeparator();

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_LOGIN));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_LOGIN));
    subAction->setEnabled(!window->isLogined());
    connect(subAction, SIGNAL(triggered()), window, SLOT(login()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_OPEN_REMOTE_FILE_LIST));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_OPEN_REMOTE_FILE_LIST));
    subAction->setCheckable(true);
    subAction->setChecked(window->getRemoteFileListWindow().isVisible());
    subAction->setEnabled(window->isLogined());
    connect(subAction, SIGNAL(triggered()), window, SLOT(openRemoteFileList()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_LOGOUT));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_LOGOUT));
    subAction->setEnabled(window->isLogined());
    connect(subAction, SIGNAL(triggered()), window, SLOT(logout()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SERVER_SETTING));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SERVER_SETTING));
    connect(subAction, SIGNAL(triggered()), window, SLOT(serverSetting()));

    menu.addSeparator();

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_OPEN));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_OPEN));
    connect(subAction, SIGNAL(triggered()), window, SLOT(open()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_OPEN_EXTERNAL));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_OPEN_EXTERNAL));
    connect(subAction, SIGNAL(triggered()), window, SLOT(openExternal()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_OPEN_DEVICE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_OPEN_DEVICE));
    connect(subAction, SIGNAL(triggered()), window, SLOT(openDevice()));

    this->createDTVMenu(&menu, window);

    menu.addSeparator();

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_ADD_TO_PLATLIST));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_ADD_TO_PLATLIST));
    connect(subAction, SIGNAL(triggered()), window, SLOT(addToPlayList()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_OPEN_PLAY_LIST));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_OPEN_PLAY_LIST));
    subAction->setCheckable(true);
    subAction->setChecked(window->getPlayListWindow().isVisible());
    connect(subAction, SIGNAL(triggered()), window, SLOT(openPlayList()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_CLOSE));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_CLOSE));
    subAction->setEnabled(this->m_player->isOpened());
    connect(subAction, SIGNAL(triggered()), window, SLOT(close()));

    menu.addSeparator();

    QSignalMapper deinterlacerMethodMapper;
    QSignalMapper deinterlacerAlgorithmMapper;
    QSignalMapper captureExtMapper;
    QSignalMapper playOrderMapper;
    QSignalMapper alignMapper;
    QSignalMapper valignMapper;
    QSignalMapper languageMapper;
    QSignalMapper audioMapper;
    QSignalMapper audioDeviceMapper;
    QSignalMapper aspectMapper;
    QSignalMapper spdifMapper;
    QSignalMapper spdifDeviceMapper;
    QSignalMapper spdifEncodingMapper;
    QSignalMapper mapper3D;
    QSignalMapper anaglyphMapper;
    QSignalMapper subtitle3DMapper;
    QSignalMapper screenRotDegreeMapper;
    QSignalMapper defaultLangMapper;

    this->createScreenMenu(&menu, window, &deinterlacerMethodMapper, &deinterlacerAlgorithmMapper, &captureExtMapper, &aspectMapper, &mapper3D, &anaglyphMapper, &screenRotDegreeMapper);
    this->createPlaybackMenu(&menu, window, &playOrderMapper);
    this->createSubtitleMenu(&menu, window, &alignMapper, &valignMapper, &languageMapper, &subtitle3DMapper);
    this->createSoundMenu(&menu, window, &audioMapper, &audioDeviceMapper, &spdifMapper, &spdifDeviceMapper, &spdifEncodingMapper);

    menu.addSeparator();

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_INFO));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_INFO));
    connect(subAction, SIGNAL(triggered()), window, SLOT(info()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_DETAIL));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_DETAIL));
    subAction->setEnabled(this->m_player->isEnabledVideo());
    subAction->setCheckable(true);
    subAction->setChecked(this->m_player->isShowDetail());
    connect(subAction, SIGNAL(triggered()), window, SLOT(detail()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_SHOW_CONTROL_BAR));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_SHOW_CONTROL_BAR));
    subAction->setEnabled(window->canShowControlBar());
    subAction->setCheckable(true);
    subAction->setChecked(window->getControlBar().isVisible());
    connect(subAction, SIGNAL(triggered()), window, SLOT(showControlBar()));

    this->createDefaultLanguageMenu(&menu, window, &defaultLangMapper);

    menu.addSeparator();

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_OPEN_CUSTOM_SHORTCUTS));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_OPEN_CUSTOM_SHORTCUTS));
    connect(subAction, SIGNAL(triggered()), window, SLOT(customShortcut()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_FILE_ASSOCIATION));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_FILE_ASSOCIATION));
    connect(subAction, SIGNAL(triggered()), window, SLOT(fileAssociation()));

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_CLEAR_PLAYLIST));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_CLEAR_PLAYLIST));
    subAction->setCheckable(true);
    subAction->setChecked(window->isClearPlayList());
    connect(subAction, SIGNAL(triggered()), window, SLOT(clearPlayList()));

    menu.addSeparator();

    subAction = menu.addAction(window->getShortcutKeyDesc(AnyVODEnums::SK_EXIT));
    subAction->setShortcut(window->getShortcutKey(AnyVODEnums::SK_EXIT));
    connect(subAction, SIGNAL(triggered()), window, SLOT(exit()));

    this->setDisableHideCusor(true);
    menu.exec(QCursor::pos());
    this->setDisableHideCusor(false);
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "core/Common.h"
#include "video/ShaderCompositer.h"
#include "media/MediaPlayer.h"

#include <QGLWidget>
#include <QEvent>
#include <QTime>

class MainWindow;
class QMenu;
class QSignalMapper;
class QOpenGLFramebufferObject;
class MediaPlayer;

class Screen : public QGLWidget
{
    Q_OBJECT
public:
    static const QEvent::Type PLAYING_EVENT;
    static const QEvent::Type EMPTY_BUFFER_EVENT;
    static const QEvent::Type AUDIO_OPTION_DESC_EVENT;
    static const QEvent::Type AUDIO_SUBTITLE_EVENT;
    static const QEvent::Type ABORT_EVENT;
    static const QEvent::Type NONE_PLAYING_DESC_EVENT;
    static const QEvent::Type STATUS_CHANGE_EVENT;

public:
    class PlayingEvent : public QEvent
    {
    public:
        PlayingEvent() :
            QEvent(PLAYING_EVENT)
        {

        }
    };

    class EmptyBufferEvent : public QEvent
    {
    public:
        EmptyBufferEvent(bool empty) :
            QEvent(EMPTY_BUFFER_EVENT),
            m_empty(empty)
        {

        }

        bool isEmpty() const { return this->m_empty; }

    private:
        bool m_empty;
    };

    class ShowAudioOptionDescEvent : public QEvent
    {
    public:
        ShowAudioOptionDescEvent(const QString &desc, bool show) :
            QEvent(AUDIO_OPTION_DESC_EVENT),
            m_desc(desc),
            m_show(show)
        {

        }

        QString getDesc() const { return this->m_desc; }
        bool getShow() const { return this->m_show; }

    private:
        QString m_desc;
        bool m_show;
    };

    class AudioSubtitleEvent : public QEvent
    {
    public:
        AudioSubtitleEvent(const QVector<Lyrics> &lines) :
            QEvent(AUDIO_SUBTITLE_EVENT),
            m_lines(lines)
        {

        }

        QVector<Lyrics> getLines() const { return this->m_lines; }

    private:
        QVector<Lyrics> m_lines;
    };

    class AbortEvent : public QEvent
    {
    public:
        AbortEvent(int reason) :
            QEvent(ABORT_EVENT),
            m_reason(reason)
        {

        }

        int getReason() const { return this->m_reason; }

    private:
        int m_reason;
    };

    class NonePlayingDescEvent : public QEvent
    {
    public:
        NonePlayingDescEvent(const QString &desc) :
            QEvent(NONE_PLAYING_DESC_EVENT),
            m_desc(desc)
        {

        }

        QString getDesc() const { return this->m_desc; }

    private:
        QString m_desc;
    };

    class StatusChangeEvent : public QEvent
    {
    public:
        StatusChangeEvent(const MediaPlayer::Status status) :
            QEvent(STATUS_CHANGE_EVENT),
            m_status(status)
        {

        }

        MediaPlayer::Status getStatus() const { return this->m_status; }

    private:
        MediaPlayer::Status m_status;
    };

    struct CaptureInfo
    {
        CaptureInfo()
        {
            ext = QString("jpg");
            capture = false;
            captureOrg = false;
            captureCount = 0;
            totalCount = 0;
            quality = -1;
            captureSize = QSize(1, 1);
            paused = false;
        }

        QString savePath;
        QString ext;
        bool capture;
        bool captureOrg;
        int captureCount;
        int totalCount;
        int quality;
        QSize captureSize;
        bool paused;
    };

    Screen(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~Screen();

    void initTextures(bool autoSize);
    MediaPlayer* getPlayer();
    void showContextMenu();

    void setCaptureInfo(const CaptureInfo &captureInfo);
    void setCaptureInfo(const CaptureInfo &captureInfo, bool raw);
    void getCaptureInfo(CaptureInfo *ret) const;

    void startCapture();
    void stopCapture();

    void toggleUseVSync();
    void setUseVSync(bool use);
    bool isUseVSync() const;

    void setCaptureSavePath(const QString &dir);
    QString getCaptureSavePath() const;

    ShaderCompositer& getShader();

    void setDisableHideCusor(bool disable);

signals:
    void screenUpdated();

protected:
    virtual void customEvent(QEvent *event);
    virtual void contextMenuEvent(QContextMenuEvent *);
    virtual void paintGL();
    virtual void timerEvent(QTimerEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    virtual void initializeGL();
    virtual void resizeGL(int width, int height);

private:
    static void playingCallback(void *userData);
    static void statusChangedCallback(void *userData);
    static void emptyBufferCallback(void *userData, bool empty);
    static void showAudioOptionDescCallback(void *userData, const QString &desc, bool show);
    static void audioSubtitleCallback(void *userData, const QVector<Lyrics> &lines);
    static void paintCallback(void *userData);
    static void abortCallback(void *userData, int reason);
    static void nonePlayingDescCallback(void *userData, const QString &desc);

private:
    void genTexture(TextureID type, int size);
    void clearTexture(TextureID type);
    void deleteTexture(TextureID type);

    void genOffScreen(int size);

    void setWaitVisible(bool visible);
    void renderWait();

    void updateMovie(bool visible);

    void capturePrologue(bool captureOrg);
    void captureEpilogue(bool captureOrg, bool captureBound);

    void createDefaultLanguageMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);

    void createScreenMenu(QMenu *menu, MainWindow *window, QSignalMapper *methodMapper, QSignalMapper *algorithmMapper,
                          QSignalMapper *captureExtMapper, QSignalMapper *aspectMapper, QSignalMapper *mapper3D,
                          QSignalMapper *anaglyphMapper, QSignalMapper *screenRotDegreeMapper);
    void createScreenRotationDegreeMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void create3DMenu(QMenu *menu, MainWindow *window, QSignalMapper *typeMapper, QSignalMapper *anaAlgorithmMapper);
    void create3DCheckerBoardMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void create3DPageFlipMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void create3DInterlaceMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void create3DAnaglyphMenu(QMenu *menu, MainWindow *window, QSignalMapper *typeMapper, QSignalMapper *anaAlgorithmMapper);

    void createScreenRatioMenu(QMenu *menu, MainWindow *window);
    void createUserAspectRatioMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void createDeinterlaceMenu(QMenu *menu, MainWindow *window, QSignalMapper *methodMapper, QSignalMapper *algorithmMapper);
    void createCaptureMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void createVideoAttributeMenu(QMenu *menu, MainWindow *window);
    void createVideoEffectMenu(QMenu *menu, MainWindow *window);

    void createPlaybackMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void createPlayOrderMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void createSkipRange(QMenu *menu, MainWindow *window);
    void createRepeatRange(QMenu *menu, MainWindow *window);

    void createSubtitleMenu(QMenu *menu, MainWindow *window, QSignalMapper *halignMapper, QSignalMapper *valignMapper,
                            QSignalMapper *languageMapper, QSignalMapper *subtitle3DMapper);
    void create3DSubtitleMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void createAlignMenu(QMenu *menu, MainWindow *window, QSignalMapper *halignMapper, QSignalMapper *valignMapper);
    void createLanguageMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void createPositionMenu(QMenu *menu, MainWindow *window);

    void createSoundMenu(QMenu *menu, MainWindow *window, QSignalMapper *audioMapper, QSignalMapper *deviceMapper, QSignalMapper *spdifMapper, QSignalMapper *spdifDeviceMapper, QSignalMapper *spdifEncodingMapper);
    void createAuidoDeviceMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void createAudioMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void createAudioEffectMenu(QMenu *menu, MainWindow *window);
    void createSPDIFMenu(QMenu *menu, MainWindow *window, QSignalMapper *spdifMapper, QSignalMapper *spdifDeviceMapper, QSignalMapper *spdifEncodingMapper);
    void createSPDIFAuidoDeviceMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);
    void createSPDIFEncodingMenu(QMenu *menu, MainWindow *window, QSignalMapper *mapper);

    void createDTVMenu(QMenu *menu, MainWindow *window);

private:
    static const int CURSOR_TIME;

private:
    MediaPlayer *m_player;
    int m_cursorTimer;
    QTime m_waitTimer;
    int m_waitCounter;
    bool m_visibleWait;
    bool m_disableHideCursor;
    bool m_useVSync;
    bool m_render;
    QOpenGLFramebufferObject *m_offScreen;
    ShaderCompositer m_shader;
    TextureInfo m_texInfo[TEX_COUNT];
    CaptureInfo m_captureInfo;
};

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "PlayListUpdater.h"
#include "core/Common.h"
#include "net/YouTubeURLPicker.h"

#include <QDialog>
#include <QList>
#include <QVariant>
#include <QUuid>
#include <QShortcut>
#include <QItemSelectionModel>
#include <QEvent>
#include <QMutex>

class QListWidgetItem;
class QFileInfo;

namespace Ui
{
    class PlayList;
}

class PlayList : public QDialog
{
    Q_OBJECT
public:
    static const QEvent::Type UPDATE_ITEM_EVENT;
    static const QEvent::Type UPDATE_PARENT_EVENT;
    static const QEvent::Type UPDATE_INC_COUNT;

public:
    class UpdateItemEvent : public QEvent
    {
    public:
        UpdateItemEvent(const QString &title, double totalTime, const QUuid &unique) :
            QEvent(UPDATE_ITEM_EVENT),
            m_title(title),
            m_totalTime(totalTime),
            m_unique(unique)
        {

        }

        QString getTitle() const { return this->m_title; }
        double getTotalTime() const { return this->m_totalTime; }
        QUuid getUnique() const { return this->m_unique; }

    private:
        QString m_title;
        double m_totalTime;
        QUuid m_unique;
    };

    class UpdateParentEvent : public QEvent
    {
    public:
        UpdateParentEvent() :
            QEvent(UPDATE_PARENT_EVENT)
        {

        }
    };

    class UpdateIncCountEvent : public QEvent
    {
    public:
        UpdateIncCountEvent(const QUuid &unique) :
            QEvent(UPDATE_INC_COUNT),
            m_unique(unique)
        {

        }

        QUuid getUnique() const { return this->m_unique; }

    private:
        QUuid m_unique;
    };

public:
    explicit PlayList(QWidget *parent = 0);
    ~PlayList();

    void setPlayList(const QVector<PlayItem> &list);
    bool addPlayList(const QVector<PlayItem> &list);
    void getPlayList(QVector<PlayItem> *ret);
    void clearPlayList();
    void stop();

    int updateYouTubeData(const QUuid &unique, const QVector<YouTubeURLPicker::Item> &items);

    int getCount() const;
    bool exist() const;

    QString getFileName(int index) const;
    PlayItem getPlayItem(int index) const;

    int findIndex(const QUuid &unique) const;

    void selectItemOption(int index, QItemSelectionModel::SelectionFlags option);
    void selectItem(int index);
    void deleteItem(int index);

    void setFixItemSize(bool fix);

private:
    virtual void dragEnterEvent(QDragEnterEvent *event);
    virtual void dropEvent(QDropEvent *event);
    virtual void resizeEvent(QResizeEvent *);
    virtual void contextMenuEvent(QContextMenuEvent *);
    virtual void changeEvent(QEvent *event);
    virtual void customEvent(QEvent *event);
    virtual void timerEvent(QTimerEvent *event);

public slots:
    void deleteSelection();
    void moveToTop();
    void moveToBottom();
    void moveUp();
    void moveDown();
    void viewPath();
    void copyPath();
    void screenExplorer();
    void selectOtherQuality(int index);

private slots:
    void on_list_itemActivated(QListWidgetItem *);

private:
    QListWidgetItem* createItem(QFileInfo &info, PlayItem &playItem) const;
    void moveItems(const QList<QListWidgetItem*> &list, int rowTo, bool dirBottom);

    void disbleUpdates();
    void enableUpdates();

    void updateParentWindow(bool updateTitle);
    void startUpdateThread();
    void setItemToolTip(QListWidgetItem *item, const QString &text, double totalTime) const;
    int findYouTubeIndex(const QString &quality, const QString &mime, const QVector<YouTubeURLPicker::Item> &items) const;

private:
    static const QSize DEFAULT_PLAYLIST_SIZE;
    static const int UPDATE_PLAYLIST_TIME;
    static const int MAX_RETRY_COUNT;

private:
    Ui::PlayList *ui;
    bool m_isInit;
    PlayListUpdater m_updater;
    int m_updateTimerID;
    QString m_lastYouTubeQuality;
    QString m_lastYouTubeMime;
    QMutex m_getPlayListMutex;
    QShortcut m_delShort;
    QShortcut m_moveUpShort;
    QShortcut m_moveDownShort;
    QShortcut m_moveToTopShort;
    QShortcut m_moveToBottomShort;
    QShortcut m_viewPathShort;
    QShortcut m_copyPathShort;
    QShortcut m_screenExplorerShort;
};

Q_DECLARE_METATYPE(PlayItem)
QDataStream& operator << (QDataStream &out, const PlayItem &item);
QDataStream& operator >> (QDataStream &in, PlayItem &item);

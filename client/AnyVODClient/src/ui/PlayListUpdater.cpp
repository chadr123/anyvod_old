﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#ifndef UNICODE
#define UNICODE
#endif

#ifndef _UNICODE
#define _UNICODE
#endif

#include "PlayListUpdater.h"
#include "core/Utils.h"
#include "net/YouTubeURLPicker.h"

#include <MediaInfoDLL/MediaInfoDLL.h>

#include <QVector>
#include <QCoreApplication>
#include <QDebug>

using namespace MediaInfoDLL;

PlayListUpdater::PlayListUpdater(PlayList *parent) :
    m_parent(parent),
    m_stop(false)
{

}

void PlayListUpdater::setStop(bool stop)
{
    this->m_stop = stop;
}

void PlayListUpdater::run()
{
    QVector<PlayItem> list;
    bool updated = false;

    this->m_parent->getPlayList(&list);
    this->m_parent->setFixItemSize(true);

    foreach (const PlayItem &item, list)
    {
        if (this->m_stop)
            break;

        if (item.itemUpdated)
            continue;

        bool itemUpdated = false;
        QString title;
        QString totalTime;

        if (Utils::determinRemoteProtocol(item.path) && !Utils::determinRemoteFile(item.path))
        {
            YouTubeURLPicker picker;
            QVector<YouTubeURLPicker::Item> youtube;

            youtube = picker.pickURL(item.path);

            if (!youtube.isEmpty())
            {
                title = youtube.first().title;
                itemUpdated = updated = true;
            }
        }
        else
        {
            MediaInfo mi;

            if (!Utils::determinDevice(item.path) && mi.Open(item.path.toStdWString()) == 1)
            {
                QString format = QString::fromStdWString(mi.Get(Stream_General, 0, __T("Format"))).trimmed();

                title = QString::fromStdWString(mi.Get(Stream_General, 0, __T("Title"))).trimmed();

                if (format.toLower() == "mpeg audio" && Utils::isLocal8bit(title))
                    title = QString::fromLocal8Bit(title.toLatin1());

                totalTime = QString::fromStdWString(mi.Get(Stream_General, 0, __T("Duration")));

                itemUpdated = updated = true;

                mi.Close();
            }
            else if (Utils::determinDevice(item.path))
            {
                title = item.title;
                itemUpdated = updated = true;
            }
        }

        if (itemUpdated)
            QCoreApplication::postEvent(this->m_parent, new PlayList::UpdateItemEvent(title, totalTime.toDouble() / 1000.0, item.unique));
        else
            QCoreApplication::postEvent(this->m_parent, new PlayList::UpdateIncCountEvent(item.unique));
    }

    this->m_parent->setFixItemSize(false);

    if (updated)
        QCoreApplication::postEvent(this->m_parent, new PlayList::UpdateParentEvent());
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "OpenDTVScanChannel.h"
#include "ui_opendtvscanchannel.h"
#include "ui/MainWindow.h"
#include "media/MediaPlayer.h"
#include "core/Utils.h"

#include <QDebug>

OpenDTVScanChannel::OpenDTVScanChannel(DTVReader &reader, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenDTVScanChannel),
    m_reader(reader),
    m_stop(false)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    QVector<DTVReaderInterface::Info> adapters;

    this->m_reader.getAdapterList(&adapters);

    foreach (const DTVReaderInterface::Info &info, adapters)
        this->ui->adapter->addItem(info.name, info.index);

    for (int i = DTVReaderInterface::ST_NONE + 1; i < DTVReaderInterface::ST_COUNT; i++)
    {
        QString name = this->m_reader.systemTypeToString((DTVReaderInterface::SystemType)i);

        if (!name.isEmpty())
            this->ui->type->addItem(name, i);
    }

    for (int i = QLocale::AnyCountry + 1; i < QLocale::LastCountry; i++)
    {
        QString name = QLocale::countryToString((QLocale::Country)i);

        if (!name.isEmpty())
            this->ui->country->addItem(name, i);
    }

    QLocale::Country country;
    DTVReaderInterface::SystemType type;

    this->m_reader.getChannelCountry(&country, &type);

    this->ui->country->setCurrentText(QLocale::countryToString(country));
    this->ui->type->setCurrentText(this->m_reader.systemTypeToString(type));

    this->updateChannelRange();

    this->ui->currentChannel->setText("0,");
    this->ui->scannedChannels->setText(QString::number(0));

    this->ui->progress->reset();
    this->ui->signalStrength->reset();

    this->initHeader();

    QVector<DTVReader::ChannelInfo> channels;

    this->m_reader.getScannedChannels(&channels);

    foreach (const DTVReader::ChannelInfo &info, channels)
        this->addChannel(info);
}

OpenDTVScanChannel::~OpenDTVScanChannel()
{
    delete ui;
}

void OpenDTVScanChannel::initHeader()
{
    QTreeWidget *channels = this->ui->channels;
    QStringList headers;

    headers.append(trUtf8("채널"));
    headers.append(trUtf8("신호 감도"));
    headers.append(trUtf8("이름"));

    channels->setColumnCount(headers.count());

    channels->setColumnWidth(0, 60);
    channels->setColumnWidth(1, 100);

    channels->setHeaderLabels(headers);
    channels->header()->setSectionsMovable(false);
}

void OpenDTVScanChannel::addChannel(const DTVReader::ChannelInfo &info)
{
    QStringList row;

    row.append(QString::number(info.channel));
    row.append(QString("%1%").arg((int)info.signal));
    row.append(info.name);

    QTreeWidgetItem *item = new QTreeWidgetItem((QTreeWidgetItem*)NULL, row);

    item->setTextAlignment(1, Qt::AlignRight);

    this->ui->channels->addTopLevelItem(item);
    this->m_channels.append(info);
}

bool OpenDTVScanChannel::existScannedChannel(int channel)
{
    foreach (const DTVReader::ChannelInfo &info, this->m_channels)
    {
        if (info.channel == channel)
            return true;
    }

    return false;
}

void OpenDTVScanChannel::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            this->initHeader();

            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void OpenDTVScanChannel::updateChannelRange()
{
    QVector<DTVChannelMap::Channel> channels;
    DTVReaderInterface::SystemType type = (DTVReaderInterface::SystemType)this->ui->type->currentData().toInt();

    if (this->m_reader.setChannelCountry((QLocale::Country)this->ui->country->currentData().toInt(), type))
        this->m_reader.getChannels(&channels);

    if (channels.isEmpty())
    {
        this->ui->start->setMinimum(0);
        this->ui->start->setMaximum(0);

        this->ui->end->setMinimum(0);
        this->ui->end->setMaximum(0);

        this->ui->start->setValue(0);
        this->ui->end->setValue(0);
    }
    else
    {
        int first = channels.first().channel;
        int last = channels.last().channel;

        this->ui->start->setMinimum(first);
        this->ui->start->setMaximum(last);

        this->ui->end->setMinimum(first);
        this->ui->end->setMaximum(last);

        this->ui->start->setValue(first);
        this->ui->end->setValue(last);
    }
}

void OpenDTVScanChannel::on_save_clicked()
{
    this->m_reader.setScannedChannels(this->m_channels);
    this->accept();
}

void OpenDTVScanChannel::on_scan_clicked()
{
    QVector<DTVChannelMap::Channel> channels;
    long adapter = this->ui->adapter->currentData().toInt();
    DTVReaderInterface::SystemType type = (DTVReaderInterface::SystemType)this->ui->type->currentData().toInt();
    QLocale::Country country = (QLocale::Country)this->ui->country->currentData().toInt();

    if (this->m_reader.setChannelCountry(country, type))
        this->m_reader.getChannels(&channels);

    if (channels.isEmpty())
    {
        Utils::informationMessageBox(this, trUtf8("채널 정보가 없습니다."));
        return;
    }

    int start = this->ui->start->value();
    int end = this->ui->end->value();

    if (start > end)
    {
        Utils::informationMessageBox(this, trUtf8("시작 채널이 종료 채널보다 큽니다."));
        return;
    }

    DTVChannelMap::Channel startInfo = channels.first();
    DTVChannelMap::Channel endInfo = channels.last();

    if (start < startInfo.channel || end > endInfo.channel)
    {
        Utils::informationMessageBox(this, trUtf8("채널 범위가 맞지 않습니다."));
        return;
    }

    this->ui->signalStrength->setValue(0);
    this->ui->channels->clear();

    this->ui->currentChannel->setText("0,");
    this->ui->scannedChannels->setText(QString::number(0));

    this->ui->scan->setEnabled(false);
    this->ui->adapter->setEnabled(false);
    this->ui->type->setEnabled(false);
    this->ui->start->setEnabled(false);
    this->ui->end->setEnabled(false);
    this->ui->country->setEnabled(false);
    this->ui->save->setEnabled(false);
    this->ui->stopScan->setEnabled(true);

    this->m_channels.clear();

    long curAdapter;
    DTVReaderInterface::SystemType curType;

    this->m_reader.getAdapter(&curAdapter, &curType);

    if (curAdapter == adapter && curType == type)
        Utils::getMainWindow()->getPlayer()->close();

    int startChannelIndex = 0;
    int endChannelIndex = 0;

    for (int i = 0; i < channels.count(); i++)
    {
        if (channels[i].channel == start)
        {
            startChannelIndex = i;
            break;
        }
    }

    for (int i = channels.count() - 1; i >= 0; i--)
    {
        if (channels[i].channel == end)
        {
            endChannelIndex = i;
            break;
        }
    }

    this->ui->progress->setMinimum(startChannelIndex);
    this->ui->progress->setMaximum(endChannelIndex);
    this->ui->progress->reset();

    for (int i = startChannelIndex; i <= endChannelIndex && i < channels.count(); i++)
    {
        const DTVChannelMap::Channel &channel = channels[i];
        DTVReader::ChannelInfo info;

        qApp->processEvents();
        qApp->processEvents();

        if (!this->existScannedChannel(channel.channel))
        {
            bool ok = this->m_reader.scan(adapter, country, type, channel, i, &info);

            if (ok)
            {
                info.index = i;
                info.country = country;

                this->addChannel(info);
            }

            this->ui->signalStrength->setValue((int)info.signal);

            this->ui->currentChannel->setText(QString("%1,").arg(info.channel));
            this->ui->scannedChannels->setText(QString::number(this->m_channels.count()));
        }

        this->ui->progress->setValue(i);

        if (this->m_stop)
            break;
    }

    this->m_stop = false;

    this->ui->signalStrength->reset();

    this->ui->scan->setEnabled(true);
    this->ui->adapter->setEnabled(true);
    this->ui->type->setEnabled(true);
    this->ui->start->setEnabled(true);
    this->ui->end->setEnabled(true);
    this->ui->country->setEnabled(true);
    this->ui->save->setEnabled(true);
    this->ui->stopScan->setEnabled(false);
}

void OpenDTVScanChannel::on_OpenDTVScanChannel_finished(int result)
{
    (void)result;

    this->m_stop = true;
}

void OpenDTVScanChannel::on_stopScan_clicked()
{
    this->m_stop = true;
}

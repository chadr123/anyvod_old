﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "FileAssociation.h"
#include "ui_fileassociation.h"
#include "MainWindow.h"
#include "core/Utils.h"
#include "core/Settings.h"

#include <QSettings>

FileAssociation::FileAssociation(QSettings &settings, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileAssociation),
    m_settings(settings)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    QString video = this->m_settings.value(SETTING_FILE_ASSOCIATION_VIDEO, MainWindow::MOVIE_EXTS).toString();
    QString audio = this->m_settings.value(SETTING_FILE_ASSOCIATION_AUDIO, MainWindow::AUDIO_EXTS).toString();
    QString subtitle = this->m_settings.value(SETTING_FILE_ASSOCIATION_SUBTITLE, MainWindow::SUBTITLE_EXTS).toString();
    QString playlist = this->m_settings.value(SETTING_FILE_ASSOCIATION_PLAYLIST, MainWindow::PLAYLIST_EXTS).toString();

    this->ui->video->setText(video);
    this->ui->audio->setText(audio);
    this->ui->subtitle->setText(subtitle);
    this->ui->playlist->setText(playlist);

    this->m_orgExts = (video + " " + audio + " " + subtitle + " " + playlist).split(" ", QString::SkipEmptyParts);

    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

FileAssociation::~FileAssociation()
{
    delete ui;
}

void FileAssociation::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

void FileAssociation::on_videoDefault_clicked()
{
    this->ui->video->setText(MainWindow::MOVIE_EXTS);
}

void FileAssociation::on_audioDefault_clicked()
{
    this->ui->audio->setText(MainWindow::AUDIO_EXTS);
}

void FileAssociation::on_subtitleDefault_clicked()
{
    this->ui->subtitle->setText(MainWindow::SUBTITLE_EXTS);
}

void FileAssociation::on_playlistDefault_clicked()
{
    this->ui->playlist->setText(MainWindow::PLAYLIST_EXTS);
}

void FileAssociation::on_apply_clicked()
{
    QString video = this->ui->video->text().trimmed();
    QString audio = this->ui->audio->text().trimmed();
    QString subtitle = this->ui->subtitle->text().trimmed();
    QString playlist = this->ui->playlist->text().trimmed();
    QStringList exts = (video + " " + audio + " " + subtitle + " " + playlist).split(" ", QString::SkipEmptyParts);

    Utils::uninstallFileAssociation(this->m_orgExts);
    Utils::installFileAssociation(exts);

    this->m_orgExts = exts;

    this->m_settings.setValue(SETTING_FILE_ASSOCIATION_VIDEO, video);
    this->m_settings.setValue(SETTING_FILE_ASSOCIATION_AUDIO, audio);
    this->m_settings.setValue(SETTING_FILE_ASSOCIATION_SUBTITLE, subtitle);
    this->m_settings.setValue(SETTING_FILE_ASSOCIATION_PLAYLIST, playlist);
    this->m_settings.sync();

#ifdef Q_OS_WIN
    Utils::informationMessageBox(this, trUtf8("관리자 권한으로 실행하지 않았을 경우 정상적으로 적용 되지 않을 수 있습니다"));
#endif

    this->accept();
}

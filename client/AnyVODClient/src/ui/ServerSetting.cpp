﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "ServerSetting.h"
#include "ui_serversetting.h"

ServerSetting::ServerSetting(const QString &address, uint16_t commandPort, uint16_t streamPort, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ServerSetting),
    m_address(address),
    m_commandPort(commandPort),
    m_streamPort(streamPort)
{
    ui->setupUi(this);

#ifdef Q_OS_MAC
    foreach (QPushButton *btn, this->findChildren<QPushButton*>())
        btn->setAttribute(Qt::WA_LayoutUsesWidgetRect);
#endif

    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->address->setText(this->m_address);
    ui->commandPort->setValue(this->m_commandPort);
    ui->streamPort->setValue(this->m_streamPort);
}

ServerSetting::~ServerSetting()
{
    delete ui;
}

void ServerSetting::on_ok_clicked()
{
    this->m_address = ui->address->text();
    this->m_commandPort = ui->commandPort->value();
    this->m_streamPort = ui->streamPort->value();

    this->accept();
}


void ServerSetting::getAddress(QString *ret) const
{
    *ret = this->m_address;
}

void ServerSetting::getCommandPort(uint16_t *ret) const
{
    *ret = this->m_commandPort;
}

void ServerSetting::getStreamPort(uint16_t *ret) const
{
    *ret = this->m_streamPort;
}

void ServerSetting::changeEvent(QEvent *event)
{
    switch (event->type())
    {
        case QEvent::LanguageChange:
        {
            this->ui->retranslateUi(this);
            event->accept();

            break;
        }
        default:
        {
            break;
        }
    }
}

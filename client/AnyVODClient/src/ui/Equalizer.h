﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <QDialog>
#include <QVariant>

class MediaPlayer;

namespace Ui
{
    class Equalizer;
}

class Equalizer : public QDialog
{
    Q_OBJECT
public:
    struct EqualizerItem
    {
        EqualizerItem()
        {
            gain = 0.0f;
        }

        float gain;
    };

    explicit Equalizer(MediaPlayer *player, QWidget *parent = 0);
    ~Equalizer();

private slots:
    void on_reset_clicked();
    void on_preamp_valueChanged(int value);
    void on_band0_valueChanged(int value);
    void on_band1_valueChanged(int value);
    void on_band2_valueChanged(int value);
    void on_band3_valueChanged(int value);
    void on_band4_valueChanged(int value);
    void on_band5_valueChanged(int value);
    void on_band6_valueChanged(int value);
    void on_band7_valueChanged(int value);
    void on_band8_valueChanged(int value);
    void on_band9_valueChanged(int value);
    void on_preset_clicked();
    void on_del_preset_clicked();
    void on_add_preset_clicked();
    void on_save_preset_clicked();
    void on_useEqualizer_clicked(bool checked);

private:
    virtual void changeEvent(QEvent *event);

private:
    struct Preset
    {
        QString desc;
        QVector<EqualizerItem> gains;
    };

    void initPreset();
    void initDefaultNames() const;

    void parsePreset(QVector<Preset> *ret);

    float valueTodB(int value);
    int dBToValue(float dB);

    void presetMenu(const char *slot);

private slots:
    void selectPreset(int index);
    void deletePreset(int index);
    void savePreset(int index);
    void canceled();
    void savePresetToFile();

private:
    Ui::Equalizer *ui;
    MediaPlayer *m_player;
    QVector<EqualizerItem> m_gains;
    QVector<Preset> m_presets;
    float m_preAmp;
    bool m_prevUseEqualizer;

private:
    static const QString EQUALIZER_FILENAME;
    static const QString EQUALIZER_ITEM_DELIMITER;
};

Q_DECLARE_METATYPE(Equalizer::EqualizerItem)
QDataStream& operator << (QDataStream &out, const Equalizer::EqualizerItem &item);
QDataStream& operator >> (QDataStream &in, Equalizer::EqualizerItem &item);

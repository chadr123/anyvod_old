﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#if !defined Q_OS_MOBILE
#include "ui/PlayList.h"
#endif

#include "core/Common.h"
#include "net/YouTubeURLPicker.h"
#include "device/DTVReader.h"

#include <QMap>
#include <QVector>
#include <QList>
#include <QCoreApplication>

#ifdef Q_OS_WIN
#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#endif

#if defined Q_OS_MAC && !defined Q_OS_IOS
#include <ApplicationServices/ApplicationServices.h>
#endif

extern "C"
{
    #include <libavformat/avformat.h>
}

class QString;
class QStringList;
class QTemporaryFile;

#if !defined Q_OS_MOBILE
class QIcon;
class QWidget;
class QListWidgetItem;
class QListWidget;
class MainWindow;
#endif

class Utils
{
    Q_DECLARE_TR_FUNCTIONS(Utils)
private:
    Utils() {}
    ~Utils() {}

public:
    enum MEDIA_TYPE
    {
        MT_VIDEO,
        MT_AUDIO,
        MT_SUBTITLE,
        MT_CAPTURE_FORMAT,
        MT_PLAYLIST,
        MT_FONT,
    };

    struct FontInfo
    {
        QString family;
        QString path;
    };

public:
    static void release();

    static bool parsePair(const QString &filePath, QMap<QString, QString> *ret);

#if !defined Q_OS_MOBILE
    static void criticalMessageBox(QWidget *parent, const QString &text);
    static void informationMessageBox(QWidget *parent, const QString &text);
    static bool questionMessageBox(QWidget *parent, const QString &text);

    static QList<QListWidgetItem*> sortByRow(const QList<QListWidgetItem*> &list, const QListWidget &widget);
    static bool ApplyCommandPlayList(const QString &path);
    static MainWindow *getMainWindow();

    static QString makeDevicePath(const QString &path);
    static void getExtentionIcon(const QString &ext, QIcon *ret, bool isDir);
#endif

    static void appendDirSeparator(QString *path);
    static QString* getTimeString(double seconds, const QString &format, QString *ret);
    static QString adjustNetworkPath(const QString &path);
    static int mapTo(int org, int clipped, int value);
    static QString removeFFMpegSeparator(const QString &org);
    static void getPlayItemFromFileNames(const QStringList &list, QVector<PlayItem> *ret);
    static void getPlayItemFromYouTube(const QVector<YouTubeURLPicker::Item> &list, bool isPlayList, QVector<PlayItem> *ret);
    static void splitStringByWidth(QString text, int width, int screenWidth, QStringList *ret);

    static bool determinRemoteFile(const QString &filePath);
    static bool determinRemoteProtocol(const QString &filePath);

    static double zeroDouble(double value);
    static QDateTime getTime(double seconds);
    static bool createDirectory(const QString &path);
    static void getLocalFileList(const QString &path, const QStringList &extList, bool ignoreExtList, QStringList *ret);
    static QStringList getOnlyMediaFileList(const QStringList &list);
    static void getLocalFileListOnlyMedia(const QString &path, const QStringList &extList, bool ignoreExtList, QStringList *ret);
    static bool is8bitFormat(AVPixelFormat format);
    static float clampValue(float value, float min, float max);

    static void installFileAssociation(const QStringList &list);
    static void uninstallFileAssociation(const QStringList &list);

    static int findChapter(double pos, const QVector<ChapterInfo> &list);
    static bool isVaildPlayListExt(const QString &ext);
    static bool isExtension(const QString &ext, MEDIA_TYPE type);
    static bool isMediaExtension(const QString &ext);

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
    static QString getDevicePath(const QString &path);
    static QString getDeviceType(const QString &path);
    static bool determinDevice(const QString &path);
    static QString makeDTVPath(const DTVReader::ChannelInfo &info);
#endif

    static bool isPortrait(double degree);

    static QString sizeToString(unsigned long long size);
    static bool isLocal8bit(const QString &text);

    static QString getSettingPath();

    static void setSubtitleCodecName(const QString &name);
    static QString getSubtitleCodecName();

    static void setDefaultFont(const FontInfo &font);
    static FontInfo getDefaultFont();

    static bool isSupported(int key);

    static int decodeFrame(AVCodecContext *ctx, AVPacket *packet, AVFrame *frame, int *decoded);
    static int encodeFrame(AVCodecContext *ctx, AVPacket *packet, AVFrame *frame, int *encoded);

private:
#ifdef Q_OS_WIN
    static void deleteRegKey(HKEY root, const QString &subKey, const QString &toDeleteKey);
#endif

#if defined Q_OS_MAC && !defined Q_OS_IOS
    static void setDefaultExtension(const QStringList &list, CFStringRef bundleID, UInt32 role);
#endif

#ifdef Q_OS_LINUX
    static void getExtentionToMime(const QString &ext, QString *ret);
    static void setFileAssociation(const QStringList &extList, bool erase);
    static void setFileAssociationInternal(const QString &filePath, const QString &header, const QStringList &extList, bool erase);
#endif

private:
    static QMap<QString, QTemporaryFile*> m_cache;
    static QString m_subtitleCodecName;
    static FontInfo m_defaultFont;

private:
    static const QChar WINDOWS_DIR_SEPARATOR;
    static const QChar DIR_SEPARATOR;
    static const QString NETWORK_PATH_PREFIX;
    static const QString PROTOCOL_POSTFIX;

public:
    static const QChar FFMPEG_SEPARATOR;
    static const QString ANYVOD_PROTOCOL;
    static const QString DTV_PROTOCOL;
    static const QString FILE_PROTOCOL;
    static const QString COMMENT_PREFIX;
    static const QString TIME_MM_SS;
    static const QString TIME_MM_SS_Z;
    static const QString TIME_HH_MM_SS;
    static const QString TIME_HH_MM_SS_ZZZ;
    static const QString TIME_HH_MM_SS_ZZZ_COMMA;
    static const QString TIME_HH_MM_SS_ZZZ_DIR;
    static const QTime ZERO_TIME;
};

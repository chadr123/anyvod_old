﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "Common.h"

const char ANYVOD_PROTOCOL_NAME[] = "anyvod";
const char DTV_PROTOCOL_NAME[] = "dtv";
const char FILE_PROTOCOL_NAME[] = "file";
const int SCREEN_TOP_OFFSET = 1;
const int TAB_SIZE = 4;
const QString TABS = QString(" ").repeated(TAB_SIZE);
const int DEFAULT_MAX_TEXTURE_SIZE = 4096;
const int DEFAULT_MIN_TEXTURE_SIZE = 1024;
const double MICRO_SECOND = 1000000.0;
const int SPDIF_MAX_FAIL_COUNT = 10;
const int THRESHOLD_FACTOR = 1024;
const int MAX_AUDIO_QUEUE_SIZE = 45 * 256 * 1024;
const int MAX_VIDEO_QUEUE_SIZE = 30 * 256 * 1024;
const int MAX_SUBTITLE_QUEUE_SIZE = 15 * 16 * 1024;
const double SYNC_THRESHOLD_MULTI = 3.0;
const double SYNC_THRESHOLD = 0.008;
const double NOSYNC_THRESHOLD = 10.0;
const double DOUBLER_START_TIME = 1.0;
const int FRAME_LOW_WARNING_THRESHOLD = 20;
const int SAMPLE_CORRECTION_PERCENT_MAX = 10;
const int AUDIO_DIFF_AVG_NB = 20;
const int PAUSE_REFRESH_DELAY = 200;
const int DEFAULT_REFRESH_DELAY = 40;
const int FIRST_REFRESH_DELAY = 1;
const int READ_CONTINUE_DELAY = 500;
const int NO_AUDIO_ALBUM_JACKET_DELAY = 100;
const int EMPTY_BUFFER_WAIT_DELAY = 10;
#if defined Q_OS_MOBILE
const QString ICON_PATH = ASSETS_PREFIX"/qml/AnyVODMobileClient/assets/big_icon.png";
#else
const QString ICON_PATH = ":/icons/logo.png";
#endif
const int MAX_READ_RETRY_COUNT = 5;
const int SPDIF_ENCODING_TMP_BUFFER_SIZE = 9;
const unsigned long long KILO = 1024ull;
const unsigned long long MEGA = KILO * KILO;
const unsigned long long GIGA = MEGA * KILO;

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "net/YouTubeURLPicker.h"

#include <stdint.h>

#include <QObject>
#include <QColor>
#include <QUuid>
#include <qopengl.h>

extern const char ANYVOD_PROTOCOL_NAME[];
extern const char DTV_PROTOCOL_NAME[];
extern const char FILE_PROTOCOL_NAME[];
extern const int SCREEN_TOP_OFFSET;
extern const int TAB_SIZE;
extern const QString TABS;
extern const int DEFAULT_MAX_TEXTURE_SIZE;
extern const int DEFAULT_MIN_TEXTURE_SIZE;
extern const double MICRO_SECOND;
extern const int SPDIF_MAX_FAIL_COUNT;
extern const int THRESHOLD_FACTOR;
extern const int MAX_AUDIO_QUEUE_SIZE;
extern const int MAX_VIDEO_QUEUE_SIZE;
extern const int MAX_SUBTITLE_QUEUE_SIZE;
extern const double SYNC_THRESHOLD_MULTI;
extern const double SYNC_THRESHOLD;
extern const double NOSYNC_THRESHOLD;
extern const double DOUBLER_START_TIME;
extern const int FRAME_LOW_WARNING_THRESHOLD;
extern const int SAMPLE_CORRECTION_PERCENT_MAX;
extern const int AUDIO_DIFF_AVG_NB;
extern const int PAUSE_REFRESH_DELAY;
extern const int DEFAULT_REFRESH_DELAY;
extern const int FIRST_REFRESH_DELAY;
extern const int READ_CONTINUE_DELAY;
extern const int NO_AUDIO_ALBUM_JACKET_DELAY;
extern const int EMPTY_BUFFER_WAIT_DELAY;
extern const QString ICON_PATH;
extern const int MAX_READ_RETRY_COUNT;
extern const int SPDIF_ENCODING_TMP_BUFFER_SIZE;
extern const unsigned long long KILO;
extern const unsigned long long MEGA;
extern const unsigned long long GIGA;

#ifndef IS_BIT_SET
#define IS_BIT_SET(src, flag) (((src)&(flag))==(flag))
#endif

#ifndef GET_RED_VALUE
#define GET_RED_VALUE(rgb) ((uint8_t)(((rgb)>>24)&0xff))
#endif

#ifndef GET_GREEN_VALUE
#define GET_GREEN_VALUE(rgb) ((uint8_t)(((rgb)>>16)&0xff))
#endif

#ifndef GET_BLUE_VALUE
#define GET_BLUE_VALUE(rgb) ((uint8_t)(((rgb)>>8)&0xff))
#endif

#ifndef GET_ALPHA_VALUE
#define GET_ALPHA_VALUE(rgb) ((uint8_t)((rgb)&0xff))
#endif

#ifndef MAKE_COLOR
#define MAKE_COLOR(r, g, b, a) ((uint32_t)((((r)&0xff)<<24) | (((g)&0xff)<<16) | (((b)&0xff)<<8) | ((a)&0xff)))
#endif

#ifndef GET_PIXEL_SIZE
#define GET_PIXEL_SIZE(alpha) ((alpha) ? 4 : 3)
#endif

#if defined Q_OS_MOBILE
#define GL_PREFIX this->m_gl->
#else
#define GL_PREFIX
#endif

#if defined Q_OS_ANDROID
#define ASSETS_PREFIX "assets:"
#else
#define ASSETS_PREFIX "."
#endif

#if defined Q_OS_RASPBERRY_PI
#define MAX_TEXTURE_COUNT 3
#else
#define MAX_TEXTURE_COUNT 1
#endif

#define MAX_PBO_COUNT 3

#define PICTURE_MAX_PLANE 4

struct ExtraPlayData
{
    ExtraPlayData()
    {
        duration = 0.0;
        totalFrame = 0;
        valid = false;
    }

    bool isValid() const
    {
        return valid;
    }

    double duration;
    uint32_t totalFrame;
    bool valid;
    QByteArray userData;
};

struct PlayItem
{
    PlayItem()
    {
        itemUpdated = false;
        totalTime = 0.0;
        retry = 0;
        youtubeIndex = 0;
    }

    QString path;
    QString title;
    double totalTime;
    ExtraPlayData extraData;
    QUuid unique;
    bool itemUpdated;
    int retry;
    int youtubeIndex;
    QVector<YouTubeURLPicker::Item> youtubeData;
};

struct Lyrics
{
    QColor color;
    QString text;
};

struct TextureInfo
{
    TextureInfo()
    {
        memset(id, 0, sizeof(id));
        memset(idPBO, 0, sizeof(idPBO));
        index = 0;
        indexPBO = 0;
        textureCount = 0;
        maxSize = 0;

        for (int i = 0; i < MAX_TEXTURE_COUNT; i++)
            init[i] = false;
    }

    GLuint id[MAX_TEXTURE_COUNT];
    GLuint idPBO[MAX_PBO_COUNT];
    unsigned int index;
    unsigned int indexPBO;
    unsigned int textureCount;
    int maxSize;
    bool init[MAX_TEXTURE_COUNT];
};

struct ChapterInfo
{
    ChapterInfo()
    {
        start = 0.0;
        end = 0.0;
    }

    double start;
    double end;
    QString desc;
};

struct StreamInfo
{
    StreamInfo()
    {
        index = -1;
    }

    int index;
    QString name;
    QString desc;
};

typedef StreamInfo AudioStreamInfo;
typedef StreamInfo SubtitleStreamInfo;

class AnyVODEnums : public QObject
{
    Q_OBJECT
public:
    AnyVODEnums() {}

    enum PlayingMethod
    {
        PM_TOTAL = 0,
        PM_TOTAL_REPEAT,
        PM_SINGLE,
        PM_SINGLE_REPEAT,
        PM_RANDOM,
        PM_COUNT
    };
    Q_ENUM(PlayingMethod)

    enum DeinterlaceAlgorithm
    {
        DA_BLEND = 0,
        DA_BOB,
        DA_YADIF,
        DA_YADIF_BOB,
        DA_W3FDIF,
        DA_KERNDEINT,
        DA_MCDEINT,
        DA_BWDIF,
        DA_BWDIF_BOB,
        DA_COUNT
    };
    Q_ENUM(DeinterlaceAlgorithm)

    enum DeinterlaceMethod
    {
        DM_AUTO = 0,
        DM_USE,
        DM_NOUSE,
        DM_COUNT
    };
    Q_ENUM(DeinterlaceMethod)

    enum HAlignMethod
    {
        HAM_NONE = 0,
        HAM_AUTO,
        HAM_LEFT,
        HAM_MIDDLE,
        HAM_RIGHT,
        HAM_COUNT
    };
    Q_ENUM(HAlignMethod)

    enum VAlignMethod
    {
        VAM_NONE = 0,
        VAM_TOP,
        VAM_BOTTOM,
        VAM_COUNT
    };
    Q_ENUM(VAlignMethod)

    enum Subtitle3DMethod
    {
        S3M_NONE = 0,
        S3M_LEFT_RIGHT,
        S3M_TOP_BOTTOM,
        S3M_PAGE_FLIP,
        S3M_INTERLACED,
        S3M_ANAGLYPH,
        S3M_CHECKER_BOARD,
        S3M_COUNT
    };
    Q_ENUM(Subtitle3DMethod)

    enum Video3DMethod
    {
        V3M_NONE = 0,
        V3M_HALF_LEFT,
        V3M_HALF_RIGHT,
        V3M_HALF_TOP,
        V3M_HALF_BOTTOM,
        V3M_FULL_LEFT_RIGHT,
        V3M_FULL_TOP_BOTTOM,
        V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR,
        V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR,
        V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR,
        V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR,
        V3M_ROW_LEFT_RIGHT_LEFT_PRIOR,
        V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR,
        V3M_ROW_TOP_BOTTOM_TOP_PRIOR,
        V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR,
        V3M_COL_LEFT_RIGHT_LEFT_PRIOR,
        V3M_COL_LEFT_RIGHT_RIGHT_PRIOR,
        V3M_COL_TOP_BOTTOM_TOP_PRIOR,
        V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR,
        V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR,
        V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR,
        V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR,
        V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR,
        V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR,
        V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR,
        V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR,
        V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR,
        V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR,
        V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR,
        V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR,
        V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR,
        V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR,
        V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR,
        V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR,
        V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR,
        V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR,
        V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR,
        V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR,
        V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR,
        V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR,
        V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR,
        V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR,
        V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR,
        V3M_COUNT
    };
    Q_ENUM(Video3DMethod)

    enum AnaglyphAlgorithm
    {
        AGA_DEFAULT = 0,
        AGA_DUBOIS = AGA_DEFAULT,
        AGA_COLORED,
        AGA_HALF_COLORED,
        AGA_GRAY,
        AGA_COUNT
    };
    Q_ENUM(AnaglyphAlgorithm)

    enum VRInputSource
    {
        VRI_NONE = 0,
        VRI_LEFT_RIGHT,
        VRI_TOP_BOTTOM,
        VRI_COPY,
        VRI_COUNT
    };
    Q_ENUM(VRInputSource)

    enum ScaleDirection
    {
        SD_NONE = 0,
        SD_WIDTH,
        SD_HEIGHT,
        SD_ALL,
        SD_COUNT
    };
    Q_ENUM(ScaleDirection)

    enum SPDIFEncodingMethod
    {
        SEM_NONE = 0,
        SEM_AC3,
        SEM_DTS,
        SEM_COUNT
    };
    Q_ENUM(SPDIFEncodingMethod)

    enum ScreenRotationDegree
    {
        SRD_NONE = 0,
        SRD_90,
        SRD_180,
        SRD_270,
        SRD_COUNT
    };
    Q_ENUM(ScreenRotationDegree)

    enum MenuID
    {
        MID_NONE = 0,
        MID_EQUALIZER,
        MID_SETTINGS,
        MID_OPEN_EXTERNAL,
        MID_SCREEN,
        MID_PLAY,
        MID_SUBTITLE_LYRICS,
        MID_SOUND,
        MID_LANG,
        MID_INFO,
        MID_DELETE,
        MID_VIEW_PATH,
        MID_COPY_PATH,
        MID_OTHER_QUALITY,
        MID_OPEN_DTV,
        MID_SEARCH,
        MID_MANAGE_PLAY_LIST,
        MID_COUNT
    };
    Q_ENUM(MenuID)

    enum ShortcutKey
    {
        SK_LANG = -1,
        SK_LOGIN = 0,
        SK_LOGOUT,
        SK_MOST_TOP,
        SK_OPEN_REMOTE_FILE_LIST,
        SK_OPEN_EXTERNAL,
        SK_OPEN,
        SK_CLOSE,
        SK_EXIT,
        SK_AUDIO_NORMALIZE,
        SK_AUDIO_EQUALIZER,
        SK_EQUALIZER_SETTING,
        SK_OPEN_PLAY_LIST,
        SK_RESET_SUBTITLE_POSITION,
        SK_UP_SUBTITLE_POSITION,
        SK_DOWN_SUBTITLE_POSITION,
        SK_LEFT_SUBTITLE_POSITION,
        SK_RIGHT_SUBTITLE_POSITION,
        SK_TOGGLE,
        SK_FULL_SCREEN_RETURN,
        SK_FULL_SCREEN_ENTER,
        SK_REWIND_5,
        SK_FORWARD_5,
        SK_REWIND_30,
        SK_FORWARD_30,
        SK_REWIND_60,
        SK_FORWARD_60,
        SK_GOTO_BEGIN,
        SK_PREV,
        SK_NEXT,
        SK_PLAY_ORDER,
        SK_SUBTITLE_TOGGLE,
        SK_PREV_SUBTITLE_SYNC,
        SK_NEXT_SUBTITLE_SYNC,
        SK_RESET_SUBTITLE_SYNC,
        SK_SUBTITLE_LANGUAGE_ORDER,
        SK_VOLUME_UP,
        SK_VOLUME_DOWN,
        SK_MUTE,
        SK_AUDIO_ORDER,
        SK_INFO,
        SK_DETAIL,
        SK_SHOW_CONTROL_BAR,
        SK_INC_OPAQUE,
        SK_DEC_OPAQUE,
        SK_MAX_OPAQUE,
        SK_MIN_OPAQUE,
        SK_DEINTERLACE_METHOD_ORDER,
        SK_DEINTERLACE_ALGORITHM_ORDER,
        SK_PREV_AUDIO_SYNC,
        SK_NEXT_AUDIO_SYNC,
        SK_RESET_AUDIO_SYNC,
        SK_SUBTITLE_HALIGN_ORDER,
        SK_REPEAT_RANGE_START,
        SK_REPEAT_RANGE_END,
        SK_REPEAT_RANGE_ENABLE,
        SK_REPEAT_RANGE_START_BACK_100MS,
        SK_REPEAT_RANGE_START_FORW_100MS,
        SK_REPEAT_RANGE_END_BACK_100MS,
        SK_REPEAT_RANGE_END_FORW_100MS,
        SK_REPEAT_RANGE_BACK_100MS,
        SK_REPEAT_RANGE_FORW_100MS,
        SK_SEEK_KEYFRAME,
        SK_SKIP_OPENING,
        SK_SKIP_ENDING,
        SK_USE_SKIP_RANGE,
        SK_SKIP_RANGE_SETTING,
        SK_SELECT_CAPTURE_EXT_ORDER,
        SK_CAPTURE_SINGLE,
        SK_CAPTURE_MULTIPLE,
        SK_PREV_FRAME,
        SK_NEXT_FRAME,
        SK_FASTER_PLAYBACK,
        SK_SLOWER_PLAYBACK,
        SK_NORMAL_PLAYBACK,
        SK_RESET_VIDEO_ATTRIBUTE,
        SK_BRIGHTNESS_DOWN,
        SK_BRIGHTNESS_UP,
        SK_SATURATION_DOWN,
        SK_SATURATION_UP,
        SK_HUE_DOWN,
        SK_HUE_UP,
        SK_CONTRAST_DOWN,
        SK_CONTRAST_UP,
        SK_OPEN_SHADER_COMPOSITER,
        SK_LOWER_MUSIC,
        SK_LOWER_VOICE,
        SK_HIGHER_VOICE,
        SK_SHARPLY,
        SK_SHARPEN,
        SK_SOFTEN,
        SK_LEFT_RIGHT_INVERT,
        SK_TOP_BOTTOM_INVERT,
        SK_ADD_TO_PLATLIST,
        SK_INC_SUBTITLE_OPAQUE,
        SK_DEC_SUBTITLE_OPAQUE,
        SK_RESET_SUBTITLE_OPAQUE,
        SK_OPEN_CAPTURE_DIRECTORY,
        SK_SELECT_CAPTURE_DIRECTORY,
        SK_OPEN_CUSTOM_SHORTCUTS,
        SK_AUDIO_DEVICE_ORDER,
        SK_FILE_ASSOCIATION,
        SK_ENABLE_SEARCH_SUBTITLE,
        SK_ENABLE_SEARCH_LYRICS,
        SK_SUBTITLE_VALIGN_ORDER,
        SK_INC_SUBTITLE_SIZE,
        SK_DEC_SUBTITLE_SIZE,
        SK_RESET_SUBTITLE_SIZE,
        SK_USER_ASPECT_RATIO_ORDER,
        SK_USER_ASPECT_RATIO,
        SK_SCREEN_SIZE_HALF,
        SK_SCREEN_SIZE_NORMAL,
        SK_SCREEN_SIZE_NORMAL_HALF,
        SK_SCREEN_SIZE_DOUBLE,
        SK_USE_HW_DECODER,
        SK_USE_SPDIF,
        SK_SPDIF_USER_SAMPLE_RATE_ORDER,
        SK_USE_PBO,
        SK_3D_VIDEO_METHOD_ORDER,
        SK_USE_VSYNC,
        SK_PREV_CHAPTER,
        SK_NEXT_CHAPTER,
        SK_CLOSE_EXTERNAL_SUBTITLE,
        SK_CLEAR_PLAYLIST,
        SK_SHOW_ALBUM_JACKET,
        SK_LAST_PLAY,
        SK_TEXT_ENCODING,
        SK_SERVER_SETTING,
        SK_SAVE_SUBTITLE,
        SK_SAVE_AS_SUBTITLE,
        SK_SPDIF_AUDIO_DEVICE_ORDER,
        SK_SCREEN_EXPLORER,
        SK_USE_FRAME_DROP,
        SK_USE_SPDIF_ENCODING_ORDER,
        SK_ANAGLYPH_ALGORITHM_ORDER,
        SK_3D_SUBTITLE_METHOD_ORDER,
        SK_USE_3D_FULL,
        SK_RESET_3D_SUBTITLE_OFFSET,
        SK_UP_3D_SUBTITLE_OFFSET,
        SK_DOWN_3D_SUBTITLE_OFFSET,
        SK_LEFT_3D_SUBTITLE_OFFSET,
        SK_RIGHT_3D_SUBTITLE_OFFSET,
        SK_SUBTITLE_DIRECTORY,
        SK_IMPORT_FONTS,
        SK_SCREEN_ROTATION_DEGREE_ORDER,
        SK_OPEN_DEVICE,
        SK_ADD_DTV_CHANNEL_TO_PLAYLIST,
        SK_OPEN_DTV_SCAN_CHANNEL,
        SK_HISTOGRAM_EQ,
        SK_HIGH_QUALITY_3D_DENOISE,
        SK_VIEW_EPG,
        SK_SEARCH_SUBTITLE_COMPLEX,
        SK_DEBAND,
        SK_ATA_DENOISE,
        SK_OW_DENOISE,
        SK_USE_BUFFERING_MODE,
        SK_TOGGLE_PLAY_MEDIA,
        SK_TOGGLE_PAUSE_MEDIA,
        SK_TOGGLE_PLAY_PAUSE_MEDIA,
        SK_PREV_MEDIA,
        SK_NEXT_MEDIA,
        SK_VOLUME_UP_MEDIA,
        SK_VOLUME_DOWN_MEDIA,
        SK_MUTE_MEDIA,
        SK_STOP,
        SK_STOP_MEDIA,
        SK_PLAYBACK_SPEED,
        SK_BRIGHTNESS,
        SK_SATURATION,
        SK_HUE,
        SK_CONTRAST,
        SK_REPEAT_RANGE,
        SK_SUBTITLE_OPAQUE,
        SK_SUBTITLE_SIZE,
        SK_SUBTITLE_SYNC,
        SK_UPDOWN_SUBTITLE_POSITION,
        SK_LEFTRIGHT_SUBTITLE_POSITION,
        SK_UPDOWN_3D_SUBTITLE_OFFSET,
        SK_LEFTRIGHT_3D_SUBTITLE_OFFSET,
        SK_SUBTITLE_CACHE_MODE,
        SK_AUDIO_SYNC,
        SK_OPEN_SUBTITLE,
        SK_FONT,
        SK_OTHER_QUALITY,
        SK_LICENSE,
        SK_DELETE_MEDIA_WITH_SUBTITLE,
        SK_USE_LOW_QUALITY_MODE,
        SK_OPEN_DTV_CHANNEL_LIST,
        SK_VR_INPUT_SOURCE_ORDER,
        SK_VR_HEAD_TRACKING,
        SK_VR_USE_BARREL_DISTORTION,
        SK_VR_COEFFICIENTS,
        SK_VR_LENS_CENTER,
        SK_COUNT
    };
    Q_ENUM(ShortcutKey)
};

enum TextureID
{
    TEX_HEAD = -1,
    TEX_MOVIE_FRAME,
    TEX_FFMPEG_SUBTITLE,
    TEX_ASS_SUBTITLE,
    TEX_YUV_0,
    TEX_YUV_1,
    TEX_YUV_2,
    TEX_COUNT
};

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "AnyVOD.h"
#include "Utils.h"
#include "Settings.h"
#include "BuildNumber.h"
#include "ui/MainWindow.h"
#include "ui/PlayList.h"
#include "ui/Equalizer.h"
#include "media/MediaPresenter.h"
#include "media/LastPlay.h"
#include "device/DTVReader.h"

#include <QCoreApplication>
#include <QGLFormat>
#include <QDir>
#include <QDebug>

int main(int argc, char *argv[])
{
    qRegisterMetaTypeStreamOperators<PlayItem>("PlayItem");
    qRegisterMetaTypeStreamOperators<Equalizer::EqualizerItem>("Equalizer::EqualizerItem");
    qRegisterMetaTypeStreamOperators<MediaPresenter::Range>("MediaPresenter::Range");
    qRegisterMetaTypeStreamOperators<LastPlay::Item>("LastPlay::Item");
    qRegisterMetaTypeStreamOperators<DTVReader::ChannelInfo>("DTVReader::ChannelInfo");

    QSettings *settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, MainWindow::ORG_NAME, MainWindow::APP_NAME);
    QGLFormat format = QGLFormat::defaultFormat();
    bool useVSync = settings->value(SETTING_USE_VSYNC, false).toBool();

    format.setSwapInterval(useVSync ? 1 : 0);
    QGLFormat::setDefaultFormat(format);

    delete settings;

#ifdef Q_OS_MAC
    {
        QCoreApplication tmp(argc, argv);
        QDir dir(tmp.applicationDirPath());

        dir.cdUp();

        dir.cd("PlugIns");
        dir.cd("platforms");

        qputenv("QT_QPA_PLATFORM_PLUGIN_PATH", dir.absolutePath().toLatin1());
    }
#endif

    AnyVOD a(argc, argv);
    QStringList cmdList = a.arguments();
    QStringList starting;

#ifdef Q_OS_MAC
    QDir dir(a.applicationDirPath());

    dir.cdUp();
    dir.cd("PlugIns");

    a.addLibraryPath(dir.absolutePath());
#endif

#ifdef QT_NO_DEBUG
    QDir::setCurrent(a.applicationDirPath());
#endif

    if (cmdList.count() > 1)
    {
        if (cmdList[1] == "-d")
        {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, MainWindow::ORG_NAME, MainWindow::APP_NAME);

            settings.clear();

            return 0;
        }
        else if (cmdList[1] == "-i")
        {
            Utils::installFileAssociation(MainWindow::ALL_EXTS_LIST);

            return 0;
        }
        else if (cmdList[1] == "-u")
        {
            Utils::uninstallFileAssociation(MainWindow::ALL_EXTS_LIST);

            return 0;
        }
        else
        {
            cmdList.removeFirst();

            QString movies = cmdList.join('\n');

            if (Utils::ApplyCommandPlayList(movies))
                return 0;
            else
                starting = cmdList;
        }
    }

    srand(time(NULL));

    a.setApplicationName(MainWindow::APP_NAME);
    a.setApplicationVersion(MainWindow::VERSION_TEMPLATE.arg(MAJOR).arg(MINOR).arg(PATCH).arg(BUILDNUM));
    a.setOrganizationDomain(MainWindow::ORG_NAME);
    a.setOrganizationName(MainWindow::ORG_NAME);
    a.setWindowIcon(QIcon(ICON_PATH));

    MainWindow w(starting);

    w.show();

    return a.exec();
}

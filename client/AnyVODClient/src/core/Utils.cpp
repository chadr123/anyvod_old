﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "Utils.h"
#include "Settings.h"
#include "TextEncodingDetector.h"

#if !defined Q_OS_MOBILE
#include "device/CaptureInfo.h"
#include "ui/MainWindow.h"
#endif

#include "../../../../common/types.h"

#if !defined Q_OS_MOBILE
#include <QApplication>
#include <QMainWindow>
#include <QListWidget>
#include <QMessageBox>
#include <QFileIconProvider>
#endif

#include <QFile>
#include <QDir>
#include <QDateTime>
#include <QTemporaryFile>
#include <QStringList>
#include <QSettings>
#include <QSharedMemory>
#include <QTextCodec>
#include <QMimeDatabase>
#include <QMap>
#include <QStandardPaths>
#include <QDebug>

#ifdef Q_OS_WIN
#include <Shlobj.h>
#endif

extern "C"
{
    #include <libavutil/pixdesc.h>
}

const QChar Utils::WINDOWS_DIR_SEPARATOR = '\\';
const QChar Utils::DIR_SEPARATOR = '/';
const QString Utils::NETWORK_PATH_PREFIX = "//";
const QString Utils::PROTOCOL_POSTFIX = "://";
const QString Utils::ANYVOD_PROTOCOL = QString(ANYVOD_PROTOCOL_NAME) + PROTOCOL_POSTFIX;
const QString Utils::DTV_PROTOCOL = QString(DTV_PROTOCOL_NAME) + PROTOCOL_POSTFIX;
const QString Utils::FILE_PROTOCOL = QString(FILE_PROTOCOL_NAME) + PROTOCOL_POSTFIX;

const QChar Utils::FFMPEG_SEPARATOR = '#';
const QString Utils::COMMENT_PREFIX = "//";
const QString Utils::TIME_MM_SS = "mm:ss";
const QString Utils::TIME_MM_SS_Z = "mm:ss.z";
const QString Utils::TIME_HH_MM_SS = "hh:mm:ss";
const QString Utils::TIME_HH_MM_SS_ZZZ = "hh:mm:ss.zzz";
const QString Utils::TIME_HH_MM_SS_ZZZ_COMMA = "hh:mm:ss,zzz";
const QString Utils::TIME_HH_MM_SS_ZZZ_DIR = "hh_mm_ss_zzz";
const QTime Utils::ZERO_TIME = QTime(0, 0);

QMap<QString, QTemporaryFile*> Utils::m_cache;
QString Utils::m_subtitleCodecName = "EUC-KR";
Utils::FontInfo Utils::m_defaultFont;

bool Utils::parsePair(const QString &filePath, QMap<QString, QString> *ret)
{
    QFile file(filePath);

    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream stream(&file);

        while (!stream.atEnd())
        {
            QString line = stream.readLine();

            line = line.trimmed();

            if (line.length() == 0)
                continue;

            if (line.startsWith(COMMENT_PREFIX))
                continue;

            QStringList pair = line.split(ENV_ITEM_DELIMITER);

            if (pair.length() < 2)
                continue;

            (*ret)[pair[0]] = pair[1];
        }

        return true;
    }

    return false;
}

void Utils::appendDirSeparator(QString *path)
{
    if (path->length() == 0 || (*path)[path->length() - 1] != DIR_SEPARATOR)
        path->append(DIR_SEPARATOR);
}

QString* Utils::getTimeString(double seconds, const QString &format, QString *ret)
{
    if (seconds < 0.0)
        seconds = 0.0;

    QDateTime time = QDateTime::fromTime_t((uint)seconds);

    time = time.addMSecs((seconds - (uint)seconds) * 1000);

    time = time.toTimeSpec(Qt::UTC);
    *ret = time.toString(format);

    return ret;
}

QString Utils::adjustNetworkPath(const QString &path)
{
    QString ret = path;

#ifdef Q_OS_WIN
    if (ret.startsWith(NETWORK_PATH_PREFIX))
        return ret.replace(DIR_SEPARATOR, WINDOWS_DIR_SEPARATOR);
#endif

    return ret;
}

int Utils::mapTo(int org, int clipped, int value)
{
    float ratio = (float)clipped / org;

    return (int)(ratio * value);
}

QString Utils::removeFFMpegSeparator(const QString &org)
{
    QString ret = org;

    if (ret.startsWith(ANYVOD_PROTOCOL) || ret.startsWith(DTV_PROTOCOL))
    {
        int ffmepgSepIndex = ret.lastIndexOf(FFMPEG_SEPARATOR);

        if (ffmepgSepIndex != -1)
            ret.remove(ffmepgSepIndex, ret.length() - ffmepgSepIndex);
    }

    return ret;
}

void Utils::getPlayItemFromFileNames(const QStringList &list, QVector<PlayItem> *ret)
{
    for (int i = 0; i < list.count(); i++)
    {
        PlayItem item;
        QString path = list[i].trimmed();

        if (!path.isEmpty())
        {
            item.path = path;
            ret->append(item);
        }
    }
}

void Utils::getPlayItemFromYouTube(const QVector<YouTubeURLPicker::Item> &list, bool isPlayList, QVector<PlayItem> *ret)
{
    for (int i = 0; i < list.count(); i++)
    {
        PlayItem item;
        QString path = list[i].orgUrl.trimmed();
        QString title = list[i].title;

        if (!path.isEmpty())
        {
            item.path = path;
            item.extraData.userData = list[i].id.toLatin1();

            if (title.isEmpty())
            {
                item.itemUpdated = false;
            }
            else
            {
                item.title = title;
                item.itemUpdated = true;
            }

            if (!isPlayList)
            {
                item.youtubeIndex = 0;
                item.youtubeData = list;
            }

            ret->append(item);
        }

        if (!isPlayList)
            break;
    }
}

#if !defined Q_OS_MOBILE
void Utils::criticalMessageBox(QWidget *parent, const QString &text)
{
    QMessageBox::critical(parent, trUtf8("오류"), text);
}

void Utils::informationMessageBox(QWidget *parent, const QString &text)
{
    QMessageBox::information(parent, trUtf8("정보"), text);
}

bool Utils::questionMessageBox(QWidget *parent, const QString &text)
{
    if (QMessageBox::question(parent, trUtf8("질문"), text, QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
        return true;
    else
        return false;
}

QList<QListWidgetItem*> Utils::sortByRow(const QList<QListWidgetItem*> &list, const QListWidget &widget)
{
    QMap<int, QListWidgetItem*> sorting;

    for (int i = 0; i < list.count(); i++)
    {
        QListWidgetItem *item = list[i];

        sorting.insert(widget.row(item), item);
    }

    return sorting.values();
}

bool Utils::ApplyCommandPlayList(const QString &path)
{
    QSharedMemory mem(MainWindow::SHARE_MEM_KEY);

    if (mem.attach() || mem.isAttached())
    {
        if (!mem.lock())
            return false;

        char *data = (char*)mem.data();
        QStringList list = QString::fromUtf8(data).split('\n', QString::SkipEmptyParts);
        QByteArray bytes = path.toUtf8();

        list.append(QString::fromUtf8(bytes));

        QByteArray joined = list.join('\n').toUtf8();

        memset(data, 0, MainWindow::SHARE_MEM_SIZE);
        memcpy(data, joined.data(), joined.size());

        mem.unlock();

        return true;
    }

    return false;
}

MainWindow* Utils::getMainWindow()
{
    static MainWindow *mainWindow = NULL;

    if (mainWindow == NULL)
    {
        QWidgetList widgets = qApp->topLevelWidgets();

        for (QWidgetList::iterator i = widgets.begin(); i != widgets.end(); ++i)
        {
            if ((*i)->objectName() == MainWindow::OBJECT_NAME)
            {
                mainWindow = (MainWindow*)(*i);
                break;
            }
        }
    }

    return mainWindow;
}

QString Utils::makeDevicePath(const QString &path)
{
    return CaptureInfo::TYPE + ":" + path;
}

void Utils::getExtentionIcon(const QString &ext, QIcon *ret, bool isDir)
{
    static QFileIconProvider icons;

    if (isDir)
    {
        *ret = icons.icon(QFileIconProvider::Folder);
    }
    else
    {
        if (Utils::m_cache.contains(ext))
        {
            *ret = icons.icon(QFileInfo(Utils::m_cache[ext]->fileName()));
            return;
        }

        QString tmpFilePath = QDir::tempPath();

        Utils::appendDirSeparator(&tmpFilePath);
        tmpFilePath += "XXXXXX.";
        tmpFilePath += ext;

        QTemporaryFile *tmpFile = new QTemporaryFile(tmpFilePath);

        if (tmpFile->open())
        {
            tmpFile->close();
            *ret = icons.icon(QFileInfo(tmpFile->fileName()));
        }
        else
        {
            *ret = icons.icon(QFileIconProvider::File);
        }

        Utils::m_cache[ext] = tmpFile;
    }
}
#endif

void Utils::splitStringByWidth(QString text, int width, int screenWidth, QStringList *ret)
{
    int count = width / screenWidth;
    QString token;

    if (count % screenWidth)
        count++;

    int first = 0;
    int textCount = text.count() / count;

    if (text.count() % count)
        textCount++;

    for (int i = 0; i < count; i++)
    {
        int last;
        int size;

        first = text.lastIndexOf(" ", first);

        if (first < 0)
            first = 0;

        last = first + textCount;
        size = last - first;

        token.insert(0, &text.data()[first], size);
        ret->append(token);
        first += size;
    }
}

bool Utils::determinRemoteFile(const QString &filePath)
{
    return filePath.startsWith(ANYVOD_PROTOCOL);
}

bool Utils::determinRemoteProtocol(const QString &filePath)
{
    return filePath.contains(PROTOCOL_POSTFIX) && !filePath.contains(FILE_PROTOCOL) && !filePath.contains(DTV_PROTOCOL);
}

double Utils::zeroDouble(double value)
{
    if (value > -0.0009 && value < 0.0009)
        return 0.0;
    else
        return value;
}

QDateTime Utils::getTime(double seconds)
{
    QDateTime time = QDateTime::fromTime_t((uint)seconds);

    time = time.addMSecs((seconds - (uint)seconds) * 1000);

    return time.toTimeSpec(Qt::UTC);
}

bool Utils::createDirectory(const QString &path)
{
    QDir dir(path);
    QString parent = dir.absolutePath();
    int index = parent.lastIndexOf(DIR_SEPARATOR);

    if (index == -1)
        return false;

    index++;

    QString subDir = parent.mid(index);

    parent = parent.mid(0, index);

    QDir mkDir(parent);

    if (subDir.isEmpty())
        return true;
    else
        return mkDir.mkpath(subDir);
}

void Utils::getLocalFileList(const QString &path, const QStringList &extList, bool ignoreExtList, QStringList *ret)
{
    QFileInfo info(path);

    if (info.isDir())
    {
        QDir dir(path);
        QStringList filter = QString::fromUtf8("*.%1").arg(extList.join(" *.")).split(" ", QString::SkipEmptyParts);
        QStringList fileList = dir.entryList(filter, QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files);
        QString dirPath = path;

        Utils::appendDirSeparator(&dirPath);

        for (int i = 0; i < fileList.count(); i++)
        {
            QString filePath = dirPath + fileList[i];

            if (QFileInfo(filePath).isDir())
                Utils::getLocalFileList(filePath, extList, ignoreExtList, ret);
            else
                ret->append(filePath);
        }
    }
    else
    {
        QFileInfo info(path);

        if (extList.contains(info.suffix().toLower()) || ignoreExtList)
            ret->append(path);
    }
}

QStringList Utils::getOnlyMediaFileList(const QStringList &list)
{
    QStringList filterd;

    foreach (const QString &item, list)
    {
        if (Utils::isMediaExtension(QFileInfo(item).suffix()))
            filterd.append(item);
    }

    return filterd;
}

void Utils::getLocalFileListOnlyMedia(const QString &path, const QStringList &extList, bool ignoreExtList, QStringList *ret)
{
    Utils::getLocalFileList(path, extList, ignoreExtList, ret);
    *ret = Utils::getOnlyMediaFileList(*ret);
}

bool Utils::is8bitFormat(AVPixelFormat format)
{
    const AVPixFmtDescriptor *desc = av_pix_fmt_desc_get(format);

    if (!desc)
        return false;

    return desc->comp[0].depth == 8;
}

float Utils::clampValue(float value, float min, float max)
{
    if (value < min)
        return min;

    if (value > max)
        return max;

    return value;
}

#ifdef Q_OS_WIN
void Utils::deleteRegKey(HKEY root, const QString &subKey, const QString &toDeleteKey)
{
    HKEY key = NULL;

    if (RegOpenKeyExA(root, subKey.toLocal8Bit(), 0, KEY_SET_VALUE, &key) == ERROR_SUCCESS)
    {
        RegDeleteKeyA(key, toDeleteKey.toLocal8Bit());
        RegCloseKey(key);
    }
}
#endif

#if defined Q_OS_MAC && !defined Q_OS_IOS
void Utils::setDefaultExtension(const QStringList &list, CFStringRef bundleID, UInt32 role)
{
    for (int i = 0; i < list.count(); i++)
    {
        CFStringRef ext = CFStringCreateWithCString(kCFAllocatorDefault, list[i].toUtf8(), kCFStringEncodingUTF8);
        CFStringRef tag = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, ext, NULL);

        LSSetDefaultRoleHandlerForContentType(tag, role, bundleID);

        CFRelease(tag);
        CFRelease(ext);
    }
}

#endif

#ifdef Q_OS_LINUX
void Utils::getExtentionToMime(const QString &ext, QString *ret)
{
    QMimeDatabase db;

    *ret = db.mimeTypeForFile("a." + ext, QMimeDatabase::MatchExtension).name();

    if (ret->startsWith("application"))
        ret->clear();
}

void Utils::setFileAssociationInternal(const QString &filePath, const QString &header, const QStringList &extList, bool erase)
{
    QFile file(filePath);
    QString appName = "dcple-com-AnyVOD.desktop";
    QString codecName;
    TextEncodingDetector detector;
    bool exist = file.exists();

    if (!detector.getCodecName(filePath, &codecName))
        codecName = QTextCodec::codecForLocale()->name();

    if (file.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        QTextStream stream(&file);
        QStringList lines;
        QString line;
        QStringList toWrite;

        stream.setCodec(codecName.toLatin1());

        while (!(line = stream.readLine()).isNull())
        {
            line = line.trimmed();

            if (!line.isEmpty())
                lines.append(line);
        }

        if (erase)
        {
            foreach (const QString &test, lines)
            {
                if (!test.contains(appName))
                    toWrite.append(test);
            }
        }
        else
        {
            int index = -1;

            toWrite = lines;

            if (!exist || toWrite.isEmpty())
                toWrite.append(header);

            for (int i = 0; i < toWrite.count(); i++)
            {
                if (toWrite[i].contains(header, Qt::CaseInsensitive))
                {
                    index = i + 1;
                    break;
                }
            }

            if (index < 0)
            {
                toWrite.append(header);
                index = toWrite.count();
            }

            foreach (const QString &ext, extList)
            {
                QString mime;

                Utils::getExtentionToMime(ext, &mime);

                if (!mime.isEmpty())
                    toWrite.insert(index, mime + "=" + appName + ";");
            }
        }

        file.close();

        if (file.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate))
        {
            stream.setDevice(&file);

            toWrite.removeDuplicates();

            foreach (const QString &test, toWrite)
                stream << test << endl;

            file.close();
        }
    }
}

void Utils::setFileAssociation(const QStringList &extList, bool erase)
{
    QString base = QDir::homePath() + "/.local/share/applications/";

    QDir::root().mkpath(base);

    Utils::setFileAssociationInternal(base + "defaults.list", "[Default Applications]", extList, erase);
    Utils::setFileAssociationInternal(base + "mimeapps.list", "[Added Associations]", extList, erase);
}
#endif

void Utils::installFileAssociation(const QStringList &list)
{
#ifdef Q_OS_WIN
    QSettings hkcr("HKEY_CLASSES_ROOT", QSettings::NativeFormat);
    QSettings hkcu("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts", QSettings::NativeFormat);
    QString appPath = QDir::toNativeSeparators(QApplication::applicationFilePath());

    for (int i = 0; i < list.count(); i++)
    {
        QString ext = list[i].toLower();
        QString fileClass = MainWindow::APP_NAME + ".Media." + ext;
        QString old = hkcr.value("." + ext + "/.").toString();

        hkcr.setValue("." + ext + "/" + fileClass + "_backup", old);
        hkcr.setValue("." + ext + "/.", fileClass);

        hkcr.setValue(fileClass + "/.", "");
        hkcr.setValue(fileClass + "/DefaultIcon/.", appPath + ",0");
        hkcr.setValue(fileClass + "/shell/.", "open");
        hkcr.setValue(fileClass + "/shell/open/.", QString(trUtf8("%1로 열기")).arg(MainWindow::APP_NAME));
        hkcr.setValue(fileClass + "/shell/open/command/.", appPath + " \"%1\"");

        old = hkcu.value("." + ext + "/UserChoice/Progid").toString();
        hkcu.setValue("." + ext + "/UserChoice_backup/Progid", old);

        Utils::deleteRegKey(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\." + ext, "UserChoice");
        hkcu.setValue("." + ext + "/UserChoice/Progid", fileClass);
    }

    SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_FLUSH, NULL, NULL);
#elif defined Q_OS_MAC && !defined Q_OS_IOS
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFStringRef bundleID = CFBundleGetIdentifier(mainBundle);

    Utils::setDefaultExtension(list, bundleID, kLSRolesViewer);

    CFRelease(bundleID);
#elif defined Q_OS_LINUX
    Utils::setFileAssociation(list, false);
#else
    list.count();
#endif
}

void Utils::uninstallFileAssociation(const QStringList &list)
{
#ifdef Q_OS_WIN
    QSettings hkcr("HKEY_CLASSES_ROOT", QSettings::NativeFormat);
    QSettings hkcu("HKEY_CURRENT_USER", QSettings::NativeFormat);

    for (int i = 0; i < list.count(); i++)
    {
        QString ext = list[i].toLower();
        QString fileClass = MainWindow::APP_NAME + ".Media." + ext;
        QString old = hkcr.value("." + ext + "/" + fileClass + "_backup").toString();

        hkcr.setValue("." + ext + "/.", old);
        hkcr.remove(fileClass);
        hkcr.remove("." + ext + "/" + fileClass + "_backup");

        old = hkcu.value("Software/Microsoft/Windows/CurrentVersion/Explorer/FileExts/." + ext + "/UserChoice_backup/Progid").toString();

        Utils::deleteRegKey(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\." + ext, "UserChoice");
        hkcu.setValue("Software/Microsoft/Windows/CurrentVersion/Explorer/FileExts/." + ext + "/UserChoice/Progid", old);

        hkcu.remove("Software/Microsoft/Windows/CurrentVersion/Explorer/FileExts/." + ext + "/UserChoice_backup");
    }

    SHChangeNotify(SHCNE_ASSOCCHANGED, SHCNF_FLUSH, NULL, NULL);
#elif defined Q_OS_MAC && !defined Q_OS_IOS
    CFStringRef bundleID = CFStringCreateWithCString(kCFAllocatorDefault, QString("com.apple.QuickTimePlayerX").toUtf8(), kCFStringEncodingUTF8);

    Utils::setDefaultExtension(list, bundleID, kLSRolesAll);

    CFRelease(bundleID);
#elif defined Q_OS_LINUX
    Utils::setFileAssociation(list, true);
#else
    list.count();
#endif
}

int Utils::findChapter(double pos, const QVector<ChapterInfo> &list)
{
    int index = -1;

    for (int i = 0; i < list.count(); i++)
    {
        const ChapterInfo &info = list[i];

        if (info.start <= pos && pos <= info.end)
        {
            index = i;
            break;
        }
    }

    return index;
}

bool Utils::isVaildPlayListExt(const QString &ext)
{
    if (ext.toLower() == "cue")
        return false;

    static QStringList exts = QString::fromStdWString(PLAYLIST_EXTENSION).split(" ", QString::SkipEmptyParts);

    return exts.contains(ext, Qt::CaseInsensitive);
}

bool Utils::isExtension(const QString &ext, Utils::MEDIA_TYPE type)
{
    QString key = ext.toLower();
    QStringList *searchExts = NULL;
    QStringList *searchCache = NULL;

    switch (type)
    {
        case MT_VIDEO:
        {
            static QStringList exts = QString::fromStdWString(VIDEO_EXTENSION).split(" ", QString::SkipEmptyParts);
            static QStringList cache;
            static bool removed = false;

#if defined Q_OS_MOBILE
            if (!removed)
            {
                exts.removeOne("dat");
                exts.removeOne("bin");

                removed = true;
            }
#else
            (void)removed;
#endif
            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        case MT_AUDIO:
        {
            static QStringList exts = QString::fromStdWString(AUDIO_EXTENSION).split(" ", QString::SkipEmptyParts);
            static QStringList cache;

            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        case MT_SUBTITLE:
        {
            static QStringList exts = QString::fromStdWString(SUBTITLE_EXTENSION).split(" ", QString::SkipEmptyParts);
            static QStringList cache;

            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        case MT_PLAYLIST:
        {
            static QStringList exts = QString::fromStdWString(PLAYLIST_EXTENSION).split(" ", QString::SkipEmptyParts);
            static QStringList cache;

            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        case MT_FONT:
        {
            static QStringList exts = QString::fromStdWString(FONT_EXTENSION).split(" ", QString::SkipEmptyParts);
            static QStringList cache;

            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        case MT_CAPTURE_FORMAT:
        {
            static QStringList exts = QString::fromStdWString(CAPTURE_FORMAT_EXTENSION).split(" ", QString::SkipEmptyParts);
            static QStringList cache;

            searchExts = &exts;
            searchCache = &cache;

            break;
        }
        default:
        {
            return false;
        }
    }

    if (searchCache->contains(key))
    {
        return true;
    }
    else
    {
        if (searchExts->contains(key))
        {
            searchCache->append(key);
            return true;
        }
        else
        {
            return false;
        }
    }
}

bool Utils::isMediaExtension(const QString &ext)
{
    return Utils::isExtension(ext, Utils::MT_VIDEO) ||
            Utils::isExtension(ext, Utils::MT_AUDIO) ||
            Utils::isExtension(ext, Utils::MT_PLAYLIST);
}

bool Utils::isPortrait(double degree)
{
    return (degree >= 70.0 && degree <= 110.0) || (degree >= 250.0 && degree <= 290.0);
}

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
QString Utils::getDevicePath(const QString &path)
{
    int index = path.indexOf(":");
    QString ret;

    if (index < 0)
        return ret;

    ret = path.mid(index + 1);

    return ret;
}

QString Utils::getDeviceType(const QString &path)
{
    int index = path.indexOf(":");
    QString ret;

    if (index < 0)
        return ret;

    ret = path.mid(0, index);

    return ret;
}

bool Utils::determinDevice(const QString &path)
{
    QString type = Utils::getDeviceType(path);

#if defined Q_OS_RASPBERRY_PI
    (void)type;

    return path.contains(DTV_PROTOCOL);
#else
    return type == CaptureInfo::TYPE || path.contains(DTV_PROTOCOL);
#endif
}

QString Utils::makeDTVPath(const DTVReader::ChannelInfo &info)
{
    QString url = QString("%1?type=%2&adapter=%3&index=%4&country=%5")
            .arg(DTV_PROTOCOL)
            .arg((int)info.type)
            .arg(info.adapter)
            .arg(info.index)
            .arg(info.country);

    return url;
}
#endif

QString Utils::sizeToString(unsigned long long size)
{
    QString ret;

    if (size == 0)
        return ret;

    if (size < MEGA)
        ret = QString("%1K").arg((int)floor(size / (double)KILO + 0.5));
    else if (size < GIGA)
        ret = QString("%1M").arg(size / (double)MEGA, 0, 'f', 1);
    else
        ret = QString("%1G").arg(size / (double)GIGA, 0, 'f', 1);

    return ret;
}

bool Utils::isLocal8bit(const QString &text)
{
    bool local8 = false;

    foreach (const QChar c, text)
    {
        if (c >= 0xa0 && c <= 0xff)
        {
            local8 = true;
            break;
        }
    }

    return local8;
}

QString Utils::getSettingPath()
{
#if defined Q_OS_MOBILE
    QString path = QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + QDir::separator();

#if defined Q_OS_RASPBERRY_PI
    path += QString("AnyVODMobile") + QDir::separator();
#endif

    return path;
#else
    return QString();
#endif
}

void Utils::setSubtitleCodecName(const QString &name)
{
    Utils::m_subtitleCodecName = name;
}

QString Utils::getSubtitleCodecName()
{
    return Utils::m_subtitleCodecName;
}

void Utils::setDefaultFont(const FontInfo &font)
{
    Utils::m_defaultFont = font;
}

Utils::FontInfo Utils::getDefaultFont()
{
    return Utils::m_defaultFont;
}

bool Utils::isSupported(int key)
{
    switch (key)
    {
        case AnyVODEnums::SK_USE_SPDIF:
#ifdef Q_OS_RASPBERRY_PI
            return true;
#else
            return false;
#endif
            break;
        default:
            return true;
    }
}

int Utils::decodeFrame(AVCodecContext *ctx, AVPacket *packet, AVFrame *frame, int *decoded)
{
    int success = avcodec_send_packet(ctx, packet);

    if (success == AVERROR(EAGAIN))
    {
        int ret = avcodec_receive_frame(ctx, frame);

        if (ret < 0)
        {
            *decoded = 0;
            return ret;
        }
        else
        {
            *decoded = 1;

            success = avcodec_send_packet(ctx, packet);

            if (success < 0)
                return 0;
            else
                return packet->size;
        }
    }
    else if (success < 0)
    {
        *decoded = 0;
        return success;
    }

    success = avcodec_receive_frame(ctx, frame);

    if (success >= 0)
    {
        *decoded = 1;
        return packet->size;
    }
    else if (success == AVERROR(EAGAIN) || success == AVERROR_EOF)
    {
        *decoded = 0;
        return packet->size;
    }
    else
    {
        *decoded = 0;
        return success;
    }
}

int Utils::encodeFrame(AVCodecContext *ctx, AVPacket *packet, AVFrame *frame, int *encoded)
{
    int success = avcodec_send_frame(ctx, frame);

    if (success == AVERROR(EAGAIN))
    {
        int ret = avcodec_receive_packet(ctx, packet);

        if (ret < 0)
        {
            *encoded = 0;
            return ret;
        }
        else
        {
            *encoded = 1;

            success = avcodec_send_frame(ctx, frame);

            if (success < 0)
                return 0;
            else
                return packet->size;
        }
    }
    else if (success < 0)
    {
        *encoded = 0;
        return success;
    }

    success = avcodec_receive_packet(ctx, packet);

    if (success >= 0)
    {
        *encoded = 1;
        return packet->size;
    }
    else if (success == AVERROR(EAGAIN) || success == AVERROR_EOF)
    {
        *encoded = 0;
        return packet->size;
    }
    else
    {
        *encoded = 0;
        return success;
    }
}

void Utils::release()
{
    foreach (const QTemporaryFile *file, Utils::m_cache.values())
        delete file;
}

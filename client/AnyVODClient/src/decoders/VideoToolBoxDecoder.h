﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "HWDecoderInterface.h"

class VideoToolBoxDecoder : public HWDecoderInterface
{
public:
    VideoToolBoxDecoder();
    virtual ~VideoToolBoxDecoder();

    virtual bool open(AVCodecContext *codec);
    virtual void close();

    virtual bool prepare(AVCodecContext *codec);

    virtual bool getBuffer(AVFrame *ret);
    virtual void releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS]);

    virtual AVPixelFormat getFormat() const;

    virtual bool decodePicture(const AVPacket &packet, AVFrame *ret);
    virtual bool copyPicture(const AVFrame &src, AVFrame *ret);

    virtual bool isDecodable(AVPixelFormat format) const;
    virtual void getDecoderDesc(QString *ret) const;

    virtual void flushSurfaceQueue();
    virtual int getSurfaceQueueCount() const;

private:
    struct Context
    {
        Context()
        {
            codec = NULL;
        }

        AVCodecContext *codec;
    };

private:
    Context m_context;
};


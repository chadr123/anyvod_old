﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include <QGlobalStatic>

#include "HWDecoder.h"
#include "HWDecoderInterface.h"

#ifdef Q_OS_WIN
#include "DXVA2Decoder.h"
#elif defined Q_OS_MAC
#include "VideoToolBoxDecoder.h"
#elif defined Q_OS_LINUX && !defined Q_OS_ANDROID && !defined Q_OS_RASPBERRY_PI
#include "VAAPIDecoder.h"
#elif defined Q_OS_ANDROID
#include "../../AnyVODMobileClient/src/decoders/MediaCodec.h"
#endif

HWDecoder::HWDecoder() :
    m_decoder(NULL),
    m_isOpened(false)
{
#ifdef Q_OS_WIN
    this->m_decoder = new DXVA2Decoder;
#elif defined Q_OS_MAC
    this->m_decoder = new VideoToolBoxDecoder;
#elif defined Q_OS_LINUX && !defined Q_OS_ANDROID && !defined Q_OS_RASPBERRY_PI
    this->m_decoder = new VAAPIDecoder;
#elif defined Q_OS_ANDROID
    this->m_decoder = new MediaCodec;
#endif
}

HWDecoder::~HWDecoder()
{
    if (this->m_decoder != NULL)
    {
        delete this->m_decoder;
        this->m_decoder = NULL;
    }
}

bool HWDecoder::isSuitable(AVCodecID codecID, AVPixelFormat format, int profile) const
{
    if (codecID == AV_CODEC_ID_H264 || codecID == AV_CODEC_ID_MPEG2VIDEO)
    {
        if (codecID == AV_CODEC_ID_H264)
        {
            if (profile == FF_PROFILE_UNKNOWN)
                return false;
        }

        if (format == AV_PIX_FMT_YUV420P || format == AV_PIX_FMT_YUVJ420P || this->m_decoder->isDecodable(format))
            return true;
    }
    else
    {
        return true;
    }

    return false;
}

bool HWDecoder::open(AVCodecContext *codec)
{
    if (this->m_decoder && this->isSuitable(codec->codec_id, codec->pix_fmt, codec->profile))
    {
        this->m_isOpened = this->m_decoder->open(codec);
        return this->m_isOpened;
    }

    return false;
}

void HWDecoder::close()
{
    if (this->m_decoder)
        this->m_decoder->close();

    this->m_isOpened = false;
}

bool HWDecoder::getBuffer(AVFrame *ret)
{
    if (this->m_decoder)
    {
        memset(ret->data, 0, sizeof(ret->data));
        memset(ret->linesize, 0, sizeof(ret->linesize));
        memset(ret->buf, 0, sizeof(ret->buf));

        ret->extended_data = ret->data;

        bool success = this->m_decoder->getBuffer(ret);

        if (success)
        {
            Buffer *buf = new Buffer;

            buf->data = ret->data;
            buf->self = this;

            ret->buf[0] = av_buffer_create(NULL, 0, HWDecoder::freeBuffer, buf, 0);
        }

        return success;
    }

    return false;
}

void HWDecoder::releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS])
{
    if (this->m_decoder && this->isOpened())
        this->m_decoder->releaseBuffer(data);
}

AVPixelFormat HWDecoder::getFormat() const
{
    if (this->m_decoder)
        return this->m_decoder->getFormat();

    return AV_PIX_FMT_NONE;
}

bool HWDecoder::decodePicture(const AVPacket &packet, AVFrame *ret)
{
    if (this->m_decoder)
        return this->m_decoder->decodePicture(packet, ret);

    return false;
}

bool HWDecoder::copyPicture(const AVFrame &src, AVFrame *ret)
{
    if (this->m_decoder)
        return this->m_decoder->copyPicture(src, ret);

    return false;
}

bool HWDecoder::isDecodable(AVPixelFormat format) const
{
    if (this->m_decoder)
        return this->m_decoder->isDecodable(format);

    return false;
}

void HWDecoder::getDecoderDesc(QString *ret) const
{
    if (this->m_decoder)
        this->m_decoder->getDecoderDesc(ret);
}

void HWDecoder::flushSurfaceQueue()
{
    if (this->m_decoder)
        this->m_decoder->flushSurfaceQueue();
}

int HWDecoder::getSurfaceQueueCount() const
{
    if (this->m_decoder)
        return this->m_decoder->getSurfaceQueueCount();

    return 0;
}

bool HWDecoder::isOpened() const
{
    return this->m_isOpened;
}

bool HWDecoder::prepare(AVCodecContext *codec)
{
    if (this->m_decoder)
        return this->m_decoder->prepare(codec);

    return false;
}

void HWDecoder::freeBuffer(void *opaque, uint8_t *)
{
    Buffer *buf = (Buffer*)opaque;

    buf->self->releaseBuffer(buf->data);

    delete buf;
}

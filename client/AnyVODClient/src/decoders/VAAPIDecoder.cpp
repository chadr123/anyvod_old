﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include <QDebug>

#include "VAAPIDecoder.h"

extern "C"
{
    #include <libavcodec/avcodec.h>
    #include <libavutil/imgutils.h>
}

VAAPIDecoder::VAAPIDecoder() :
    m_initialRefCount(0)
{

}

VAAPIDecoder::~VAAPIDecoder()
{
    this->close();
}

bool VAAPIDecoder::open(AVCodecContext *codec)
{
    Context &context = this->m_context;

    if (!this->openDisplay(codec->codec_id, codec->profile))
    {
        this->close();
        return false;
    }

    if (!this->createSurfaces(codec->width, codec->height))
    {
        this->close();
        return false;
    }

    context.hw.display = context.display;
    context.hw.config_id  = context.configID;
    context.hw.context_id = context.contextID;

    codec->hwaccel_context = &context.hw;

    return true;
}

void VAAPIDecoder::close()
{
    this->deleteSurfaces();
    this->closeDisplay();
}

bool VAAPIDecoder::prepare(AVCodecContext *codec)
{
    (void)codec;

    return true;
}

bool VAAPIDecoder::getBuffer(AVFrame *ret)
{
    Context &context = this->m_context;
    int i = 0;
    int old = 0;
    int noUsing = -1;

    for (; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        if (!surface.ref && (noUsing == -1 || surface.order < context.surface[noUsing].order))
            noUsing = i;

        if (surface.order < context.surface[old].order)
            old = i;
    }

    i = (noUsing == -1) ? old : noUsing;

    Surface &surface = context.surface[i];

    surface.ref = this->m_initialRefCount;
    surface.order = context.surfaceOrder++;

    ret->data[0] = (uint8_t*)(unsigned long)surface.id;
    ret->data[3] = (uint8_t*)(unsigned long)surface.id;

    return true;
}

void VAAPIDecoder::releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS])
{
    Surface *surface = this->findSurface((VASurfaceID)((unsigned long)data[3]));

    if (surface)
        surface->ref--;
}

AVPixelFormat VAAPIDecoder::getFormat() const
{
    const Context &context = this->m_context;
    uint32_t fourcc = context.image.format.fourcc;
    AVPixelFormat format;

    if (fourcc == VA_FOURCC('Y','V','1','2') || fourcc == VA_FOURCC('I','4','2','0'))
        format = AV_PIX_FMT_YUV420P;
    else if (fourcc == VA_FOURCC('N','V','1','2'))
        format = AV_PIX_FMT_NV12;
    else
        format = AV_PIX_FMT_NONE;

    return format;
}

bool VAAPIDecoder::decodePicture(const AVPacket &packet, AVFrame *ret)
{
    (void)packet;
    (void)ret;

    return false;
}

bool VAAPIDecoder::copyPicture(const AVFrame &src, AVFrame *ret)
{
    Context &context = this->m_context;
    VASurfaceID srcID = (VASurfaceID)((unsigned long)src.data[3]);
    VASurfaceID destID = context.surfaceQueue[context.surfaceQueueIndex];
    Surface *srcSurface = this->findSurface(srcID);
    AVPixelFormat format = this->getFormat();

    context.surfaceQueue[context.surfaceQueueIndex] = srcID;
    context.surfaceQueueIndex = (context.surfaceQueueIndex + 1) % context.surfaceQueueCount;

    srcSurface->ref++;

    if (!destID)
    {
        if (context.surfaceQueueInit < VAAPI_MAX_SURFACE_QUEUE)
        {
            const int initialColor = 128;

            memset(ret->data[0], 0, ret->linesize[0] * context.surfaceHeight);
            memset(ret->data[1], initialColor, ret->linesize[1] * context.surfaceHeight / 2);

            if (format == AV_PIX_FMT_YUV420P)
                memset(ret->data[2], initialColor, ret->linesize[2] * context.surfaceHeight / 2);

            context.surfaceQueueInit++;
        }

        return true;
    }

    if (vaSyncSurface(context.display, destID))
        return false;

    if (vaGetImage(context.display, destID, 0, 0, context.surfaceWidth, context.surfaceHeight, context.image.image_id))
        return false;

    void *base;

    if (vaMapBuffer(context.display, context.image.buf, &base))
        return false;

    bool result = true;
    AVFrame pic;
    Surface *destSurface = this->findSurface(destID);
    int planes;

    destSurface->ref--;

    if (format == AV_PIX_FMT_YUV420P)
    {
        bool swapUV = context.image.format.fourcc != VA_FOURCC('I','4','2','0');

        planes = 3;

        for (int i = 0; i < planes; i++)
        {
            int srcPlane = (swapUV && i != 0) ?  (3 - i) : i;

            pic.data[i] = (uint8_t*)base + context.image.offsets[srcPlane];
            pic.linesize[i] = context.image.pitches[srcPlane];
        }
    }
    else if (format == AV_PIX_FMT_NV12)
    {
        planes = 2;

        for (int i = 0; i < planes; i++)
        {
            pic.data[i] = (uint8_t*)base + context.image.offsets[i];
            pic.linesize[i] = context.image.pitches[i];
        }
    }
    else
    {
        result = false;
    }

    if (result)
    {
        if (!this->getMemCopy().copyYUV(pic, ret, context.surfaceHeight, planes))
            av_image_copy(ret->data, ret->linesize, (const uint8_t**)pic.data, pic.linesize, format, context.surfaceWidth, context.surfaceHeight);
    }

    if (vaUnmapBuffer(context.display, context.image.buf))
        return false;

    return result;
}

bool VAAPIDecoder::isDecodable(AVPixelFormat format) const
{
    return format == AV_PIX_FMT_VAAPI_VLD;
}

void VAAPIDecoder::getDecoderDesc(QString *ret) const
{
    switch (this->m_context.profile)
    {
        case VAProfileMPEG2Simple:
            *ret = "MPEG2 Simple";
            break;
        case VAProfileMPEG2Main:
            *ret = "MPEG2 Main";
            break;
        case VAProfileH263Baseline:
            *ret = "H263 Baseline";
            break;
        case VAProfileMPEG4Simple:
            *ret = "MPEG4 Simple";
            break;
        case VAProfileMPEG4AdvancedSimple:
            *ret = "MPEG4 Advanced Simple";
            break;
        case VAProfileMPEG4Main:
            *ret = "MPEG4 Main";
            break;
        case VAProfileVC1Simple:
            *ret = "VC1 Simple";
            break;
        case VAProfileVC1Main:
            *ret = "VC1 Main";
            break;
        case VAProfileVC1Advanced:
            *ret = "VC1 Advanced";
            break;
        case VAProfileVP8Version0_3:
            *ret = "VP8";
            break;
        case VAProfileH264Baseline:
            *ret = "H264 Baseline";
            break;
        case VAProfileH264ConstrainedBaseline:
            *ret = "H264 Constrained Baseline";
            break;
        case VAProfileH264Main:
            *ret = "H264 Main";
            break;
        case VAProfileH264High:
            *ret = "H264 High";
            break;
#if VA_CHECK_VERSION(0, 38, 0)
        case VAProfileHEVCMain:
            *ret = "HEVC Main";
            break;
        case VAProfileHEVCMain10:
            *ret = "HEVC Main10";
            break;
#endif
#if VA_CHECK_VERSION(0, 38, 1)
        case VAProfileVP9Profile0:
            *ret = "VP9 Profile0";
            break;
#endif
#if VA_CHECK_VERSION(0, 39, 0)
        case VAProfileVP9Profile3:
            *ret = "VP9 Profile3";
            break;
        case VAProfileVP9Profile2:
            *ret = "VP9 Profile2";
            break;
        case VAProfileVP9Profile1:
            *ret = "VP9 Profile1";
            break;
#endif
        default:
            break;
    }
}

void VAAPIDecoder::flushSurfaceQueue()
{
    Context &context = this->m_context;

    for (int i = 0; i < context.surfaceQueueCount; i++)
    {
        if (context.surfaceQueue[context.surfaceQueueIndex])
            context.surfaceQueue[context.surfaceQueueIndex] = 0;

        context.surfaceQueueIndex = (context.surfaceQueueIndex + 1) % context.surfaceQueueCount;
    }
}

int VAAPIDecoder::getSurfaceQueueCount() const
{
    return this->m_context.surfaceQueueCount;
}

bool VAAPIDecoder::openDisplay(AVCodecID codecID, int ffProfile)
{
    VAProfile profile;
    VAProfile *profilesList;
    bool supported = false;
    int profilesCount = 0;
    int surfaceCount;
    Context &context = this->m_context;

    switch (codecID)
    {
        case AV_CODEC_ID_MPEG2VIDEO:
        {
            profile = VAProfileMPEG2Main;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_H263:
        {
            profile = VAProfileH263Baseline;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_MPEG4:
        {
            profile = VAProfileMPEG4AdvancedSimple;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_WMV3:
        {
            profile = VAProfileVC1Main;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_VC1:
        {
            profile = VAProfileVC1Advanced;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_VP8:
        {
            profile = VAProfileVP8Version0_3;
            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
        case AV_CODEC_ID_H264:
        {
            if (ffProfile == FF_PROFILE_H264_BASELINE)
                profile = VAProfileH264Baseline;
            else if (ffProfile == FF_PROFILE_H264_CONSTRAINED_BASELINE)
                profile = VAProfileH264ConstrainedBaseline;
            else if (ffProfile == FF_PROFILE_H264_MAIN)
                profile = VAProfileH264Main;
            else
                profile = VAProfileH264High;

            surfaceCount = 48 + 1;
            this->m_initialRefCount = 2;

            break;
        }
#if VA_CHECK_VERSION(0, 38, 0)
        case AV_CODEC_ID_HEVC:
        {
            if (ffProfile == FF_PROFILE_HEVC_MAIN_10)
                profile = VAProfileHEVCMain10;
            else
                profile = VAProfileHEVCMain;

            surfaceCount = 16 + 1;
            this->m_initialRefCount = 2;

            break;
        }
#endif
#if VA_CHECK_VERSION(0, 38, 1)
        case AV_CODEC_ID_VP9:
        {
#if VA_CHECK_VERSION(0, 39, 0)
            if (ffProfile == FF_PROFILE_VP9_3)
                profile = VAProfileVP9Profile3;
            else if (ffProfile == FF_PROFILE_VP9_2)
                profile = VAProfileVP9Profile2;
            else if (ffProfile == FF_PROFILE_VP9_1)
                profile = VAProfileVP9Profile1;
            else
#endif
                profile = VAProfileVP9Profile0;

            surfaceCount = 9 + 1;
            this->m_initialRefCount = 2;

            break;
        }
#endif
        default:
        {
            return false;
        }
    }

    surfaceCount += context.surfaceQueueCount;

    context.x11 = XOpenDisplay(NULL);

    if (!context.x11)
        return false;

    context.display = vaGetDisplay(context.x11);

    if (!context.display)
        return false;

    int major;
    int minor;

    VAStatus status = vaInitialize(context.display, &major, &minor);

    if (status != VA_STATUS_SUCCESS)
        return false;

    profilesCount = vaMaxNumProfiles(context.display);
    profilesList = (VAProfile*)calloc(profilesCount, sizeof(VAProfile));

    if (!profilesList)
        return false;

    status = vaQueryConfigProfiles(context.display, profilesList, &profilesCount);

    if (status == VA_STATUS_SUCCESS)
    {
        for ( int i = 0; i < profilesCount; i++)
        {
            if (profilesList[i] == profile)
            {
                supported = true;
                break;
            }
        }
    }

    free(profilesList);

    if (!supported)
        return false;

    VAConfigAttrib attrib;

    memset(&attrib, 0, sizeof(attrib));
    attrib.type = VAConfigAttribRTFormat;

    if (vaGetConfigAttributes(context.display, profile, VAEntrypointVLD, &attrib, 1))
        return false;

    if ((attrib.value & VA_RT_FORMAT_YUV420) == 0)
        return false;

    if (vaCreateConfig(context.display, profile, VAEntrypointVLD, &attrib, 1, &context.configID))
        return false;

    context.surfaceCount = surfaceCount;
    context.profile = profile;

    return true;
}

void VAAPIDecoder::closeDisplay()
{
    Context &context = this->m_context;

    if (context.configID != VA_INVALID_ID)
    {
        vaDestroyConfig(context.display, context.configID);
        context.configID = VA_INVALID_ID;
    }

    if (context.display)
    {
        vaTerminate(context.display);
        context.display = NULL;
    }

    if (context.x11)
    {
        XCloseDisplay(context.x11);
        context.x11 = NULL;
    }

    context.surfaceCount = 0;
}

VAAPIDecoder::Surface* VAAPIDecoder::findSurface(VASurfaceID id)
{
    Context &context = this->m_context;

    if (id == VA_INVALID_SURFACE)
        return NULL;

    for (int i = 0; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        if (surface.id == id)
            return &surface;
    }

    return NULL;
}

bool VAAPIDecoder::createSurfaces(int width, int height)
{
    Context &context = this->m_context;
    VASurfaceID surfaceID[VAAPI_MAX_SURFACE_COUNT];

    width = FFALIGN(width, 16);
    height = FFALIGN(height, 16);

    if (vaCreateSurfaces(context.display, VA_RT_FORMAT_YUV420, width, height, surfaceID, context.surfaceCount, NULL, 0))
        return false;

    for (int i = 0; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        surface.id = surfaceID[i];
        surface.ref = 0;
        surface.order = 0;
    }

    if (vaCreateContext(context.display, context.configID, width, height, VA_PROGRESSIVE, surfaceID, context.surfaceCount, &context.contextID))
        return false;

    int formatCount = vaMaxNumImageFormats(context.display);
    VAImageFormat formats[formatCount];

    if (vaQueryImageFormats(context.display, formats, &formatCount))
        return false;

    bool surpported = false;

    for ( int i = 0; i < formatCount; i++)
    {
        if (formats[i].fourcc == VA_FOURCC( 'Y', 'V', '1', '2' ) ||
                formats[i].fourcc == VA_FOURCC( 'I', '4', '2', '0' ) ||
                formats[i].fourcc == VA_FOURCC( 'N', 'V', '1', '2' ))
        {
            if (vaCreateImage(context.display, &formats[i], width, height, &context.image))
                continue;

            if (vaGetImage(context.display, surfaceID[0], 0, 0, width, height, context.image.image_id))
            {
                vaDestroyImage(context.display, context.image.image_id);
                context.image.image_id = VA_INVALID_ID;

                continue;
            }

            surpported = true;
            break;
        }
    }

    if (!surpported)
        return false;

    context.surfaceWidth = width;
    context.surfaceHeight = height;

    return true;
}

void VAAPIDecoder::deleteSurfaces()
{
    Context &context = this->m_context;

    if (context.image.image_id != VA_INVALID_ID)
    {
        vaDestroyImage(context.display, context.image.image_id);
        context.image.image_id = VA_INVALID_ID;
    }

    if (context.contextID != VA_INVALID_ID)
    {
        vaDestroyContext(context.display, context.contextID);
        context.contextID = VA_INVALID_ID;
    }

    for (int i = 0; i < context.surfaceCount; i++)
    {
        Surface &surface = context.surface[i];

        if (surface.id != VA_INVALID_SURFACE)
        {
            vaDestroySurfaces(context.display, &surface.id, 1);
            surface.id = VA_INVALID_SURFACE;
        }
    }

    context.surfaceQueueIndex = 0;
    context.surfaceQueueCount = VAAPI_MAX_SURFACE_QUEUE;
    context.surfaceQueueInit = 0;
    memset(context.surfaceQueue, 0, sizeof(context.surfaceQueue));

    context.surfaceOrder = 0;
    context.surfaceWidth = 0;
    context.surfaceHeight = 0;
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "HWDecoderInterface.h"

#include <X11/Xlib.h>
#include <va/va_x11.h>

extern "C"
{
    #include <libavcodec/vaapi.h>
}

const int VAAPI_MAX_SURFACE_COUNT = 64;
const int VAAPI_MAX_SURFACE_QUEUE = 2;

class VAAPIDecoder : public HWDecoderInterface
{
public:
    VAAPIDecoder();
    virtual ~VAAPIDecoder();

    virtual bool open(AVCodecContext *codec);
    virtual void close();

    virtual bool prepare(AVCodecContext *codec);

    virtual bool getBuffer(AVFrame *ret);
    virtual void releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS]);

    virtual AVPixelFormat getFormat() const;

    virtual bool decodePicture(const AVPacket &packet, AVFrame *ret);
    virtual bool copyPicture(const AVFrame &src, AVFrame *ret);

    virtual bool isDecodable(AVPixelFormat format) const;
    virtual void getDecoderDesc(QString *ret) const;

    virtual void flushSurfaceQueue();
    virtual int getSurfaceQueueCount() const;

private:
    struct Surface
    {
        Surface()
        {
            id = VA_INVALID_SURFACE;
            ref = 0;
            order = 0;
        }

        VASurfaceID id;
        int ref;
        unsigned int order;
    };

    struct Context
    {
        Context()
        {
            memset(&hw, 0, sizeof(hw));

            x11 = NULL;
            display = NULL;

            configID = VA_INVALID_ID;
            contextID = VA_INVALID_ID;

            surfaceWidth = 0;
            surfaceHeight = 0;
            surfaceCount = 0;
            surfaceOrder = 0;

            surfaceQueueIndex = 0;
            surfaceQueueCount = VAAPI_MAX_SURFACE_QUEUE;
            surfaceQueueInit = 0;
            memset(surfaceQueue, 0, sizeof(surfaceQueue));

            memset(&image, 0, sizeof(image));
            image.image_id = VA_INVALID_ID;
        }

        vaapi_context hw;

        Display *x11;
        VADisplay display;

        VAProfile profile;
        VAConfigID configID;
        VAContextID contextID;

        int surfaceWidth;
        int surfaceHeight;
        int surfaceCount;
        unsigned int surfaceOrder;

        int surfaceQueueIndex;
        int surfaceQueueCount;
        int surfaceQueueInit;
        VASurfaceID surfaceQueue[VAAPI_MAX_SURFACE_QUEUE];

        Surface surface[VAAPI_MAX_SURFACE_COUNT];
        VAImage image;
    };

private:
    bool openDisplay(AVCodecID codecID, int ffProfile);
    void closeDisplay();

    Surface* findSurface(VASurfaceID id);

    bool createSurfaces(int width, int height);
    void deleteSurfaces();

private:
    Context m_context;
    int m_initialRefCount;
};

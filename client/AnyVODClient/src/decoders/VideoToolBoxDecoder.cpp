﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "VideoToolBoxDecoder.h"

#include <QDebug>

extern "C"
{
    #include <libavcodec/videotoolbox.h>
    #include <libavutil/imgutils.h>
}

#include <mach/machine.h>
#include <sys/sysctl.h>

VideoToolBoxDecoder::VideoToolBoxDecoder()
{

}

VideoToolBoxDecoder::~VideoToolBoxDecoder()
{
    this->close();
}

bool VideoToolBoxDecoder::open(AVCodecContext *codec)
{
    this->m_context.codec = codec;

    return true;
}

void VideoToolBoxDecoder::close()
{
    if (this->m_context.codec)
    {
        av_videotoolbox_default_free(this->m_context.codec);
        this->m_context.codec = NULL;
    }
}

bool VideoToolBoxDecoder::prepare(AVCodecContext *codec)
{
    int ret = av_videotoolbox_default_init2(codec, NULL);

    if (ret < 0)
        return false;

    return true;
}

bool VideoToolBoxDecoder::getBuffer(AVFrame *ret)
{
    (void)ret;

    return false;
}

void VideoToolBoxDecoder::releaseBuffer(uint8_t *data[])
{
    (void)data;
}

AVPixelFormat VideoToolBoxDecoder::getFormat() const
{
    AVVideotoolboxContext *context = (AVVideotoolboxContext*)this->m_context.codec->hwaccel_context;
    AVPixelFormat format;

    if (context)
    {
        OSType fourcc = context->cv_pix_fmt_type;

        if (fourcc == kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange)
            format = AV_PIX_FMT_NV12;
        else if (fourcc == kCVPixelFormatType_420YpCbCr8Planar)
            format = AV_PIX_FMT_YUV420P;
        else
            format = AV_PIX_FMT_NONE;
    }
    else
    {
        format = this->m_context.codec->pix_fmt;
    }

    return format;
}

bool VideoToolBoxDecoder::decodePicture(const AVPacket &packet, AVFrame *ret)
{
    (void)packet;
    (void)ret;

    return false;
}

bool VideoToolBoxDecoder::copyPicture(const AVFrame &src, AVFrame *ret)
{
    CVPixelBufferRef pixbuf = (CVPixelBufferRef)src.data[3];
    CVReturn err;

    err = CVPixelBufferLockBaseAddress(pixbuf, kCVPixelBufferLock_ReadOnly);

    if (err != kCVReturnSuccess)
        return false;

    AVFrame pic;
    int planes;

    if (CVPixelBufferIsPlanar(pixbuf))
    {
        planes = CVPixelBufferGetPlaneCount(pixbuf);

        for (int i = 0; i < planes; i++)
        {
            pic.data[i] = (uint8_t*)CVPixelBufferGetBaseAddressOfPlane(pixbuf, i);
            pic.linesize[i] = CVPixelBufferGetBytesPerRowOfPlane(pixbuf, i);
        }
    }
    else
    {
        planes = 1;

        pic.data[0] = (uint8_t*)CVPixelBufferGetBaseAddress(pixbuf);
        pic.linesize[0] = CVPixelBufferGetBytesPerRow(pixbuf);
    }

    int width = this->m_context.codec->width;
    int height = this->m_context.codec->height;
    AVPixelFormat format = this->getFormat();

#if !defined Q_OS_IOS
    if (!this->getMemCopy().copyYUV(pic, ret, height, planes))
#endif
        av_image_copy(ret->data, ret->linesize, (const uint8_t**)pic.data, pic.linesize, format, width, height);

    CVPixelBufferUnlockBaseAddress(pixbuf, kCVPixelBufferLock_ReadOnly);

    return true;
}

bool VideoToolBoxDecoder::isDecodable(AVPixelFormat format) const
{
    if (format == AV_PIX_FMT_VIDEOTOOLBOX || format == AV_PIX_FMT_YUV420P)
    {
        return true;
    }
    else if (format == AV_PIX_FMT_YUV420P10LE)
    {
#if defined Q_OS_IOS
        cpu_type_t type = 0;
        size_t size = sizeof(type);

        sysctlbyname("hw.cputype", &type, &size, NULL, 0);

        return type == CPU_TYPE_ARM64;
#else
        return true;
#endif
    }
    else
    {
        return false;
    }
}

void VideoToolBoxDecoder::getDecoderDesc(QString *ret) const
{
    switch (this->m_context.codec->codec_id)
    {
        case AV_CODEC_ID_H263:
            *ret = "H263";
            break;
        case AV_CODEC_ID_H264:
            *ret = "H264";
            break;
        case AV_CODEC_ID_MPEG1VIDEO:
            *ret = "MPEG1 Video";
            break;
        case AV_CODEC_ID_MPEG2VIDEO:
            *ret = "MPEG2 Video";
            break;
        case AV_CODEC_ID_MPEG4:
            *ret = "MPEG4";
            break;
        default:
            break;
    }
}

void VideoToolBoxDecoder::flushSurfaceQueue()
{

}

int VideoToolBoxDecoder::getSurfaceQueueCount() const
{
    return 0;
}

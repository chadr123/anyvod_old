﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#if !defined Q_OS_MOBILE
#include "video/USWCMemCopy.h"
#endif

#include <QString>

extern "C"
{
    #include <libavcodec/avcodec.h>
    #include <libavutil/pixfmt.h>
}

class HWDecoderInterface
{
public:
    HWDecoderInterface();
    virtual ~HWDecoderInterface();

    virtual bool open(AVCodecContext *codec) = 0;
    virtual void close() = 0;

    virtual bool prepare(AVCodecContext *codec) = 0;

    virtual bool getBuffer(AVFrame *ret) = 0;
    virtual void releaseBuffer(uint8_t *data[AV_NUM_DATA_POINTERS]) = 0;

    virtual AVPixelFormat getFormat() const = 0;

    virtual bool decodePicture(const AVPacket &packet, AVFrame *ret) = 0;
    virtual bool copyPicture(const AVFrame &src, AVFrame *ret) = 0;

    virtual bool isDecodable(AVPixelFormat format) const = 0;
    virtual void getDecoderDesc(QString *ret) const = 0;

    virtual void flushSurfaceQueue() = 0;
    virtual int getSurfaceQueueCount() const = 0;

#if !defined Q_OS_MOBILE
protected:
    USWCMemCopy& getMemCopy();

private:
    USWCMemCopy m_memCopy;
#endif
};

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <stdint.h>
#include <QImage>
#include <QVector>

extern "C"
{
    #include <libavutil/avutil.h>
}

struct AVFormatContext;
struct AVCodecContext;
struct AVPacket;
struct SwsContext;

class FrameExtractor
{
public:
    struct FRAME_ITEM
    {
        FRAME_ITEM()
        {
            time = -1.0;
            realTime = -1.0;
            buffer = NULL;
            rotation = 0.0;
        }

        double time;
        double realTime;
        double rotation;
        QImage frame;
        uint8_t *buffer;
    };

    FrameExtractor();
    ~FrameExtractor();

    bool open(const QString &filePath);
    bool openWithType(const QString &filePath, bool *isVideo);
    void close();

    bool isVideo(const QString &filePath);

    bool getFrame(double time, bool ignoreTime, FRAME_ITEM *ret);
    bool getFramesByPeriod(double period, QVector<FRAME_ITEM> *ret);

    double getDuration() const;
    int getWidth() const;
    int getHeight() const;

private:
    bool readPacket(double *retPTS, AVPacket *ret);
    bool decodePacket(double time, bool ignoreTime, AVPacket &packet, FRAME_ITEM *ret);

private:
    double m_duration;
    int m_width;
    int m_height;
    AVFormatContext *m_format;
    AVCodecContext *m_ctx;
    SwsContext *m_imageConverter;
    int m_index;
    double m_rotation;
};

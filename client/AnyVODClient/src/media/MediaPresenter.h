﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "SyncType.h"
#include "VideoThread.h"
#include "SubtitleThread.h"
#include "ReadThread.h"
#include "RefreshThread.h"
#include "PacketQueue.h"
#include "audio/SPDIF.h"
#include "parsers/subtitle/SAMIParser.h"
#include "parsers/subtitle/ASSParser.h"
#include "parsers/subtitle/SRTParser.h"
#include "parsers/subtitle/LRCParser.h"
#include "parsers/subtitle/AVParser.h"
#include "parsers/subtitle/YouTubeParser.h"
#include "parsers/playlist/CueParser.h"
#include "video/Font.h"
#include "video/Deinterlacer.h"
#include "video/ShaderCompositer.h"

#include "video/FilterGraph.h"
#if !defined Q_OS_MOBILE
#include "net/VirtualFile.h"
#endif

#include "device/DTVReader.h"
#include "decoders/HWDecoder.h"

#include <QThread>
#include <QMutex>
#include <QTime>
#include <QAtomicInt>
#include <QVector>
#include <QVariant>

#ifdef Q_OS_WIN
#include <qwindowdefs.h>
#endif

#include <bass/bass.h>

class QOpenGLFramebufferObject;

#if defined Q_OS_MOBILE
class QOpenGLFunctions;
#endif

struct MediaState;
struct Packet;
struct VideoPicture;
struct Surface;
struct Stream;

class MediaPresenter : public QThread
{
    Q_OBJECT
public:
    friend class VideoThread;
    friend class SubtitleThread;
    friend class ReadThread;
    friend class RefreshThread;

    struct Detail
    {
        Detail()
        {
            cpuUsage = 0.0f;
            lastProcessKernelTime = 0;
            lastProcessUserTime = 0;
            lastSystemKernelTime = 0;
            lastSystemUserTime = 0;
            currentTime = 0.0;
            totalTime = 0.0;
            timePercentage = 0.0;
            dtvSignal = false;
            dtvSignalStrength = 0.0;
            videoInputBits = 0;
            videoInputByteRate = 0;
            videoInputByteCount = 0;
            videoOutputBits = 0;
            videoOutputByteRate = 0;
            videoOutputByteCount = 0;
            videoFrameCount = 0;
            videoFPS = 0.0;
            videoTotalFrame = 0;
            videoCurrentFrame = 0;
            videoFrameDropCount = 0;
            videoInterlaced = false;
            videoDeinterlaced = false;
            audioInputBits = 0;
            audioInputSampleRate = 0;
            audioInputChannels = 0;
            audioInputByteRate = 0;
            audioInputByteCount = 0;
            audioOutputSampleRate = 0;
            audioOutputChannels = 0;
            audioOutputBits = 0;
            audioOutputByteRate = 0;
            audioOutputByteCount = 0;
            subtitleBitmap = false;
            subtitleVaildColor = true;
            subtitleColorCount = 0;
        }

        float cpuUsage;
        uint64_t lastProcessKernelTime;
        uint64_t lastProcessUserTime;
        uint64_t lastSystemKernelTime;
        uint64_t lastSystemUserTime;

        QString fileName;
        double currentTime;
        double totalTime;
        double timePercentage;
        bool dtvSignal;
        double dtvSignalStrength;

        QString videoCodec;
        QString videoHWDecoder;

        QString videoInputType;
        int videoInputBits;
        QSize videoInputSize;
        bool videoInterlaced;
        bool videoDeinterlaced;

        int videoInputByteRate;
        QAtomicInt videoInputByteCount;

        QString videoOutputType;
        int videoOutputBits;
        QSize videoOutputSize;

        int videoOutputByteRate;
        QAtomicInt videoOutputByteCount;

        QAtomicInt videoFrameCount;
        float videoFPS;

        int videoTotalFrame;
        QAtomicInt videoCurrentFrame;
        QAtomicInt videoFrameDropCount;

        QString audioCodec;
        QString audioCodecSimple;

        QString audioInputType;
        int audioInputBits;
        int audioInputSampleRate;
        int audioInputChannels;

        int audioInputByteRate;
        QAtomicInt audioInputByteCount;

        QString audioSPDIFOutputDevice;
        QString audioOutputType;
        int audioOutputSampleRate;
        int audioOutputChannels;
        int audioOutputBits;

        int audioOutputByteRate;
        QAtomicInt audioOutputByteCount;

        QString subtitleCodec;
        bool subtitleBitmap;
        bool subtitleVaildColor;
        int subtitleColorCount;
    };

    struct Range
    {
        Range()
        {
            start = 0.0;
            end = 0.0;
            enable = false;
        }

        double start;
        double end;
        bool enable;
    };

    struct UserAspectRatio
    {
        UserAspectRatio()
        {
            use = false;
            fullscreen = false;
            width = 1.0;
            height = 1.0;
        }

        double getRatio() const
        {
            return width / height;
        }

        bool use;
        bool fullscreen;
        double width;
        double height;
    };

    struct EventCallback
    {
        EventCallback()
        {
            callback = NULL;
            userData = NULL;
        }

        void(*callback)(void*);
        void *userData;
    };

    struct EmptyBufferCallback
    {
        EmptyBufferCallback()
        {
            callback = NULL;
            userData = NULL;
        }

        void(*callback)(void*, bool);
        void *userData;
    };

    struct ShowAudioOptionDescCallback
    {
        ShowAudioOptionDescCallback()
        {
            callback = NULL;
            userData = NULL;
        }

        void(*callback)(void*, const QString&, bool);
        void *userData;
    };

    struct AudioSubtitleCallback
    {
        AudioSubtitleCallback()
        {
            callback = NULL;
            userData = NULL;
        }

        void(*callback)(void*, const QVector<Lyrics>&);
        void *userData;
    };

    struct PaintCallback
    {
        PaintCallback()
        {
            callback = NULL;
            userData = NULL;
        }

        void(*callback)(void*);
        void *userData;
    };

    struct AbortCallback
    {
        AbortCallback()
        {
            callback = NULL;
            userData = NULL;
        }

        void(*callback)(void*, int);
        void *userData;
    };

    struct NonePlayingDescCallback
    {
        NonePlayingDescCallback()
        {
            callback = NULL;
            userData = NULL;
        }

        void(*callback)(void*, const QString&);
        void *userData;
    };

    MediaPresenter(const int width, const int height);
    ~MediaPresenter();

    void initProtocol();

    bool open(const QString &filePath, const QString &title, const ExtraPlayData &data,
              const QString &fontFamily, const int fontSize, const int subtitleOutlineSize,
              const QString &audioPath);
    void close();

    void setDevicePixelRatio(double ratio);

#if defined Q_OS_MOBILE
    void setGL(QOpenGLFunctions *gl);
#endif

    bool saveSubtitleAs(const QString &filePath);
    bool saveSubtitle();

    QString getSubtitlePath() const;

#if !defined Q_OS_MOBILE
    void openRemoteSubtitle(const QString &filePath);
#endif

    bool openYouTube(const QString &id);
    bool openSubtitle(const QString &filePath);
    void closeAllExternalSubtitles();

    bool resetScreen(const int width, const int height, TextureInfo *texInfo, bool inContext);

    bool play();
    void pause();
    void resume();
    void stop();

    bool isPlayUserDataEmpty() const;
    QString getTitle() const;

    void prevFrame(int count);
    void nextFrame(int count);

    bool render(ShaderCompositer &shader);

    void setSubtitleSync(double value);
    double getSubtitleSync() const;

    void audioSync(double value);
    double getAudioSync() const;

    void showDetail(bool show);
    bool isShowDetail() const;
    const Detail& getDetail() const;

    void getAudioDevices(QStringList *ret) const;
    bool setAudioDevice(int device);
    int getCurrentAudioDevice() const;

    void getSPDIFAudioDevices(QStringList *ret);
    bool setSPDIFAudioDevice(int device);
    int getCurrentSPDIFAudioDevice() const;

    bool setAudioDeviceAfter();

    void showSubtitle(bool show);
    bool isShowSubtitle() const;

    bool existFileSubtitle();
    bool existSubtitle();
    bool existExternalSubtitle();

    double getRotation() const;

    bool isAlignable();
    AnyVODEnums::HAlignMethod getHAlign() const;
    void setHAlign(AnyVODEnums::HAlignMethod align);
    AnyVODEnums::VAlignMethod getVAlign() const;
    void setVAlign(AnyVODEnums::VAlignMethod align);

    bool existAudioSubtitle();
    bool existAudioSubtitleGender();

    void getSubtitleClasses(QStringList *classNames);
    void getCurrentSubtitleClass(QString *className);
    bool setCurrentSubtitleClass(const QString &className);

    const QVector<ChapterInfo>& getChapters() const;

    void setSubtitleURL(const QString &url);
    void getSubtitleURL(QString *ret) const;

    void setASSFontPath(const QString &path);
    void setASSFontFamily(const QString &family);

    void resetSubtitlePosition();
    void setVerticalSubtitlePosition(int pos);
    void setHorizontalSubtitlePosition(int pos);
    void setVerticalSubtitleAbsolutePosition(int pos);
    void setHorizontalSubtitleAbsolutePosition(int pos);
    int getVerticalSubtitlePosition() const;
    int getHorizontalSubtitlePosition() const;

    void reset3DSubtitleOffset();
    void setVertical3DSubtitleOffset(int pos);
    void setHorizontal3DSubtitleOffset(int pos);
    void setVertical3DSubtitleAbsoluteOffset(int pos);
    void setHorizontal3DSubtitleAbsoluteOffset(int pos);
    int getVertical3DSubtitleOffset() const;
    int getHorizontal3DSubtitleOffset() const;

    void setRepeatStart(double start);
    void setRepeatEnd(double end);
    void setRepeatEnable(bool enable);
    bool getRepeatEnable() const;
    double getRepeatStart() const;
    double getRepeatEnd() const;

    void setCaptureMode(bool capture);
    bool getCaptureMode() const;

    void setSeekKeyFrame(bool keyFrame);
    bool isSeekKeyFrame() const;

    void set3DMethod(AnyVODEnums::Video3DMethod method);
    AnyVODEnums::Video3DMethod get3DMethod() const;

    void setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method);
    AnyVODEnums::Subtitle3DMethod getSubtitle3DMethod() const;

    void setVRInputSource(AnyVODEnums::VRInputSource source);
    AnyVODEnums::VRInputSource getVRInputSource() const;

    void setSkipRanges(const QVector<Range> &ranges);
    void getSkipRanges(QVector<Range> *ret) const;

    void setSkipOpening(bool skip);
    bool getSkipOpening() const;
    void setSkipEnding(bool skip);
    bool getSkipEnding() const;
    void setUseSkipRange(bool use);
    bool getUseSkipRange() const;

    double getOpeningSkipTime() const;
    double getEndingSkipTime() const;

    void useNormalizer(bool use);
    bool isUsingNormalizer() const;

    void useEqualizer(bool use);
    bool isUsingEqualizer() const;

    void useLowerVoice(bool use);
    bool isUsingLowerVoice() const;

    void useHigherVoice(bool use);
    bool isUsingHigherVoice() const;

    void useLowerMusic(bool use);
    bool isUsingLowerMusic() const;

    void setSubtitleOpaque(float opaque);
    float getSubtitleOpaque() const;

    void setSubtitleSize(float size);
    float getSubtitleSize() const;
    void setScheduleRecomputeSubtitleSize();

    void useHWDecoder(bool enable);
    bool isUseHWDecoder() const;
    bool isOpenedHWDecoder() const;

    void useLowQualityMode(bool enable);
    bool isUseLowQualityMode() const;

    void useSPDIF(bool enable);
    bool isUseSPDIF() const;
    bool isOpenedSPDIF() const;
    bool isSPDIFAvailable() const;

    void setSPDIFEncodingMethod(AnyVODEnums::SPDIFEncodingMethod method);
    AnyVODEnums::SPDIFEncodingMethod getSPDIFEncodingMethod() const;
    bool isUsingSPDIFEncoding() const;

    void setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree);
    AnyVODEnums::ScreenRotationDegree getScreenRotationDegree() const;

    void use3DFull(bool enable);
    bool isUse3DFull() const;

    void usePBO(bool enable);
    bool isUsePBO() const;
    bool isUsablePBO() const;
    bool isUsingPBO() const;

    void setUserSPDIFSampleRate(int sampleRate);
    int getUserSPDIFSampleRate() const;

    bool setPreAmp(float dB);
    float getPreAmp() const;

    bool setEqualizerGain(int band, float gain);
    float getEqualizerGain(int band) const;
    int getBandCount() const;

    bool isEnableSearchSubtitle() const;
    bool isEnableSearchLyrics() const;
    void enableSearchSubtitle(bool enable);
    void enableSearchLyrics(bool enable);

    void showAlbumJacket(bool show);
    bool isShowAlbumJacket() const;

    void useFrameDrop(bool enable);
    bool isUseFrameDrop() const;

    void useBufferingMode(bool enable);
    bool isUseBufferingMode() const;

    void seek(double time, bool any);
    bool recover(double clock);

    void volume(uint8_t volume);
    void mute(bool mute);

    uint8_t getVolume() const;
    uint8_t getMaxVolume() const;

    double getDuration() const;
    double getCurrentPosition();
    bool hasDuration() const;

    double getAspectRatio(bool widthPrio) const;

    bool isEnabledVideo() const;
    bool isAudio() const;
    bool isVideo() const;
    bool isValid() const;

    bool isTempoUsable() const;
    float getTempo() const;
    void setTempo(float percent);

    void setUserAspectRatio(UserAspectRatio &ratio);
    void getUserAspectRatio(UserAspectRatio *ret) const;

    void setMaxTextureSize(int size);
    int getMaxTextureSize() const;

    void useSubtitleCacheMode(bool use);
    bool isUseSubtitleCacheMode() const;

    void showOptionDesc(const QString &desc);
    void clearFonts();
    void clearFrameBuffers();

    float getCPUUsage();

#if !defined Q_OS_MOBILE
    VirtualFile& getVirtualFile();
#endif

    DTVReader& getDTVReader();
    const DTVReader& getDTVReader() const;

    bool isRemoteFile() const;
    bool isRemoteProtocol() const;

    bool getFrameSize(int *width, int *height) const;
    bool getPictureRect(QRect *rect) const;

    bool changeStream(int newIndex, int oldIndex, bool isAudio, int *lastStreamIndex, bool *quitFlag);

    int getCurrentAudioStreamIndex() const;
    void getAudioStreamInfo(QVector<AudioStreamInfo> *ret) const;
    bool changeAudioStream(int index);
    bool resetAudioStream();
    HSTREAM getAudioHandle() const;

    Deinterlacer& getDeinterlacer();
    void setDeinterlacerAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm);

    FilterGraph& getFilterGraph();
    bool configFilterGraph();

    int getOptionDescY() const;
    void setOptionDescY(int y);

    void setVerticalScreenOffset(int offset);
    void setHorizontalScreenOffset(int offset);

    void useBarrelDistortion(bool use);
    bool isUseBarrelDistortion() const;

    void setBarrelDistortionCoefficients(const QVector2D &coefficients);
    QVector2D getBarrelDistortionCoefficients() const;

    void setBarrelDistortionLensCenter(const QVector2D &lensCenter);
    QVector2D getBarrelDistortionLensCenter() const;

    QString getRealFilePath() const;

#ifdef Q_OS_IOS
    void setIOSNotifyCallback(IOSNOTIFYPROC *proc);
#endif
    void setStatusChangedCallback(EventCallback *playing, EventCallback *ended);
    void setEmptyBufferCallback(EmptyBufferCallback &callback);
    void setShowAudioOptionDescCallback(ShowAudioOptionDescCallback &callback);
    void setAudioSubtitleCallback(AudioSubtitleCallback &callback);
    void setPaintCallback(PaintCallback &callback);
    void setAbortCallback(AbortCallback &callback);
    void setNonePlayingDescCallback(NonePlayingDescCallback &callback);
    void setRecoverCallback(EventCallback &callback);

private:
    struct Equalizer
    {
        Equalizer()
        {
            center = 0.0f;
            gain = 0.0f;
            octave = 1.0f;
        }

        float center;
        float gain;
        float octave;
    };

    struct AudioEffect
    {
        AudioEffect()
        {
            useNormalizer = false;
            useEqualizer = false;
            useLowerVoice = false;
            useHigherVoice = false;
            useLowerMusic = false;
            damp = 0;
            compressor = 0;
            eqaulizer = 0;
            preamp = 0;
            lowerVoice = 0;
            higherVoice = 0;
            lowerMusic = 0;

            preampValue = 0;

            Equalizer value;

            value.center = 31.25f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 62.5f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 125.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 250.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 500.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 1000.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 2000.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 4000.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 8000.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            value.center = 16000.0f;
            value.octave = 1.0f;
            equalizerValues.append(value);

            higherVoiceValue.center = 531.0f;
            higherVoiceValue.octave = 4.0f;
            higherVoiceValue.gain = 10.0f;
        }

        bool useNormalizer;
        bool useEqualizer;
        bool useLowerVoice;
        bool useHigherVoice;
        bool useLowerMusic;
        HFX damp;
        HFX compressor;
        HFX eqaulizer;
        HFX preamp;
        HFX lowerVoice;
        HFX higherVoice;
        HFX lowerMusic;
        float preampValue;//dB
        QVector<Equalizer> equalizerValues;
        Equalizer higherVoiceValue;
    };

    enum ASS_RENDER_METHOD
    {
        ASSRM_FILE,
        ASSRM_SINGLE,
        ASSRM_AV
    };

private:
    double getAudioClock();
    double getVideoClock();
    double getExternalClock() const;
    double getMasterClock();
    int64_t getAbsoluteClock() const;
    double getAudioClockOffset() const;
    double frameNumberToClock(int number) const;

    void initFrameBufferObject(QOpenGLFramebufferObject **object, int width, int height);
    void destroyFrameBufferObject(QOpenGLFramebufferObject **object);

    int synchronizeAudio(short *samples, int samplesSize);
    double synchronizeVideo(AVFrame *srcFrame, double pts, const AVRational timeBase);

    int decodeAudioFrame(uint8_t *audioBuffer, int bufSize);
    int decodeAudio(AVCodecContext *ctx, uint8_t **samples, int *frameSize, PacketQueue::Packet *pkt) const;
    int decodeAudioAndSampleCount(AVCodecContext *ctx, uint8_t **samples, int *frameSize, int *sampleCount, PacketQueue::Packet *pkt) const;
    int decodeAudioAsSPDIFEncoding(uint8_t *audioBuffer, int bufSize, AVCodecContext *codec, int *dataSize, PacketQueue::Packet *tmpPacket);

    double calFrameDelay(double pts);
    void refreshSchedule(int delay);
    bool update(ShaderCompositer &shader);
    void updateVideoRefreshTimer(ShaderCompositer &shader);

    void displayVideo(ShaderCompositer &shader, const VideoPicture *vp);

    void allocPicture();
    void releasePictures();
    void flushPictureQueue();

    Surface* createSurface(int width, int height, AVPixelFormat format) const;
    void deleteSurface(Surface *surface) const;

    bool isUseGPUConvert(AVPixelFormat format) const;
    bool isYUV(AVPixelFormat format) const;
    AVPixelFormat getCompatibleFormat(AVPixelFormat format) const;

    uint8_t getLuminanceAvg(uint8_t *data, int size, AVPixelFormat format) const;

    void releaseSubtitles();

    int findWordWrapPos(const QFontMetrics &fm, const QString &text) const;
    void applyWordWrap(const QFontMetrics &fm, SAMIParser::Paragraph *ret) const;
    void applyWordWrap(const QFontMetrics &fm, SRTParser::Item *ret) const;

    void drawDetail(ShaderCompositer &shader, const VideoPicture *vp);
    void drawOptionDesc(ShaderCompositer &shader, const VideoPicture *vp);

    int drawOutlined(ShaderCompositer &shader, Font &font, const QPoint &pos, const QString &text, int outline, const Font::Context &context, float opaque, const VideoPicture *vp) const;

    void drawSubtitles(ShaderCompositer &shader, const VideoPicture *vp);
    int drawSubtitleLine(ShaderCompositer &shader, int lineNum, int totalLineCount, const QString &text,
                         const QPoint &margin, const Font::Context &context, const QString &totalText, int maxWidth,
                         bool forcedLeft, const VideoPicture *vp);
    bool drawAVSubtitle(ShaderCompositer &shader, const QFontMetrics &fm, const AVSubtitle *sp, const VideoPicture *vp);

    bool needMaxWidth() const;
    void getSubtitleSize(const QFontMetrics &fm, const QString &text, QSize *ret) const;
    bool isLeftAlignLine(const QString &text) const;

    void drawASS(const ASS_RENDER_METHOD method, const int32_t time, ShaderCompositer &shader, const VideoPicture *vp);
    void renderASS(ASS_Image *ass, bool blend, ShaderCompositer &shader, const VideoPicture *vp) const;
    void renderASSSub(ShaderCompositer &shader, ShaderCompositer::ShaderType type, const QRect &rect, const Surface &frame, bool blend, const QColor &color, bool updateTexture) const;
    void blendASS(ASS_Image *single, Surface *frame) const;

    bool convertPicture(AVFrame &inFrame, double pts, AVPixelFormat format, bool leftOrTop3D);
    void resizePicture(const VideoPicture *src, VideoPicture *dest) const;
    void copyPicture(const VideoPicture *src, VideoPicture *dest) const;

    void renderTexture(ShaderCompositer &shader, ShaderCompositer::ShaderType type, const QRect &rect,
                       TextureInfo &texInfo, const Surface &surface, const QRectF &renderRect,
                       const QColor &color, const QMatrix4x4 &model, bool updateTexture) const;
    void clearTexture(const QRect &rect, const TextureInfo &texInfo) const;

    void getSubtitlePositionOffset(QPoint *ret) const;
    void getSubtitlePositionOffsetByFrame(const QSize &org, QPoint *ret) const;

    bool get3DParameters(bool leftOrTop3D, const QSizeF &adjust, const QSizeF &surfaceSize, QRectF *renderRect, bool *sideBySide, bool *leftOrTop);
    bool isSideBySide() const;

    bool openStream();
    void closeStream();
    void closeInternal();
    bool openAudioStream(int *ret);

    void seekStream(double pos, double dir, int flag);
    void seek();

    bool isUseAudioPath() const;

    bool openStreamComponent(unsigned int streamIndex, bool isAudio);
    void closeStreamComponent(unsigned int streamIndex, bool isAudio);

    Stream* getStream(int index);
    const char* findProfileName(const AVProfile *profiles, int profile) const;

    void processEmptyAudio();

    AVHWAccel* existHWAccel(AVCodecID codecID) const;
    AVHWAccel* findHWAccel(AVCodecID codecID) const;
    AVCodec* tryInternalHWDecoder(AVCodecContext *context, const QString &postFix) const;
    AVCodec* tryCrystalHDDecoder(AVCodecContext *context) const;

#if defined Q_OS_RASPBERRY_PI
    AVCodec* tryMMALDecoder(AVCodecContext *context) const;
#endif

    void computeFrameSize();
    void computeSubtitleSize();

    void getSPDIFParams(const AVCodecContext *context, int *sampleRate, int *channelCount, AVSampleFormat *format);

    bool initAudio(const AVCodecContext *context);

    bool initNormalizer();
    void closeNormalizer();

    bool initEqualizer();
    void closeEqualizer();

    bool initLowerVoice();
    void closeLowerVoice();

    bool initHigherVoice();
    void closeHigherVoice();

    bool initLowerMusic();
    void closeLowerMusic();

    void scheduleInitTextures();
    SyncType getRecommandSyncType() const;

    void volumeInternal(uint8_t volume);

    void callEmptyCallback(bool show);

    bool openSAMI(const QString &filePath);
    bool openASS(const QString &filePath);
    bool openSRT(const QString &filePath);
    bool openLRC(const QString &filePath);
    bool openAVParser(const QString &filePath);

    void startReadThread();
    void processSkipRange();

    template <typename T1, typename T2>
    int downSampleDecode(AVCodecContext *codec, uint8_t *audioBuffer, int bufSize,
                         double maximum, PacketQueue::Packet &packet, int *dataSize) const;

    static int audioSPDIFCallback(void *buffer, int length, void *user);
    static DWORD CALLBACK audioCallback(HSTREAM, void *buffer, DWORD length, void *user);
    static int decodingInterruptCallback(void *userData);

    static int getBuffer(AVCodecContext *ctx, AVFrame *pic, int flag);
    static AVPixelFormat getFormat(struct AVCodecContext *ctx, const AVPixelFormat *fmt);

protected:
    void abort(int reason);
    virtual void run();

private:
    uint8_t m_volume;
    int m_width;
    int m_height;
    QString m_filePath;
    QString m_realFilePath;
    QString m_audioPath;
    QString m_title;
    MediaState *m_state;
    bool m_forceExit;
    AVPixelFormat m_format;
    Font m_font;
    bool m_showDetail;
    bool m_isMute;
    Font m_subtitleFont;
    int m_subtitleFontSize;
    bool m_showSubtitle;
    bool m_showOptionDesc;
    bool m_showingOptionDesc;
    QMutex m_optionDescMutex;
    double m_subtitleSync;
    double m_audioSync;
    bool m_isRemoteFile;
    bool m_isRemoteProtocol;
    QString m_optionDesc;
    QString m_GOMSubtitleURL;
    QString m_fontFamily;
    int m_fontSize;
    int m_fontOutlineSize;
    int m_subtitleOutlineSize;
    int m_subtitleMaxOutlineSize;
    QVector<AudioStreamInfo> m_audioStreamInfo;
    QVector<SubtitleStreamInfo> m_subtitleStreamInfo;
    int m_lastAudioStream;
    int m_lastSubtitleStream;
    int m_vertPosition;
    int m_horiPosition;
    TextureInfo *m_texInfo;
    bool m_isAudioExt;
    AnyVODEnums::HAlignMethod m_halign;
    AnyVODEnums::VAlignMethod m_valign;
    AnyVODEnums::Video3DMethod m_3dMethod;
    AnyVODEnums::Subtitle3DMethod m_3dSubtitleMethod;
    AnyVODEnums::VRInputSource m_vrInputSource;
    bool m_seekKeyFrame;
    bool m_skipOpening;
    bool m_skipEnding;
    bool m_useSkipRange;
    float m_subtitleOpaque;
    float m_subtitleSize;
    bool m_useHWDecoder;
    bool m_useSPDIF;
    int m_userSPDIFSampleRate;
    bool m_usePBO;
    bool m_enableSearchSubtitle;
    bool m_enableSearchLyrics;
    bool m_showAlbumJacket;
    bool m_useFrameDrop;
    bool m_useBufferingMode;
    AnyVODEnums::SPDIFEncodingMethod m_SPIDFEncodingMethod;
    AnyVODEnums::ScreenRotationDegree m_screenRotationDegree;
    bool m_use3DFull;
    int m_maxTextureSize;
    QOpenGLFramebufferObject *m_anaglyphFrameBuffer;
    QOpenGLFramebufferObject *m_barrelDistortionFrameBuffer;
    QOpenGLFramebufferObject *m_leftBarrelDistortionFrameBuffer;
    QOpenGLFramebufferObject *m_rightBarrelDistortionFrameBuffer;
    QPoint m_3dSubtitleOffset;
    QVector<Range> m_skipRanges;
    QVector<ChapterInfo> m_chapters;
    Range m_repeatRange;
    UserAspectRatio m_userRatio;
    Deinterlacer m_deinterlacer;
    VideoThread m_videoThread;
    SubtitleThread m_subtitleThread;
    ReadThread m_readThread;
    RefreshThread m_refreshThread;
    Detail m_detail;
    ExtraPlayData m_playData;
    SAMIParser m_samiParser;
    ASSParser m_assParser;
    SRTParser m_srtParser;
    LRCParser m_lrcParser;
    AVParser m_avParser;
    YouTubeParser m_youtubeParser;
    CueParser m_cueParser;
    QString m_subtitleFilePath;
    AudioEffect m_audioEffect;
    HWDecoder m_hwDecoder;
    SPDIF m_spdif;
#if !defined Q_OS_MOBILE
    VirtualFile m_virtualFile;
#endif
    DTVReader m_dtvReader;
    FilterGraph m_filterGraph;
#ifdef Q_OS_IOS
    IOSNOTIFYPROC *m_iosNotify;
#endif
    EventCallback m_playing;
    EventCallback m_ended;
    EmptyBufferCallback m_emptyBufferCallback;
    ShowAudioOptionDescCallback m_showAudioOptionDescCallback;
    AudioSubtitleCallback m_audioSubtitleCallback;
    PaintCallback m_paintCallback;
    AbortCallback m_abortCallback;
    NonePlayingDescCallback m_nonePlayingDescCallback;
    EventCallback m_recoverCallback;
#if defined Q_OS_MOBILE
    QOpenGLFunctions *m_gl;
    QMatrix4x4 m_ortho;
#endif
    int m_audioDevice;
    double m_rotation;
    int m_optionDescY;
    bool m_scheduleDisableHWDecoder;
    bool m_scheduleRecomputeSubtitleSize;
    bool m_useSubtitleCacheMode;
    double m_devicePixelRatio;
    bool m_dontWait;
    QString m_assFontFamily;
    bool m_captureMode;
    bool m_useLowQualityMode;
    QMutex m_controlLocker;
    QPoint m_screenOffset;
    bool m_useBarrelDistortion;
    QVector2D m_barrelDistortionCoefficients;
    QVector2D m_barrelDistortionLensCenter;

public:
    static const int OPTION_DESC_TIME;

private:
    static const int DEFAULT_VIRT_SUBTITLE_RATIO;
    static const int DEFAULT_HORI_SUBTITLE_RATIO;
    static const QString SUBTITLE_CODEC_FORMAT;
    static const QPoint DEFAULT_3D_SUBTITLE_OFFSET;
    static const QPoint DEFAULT_VR_SUBTITLE_OFFSET;
    static const AVPixelFormat DEFAULT_PIX_FORMAT;
};

Q_DECLARE_METATYPE(MediaPresenter::Range)
QDataStream& operator << (QDataStream &out, const MediaPresenter::Range &item);
QDataStream& operator >> (QDataStream &in, MediaPresenter::Range &item);

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <QSettings>
#include <QHash>
#include <QDateTime>

class LastPlay
{
public:
    struct Item
    {
        double pos;
        QDateTime lastTime;
    };

public:
    LastPlay();
    ~LastPlay();

    void clean();

    double get(const QString &filePath);
    void clear(const QString &filePath);
    void update(const QString &filePath, double pos);

private:
    QHash<QString, Item> m_table;
    QSettings m_file;
};

Q_DECLARE_METATYPE(LastPlay::Item)
QDataStream& operator << (QDataStream &out, const LastPlay::Item &item);
QDataStream& operator >> (QDataStream &in, LastPlay::Item &item);

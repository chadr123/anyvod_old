﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "ReadThread.h"
#include "MediaPresenter.h"
#include "MediaState.h"

extern "C"
{
#if defined Q_OS_MOBILE
    int avio_feof(AVIOContext *s);
#endif
}

#include <QDebug>

ReadThread::ReadThread(QObject *parent) :
    QThread(parent)
{

}

bool ReadThread::readAudio() const
{
    MediaPresenter *parent = (MediaPresenter*)this->parent();
    MediaState *ms = parent->m_state;
    Audio &audio = ms->audio;
    PacketQueue::Packet packet;
    int ret = av_read_frame(ms->audioFormat, &packet.packet);

    if (ret >= 0)
    {
        if (packet.packet.stream_index == audio.stream.index)
            audio.stream.queue.put(&packet);
        else
            av_packet_unref(&packet.packet);

        return true;
    }
    else
    {
        if (ret == AVERROR_EOF || avio_feof(ms->audioFormat->pb))
            return false;
        else
            return true;
    }
}

void ReadThread::run()
{
    MediaPresenter *parent = (MediaPresenter*)this->parent();
    MediaState *ms = parent->m_state;
    Video &video = ms->video;
    Audio &audio = ms->audio;
    Subtitle &subtitle = ms->subtitle;
    Seek &seek = ms->seek;
    Pause &pause = ms->pause;
    bool videoDelayPut = false;
    bool audioDelayPut = false;
    bool callEnded = false;
    int readFailCount = MAX_READ_RETRY_COUNT;
    bool moreReadAudio = true;
    const DWORD invaildAudioSizeThreshold = 100;

    while (true)
    {
        if (ms->quit)
            break;

        if (seek.seekOnResume && !pause.pause)
        {
            moreReadAudio = true;
            seek.seekOnResume = false;

            parent->seek();
        }

        if (seek.request)
        {
            if (parent->isAudio() && parent->isRemoteFile() && pause.pause)
            {
                seek.seekOnResume = true;
                seek.request = false;
            }
            else
            {
                moreReadAudio = true;
                parent->seek();
            }
        }

        if (ms->readThreadQuit)
            break;

        if (pause.pause)
            this->msleep(PAUSE_REFRESH_DELAY);

        if (audio.stream.queue.getBufferSizeInByte() >= MAX_AUDIO_QUEUE_SIZE ||
                video.stream.queue.getBufferSizeInByte() >= MAX_VIDEO_QUEUE_SIZE ||
                subtitle.stream.queue.getBufferSizeInByte() >= MAX_SUBTITLE_QUEUE_SIZE)
        {
            if (ms->audioFormat)
            {
                if (audio.stream.queue.getBufferSizeInByte() < MAX_AUDIO_QUEUE_SIZE)
                {
                    if (moreReadAudio)
                        moreReadAudio = this->readAudio();

                    continue;
                }

                if (subtitle.stream.queue.getBufferSizeInByte() >= MAX_SUBTITLE_QUEUE_SIZE ||
                        video.stream.queue.getBufferSizeInByte() >= MAX_VIDEO_QUEUE_SIZE)
                {
                    this->msleep(READ_CONTINUE_DELAY);
                    continue;
                }
            }
            else
            {
                this->msleep(READ_CONTINUE_DELAY);
                continue;
            }
        }

        PacketQueue::Packet packet;
        int ret = av_read_frame(ms->format, &packet.packet);

        if (ret < 0)
        {
            if (ret == AVERROR_EOF || avio_feof(ms->format->pb))
            {
                bool ended = false;

                ms->willBeEnd = true;
                ms->seek.readable = true;

                if (!videoDelayPut && video.stream.stream && video.stream.ctx->codec &&
                        video.stream.ctx->codec->capabilities & CODEC_CAP_DELAY)
                {
                    video.stream.queue.putNullPacket(video.stream.index);
                    videoDelayPut = true;
                }

                if (!audioDelayPut && audio.stream.stream && audio.stream.ctx->codec &&
                        audio.stream.ctx->codec->capabilities & CODEC_CAP_DELAY)
                {
                    audio.stream.queue.putNullPacket(audio.stream.index);
                    audioDelayPut = true;
                }

                if (parent->isEnabledVideo())
                {
                    if (audio.stream.stream && video.stream.stream)
                    {
                        DWORD len = 0;

                        if (!parent->m_spdif.isOpened())
                            len = BASS_ChannelGetData(audio.handle, NULL, BASS_DATA_AVAILABLE);

                        if (!audio.stream.queue.hasPacket() && !video.stream.queue.hasPacket() &&
                             (parent->isAudio() || ms->videoFrames.data.size <= 0) && len < invaildAudioSizeThreshold)
                        {
                            ended = true;
                        }
                    }
                    else if (video.stream.stream)
                    {
                        if (!video.stream.queue.hasPacket() && ms->videoFrames.data.size <= 0)
                            ended = true;
                    }
                }
                else
                {
                    DWORD len = 0;

                    if (!parent->m_spdif.isOpened())
                        len = BASS_ChannelGetData(audio.handle, NULL, BASS_DATA_AVAILABLE);

                    if (!audio.stream.queue.hasPacket() && len < invaildAudioSizeThreshold)
                        ended = true;
                }

                if (ended && !callEnded)
                {
                    MediaPresenter::EventCallback &cb = parent->m_ended;

                    if (cb.callback)
                        cb.callback(cb.userData);

                    callEnded = true;
                }

                this->msleep(DEFAULT_REFRESH_DELAY);
            }
            else
            {
                if (AVERROR(EAGAIN) == ret)
                {
                    readFailCount = MAX_READ_RETRY_COUNT;
                }
                else
                {
                    readFailCount--;

                    if (readFailCount <= 0)
                    {
                        parent->abort(ret);
                        break;
                    }
                }
            }

            continue;
        }

        bool audioStreamDiscard = false;

        if (seek.discard && !seek.request)
        {
            Stream *stream = parent->getStream(packet.packet.stream_index);

            if (stream)
            {
                if (packet.packet.dts != AV_NOPTS_VALUE)
                {
                    double pts = av_q2d(stream->stream->time_base) * packet.packet.dts;

                    pts -= parent->getAudioClockOffset();

                    if (pts < seek.discardTime)
                    {
                        packet.discard = true;
                        stream->discardCount++;
                    }
                    else
                    {
                        stream->discard = false;
                        audioStreamDiscard = true;
                    }
                }
                else
                {
                    packet.discard = true;
                    stream->discardCount++;
                }
            }

            if (!video.stream.discard && !audio.stream.discard)
            {
                if (audio.stream.index != -1 && (video.stream.discardCount <= 0 || parent->isAudio()))
                {
                    if (parent->m_spdif.isOpened())
                        parent->m_spdif.resume();
                    else
                        BASS_Start();
                }

                seek.readDiscardDriftTime += parent->getAbsoluteClock() - seek.readDiscardStartTime;
                seek.discard = false;
            }

            if (parent->isAudio() && packet.packet.stream_index == video.stream.index)
                packet.discard = false;
        }

        if (packet.packet.stream_index == video.stream.index)
            video.stream.queue.put(&packet);
        else if (packet.packet.stream_index == audio.stream.index && !ms->audioFormat)
            audio.stream.queue.put(&packet);
        else if (packet.packet.stream_index == subtitle.stream.index)
            subtitle.stream.queue.put(&packet);
        else
            av_packet_unref(&packet.packet);

        if (ms->audioFormat)
        {
            if (moreReadAudio)
                moreReadAudio = this->readAudio();

            if (audioStreamDiscard)
                audio.stream.discard = false;
        }
    }
}

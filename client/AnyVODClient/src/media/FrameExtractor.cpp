﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "FrameExtractor.h"
#include "core/Common.h"
#include "core/Utils.h"

#include <QFileInfo>

#ifndef UNICODE
#define UNICODE
#endif

#ifndef _UNICODE
#define _UNICODE
#endif

#if !defined Q_OS_MOBILE
#include <MediaInfoDLL/MediaInfoDLL.h>
#endif

extern "C"
{
    #include <libavformat/avformat.h>
    #include <libswscale/swscale.h>
}

#if !defined Q_OS_MOBILE
using namespace MediaInfoDLL;
#endif

FrameExtractor::FrameExtractor() :
    m_duration(0.0),
    m_width(0),
    m_height(0),
    m_format(NULL),
    m_ctx(NULL),
    m_imageConverter(NULL),
    m_index(-1),
    m_rotation(0.0)
{
    avcodec_register_all();
    av_register_all();
}

FrameExtractor::~FrameExtractor()
{
    this->close();
}

bool FrameExtractor::openWithType(const QString &filePath, bool *isVideo)
{
    if (this->m_format)
        return false;

    QString path = filePath;

    path = path.replace("mms://", "mmst://", Qt::CaseInsensitive);

    if (avformat_open_input(&this->m_format, path.toUtf8(), NULL, NULL) != 0)
    {
        this->close();
        return false;
    }

    if (avformat_find_stream_info(this->m_format, NULL) < 0)
    {
        this->close();
        return false;
    }

    int videoIndex = -1;

    for (unsigned int i = 0; i < this->m_format->nb_streams; i++)
    {
        AVCodecParameters *context = this->m_format->streams[i]->codecpar;

        if (context->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            videoIndex = i;
            break;
        }
    }

    if (videoIndex < 0)
    {
        this->close();
        return false;
    }

    this->m_index = videoIndex;

    AVStream *stream = this->m_format->streams[videoIndex];
    AVCodecContext *context = avcodec_alloc_context3(NULL);

    if (avcodec_parameters_to_context(context, stream->codecpar) < 0)
    {
        avcodec_free_context(&context);
        return false;
    }

    av_codec_set_pkt_timebase(context, stream->time_base);

    this->m_ctx = context;

    AVCodec *codec = NULL;
    AVCodecID codecID = context->codec_id;

    codec = avcodec_find_decoder(codecID);

    if (!codec)
    {
        this->close();
        return false;
    }

    if (avcodec_open2(context, codec, NULL) < 0)
    {
        this->close();
        return false;
    }

#if !defined Q_OS_MOBILE
    MediaInfo mi;
    bool isDevice = false;

    isDevice = Utils::determinDevice(path);

    if (!isDevice && mi.Open(path.toStdWString()) == 1)
    {
        QString totalTime = QString::fromStdWString(mi.Get(Stream_General, 0, __T("Duration")));

        if (totalTime.toDouble() < 0.0)
            this->m_duration = this->m_format->duration / (double)AV_TIME_BASE;
        else
            this->m_duration = totalTime.toDouble() / 1000.0;

        QString rotation = QString::fromStdWString(mi.Get(Stream_Video, 0, __T("Rotation")));

        this->m_rotation = rotation.toDouble();

        mi.Close();
    }
    else
#endif
    {
        this->m_duration = this->m_format->duration / (double)AV_TIME_BASE;
    }

    if (this->m_duration < 0.0)
        this->m_duration = 0.0;

    if (Utils::zeroDouble(this->m_rotation) <= 0.0)
    {
        AVDictionary *meta = stream->metadata;
        AVDictionaryEntry *entry;
        QString rotation;

        entry = av_dict_get(meta, "rotate", NULL, AV_DICT_MATCH_CASE);

        if (entry)
            rotation = QString::fromLocal8Bit(entry->value);

        this->m_rotation = rotation.toDouble();
    }

    if (isVideo)
    {
        int64_t frames = stream->nb_frames;

        if (frames <= 0)
            frames = av_q2d(stream->avg_frame_rate) * this->m_duration;

        *isVideo = !(frames <= 0 && Utils::isExtension(QFileInfo(path).suffix(), Utils::MT_AUDIO));
    }

    this->m_width = context->width;
    this->m_height = context->height;

    return true;
}

void FrameExtractor::close()
{
    if (this->m_ctx)
        avcodec_free_context(&this->m_ctx);

    if (this->m_format)
        avformat_close_input(&this->m_format);

    if (this->m_imageConverter)
    {
        sws_freeContext(this->m_imageConverter);
        this->m_imageConverter = NULL;
    }

    this->m_index = -1;
    this->m_duration = 0.0;
    this->m_width = 0;
    this->m_height = 0;
    this->m_rotation = 0.0;
}

bool FrameExtractor::isVideo(const QString &filePath)
{
    bool isVideo = false;

    if (this->openWithType(filePath, &isVideo))
        this->close();

    return isVideo;
}

bool FrameExtractor::getFrame(double time, bool ignoreTime, FrameExtractor::FRAME_ITEM *ret)
{
    if (!this->m_format)
        return false;

    AVRational r = {1, AV_TIME_BASE};
    AVStream *stream = this->m_format->streams[this->m_index];

    if (!ignoreTime)
    {
        int64_t target = (int64_t)(time * AV_TIME_BASE);

        target = av_rescale_q(target, r, stream->time_base);

        if (av_seek_frame(this->m_format, this->m_index, target, AVSEEK_FLAG_BACKWARD) < 0)
            return false;
    }

    bool ok = false;

    while (true)
    {
        double pts = -1.0;
        AVPacket packet;

        if (!this->readPacket(&pts, &packet))
            break;

        if (this->decodePacket(time, ignoreTime, packet, ret))
        {
            ok = true;
            av_packet_unref(&packet);

            break;
        }
        else
        {
            av_packet_unref(&packet);

            if (pts > 0.0)
                continue;
        }
    }

    return ok;
}

bool FrameExtractor::getFramesByPeriod(double period, QVector<FrameExtractor::FRAME_ITEM> *ret)
{
    if (!this->m_format)
        return false;

    if (this->m_duration <= 0.0)
        return false;

    for (double time = 0.0; time <= this->m_duration; time += period)
    {
        FRAME_ITEM item;

        if (this->getFrame(time, false, &item))
            ret->append(item);
    }

    return true;
}

double FrameExtractor::getDuration() const
{
    return this->m_duration;
}

int FrameExtractor::getWidth() const
{
    return this->m_width;
}

int FrameExtractor::getHeight() const
{
    return this->m_height;
}

bool FrameExtractor::open(const QString &filePath)
{
    return this->openWithType(filePath, NULL);
}

bool FrameExtractor::readPacket(double *retPTS, AVPacket *ret)
{
    AVStream *stream = this->m_format->streams[this->m_index];

    while (true)
    {
        int error;
        int retryCount = MAX_READ_RETRY_COUNT;

        do
        {
            av_init_packet(ret);
            error = av_read_frame(this->m_format, ret);

            if (error < 0)
            {
                if (AVERROR(EAGAIN) != error)
                    return false;
            }
            else
            {
                retryCount = 0;
            }
        }
        while (retryCount --> 0);

        if (ret->stream_index == this->m_index)
        {
            if (ret->dts != AV_NOPTS_VALUE)
            {
                double pts = av_q2d(stream->time_base) * ret->dts;

                *retPTS = pts;
            }

            break;
        }
        else
        {
            av_packet_unref(ret);
            continue;
        }
    }

    return true;
}

bool FrameExtractor::decodePacket(double time, bool ignoreTime, AVPacket &packet, FrameExtractor::FRAME_ITEM *ret)
{
    AVFrame *frame = av_frame_alloc();
    AVStream *stream = this->m_format->streams[this->m_index];
    int success;
    int decoded;
    bool ok = false;

    success = Utils::decodeFrame(this->m_ctx, &packet, frame, &decoded);

    if (success > 0 && decoded)
    {
        double pts = av_frame_get_best_effort_timestamp(frame);

        pts *= av_q2d(stream->time_base);

        if (pts >= time || ignoreTime)
        {
            AVCodecContext *codec = this->m_ctx;
            int w = codec->width;
            int h = codec->height;

            this->m_imageConverter = sws_getCachedContext(
                        this->m_imageConverter,
                        w, h, codec->pix_fmt,
                        w, h, AV_PIX_FMT_RGB32,
                        SWS_POINT, NULL, NULL, NULL);

            AVFrame pict;

            memset(&pict, 0, sizeof(pict));

            ret->buffer = (uint8_t*)av_malloc(w * h * GET_PIXEL_SIZE(true) * sizeof(uint8_t));

            pict.data[0] = ret->buffer;
            pict.linesize[0] = w * GET_PIXEL_SIZE(true);

            if (this->m_imageConverter)
                sws_scale(this->m_imageConverter, frame->data, frame->linesize, 0, h, pict.data, pict.linesize);

            ret->time = time;
            ret->rotation = this->m_rotation;
            ret->realTime = pts;
            ret->frame = QImage(ret->buffer, w, h, QImage::Format_ARGB32, av_free, ret->buffer);

            ok = true;
        }
    }

    av_frame_free(&frame);

    return ok;
}

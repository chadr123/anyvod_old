﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "SyncType.h"
#include "PacketQueue.h"
#include "core/Common.h"

#include <QImage>
#include <QMutex>

extern "C"
{
    #include <libavformat/avformat.h>
}

struct SwrContext;
struct SwsContext;
struct AVAudioFifo;

#include <bass/bass.h>

#define VIDEO_PICTURE_QUEUE_SIZE (2)
#define MAX_AUDIO_FRAME_SIZE (192000)
#define AUDIO_BUFFER_MULTIPLIER (4)

struct Surface
{
    Surface()
    {
        memset(pixels, 0, sizeof(pixels));
        memset(lineSize, 0, sizeof(lineSize));
        plane = 0;
        width = 0;
        height = 0;
        format = AV_PIX_FMT_NONE;
    }

    QImage getImage() const
    {
        if (pixels[0])
            return QImage(pixels[0], width, height, format == AV_PIX_FMT_YUV420P ? QImage::Format_Grayscale8 : QImage::Format_ARGB32);
        else
            return QImage();
    }

    uint8_t *pixels[PICTURE_MAX_PLANE];
    int lineSize[PICTURE_MAX_PLANE];
    int plane;
    int width;
    int height;
    AVPixelFormat format;
};

struct VideoPicture
{
    VideoPicture()
    {
        surface = NULL;
        width = 0;
        height = 0;
        orgWidth = 0;
        orgHeight = 0;
        pts = 0.0;
        time = 0;
        lumAvg = 0;
        leftOrTop3D = false;
    }

    Surface *surface;
    int width;
    int height;
    int orgWidth;
    int orgHeight;
    double pts;
    uint32_t time;
    uint8_t lumAvg;
    bool leftOrTop3D;
};

struct SubtitleElement
{
    SubtitleElement()
    {
        pts = 0.0;
        nextUpdate = false;
    }

    bool operator < (const SubtitleElement &rhs) const
    {
        return pts < rhs.pts;
    }

    bool operator == (const SubtitleElement &rhs) const
    {
        return pts == rhs.pts;
    }

    AVSubtitle subtitle;
    double pts;
    bool nextUpdate;
};

struct ExternalClock
{
    ExternalClock()
    {
        base = 0;
    }

    int64_t base;
};

struct Seek
{
    Seek()
    {
        pauseSeeking = false;
        flushed = false;
        request = false;
        firstFrameAfterFlush = false;
        firstAudioAfterFlush = false;
        seekOnResume = false;
        readable = true;
        discard = false;
        discardCount = 0;
        flags = 0;
        pos = 0;
        inc = 0.0;
        discardTime = 0.0;
        videoDiscardStartTime = 0;
        videoDiscardDriftTime = 0;
        readDiscardStartTime = 0;
        readDiscardDriftTime = 0;
        requestPauseOnRender = false;
        pauseOnRenderCount = 0;
    }

    bool pauseSeeking;
    bool flushed;
    bool request;
    bool firstFrameAfterFlush;
    bool firstAudioAfterFlush;
    bool seekOnResume;
    bool readable;
    bool discard;
    int discardCount;
    int flags;
    int64_t pos;
    double time;
    double inc;
    double discardTime;
    int64_t videoDiscardStartTime;
    int64_t videoDiscardDriftTime;
    int64_t readDiscardStartTime;
    int64_t readDiscardDriftTime;
    bool requestPauseOnRender;
    int pauseOnRenderCount;
};

struct Stream
{
    Stream()
    {
        index = -1;
        clock = 0.0;
        clockOffset = 0.0;
        discard = false;
        discardCount = 0;
        stream = NULL;
        ctx = NULL;
    }

    int index;
    double clock;
    double clockOffset;
    bool discard;
    int discardCount;
    AVStream *stream;
    AVCodecContext *ctx;
    PacketQueue queue;
    QMutex clockLock;
};

struct Video
{
    Video()
    {
        threadQuit = false;
        frameDrop = 0;
        tempo = 0.0f;
        startTime = 0;
        driftTime = 0;
        assFrame = NULL;
        pixFormat = AV_PIX_FMT_NONE;
    }

    Stream stream;
    bool threadQuit;
    int frameDrop;
    float tempo;
    int64_t startTime;
    int64_t driftTime;
    Surface *assFrame;
    AVPixelFormat pixFormat;
};

struct Subtitle
{
    Subtitle()
    {
        requestReleaseQueue = false;
        threadQuit = false;
        isBitmap = false;
        seekFlags = 0;
        isASS = false;
    }

    Stream stream;
    bool requestReleaseQueue;
    bool threadQuit;
    bool isBitmap;
    int seekFlags;
    bool isASS;
    QSize prevSize;
};

struct AudioSpec
{
    AudioSpec()
    {
        channelCount = 0;
        currentChannelCount = 0;
        bytesPerSec = 0;
        latency = 0.0;
        format = AV_SAMPLE_FMT_NONE;
    }

    int channelCount;
    int currentChannelCount;
    int bytesPerSec;
    double latency;
    AVSampleFormat format;
};

struct SPDIFEncoding
{
    SPDIFEncoding()
    {
        frame = NULL;
        encoder = NULL;
        fifo = NULL;
        buffers = NULL;
        bufferSize = 0;
        tmpBuffers = NULL;
        tmpBufferSize = 0;
    }

    AVFrame *frame;
    AVCodecContext *encoder;
    AVAudioFifo *fifo;
    uint8_t **buffers;
    uint8_t **tmpBuffers;
    int bufferSize;
    int tmpBufferSize;
};

struct Audio
{
    Audio()
    {
        memset(audioBuffer, 0, sizeof(audioBuffer));
        bufferSize = 0;
        bufferIndex = 0;
        packetData = NULL;
        packetSize = 0;
        diffComputation = 0.0;
        diffAvgCoef = 0.0;
        diffThreshold = 0.0;
        diffAvgCount = 0;
        handle = 0;
        tempo = 0;
        isEmpty = false;
        audioConverter = NULL;
    }

    ~Audio()
    {
        av_packet_unref(&packet.packet);
    }

    Stream stream;
    DECLARE_ALIGNED(16, uint8_t, audioBuffer[MAX_AUDIO_FRAME_SIZE * AUDIO_BUFFER_MULTIPLIER]);
    DECLARE_ALIGNED(16, uint8_t, tmpBuffer[MAX_AUDIO_FRAME_SIZE * AUDIO_BUFFER_MULTIPLIER]);
    unsigned int bufferSize;
    unsigned int bufferIndex;
    PacketQueue::Packet packet;
    uint8_t *packetData;
    int packetSize;
    double diffComputation;
    double diffAvgCoef;
    double diffThreshold;
    int diffAvgCount;
    HSTREAM handle;
    HSTREAM tempo;
    AudioSpec spec;
    SPDIFEncoding spdifEncoding;
    bool isEmpty;
    SwrContext *audioConverter;
};

struct FrameTimer
{
    FrameTimer()
    {
        timer = 0;
        lastPTS = 0.0;
        lastRealPTS = 0.0;
        lastDelay = 0.0;
        lowFrameCounter = 0;
    }

    int64_t timer;
    double lastPTS;
    double lastRealPTS;
    double lastDelay;
    int lowFrameCounter;
};

struct FrameQueueData
{
    FrameQueueData()
    {
        size = 0;
        rIndex = 0;
        wIndex = 0;
    }

    int size;
    int rIndex;
    int wIndex;
    PacketQueue::Concurrent lock;
};

struct VideoFrames
{
    VideoPicture queue[VIDEO_PICTURE_QUEUE_SIZE];
    FrameQueueData data;
    VideoPicture audioPicture;
    VideoPicture prevPicture;
};

struct SubtitleFrames
{
    QVector<SubtitleElement> items;
    QMutex lock;
};

struct FrameSize
{
    FrameSize()
    {
        width = 0;
        height = 0;
    }

    int width;
    int height;
};

struct Pause
{
    Pause()
    {
        pause = false;
        startTime = 0;
        driftTime = 0;
        lastPausedTime = 0.0;
    }

    bool pause;
    int64_t startTime;
    int64_t driftTime;
    double lastPausedTime;
};

struct MediaState
{
    MediaState()
    {
        format = NULL;
        audioFormat = NULL;
        syncType = SYNC_AUDIO_MASTER;

        quit = false;
        willBeEnd = false;
        readThreadQuit = false;

        imageYUV420PConverter = NULL;
        imageRGBConverter = NULL;

        streamChangeStartTime = 0;
        streamChangeDriftTime = 0;
    }

    AVFormatContext *format;
    AVFormatContext *audioFormat;

    SyncType syncType;

    ExternalClock externalClock;
    Seek seek;
    Audio audio;
    Video video;
    Subtitle subtitle;
    FrameTimer frameTimer;
    VideoFrames videoFrames;
    SubtitleFrames subtitleFrames;
    FrameSize frameSize;

    bool quit;
    bool willBeEnd;
    bool readThreadQuit;

    int64_t streamChangeStartTime;
    int64_t streamChangeDriftTime;

    Pause pause;

    SwsContext *imageYUV420PConverter;
    SwsContext *imageRGBConverter;
};

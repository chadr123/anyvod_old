﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "RefreshThread.h"
#include "MediaPresenter.h"
#include "MediaState.h"

#include <limits>

#include <QDebug>

using namespace std;

RefreshThread::RefreshThread(QObject *parent) :
    QThread(parent),
    m_stop(false),
    m_fired(false),
    m_started(false)
{

}

RefreshThread::~RefreshThread()
{
    this->m_refreshTime.fetchAndStoreOrdered(0);
    this->stop();
}

bool RefreshThread::isStarted() const
{
    return this->m_started;
}

void RefreshThread::refreshTimer(int refreshTime)
{
    this->m_refreshTime.fetchAndStoreOrdered(refreshTime);
    this->m_fired = true;

    this->m_lock.lock();
    this->m_wait.wakeOne();
    this->m_lock.unlock();
}

void RefreshThread::stop()
{
    this->m_stop = true;
    this->refreshTimer(1);

    if (this->isRunning())
        this->wait();

    this->m_stop = false;
    this->m_fired = false;
    this->m_started = false;
}

void RefreshThread::run()
{
    MediaPresenter *parent = (MediaPresenter*)this->parent();

    while (!this->m_stop)
    {
        if (!this->m_fired)
        {
            this->m_lock.lock();
            this->m_started = true;
            this->m_wait.wait(&this->m_lock);
            this->m_lock.unlock();
        }

        this->m_fired = false;

        int refresh = this->m_refreshTime.fetchAndAddOrdered(0);

        if (refresh > 0)
        {
            this->m_refreshLock.lock();
            this->m_refreshWait.wait(&this->m_refreshLock, refresh);
            this->m_refreshLock.unlock();
        }

#if !defined Q_OS_MOBILE
        if (parent->isEnabledVideo())
        {
#endif
            if (parent->m_paintCallback.callback)
                parent->m_paintCallback.callback(parent->m_paintCallback.userData);
#if !defined Q_OS_MOBILE
        }
#endif
    }
}

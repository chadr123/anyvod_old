﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <QThread>
#include <QWaitCondition>
#include <QMutex>

class RefreshThread : public QThread
{
    Q_OBJECT
public:
    explicit RefreshThread(QObject *parent);
    ~RefreshThread();

    bool isStarted() const;
    void refreshTimer(int refreshTime);
    void stop();

protected:
    virtual void run();

private:
    QWaitCondition m_wait;
    QMutex m_lock;
    QWaitCondition m_refreshWait;
    QMutex m_refreshLock;
    QAtomicInt m_refreshTime;
    bool m_stop;
    bool m_fired;
    bool m_started;
};

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "MediaPresenter.h"
#include "LastPlay.h"

#include <QUuid>
#include <QCoreApplication>

class MediaPlayer
{
    Q_DECLARE_TR_FUNCTIONS(MediaPlayer)
public:
    enum Status
    {
        Started,
        Playing,
        Paused,
        Stopped,
        Ended
    };

    MediaPlayer(const int width, const int height);
    ~MediaPlayer();

    void setPlayingCallback(MediaPresenter::EventCallback &callback);
    void setStatusChangedCallback(MediaPresenter::EventCallback &callback);
    void setEmptyBufferCallback(MediaPresenter::EmptyBufferCallback &callback);
    void setShowAudioOptionDescCallback(MediaPresenter::ShowAudioOptionDescCallback &callback);
    void setAudioSubtitleCallback(MediaPresenter::AudioSubtitleCallback &callback);
    void setPaintCallback(MediaPresenter::PaintCallback &callback);
    void setAbortCallback(MediaPresenter::AbortCallback &callback);
    void setNonePlayingDescCallback(MediaPresenter::NonePlayingDescCallback &callback);

    bool reOpen();
    bool open(const QString &filePath, const QString &title, const ExtraPlayData &data, const QUuid &unique,
              const QString &fontFamily, const int fontSize, const int subtitleOutlineSize, const QString &audioPath);
    void close();
    bool isOpened() const;

    bool resetScreen(const int width, const int height, TextureInfo *texInfo, bool inContext);

    QString getSubtitlePath() const;
    bool openSubtitle(const QString &filePath, bool showDesc);
    bool saveSubtitleAs(const QString &filePath);
    bool saveSubtitle();
    void closeAllExternalSubtitles();

    QString getFileName() const;
    QString getFilePath() const;

    QUuid getUnique() const;
    const QVector<ChapterInfo>& getChapters() const;

    void pause(bool useRaw);
    void resume(bool useRaw);
    bool recover();

    bool play();
    void pause();
    void resume();
    void stop();

    void toggle();

    bool isPlayUserDataEmpty() const;
    QString getTitle() const;

    bool isPlayOrPause() const;
    bool isRemoteFile() const;

    void prevFrame(int count);
    void nextFrame(int count);

    bool canMoveChapter() const;
    bool canMoveFrame() const;

    bool isTempoUsable() const;
    float getTempo() const;
    void setTempo(float percent);

    void setCaptureMode(bool capture);
    bool getCaptureMode() const;

    void setUserAspectRatio(MediaPresenter::UserAspectRatio &ratio);
    void getUserAspectRatio(MediaPresenter::UserAspectRatio *ret) const;

    bool render(ShaderCompositer &shader);

    void getGOMSubtitleURL(QString *ret) const;
    void retreiveExternalSubtitle(const QString &filePath);

    int getCurrentAudioStreamIndex() const;
    void getAudioStreamInfo(QVector<AudioStreamInfo> *ret) const;
    bool changeAudioStream(const int index);
    HSTREAM getAudioHandle() const;

    void setDeinterlaceMethod(AnyVODEnums::DeinterlaceMethod method);
    void setDeinterlaceAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm);
    AnyVODEnums::DeinterlaceMethod getDeinterlaceMethod();
    AnyVODEnums::DeinterlaceAlgorithm getDeinterlaceAlgorithm();

    void setHAlign(AnyVODEnums::HAlignMethod align);
    AnyVODEnums::HAlignMethod getHAlign() const;
    void setVAlign(AnyVODEnums::VAlignMethod align);
    AnyVODEnums::VAlignMethod getVAlign() const;
    bool isAlignable();

    void showDetail(bool show);
    bool isShowDetail() const;
    const MediaPresenter::Detail& getDetail() const;

    void getAudioDevices(QStringList *ret) const;
    bool setAudioDevice(int device);
    int getCurrentAudioDevice() const;

    void getSPDIFAudioDevices(QStringList *ret);
    bool setSPDIFAudioDevice(int device);
    int getCurrentSPDIFAudioDevice() const;

    void setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree);
    AnyVODEnums::ScreenRotationDegree getScreenRotationDegree() const;

    void showOptionDesc(const QString &desc);
    void setASSFontPath(const QString &path);
    void setASSFontFamily(const QString &family);

    void showSubtitle(bool show);
    void showSubtitle(bool show, bool raw);
    bool isShowSubtitle() const;

    void setSearchSubtitleComplex(bool use);
    bool getSearchSubtitleComplex() const;

    bool existSubtitle();
    bool existFileSubtitle();
    bool existExternalSubtitle();
    bool existAudioSubtitle();
    bool existAudioSubtitleGender();

    void getSubtitleClasses(QStringList *classNames);
    void getCurrentSubtitleClass(QString *className);
    bool setCurrentSubtitleClass(const QString &className);

    void resetSubtitlePosition();
    void setVerticalSubtitlePosition(int pos);
    void setHorizontalSubtitlePosition(int pos);
    int getVerticalSubtitlePosition();
    int getHorizontalSubtitlePosition();

    void reset3DSubtitleOffset();
    void setVertical3DSubtitleOffset(int offset);
    void setHorizontal3DSubtitleOffset(int offset);
    int getVertical3DSubtitleOffset();
    int getHorizontal3DSubtitleOffset();

    void setRepeatStart(double start);
    void setRepeatEnd(double end);
    void setRepeatEnable(bool enable);
    bool getRepeatEnable() const;
    double getRepeatStart() const;
    double getRepeatEnd() const;

    void setRepeatStartMove(double offset);
    void setRepeatEndMove(double offset);
    void setRepeatMove(double offset);

    void setSeekKeyFrame(bool keyFrame);
    bool isSeekKeyFrame() const;

    void setSubtitleDirectory(const QStringList &paths, bool prior);
    void getSubtitleDirectory(QStringList *path, bool *prior) const;

    void set3DMethod(AnyVODEnums::Video3DMethod method);
    AnyVODEnums::Video3DMethod get3DMethod() const;

    void setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method);
    AnyVODEnums::Subtitle3DMethod getSubtitle3DMethod() const;

    void setSkipRanges(const QVector<MediaPresenter::Range> &ranges);
    void getSkipRanges(QVector<MediaPresenter::Range> *ret) const;

    void setSkipOpening(bool skip);
    bool getSkipOpening() const;
    void setSkipEnding(bool skip);
    bool getSkipEnding() const;
    void setUseSkipRange(bool use);
    bool getUseSkipRange() const;

    void useNormalizer(bool use);
    bool isUsingNormalizer() const;

    void useEqualizer(bool use);
    bool isUsingEqualizer() const;

    void useLowerVoice(bool use);
    bool isUsingLowerVoice() const;

    void useHigherVoice(bool use);
    bool isUsingHigherVoice() const;

    void useLowerMusic(bool use);
    bool isUsingLowerMusic() const;

    void addSubtitleOpaque(float inc);
    float getSubtitleOpaque() const;
    void resetSubtitleOpaque();

    void addSubtitleSize(float inc);
    float getSubtitleSize() const;
    void setSubtitleSize(float size);
    void resetSubtitleSize();

    void useHWDecoder(bool enable);
    bool isUseHWDecoder() const;

    void useFrameDrop(bool enable);
    bool isUseFrameDrop() const;

    void useBufferingMode(bool enable);
    bool isUseBufferingMode() const;

    void useSPDIF(bool enable);
    bool isUseSPDIF() const;
    bool isOpenedSPDIF() const;
    bool isSPDIFAvailable() const;

    void setSPDIFEncodingMethod(AnyVODEnums::SPDIFEncodingMethod method);
    AnyVODEnums::SPDIFEncodingMethod getSPDIFEncodingMethod() const;

    void use3DFull(bool enable);
    bool isUse3DFull() const;

    void usePBO(bool enable);
    bool isUsePBO() const;
    bool isUsablePBO() const;

    void setUserSPDIFSampleRate(int sampleRate);
    int getUserSPDIFSampleRate() const;

    bool setPreAmp(float dB);
    float getPreAmp() const;

    bool setEqualizerGain(int band, float gain);
    float getEqualizerGain(int band) const;
    int getBandCount() const;

    void prevSubtitleSync(double amount);
    void nextSubtitleSync(double amount);
    void resetSubtitleSync();

    void prevAudioSync(double amount);
    void nextAudioSync(double amount);
    void resetAudioSync();

    bool isEnableSearchSubtitle() const;
    bool isEnableSearchLyrics() const;
    void enableSearchSubtitle(bool enable);
    void enableSearchLyrics(bool enable);

    void seek(double time, bool any);
    void rewind(double distance);
    void forward(double distance);

    void volume(uint8_t volume);
    void mute(bool mute, bool showDesc);

    uint8_t getMaxVolume() const;
    uint8_t getVolume() const;

    double getDuration() const;
    double getCurrentPosition();
    bool hasDuration() const;

    double getAspectRatio(bool widthPrio) const;

    void setMaxTextureSize(int size);
    int getMaxTextureSize() const;

    Status getStatus();

    bool isEnabledVideo() const;
    bool isAudio() const;
    bool isVideo() const;
    bool isSubtitleMoveable();
    bool is3DSubtitleMoveable();
    bool isMovieSubtitleVisiable() const;

    void showAlbumJacket(bool show);
    bool isShowAlbumJacket() const;

    bool getFrameSize(int *width, int *height) const;
    bool getPictureRect(QRect *rect) const;

    void enableGotoLastPos(bool enable, bool raw);
    bool isGotoLastPos() const;

    VirtualFile& getVirtualFile();
    DTVReader& getDTVReader();
    FilterGraph& getFilterGraph();

    bool configFilterGraph();

private:
    void navigate(double distance);

    void subtitleSync(double value);
    void audioSync(double value);

    static void playingCallback(void *userData);
    static void endedCallback(void *userData);

    void callChanged();

    void tryOpenSubtitle(const QString &filePath);

    void retreiveLyrics(const QString &filePath);
    void retreiveSubtitleURL(const QString &filePath);

    void updateLastPlay();

private:
    MediaPresenter m_presenter;
    Status m_status;
    QString m_filePath;
    QString m_audioPath;
    QString m_title;
    QString m_lastPlayPath;
    QUuid m_unique;
    ExtraPlayData m_playData;
    QString m_fontFamily;
    int m_fontSize;
    int m_subtitleOutlineSize;
    MediaPresenter::EventCallback m_playing;
    MediaPresenter::EventCallback m_statusChanged;
    LastPlay m_lastPlay;
    bool m_gotoLastPlay;
    bool m_priorSubtitleDirectory;
    bool m_useSearchSubtitleComplex;
    QStringList m_subtitleDirectory;
};

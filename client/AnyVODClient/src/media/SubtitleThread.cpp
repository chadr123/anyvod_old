﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "SubtitleThread.h"
#include "MediaPresenter.h"
#include "MediaState.h"

#include <limits>

#include <QDebug>

using namespace std;

SubtitleThread::SubtitleThread(QObject *parent) :
    QThread(parent),
    m_lastVaildTime(0)
{

}

void SubtitleThread::run()
{
    MediaPresenter *parent = (MediaPresenter*)this->parent();
    MediaState *ms = parent->m_state;
    Subtitle &subtitle = ms->subtitle;
    SubtitleFrames &frames = ms->subtitleFrames;
    QVector<bool*> quits;

    quits.append(&ms->quit);
    quits.append(&ms->subtitle.threadQuit);

    this->m_lastVaildTime = 0;

    while (true)
    {
        class localPtr
        {
        public:
            localPtr()
            {

            }

            ~localPtr()
            {
                av_packet_unref(&this->packet.packet);
            }

            PacketQueue::Packet packet;
        }l;

        if (ms->quit)
            break;

        if (ms->subtitle.threadQuit)
            break;

        bool block = true;

        if (!subtitle.stream.queue.get(&l.packet, quits, &block))
            break;

        if (subtitle.stream.queue.isFlushPacket(&l.packet))
        {
            avcodec_flush_buffers(subtitle.stream.ctx);
            continue;
        }

        if (!l.packet.discard)
        {
            int decoded = 0;
            int success = 0;
            double pts = 0.0;
            SubtitleElement sub;

            if (l.packet.packet.pts != AV_NOPTS_VALUE)
                pts = av_q2d(subtitle.stream.stream->time_base) * l.packet.packet.pts;

            sub.pts = pts;

            frames.lock.lock();

            if (!frames.items.contains(sub))
            {
                success = avcodec_decode_subtitle2(subtitle.stream.ctx, &sub.subtitle, &decoded, &l.packet.packet);

                if (success && decoded)
                {
                    if (sub.subtitle.num_rects > 0)
                    {
                        subtitle.isBitmap = sub.subtitle.rects[0]->type == SUBTITLE_BITMAP;
                        subtitle.isASS = sub.subtitle.rects[0]->type == SUBTITLE_ASS;

                        if (subtitle.isASS)
                        {
                            for (unsigned int i = 0; i < sub.subtitle.num_rects; i++)
                                parent->m_assParser.parseSingle(sub.subtitle.rects[i]->ass);

                            avsubtitle_free(&sub.subtitle);
                            frames.lock.unlock();

                            continue;
                        }
                    }

                    frames.items.push_back(sub);

                    bool rebuild = IS_BIT_SET(subtitle.seekFlags, AVSEEK_FLAG_BACKWARD) && ms->seek.firstFrameAfterFlush;

                    if (rebuild)
                    {
                        qStableSort(frames.items);
                        subtitle.seekFlags = 0;
                    }

                    int count = frames.items.count();

                    if (count > 1)
                    {
                        if (rebuild)
                        {
                            for (int i = 0; i < count - 1; i++)
                                this->rebuildSubtitleTime(frames.items[i + 1], &frames.items[i]);
                        }
                        else
                        {
                            this->rebuildSubtitleTime(frames.items[count - 1], &frames.items[count - 2]);
                        }
                    }

                    SubtitleElement &last = frames.items.last();

                    if (last.subtitle.start_display_time == last.subtitle.end_display_time ||
                            last.subtitle.end_display_time == numeric_limits<uint32_t>::max())
                    {
                        last.subtitle.end_display_time = this->m_lastVaildTime;
                        last.nextUpdate = true;
                    }
                    else
                    {
                        this->m_lastVaildTime = max(last.subtitle.end_display_time, this->m_lastVaildTime);
                    }
                }
            }

            frames.lock.unlock();
        }
        else
        {
            l.packet.discard = false;
        }
    }
}

bool SubtitleThread::rebuildSubtitleTime(const SubtitleElement &second, SubtitleElement *first) const
{
    if (first->subtitle.start_display_time == first->subtitle.end_display_time ||
            first->subtitle.end_display_time == numeric_limits<uint32_t>::max() ||
            first->nextUpdate)
    {
        double diff = second.pts - first->pts;

        if (diff < 0.0)
            return false;

        first->subtitle.end_display_time = diff * 1000.0;

        if (first->subtitle.end_display_time > this->m_lastVaildTime && this->m_lastVaildTime > 0)
        {
            first->subtitle.end_display_time = this->m_lastVaildTime;
            return true;
        }
    }

    if (first->nextUpdate)
        first->nextUpdate = false;

    return true;
}

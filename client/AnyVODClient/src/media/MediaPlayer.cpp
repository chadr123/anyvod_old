﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "MediaPlayer.h"
#include "core/Utils.h"
#include "net/SubtitleImpl.h"
#include "net/Socket.h"
#include "ui/MainWindow.h"
#include "../../../../common/SubtitleFileNameGenerator.h"

#include <QFileInfo>
#include <QTemporaryFile>
#include <QDir>
#include <QTextStream>
#include <QDebug>

MediaPlayer::MediaPlayer(const int width, const int height) :
    m_presenter(width, height),
    m_fontSize(0),
    m_subtitleOutlineSize(0),
    m_gotoLastPlay(false),
    m_priorSubtitleDirectory(false),
    m_useSearchSubtitleComplex(false)
{
    this->m_presenter.initProtocol();
    this->m_status = Stopped;

    MediaPresenter::EventCallback playing;
    MediaPresenter::EventCallback ended;

    playing.callback = MediaPlayer::playingCallback;
    playing.userData = this;

    ended.callback = MediaPlayer::endedCallback;
    ended.userData = this;

    this->m_presenter.setStatusChangedCallback(&playing, &ended);
}

MediaPlayer::~MediaPlayer()
{
    this->close();
}

void MediaPlayer::setPlayingCallback(MediaPresenter::EventCallback &callBack)
{
    this->m_playing = callBack;
}

void MediaPlayer::setStatusChangedCallback(MediaPresenter::EventCallback &callBack)
{
    this->m_statusChanged = callBack;
}

void MediaPlayer::setEmptyBufferCallback(MediaPresenter::EmptyBufferCallback &callback)
{
    this->m_presenter.setEmptyBufferCallback(callback);
}

void MediaPlayer::setShowAudioOptionDescCallback(MediaPresenter::ShowAudioOptionDescCallback &callback)
{
    this->m_presenter.setShowAudioOptionDescCallback(callback);
}

void MediaPlayer::setAudioSubtitleCallback(MediaPresenter::AudioSubtitleCallback &callback)
{
    this->m_presenter.setAudioSubtitleCallback(callback);
}

void MediaPlayer::setPaintCallback(MediaPresenter::PaintCallback &callback)
{
    this->m_presenter.setPaintCallback(callback);
}

void MediaPlayer::setAbortCallback(MediaPresenter::AbortCallback &callback)
{
    this->m_presenter.setAbortCallback(callback);
}

void MediaPlayer::setNonePlayingDescCallback(MediaPresenter::NonePlayingDescCallback &callback)
{
    this->m_presenter.setNonePlayingDescCallback(callback);
}

bool MediaPlayer::reOpen()
{
    ExtraPlayData data;

    if (Utils::determinRemoteFile(this->m_filePath) || Utils::determinRemoteProtocol(this->m_filePath))
        data = this->m_playData;

    return this->open(this->m_filePath, this->m_title, data, this->m_unique, this->m_fontFamily,
                      this->m_fontSize, this->m_subtitleOutlineSize, this->m_audioPath);
}

bool MediaPlayer::isOpened() const
{
    return !this->m_filePath.isEmpty();
}

QString MediaPlayer::getSubtitlePath() const
{
    return this->m_presenter.getSubtitlePath();
}

bool MediaPlayer::openSubtitle(const QString &filePath, bool showDesc)
{
    QFileInfo info(filePath);
    bool success = false;

    this->m_presenter.closeAllExternalSubtitles();
    success = this->m_presenter.openSubtitle(filePath);

    if (success && showDesc)
    {
        if (this->isMovieSubtitleVisiable())
            this->m_presenter.showOptionDesc(trUtf8("자막 열기 : %1").arg(info.fileName()));
        else
            this->m_presenter.showOptionDesc(trUtf8("가사 열기 : %1").arg(info.fileName()));
    }

    return success;
}

bool MediaPlayer::saveSubtitleAs(const QString &filePath)
{
    QString desc;
    QFileInfo info(filePath);
    bool success = this->m_presenter.saveSubtitleAs(filePath);

    if (success)
    {
        if (this->isMovieSubtitleVisiable())
            desc = trUtf8("자막이 저장 되었습니다 : %1");
        else
            desc = trUtf8("가사가 저장 되었습니다 : %1");
    }
    else
    {
        if (this->isMovieSubtitleVisiable())
            desc = trUtf8("자막이 저장 되지 않았습니다 : %1");
        else
            desc = trUtf8("가사가 저장 되지 않았습니다 : %1");
    }

    this->showOptionDesc(desc.arg(info.fileName()));

    return success;
}

bool MediaPlayer::saveSubtitle()
{
    return this->saveSubtitleAs(this->getSubtitlePath());
}

void MediaPlayer::closeAllExternalSubtitles()
{
    this->m_presenter.closeAllExternalSubtitles();

    if (this->isMovieSubtitleVisiable())
        this->m_presenter.showOptionDesc(trUtf8("외부 자막 닫기"));
    else
        this->m_presenter.showOptionDesc(trUtf8("외부 가사 닫기"));
}

QString MediaPlayer::getFileName() const
{
    QFileInfo info(this->m_filePath);

    return info.fileName();
}

QString MediaPlayer::getFilePath() const
{
    return this->m_filePath;
}

QUuid MediaPlayer::getUnique() const
{
    return this->m_unique;
}

const QVector<ChapterInfo>& MediaPlayer::getChapters() const
{
    return this->m_presenter.getChapters();
}

void MediaPlayer::tryOpenSubtitle(const QString &filePath)
{
    QFileInfo info(filePath);
    QString path = info.absolutePath();

    Utils::appendDirSeparator(&path);

    this->m_presenter.closeAllExternalSubtitles();

    vector_wstring fileNames;
    SubtitleFileNameGenerator gen;
    bool opened = false;
    QStringList searchPaths = this->m_subtitleDirectory;

    if (this->m_priorSubtitleDirectory)
        searchPaths.append(path);
    else
        searchPaths.prepend(path);

    foreach (const QString &searchPath, searchPaths)
    {
        fileNames.clear();
        gen.getFileNamesWithExt(filePath.toStdWString(), &fileNames);

        for (size_t i = 0; i < fileNames.size(); i++)
        {
            QString subtitleFilePath = searchPath + QString::fromStdWString(fileNames[i]);

            if (this->m_presenter.openSubtitle(subtitleFilePath))
            {
                opened = true;
                break;
            }
        }

        if (!opened && this->m_useSearchSubtitleComplex)
        {
            fileNames.clear();
            gen.getFileNames(filePath.toStdWString(), &fileNames);

            for (size_t i = 0; i < fileNames.size(); i++)
            {
                QString subtitleFilePath = searchPath + QString::fromStdWString(fileNames[i]);

                if (this->m_presenter.openSubtitle(subtitleFilePath))
                {
                    opened = true;
                    break;
                }
            }
        }

        if (opened)
            break;
    }
}

void MediaPlayer::retreiveLyrics(const QString &filePath)
{
    SubtitleImpl subtitle;
    wstring ret;

    if (!this->isEnableSearchLyrics())
        return;

    if (subtitle.getLyrics(filePath.toStdWString(), &ret))
    {
        QString tmpFilePath = QDir::tempPath();

        Utils::appendDirSeparator(&tmpFilePath);
        tmpFilePath += "XXXXXX";
        tmpFilePath += ".lrc";

        QTemporaryFile tmpFile(tmpFilePath);

        if (tmpFile.open())
        {
            QFile file(tmpFile.fileName());

            if (file.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                QTextStream stream(&file);

                stream.setCodec("UTF-8");
                stream.setGenerateByteOrderMark(true);

                stream << QString::fromStdWString(ret);

                file.close();
                tmpFile.close();

                this->openSubtitle(tmpFile.fileName(), false);
            }
        }
    }
}

void MediaPlayer::retreiveSubtitleURL(const QString &filePath)
{
    SubtitleImpl subtitle;
    QFileInfo info(filePath);
    string url;

    if (!this->isEnableSearchSubtitle())
        return;

    if (subtitle.existSubtitle(filePath.toStdWString(), info.fileName().toLocal8Bit().constData(), &url))
        this->m_presenter.setSubtitleURL(QString::fromStdString(url));
}

void MediaPlayer::retreiveExternalSubtitle(const QString &filePath)
{
    if (!this->m_presenter.existExternalSubtitle())
    {
        if (this->m_presenter.isRemoteFile())
        {
            char proto[strlen(ANYVOD_PROTOCOL_NAME) + 1];
            char path[MAX_FILEPATH_CHAR_SIZE];
            QString realPath = Utils::removeFFMpegSeparator(filePath);

            av_url_split(proto, sizeof(proto), NULL, 0, NULL, 0, NULL, path, sizeof(path), realPath.toUtf8().constData());
            this->m_presenter.openRemoteSubtitle(QString::fromUtf8(path));

            if (!this->existSubtitle() && !this->m_presenter.isAudio() && this->isEnableSearchSubtitle())
            {
                QString url;

                Socket::getInstance().existSubtitleURL(QString::fromUtf8(path), true, NULL, &url);
                this->m_presenter.setSubtitleURL(url);
            }
        }
        else if (this->m_presenter.isRemoteProtocol() && !Utils::determinDevice(filePath))
        {
            if (!this->m_playData.userData.isEmpty())
                this->m_presenter.openYouTube(this->m_playData.userData);
        }
        else if (!Utils::determinDevice(filePath))
        {
            if (this->m_presenter.isAudio())
                this->retreiveLyrics(filePath);
            else
                this->retreiveSubtitleURL(filePath);
        }
    }
}

bool MediaPlayer::open(const QString &filePath, const QString &title, const ExtraPlayData &data, const QUuid &unique,
                       const QString &fontFamily, const int fontSize, const int subtitleOutlineSize, const QString &audioPath)
{
    bool success = false;

    success = this->m_presenter.open(filePath, title, data, fontFamily, fontSize, subtitleOutlineSize, audioPath);

    if (success)
    {
        this->m_filePath = filePath;
        this->m_audioPath = audioPath;
        this->m_playData = data;
        this->m_unique = unique;
        this->m_title = title;

        if (data.userData.isEmpty())
            this->m_lastPlayPath = filePath;
        else
            this->m_lastPlayPath = data.userData;

        this->m_fontFamily = fontFamily;
        this->m_fontSize = fontSize;
        this->m_subtitleOutlineSize = subtitleOutlineSize;

        if (!this->m_presenter.isRemoteFile() && !this->m_presenter.isRemoteProtocol())
            this->tryOpenSubtitle(Utils::adjustNetworkPath(filePath));

        this->retreiveExternalSubtitle(filePath);
    }

    return success;
}

void MediaPlayer::showDetail(bool show)
{
    this->m_presenter.showDetail(show);
}

bool MediaPlayer::isShowDetail() const
{
    return this->m_presenter.isShowDetail();
}

const MediaPresenter::Detail& MediaPlayer::getDetail() const
{
    return this->m_presenter.getDetail();
}

void MediaPlayer::showSubtitle(bool show)
{
    this->showSubtitle(show, false);
}

void MediaPlayer::showSubtitle(bool show, bool raw)
{
    this->m_presenter.showSubtitle(show);

    if (raw)
        return;

    QString desc;

    if (this->isMovieSubtitleVisiable())
    {
        if (show)
            desc = trUtf8("자막 보이기");
        else
            desc = trUtf8("자막 숨기기");
    }
    else
    {
        if (show)
            desc = trUtf8("가사 보이기");
        else
            desc = trUtf8("가사 숨기기");
    }

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isShowSubtitle() const
{
    return this->m_presenter.isShowSubtitle();
}

void MediaPlayer::setSearchSubtitleComplex(bool use)
{
    this->m_useSearchSubtitleComplex = use;

    QString desc;

    if (this->isMovieSubtitleVisiable())
    {
        if (use)
            desc = trUtf8("고급 자막 검색 사용");
        else
            desc = trUtf8("고급 자막 검색 사용 안 함");
    }
    else
    {
        if (use)
            desc = trUtf8("고급 가사 검색 사용");
        else
            desc = trUtf8("고급 가사 검색 사용 안 함");
    }

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::getSearchSubtitleComplex() const
{
    return this->m_useSearchSubtitleComplex;
}

bool MediaPlayer::existSubtitle()
{
    return this->m_presenter.existSubtitle();
}

bool MediaPlayer::existFileSubtitle()
{
    return this->m_presenter.existFileSubtitle();
}

bool MediaPlayer::existExternalSubtitle()
{
    return this->m_presenter.existExternalSubtitle();
}

bool MediaPlayer::existAudioSubtitle()
{
    return this->m_presenter.existAudioSubtitle();
}

bool MediaPlayer::existAudioSubtitleGender()
{
    return this->m_presenter.existAudioSubtitleGender();
}

void MediaPlayer::getSubtitleClasses(QStringList *classNames)
{
    this->m_presenter.getSubtitleClasses(classNames);
}

void MediaPlayer::getCurrentSubtitleClass(QString *className)
{
    this->m_presenter.getCurrentSubtitleClass(className);
}

bool MediaPlayer::setCurrentSubtitleClass(const QString &className)
{
    QString desc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 언어 변경");
    else
        desc = trUtf8("가사 언어 변경");

    desc += QString(" (%1)").arg(className);

    bool success = this->m_presenter.setCurrentSubtitleClass(className);

    this->m_presenter.showOptionDesc(desc);

    return success;
}

void MediaPlayer::resetSubtitlePosition()
{
    this->m_presenter.resetSubtitlePosition();
    this->m_presenter.showOptionDesc(trUtf8("자막 위치 초기화"));
}

void MediaPlayer::setVerticalSubtitlePosition(int pos)
{
    QString desc;

    this->m_presenter.setVerticalSubtitlePosition(pos);

    if (pos > 0)
        desc = trUtf8("자막 위치 위로 : %1");
    else
        desc = trUtf8("자막 위치 아래로 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getVerticalSubtitlePosition()));
}

void MediaPlayer::setHorizontalSubtitlePosition(int pos)
{
    QString desc;

    this->m_presenter.setHorizontalSubtitlePosition(pos);

    if (pos > 0)
        desc = trUtf8("자막 위치 왼쪽으로 : %1");
    else
        desc = trUtf8("자막 위치 오른쪽으로 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getHorizontalSubtitlePosition()));
}

int MediaPlayer::getVerticalSubtitlePosition()
{
    return this->m_presenter.getVerticalSubtitlePosition();
}

int MediaPlayer::getHorizontalSubtitlePosition()
{
    return this->m_presenter.getHorizontalSubtitlePosition();
}

void MediaPlayer::reset3DSubtitleOffset()
{
    this->m_presenter.reset3DSubtitleOffset();
    this->m_presenter.showOptionDesc(trUtf8("3D 자막 위치 초기화"));
}

void MediaPlayer::setVertical3DSubtitleOffset(int pos)
{
    QString desc;

    this->m_presenter.setVertical3DSubtitleOffset(pos);

    if (pos > 0)
        desc = trUtf8("3D 자막 위치 가깝게(세로) : %1");
    else
        desc = trUtf8("3D 자막 위치 멀게(세로) : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getVertical3DSubtitleOffset()));
}

void MediaPlayer::setHorizontal3DSubtitleOffset(int pos)
{
    QString desc;

    this->m_presenter.setHorizontal3DSubtitleOffset(pos);

    if (pos > 0)
        desc = trUtf8("3D 자막 위치 가깝게(가로) : %1");
    else
        desc = trUtf8("3D 자막 위치 멀게(가로) : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getHorizontal3DSubtitleOffset()));
}

int MediaPlayer::getVertical3DSubtitleOffset()
{
    return this->m_presenter.getVertical3DSubtitleOffset();
}

int MediaPlayer::getHorizontal3DSubtitleOffset()
{
    return this->m_presenter.getHorizontal3DSubtitleOffset();
}

void MediaPlayer::setRepeatStart(double start)
{
    QString desc;
    QString time;

    Utils::getTimeString(start, Utils::TIME_HH_MM_SS_ZZZ, &time);

    desc = trUtf8("구간 반복 시작 : %1").arg(time);

    this->m_presenter.setRepeatStart(start);
    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::setRepeatEnd(double end)
{
    QString desc;
    QString time;

    Utils::getTimeString(end, Utils::TIME_HH_MM_SS_ZZZ, &time);

    desc = trUtf8("구간 반복 끝 : %1").arg(time);

    this->m_presenter.setRepeatEnd(end);
    this->m_presenter.setRepeatEnable(true);

    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::setRepeatEnable(bool enable)
{
    QString desc;
    QString en;
    QString start;
    QString end;

    if (enable)
        en = trUtf8("구간 반복 활성화");
    else
        en = trUtf8("구간 반복 비활성화");

    Utils::getTimeString(this->getRepeatStart(), Utils::TIME_HH_MM_SS_ZZZ, &start);
    Utils::getTimeString(this->getRepeatEnd(), Utils::TIME_HH_MM_SS_ZZZ, &end);

    if (start == end && enable)
        desc = trUtf8("시작과 끝 시각이 같으므로 활성화 되지 않습니다");
    else
        desc = QString("%1 : %2 ~ %3").arg(en).arg(start).arg(end);

    this->m_presenter.setRepeatEnable(enable);
    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::getRepeatEnable() const
{
    return this->m_presenter.getRepeatEnable();
}

double MediaPlayer::getRepeatStart() const
{
    return this->m_presenter.getRepeatStart();
}

double MediaPlayer::getRepeatEnd() const
{
    return this->m_presenter.getRepeatEnd();
}

void MediaPlayer::setRepeatStartMove(double offset)
{
    QString desc;

    if (this->getRepeatStart() != this->getRepeatEnd())
    {
        double moveToTime = this->getRepeatStart() + offset;

        if (moveToTime >= 0.0 && moveToTime < this->getRepeatEnd())
        {
            QString time;

            Utils::getTimeString(moveToTime, Utils::TIME_HH_MM_SS_ZZZ, &time);

            if (offset < 0.0)
                desc = trUtf8("구간 반복 시작 위치 %1초 뒤로 이동 (%2)").arg(fabs(offset)).arg(time);
            else
                desc = trUtf8("구간 반복 시작 위치 %1초 앞으로 이동 (%2)").arg(offset).arg(time);

            this->m_presenter.setRepeatStart(moveToTime);
        }
        else
        {
            desc = trUtf8("구간 반복 시작 위치가 범위를 벗어났습니다");
        }
    }
    else
    {
        desc = trUtf8("구간 반복이 설정 되지 않았습니다");
    }

    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::setRepeatEndMove(double offset)
{
    QString desc;

    if (this->getRepeatStart() != this->getRepeatEnd())
    {
        double moveToTime = this->getRepeatEnd() + offset;

        if (moveToTime > this->getRepeatStart() && moveToTime <= this->getDuration())
        {
            QString time;

            Utils::getTimeString(moveToTime, Utils::TIME_HH_MM_SS_ZZZ, &time);

            if (offset < 0.0)
                desc = trUtf8("구간 반복 끝 위치 %1초 뒤로 이동 (%2)").arg(fabs(offset)).arg(time);
            else
                desc = trUtf8("구간 반복 끝 위치 %1초 앞으로 이동 (%2)").arg(offset).arg(time);

            this->m_presenter.setRepeatEnd(moveToTime);
        }
        else
        {
            desc = trUtf8("구간 반복 끝 위치가 범위를 벗어났습니다");
        }
    }
    else
    {
        desc = trUtf8("구간 반복이 설정 되지 않았습니다");
    }

    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::setRepeatMove(double offset)
{
    QString desc;

    if (this->getRepeatStart() != this->getRepeatEnd())
    {
        double moveToStartTime = this->getRepeatStart() + offset;
        double moveToEndTime = this->getRepeatEnd() + offset;

        if ((moveToStartTime >= 0.0 && moveToStartTime < this->getRepeatEnd()) &&
                (moveToEndTime > this->getRepeatStart() && moveToEndTime <= this->getDuration()))
        {
            QString startTime;
            QString endTime;

            Utils::getTimeString(moveToStartTime, Utils::TIME_HH_MM_SS_ZZZ, &startTime);
            Utils::getTimeString(moveToEndTime, Utils::TIME_HH_MM_SS_ZZZ, &endTime);

            if (offset < 0.0)
                desc = trUtf8("구간 반복 위치 %1초 뒤로 이동 (%2 ~ %3)").arg(fabs(offset)).arg(startTime).arg(endTime);
            else
                desc = trUtf8("구간 반복 위치 %1초 앞으로 이동 (%2 ~ %3)").arg(offset).arg(startTime).arg(endTime);

            this->m_presenter.setRepeatStart(moveToStartTime);
            this->m_presenter.setRepeatEnd(moveToEndTime);
        }
        else
        {
            desc = trUtf8("구간 반복 위치가 범위를 벗어났습니다");
        }
    }
    else
    {
        desc = trUtf8("구간 반복이 설정 되지 않았습니다");
    }

    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::setSeekKeyFrame(bool keyFrame)
{
    QString desc;

    if (keyFrame)
        desc = trUtf8("키프레임 단위로 이동 함");
    else
        desc = trUtf8("키프레임 단위로 이동 안 함");

    this->m_presenter.setSeekKeyFrame(keyFrame);
    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isSeekKeyFrame() const
{
    return this->m_presenter.isSeekKeyFrame();
}

void MediaPlayer::setSubtitleDirectory(const QStringList &paths, bool prior)
{
    this->m_subtitleDirectory = paths;
    this->m_priorSubtitleDirectory = prior;
}

void MediaPlayer::getSubtitleDirectory(QStringList *path, bool *prior) const
{
    *path = this->m_subtitleDirectory;
    *prior = this->m_priorSubtitleDirectory;
}

void MediaPlayer::set3DMethod(AnyVODEnums::Video3DMethod method)
{
    QString methodDesc = Utils::getMainWindow()->get3DMethodDesc(method);

    this->m_presenter.set3DMethod(method);
    this->m_presenter.showOptionDesc(trUtf8("3D 영상 (%1)").arg(methodDesc));
}

AnyVODEnums::Video3DMethod MediaPlayer::get3DMethod() const
{
    return this->m_presenter.get3DMethod();
}

void MediaPlayer::setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method)
{
    QString methodDesc = Utils::getMainWindow()->getSubtitle3DMethodDesc(method);

    this->m_presenter.setSubtitle3DMethod(method);
    this->m_presenter.showOptionDesc(trUtf8("3D 자막 (%1)").arg(methodDesc));
}

AnyVODEnums::Subtitle3DMethod MediaPlayer::getSubtitle3DMethod() const
{
    return this->m_presenter.getSubtitle3DMethod();
}

void MediaPlayer::setSkipRanges(const QVector<MediaPresenter::Range> &ranges)
{
    this->m_presenter.setSkipRanges(ranges);
}

void MediaPlayer::getSkipRanges(QVector<MediaPresenter::Range> *ret) const
{
    this->m_presenter.getSkipRanges(ret);
}

void MediaPlayer::setSkipOpening(bool skip)
{
    QString desc;
    QString enable;
    QString time;

    Utils::getTimeString(this->m_presenter.getOpeningSkipTime(), Utils::TIME_HH_MM_SS, &time);

    if (skip)
        enable = trUtf8("오프닝 스킵 사용 함");
    else
        enable = trUtf8("오프닝 스킵 사용 안 함");

    desc = QString("%1 : %2").arg(enable).arg(time);

    this->m_presenter.setSkipOpening(skip);
    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::getSkipOpening() const
{
    return this->m_presenter.getSkipOpening();
}

void MediaPlayer::setSkipEnding(bool skip)
{
    QString desc;
    QString enable;
    QString time;

    Utils::getTimeString(this->m_presenter.getEndingSkipTime(), Utils::TIME_HH_MM_SS, &time);

    if (skip)
        enable = trUtf8("엔딩 스킵 사용 함");
    else
        enable = trUtf8("엔딩 스킵 사용 안 함");

    desc = QString("%1 : %2").arg(enable).arg(time);

    this->m_presenter.setSkipEnding(skip);
    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::getSkipEnding() const
{
    return this->m_presenter.getSkipEnding();
}

void MediaPlayer::setUseSkipRange(bool use)
{
    QString desc;

    if (use)
        desc = trUtf8("재생 스킵 사용 함");
    else
        desc = trUtf8("재생 스킵 사용 안 함");

    this->m_presenter.setUseSkipRange(use);
    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::getUseSkipRange() const
{
    return this->m_presenter.getUseSkipRange();
}

void MediaPlayer::useNormalizer(bool use)
{
    if (this->isUseSPDIF())
        return;

    this->m_presenter.useNormalizer(use);

    QString desc;

    if (use)
        desc = trUtf8("노멀라이저 켜짐");
    else
        desc = trUtf8("노멀라이저 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUsingNormalizer() const
{
    return this->m_presenter.isUsingNormalizer();
}

void MediaPlayer::useEqualizer(bool use)
{
    if (this->isUseSPDIF())
        return;

    this->m_presenter.useEqualizer(use);

    QString desc;

    if (use)
        desc = trUtf8("이퀄라이저 켜짐");
    else
        desc = trUtf8("이퀄라이저 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUsingEqualizer() const
{
    return this->m_presenter.isUsingEqualizer();
}

void MediaPlayer::useLowerMusic(bool use)
{
    if (this->isUseSPDIF())
        return;

    this->m_presenter.useLowerMusic(use);

    QString desc;

    if (use)
        desc = trUtf8("음악 줄임 켜짐");
    else
        desc = trUtf8("음악 줄임 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUsingLowerMusic() const
{
    return this->m_presenter.isUsingLowerMusic();
}

void MediaPlayer::addSubtitleOpaque(float inc)
{
    float opaque = this->m_presenter.getSubtitleOpaque() + inc;
    QString desc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 투명도");
    else
        desc = trUtf8("가사 투명도");

    opaque = Utils::zeroDouble(opaque);

    if (opaque < 0.0f)
        opaque = 0.0f;

    if (opaque > 1.0f)
        opaque = 1.0f;

    this->m_presenter.setSubtitleOpaque(opaque);
    this->m_presenter.showOptionDesc(QString("%1 (%2%)").arg(desc).arg((int)(ceil(opaque * 100))));
}

float MediaPlayer::getSubtitleOpaque() const
{
    return this->m_presenter.getSubtitleOpaque();
}

void MediaPlayer::resetSubtitleOpaque()
{
    QString desc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 투명도 초기화");
    else
        desc = trUtf8("가사 투명도 초기화");

    this->m_presenter.setSubtitleOpaque(1.0f);
    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::addSubtitleSize(float inc)
{
    float size = this->m_presenter.getSubtitleSize() + inc;
    QString desc = trUtf8("자막 크기");

    size = Utils::zeroDouble(size);

    if (size < 0.0f)
        size = 0.0f;

    this->m_presenter.setSubtitleSize(size);
    this->m_presenter.showOptionDesc(QString("%1 (%2%)").arg(desc).arg((int)(ceil(size * 100))));
}

float MediaPlayer::getSubtitleSize() const
{
    return this->m_presenter.getSubtitleSize();
}

void MediaPlayer::setSubtitleSize(float size)
{
    this->m_presenter.setSubtitleSize(size);
}

void MediaPlayer::resetSubtitleSize()
{
    QString desc = trUtf8("자막 크기 초기화");

    this->m_presenter.setSubtitleSize(1.0f);
    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::useHWDecoder(bool enable)
{
    QString desc;

    this->m_presenter.useHWDecoder(enable);

    if (enable)
    {
        desc = trUtf8("하드웨어 디코더 사용 함");

        if (!this->m_presenter.isOpenedHWDecoder())
            desc += trUtf8(" (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)");
    }
    else
    {
        desc = trUtf8("하드웨어 디코더 사용 안 함");
    }

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUseHWDecoder() const
{
    return this->m_presenter.isUseHWDecoder();
}

void MediaPlayer::useFrameDrop(bool enable)
{
    QString desc;

    this->m_presenter.useFrameDrop(enable);

    if (enable)
        desc = trUtf8("프레임 드랍 사용 함");
    else
        desc = trUtf8("프레임 드랍 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUseFrameDrop() const
{
    return this->m_presenter.isUseFrameDrop();
}

void MediaPlayer::useBufferingMode(bool enable)
{
    QString desc;

    this->m_presenter.useBufferingMode(enable);

    if (enable)
        desc = trUtf8("버퍼링 모드 사용 함");
    else
        desc = trUtf8("버퍼링 모드 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUseBufferingMode() const
{
    return this->m_presenter.isUseBufferingMode();
}

void MediaPlayer::useSPDIF(bool enable)
{
    this->m_presenter.useSPDIF(enable);

    QString desc;

    if (this->isUseSPDIF() == enable)
    {
        if (enable)
            desc = trUtf8("S/PDIF 출력 사용");
        else
            desc = trUtf8("S/PDIF 출력 사용 안 함");
    }
    else
    {
        desc = trUtf8("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");
    }

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUseSPDIF() const
{
    return this->m_presenter.isUseSPDIF();
}

bool MediaPlayer::isOpenedSPDIF() const
{
    return this->m_presenter.isOpenedSPDIF();
}

bool MediaPlayer::isSPDIFAvailable() const
{
    return this->m_presenter.isSPDIFAvailable();
}

void MediaPlayer::setSPDIFEncodingMethod(AnyVODEnums::SPDIFEncodingMethod method)
{
    QString desc;

    this->m_presenter.setSPDIFEncodingMethod(method);

    if (this->getSPDIFEncodingMethod() == method)
    {
        switch (method)
        {
            case AnyVODEnums::SEM_NONE:
                desc = trUtf8("S/PDIF 출력 시 인코딩 사용 안 함");
                break;
            case AnyVODEnums::SEM_AC3:
                desc = trUtf8("S/PDIF 출력 시 AC3 인코딩 사용");
                break;
            case AnyVODEnums::SEM_DTS:
                desc = trUtf8("S/PDIF 출력 시 DTS 인코딩 사용");
                break;
            default:
                break;
        }
    }
    else
    {
        desc = trUtf8("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");
    }

    this->m_presenter.showOptionDesc(desc);
}

AnyVODEnums::SPDIFEncodingMethod MediaPlayer::getSPDIFEncodingMethod() const
{
    return this->m_presenter.getSPDIFEncodingMethod();
}

void MediaPlayer::use3DFull(bool enable)
{
    this->m_presenter.use3DFull(enable);

    QString desc;

    if (enable)
        desc = trUtf8("3D 전체 해상도 사용");
    else
        desc = trUtf8("3D 전체 해상도 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUse3DFull() const
{
    return this->m_presenter.isUse3DFull();
}

void MediaPlayer::usePBO(bool enable)
{
    this->m_presenter.usePBO(enable);

    QString desc;

    if (enable)
        desc = trUtf8("고속 렌더링 사용");
    else
        desc = trUtf8("고속 렌더링 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUsePBO() const
{
    return this->m_presenter.isUsePBO();
}

bool MediaPlayer::isUsablePBO() const
{
    return this->m_presenter.isUsablePBO();
}

void MediaPlayer::setUserSPDIFSampleRate(int sampleRate)
{
    bool useSPDIF = this->isUseSPDIF();
    QString desc;

    this->m_presenter.setUserSPDIFSampleRate(sampleRate);

    if (this->isUseSPDIF() == useSPDIF)
    {
        desc = trUtf8("S/PDIF 샘플 속도 (%1)");

        if (sampleRate == 0)
            desc = desc.arg(trUtf8("기본 속도"));
        else
            desc = desc.arg(QString("%1kHz").arg(sampleRate / 1000.0f, 0, 'f', 1));
    }
    else
    {
        desc = trUtf8("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");
    }

    this->m_presenter.showOptionDesc(desc);
}

int MediaPlayer::getUserSPDIFSampleRate() const
{
    return this->m_presenter.getUserSPDIFSampleRate();
}

void MediaPlayer::useLowerVoice(bool use)
{
    if (this->isUseSPDIF())
        return;

    this->m_presenter.useLowerVoice(use);

    QString desc;

    if (use)
        desc = trUtf8("음성 줄임 켜짐");
    else
        desc = trUtf8("음성 줄임 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUsingLowerVoice() const
{
    return this->m_presenter.isUsingLowerVoice();
}

void MediaPlayer::useHigherVoice(bool use)
{
    if (this->isUseSPDIF())
        return;

    this->m_presenter.useHigherVoice(use);

    QString desc;

    if (use)
        desc = trUtf8("음성 강조 켜짐");
    else
        desc = trUtf8("음성 강조 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isUsingHigherVoice() const
{
    return this->m_presenter.isUsingHigherVoice();
}

bool MediaPlayer::setPreAmp(float dB)
{
    return this->m_presenter.setPreAmp(dB);
}

float MediaPlayer::getPreAmp() const
{
    return this->m_presenter.getPreAmp();
}

bool MediaPlayer::setEqualizerGain(int band, float gain)
{
    return this->m_presenter.setEqualizerGain(band, gain);
}

float MediaPlayer::getEqualizerGain(int band) const
{
    return this->m_presenter.getEqualizerGain(band);
}

int MediaPlayer::getBandCount() const
{
    return this->m_presenter.getBandCount();
}

void MediaPlayer::close()
{
    this->updateLastPlay();

    this->m_presenter.close();
    this->m_status = Stopped;
    this->m_filePath.clear();
    this->m_audioPath.clear();
    this->m_unique = QUuid();

    this->callChanged();
}

bool MediaPlayer::resetScreen(const int width, const int height, TextureInfo *texInfo, bool inContext)
{
    return this->m_presenter.resetScreen(width, height, texInfo, inContext);
}

bool MediaPlayer::play()
{
    bool success = false;

    success = this->m_presenter.play();

    this->m_status = Started;
    this->callChanged();

    if (success)
        this->m_status = Playing;
    else
        this->m_status = Stopped;

    this->callChanged();

    if (success)
    {
        QString desc;

        if (this->m_playData.userData.isEmpty())
            desc = QFileInfo(Utils::removeFFMpegSeparator(this->m_filePath)).fileName();
        else
            desc = this->m_title;

        desc += " (";

        if (this->isMovieSubtitleVisiable())
        {
            if (this->m_presenter.existSubtitle())
                desc += trUtf8("자막 있음");
            else
                desc += trUtf8("자막 없음");
        }
        else
        {
            if (this->m_presenter.existSubtitle())
                desc += trUtf8("가사 있음");
            else
                desc += trUtf8("가사 없음");
        }

        desc += ")";

        this->m_presenter.showOptionDesc(desc);
    }

    if (this->m_gotoLastPlay)
    {
        double lastPos = this->m_lastPlay.get(Utils::removeFFMpegSeparator(this->m_lastPlayPath));

        if (lastPos > 0.0)
            this->seek(lastPos, true);
    }

    return success;
}

void MediaPlayer::pause()
{
    this->pause(false);
}

void MediaPlayer::resume()
{
    this->resume(false);
}

void MediaPlayer::pause(bool useRaw)
{
    if (useRaw)
    {
        this->m_presenter.pause();
    }
    else
    {
        if (this->m_presenter.isRunning())
        {
            this->m_presenter.pause();
            this->m_status = Paused;
        }
        else
        {
            this->m_status = Stopped;
        }

        this->callChanged();

        this->m_presenter.showOptionDesc(trUtf8("일시정지"));
    }
}

void MediaPlayer::resume(bool useRaw)
{
    if (useRaw)
    {
        this->m_presenter.resume();
    }
    else
    {
        if (this->m_presenter.isRunning())
        {
            this->m_presenter.resume();
            this->m_status = Playing;
        }
        else
        {
            this->m_status = Stopped;
        }

        this->callChanged();

        this->m_presenter.showOptionDesc(trUtf8("재생"));
    }
}

bool MediaPlayer::recover()
{
    return this->m_presenter.recover(this->m_presenter.getCurrentPosition());
}

void MediaPlayer::stop()
{
    this->updateLastPlay();

    this->m_presenter.stop();
    this->m_status = Stopped;

    this->callChanged();
}

void MediaPlayer::toggle()
{
    if (this->m_status == Paused || this->m_status == Ended)
        this->resume();
    else if (this->m_status == Playing)
        this->pause();
}

bool MediaPlayer::isPlayUserDataEmpty() const
{
    return this->m_playData.userData.isEmpty();
}

QString MediaPlayer::getTitle() const
{
    return this->m_title;
}

void MediaPlayer::updateLastPlay()
{
    if (this->m_gotoLastPlay)
    {
        if (this->m_status != Stopped && this->m_status != Ended)
            this->m_lastPlay.update(Utils::removeFFMpegSeparator(this->m_lastPlayPath), this->getCurrentPosition());
    }
    else
    {
        this->m_lastPlay.clear(Utils::removeFFMpegSeparator(this->m_lastPlayPath));
    }
}

bool MediaPlayer::isPlayOrPause() const
{
    return this->m_status == Playing || this->m_status == Paused;
}

bool MediaPlayer::isRemoteFile() const
{
    return this->m_presenter.isRemoteFile();
}

void MediaPlayer::prevFrame(int count)
{
    this->m_presenter.prevFrame(count);

    if (this->m_status != Paused)
    {
        this->m_status = Paused;
        this->callChanged();
    }

    QString desc = trUtf8("이전으로 %1 프레임 이동").arg(count);

    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::nextFrame(int count)
{
    this->m_presenter.nextFrame(count);

    if (this->m_status != Paused)
    {
        this->m_status = Paused;
        this->callChanged();
    }

    QString desc = trUtf8("다음으로 %1 프레임 이동").arg(count);

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::canMoveChapter() const
{
    return this->isPlayOrPause() && this->hasDuration() && this->getChapters().count() > 0;
}

bool MediaPlayer::canMoveFrame() const
{
    return this->isVideo() && this->hasDuration();
}

bool MediaPlayer::isTempoUsable() const
{
    return this->m_presenter.isTempoUsable();
}

float MediaPlayer::getTempo() const
{
    return this->m_presenter.getTempo();
}

void MediaPlayer::setTempo(float percent)
{
    QString desc;

    if (Utils::zeroDouble(percent) == 0.0)
    {
        percent = 0.0f;
        desc = trUtf8("재생 속도 초기화");
    }
    else
    {
        desc = trUtf8("재생 속도 : %1배").arg(1.0f + percent / 100.0f);
    }

    this->m_presenter.setTempo(percent);
    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::setUserAspectRatio(MediaPresenter::UserAspectRatio &ratio)
{
    QString desc;

    if (ratio.use)
    {
        desc = trUtf8("화면 비율 사용 함");

        if (ratio.fullscreen)
            desc += trUtf8(" (화면 채우기)");
        else
            desc += QString(" (%1:%2)").arg(ratio.width).arg(ratio.height);
    }
    else
    {
        desc = trUtf8("화면 비율 사용 안 함");
    }

    this->m_presenter.setUserAspectRatio(ratio);
    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::getUserAspectRatio(MediaPresenter::UserAspectRatio *ret) const
{
    this->m_presenter.getUserAspectRatio(ret);
}

bool MediaPlayer::render(ShaderCompositer &shader)
{
    return this->m_presenter.render(shader);
}

void MediaPlayer::getGOMSubtitleURL(QString *ret) const
{
    this->m_presenter.getSubtitleURL(ret);
}

int MediaPlayer::getCurrentAudioStreamIndex() const
{
    return this->m_presenter.getCurrentAudioStreamIndex();
}

void MediaPlayer::getAudioStreamInfo(QVector<AudioStreamInfo> *ret) const
{
    this->m_presenter.getAudioStreamInfo(ret);
}

bool MediaPlayer::changeAudioStream(const int index)
{
    QVector<AudioStreamInfo> info;
    QString desc;

    this->m_presenter.getAudioStreamInfo(&info);

    for (int i = 0; i < info.count(); i++)
    {
        if (info[i].index == index)
        {
            desc = info[i].name;
            break;
        }
    }

    bool success = this->m_presenter.changeAudioStream(index);

    this->m_presenter.showOptionDesc(trUtf8("음성 변경 (%1)").arg(desc));

    return success;
}

HSTREAM MediaPlayer::getAudioHandle() const
{
    return this->m_presenter.getAudioHandle();
}

void MediaPlayer::setDeinterlaceMethod(AnyVODEnums::DeinterlaceMethod method)
{
    QString methodDesc;

    this->m_presenter.getDeinterlacer().setMethod(method);

    switch (method)
    {
        case AnyVODEnums::DM_AUTO:
            methodDesc = trUtf8("자동 판단");
            break;
        case AnyVODEnums::DM_USE:
            methodDesc = trUtf8("항상 사용");
            break;
        case AnyVODEnums::DM_NOUSE:
            methodDesc = trUtf8("사용 안 함");
            break;
        default:
            break;
    }

    this->m_presenter.showOptionDesc(trUtf8("디인터레이스 (%1)").arg(methodDesc));
}

void MediaPlayer::setDeinterlaceAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm)
{
    QString algorithmDesc = Utils::getMainWindow()->getDeinterlaceDesc(algorithm);

    this->m_presenter.setDeinterlacerAlgorithm(algorithm);
    this->m_presenter.showOptionDesc(trUtf8("디인터레이스 알고리즘 (%1)").arg(algorithmDesc));
}

AnyVODEnums::DeinterlaceMethod MediaPlayer::getDeinterlaceMethod()
{
    return this->m_presenter.getDeinterlacer().getMethod();
}

AnyVODEnums::DeinterlaceAlgorithm MediaPlayer::getDeinterlaceAlgorithm()
{
    return this->m_presenter.getDeinterlacer().getAlgorithm();
}

void MediaPlayer::setHAlign(AnyVODEnums::HAlignMethod align)
{
    QString desc;
    QString alignDesc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 가로 정렬 변경");
    else
        desc = trUtf8("가사 가로 정렬 변경");

    switch (align)
    {
        case AnyVODEnums::HAM_AUTO:
            alignDesc = trUtf8("자동 정렬");
            break;
        case AnyVODEnums::HAM_LEFT:
            alignDesc = trUtf8("왼쪽 정렬");
            break;
        case AnyVODEnums::HAM_RIGHT:
            alignDesc = trUtf8("오른쪽 정렬");
            break;
        case AnyVODEnums::HAM_MIDDLE:
            alignDesc = trUtf8("가운데 정렬");
            break;
        case AnyVODEnums::HAM_NONE:
            alignDesc = trUtf8("기본 정렬");
            break;
        default:
            break;
    }

    desc += QString(" (%1)").arg(alignDesc);

    this->m_presenter.setHAlign(align);
    this->m_presenter.showOptionDesc(desc);
}

AnyVODEnums::HAlignMethod MediaPlayer::getHAlign() const
{
    return this->m_presenter.getHAlign();
}

void MediaPlayer::setVAlign(AnyVODEnums::VAlignMethod align)
{
    QString desc;
    QString alignDesc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 세로 정렬 변경");
    else
        desc = trUtf8("가사 세로 정렬 변경");

    switch (align)
    {
        case AnyVODEnums::VAM_TOP:
            alignDesc = trUtf8("상단 정렬");
            break;
        case AnyVODEnums::VAM_BOTTOM:
            alignDesc = trUtf8("하단 정렬");
            break;
        case AnyVODEnums::VAM_NONE:
            alignDesc = trUtf8("기본 정렬");
            break;
        default:
            break;
    }

    desc += QString(" (%1)").arg(alignDesc);

    this->m_presenter.setVAlign(align);
    this->m_presenter.showOptionDesc(desc);
}

AnyVODEnums::VAlignMethod MediaPlayer::getVAlign() const
{
    return this->m_presenter.getVAlign();
}

void MediaPlayer::prevSubtitleSync(double amount)
{
    this->subtitleSync(-amount);
}

void MediaPlayer::nextSubtitleSync(double amount)
{
    this->subtitleSync(amount);
}

void MediaPlayer::resetSubtitleSync()
{
    QString desc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 싱크 초기화");
    else
        desc = trUtf8("가사 싱크 초기화");

    this->m_presenter.setSubtitleSync(0.0);
    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::subtitleSync(double value)
{
    double current = this->m_presenter.getSubtitleSync();
    double target = value + current;

    this->m_presenter.setSubtitleSync(target);

    QString desc;

    if (this->isMovieSubtitleVisiable())
    {
        if (value > 0.0)
            desc = trUtf8("자막 싱크 %1초 빠르게");
        else
            desc = trUtf8("자막 싱크 %1초 느리게");
    }
    else
    {
        if (value > 0.0)
            desc = trUtf8("가사 싱크 %1초 빠르게");
        else
            desc = trUtf8("가사 싱크 %1초 느리게");
    }

    desc = desc.arg(fabs(value));

    target = Utils::zeroDouble(target);
    desc += trUtf8(" (%1초)").arg(target);

    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::prevAudioSync(double amount)
{
    this->audioSync(-amount);
}

void MediaPlayer::nextAudioSync(double amount)
{
    this->audioSync(amount);
}

void MediaPlayer::resetAudioSync()
{
    QString desc;

    desc = trUtf8("소리 싱크 초기화");

    this->m_presenter.audioSync(0.0);
    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isEnableSearchSubtitle() const
{
    return this->m_presenter.isEnableSearchSubtitle();
}

bool MediaPlayer::isEnableSearchLyrics() const
{
    return this->m_presenter.isEnableSearchLyrics();
}

void MediaPlayer::enableSearchSubtitle(bool enable)
{
    QString desc;

    if (enable)
        desc = trUtf8("자막 찾기 켜짐");
    else
        desc = trUtf8("자막 찾기 꺼짐");

    this->m_presenter.enableSearchSubtitle(enable);
    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::enableSearchLyrics(bool enable)
{
    QString desc;

    if (enable)
        desc = trUtf8("가사 찾기 켜짐");
    else
        desc = trUtf8("가사 찾기 꺼짐");

    this->m_presenter.enableSearchLyrics(enable);
    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::audioSync(double value)
{
    double current = this->m_presenter.getAudioSync();
    double target = value + current;

    this->m_presenter.audioSync(target);

    QString desc;

    if (value > 0.0)
        desc = trUtf8("소리 싱크 %1초 빠르게");
    else
        desc = trUtf8("소리 싱크 %1초 느리게");

    desc = desc.arg(fabs(value));

    target = Utils::zeroDouble(target);
    desc += trUtf8(" (%1초)").arg(target);

    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::seek(double time, bool any)
{
    this->m_presenter.seek(time, any);

    double current = this->m_presenter.getCurrentPosition();
    double max = this->m_presenter.getDuration();

    if (current < max && this->m_status == Ended)
    {
        this->m_status = Paused;
        this->callChanged();
    }
}

void MediaPlayer::rewind(double distance)
{
    this->navigate(-distance);
}

void MediaPlayer::forward(double distance)
{
    this->navigate(distance);
}

void MediaPlayer::navigate(double distance)
{
    double current = this->m_presenter.getCurrentPosition();

    if (this->m_status == Ended)
    {
        this->m_status = Paused;
        this->callChanged();
    }

    double dist = current + distance;
    double duration = this->m_presenter.getDuration();

    if (dist < 0.0)
        dist = 0.0;
    else if (dist > duration)
        dist = duration;

    this->seek(dist, false);

    QString desc;
    QString time;

    if (this->isMovieSubtitleVisiable() && this->isSeekKeyFrame())
    {
        if (distance > 0.0)
            desc = trUtf8("앞으로 %1초 키프레임 이동");
        else
            desc = trUtf8("뒤로 %1초 키프레임 이동");
    }
    else
    {
        if (distance > 0.0)
            desc = trUtf8("앞으로 %1초 이동");
        else
            desc = trUtf8("뒤로 %1초 이동");
    }

    Utils::getTimeString((uint32_t)dist, Utils::TIME_HH_MM_SS, &time);

    desc = desc.arg(fabs(distance));
    desc += QString(" (%1, %2%)").arg(time).arg(dist / duration * 100, 0, 'f', 2);

    this->m_presenter.showOptionDesc(desc);
}

uint8_t MediaPlayer::getMaxVolume() const
{
    return this->m_presenter.getMaxVolume();
}

void MediaPlayer::volume(uint8_t volume)
{
    if (this->isUseSPDIF())
        return;

    this->m_presenter.volume(volume);

    int perVolume = (int)((float)volume / this->m_presenter.getMaxVolume() * 100);
    QString desc = trUtf8("소리 (%1%)").arg(perVolume);

    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::mute(bool mute, bool showDesc)
{
    this->m_presenter.mute(mute);

    if (showDesc)
    {
        QString desc;

        if (mute)
            desc = trUtf8("소리 꺼짐");
        else
            desc = trUtf8("소리 켜짐");

        this->m_presenter.showOptionDesc(desc);
    }
}

uint8_t MediaPlayer::getVolume() const
{
    return this->m_presenter.getVolume();
}

double MediaPlayer::getDuration() const
{
    return this->m_presenter.getDuration();
}

double MediaPlayer::getCurrentPosition()
{
    return this->m_presenter.getCurrentPosition();
}

bool MediaPlayer::hasDuration() const
{
    return this->m_presenter.hasDuration();
}

double MediaPlayer::getAspectRatio(bool widthPrio) const
{
    return this->m_presenter.getAspectRatio(widthPrio);
}

void MediaPlayer::setMaxTextureSize(int size)
{
    this->m_presenter.setMaxTextureSize(size);
}

int MediaPlayer::getMaxTextureSize() const
{
    return this->m_presenter.getMaxTextureSize();
}

MediaPlayer::Status MediaPlayer::getStatus()
{
    if (this->m_presenter.isRunning() == false)
        this->m_status = Stopped;

    return this->m_status;
}

bool MediaPlayer::isEnabledVideo() const
{
    return this->m_presenter.isEnabledVideo();
}

bool MediaPlayer::isAudio() const
{
    return this->m_presenter.isAudio();
}

bool MediaPlayer::isVideo() const
{
    return this->m_presenter.isVideo();
}

bool MediaPlayer::isAlignable()
{
    return this->m_presenter.isAlignable();
}

bool MediaPlayer::isSubtitleMoveable()
{
    return this->existSubtitle() && this->isVideo();
}

bool MediaPlayer::is3DSubtitleMoveable()
{
    return this->isSubtitleMoveable() && this->m_presenter.getSubtitle3DMethod() != AnyVODEnums::S3M_NONE;
}

bool MediaPlayer::isMovieSubtitleVisiable() const
{
    return this->isVideo() || !this->isOpened();
}

void MediaPlayer::showAlbumJacket(bool show)
{
    QString desc;

    if (show)
        desc = trUtf8("앨범 자켓 보이기");
    else
        desc = trUtf8("앨범 자켓 숨기기");

    this->m_presenter.showAlbumJacket(show);
    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isShowAlbumJacket() const
{
    return this->m_presenter.isShowAlbumJacket();
}

bool MediaPlayer::getFrameSize(int *width, int *height) const
{
    return this->m_presenter.getFrameSize(width, height);
}

bool MediaPlayer::getPictureRect(QRect *rect) const
{
    return this->m_presenter.getPictureRect(rect);
}

void MediaPlayer::enableGotoLastPos(bool enable, bool raw)
{
    this->m_gotoLastPlay = enable;

    if (raw)
        return;

    QString desc;

    if (enable)
        desc = trUtf8("재생 위치 기억 켜짐");
    else
        desc = trUtf8("재생 위치 기억 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool MediaPlayer::isGotoLastPos() const
{
    return this->m_gotoLastPlay;
}

VirtualFile& MediaPlayer::getVirtualFile()
{
    return this->m_presenter.getVirtualFile();
}

DTVReader& MediaPlayer::getDTVReader()
{
    return this->m_presenter.getDTVReader();
}

FilterGraph& MediaPlayer::getFilterGraph()
{
    return this->m_presenter.getFilterGraph();
}

bool MediaPlayer::configFilterGraph()
{
    return this->m_presenter.configFilterGraph();
}

void MediaPlayer::getAudioDevices(QStringList *ret) const
{
    this->m_presenter.getAudioDevices(ret);
}

bool MediaPlayer::setAudioDevice(int device)
{
    QStringList list;
    QString name;

    this->m_presenter.getAudioDevices(&list);

    if (device == -1)
    {
        name = trUtf8("기본 장치");
    }
    else
    {
        for (int i = 0; i < list.count(); i++)
        {
            if (i == device)
            {
                name = list[i];
                break;
            }
        }
    }

    bool success = this->m_presenter.setAudioDevice(device);

    this->m_presenter.showOptionDesc(trUtf8("소리 출력 장치 변경 (%1)").arg(name));

    return success;
}

int MediaPlayer::getCurrentAudioDevice() const
{
    return this->m_presenter.getCurrentAudioDevice();
}

void MediaPlayer::getSPDIFAudioDevices(QStringList *ret)
{
    this->m_presenter.getSPDIFAudioDevices(ret);
}

bool MediaPlayer::setSPDIFAudioDevice(int device)
{
    QStringList list;
    QString name;

    this->m_presenter.getSPDIFAudioDevices(&list);

    if (device == -1)
    {
        name = trUtf8("기본 장치");
    }
    else
    {
        for (int i = 0; i < list.count(); i++)
        {
            if (i == device)
            {
                name = list[i];
                break;
            }
        }
    }

    bool useSPDIF = this->isUseSPDIF();
    bool success = this->m_presenter.setSPDIFAudioDevice(device);
    QString desc;

    if (this->isUseSPDIF() == useSPDIF)
        desc = trUtf8("S/PDIF 소리 출력 장치 변경 (%1)").arg(name);
    else
        desc = trUtf8("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");

    this->m_presenter.showOptionDesc(desc);

    return success;
}

int MediaPlayer::getCurrentSPDIFAudioDevice() const
{
    return this->m_presenter.getCurrentSPDIFAudioDevice();
}

void MediaPlayer::setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree)
{
    QString desc;

    switch (degree)
    {
        case AnyVODEnums::SRD_NONE:
            desc = trUtf8("사용 안 함");
            break;
        case AnyVODEnums::SRD_90:
            desc = trUtf8("90도");
            break;
        case AnyVODEnums::SRD_180:
            desc = trUtf8("180도");
            break;
        case AnyVODEnums::SRD_270:
            desc = trUtf8("270도");
            break;
        default:
            break;
    }

    this->m_presenter.showOptionDesc(trUtf8("화면 회전 각도 (%1)").arg(desc));
    this->m_presenter.setScreenRotationDegree(degree);
}

AnyVODEnums::ScreenRotationDegree MediaPlayer::getScreenRotationDegree() const
{
    return this->m_presenter.getScreenRotationDegree();
}

void MediaPlayer::showOptionDesc(const QString &desc)
{
    this->m_presenter.showOptionDesc(desc);
}

void MediaPlayer::setASSFontPath(const QString &path)
{
    this->m_presenter.setASSFontPath(path);
}

void MediaPlayer::setASSFontFamily(const QString &family)
{
    this->m_presenter.setASSFontFamily(family);
}

void MediaPlayer::playingCallback(void *userData)
{
    MediaPlayer *parent = (MediaPlayer*)userData;

    if (parent->m_playing.callback)
        parent->m_playing.callback(parent->m_playing.userData);
}

void MediaPlayer::endedCallback(void *userData)
{
    MediaPlayer *parent = (MediaPlayer*)userData;

    parent->m_status = Ended;
    parent->callChanged();

    parent->m_lastPlay.clear(Utils::removeFFMpegSeparator(parent->m_lastPlayPath));
}

void MediaPlayer::callChanged()
{
    if (this->m_statusChanged.callback)
        this->m_statusChanged.callback(this->m_statusChanged.userData);
}

void MediaPlayer::setCaptureMode(bool capture)
{
    this->m_presenter.setCaptureMode(capture);
}

bool MediaPlayer::getCaptureMode() const
{
    return this->m_presenter.getCaptureMode();
}

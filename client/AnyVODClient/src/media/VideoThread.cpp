﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "VideoThread.h"
#include "MediaPresenter.h"
#include "MediaState.h"
#include "core/Utils.h"

#include <QDebug>

extern "C"
{
    #include <libavutil/pixdesc.h>
    #include <libavutil/imgutils.h>
}

VideoThread::VideoThread(QObject *parent) :
    QThread(parent)
{

}

void VideoThread::run()
{
    MediaPresenter *parent = (MediaPresenter*)this->parent();
    MediaState *ms = parent->m_state;
    Video &video = ms->video;
    Seek &seek = ms->seek;
    AVFrame *frame = av_frame_alloc();
    AVCodecContext *ctx = video.stream.ctx;
    AVFrame first;
    AVFrame second;
#ifndef Q_OS_ANDROID
    AVFrame hwPicture;
#endif
    QVector<bool*> quits;
    bool firstDiscard = true;
    bool allocedPicture = false;
    int lastWidth = 0;
    int lastHeight = 0;
#ifndef Q_OS_ANDROID
    bool hwAllocedPicture = false;
    int hwLastWidth = 0;
    int hwLastHeight = 0;
#endif

    quits.append(&ms->quit);
    quits.append(&ms->video.threadQuit);

    while (true)
    {
        class localPtr
        {
        public:
            localPtr()
            {

            }

            ~localPtr()
            {
                av_packet_unref(&this->packet.packet);
            }

            PacketQueue::Packet packet;
        }l;

        if (ms->quit)
            break;

        if (ms->video.threadQuit)
            break;

        bool block = true;
        bool isEmptyBuffer = false;
        bool isVideoEmpty = false;

        if (!video.stream.queue.hasPacket() && !ms->pause.pause && !ms->willBeEnd)
        {
            if (!parent->isAudio())
            {
                isVideoEmpty = true;
                video.startTime = parent->getAbsoluteClock();

                if (parent->isRemoteProtocol())
                {
                    isEmptyBuffer = true;
                    parent->callEmptyCallback(isEmptyBuffer);

                    while (parent->isUseBufferingMode())
                    {
                        if (ms->quit)
                            break;

                        if (ms->video.threadQuit)
                            break;

                        if (video.stream.queue.hasPacketInPercent(0.5, MAX_VIDEO_QUEUE_SIZE))
                            break;

                        QThread::msleep(EMPTY_BUFFER_WAIT_DELAY);
                    }
                }
            }
        }

        if (!video.stream.queue.get(&l.packet, quits, &block))
            break;

        if (isEmptyBuffer)
            parent->callEmptyCallback(false);

        if (isVideoEmpty)
            video.driftTime += parent->getAbsoluteClock() - video.startTime;

        if (video.stream.queue.isFlushPacket(&l.packet))
        {
            if (!parent->m_hwDecoder.isOpened())
                avcodec_flush_buffers(ctx);

            parent->flushPictureQueue();

            if (parent->m_hwDecoder.isOpened())
                parent->m_hwDecoder.flushSurfaceQueue();

            seek.flushed = true;
            seek.firstFrameAfterFlush = false;

            continue;
        }

        parent->m_detail.videoInputByteCount.fetchAndAddOrdered(l.packet.packet.size);

        int success = 0;
        int decoded = 0;
        bool isDelayed = false;

#ifdef Q_OS_ANDROID
        if (parent->m_hwDecoder.isOpened())
        {
            bool ret = parent->m_hwDecoder.decodePicture(l.packet.packet, frame);

            success = ret ? 1 : 0;
            decoded = success;
        }
        else
        {
#endif
            success = Utils::decodeFrame(ctx, &l.packet.packet, frame, &decoded);
#ifdef Q_OS_ANDROID
        }
#endif

        if (decoded && parent->m_detail.videoInputType.isEmpty())
        {
            const int bufSize = 128;
            char buf[bufSize] = {0, };
            AVPixelFormat pixFormat = parent->m_hwDecoder.getFormat();
            QStringList pixFMT = QString(av_get_pix_fmt_string(buf, bufSize, pixFormat)).split(" ", QString::SkipEmptyParts);

            parent->m_detail.videoInputType = pixFMT[0].toUpper();
            parent->m_detail.videoInputBits = pixFMT[2].toInt();

            if (parent->isUseGPUConvert(pixFormat))
                parent->m_format = pixFormat;

            pixFMT = QString(av_get_pix_fmt_string(buf, bufSize, parent->m_format)).split(" ", QString::SkipEmptyParts);

            parent->m_detail.videoOutputType = pixFMT[0].toUpper();
            parent->m_detail.videoOutputBits = pixFMT[2].toInt();

            parent->m_deinterlacer.setCodec(video.stream.ctx, pixFormat, video.stream.stream->time_base);
            parent->m_filterGraph.setCodec(video.stream.ctx, pixFormat, parent->m_format, video.stream.stream->time_base);

            video.pixFormat = pixFormat;
        }

        if (l.packet.isNullPacket() && ctx->codec && ctx->codec->capabilities & CODEC_CAP_DELAY)
            isDelayed = true;

        if (l.packet.discard)
        {
            if (firstDiscard)
            {
                seek.videoDiscardStartTime = parent->getAbsoluteClock();
                firstDiscard = false;
            }

            l.packet.discard = false;
        }
        else
        {
            if (!firstDiscard)
            {
                firstDiscard = true;
                seek.videoDiscardDriftTime += parent->getAbsoluteClock() - seek.videoDiscardStartTime;

                if (parent->getCurrentAudioStreamIndex() != -1)
                {
                    if (parent->m_spdif.isOpened())
                        parent->m_spdif.resume();
                    else
                        BASS_Start();
                }
            }

            if (success >= 0 || isDelayed)
            {
                if (decoded)
                {
                    if (video.frameDrop > 0)
                    {
                        parent->m_detail.videoFrameDropCount.fetchAndAddOrdered(1);
                        video.frameDrop--;

                        continue;
                    }

                    bool deinterlace = false;
                    bool doubler = false;
                    bool getFirstFrame = false;
                    bool getSecondFrame = false;

                    if ((parent->m_deinterlacer.getMethod() == AnyVODEnums::DM_AUTO && frame->interlaced_frame) || parent->m_deinterlacer.getMethod() == AnyVODEnums::DM_USE)
                        deinterlace = true;

                    if (!parent->m_detail.videoInterlaced)
                        parent->m_detail.videoInterlaced = (bool)frame->interlaced_frame;

                    parent->m_detail.videoDeinterlaced = deinterlace;

                    AVFrame org;
                    AVPixelFormat format;
#ifdef Q_OS_ANDROID
                    if (parent->m_hwDecoder.isOpened())
                    {
                        format = parent->m_hwDecoder.getFormat();

                        if (format == AV_PIX_FMT_NONE)
                            continue;

                        memcpy(org.data, frame->data, sizeof(org.data));
                        memcpy(org.linesize, frame->linesize, sizeof(org.linesize));
                    }
                    else
#else
                    if (parent->m_hwDecoder.isOpened())
                    {
                        format = parent->m_hwDecoder.getFormat();

                        if (format == AV_PIX_FMT_NONE)
                            continue;

                        if (hwLastWidth != frame->width || hwLastHeight != frame->height)
                        {
                            if (hwAllocedPicture)
                                av_freep(&hwPicture.data[0]);

                            av_image_alloc(hwPicture.data, hwPicture.linesize, FFALIGN(frame->width, 16), FFALIGN(frame->height, 16), format, 1);
                            hwAllocedPicture = true;

                            hwLastWidth = frame->width;
                            hwLastHeight = frame->height;
                        }

                        AVFrame raw;

                        memcpy(raw.data, frame->data, sizeof(raw.data));
                        memcpy(raw.linesize, frame->linesize, sizeof(raw.linesize));

                        if (!parent->m_hwDecoder.copyPicture(raw, &hwPicture))
                            continue;

                        memcpy(org.data, hwPicture.data, sizeof(org.data));
                        memcpy(org.linesize, hwPicture.linesize, sizeof(org.linesize));
                    }
                    else
#endif
                    {
                        memcpy(org.data, frame->data, sizeof(org.data));
                        memcpy(org.linesize, frame->linesize, sizeof(org.linesize));

                        format = ctx->pix_fmt;
                    }

                    double pts;

                    pts = av_q2d(video.stream.stream->time_base) * av_frame_get_best_effort_timestamp(frame);
                    pts = parent->synchronizeVideo(frame, pts, video.stream.ctx->framerate);

                    if (deinterlace)
                    {
                        if (lastWidth != frame->width || lastHeight != frame->height)
                        {
                            if (allocedPicture)
                            {
                                av_freep(&first.data[0]);
                                av_freep(&second.data[0]);
                            }

                            av_image_alloc(first.data, first.linesize, frame->width, frame->height, format, 1);
                            av_image_alloc(second.data, second.linesize, frame->width, frame->height, format, 1);

                            allocedPicture = true;

                            lastWidth = frame->width;
                            lastHeight = frame->height;
                        }

                        if (parent->m_deinterlacer.isAVFilter())
                        {
                            AVFrame *to;

                            if (parent->m_hwDecoder.isOpened())
                            {
                                to = av_frame_alloc();

                                to->width  = frame->width;
                                to->height = frame->height;
                                to->format = format;

                                av_frame_get_buffer(to, 16);
                                av_frame_copy_props(to, frame);

                                av_image_copy(to->data, to->linesize, (const uint8_t**)org.data, org.linesize, format, frame->width, frame->height);

                                to->interlaced_frame = frame->interlaced_frame;
                                to->top_field_first = frame->top_field_first;
                            }
                            else
                            {
                                to = frame;
                            }

                            doubler = parent->m_deinterlacer.deinterlace(&first, &getFirstFrame, &second, &getSecondFrame, to);

                            if (parent->m_hwDecoder.isOpened())
                                av_frame_free(&to);
                        }
                        else
                        {
                            av_image_copy(first.data, first.linesize, (const uint8_t**)org.data, org.linesize, format, frame->width, frame->height);

                            doubler = parent->m_deinterlacer.deinterlace(&first, &second, frame->height, format);

                            getFirstFrame = true;

                            if (doubler)
                                getSecondFrame = true;
                        }
                    }

                    if (doubler)
                    {
                        if (getFirstFrame)
                        {
                            if (!parent->convertPicture(frame->top_field_first ? first : second, pts, format, false))
                                break;
                        }

                        if (getSecondFrame)
                        {
                            if (ms->seek.firstFrameAfterFlush && !ms->seek.pauseSeeking)
                            {
                                double diff = pts - ms->frameTimer.lastRealPTS;
                                double nextPTS = pts + (diff / 2.0);

                                if (!parent->convertPicture(frame->top_field_first ? second : first, nextPTS, format, false))
                                    break;
                            }
                        }
                    }
                    else if (deinterlace)
                    {
                        if (getFirstFrame)
                        {
                            if (!parent->convertPicture(first, pts, format, false))
                                break;
                        }
                    }
                    else
                    {
                        bool pageFlip = false;
                        bool leftOrTop = false;

                        switch (parent->m_3dMethod)
                        {
                            case AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR:
                            case AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR:
                            {
                                pageFlip = true;
                                leftOrTop = true;

                                break;
                            }
                            case AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR:
                            case AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR:
                            {
                                pageFlip = true;
                                leftOrTop = false;

                                break;
                            }
                            default:
                            {
                                break;
                            }
                        }

                        if (pageFlip)
                        {
                            if (!parent->convertPicture(org, pts, format, leftOrTop))
                                break;

                            if (ms->seek.firstFrameAfterFlush && !ms->seek.pauseSeeking)
                            {
                                double diff = pts - ms->frameTimer.lastRealPTS;
                                double nextPTS = pts + (diff / 2.0);

                                if (!parent->convertPicture(org, nextPTS, format, !leftOrTop))
                                    break;
                            }
                        }
                        else
                        {
                            if (!parent->convertPicture(org, pts, format, false))
                                break;
                        }
                    }

                    ms->frameTimer.lastRealPTS = pts;
                }
            }
        }
    }

    if (allocedPicture)
    {
        av_freep(&first.data[0]);
        av_freep(&second.data[0]);
    }

#ifndef Q_OS_ANDROID
    if (hwAllocedPicture)
        av_freep(&hwPicture.data[0]);
#endif

    av_frame_free(&frame);
}

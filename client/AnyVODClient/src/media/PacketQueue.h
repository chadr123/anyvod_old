﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <QVector>
#include <QMutex>
#include <QWaitCondition>

extern "C"
{
    #include <libavformat/avformat.h>
}

class PacketQueue
{
public:
    struct Concurrent
    {
        Concurrent()
        {

        }

        QMutex mutex;
        QWaitCondition cond;
    };

    struct Packet
    {
        Packet()
        {
            av_init_packet(&packet);
            discard = false;
        }

        bool isNullPacket() const
        {
            return packet.data == NULL && packet.size == 0;
        }

        AVPacket packet;
        bool discard;
    };

    struct PacketList
    {
        Packet pkt;
        PacketList *next;
    };

    PacketQueue();

    void init();
    void end();

    bool hasNullPacket();
    bool hasPacket();
    bool hasPacketInPercent(double ratio, double max);

    bool putFlushPacket();
    bool putNullPacket(int streamIndex);
    bool put(Packet *packet);

    int getBufferSizeInByte();
    bool get(Packet *packet, QVector<bool*> &quits, bool *block);

    bool isFlushPacket(Packet *packet) const;
    void flush();

    void unlock();

private:
    PacketList *m_first;
    PacketList *m_last;
    int m_byteSize;
    int m_nullPacketCount;
    Concurrent m_lock;
    Packet m_flushPacket;
};

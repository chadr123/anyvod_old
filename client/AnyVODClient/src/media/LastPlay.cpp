﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "LastPlay.h"
#include "../core/Utils.h"

#include <QFileInfo>
#include <QDebug>

QDataStream& operator << (QDataStream &out, const LastPlay::Item &item)
{
    out << item.pos;
    out << item.lastTime;

    return out;
}

QDataStream& operator >> (QDataStream &in, LastPlay::Item &item)
{
    in >> item.pos;
    in >> item.lastTime;

    return in;
}

LastPlay::LastPlay() :
    m_file(Utils::getSettingPath() + "lastplay.ini", QSettings::IniFormat)
{
    QHash<QString, QVariant> list = this->m_file.value("list").toHash();
    QHash<QString, QVariant>::iterator i = list.begin();

    for (; i != list.end(); i++)
        this->m_table[i.key()] = i.value().value<Item>();
}

LastPlay::~LastPlay()
{
    this->clean();

    QHash<QString, QVariant> list;
    QHash<QString, Item>::iterator i = this->m_table.begin();

    for (; i != this->m_table.end(); i++)
        list[i.key()] = qVariantFromValue(i.value());

    this->m_file.setValue("list", list);
    this->m_file.sync();
}

void LastPlay::clean()
{
    QHash<QString, Item>::iterator i = this->m_table.begin();

    while (i != this->m_table.end())
    {
        const Item &item = i.value();
        bool erase = false;

        if (!erase && item.lastTime.daysTo(QDateTime::currentDateTime()) > 30)
            erase = true;

        if (erase)
            i = this->m_table.erase(i);
        else
            i++;
    }
}

double LastPlay::get(const QString &filePath)
{
    QHash<QString, Item>::iterator i = this->m_table.find(filePath);

    if (i == this->m_table.end())
        return 0.0;
    else
        return i.value().pos;
}

void LastPlay::clear(const QString &filePath)
{
    this->m_table.remove(filePath);
}

void LastPlay::update(const QString &filePath, double pos)
{
    Item item;

    item.pos = pos;
    item.lastTime = QDateTime::currentDateTime();

    this->m_table[filePath] = item;
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "core/Common.h"

#include <QMutex>

#ifdef Q_OS_MAC
#include <dispatch/dispatch.h>
#endif

extern "C"
{
    #include <libavformat/avformat.h>
    #include <libavfilter/avfiltergraph.h>
}

class Deinterlacer
{
public:
    Deinterlacer();
    ~Deinterlacer();

    bool setCodec(AVCodecContext *codec, AVPixelFormat pixFormat, const AVRational timeBase);
    bool deinterlace(AVFrame *first, bool *getFirstFrame, AVFrame *second, bool *getSecondFrame, AVFrame *frame);
    bool isAVFilter() const;

    bool deinterlace(const AVFrame *first, const AVFrame *second, int height, AVPixelFormat format) const;

    void setMethod(AnyVODEnums::DeinterlaceMethod method);
    void setAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm);

    AnyVODEnums::DeinterlaceMethod getMethod() const;
    AnyVODEnums::DeinterlaceAlgorithm getAlgorithm() const;

private:
    struct FILTER
    {
        FILTER() :
            in(NULL),
            deint(NULL),
            out(NULL),
            graph(NULL)
        {

        }

        AVFilterContext *in;
        AVFilterContext *deint;
        AVFilterContext *out;
        AVFilterGraph *graph;
    };

private:
    void unInitFilter();
    void deint(AVFrame *first, bool *getFirstFrame, AVFrame *second, bool *getSecondFrame, AVFrame *frame, const FILTER &filter);
    void getDeintNameAndParam(QString *name, QString *param) const;

    void blend(const AVFrame *first, int height, int height2, AVPixelFormat format, int planeCount) const;
    void bob(const AVFrame *first, const AVFrame *second, int height, int height2, AVPixelFormat format, int planeCount) const;

    void blendPlane(uint8_t *data, int linesize, int height) const;
    void splitPicturePlane(const uint8_t * const first, uint8_t *second, int linesize, int height) const;
    void interpolatePlane(uint8_t *ret, int linesize, int height, bool secondPlane) const;

private:
    AnyVODEnums::DeinterlaceMethod m_method;
    AnyVODEnums::DeinterlaceAlgorithm m_algorithm;
    FILTER m_deint;
    QMutex m_deintLock;

#ifdef Q_OS_MAC
    dispatch_group_t m_group;
    dispatch_queue_t m_queue;
#endif
};

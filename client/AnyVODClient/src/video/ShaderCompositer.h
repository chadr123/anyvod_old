﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "core/Common.h"

#include <QOpenGLShaderProgram>
#include <QMatrix4x4>
#include <QVector2D>

extern "C"
{
    #include <libavutil/pixfmt.h>
}

class ShaderCompositer
{
public:
    enum ShaderType
    {
        ST_SCREEN,
        ST_SIMPLE,
        ST_SUBTITLE_INTERLACE,
        ST_SUBTITLE_CHECKER_BOARD,
        ST_SUBTITLE_ANAGLYPH,
        ST_BARREL_DISTORTION,
        ST_COUNT
    };

public:
    ShaderCompositer();

    void setRenderData(ShaderType type, const QMatrix4x4 &proj, const QMatrix4x4 &model,
                       QVector2D *vertices, QVector2D *texCoords, const QVector4D &color,
                       bool isGPUConvert, AVPixelFormat pixFormat);
    void updateRenderData(ShaderType type, const QMatrix4x4 &model, QVector2D *vertices, QVector2D *texCoords);
    void setTextureSampler(ShaderType type, int index);

    bool startScreen(bool useNormalizedCoord, const QSizeF &surfaceSize, const QSizeF &screenSize, const QSize &texSize, double clock, double lumAvg);
    void endScreen();

    bool startSimple();
    void endSimple();

    bool startSubtitleInterlace(bool firstFrame);
    void endSubtitleInterlace();

    bool startSubtitleCheckerBoard(bool firstFrame);
    void endSubtitleCheckerBoard();

    bool startSubtitleAnaglyph(bool firstFrame);
    void endSubtitleAnaglyph();

    bool startBarrelDistortion(const QVector2D &texRange, const QVector2D &lensCenterOffset,
                               const QVector2D &coefficients, float fillScale);
    void endBarrelDistortion();

    bool addShader(const QString &filePath);
    bool deleteShader(const QString &filePath);
    void clearShaders();

    void getShaderList(QStringList *ret);
    void getFragmentSource(QString *ret) const;
    void getVertexSource(QString *ret) const;

    bool build();
    void getLog(QString *ret) const;

    void setHue(double hue);
    double getHue() const;

    void setSaturation(double saturation);
    double getSaturation() const;

    void setContrast(double contrast);
    double getContrast() const;

    void setBrightness(double brightness);
    double getBrightness() const;

    void useSharply(bool use);
    bool isUsingSharply() const;

    void useSharpen(bool use);
    bool isUsingSharpen() const;

    void useSoften(bool use);
    bool isUsingSoften() const;

    void useLeftRightInvert(bool use);
    bool isUsingLeftRightInvert() const;

    void useTopBottomInvert(bool use);
    bool isUsingTopBottomInvert() const;

    void set3DMethod(AnyVODEnums::Video3DMethod method3D);
    AnyVODEnums::Video3DMethod get3DMethod() const;

    void setAnaglyphAlgorithm(AnyVODEnums::AnaglyphAlgorithm algoritm);
    AnyVODEnums::AnaglyphAlgorithm getAnaglyphAlgorithm() const;

    bool canUseAnaglyphAlgorithm() const;

    void setup3D(bool sideBySide, bool leftOrTop);

    void getAnaglyphMatrix(float matFirst[3][3], float matSecond[3][3]) const;
    bool getLeftOrTop() const;

private:
    enum VertexShaderLocation
    {
        VSL_VERTEX,
        VSL_TEXTURE_COORD,
        VSL_COUNT
    };

    struct DefaultParam
    {
        DefaultParam()
        {
            hue = 0.0;
            saturation = 1.0;
            contrast = 1.0;
            brightness = 1.0;
            useSharply = false;
            useSharpen = false;
            useSoften = false;
            useLeftRightInvert = false;
            useTopBottomInvert = false;
            interlaced = false;
            leftOrTop = false;
            rowOrCol = false;
            sideBySide = false;
            anaglyph = false;
            checker = false;
            method3D = AnyVODEnums::V3M_NONE;
            anaglyphAlgorithm = AnyVODEnums::AGA_DEFAULT;
        }

        double hue;
        double saturation;
        double contrast;
        double brightness;
        bool useSharply;
        bool useSharpen;
        bool useSoften;
        bool useLeftRightInvert;
        bool useTopBottomInvert;
        bool interlaced;
        bool leftOrTop;
        bool rowOrCol;
        bool sideBySide;
        bool anaglyph;
        bool checker;
        AnyVODEnums::Video3DMethod method3D;
        AnyVODEnums::AnaglyphAlgorithm anaglyphAlgorithm;
    };

    struct ShaderItem
    {
        QString path;
        QString content;
    };

private:
    void makeEntry(QString *ret) const;
    void addEntry(const QString &entry, QString *ret) const;
    void getFuncs(QString *ret) const;
    int findShader(const QString &path) const;
    void setRenderData(QOpenGLShaderProgram &shader, const QMatrix4x4 &proj, const QMatrix4x4 &model,
                       QVector2D *vertices, QVector2D *texCoords, const QVector4D &color, bool isGPUConvert,
                       AVPixelFormat pixFormat);
    void updateRenderData(QOpenGLShaderProgram &shader, const QMatrix4x4 &model, QVector2D *vertices, QVector2D *texCoords);
    void unsetRenderData(QOpenGLShaderProgram &shader);
    void setTextureSampler(QOpenGLShaderProgram &shader, int index);
    bool buildShader(QOpenGLShaderProgram &shader, const QString &code);

private:
    QOpenGLShaderProgram m_shaderScreen;
    QOpenGLShaderProgram m_shaderSimple;
    QOpenGLShaderProgram m_shaderBarrelDistortion;
    QOpenGLShaderProgram m_shaderSubtitleInterlace;
    QOpenGLShaderProgram m_shaderSubtitleCheckerBoard;
    QOpenGLShaderProgram m_shaderSubtitleAnaglyph;
    QList<ShaderItem> m_shaderSources;
    const QString m_vertexFrame;
    const QString m_shaderLib;
    const QString m_shaderFrame;
    const QString m_effect;
    const QString m_interlace3D;
    const QString m_checkerBoard;
    const QString m_anaglyph;
    const QString m_subtitleInterlace;
    const QString m_subtitleCheckerBoard;
    const QString m_subtitleAnaglyph;
    const QString m_barrelDistortion;
    const QString m_simple;
    const QString m_default;
    DefaultParam m_defaultParam;
    QString m_fragmentSource;
    QString m_vertexSource;

private:
    static const QString ENTRY_HEADER;
    static const QString ENTRY_FOOTER;
    static const QString ENTRY_SIG;
    static const QString FUNCS_SIG;
};

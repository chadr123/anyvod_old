﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "USWCMemCopy.h"

#include <QtGlobal>
#include <QDebug>

extern "C"
{
    #include <libavcodec/avcodec.h>
    #include <libavutil/cpu.h>
}

#define RUN_ASM(cpu, flag, op) do { \
    if (cpu & flag)                 \
        asm volatile (op);          \
    } while (0)

#define COPY_STREAM64(srcp, destp, load, store)  \
    asm volatile (                               \
        load "  0(%[src]), %%xmm1\n"             \
        load " 16(%[src]), %%xmm2\n"             \
        load " 32(%[src]), %%xmm3\n"             \
        load " 48(%[src]), %%xmm4\n"             \
        store " %%xmm1,    0(%[dst])\n"          \
        store " %%xmm2,   16(%[dst])\n"          \
        store " %%xmm3,   32(%[dst])\n"          \
        store " %%xmm4,   48(%[dst])\n"          \
        : : [dst]"r"(destp), [src]"r"(srcp) : "memory")

const int USWCMemCopy::USWC_MAX_BUFFER_SIZE = 4096;

USWCMemCopy::USWCMemCopy() :
    m_cpu(av_get_cpu_flags()),
    m_buf((uint8_t*)av_malloc(USWC_MAX_BUFFER_SIZE))
{

}

USWCMemCopy::~USWCMemCopy()
{
    av_freep(&this->m_buf);
}

bool USWCMemCopy::copyYUV(const AVFrame &src, AVFrame *ret, int height, int planes) const
{
    if (src.linesize[0] != ret->linesize[0])
        return false;

    if (!(this->m_cpu & AV_CPU_FLAG_SSE4 || this->m_cpu & AV_CPU_FLAG_SSE2))
        return false;

    for (int i = 0; i < planes; i++)
    {
        int div = i > 0 ? 2 : 1;

        this->copyPlane(src.data[i], src.linesize[i], ret->data[i], ret->linesize[i], height / div);
    }

    return true;
}

void USWCMemCopy::copyPlane(const uint8_t *src, int srcLineSize, uint8_t *dest, int destLineSize, int height) const
{
    int size = FFALIGN(destLineSize, 16);
    int blockUnit = USWC_MAX_BUFFER_SIZE / size;

    for (int i = 0; i < height; i += blockUnit)
    {
        int blocks = qMin(blockUnit, height - i);

        this->copyFromUSWC(src, srcLineSize, this->m_buf, size, blocks);
        this->copyToMem(this->m_buf, size, dest, destLineSize, blocks);

        src += srcLineSize * blocks;
        dest += destLineSize * blocks;
    }
}

void USWCMemCopy::copyFromUSWC(const uint8_t *src, int srcLineSize, uint8_t *dest, int destLineSize, int height) const
{
    RUN_ASM(this->m_cpu, AV_CPU_FLAG_SSE2, "mfence");

    for (int i = 0; i < height; i++)
    {
        int unAlignedSize = (intptr_t)src & 15;
        int x = 0;

        memcpy(&dest[x], &src[x], unAlignedSize);
        x += unAlignedSize;

        if (this->m_cpu & AV_CPU_FLAG_SSE4)
        {
            if (unAlignedSize)
            {
                for (; x + 63 < destLineSize; x += 64)
                    COPY_STREAM64(&src[x], &dest[x], "movntdqa", "movdqu");
            }
            else
            {
                for (; x + 63 < destLineSize; x += 64)
                    COPY_STREAM64(&src[x], &dest[x], "movntdqa", "movdqa");
            }
        }
        else if (this->m_cpu & AV_CPU_FLAG_SSE2)
        {
            if (unAlignedSize)
            {
                for (; x + 63 < destLineSize; x += 64)
                    COPY_STREAM64(&src[x], &dest[x], "movdqa", "movdqu");
            }
            else
            {
                for (; x + 63 < destLineSize; x += 64)
                    COPY_STREAM64(&src[x], &dest[x], "movdqa", "movdqa");
            }
        }

        memcpy(&dest[x], &src[x], destLineSize - x);

        src += srcLineSize;
        dest += destLineSize;
    }
}

void USWCMemCopy::copyToMem(const uint8_t *src, int srcLineSize, uint8_t *dest, int destLineSize, int height) const
{
    RUN_ASM(this->m_cpu, AV_CPU_FLAG_SSE2, "mfence");

    for (int i = 0; i < height; i++)
    {
        bool aligned = ((intptr_t)dest & 15) == 0;
        int x = 0;

        if (this->m_cpu & AV_CPU_FLAG_SSE2)
        {
            if (aligned)
            {
                for (; x + 63 < destLineSize; x += 64)
                    COPY_STREAM64(&src[x], &dest[x], "movdqa", "movntdq");
            }
            else
            {
                for (; x + 63 < destLineSize; x += 64)
                    COPY_STREAM64(&src[x], &dest[x], "movdqa", "movdqu");
            }
        }

       memcpy(&dest[x], &src[x], destLineSize - x);

       src += srcLineSize;
       dest += destLineSize;
   }
}

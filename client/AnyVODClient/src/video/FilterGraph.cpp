﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "FilterGraph.h"

#include <QMutexLocker>
#include <QDebug>

extern "C"
{
    #include <libavutil/opt.h>
    #include <libavutil/imgutils.h>

    #include <libavfilter/buffersrc.h>
    #include <libavfilter/buffersink.h>
}

FilterGraph::FilterGraph() :
    m_in(NULL),
    m_out(NULL),
    m_graph(NULL),
    m_delayCount(0)
{

}

FilterGraph::~FilterGraph()
{
    this->unInitFilter();
}

bool FilterGraph::setCodec(AVCodecContext *codec, AVPixelFormat pixFormat, AVPixelFormat screenFormat, const AVRational timeBase)
{
    QMutexLocker lock(&this->m_lock);

    this->unInitFilter();

    this->m_graph = avfilter_graph_alloc();

    if (!this->m_graph)
        return false;

    this->m_graph->scale_sws_opts = av_strdup("flags=neighbor");

    AVFilter *in = avfilter_get_by_name("buffer");
    AVFilter *out = avfilter_get_by_name("buffersink");
    QString inParam;
    AVPixelFormat pixFormats[] = {screenFormat, AV_PIX_FMT_NONE};

    inParam = QString("video_size=%1x%2:pix_fmt=%3:time_base=%4/%5:pixel_aspect=%6/%7")
            .arg(codec->width)
            .arg(codec->height)
            .arg(pixFormat)
            .arg(timeBase.num)
            .arg(timeBase.den)
            .arg(codec->sample_aspect_ratio.num)
            .arg(codec->sample_aspect_ratio.den);

    if (avfilter_graph_create_filter(&this->m_in, in, "in", inParam.toLatin1(), NULL, this->m_graph) < 0)
    {
        this->unInitFilter();
        return false;
    }

    this->m_filters.append(this->m_in);

    if (this->m_hqdn3d.enable)
    {
        QString param = "luma_spatial=4.0";
        AVFilter *filter = avfilter_get_by_name("hqdn3d");

        if (avfilter_graph_create_filter(&this->m_hqdn3d.filter, filter, "hqdn3d", param.toLatin1(), NULL, this->m_graph) < 0)
        {
            this->unInitFilter();
            return false;
        }

        this->m_filters.append(this->m_hqdn3d.filter);
    }

    if (this->m_atadenoise.enable)
    {
        QString param = "0a=0.02:0b=0.02:1a=0.02:1b=0.02:2a=0.02:2b=0.02:s=33";
        AVFilter *filter = avfilter_get_by_name("atadenoise");

        if (avfilter_graph_create_filter(&this->m_atadenoise.filter, filter, "atadenoise", param.toLatin1(), NULL, this->m_graph) < 0)
        {
            this->unInitFilter();
            return false;
        }

        this->m_delayCount += 33 / 2;

        this->m_filters.append(this->m_atadenoise.filter);
    }

    if (this->m_owdenoise.enable)
    {
        QString param = "depth=8:ls=1.0:cs=1.0";
        AVFilter *filter = avfilter_get_by_name("owdenoise");

        if (avfilter_graph_create_filter(&this->m_owdenoise.filter, filter, "owdenoise", param.toLatin1(), NULL, this->m_graph) < 0)
        {
            this->unInitFilter();
            return false;
        }

        this->m_filters.append(this->m_owdenoise.filter);
    }

    if (this->m_histeq.enable)
    {
        QString param = "strength=0.200:intensity=0.210:antibanding=none";
        AVFilter *filter = avfilter_get_by_name("histeq");

        if (avfilter_graph_create_filter(&this->m_histeq.filter, filter, "histeq", param.toLatin1(), NULL, this->m_graph) < 0)
        {
            this->unInitFilter();
            return false;
        }

        this->m_filters.append(this->m_histeq.filter);
    }

    if (this->m_deband.enable)
    {
        QString param = "1thr=0.02:2thr=0.02:3thr=0.02:4thr=0.02:r=16:blur=1";
        AVFilter *filter = avfilter_get_by_name("deband");

        if (avfilter_graph_create_filter(&this->m_deband.filter, filter, "deband", param.toLatin1(), NULL, this->m_graph) < 0)
        {
            this->unInitFilter();
            return false;
        }

        this->m_filters.append(this->m_deband.filter);
    }

    if (avfilter_graph_create_filter(&this->m_out, out, "out", NULL, NULL, this->m_graph) < 0)
    {
        this->unInitFilter();
        return false;
    }

    if (av_opt_set_int_list(this->m_out, "pix_fmts", pixFormats, AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN) < 0)
        return false;

    this->m_filters.append(this->m_out);

    if (!this->link())
    {
        this->unInitFilter();
        return false;
    }

    if (avfilter_graph_config(this->m_graph, NULL) < 0)
    {
        this->unInitFilter();
        return false;
    }

    return true;
}

void FilterGraph::setEnableHistEQ(bool enable)
{
    this->m_histeq.enable = enable;
}

bool FilterGraph::getEnableHistEQ() const
{
    return this->m_histeq.enable;
}

void FilterGraph::setEnableHighQuality3DDenoise(bool enable)
{
    this->m_hqdn3d.enable = enable;
}

bool FilterGraph::getEnableHighQuality3DDenoise() const
{
    return this->m_hqdn3d.enable;
}

void FilterGraph::setEnableDeBand(bool enable)
{
    this->m_deband.enable = enable;
}

bool FilterGraph::getEnableDeBand() const
{
    return this->m_deband.enable;
}

void FilterGraph::setEnableATADenoise(bool enable)
{
    this->m_atadenoise.enable = enable;
}

bool FilterGraph::getEnableATADenoise() const
{
    return this->m_atadenoise.enable;
}

void FilterGraph::setEnableOWDenoise(bool enable)
{
    this->m_owdenoise.enable = enable;
}

bool FilterGraph::getEnableOWDenoise() const
{
    return this->m_owdenoise.enable;
}

bool FilterGraph::getFrame(int width, int height, AVPixelFormat informat, const AVFrame &in,
                           AVFrame *out, AVPixelFormat *retFormat)
{
    QMutexLocker lock(&this->m_lock);

    class localPtr
    {
    public:
        localPtr() :
            m_in(av_frame_alloc()),
            m_out(av_frame_alloc())
        {

        }

        ~localPtr()
        {
            if (this->m_in)
                av_frame_free(&this->m_in);

            if (this->m_out)
                av_frame_free(&this->m_out);
        }

        AVFrame *m_in;
        AVFrame *m_out;
    }l;

    if (!this->m_in || !this->m_out)
        return false;

    l.m_in->width  = width;
    l.m_in->height = height;
    l.m_in->format = informat;

    memcpy(l.m_in->data, in.data, sizeof(l.m_in->data));
    memcpy(l.m_in->linesize, in.linesize, sizeof(l.m_in->linesize));

    if (av_buffersrc_add_frame_flags(this->m_in, l.m_in, AV_BUFFERSRC_FLAG_KEEP_REF) < 0)
        return false;

    if (av_buffersink_get_frame(this->m_out, l.m_out) < 0)
        return false;

    AVPixelFormat outFormat = (AVPixelFormat)l.m_out->format;

    av_image_alloc(out->data, out->linesize, width, height, outFormat, 1);
    av_image_copy(out->data, out->linesize, (const uint8_t**)l.m_out->data, l.m_out->linesize, outFormat, width, height);

    *retFormat = outFormat;

    return true;
}

bool FilterGraph::hasFilters()
{
    QMutexLocker lock(&this->m_lock);

    return this->m_filters.count() > 2;
}

int FilterGraph::getDelayCount() const
{
    return this->m_delayCount;
}

void FilterGraph::unInitFilter()
{
    if (this->m_graph)
        avfilter_graph_free(&this->m_graph);

    this->m_in = NULL;
    this->m_out = NULL;

    this->m_hqdn3d.filter = NULL;
    this->m_histeq.filter = NULL;
    this->m_deband.filter = NULL;
    this->m_atadenoise.filter = NULL;
    this->m_owdenoise.filter = NULL;

    this->m_delayCount = 0;

    this->m_filters.clear();
}

bool FilterGraph::link()
{
    if (this->m_filters.count() < 3)
        return false;

    AVFilterContext *prev = this->m_filters.first();

    for (int i = 1; i < this->m_filters.count(); i++)
    {
        AVFilterContext *next = this->m_filters[i];

        if (avfilter_link(prev, 0, next, 0) < 0)
            return false;

        prev = next;
    }

    return true;
}

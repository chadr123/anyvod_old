﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "ShaderCompositer.h"
#include "core/Utils.h"

#include <QFile>
#include <QDebug>

const QString ShaderCompositer::ENTRY_HEADER = "  ";
const QString ShaderCompositer::ENTRY_FOOTER = "(texel, coord);\n";
const QString ShaderCompositer::ENTRY_SIG = "ENTRIES";
const QString ShaderCompositer::FUNCS_SIG = "FUNCS";

ShaderCompositer::ShaderCompositer() :
#if defined Q_OS_MOBILE
    m_vertexFrame(
        "uniform mat4 AnyVOD_projection;\n"
        "uniform mat4 AnyVOD_modelView;\n"
        "\n"
        "attribute vec4 AnyVOD_vertex;\n"
        "\n"
        "attribute vec2 AnyVOD_inTexCoords;\n"
        "varying vec2 AnyVOD_outTexCoords;\n"
        "\n"
        "uniform vec4 AnyVOD_inColor;\n"
        "varying vec4 AnyVOD_outColor;\n"
        "\n"
        "void main()\n"
        "{\n"
        "  AnyVOD_outTexCoords = AnyVOD_inTexCoords;\n"
        "  AnyVOD_outColor = AnyVOD_inColor;\n"
        "\n"
        "  gl_Position = AnyVOD_projection * AnyVOD_modelView * AnyVOD_vertex;\n"
        "}\n"
        "\n"
        ),
#endif
    m_shaderLib(
#if defined Q_OS_MAC && !defined Q_OS_IOS
        "#extension GL_EXT_gpu_shader4 : enable\n"
        "\n"
#endif
#if defined Q_OS_MOBILE
        "precision highp float;\n"
        "precision highp sampler2D;\n"
        "\n"
        "varying vec2 AnyVOD_outTexCoords;\n"
        "varying vec4 AnyVOD_outColor;\n"
        "\n"
#endif
        "uniform sampler2D AnyVOD_tex0;\n"
        "uniform sampler2D AnyVOD_tex1;\n"
        "uniform sampler2D AnyVOD_tex2;\n"
        "\n"
        "uniform bool AnyVOD_useGPUConvert;\n"
        "uniform int AnyVOD_pixFormat;\n"
        "\n"
        "bool AnyVOD_isEven(in int value)\n"
        "{\n"
#if defined Q_OS_MAC && !defined Q_OS_IOS
        "  return value % 2 == 0;\n"
#else
        "  return mod(float(value), 2.0) == 0.0;\n"
#endif
        "}\n"
        "\n"
        "vec4 AnyVOD_GetRGBFromYUV(in float y, in float u, in float v)\n"
        "{\n"
        "  float r = y + 1.402 * v;\n"
        "  float g = y - 0.34414 * u - 0.71414 * v;\n"
        "  float b = y + 1.772 * u;\n"
        "\n"
        "  return vec4(r, g, b, 1.0);\n"
        "}\n"
        "\n"
        "vec4 AnyVOD_YUV420P(in vec2 coord)\n"
        "{\n"
        "  float y = texture2D(AnyVOD_tex0, coord).r;\n"
        "  float u = texture2D(AnyVOD_tex1, coord).r - 0.5;\n"
        "  float v = texture2D(AnyVOD_tex2, coord).r - 0.5;\n"
        "\n"
        "  return AnyVOD_GetRGBFromYUV(y, u, v);\n"
        "}\n"
        "\n"
        "vec4 AnyVOD_NV12orNV21(in vec2 coord, in bool isNV12)\n"
        "{\n"
        "  vec4 uv = texture2D(AnyVOD_tex1, coord / 2.0);\n"
        "  float y = texture2D(AnyVOD_tex0, coord).r;\n"
        "  float u = -0.5;\n"
        "  float v = -0.5;\n"
        "\n"
        "  if (isNV12)\n"
        "  {\n"
        "    u += uv.r;\n"
        "    v += uv.a;\n"
        "  }\n"
        "  else\n"
        "  {\n"
        "    u += uv.a;\n"
        "    v += uv.r;\n"
        "  }\n"
        "\n"
        "  return AnyVOD_GetRGBFromYUV(y, u, v);\n"
        "}\n"
        "\n"
        "vec4 AnyVOD_getColor()\n"
        "{\n"
#if defined Q_OS_MOBILE
        "  return AnyVOD_outColor;\n"
#else
        "  return gl_Color;\n"
#endif
        "}\n"
        "\n"
        "vec4 AnyVOD_getTexel(in vec2 coord)\n"
        "{\n"
        "  if (AnyVOD_useGPUConvert)\n"
        "  {\n"
        "    if (AnyVOD_pixFormat == " + QString::number(AV_PIX_FMT_YUV420P) + ")\n"
        "      return AnyVOD_YUV420P(coord);\n"
        "    else if (AnyVOD_pixFormat == " + QString::number(AV_PIX_FMT_NV12) + ")\n"
        "      return AnyVOD_NV12orNV21(coord, true);\n"
        "    else if (AnyVOD_pixFormat == " + QString::number(AV_PIX_FMT_NV21) + ")\n"
        "      return AnyVOD_NV12orNV21(coord, false);\n"
        "    else\n"
        "      return texture2D(AnyVOD_tex0, coord);\n"
        "  }\n"
        "  else\n"
        "  {\n"
        "    return texture2D(AnyVOD_tex0, coord);\n"
        "  }\n"
        "}\n"
        "\n"
        "vec2 AnyVOD_getCoord()\n"
        "{\n"
#if defined Q_OS_MOBILE
        "  return AnyVOD_outTexCoords.st;\n"
#else
        "  return gl_TexCoord[0].st;\n"
#endif
        "}\n"
        "\n"
        ),
    m_shaderFrame(
        m_shaderLib +
        "uniform float AnyVOD_Hue;\n"
        "uniform float AnyVOD_Saturation;\n"
        "uniform float AnyVOD_Contrast;\n"
        "uniform float AnyVOD_Brightness;\n"
        "\n"
        "uniform float AnyVOD_Width;\n"
        "uniform float AnyVOD_Height;\n"
        "uniform float AnyVOD_Clock;\n"
        "uniform vec3 AnyVOD_LumAvg;\n"
        "uniform vec2 AnyVOD_TexSize;\n"
        "\n"
        "uniform bool AnyVOD_useLeftRightInvert;\n"
        "uniform bool AnyVOD_useTopBottomInvert;\n"
        "\n"
        "uniform bool AnyVOD_leftOrTop;\n"
        "uniform bool AnyVOD_rowOrCol;\n"
        "uniform bool AnyVOD_sideBySide;\n"
        "uniform vec2 AnyVOD_screenCoord;\n"
        "uniform mat3 AnyVOD_anaglyphFirstMatrix;\n"
        "uniform mat3 AnyVOD_anaglyphSecondMatrix;\n"
        "\n"
        "vec2 AnyVOD_leftSBS(in vec2 coord)\n"
        "{\n"
        "  return vec2(coord.s / 2.0, coord.t);\n"
        "}\n"
        "\n"
        "vec2 AnyVOD_rightSBS(in vec2 coord)\n"
        "{\n"
        "  return vec2((AnyVOD_screenCoord.s + coord.s) / 2.0, coord.t);\n"
        "}\n"
        "\n"
        "vec2 AnyVOD_topTAB(in vec2 coord)\n"
        "{\n"
        "  return vec2(coord.s, coord.t / 2.0);\n"
        "}\n"
        "\n"
        "vec2 AnyVOD_bottomTAB(in vec2 coord)\n"
        "{\n"
        "  return vec2(coord.s, (AnyVOD_screenCoord.t + coord.t) / 2.0);\n"
        "}\n"
        "\n"
        "void AnyVOD_leftOrRight(inout vec2 coord, in bool left)\n"
        "{\n"
        "  if (left)\n"
        "    coord = AnyVOD_leftSBS(coord);\n"
        "  else\n"
        "    coord = AnyVOD_rightSBS(coord);\n"
        "}\n"
        "\n"
        "void AnyVOD_topOrBottom(inout vec2 coord, in bool top)\n"
        "{\n"
        "  if (top)\n"
        "    coord = AnyVOD_topTAB(coord);\n"
        "  else\n"
        "    coord = AnyVOD_bottomTAB(coord);\n"
        "}\n"
        "\n"
        + FUNCS_SIG +
        "\n"
        "void main()\n"
        "{\n"
        "  vec2 coord;\n"
        "  vec4 texel;\n"
        "\n"
        "  coord = AnyVOD_getCoord();\n"
        "  texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);\n"
        "\n"
        + ENTRY_SIG +
        "\n"
        "  gl_FragColor = texel;\n"
        "}\n"
        "\n"
        ),
    m_effect(
        "void AnyVOD_sharply(inout vec4 texel, inout vec2 coord)\n"
        "{\n"
        "  const float effect_width = 1.6;\n"
        "  const float positive = 2.0;\n"
        "  const float negative = -0.125;\n"
        "\n"
        "  float dx = effect_width / AnyVOD_Width;\n"
        "  float dy = effect_width / AnyVOD_Height;\n"
        "\n"
        "  vec4 c1 = AnyVOD_getTexel(coord + vec2(-dx, -dy)) * negative;\n"
        "  vec4 c2 = AnyVOD_getTexel(coord + vec2(0.0, -dy)) * negative;\n"
        "  vec4 c3 = AnyVOD_getTexel(coord + vec2(-dx, 0.0)) * negative;\n"
        "  vec4 c4 = AnyVOD_getTexel(coord + vec2(dx, 0.0)) * negative;\n"
        "  vec4 c5 = AnyVOD_getTexel(coord + vec2(0.0, dy)) * negative;\n"
        "  vec4 c6 = AnyVOD_getTexel(coord + vec2(dx, dy)) * negative;\n"
        "  vec4 c7 = AnyVOD_getTexel(coord + vec2(-dx, dy)) * negative;\n"
        "  vec4 c8 = AnyVOD_getTexel(coord + vec2(dx, -dy)) * negative;\n"
        "  vec4 c9 = texel * positive;\n"
        "\n"
        "  texel = c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9;\n"
        "}\n"
        "\n"
        "void AnyVOD_sharpen(inout vec4 texel, inout vec2 coord)\n"
        "{\n"
        "  const float threshold = 0.0009;\n"
        "  const float intensity = 0.5;\n"
        "  const vec4 mask = vec4(0.299, 0.587, 0.114, 0.0);\n"
        "\n"
        "  float dx = 4.0 / AnyVOD_Width;\n"
        "  float dy = 4.0 / AnyVOD_Height;\n"
        "\n"
        "  vec4 c1 = AnyVOD_getTexel(coord + vec2(-dx, dy));\n"
        "  vec4 c2 = AnyVOD_getTexel(coord + vec2(0.0, dy));\n"
        "  vec4 c3 = AnyVOD_getTexel(coord + vec2(dx, dy));\n"
        "  vec4 c4 = AnyVOD_getTexel(coord + vec2(-dx, 0.0));\n"
        "  vec4 c5 = AnyVOD_getTexel(coord + vec2(0.0, 0.0));\n"
        "  vec4 c6 = AnyVOD_getTexel(coord + vec2(dx, 0.0));\n"
        "  vec4 c7 = AnyVOD_getTexel(coord + vec2(-dx, -dy));\n"
        "  vec4 c8 = AnyVOD_getTexel(coord + vec2(0.0, -dy));\n"
        "  vec4 c9 = AnyVOD_getTexel(coord + vec2(dx, -dy));\n"
        "\n"
        "  vec4 c10 = (2.0 * (c2 + c4 + c6 + c8) + (c1 + c3 + c7 + c9) + 4.0 * c5) / 16.0;\n"
        "  float result = abs(dot(c10 - c5, mask));\n"
        "  vec4 c11 = vec4(result, result, result, result);\n"
        "  vec4 c0;\n"
        "\n"
        "  if (c11.x < threshold)\n"
        "   c0 = c5;\n"
        "  else\n"
        "   c0 = c5 + intensity * (c5 - c10);\n"
        "\n"
        "  if ((dot(c1, mask) < 0.067 && dot(c2, mask) < 0.067 && dot(c3, mask) > -0.5005) ||\n"
        "       (dot(c7, mask) < 0.067 && dot(c8, mask) < 0.067 && dot(c9, mask) > 0.5005))\n"
        "  {\n"
        "    c0 = c5;\n"
        "  }\n"
        "\n"
        "  texel = c0;\n"
        "}\n"
        "\n"
        "void AnyVOD_soften(inout vec4 texel, inout vec2 coord)\n"
        "{\n"
        "  const float threshold = 0.3;\n"
        "  const float inc = 0.6;\n"
        "\n"
        "  float dx = 1.0 / AnyVOD_Width;\n"
        "  float dy = 1.0 / AnyVOD_Height;\n"
        "  vec4 total = texel;\n"
        "  float n = 1.0;\n"
        "  vec4 c[8];\n"
        "\n"
        "  c[0] = AnyVOD_getTexel(coord + vec2(-dx, -dy));\n"
        "  c[1] = AnyVOD_getTexel(coord + vec2(0.0, -dy));\n"
        "  c[2] = AnyVOD_getTexel(coord + vec2(dx, -dy));\n"
        "  c[3] = AnyVOD_getTexel(coord + vec2(-dx, 0.0));\n"
        "  c[4] = AnyVOD_getTexel(coord + vec2(dx, 0.0));\n"
        "  c[5] = AnyVOD_getTexel(coord + vec2(-dx, dy));\n"
        "  c[6] = AnyVOD_getTexel(coord + vec2(0.0, dy));\n"
        "  c[7] = AnyVOD_getTexel(coord + vec2(dx, dy));\n"
        "\n"
        "  for (int i = 0; i < 7; i++)\n"
        "  {\n"
        "    if (length(texel - c[i]) < threshold)\n"
        "    {\n"
        "      total += c[i] * inc;\n"
        "      n += inc;\n"
        "    }\n"
        "  }\n"
        "\n"
        "  texel = total / n;\n"
        "}\n"
        "\n"
        "void AnyVOD_invert(inout vec4 texel, inout vec2 coord)\n"
        "{\n"
        "  if (AnyVOD_useLeftRightInvert)\n"
#if defined Q_OS_MOBILE
        "    coord.s = 1.0 - coord.s;\n"
#else
        "    coord.s = (AnyVOD_Width / AnyVOD_TexSize.s) - coord.s;\n"
#endif
        "\n"
        "  if (AnyVOD_useTopBottomInvert)\n"
#if defined Q_OS_MOBILE
        "    coord.t = 1.0 - coord.t;\n"
#else
        "    coord.t = (AnyVOD_Height / AnyVOD_TexSize.t) - coord.t;\n"
#endif
        "\n"
        "  texel = AnyVOD_getTexel(coord);\n"
        "}\n"
        "\n"
        ),
    m_interlace3D(
        "void AnyVOD_interlace3DTABSub(in bool rowEven, inout vec2 coord)\n"
        "{\n"
        "  if (rowEven)\n"
        "    AnyVOD_topOrBottom(coord, AnyVOD_leftOrTop);\n"
        "  else\n"
        "    AnyVOD_topOrBottom(coord, !AnyVOD_leftOrTop);\n"
        "}\n"
        "\n"
        "void AnyVOD_interlace3DSBSSub(in bool rowEven, inout vec2 coord)\n"
        "{\n"
        "  if (rowEven)\n"
        "    AnyVOD_leftOrRight(coord, AnyVOD_leftOrTop);\n"
        "  else\n"
        "    AnyVOD_leftOrRight(coord, !AnyVOD_leftOrTop);\n"
        "}\n"
        "\n"
        "void AnyVOD_interlace3DSub(inout vec2 coord, in int fragCoord)\n"
        "{\n"
        "  if (AnyVOD_sideBySide)\n"
        "    AnyVOD_interlace3DSBSSub(AnyVOD_isEven(fragCoord), coord);\n"
        "  else\n"
        "    AnyVOD_interlace3DTABSub(AnyVOD_isEven(fragCoord), coord);\n"
        "}\n"
        "\n"
        "void AnyVOD_interlace3D(inout vec4 texel, inout vec2 coord)\n"
        "{\n"
        "  ivec2 ticoord = ivec2(gl_FragCoord.xy);\n"
        "\n"
        "  if (AnyVOD_rowOrCol)\n"
        "    AnyVOD_interlace3DSub(coord, ticoord.y);\n"
        "  else\n"
        "    AnyVOD_interlace3DSub(coord, ticoord.x);\n"
        "\n"
        "  texel = AnyVOD_getTexel(coord);\n"
        "}\n"
        "\n"
        ),
    m_checkerBoard(
        "void AnyVOD_checkerBoardTABSub(in bool rowEven, in int colCoord, inout vec2 coord)\n"
        "{\n"
        "  if (rowEven)\n"
        "    AnyVOD_topOrBottom(coord, AnyVOD_isEven(colCoord));\n"
        "  else\n"
        "    AnyVOD_topOrBottom(coord, !AnyVOD_isEven(colCoord));\n"
        "}\n"
        "\n"
        "void AnyVOD_checkerBoardSBSSub(in bool rowEven, in int colCoord, inout vec2 coord)\n"
        "{\n"
        "  if (rowEven)\n"
        "    AnyVOD_leftOrRight(coord, AnyVOD_isEven(colCoord));\n"
        "  else\n"
        "    AnyVOD_leftOrRight(coord, !AnyVOD_isEven(colCoord));\n"
        "}\n"
        "\n"
        "void AnyVOD_checkerBoardSub(inout vec2 coord, in ivec2 fragCoord)\n"
        "{\n"
        "  if (AnyVOD_sideBySide)\n"
        "  {\n"
        "    if (AnyVOD_leftOrTop)\n"
        "      AnyVOD_checkerBoardSBSSub(AnyVOD_isEven(fragCoord.y), fragCoord.x, coord);\n"
        "    else\n"
        "      AnyVOD_checkerBoardSBSSub(!AnyVOD_isEven(fragCoord.y), fragCoord.x, coord);\n"
        "  }\n"
        "  else\n"
        "  {\n"
        "    if (AnyVOD_leftOrTop)\n"
        "      AnyVOD_checkerBoardTABSub(AnyVOD_isEven(fragCoord.y), fragCoord.x, coord);\n"
        "    else\n"
        "      AnyVOD_checkerBoardTABSub(!AnyVOD_isEven(fragCoord.y), fragCoord.x, coord);\n"
        "  }\n"
        "}\n"
        "\n"
        "void AnyVOD_checkerBoard(inout vec4 texel, inout vec2 coord)\n"
        "{\n"
        "  ivec2 ticoord = ivec2(gl_FragCoord.xy);\n"
        "\n"
        "  AnyVOD_checkerBoardSub(coord, ticoord);\n"
        "  texel = AnyVOD_getTexel(coord);\n"
        "}\n"
        "\n"
        ),
    m_anaglyph(
        "void AnyVOD_splitScreenSBSSub(in vec2 coord, out vec2 firstCoord, out vec2 secondCoord)\n"
        "{\n"
        "  firstCoord = AnyVOD_leftSBS(coord);\n"
        "  secondCoord = AnyVOD_rightSBS(coord);\n"
        "}\n"
        "\n"
        "void AnyVOD_splitScreenTABSub(in vec2 coord, out vec2 firstCoord, out vec2 secondCoord)\n"
        "{\n"
        "  firstCoord = AnyVOD_topTAB(coord);\n"
        "  secondCoord = AnyVOD_bottomTAB(coord);\n"
        "}\n"
        "\n"
        "void AnyVOD_splitScreen(inout vec4 texel, inout vec2 coord, out vec4 firstTexel, out vec4 secondTexel)\n"
        "{\n"
        "  vec2 firstCoord;\n"
        "  vec2 secondCoord;\n"
        "\n"
        "  if (AnyVOD_sideBySide)\n"
        "  {\n"
        "    if (AnyVOD_leftOrTop)\n"
        "      AnyVOD_splitScreenSBSSub(coord, firstCoord, secondCoord);\n"
        "    else\n"
        "      AnyVOD_splitScreenSBSSub(coord, secondCoord, firstCoord);\n"
        "  }\n"
        "  else\n"
        "  {\n"
        "    if (AnyVOD_leftOrTop)\n"
        "      AnyVOD_splitScreenTABSub(coord, firstCoord, secondCoord);\n"
        "    else\n"
        "      AnyVOD_splitScreenTABSub(coord, secondCoord, firstCoord);\n"
        "  }\n"
        "\n"
        "  firstTexel = AnyVOD_getTexel(firstCoord);\n"
        "  secondTexel = AnyVOD_getTexel(secondCoord);\n"
        "}\n"
        "\n"
        "void AnyVOD_anaglyph(inout vec4 texel, inout vec2 coord)\n"
        "{\n"
        "  vec4 firstTexel;\n"
        "  vec4 secondTexel;\n"
        "\n"
        "  AnyVOD_splitScreen(texel, coord, firstTexel, secondTexel);\n"
        "\n"
        "  texel.rgb = AnyVOD_anaglyphFirstMatrix * firstTexel.rgb + AnyVOD_anaglyphSecondMatrix * secondTexel.rgb;\n"
        "  texel.a = firstTexel.a;\n"
        "\n"
        "  texel = clamp(texel, 0.0, 1.0);\n"
        "}\n"
        "\n"
        ),
    m_subtitleInterlace(
        m_shaderLib +
        "uniform bool AnyVOD_firstFrame;\n"
        "uniform bool AnyVOD_rowOrCol;\n"
        "\n"
        "void AnyVOD_interlace3DSubtitleSub(inout vec4 texel, in int fragCoord, in vec2 coord)\n"
        "{\n"
        "  if (AnyVOD_isEven(fragCoord))\n"
        "  {\n"
        "    if (AnyVOD_firstFrame)\n"
        "       texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);\n"
        "    else\n"
        "       texel = vec4(0.0, 0.0, 0.0, 0.0);\n"
        "  }\n"
        "  else\n"
        "  {\n"
        "    if (!AnyVOD_firstFrame)\n"
        "       texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);\n"
        "    else\n"
        "       texel = vec4(0.0, 0.0, 0.0, 0.0);\n"
        "  }\n"
        "}\n"
        "\n"
        "void AnyVOD_interlace3DSubtitle(inout vec4 texel, in vec2 coord)\n"
        "{\n"
        "  ivec2 ticoord = ivec2(gl_FragCoord.xy);\n"
        "\n"
        "  if (AnyVOD_rowOrCol)\n"
        "    AnyVOD_interlace3DSubtitleSub(texel, ticoord.y, coord);\n"
        "  else\n"
        "    AnyVOD_interlace3DSubtitleSub(texel, ticoord.x, coord);\n"
        "}\n"
        "\n"
        "void main()\n"
        "{\n"
        "  AnyVOD_interlace3DSubtitle(gl_FragColor, AnyVOD_getCoord());\n"
        "}\n"
        "\n"
        ),
    m_subtitleCheckerBoard(
        m_shaderLib +
        "uniform bool AnyVOD_firstFrame;\n"
        "\n"
        "void AnyVOD_checkerBoardSubtitleSub(inout vec4 texel, in ivec2 fragCoord, in vec2 coord)\n"
        "{\n"
        "  if (AnyVOD_isEven(fragCoord.y))\n"
        "  {\n"
        "    if (AnyVOD_isEven(fragCoord.x))\n"
        "    {\n"
        "      if (AnyVOD_firstFrame)\n"
        "         texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);\n"
        "      else\n"
        "         texel = vec4(0.0, 0.0, 0.0, 0.0);\n"
        "    }\n"
        "    else\n"
        "    {\n"
        "      if (!AnyVOD_firstFrame)\n"
        "         texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);\n"
        "      else\n"
        "         texel = vec4(0.0, 0.0, 0.0, 0.0);\n"
        "    }\n"
        "  }\n"
        "  else\n"
        "  {\n"
        "    if (AnyVOD_isEven(fragCoord.x))\n"
        "    {\n"
        "      if (!AnyVOD_firstFrame)\n"
        "         texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);\n"
        "      else\n"
        "         texel = vec4(0.0, 0.0, 0.0, 0.0);\n"
        "    }\n"
        "    else\n"
        "    {\n"
        "      if (AnyVOD_firstFrame)\n"
        "         texel = AnyVOD_getColor() * AnyVOD_getTexel(coord);\n"
        "      else\n"
        "         texel = vec4(0.0, 0.0, 0.0, 0.0);\n"
        "    }\n"
        "  }\n"
        "}\n"
        "\n"
        "void AnyVOD_checkerBoardSubtitle(inout vec4 texel, in vec2 coord)\n"
        "{\n"
        "  ivec2 ticoord = ivec2(gl_FragCoord.xy);\n"
        "\n"
        "  AnyVOD_checkerBoardSubtitleSub(texel, ticoord, coord);\n"
        "}\n"
        "\n"
        "void main()\n"
        "{\n"
        "  AnyVOD_checkerBoardSubtitle(gl_FragColor, AnyVOD_getCoord());\n"
        "}\n"
        "\n"
        ),
    m_subtitleAnaglyph(
        m_shaderLib +
        "uniform mat3 AnyVOD_anaglyphMatrix;\n"
        "\n"
        "void main()\n"
        "{\n"
        "  vec4 texel;\n"
        "\n"
        "  texel = AnyVOD_getTexel(AnyVOD_getCoord());\n"
        "\n"
        "  texel.rgb = AnyVOD_anaglyphMatrix * texel.rgb;\n"
        "  texel = AnyVOD_getColor() * clamp(texel, 0.0, 1.0);\n"
        "\n"
        "  gl_FragColor = texel;\n"
        "}\n"
        "\n"
        ),
    m_barrelDistortion(
        m_shaderLib +
        "uniform vec2 AnyVOD_texRange;\n"
        "uniform vec2 AnyVOD_lensCenterOffset;\n"
        "uniform vec2 AnyVOD_coefficients;\n"
        "uniform float AnyVOD_fillScale;\n"
        "\n"
        "float AnyVOD_distortionScale(in vec2 offset)\n"
        "{\n"
        "  // Refer to https://en.wikipedia.org/wiki/Distortion_(optics)#Software_correction\n"
        "  vec2 offsetSquared = offset * offset;\n"
        "  float radiusSquared = offsetSquared.x + offsetSquared.y;\n"
        "  float distortionScale =\n"
        "    1.0 +\n"
        "    AnyVOD_coefficients[0] * radiusSquared +\n"
        "    AnyVOD_coefficients[1] * radiusSquared * radiusSquared;\n"
        "\n"
        "  return distortionScale;\n"
        "}\n"
        "\n"
        "vec2 AnyVOD_texCoordsToDistortionCoords(in vec2 texCoord)\n"
        "{\n"
        "  //Convert the texture coordinates from \"0 to 1\" to \"-1 to 1\"\n"
        "  vec2 result = texCoord * 2.0 - 1.0;\n"
        "\n"
        "  //Convert from using the center of the screen as the origin to\n"
        "  //using the lens center as the origin\n"
        "  result -= AnyVOD_lensCenterOffset;\n"
        "\n"
        "  return result;\n"
        "}\n"
        "\n"
        "vec2 AnyVOD_distortionCoordsToTexCoords(in vec2 offset)\n"
        "{\n"
        "  //Scale the distorted result so that we fill the desired amount of pixel real-estate\n"
        "  vec2 result = offset / AnyVOD_fillScale;\n"
        "\n"
        "  //Convert from using the lens center as the origin to\n"
        "  //using the screen center as the origin\n"
        "  result += AnyVOD_lensCenterOffset;\n"
        "\n"
        "  // Convert the texture coordinates from \"-1 to 1\" to \"0 to 1\"\n"
        "  result += 1.0;\n"
        "  result /= 2.0;\n"
        "\n"
        "  return result;\n"
        "}\n"
        "\n"
        "void main()\n"
        "{\n"
        "  vec4 texel;\n"
        "  vec2 offset = AnyVOD_texCoordsToDistortionCoords(AnyVOD_getCoord());\n"
        "  float scale = AnyVOD_distortionScale(offset);\n"
        "  vec2 distortedOffset = offset * scale;\n"
        "  vec2 actualTextureCoords = AnyVOD_distortionCoordsToTexCoords(distortedOffset) * AnyVOD_texRange;\n"
        "  vec2 clamped = clamp(actualTextureCoords, vec2(0.0, 0.0), AnyVOD_texRange);\n"
        "\n"
        "  if (!all(equal(clamped, actualTextureCoords)))\n"
        "    texel = vec4(0.0, 0.0, 0.0, 1.0);\n"
        "  else\n"
        "    texel = AnyVOD_getTexel(actualTextureCoords);\n"
        "\n"
        "  texel = AnyVOD_getColor() * clamp(texel, 0.0, 1.0);\n"
        "\n"
        "  gl_FragColor = texel;\n"
        "}\n"
        "\n"
        ),
    m_simple(
        m_shaderLib +
        "void main()\n"
        "{\n"
        "  vec4 texel;\n"
        "\n"
        "  texel = AnyVOD_getColor() * AnyVOD_getTexel(AnyVOD_getCoord());\n"
        "\n"
        "  gl_FragColor = texel;\n"
        "}\n"
        "\n"
        ),
    m_default(
        "vec3 AnyVOD_rgbTohsl(in vec3 color)\n"
        "{\n"
        "  vec3 hsl;\n"
        "\n"
        "  float minValue = min(min(color.r, color.g), color.b);\n"
        "  float maxValue = max(max(color.r, color.g), color.b);\n"
        "  float delta = maxValue - minValue;\n"
        "  float halfDelta = delta / 2.0;\n"
        "\n"
        "  hsl.z = (maxValue + minValue) / 2.0;\n"
        "\n"
        "  if (delta == 0.0)\n"
        "  {\n"
        "    hsl.x = -1.0;\n"
        "    hsl.y = 0.0;\n"
        "  }\n"
        "  else\n"
        "  {\n"
        "    if (hsl.z < 0.5)\n"
        "      hsl.y = delta / (maxValue + minValue);\n"
        "    else\n"
        "      hsl.y = delta / (2.0 - maxValue - minValue);\n"
        "\n"
        "    float deltaR = (((maxValue - color.r) / 6.0) + halfDelta) / delta;\n"
        "    float deltaG = (((maxValue - color.g) / 6.0) + halfDelta) / delta;\n"
        "    float deltaB = (((maxValue - color.b) / 6.0) + halfDelta) / delta;\n"
        "\n"
        "    if (color.r == maxValue)\n"
        "      hsl.x = deltaB - deltaG;\n"
        "    else if (color.g == maxValue)\n"
        "      hsl.x = (1.0 / 3.0) + deltaR - deltaB;\n"
        "    else if (color.b == maxValue)\n"
        "      hsl.x = (2.0 / 3.0) + deltaG - deltaR;\n"
        "    else\n"
        "      hsl.x = 0.0;\n"
        "\n"
        "    if (hsl.x < 0.0)\n"
        "      hsl.x += 1.0;\n"
        "    else if (hsl.x > 1.0)\n"
        "      hsl.x -= 1.0;\n"
        "  }\n"
        "\n"
        "  return hsl;\n"
        "}\n"
        "\n"
        "float AnyVOD_hueToValue(in float f1, in float f2, in float hue)\n"
        "{\n"
        "  if (hue < 0.0)\n"
        "    hue += 1.0;\n"
        "  else if (hue > 1.0)\n"
        "    hue -= 1.0;\n"
        "\n"
        "  float res;\n"
        "\n"
        "  if ((6.0 * hue) < 1.0)\n"
        "    res = f1 + (f2 - f1) * 6.0 * hue;\n"
        "  else if ((2.0 * hue) < 1.0)\n"
        "    res = f2;\n"
        "  else if ((3.0 * hue) < 2.0)\n"
        "    res = f1 + (f2 - f1) * ((2.0 / 3.0) - hue) * 6.0;\n"
        "  else\n"
        "    res = f1;\n"
        "\n"
        "  return res;\n"
        "}\n"
        "\n"
        "vec3 AnyVOD_hslTorgb(in vec3 hsl)\n"
        "{\n"
        "  vec3 rgb;\n"
        "\n"
        "  if (hsl.y == 0.0)\n"
        "  {\n"
        "    rgb = vec3(hsl.z);\n"
        "  }\n"
        "  else\n"
        "  {\n"
        "    float f1;\n"
        "    float f2;\n"
        "\n"
        "    if (hsl.z < 0.5)\n"
        "      f2 = hsl.z * (1.0 + hsl.y);\n"
        "    else\n"
        "      f2 = (hsl.z + hsl.y) - (hsl.y * hsl.z);\n"
        "\n"
        "    f1 = 2.0 * hsl.z - f2;\n"
        "\n"
        "    rgb.r = AnyVOD_hueToValue(f1, f2, hsl.x + (1.0 / 3.0));\n"
        "    rgb.g = AnyVOD_hueToValue(f1, f2, hsl.x);\n"
        "    rgb.b = AnyVOD_hueToValue(f1, f2, hsl.x - (1.0 / 3.0));\n"
        "  }\n"
        "\n"
        "  return rgb;\n"
        "}\n"
        "\n"
        "void AnyVOD_hue(inout vec4 texel, inout vec2 coord)\n"
        "{\n"
        "  if (AnyVOD_Hue == 0.0)\n"
        "    return;\n"
        "\n"
        "  vec3 hsl = AnyVOD_rgbTohsl(texel.rgb);\n"
        "\n"
        "  hsl.x += AnyVOD_Hue;\n"
        "  texel.rgb = AnyVOD_hslTorgb(hsl);\n"
        "}\n"
        "\n"
        "void AnyVOD_saturation_contrast(inout vec4 texel, inout vec2 coord)\n"
        "{\n"
        "  if (AnyVOD_Saturation == 1.0 && AnyVOD_Contrast == 1.0)\n"
        "    return;\n"
        "\n"
        "  const vec3 lumCoeff = vec3(0.2125, 0.7154, 0.0721);\n"
        "  vec3 intensity = vec3(dot(texel.rgb, lumCoeff));\n"
        "  vec3 satColor = mix(intensity, texel.rgb, AnyVOD_Saturation);\n"
        "  vec3 conColor = mix(AnyVOD_LumAvg, satColor, AnyVOD_Contrast);\n"
        "\n"
        "  conColor = clamp(conColor, 0.0, 1.0);\n"
        "  texel.rgb = conColor;\n"
        "}\n"
        "\n"
        "void AnyVOD_brightness(inout vec4 texel, inout vec2 coord)\n"
        "{\n"
        "  if (AnyVOD_Brightness == 1.0)\n"
        "    return;\n"
        "\n"
        "  texel.rgb *= AnyVOD_Brightness;\n"
        "  texel = clamp(texel, 0.0, 1.0);\n"
        "}\n"
        "\n"
        )
{

}

void ShaderCompositer::getAnaglyphMatrix(float matFirst[3][3], float matSecond[3][3]) const
{
    switch (this->m_defaultParam.method3D)
    {
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            switch (this->m_defaultParam.anaglyphAlgorithm)
            {
                case AnyVODEnums::AGA_GRAY:
                {
                    matFirst[0][0] = 0.299f;
                    matFirst[1][0] = 0.587f;
                    matFirst[2][0] = 0.114f;
                    matFirst[0][1] = 0.0f;
                    matFirst[1][1] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[0][1] = 0.299f;
                    matSecond[1][1] = 0.587f;
                    matSecond[2][1] = 0.114f;
                    matSecond[0][2] = 0.299f;
                    matSecond[1][2] = 0.587f;
                    matSecond[2][2] = 0.114f;

                    break;
                }
                case AnyVODEnums::AGA_COLORED:
                {
                    matFirst[0][0] = 1.0f;
                    matFirst[1][0] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[0][1] = 0.0f;
                    matFirst[1][1] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[1][1] = 1.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][2] = 1.0f;

                    break;
                }
                case AnyVODEnums::AGA_HALF_COLORED:
                {
                    matFirst[0][0] = 0.299f;
                    matFirst[1][0] = 0.587f;
                    matFirst[2][0] = 0.114f;
                    matFirst[0][1] = 0.0f;
                    matFirst[1][1] = 0.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[1][1] = 1.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][2] = 1.0f;

                    break;
                }
                case AnyVODEnums::AGA_DUBOIS:
                {
                    matFirst[0][0] = 0.437f;
                    matFirst[1][0] = 0.449f;
                    matFirst[2][0] = 0.164f;
                    matFirst[0][1] = -0.062f;
                    matFirst[1][1] = -0.062f;
                    matFirst[2][1] = -0.024f;
                    matFirst[0][2] = -0.048f;
                    matFirst[1][2] = -0.050f;
                    matFirst[2][2] = -0.017f;

                    matSecond[0][0] = -0.011f;
                    matSecond[1][0] = -0.032f;
                    matSecond[2][0] = -0.007f;
                    matSecond[0][1] = 0.377f;
                    matSecond[1][1] = 0.761f;
                    matSecond[2][1] = 0.009f;
                    matSecond[0][2] = -0.026f;
                    matSecond[1][2] = -0.093f;
                    matSecond[2][2] = 1.234f;

                    break;
                }
                default:
                {
                    break;
                }
            }

            break;
        }
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            switch (this->m_defaultParam.anaglyphAlgorithm)
            {
                case AnyVODEnums::AGA_GRAY:
                {
                    matFirst[0][0] = 0.0f;
                    matFirst[1][0] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[0][1] = 0.299f;
                    matFirst[1][1] = 0.587f;
                    matFirst[2][1] = 0.114f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.299f;
                    matSecond[1][0] = 0.587f;
                    matSecond[2][0] = 0.114f;
                    matSecond[0][1] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[0][2] = 0.299f;
                    matSecond[1][2] = 0.587f;
                    matSecond[2][2] = 0.114f;

                    break;
                }
                case AnyVODEnums::AGA_COLORED:
                {
                    matFirst[0][0] = 0.0f;
                    matFirst[1][0] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[0][1] = 0.0f;
                    matFirst[1][1] = 1.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 1.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][2] = 1.0f;

                    break;
                }
                case AnyVODEnums::AGA_HALF_COLORED:
                {
                    matFirst[0][0] = 0.0f;
                    matFirst[1][0] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[0][1] = 0.299f;
                    matFirst[1][1] = 0.587f;
                    matFirst[2][1] = 0.114f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 1.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][2] = 1.0f;

                    break;
                }
                case AnyVODEnums::AGA_DUBOIS:
                {
                    matFirst[0][0] = -0.062f;
                    matFirst[1][0] = -0.158f;
                    matFirst[2][0] = -0.039f;
                    matFirst[0][1] = 0.284f;
                    matFirst[1][1] = 0.668f;
                    matFirst[2][1] = 0.143f;
                    matFirst[0][2] = -0.015f;
                    matFirst[1][2] = -0.027f;
                    matFirst[2][2] = 0.021f;

                    matSecond[0][0] = 0.529f;
                    matSecond[1][0] = 0.705f;
                    matSecond[2][0] = 0.024f;
                    matSecond[0][1] = -0.016f;
                    matSecond[1][1] = -0.015f;
                    matSecond[2][1] = -0.065f;
                    matSecond[0][2] = 0.009f;
                    matSecond[1][2] = 0.075f;
                    matSecond[2][2] = 0.937f;

                    break;
                }
                default:
                {
                    break;
                }
            }

            break;
        }
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            switch (this->m_defaultParam.anaglyphAlgorithm)
            {
                case AnyVODEnums::AGA_GRAY:
                {
                    matFirst[0][0] = 0.299f;
                    matFirst[1][0] = 0.587f;
                    matFirst[2][0] = 0.114f;
                    matFirst[0][1] = 0.299f;
                    matFirst[1][1] = 0.587f;
                    matFirst[2][1] = 0.114f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[0][2] = 0.299f;
                    matSecond[1][2] = 0.587f;
                    matSecond[2][2] = 0.114f;

                    break;
                }
                case AnyVODEnums::AGA_COLORED:
                {
                    matFirst[0][0] = 1.0f;
                    matFirst[1][0] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[0][1] = 0.0f;
                    matFirst[1][1] = 1.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[0][2] = 0.0f;
                    matSecond[1][2] = 0.0f;
                    matSecond[2][2] = 1.0f;

                    break;
                }
                case AnyVODEnums::AGA_HALF_COLORED:
                {
                    matFirst[0][0] = 1.0f;
                    matFirst[1][0] = 0.0f;
                    matFirst[2][0] = 0.0f;
                    matFirst[0][1] = 0.0f;
                    matFirst[1][1] = 1.0f;
                    matFirst[2][1] = 0.0f;
                    matFirst[0][2] = 0.0f;
                    matFirst[1][2] = 0.0f;
                    matFirst[2][2] = 0.0f;

                    matSecond[0][0] = 0.0f;
                    matSecond[1][0] = 0.0f;
                    matSecond[2][0] = 0.0f;
                    matSecond[0][1] = 0.0f;
                    matSecond[1][1] = 0.0f;
                    matSecond[2][1] = 0.0f;
                    matSecond[0][2] = 0.299f;
                    matSecond[1][2] = 0.587f;
                    matSecond[2][2] = 0.114f;

                    break;
                }
                case AnyVODEnums::AGA_DUBOIS:
                {
                    matFirst[0][0] = 1.062f;
                    matFirst[1][0] = -0.205f;
                    matFirst[2][0] = 0.299f;
                    matFirst[0][1] = -0.026f;
                    matFirst[1][1] = 0.908f;
                    matFirst[2][1] = 0.068f;
                    matFirst[0][2] = -0.038f;
                    matFirst[1][2] = -0.173f;
                    matFirst[2][2] = 0.022f;

                    matSecond[0][0] = -0.016f;
                    matSecond[1][0] = -0.123f;
                    matSecond[2][0] = -0.017f;
                    matSecond[0][1] = 0.006f;
                    matSecond[1][1] = 0.062f;
                    matSecond[2][1] = -0.017f;
                    matSecond[0][2] = 0.094f;
                    matSecond[1][2] = 0.185f;
                    matSecond[2][2] = 0.911f;

                    break;
                }
                default:
                {
                    break;
                }
            }

            break;
        }
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            matFirst[0][0] = 0.299f;
            matFirst[1][0] = 0.587f;
            matFirst[2][0] = 0.114f;
            matFirst[0][1] = 0.0f;
            matFirst[1][1] = 0.0f;
            matFirst[2][1] = 0.0f;
            matFirst[0][2] = 0.0f;
            matFirst[1][2] = 0.0f;
            matFirst[2][2] = 0.0f;

            matSecond[0][0] = 0.0f;
            matSecond[1][0] = 0.0f;
            matSecond[2][0] = 0.0f;
            matSecond[0][1] = 0.0f;
            matSecond[1][1] = 0.0f;
            matSecond[2][1] = 0.0f;
            matSecond[0][2] = 0.299f;
            matSecond[1][2] = 0.587f;
            matSecond[2][2] = 0.114f;

            break;
        }
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            matFirst[0][0] = 0.299f;
            matFirst[1][0] = 0.587f;
            matFirst[2][0] = 0.114f;
            matFirst[0][1] = 0.0f;
            matFirst[1][1] = 0.0f;
            matFirst[2][1] = 0.0f;
            matFirst[0][2] = 0.0f;
            matFirst[1][2] = 0.0f;
            matFirst[2][2] = 0.0f;

            matSecond[0][0] = 0.0f;
            matSecond[1][0] = 0.0f;
            matSecond[2][0] = 0.0f;
            matSecond[0][1] = 0.299f;
            matSecond[1][1] = 0.587f;
            matSecond[2][1] = 0.114f;
            matSecond[0][2] = 0.0f;
            matSecond[1][2] = 0.0f;
            matSecond[2][2] = 0.0f;

            break;
        }
        default:
        {
            break;
        }
    }
}

bool ShaderCompositer::getLeftOrTop() const
{
    return this->m_defaultParam.leftOrTop;
}

bool ShaderCompositer::startScreen(bool useNormalizedCoord, const QSizeF &surfaceSize, const QSizeF &screenSize, const QSize &texSize, double clock, double lumAvg)
{
    bool success = false;

    if (this->m_shaderScreen.isLinked())
        success = this->m_shaderScreen.bind();

    if (success)
    {
        this->m_shaderScreen.setUniformValue("AnyVOD_Hue", (float)this->m_defaultParam.hue);
        this->m_shaderScreen.setUniformValue("AnyVOD_Saturation", (float)this->m_defaultParam.saturation);
        this->m_shaderScreen.setUniformValue("AnyVOD_Contrast", (float)this->m_defaultParam.contrast);
        this->m_shaderScreen.setUniformValue("AnyVOD_Brightness", (float)this->m_defaultParam.brightness);

        this->m_shaderScreen.setUniformValue("AnyVOD_Width", (float)surfaceSize.width());
        this->m_shaderScreen.setUniformValue("AnyVOD_Height", (float)surfaceSize.height());
        this->m_shaderScreen.setUniformValue("AnyVOD_Clock", (float)clock);
        this->m_shaderScreen.setUniformValue("AnyVOD_LumAvg", QVector3D(lumAvg, lumAvg, lumAvg));
        this->m_shaderScreen.setUniformValue("AnyVOD_TexSize", QVector2D(texSize.width(), texSize.height()));

        this->m_shaderScreen.setUniformValue("AnyVOD_useLeftRightInvert", this->m_defaultParam.useLeftRightInvert);
        this->m_shaderScreen.setUniformValue("AnyVOD_useTopBottomInvert", this->m_defaultParam.useTopBottomInvert);

        QVector2D screenCoord;

        if (useNormalizedCoord)
            screenCoord = QVector2D(screenSize.width(), screenSize.height());
        else
            screenCoord = QVector2D((float)screenSize.width() / texSize.width(), (float)screenSize.height() / texSize.height());

        if (this->m_defaultParam.interlaced)
        {
            this->m_shaderScreen.setUniformValue("AnyVOD_leftOrTop", this->m_defaultParam.leftOrTop);
            this->m_shaderScreen.setUniformValue("AnyVOD_rowOrCol", this->m_defaultParam.rowOrCol);
            this->m_shaderScreen.setUniformValue("AnyVOD_sideBySide", this->m_defaultParam.sideBySide);
            this->m_shaderScreen.setUniformValue("AnyVOD_screenCoord", screenCoord);
        }

        if (this->m_defaultParam.checker)
        {
            this->m_shaderScreen.setUniformValue("AnyVOD_leftOrTop", this->m_defaultParam.leftOrTop);
            this->m_shaderScreen.setUniformValue("AnyVOD_sideBySide", this->m_defaultParam.sideBySide);
            this->m_shaderScreen.setUniformValue("AnyVOD_screenCoord", screenCoord);
        }

        if (this->m_defaultParam.anaglyph)
        {
            this->m_shaderScreen.setUniformValue("AnyVOD_leftOrTop", this->m_defaultParam.leftOrTop);
            this->m_shaderScreen.setUniformValue("AnyVOD_sideBySide", this->m_defaultParam.sideBySide);
            this->m_shaderScreen.setUniformValue("AnyVOD_screenCoord", screenCoord);

            float matFirst[3][3];
            float matSecond[3][3];

            this->getAnaglyphMatrix(matFirst, matSecond);

            this->m_shaderScreen.setUniformValue("AnyVOD_anaglyphFirstMatrix", &matFirst[0]);
            this->m_shaderScreen.setUniformValue("AnyVOD_anaglyphSecondMatrix", &matSecond[0]);
        }
    }

    return success;
}

void ShaderCompositer::setRenderData(ShaderCompositer::ShaderType type, const QMatrix4x4 &proj, const QMatrix4x4 &model,
                                     QVector2D *vertices, QVector2D *texCoords, const QVector4D &color, bool isGPUConvert, AVPixelFormat pixFormat)
{
    switch (type)
    {
        case ST_SCREEN:
            this->setRenderData(this->m_shaderScreen, proj, model, vertices, texCoords, color, isGPUConvert, pixFormat);
            break;
        case ST_SIMPLE:
            this->setRenderData(this->m_shaderSimple, proj, model, vertices, texCoords, color, false, pixFormat);
            break;
        case ST_SUBTITLE_INTERLACE:
            this->setRenderData(this->m_shaderSubtitleInterlace, proj, model, vertices, texCoords, color, false, pixFormat);
            break;
        case ST_SUBTITLE_CHECKER_BOARD:
            this->setRenderData(this->m_shaderSubtitleCheckerBoard, proj, model, vertices, texCoords, color, false, pixFormat);
            break;
        case ST_SUBTITLE_ANAGLYPH:
            this->setRenderData(this->m_shaderSubtitleAnaglyph, proj, model, vertices, texCoords, color, false, pixFormat);
            break;
        case ST_BARREL_DISTORTION:
            this->setRenderData(this->m_shaderBarrelDistortion, proj, model, vertices, texCoords, color, false, pixFormat);
            break;
        default:
            break;
    }
}

void ShaderCompositer::updateRenderData(ShaderCompositer::ShaderType type, const QMatrix4x4 &model, QVector2D *vertices, QVector2D *texCoords)
{
    switch (type)
    {
        case ST_SCREEN:
            this->updateRenderData(this->m_shaderScreen, model, vertices, texCoords);
            break;
        case ST_SIMPLE:
            this->updateRenderData(this->m_shaderSimple, model, vertices, texCoords);
            break;
        case ST_SUBTITLE_INTERLACE:
            this->updateRenderData(this->m_shaderSubtitleInterlace, model, vertices, texCoords);
            break;
        case ST_SUBTITLE_CHECKER_BOARD:
            this->updateRenderData(this->m_shaderSubtitleCheckerBoard, model, vertices, texCoords);
            break;
        case ST_SUBTITLE_ANAGLYPH:
            this->updateRenderData(this->m_shaderSubtitleAnaglyph, model, vertices, texCoords);
            break;
        case ST_BARREL_DISTORTION:
            this->updateRenderData(this->m_shaderBarrelDistortion, model, vertices, texCoords);
            break;
        default:
            break;
    }
}

void ShaderCompositer::setTextureSampler(ShaderCompositer::ShaderType type, int index)
{
    switch (type)
    {
        case ST_SCREEN:
            this->setTextureSampler(this->m_shaderScreen, index);
            break;
        case ST_SIMPLE:
            this->setTextureSampler(this->m_shaderSimple, index);
            break;
        case ST_SUBTITLE_INTERLACE:
            this->setTextureSampler(this->m_shaderSubtitleInterlace, index);
            break;
        case ST_SUBTITLE_CHECKER_BOARD:
            this->setTextureSampler(this->m_shaderSubtitleCheckerBoard, index);
            break;
        case ST_SUBTITLE_ANAGLYPH:
            this->setTextureSampler(this->m_shaderSubtitleAnaglyph, index);
            break;
        case ST_BARREL_DISTORTION:
            this->setTextureSampler(this->m_shaderBarrelDistortion, index);
            break;
        default:
            break;
    }
}

void ShaderCompositer::endScreen()
{
    this->unsetRenderData(this->m_shaderScreen);
    this->m_shaderScreen.release();
}

bool ShaderCompositer::startSimple()
{
    bool success = false;

    if (this->m_shaderSimple.isLinked())
        success = this->m_shaderSimple.bind();

    return success;
}

void ShaderCompositer::endSimple()
{
    this->unsetRenderData(this->m_shaderSimple);
    this->m_shaderSimple.release();
}

bool ShaderCompositer::startSubtitleInterlace(bool firstFrame)
{
    bool success = false;

    if (this->m_shaderSubtitleInterlace.isLinked())
        success = this->m_shaderSubtitleInterlace.bind();

    if (success)
    {
        this->m_shaderSubtitleInterlace.setUniformValue("AnyVOD_firstFrame", firstFrame);
        this->m_shaderSubtitleInterlace.setUniformValue("AnyVOD_rowOrCol", this->m_defaultParam.rowOrCol);
    }

    return success;
}

void ShaderCompositer::endSubtitleInterlace()
{
    this->unsetRenderData(this->m_shaderSubtitleInterlace);
    this->m_shaderSubtitleInterlace.release();
}

bool ShaderCompositer::startSubtitleCheckerBoard(bool firstFrame)
{
    bool success = false;

    if (this->m_shaderSubtitleCheckerBoard.isLinked())
        success = this->m_shaderSubtitleCheckerBoard.bind();

    if (success)
        this->m_shaderSubtitleCheckerBoard.setUniformValue("AnyVOD_firstFrame", firstFrame);

    return success;
}

void ShaderCompositer::endSubtitleCheckerBoard()
{
    this->unsetRenderData(this->m_shaderSubtitleCheckerBoard);
    this->m_shaderSubtitleCheckerBoard.release();
}

bool ShaderCompositer::startSubtitleAnaglyph(bool firstFrame)
{
    bool success = false;

    if (this->m_shaderSubtitleAnaglyph.isLinked())
        success = this->m_shaderSubtitleAnaglyph.bind();

    if (success)
    {
        float matFirst[3][3];
        float matSecond[3][3];

        this->getAnaglyphMatrix(matFirst, matSecond);

        this->m_shaderSubtitleAnaglyph.setUniformValue("AnyVOD_anaglyphMatrix", firstFrame ? &matSecond[0] : &matFirst[0]);
    }

    return success;
}

void ShaderCompositer::endSubtitleAnaglyph()
{
    this->unsetRenderData(this->m_shaderSubtitleAnaglyph);
    this->m_shaderSubtitleAnaglyph.release();
}

bool ShaderCompositer::startBarrelDistortion(const QVector2D &texRange, const QVector2D &lensCenterOffset,
                                             const QVector2D &coefficients, float fillScale)
{
    bool success = false;

    if (this->m_shaderBarrelDistortion.isLinked())
        success = this->m_shaderBarrelDistortion.bind();

    if (success)
    {
        this->m_shaderBarrelDistortion.setUniformValue("AnyVOD_texRange", texRange);
        this->m_shaderBarrelDistortion.setUniformValue("AnyVOD_lensCenterOffset", lensCenterOffset);
        this->m_shaderBarrelDistortion.setUniformValue("AnyVOD_coefficients", coefficients);
        this->m_shaderBarrelDistortion.setUniformValue("AnyVOD_fillScale", fillScale);
    }

    return success;
}

void ShaderCompositer::endBarrelDistortion()
{
    this->unsetRenderData(this->m_shaderBarrelDistortion);
    this->m_shaderBarrelDistortion.release();
}

bool ShaderCompositer::addShader(const QString &filePath)
{
    if (this->findShader(filePath) >= 0)
        return false;

    QFile file(filePath);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QTextStream stream(&file);
    ShaderItem item;

    item.path = filePath;
    item.content = stream.readAll();

    this->m_shaderSources.append(item);

    return true;
}

bool ShaderCompositer::deleteShader(const QString &filePath)
{
    int index = this->findShader(filePath);

    if (index < 0)
        return false;

    this->m_shaderSources.removeAt(index);

    return true;
}

void ShaderCompositer::clearShaders()
{
    this->m_shaderSources.clear();
}

void ShaderCompositer::getShaderList(QStringList *ret)
{
    for (int i = 0; i < this->m_shaderSources.count(); i++)
        ret->append(this->m_shaderSources[i].path);
}

void ShaderCompositer::getFragmentSource(QString *ret) const
{
    *ret = this->m_fragmentSource;
}

void ShaderCompositer::getVertexSource(QString *ret) const
{
    *ret = this->m_vertexSource;
}

bool ShaderCompositer::build()
{
    QString entries;
    QString source = this->m_shaderFrame;
    QString funcs;
    bool success;

    this->makeEntry(&entries);
    this->getFuncs(&funcs);

    funcs.append(this->m_effect);
    funcs.append(this->m_default);

    if (this->m_defaultParam.interlaced)
        funcs.append(this->m_interlace3D);

    if (this->m_defaultParam.checker)
        funcs.append(this->m_checkerBoard);

    if (this->m_defaultParam.anaglyph)
        funcs.append(this->m_anaglyph);

    source.replace(FUNCS_SIG, funcs);
    source.replace(ENTRY_SIG, entries);

    this->m_fragmentSource = source;
    this->m_vertexSource = this->m_vertexFrame;

    success = this->buildShader(this->m_shaderScreen, source);
    success &= this->buildShader(this->m_shaderSimple, this->m_simple);

    this->buildShader(this->m_shaderSubtitleInterlace, this->m_subtitleInterlace);
    this->buildShader(this->m_shaderSubtitleCheckerBoard, this->m_subtitleCheckerBoard);
    this->buildShader(this->m_shaderSubtitleAnaglyph, this->m_subtitleAnaglyph);
    this->buildShader(this->m_shaderBarrelDistortion, this->m_barrelDistortion);

    return success;
}

void ShaderCompositer::getLog(QString *ret) const
{
    *ret = this->m_shaderScreen.log();
}

void ShaderCompositer::addEntry(const QString &entry, QString *ret) const
{
    ret->append(ENTRY_HEADER);
    ret->append(entry);
    ret->append(ENTRY_FOOTER);
}

void ShaderCompositer::getFuncs(QString *ret) const
{
    for (int i = 0; i < this->m_shaderSources.count(); i++)
        ret->append(this->m_shaderSources[i].content + "\n");
}

int ShaderCompositer::findShader(const QString &path) const
{
    for (int i = 0; i < this->m_shaderSources.count(); i++)
    {
        if (this->m_shaderSources[i].path == path)
            return i;
    }

    return -1;
}

void ShaderCompositer::setRenderData(QOpenGLShaderProgram &shader, const QMatrix4x4 &proj, const QMatrix4x4 &model,
                                     QVector2D *vertices, QVector2D *texCoords, const QVector4D &color, bool isGPUConvert,
                                     AVPixelFormat pixFormat)
{
    shader.setUniformValue("AnyVOD_useGPUConvert", isGPUConvert);
    shader.setUniformValue("AnyVOD_pixFormat", pixFormat);

#if defined Q_OS_MOBILE
    shader.setUniformValue("AnyVOD_projection", proj);
    shader.setUniformValue("AnyVOD_modelView", model);
    shader.setUniformValue("AnyVOD_inColor", color);

    if (vertices)
    {
        shader.enableAttributeArray(VSL_VERTEX);
        shader.setAttributeArray(VSL_VERTEX, vertices);
    }

    if (texCoords)
    {
        shader.enableAttributeArray(VSL_TEXTURE_COORD);
        shader.setAttributeArray(VSL_TEXTURE_COORD, texCoords);
    }
#else
    (void) proj;
    (void) model;
    (void) vertices;
    (void) texCoords;
    (void) color;
#endif
}

void ShaderCompositer::updateRenderData(QOpenGLShaderProgram &shader, const QMatrix4x4 &model, QVector2D *vertices, QVector2D *texCoords)
{
#if defined Q_OS_MOBILE
    shader.setUniformValue("AnyVOD_modelView", model);

    shader.enableAttributeArray(VSL_VERTEX);
    shader.setAttributeArray(VSL_VERTEX, vertices);

    shader.enableAttributeArray(VSL_TEXTURE_COORD);
    shader.setAttributeArray(VSL_TEXTURE_COORD, texCoords);
#else
    (void) shader;
    (void) model;
    (void) vertices;
    (void) texCoords;
#endif
}

void ShaderCompositer::unsetRenderData(QOpenGLShaderProgram &shader)
{
#if defined Q_OS_MOBILE
    shader.disableAttributeArray(VSL_VERTEX);
    shader.disableAttributeArray(VSL_TEXTURE_COORD);
#else
    (void) shader;
#endif
}

void ShaderCompositer::setTextureSampler(QOpenGLShaderProgram &shader, int index)
{
    QString name = "AnyVOD_tex" + QString::number(index);
    int loc = shader.uniformLocation(name);

    shader.setUniformValue(loc, index);
}

bool ShaderCompositer::buildShader(QOpenGLShaderProgram &shader, const QString &code)
{
    shader.removeAllShaders();
#if defined Q_OS_MOBILE
    if (!shader.addShaderFromSourceCode(QOpenGLShader::Vertex, this->m_vertexSource))
        return false;

    shader.bindAttributeLocation("AnyVOD_vertex", VSL_VERTEX);
    shader.bindAttributeLocation("AnyVOD_inTexCoords", VSL_TEXTURE_COORD);
#endif

    if (!shader.addShaderFromSourceCode(QOpenGLShader::Fragment, code))
        return false;

    return shader.link();
}

void ShaderCompositer::makeEntry(QString *ret) const
{
    QStringList shaders;

    if (this->m_defaultParam.interlaced)
        this->addEntry("AnyVOD_interlace3D", ret);

    if (this->m_defaultParam.checker)
        this->addEntry("AnyVOD_checkerBoard", ret);

    if (this->m_defaultParam.useLeftRightInvert || this->m_defaultParam.useTopBottomInvert)
        this->addEntry("AnyVOD_invert", ret);

    for (int i = 0; i < this->m_shaderSources.count(); i++)
        shaders.append(this->m_shaderSources[i].content);

    for (int i = 0; i < shaders.count(); i++)
    {
        QString &shader = shaders[i];
        QTextStream stream(&shader);

        while (!stream.atEnd())
        {
            const QString line = stream.readLine().trimmed();

            if (!line.startsWith(Utils::COMMENT_PREFIX))
                continue;

            const QString entrySig = "entry";
            QString content = line;

            content = content.remove(Utils::COMMENT_PREFIX).trimmed();

            if (!content.startsWith(entrySig, Qt::CaseInsensitive))
                continue;

            int index = line.indexOf(entrySig);

            if (index == -1)
                continue;

            this->addEntry(line.mid(index + entrySig.count()).trimmed(), ret);
        }
    }

    if (this->m_defaultParam.useSharply)
        this->addEntry("AnyVOD_sharply", ret);

    if (this->m_defaultParam.useSharpen)
        this->addEntry("AnyVOD_sharpen", ret);

    if (this->m_defaultParam.useSoften)
        this->addEntry("AnyVOD_soften", ret);

#if !defined Q_OS_RASPBERRY_PI
    this->addEntry("AnyVOD_hue", ret);
    this->addEntry("AnyVOD_saturation_contrast", ret);
    this->addEntry("AnyVOD_brightness", ret);
#endif

    if (this->m_defaultParam.anaglyph)
        this->addEntry("AnyVOD_anaglyph", ret);
 }

void ShaderCompositer::setHue(double hue)
{
    this->m_defaultParam.hue = hue;
}

double ShaderCompositer::getHue() const
{
    return this->m_defaultParam.hue;
}

void ShaderCompositer::setSaturation(double saturation)
{
    this->m_defaultParam.saturation = saturation;
}

double ShaderCompositer::getSaturation() const
{
    return this->m_defaultParam.saturation;
}

void ShaderCompositer::setContrast(double contrast)
{
    this->m_defaultParam.contrast = contrast;
}

double ShaderCompositer::getContrast() const
{
    return this->m_defaultParam.contrast;
}

void ShaderCompositer::setBrightness(double brightness)
{
    this->m_defaultParam.brightness = brightness;
}

double ShaderCompositer::getBrightness() const
{
    return this->m_defaultParam.brightness;
}

void ShaderCompositer::useSharply(bool use)
{
    this->m_defaultParam.useSharply = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingSharply() const
{
    return this->m_defaultParam.useSharply;
}

void ShaderCompositer::useSharpen(bool use)
{
    this->m_defaultParam.useSharpen = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingSharpen() const
{
    return this->m_defaultParam.useSharpen;
}

void ShaderCompositer::useSoften(bool use)
{
    this->m_defaultParam.useSoften = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingSoften() const
{
    return this->m_defaultParam.useSoften;
}

void ShaderCompositer::useLeftRightInvert(bool use)
{
    this->m_defaultParam.useLeftRightInvert = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingLeftRightInvert() const
{
    return this->m_defaultParam.useLeftRightInvert;
}

void ShaderCompositer::useTopBottomInvert(bool use)
{
    this->m_defaultParam.useTopBottomInvert = use;
#if !defined Q_OS_MOBILE
    this->build();
#endif
}

bool ShaderCompositer::isUsingTopBottomInvert() const
{
    return this->m_defaultParam.useTopBottomInvert;
}

void ShaderCompositer::set3DMethod(AnyVODEnums::Video3DMethod method3D)
{
    this->m_defaultParam.method3D = method3D;
    this->m_defaultParam.interlaced = false;
    this->m_defaultParam.anaglyph = false;
    this->m_defaultParam.checker = false;

    switch (method3D)
    {
        case AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            this->m_defaultParam.interlaced = true;
            this->m_defaultParam.rowOrCol = true;

            break;
        }
        case AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            this->m_defaultParam.interlaced = true;
            this->m_defaultParam.rowOrCol = false;

            break;
        }
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            this->m_defaultParam.anaglyph = true;
            break;
        }
        case AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR:
        {
            this->m_defaultParam.checker = true;
            break;
        }
        default:
        {
            break;
        }
    }

#if !defined Q_OS_MOBILE
    this->build();
#endif
}

AnyVODEnums::Video3DMethod ShaderCompositer::get3DMethod() const
{
    return this->m_defaultParam.method3D;
}

void ShaderCompositer::setAnaglyphAlgorithm(AnyVODEnums::AnaglyphAlgorithm algoritm)
{
    this->m_defaultParam.anaglyphAlgorithm = algoritm;
}

AnyVODEnums::AnaglyphAlgorithm ShaderCompositer::getAnaglyphAlgorithm() const
{
    return this->m_defaultParam.anaglyphAlgorithm;
}

bool ShaderCompositer::canUseAnaglyphAlgorithm() const
{
    switch (this->m_defaultParam.method3D)
    {
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR:
        case AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR:
            return false;
        default:
            break;

    }

    return true;
}

void ShaderCompositer::setup3D(bool sideBySide, bool leftOrTop)
{
    this->m_defaultParam.sideBySide = sideBySide;
    this->m_defaultParam.leftOrTop = leftOrTop;
}

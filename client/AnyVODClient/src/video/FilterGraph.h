﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include <QMutex>
#include <QVector>

extern "C"
{
    #include <libavformat/avformat.h>
    #include <libavfilter/avfiltergraph.h>
}

class FilterGraph
{
public:
    FilterGraph();
    ~FilterGraph();

    bool setCodec(AVCodecContext *codec, AVPixelFormat pixFormat, AVPixelFormat screenFormat, const AVRational timeBase);
    bool getFrame(int width, int height, AVPixelFormat informat, const AVFrame &in,
                  AVFrame *out, AVPixelFormat *retFormat);
    bool hasFilters();
    int getDelayCount() const;

    void setEnableHistEQ(bool enable);
    bool getEnableHistEQ() const;

    void setEnableHighQuality3DDenoise(bool enable);
    bool getEnableHighQuality3DDenoise() const;

    void setEnableDeBand(bool enable);
    bool getEnableDeBand() const;

    void setEnableATADenoise(bool enable);
    bool getEnableATADenoise() const;

    void setEnableOWDenoise(bool enable);
    bool getEnableOWDenoise() const;

private:
    struct FILTER
    {
        FILTER() :
            filter(NULL),
            enable(false)
        {

        }

        AVFilterContext *filter;
        bool enable;
    };

private:
    void unInitFilter();
    bool link();

private:
    AVFilterContext *m_in;
    FILTER m_histeq;
    FILTER m_hqdn3d;
    FILTER m_atadenoise;
    FILTER m_owdenoise;
    FILTER m_deband;
    AVFilterContext *m_out;
    AVFilterGraph *m_graph;
    int m_delayCount;
    QMutex m_lock;
    QVector<AVFilterContext*> m_filters;
};

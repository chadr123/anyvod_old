﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "DTVReaderInterface.h"

#include <QString>
#include <QMap>
#include <QVector>
#include <QLocale>

#include <stdint.h>

class DTVChannelMap
{
public:
    struct Channel
    {
        Channel() :
            channel(0),
            freq(0),
            hpFEC(0),
            srate(0),
            pilot(0),
            rolloff(0),
            sid(0),
            pol(0),
            lowf(0),
            highf(0),
            switchf(0),
            lpFEC(0),
            bandwidth(0),
            transmission(0),
            guard(0),
            hierarchy(0),
            plp(0),
            tsID(0),
            highv(0),
            satNo(0),
            uncommitted(0)
        {

        }

        /* ATSC, CQAM */
        Channel(int ichannel, uint64_t ifreq, const QString &imod) :
            channel(ichannel),
            freq(ifreq),
            mod(imod),
            hpFEC(0),
            srate(0),
            pilot(0),
            rolloff(0),
            sid(0),
            pol(0),
            lowf(0),
            highf(0),
            switchf(0),
            lpFEC(0),
            bandwidth(0),
            transmission(0),
            guard(0),
            hierarchy(0),
            plp(0),
            tsID(0),
            highv(0),
            satNo(0),
            uncommitted(0)
        {

        }

        /* DVBC, ISDBC */
        Channel(int ichannel, uint64_t ifreq, const QString &imod, uint32_t ihpFEC, uint32_t israte) :
            channel(ichannel),
            freq(ifreq),
            mod(imod),
            hpFEC(ihpFEC),
            srate(israte),
            pilot(0),
            rolloff(0),
            sid(0),
            pol(0),
            lowf(0),
            highf(0),
            switchf(0),
            lpFEC(0),
            bandwidth(0),
            transmission(0),
            guard(0),
            hierarchy(0),
            plp(0),
            tsID(0),
            highv(0),
            satNo(0),
            uncommitted(0)
        {

        }

        /* DVBS */
        Channel(int ichannel, uint64_t ifreq, const QString &imod, uint32_t ihpFEC, uint32_t israte, char ipol,
                int32_t ihighv, uint32_t isatNo, uint32_t iuncommitted, const QString &idummy) :
            channel(ichannel),
            freq(ifreq),
            mod(imod),
            hpFEC(ihpFEC),
            srate(israte),
            pilot(0),
            rolloff(0),
            sid(0),
            pol(ipol),
            lowf(0),
            highf(0),
            switchf(0),
            lpFEC(0),
            bandwidth(0),
            transmission(0),
            guard(0),
            hierarchy(0),
            plp(0),
            tsID(0),
            highv(ihighv),
            satNo(isatNo),
            uncommitted(iuncommitted),
            dummy(idummy)
        {

        }

        /* DVBS2 */
        Channel(int ichannel, uint64_t ifreq, const QString &imod, uint32_t ihpFEC, uint32_t israte, char ipol,
                int ipilot, int irolloff, uint8_t isid, int32_t ihighv, uint32_t isatNo, uint32_t iuncommitted) :
            channel(ichannel),
            freq(ifreq),
            mod(imod),
            hpFEC(ihpFEC),
            srate(israte),
            pilot(ipilot),
            rolloff(irolloff),
            sid(isid),
            pol(ipol),
            lowf(0),
            highf(0),
            switchf(0),
            lpFEC(0),
            bandwidth(0),
            transmission(0),
            guard(0),
            hierarchy(0),
            plp(0),
            tsID(0),
            highv(ihighv),
            satNo(isatNo),
            uncommitted(iuncommitted)
        {

        }

        /* DVBT */
        Channel(int ichannel, uint64_t ifreq, const QString &imod, uint32_t ihpFEC, uint32_t ilpFEC,
                uint32_t ibandwidth, int itransmission, uint32_t iguard, int ihierarchy) :
            channel(ichannel),
            freq(ifreq),
            mod(imod),
            hpFEC(ihpFEC),
            srate(0),
            pilot(0),
            rolloff(0),
            sid(0),
            pol(0),
            lowf(0),
            highf(0),
            switchf(0),
            lpFEC(ilpFEC),
            bandwidth(ibandwidth),
            transmission(itransmission),
            guard(iguard),
            hierarchy(ihierarchy),
            plp(0),
            tsID(0),
            highv(0),
            satNo(0),
            uncommitted(0)
        {

        }

        /* DVBT2 */
        Channel(int ichannel, uint64_t ifreq, const QString &imod, uint32_t ihpFEC, uint32_t ibandwidth,
                int itransmission, uint32_t iguard, uint8_t iplp) :
            channel(ichannel),
            freq(ifreq),
            mod(imod),
            hpFEC(ihpFEC),
            srate(0),
            pilot(0),
            rolloff(0),
            sid(0),
            pol(0),
            lowf(0),
            highf(0),
            switchf(0),
            lpFEC(0),
            bandwidth(ibandwidth),
            transmission(itransmission),
            guard(iguard),
            hierarchy(0),
            plp(iplp),
            tsID(0),
            highv(0),
            satNo(0),
            uncommitted(0)
        {

        }

        /* ISDBT */
        Channel(int ichannel, uint64_t ifreq, uint32_t ibandwidth, int itransmission, uint32_t iguard,
                DTVReaderInterface::ISDBTLayer ilayers[3]) :
            channel(ichannel),
            freq(ifreq),
            hpFEC(0),
            srate(0),
            pilot(0),
            rolloff(0),
            sid(0),
            pol(0),
            lowf(0),
            highf(0),
            switchf(0),
            lpFEC(0),
            bandwidth(ibandwidth),
            transmission(itransmission),
            guard(iguard),
            hierarchy(0),
            plp(0),
            tsID(0),
            highv(0),
            satNo(0),
            uncommitted(0)
        {
            for (size_t i = 0; i < sizeof(layers)/sizeof(layers[0]); i++)
                layers[i] = ilayers[i];
        }

        /* ISDBS */
        Channel(int ichannel, uint64_t ifreq, uint16_t itsID, char ipol, int32_t ihighv, uint32_t isatNo, uint32_t iuncommitted) :
            channel(ichannel),
            freq(ifreq),
            hpFEC(0),
            srate(0),
            pilot(0),
            rolloff(0),
            sid(0),
            pol(ipol),
            lowf(0),
            highf(0),
            switchf(0),
            lpFEC(0),
            bandwidth(0),
            transmission(0),
            guard(0),
            hierarchy(0),
            plp(0),
            tsID(itsID),
            highv(ihighv),
            satNo(isatNo),
            uncommitted(iuncommitted)
        {

        }

        int channel;
        uint64_t freq;
        QString mod;
        uint32_t hpFEC;
        uint32_t srate;
        int pilot;
        int rolloff;
        uint8_t sid;
        char pol;
        uint32_t lowf;
        uint32_t highf;
        uint32_t switchf;
        uint32_t lpFEC;
        uint32_t bandwidth;
        int transmission;
        uint32_t guard;
        int hierarchy;
        uint8_t plp;
        DTVReaderInterface::ISDBTLayer layers[3];
        uint16_t tsID;
        int32_t highv;
        uint32_t satNo;
        uint32_t uncommitted;
        QString dummy;
    };

public:
    DTVChannelMap();
    ~DTVChannelMap();

    bool setChannelCountry(QLocale::Country country, DTVReaderInterface::SystemType type);
    void getChannelCountry(QLocale::Country *country, DTVReaderInterface::SystemType *type);
    void getChannels(QVector<Channel> *ret);
    bool getChannel(int index, Channel *ret);

private:
    QString getCountryCode(QLocale::Country country, DTVReaderInterface::SystemType type);
    void addSouthKorea();

private:
    QString m_country;
    QMap<QString, QVector<Channel> > m_channel;
};

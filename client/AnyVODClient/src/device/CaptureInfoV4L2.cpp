﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "CaptureInfoV4L2.h"

#include <QDir>
#include <QDebug>

#include <sys/ioctl.h>
#include <linux/videodev2.h>

CaptureInfoV4L2::CaptureInfoV4L2()
{

}

CaptureInfoV4L2::~CaptureInfoV4L2()
{

}

void CaptureInfoV4L2::getVideoDevices(QVector<CaptureInfo::Info> *ret) const
{
    const QString root = "/dev/v4l/by-id";
    QDir dir(root);

    foreach (const QFileInfo &device, dir.entryInfoList())
    {
        if (!device.isDir())
        {
            CaptureInfo::Info info;
            QFile file(device.absoluteFilePath());

            if (!file.open(QFile::ReadOnly))
                continue;

            int fd = file.handle();
            v4l2_capability cap;

            if (ioctl(fd, VIDIOC_QUERYCAP, &cap) < 0)
                continue;

            info.name = device.absoluteFilePath();
            info.desc = QString::fromUtf8((const char*)cap.card);

            ret->append(info);
        }
    }
}

void CaptureInfoV4L2::getAudioDevices(QVector<CaptureInfo::Info> *ret) const
{
    (void)ret;
}

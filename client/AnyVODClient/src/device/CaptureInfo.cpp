﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include <QGlobalStatic>

#include "CaptureInfo.h"
#include "CaptureInfoInterface.h"

#ifdef Q_OS_WIN
#include "CaptureInfoDShow.h"
#elif defined Q_OS_MAC
#include "CaptureInfoAVFoundation.h"
#elif defined Q_OS_LINUX
#include "CaptureInfoV4L2.h"
#endif

#ifdef Q_OS_WIN
const QString CaptureInfo::TYPE = "dshow";
#elif defined Q_OS_MAC
const QString CaptureInfo::TYPE = "avfoundation";
#elif defined Q_OS_LINUX
const QString CaptureInfo::TYPE = "v4l2";
#endif

#include <QDebug>

CaptureInfo::CaptureInfo() :
    m_info(NULL)
{
#ifdef Q_OS_WIN
    this->m_info = new CaptureInfoDShow;
#elif defined Q_OS_MAC
    this->m_info = new CaptureInfoAVFoundation;
#elif defined Q_OS_LINUX
    this->m_info = new CaptureInfoV4L2;
#endif
}

CaptureInfo::~CaptureInfo()
{
    if (this->m_info)
    {
        delete this->m_info;
        this->m_info = NULL;
    }
}

void CaptureInfo::getVideoDevices(QVector<CaptureInfo::Info> *ret) const
{
    if (this->m_info)
        this->m_info->getVideoDevices(ret);
}

void CaptureInfo::getAudioDevices(QVector<CaptureInfo::Info> *ret) const
{
    if (this->m_info)
        this->m_info->getAudioDevices(ret);
}

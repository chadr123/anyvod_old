﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "DTVReaderInterface.h"

#include <linux/dvb/frontend.h>

#include <dvbpsi/dvbpsi.h>
#include <dvbpsi/demux.h>
#include <dvbpsi/descriptor.h>
#include <dvbpsi/atsc_vct.h>
#include <dvbpsi/atsc_mgt.h>
#include <dvbpsi/atsc_eit.h>
#include <dvbpsi/atsc_stt.h>
#include <dvbpsi/sdt.h>
#include <dvbpsi/eit.h>
#include <dvbpsi/tot.h>

#include <QVector>

class DTVLinuxDVB : public DTVReaderInterface
{
public:
    DTVLinuxDVB();
    virtual ~DTVLinuxDVB();

    virtual bool open(const QString &path);
    virtual void close();
    virtual int read(uint8_t *buf, int size);

    virtual unsigned int enumSystems();
    virtual float getSignalStrength();
    virtual float getSignalNoiseRatio();
    virtual QString getChannelName();
    virtual void getEPG(QVector<DTVReaderInterface::EPG> *ret);
    virtual QDateTime getCurrentDateTime();
    virtual bool tune();

    virtual bool getAdapterList(QVector<Info> *ret);

    virtual bool setInversion(Inversion inversion);

    /* DVB-C */
    virtual bool setDVBC(uint32_t freq, const QString &mod, uint32_t srate, uint32_t fec);

    /* DVB-S */
    virtual bool setDVBS(uint64_t freq, uint32_t srate, uint32_t fec);
    virtual bool setDVBS2(uint64_t freq, const QString &mod, uint32_t srate, uint32_t fec, int pilot, int rolloff, uint8_t sid);
    virtual bool setSEC(uint64_t freq, uint32_t srate, uint32_t fec, char pol, uint32_t lowf, uint32_t highf, uint32_t switchf,
                        int32_t highv, uint32_t satNo, uint32_t uncommitted);

    /* DVB-T */
    virtual bool setDVBT(uint32_t freq, const QString &mod, uint32_t fecHP, uint32_t fecLP,
                         uint32_t bandwidth, int transmission, uint32_t guard, int hierarchy);
    virtual bool setDVBT2(uint32_t freq, const QString &mod, uint32_t fec, uint32_t bandwidth,
                          int transmission, uint32_t guard, uint8_t plp);

    /* ATSC */
    virtual bool setATSC(uint32_t freq, const QString &mod);
    virtual bool setCQAM(uint32_t freq, const QString &mod);

    /* ISDB-C */
    virtual bool setISDBC(uint32_t freq, const QString &mod, uint32_t srate, uint32_t fec);

    /* ISDB-S */
    virtual bool setISDBS(uint64_t freq, uint16_t tsID);

    /* ISDB-T */
    virtual bool setISDBT(uint32_t freq, uint32_t bandwidth, int transmission, uint32_t guard, const ISDBTLayer layers[3]);

private:
    struct PSISignal
    {
        PSISignal() :
            parser(NULL),
            got(false),
            tid(0),
            tsID(0)
        {

        }

        dvbpsi_t *parser;
        bool got;
        uint8_t tid;
        uint16_t tsID;
        QString name;
        QVector<int> pids;
        QVector<DTVReaderInterface::EPG> epgs;
        QDateTime curDateTime;
    };

private:
    int openNode(long adapter, const QString &node, int flags) const;
    bool openFrontend();
    void destroy();

    bool setVProps(size_t n, va_list ap);
    bool setProps(size_t n, ...);
    bool setProp(uint32_t prop, uint32_t val);

    void dvbMJDToUTC(uint32_t date, int *year, int *month, int *day) const;
    uint32_t dvbDecodeTime(uint32_t time) const;
    uint8_t bcdToInt(uint8_t bcd) const;

    void getATSCEPG(QVector<DTVReaderInterface::EPG> *ret);
    void getDVBEPG(QVector<DTVReaderInterface::EPG> *ret);
    void getDVBEPGSub(int tid, QVector<DTVReaderInterface::EPG> *ret);

    QDateTime getATSCCurrentDateTime();
    QDateTime getDVBCurrentDateTime();

    QString getATSCChannelName();
    QString getATSCChannelNameSub(int tid);
    QString getDVBChannelName();
    QString getDVBChannelNameSub(int tid);
    void pushPSIData(PSISignal &psi, int tid, int pid, int maxTime) const;
    QString decodeDVBString(uint8_t *str, int len) const;

    fe_modulation_t getModulation(const QString &mod, fe_modulation_t def) const;
    fe_code_rate_t getFEC(uint32_t fec, fe_code_rate_t def) const;
    uint32_t getBandwidth(uint32_t bandwidth) const;
    fe_transmit_mode_t getTransmission(int transmission, fe_transmit_mode_t def) const;
    fe_guard_interval_t getGuard(uint32_t guard, fe_guard_interval_t def) const;
    fe_hierarchy_t getHierarchy(int hierarchy, fe_hierarchy_t def) const;
    fe_sec_voltage_t getPolarization(char pol, fe_sec_voltage_t def) const;

private:
    static void messageCallback(dvbpsi_t *parser, const dvbpsi_msg_level_t level, const char *msg);
    static void atscEITCallback(void *opaque, dvbpsi_atsc_eit_t *eit);
    static void atscMGTCallback(void *opaque, dvbpsi_atsc_mgt_t *mgt);
    static void atscVCTCallback(void *opaque, dvbpsi_atsc_vct_t *vct);
    static void atscSTTCallback(void *opaque, dvbpsi_atsc_stt_t *vct);
    static void atscEITDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque);
    static void atscMGTDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque);
    static void atscVCTDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque);
    static void atscSTTDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque);
    static void dvbSDTCallback(void *opaque, dvbpsi_sdt_t *sdt);
    static void dvbEITCallback(void *opaque, dvbpsi_eit_t *eit);
    static void dvbTDTCallback(void *opaque, dvbpsi_tot_t *tot);
    static void dvbSDTDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque);
    static void dvbEITDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque);
    static void dvbTDTDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque);

private:
    static const QString ROOT;

private:
    int m_frontend;
    int m_demux;
    PSISignal m_psi;
};

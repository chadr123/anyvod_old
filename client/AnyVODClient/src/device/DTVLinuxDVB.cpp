﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "DTVLinuxDVB.h"

#include <QDir>
#include <QVector>
#include <QThread>
#include <QTime>
#include <QTextCodec>
#include <QDebug>

#include <linux/dvb/version.h>
#include <linux/dvb/dmx.h>

#include <dvbpsi/dr_48.h>
#include <dvbpsi/dr_4d.h>

#include <fcntl.h>
#include <poll.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <netinet/in.h>

const QString DTVLinuxDVB::ROOT = "/dev/dvb/adapter";

DTVLinuxDVB::DTVLinuxDVB() :
    m_frontend(-1),
    m_demux(-1)
{

}

DTVLinuxDVB::~DTVLinuxDVB()
{

}

bool DTVLinuxDVB::open(const QString &path)
{
    (void)path;

    this->m_demux = this->openNode(this->m_adapter, "demux", O_RDONLY);

    if (this->m_demux < 0)
    {
        this->destroy();
        return false;
    }

    ioctl(this->m_demux, DMX_SET_BUFFER_SIZE, 1 << 20);

    dmx_pes_filter_params param;

    param.pid = DTV_BUDGET_PID;
    param.input = DMX_IN_FRONTEND;
    param.output = DMX_OUT_TSDEMUX_TAP;
    param.pes_type = DMX_PES_OTHER;
    param.flags = DMX_IMMEDIATE_START;

    if (ioctl(this->m_demux, DMX_SET_PES_FILTER, &param) < 0)
    {
        this->destroy();
        return false;
    }

    return true;
}

void DTVLinuxDVB::close()
{
    this->destroy();
}

int DTVLinuxDVB::read(uint8_t *buf, int size)
{
    pollfd fds[2];

    fds[0].fd = this->m_demux;
    fds[0].events = POLLIN;

    fds[1].fd = this->m_frontend;
    fds[1].events = POLLIN;

    if (poll(fds, sizeof(fds) / sizeof(fds[0]), 500) <= 0)
        return 0;

    if (fds[1].revents)
    {
        dvb_frontend_event ev;

        if (ioctl(fds[1].fd, FE_GET_EVENT, &ev) < 0)
            return 0;
    }

    if (fds[0].revents)
    {
        ssize_t ret;

        ret = ::read(fds[0].fd, buf, size);

        if (ret < 0)
            return 0;

        return ret;
    }

    return 0;
}

unsigned int DTVLinuxDVB::enumSystems()
{
    class localPtr
    {
    public:
        localPtr() :
            m_fd(-1)
        {

        }

        ~localPtr()
        {
            if (this->m_fd != -1)
                ::close(this->m_fd);
        }

        int m_fd;
    }l;

    dtv_properties props;
    dtv_property prop;

    prop.cmd = DTV_ENUM_DELSYS;

    props.num = 1;
    props.props = &prop;

    const unsigned int sysTab[SYS_DVBC_ANNEX_C + 1] =
    {
        ST_NONE, ST_DVB_C, ST_CQAM, ST_DVB_T, ST_NONE,
        ST_DVB_S, ST_DVB_S2, ST_NONE, ST_ISDB_T,
        ST_ISDB_S, ST_ISDB_C, ST_ATSC_T | ST_ATSC_C,
        ST_NONE, ST_NONE, ST_NONE, ST_NONE, ST_DVB_T2,
        ST_NONE, ST_ISDB_C
    };

    l.m_fd = this->openNode(this->m_adapter, "frontend", O_RDWR);

    if (l.m_fd < 0)
        return 0;

    if (ioctl(this->m_frontend, FE_GET_PROPERTY, &props) < 0)
        return 0;

    unsigned int systems = 0;

    for (__u32 i = 0; i < prop.u.buffer.len; i++)
    {
        unsigned int sys = prop.u.buffer.data[i];

        if (sys >= (sizeof(sysTab) / sizeof (sysTab[0])) || !sysTab[sys])
            continue;

        systems |= sysTab[sys];
    }

    return systems;
}

float DTVLinuxDVB::getSignalStrength()
{
    uint16_t strength = 0;

    if (this->m_frontend >= 0 && ioctl(this->m_frontend, FE_READ_SIGNAL_STRENGTH, &strength) == 0)
        return strength / 65535.0f * 100.0f;

    return 0.0f;
}

float DTVLinuxDVB::getSignalNoiseRatio()
{
    uint16_t snr = 0;

    if (this->m_frontend >= 0 && ioctl(this->m_frontend, FE_READ_SNR, &snr) == 0)
        return snr / 65535.0f * 100.0f;

    return 0.0f;
}

QString DTVLinuxDVB::getChannelName()
{
    QString name;
    dvbpsi_t *parser = dvbpsi_new(this->messageCallback, DVBPSI_MSG_NONE);

    if (!parser)
        return name;

    this->m_psi.parser = parser;

    switch (this->m_type)
    {
        case ST_ATSC_C:
        case ST_ATSC_T:
        case ST_CQAM:
        {
            name = this->getATSCChannelName();
            break;
        }
        case ST_DVB_C:
        case ST_DVB_C2:
        case ST_DVB_S:
        case ST_DVB_S2:
        case ST_DVB_T:
        case ST_DVB_T2:
        case ST_ISDB_C:
        case ST_ISDB_T:
        case ST_ISDB_S:
        {
            name = this->getDVBChannelName();
            break;
        }
        default:
            break;
    }

    this->m_psi = PSISignal();
    dvbpsi_delete(parser);

    return name;
}

void DTVLinuxDVB::getEPG(QVector<DTVReaderInterface::EPG> *ret)
{
    dvbpsi_t *parser = dvbpsi_new(this->messageCallback, DVBPSI_MSG_NONE);

    if (!parser)
        return ;

    this->m_psi.parser = parser;

    switch (this->m_type)
    {
        case ST_ATSC_C:
        case ST_ATSC_T:
        case ST_CQAM:
        {
            this->getATSCEPG(ret);
            break;
        }
        case ST_DVB_C:
        case ST_DVB_C2:
        case ST_DVB_S:
        case ST_DVB_S2:
        case ST_DVB_T:
        case ST_DVB_T2:
        case ST_ISDB_C:
        case ST_ISDB_T:
        case ST_ISDB_S:
        {
            this->getDVBEPG(ret);
            break;
        }
        default:
            break;
    }

    this->m_psi = PSISignal();
    dvbpsi_delete(parser);
}

QDateTime DTVLinuxDVB::getCurrentDateTime()
{
    QDateTime time;
    dvbpsi_t *parser = dvbpsi_new(this->messageCallback, DVBPSI_MSG_NONE);

    if (!parser)
        return time;

    this->m_psi.parser = parser;

    switch (this->m_type)
    {
        case ST_ATSC_C:
        case ST_ATSC_T:
        case ST_CQAM:
        {
            time = this->getATSCCurrentDateTime();
            break;
        }
        case ST_DVB_C:
        case ST_DVB_C2:
        case ST_DVB_S:
        case ST_DVB_S2:
        case ST_DVB_T:
        case ST_DVB_T2:
        case ST_ISDB_C:
        case ST_ISDB_T:
        case ST_ISDB_S:
        {
            time = this->getDVBCurrentDateTime();
            break;
        }
        default:
            break;
    }

    this->m_psi = PSISignal();
    dvbpsi_delete(parser);

    return time;
}

bool DTVLinuxDVB::tune()
{
    if (this->setProp(DTV_TUNE, DTV_UNDEFINED))
    {
        for (int i = 0; i < 10; i++)
        {
            fe_status_t status;

            if (ioctl(this->m_frontend, FE_READ_STATUS, &status) < 0)
                return false;

            if ((status & FE_HAS_SIGNAL) && (status & FE_HAS_CARRIER) && (status & FE_HAS_LOCK))
                return true;

            QThread::msleep(50);
        }
    }

    return false;
}

bool DTVLinuxDVB::getAdapterList(QVector<DTVReaderInterface::Info> *ret)
{
    QDir dir(ROOT);

    dir.cdUp();

    foreach (const QString &adapter, dir.entryList(QDir::NoDotAndDotDot | QDir::Dirs))
    {
        DTVReaderInterface::Info info;
        QString full = dir.absoluteFilePath(adapter);
        int index = full.remove(ROOT).toInt();
        int fd = this->openNode(index, "frontend", O_RDWR);
        dvb_frontend_info cap;

        info.index = index;

        if (fd >= 0 && ioctl(fd, FE_GET_INFO, &cap) == 0)
        {
            info.name = QString::fromUtf8(cap.name);

            ::close(fd);
        }
        else
        {
            info.name = QString("%1%2").arg("adapter").arg(index);
        }

        ret->append(info);
    }

    return ret->count() > 0;
}

bool DTVLinuxDVB::setInversion(DTVReaderInterface::Inversion inversion)
{
    fe_spectral_inversion v;

    switch (inversion)
    {
        case IV_NORMAL:
            v = INVERSION_OFF;
            break;
        case IV_INVERTED:
            v = INVERSION_ON;
            break;
        default:
            v = INVERSION_AUTO;
            break;
    }

    return this->setProp(DTV_INVERSION, v);
}

bool DTVLinuxDVB::setDVBC(uint32_t freq, const QString &mod, uint32_t srate, uint32_t fec)
{
    fe_modulation_t m = this->getModulation(mod, QAM_AUTO);
    fe_code_rate_t f = this->getFEC(fec, FEC_AUTO);

    if (!this->openFrontend())
        return false;

    return this->setProps(6, DTV_CLEAR, DTV_UNDEFINED, DTV_DELIVERY_SYSTEM, SYS_DVBC_ANNEX_A,
                          DTV_FREQUENCY, freq, DTV_MODULATION, m, DTV_SYMBOL_RATE, srate, DTV_INNER_FEC, f);
}

bool DTVLinuxDVB::setDVBS(uint64_t freq, uint32_t srate, uint32_t fec)
{
    fe_code_rate_t f = this->getFEC(fec, FEC_AUTO);

    if (!this->openFrontend())
        return false;

    freq /= 1000;

    return this->setProps(5, DTV_CLEAR, DTV_UNDEFINED, DTV_DELIVERY_SYSTEM, SYS_DVBS,
                          DTV_FREQUENCY, freq, DTV_SYMBOL_RATE, srate, DTV_INNER_FEC, f);
}

bool DTVLinuxDVB::setDVBS2(uint64_t freq, const QString &mod, uint32_t srate, uint32_t fec, int pilot, int rolloff, uint8_t sid)
{
    fe_modulation_t m = this->getModulation(mod, QPSK);
    fe_code_rate_t f = this->getFEC(fec, FEC_AUTO);
    fe_pilot_t p;
    fe_rolloff_t r;

    switch (pilot)
    {
        case 0:
            p = PILOT_OFF;
            break;
        case 1:
            p = PILOT_ON;
            break;
        default:
            p = PILOT_AUTO;
            break;
    }

    switch (rolloff)
    {
        case 20:
            r = ROLLOFF_20;
            break;
        case 25:
            r = ROLLOFF_25;
            break;
        case 35:
            r = ROLLOFF_35;
            break;
        default:
            r = ROLLOFF_AUTO;
            break;
    }

    if (!this->openFrontend())
        return false;

    freq /= 1000;

    return this->setProps(9, DTV_CLEAR, DTV_UNDEFINED, DTV_DELIVERY_SYSTEM, SYS_DVBS2,
                          DTV_FREQUENCY, freq, DTV_MODULATION, m, DTV_SYMBOL_RATE, srate,
                          DTV_INNER_FEC, f, DTV_PILOT, p, DTV_ROLLOFF, r, DTV_STREAM_ID, (uint32_t)sid);

}

bool DTVLinuxDVB::setSEC(uint64_t freq, uint32_t srate, uint32_t fec, char pol, uint32_t lowf, uint32_t highf, uint32_t switchf,
                         int32_t highv, uint32_t satNo, uint32_t uncommitted)
{
    (void)srate;
    (void)fec;

    ioctl(this->m_frontend, FE_ENABLE_HIGH_LNB_VOLTAGE, &highv);

    freq /= 1000;

    if (!lowf)
    {   /* Default oscillator frequencies */
        static const struct
        {
            uint16_t min, max, low, high;
        } tab[] =
        {    /*  min    max    low   high */
            { 10700, 13250,  9750, 10600 }, /* Ku band */
            {  4500,  4800,  5950,     0 }, /* C band (high) */
            {  3400,  4200,  5150,     0 }, /* C band (low) */
            {  2500,  2700,  3650,     0 }, /* S band */
            {   950,  2150,     0,     0 }, /* adjusted IF (L band) */
        };

        uint_fast16_t mHz = freq / 1000;

        for (size_t i = 0; i < sizeof(tab) / sizeof(tab[0]); i++)
        {
            if (mHz >= tab[i].min && mHz <= tab[i].max)
            {
                lowf = tab[i].low * 1000;
                highf = tab[i].high * 1000;

                break;
            }
        }
    }

    /* Use high oscillator frequency? */
    bool high = highf != 0 && freq > switchf;

    freq -= high ? highf : lowf;

    if ((int32_t)freq < 0)
        freq *= -1;

    fe_sec_tone_mode_t tone = high ? SEC_TONE_ON : SEC_TONE_OFF;

    /*** LNB selection / DiSEqC ***/
    fe_sec_voltage_t voltage = this->getPolarization(pol, SEC_VOLTAGE_OFF);

    if (!this->setProps(2, DTV_TONE, SEC_TONE_OFF, DTV_VOLTAGE, voltage))
        return false;

    if (satNo > 0)
    {
        /* DiSEqC 1.1 */
        dvb_diseqc_master_cmd uncmd;

        QThread::msleep(15000); /* wait 15 ms before DiSEqC command */

        if (uncommitted > 0)
        {
            uncommitted = (uncommitted - 1) & 3;

            uncmd.msg[0] = 0xE0; /* framing: master, no reply, 1st TX */
            uncmd.msg[1] = 0x10; /* address: all LNB/switch */
            uncmd.msg[2] = 0x39; /* command: Write Port Group 1 (uncommitted) */
            uncmd.msg[3] = 0xF0  /* data[0]: clear all bits */
                    | (uncommitted << 2) /* LNB (A, B, C or D) */
                    | ((voltage == SEC_VOLTAGE_18) << 1) /* polarization */
                    | (tone == SEC_TONE_ON); /* option */
            uncmd.msg[4] = uncmd.msg[5] = 0; /* unused */
            uncmd.msg_len = 4; /* length */

            if (ioctl(this->m_frontend, FE_DISEQC_SEND_MASTER_CMD, &uncmd) < 0)
                return false;

            /* Repeat uncommitted command */
            uncmd.msg[0] = 0xE1; /* framing: master, no reply, repeated TX */

            if (ioctl(this->m_frontend, FE_DISEQC_SEND_MASTER_CMD, &uncmd) < 0)
                return false;

            QThread::msleep(125000); /* wait 125 ms before committed DiSEqC command */
        }

        /* DiSEqC 1.0 */
        dvb_diseqc_master_cmd cmd;

        satNo = (satNo - 1) & 3;

        cmd.msg[0] = 0xE0; /* framing: master, no reply, 1st TX */
        cmd.msg[1] = 0x10; /* address: all LNB/switch */
        cmd.msg[2] = 0x38; /* command: Write Port Group 0 (committed) */
        cmd.msg[3] = 0xF0  /* data[0]: clear all bits */
                | (satNo << 2) /* LNB (A, B, C or D) */
                | ((voltage == SEC_VOLTAGE_18) << 1) /* polarization */
                | (tone == SEC_TONE_ON); /* option */
        cmd.msg[4] = cmd.msg[5] = 0; /* unused */
        cmd.msg_len = 4; /* length */

        if (ioctl(this->m_frontend, FE_DISEQC_SEND_MASTER_CMD, &cmd) < 0)
            return false;

        QThread::msleep(54000 + 15000);

        /* Mini-DiSEqC */
        satNo &= 1;

        if (ioctl(this->m_frontend, FE_DISEQC_SEND_BURST, satNo ? SEC_MINI_B : SEC_MINI_A) < 0)
            return false;

        QThread::msleep(15000);
    }

    /* Continuous tone (to select high oscillator frequency) */
    return this->setProps(2, DTV_FREQUENCY, freq, DTV_TONE, tone);
}

bool DTVLinuxDVB::setDVBT(uint32_t freq, const QString &mod, uint32_t fecHP, uint32_t fecLP, uint32_t bandwidth, int transmission, uint32_t guard, int hierarchy)
{
    fe_modulation_t m = this->getModulation(mod, QAM_AUTO);
    fe_code_rate_t fhp = this->getFEC(fecHP, FEC_AUTO);
    fe_code_rate_t flp = this->getFEC(fecLP, FEC_AUTO);
    uint32_t b = this->getBandwidth(bandwidth);
    fe_transmit_mode_t t = this->getTransmission(transmission, TRANSMISSION_MODE_AUTO);
    fe_guard_interval_t g = this->getGuard(guard, GUARD_INTERVAL_AUTO);
    fe_hierarchy_t h = this->getHierarchy(hierarchy, HIERARCHY_AUTO);

    if (!this->openFrontend())
        return false;

    return this->setProps(10, DTV_CLEAR, DTV_UNDEFINED, DTV_DELIVERY_SYSTEM, SYS_DVBT,
                          DTV_FREQUENCY, freq, DTV_MODULATION, m,
                          DTV_CODE_RATE_HP, fhp, DTV_CODE_RATE_LP, flp, DTV_BANDWIDTH_HZ, b,
                          DTV_TRANSMISSION_MODE, t, DTV_GUARD_INTERVAL, g, DTV_HIERARCHY, h);
}

bool DTVLinuxDVB::setDVBT2(uint32_t freq, const QString &mod, uint32_t fec, uint32_t bandwidth, int transmission, uint32_t guard, uint8_t plp)
{
    fe_modulation_t m = this->getModulation(mod, QAM_AUTO);
    fe_code_rate_t f = this->getFEC(fec, FEC_AUTO);
    uint32_t b = this->getBandwidth(bandwidth);
    fe_transmit_mode_t t = this->getTransmission(transmission, TRANSMISSION_MODE_AUTO);
    fe_guard_interval_t g = this->getGuard(guard, GUARD_INTERVAL_AUTO);

    if (!this->openFrontend())
        return false;

    return this->setProps(9, DTV_CLEAR, DTV_UNDEFINED, DTV_DELIVERY_SYSTEM, SYS_DVBT2,
                          DTV_FREQUENCY, freq, DTV_MODULATION, m, DTV_INNER_FEC, f,
                          DTV_BANDWIDTH_HZ, b, DTV_TRANSMISSION_MODE, t, DTV_GUARD_INTERVAL, g,
                          DTV_STREAM_ID, (uint32_t)plp);
}

bool DTVLinuxDVB::setATSC(uint32_t freq, const QString &mod)
{
    fe_modulation_t m = this->getModulation(mod, VSB_8);

    if (!this->openFrontend())
        return false;

    freq += 1750000;

    return this->setProps(4, DTV_CLEAR, DTV_UNDEFINED, DTV_DELIVERY_SYSTEM, SYS_ATSC,
                          DTV_FREQUENCY, freq, DTV_MODULATION, m);
}

bool DTVLinuxDVB::setCQAM(uint32_t freq, const QString &mod)
{
    fe_modulation_t m = this->getModulation(mod, VSB_8);

    if (!this->openFrontend())
        return false;

    freq += 1750000;

    return this->setProps(4, DTV_CLEAR, DTV_UNDEFINED, DTV_DELIVERY_SYSTEM, SYS_DVBC_ANNEX_B,
                          DTV_FREQUENCY, freq, DTV_MODULATION, m);
}

bool DTVLinuxDVB::setISDBC(uint32_t freq, const QString &mod, uint32_t srate, uint32_t fec)
{
    fe_modulation_t m = this->getModulation(mod, QAM_AUTO);
    fe_code_rate_t f = this->getFEC(fec, FEC_AUTO);

    if (!this->openFrontend())
        return false;

    return this->setProps(6, DTV_CLEAR, DTV_UNDEFINED, DTV_DELIVERY_SYSTEM, SYS_DVBC_ANNEX_C,
                          DTV_FREQUENCY, freq, DTV_MODULATION, m, DTV_SYMBOL_RATE, srate, DTV_INNER_FEC, f);
}

bool DTVLinuxDVB::setISDBS(uint64_t freq, uint16_t tsID)
{
    if (!this->openFrontend())
        return false;

    freq /= 1000;

    return this->setProps(4, DTV_CLEAR, DTV_UNDEFINED, DTV_DELIVERY_SYSTEM, SYS_ISDBS, DTV_FREQUENCY, freq, DTV_STREAM_ID, (uint32_t)tsID);
}

bool DTVLinuxDVB::setISDBT(uint32_t freq, uint32_t bandwidth, int transmission, uint32_t guard,
                           const DTVReaderInterface::ISDBTLayer layers[3])
{
    (void)transmission;

    uint32_t b = this->getBandwidth(bandwidth);
    fe_guard_interval_t g = this->getGuard(guard, GUARD_INTERVAL_AUTO);

    if (!this->openFrontend())
        return false;

    if (!this->setProps(5, DTV_CLEAR, DTV_UNDEFINED, DTV_DELIVERY_SYSTEM, SYS_ISDBT,
                        DTV_FREQUENCY, freq, DTV_BANDWIDTH_HZ, b, DTV_GUARD_INTERVAL, g))
        return false;

    for (int i = 0; i < 3; i++)
    {
        const DTVReaderInterface::ISDBTLayer &l = layers[i];
        fe_modulation_t m = this->getModulation(l.mod, QAM_AUTO);
        fe_code_rate_t f = this->getFEC(l.codeRate, FEC_AUTO);
        uint32_t count = l.segmentCount;
        uint32_t ti = l.timeInterleaving;
        int num = i;

        num *= DTV_ISDBT_LAYERB_FEC - DTV_ISDBT_LAYERA_FEC;

        return this->setProps(5, DTV_DELIVERY_SYSTEM, SYS_ISDBT, DTV_ISDBT_LAYERA_FEC + num, f,
                              DTV_ISDBT_LAYERA_MODULATION + num, m,
                              DTV_ISDBT_LAYERA_SEGMENT_COUNT + num, count,
                              DTV_ISDBT_LAYERA_TIME_INTERLEAVING + num, ti);
    }

    return true;
}

int DTVLinuxDVB::openNode(long adapter, const QString &node, int flags) const
{
    QString path = QString("%1%2/%3%4")
            .arg(ROOT)
            .arg(adapter)
            .arg(node)
            .arg(adapter);

    return ::open(path.toLatin1(), flags | O_NONBLOCK);
}

bool DTVLinuxDVB::openFrontend()
{
    this->m_frontend = this->openNode(this->m_adapter, "frontend", O_RDWR);

    return this->m_frontend >= 0;
}

void DTVLinuxDVB::destroy()
{
    if (this->m_frontend != -1)
    {
        ::close(this->m_frontend);
        this->m_frontend = -1;
    }

    if (this->m_demux != -1)
    {
        ::close(this->m_demux);
        this->m_demux = -1;
    }
}

bool DTVLinuxDVB::setVProps(size_t n, va_list ap)
{
    dtv_property buf[n];
    dtv_property *prop = buf;
    dtv_properties props = { .num = (__u32)n, .props = buf };

    memset(buf, 0, sizeof (buf));

    while (n > 0)
    {
        prop->cmd = va_arg(ap, uint32_t);
        prop->u.data = va_arg(ap, uint32_t);

        prop++;
        n--;
    }

    if (ioctl(this->m_frontend, FE_SET_PROPERTY, &props) < 0)
        return false;

    return true;
}

bool DTVLinuxDVB::setProps(size_t n, ...)
{
    va_list ap;
    bool ret;

    va_start(ap, n);
    ret = this->setVProps(n, ap);
    va_end (ap);

    return ret;
}

bool DTVLinuxDVB::setProp(uint32_t prop, uint32_t val)
{
    return this->setProps(1, prop, val);
}

void DTVLinuxDVB::dvbMJDToUTC(uint32_t date, int *year, int *month, int *day) const
{
    int yTmp = int((date - 15078.2) / 365.25);
    int mTmp = int ((date - 14956.1 - int(yTmp * 365.25)) / 30.6001);
    int k;

    *day = date - 14956 - int(yTmp * 365.25) - int (mTmp * 30.6001);

    if (mTmp == 14 || mTmp == 15)
        k = 1;
    else
        k = 0;

    *year = yTmp + k + 1900;
    *month = mTmp - 1 - k * 12;
}

uint32_t DTVLinuxDVB::dvbDecodeTime(uint32_t time) const
{
    uint32_t result = 0;

    result += this->bcdToInt((time >> 16) & 0xff) * 3600;
    result += this->bcdToInt((time >> 8) & 0xff) * 60;
    result += this->bcdToInt(time & 0xff);

    return result;
}

uint8_t DTVLinuxDVB::bcdToInt(uint8_t bcd) const
{
    uint8_t result = 0;

    result += ((bcd >> 4) & 0x0f) * 10;
    result += bcd & 0x0f;

    return result;
}

void DTVLinuxDVB::getATSCEPG(QVector<DTVReaderInterface::EPG> *ret)
{
    if (!dvbpsi_AttachDemux(this->m_psi.parser, this->atscMGTDemuxCallback, this))
        return;

    this->pushPSIData(this->m_psi, ATSC_MGT_TID, ATSC_MGT_PID, 1000);

    dvbpsi_atsc_DetachMGT(this->m_psi.parser, ATSC_MGT_TID, this->m_psi.tsID);
    dvbpsi_DetachDemux(this->m_psi.parser);

    foreach (int pid, this->m_psi.pids)
    {
        this->m_psi.got = false;
        this->m_psi.tid = 0;
        this->m_psi.tsID = 0;

        if (!dvbpsi_AttachDemux(this->m_psi.parser, this->atscEITDemuxCallback, this))
            return;

        this->pushPSIData(this->m_psi, ATSC_EIT_TID, pid, 5000);

        dvbpsi_atsc_DetachEIT(this->m_psi.parser, ATSC_EIT_TID, this->m_psi.tsID);
        dvbpsi_DetachDemux(this->m_psi.parser);
    }

    *ret = this->m_psi.epgs;
}

void DTVLinuxDVB::getDVBEPG(QVector<DTVReaderInterface::EPG> *ret)
{
    this->getDVBEPGSub(DVB_EIT_ACTUAL_TID, ret);

    if (ret->isEmpty())
        this->getDVBEPGSub(DVB_EIT_OTHER_TID, ret);
}

void DTVLinuxDVB::getDVBEPGSub(int tid, QVector<DTVReaderInterface::EPG> *ret)
{
    if (!dvbpsi_AttachDemux(this->m_psi.parser, this->dvbEITDemuxCallback, this))
        return;

    this->pushPSIData(this->m_psi, tid, DVB_EIT_PID, 5000);

    dvbpsi_eit_detach(this->m_psi.parser, tid, this->m_psi.tsID);
    dvbpsi_DetachDemux(this->m_psi.parser);

    *ret = this->m_psi.epgs;
}

QDateTime DTVLinuxDVB::getATSCCurrentDateTime()
{
    if (!dvbpsi_AttachDemux(this->m_psi.parser, this->atscSTTDemuxCallback, this))
        return QDateTime();

    this->pushPSIData(this->m_psi, ATSC_STT_TID, ATSC_STT_PID, 3000);

    dvbpsi_atsc_DetachSTT(this->m_psi.parser, ATSC_STT_TID, this->m_psi.tsID);
    dvbpsi_DetachDemux(this->m_psi.parser);

    return this->m_psi.curDateTime;
}

QDateTime DTVLinuxDVB::getDVBCurrentDateTime()
{
    if (!dvbpsi_AttachDemux(this->m_psi.parser, this->dvbTDTDemuxCallback, this))
        return QDateTime();

    this->pushPSIData(this->m_psi, DVB_TDT_TID, DVB_TDT_PID, 3000);

    dvbpsi_tot_detach(this->m_psi.parser, DVB_TDT_TID, this->m_psi.tsID);
    dvbpsi_DetachDemux(this->m_psi.parser);

    return this->m_psi.curDateTime;
}

QString DTVLinuxDVB::getATSCChannelName()
{
    QString name;

    name = this->getATSCChannelNameSub(ATSC_VCT_TERR_TID);

    if (name.isEmpty())
        name = this->getATSCChannelNameSub(ATSC_VCT_CABL_TID);

    return name;
}

QString DTVLinuxDVB::getATSCChannelNameSub(int tid)
{
    QString name;

    if (!dvbpsi_AttachDemux(this->m_psi.parser, this->atscVCTDemuxCallback, this))
        return name;

    this->pushPSIData(this->m_psi, tid, ATSC_VCT_PID, 1000);

    name = this->m_psi.name;

    dvbpsi_atsc_DetachVCT(this->m_psi.parser, tid, this->m_psi.tsID);
    dvbpsi_DetachDemux(this->m_psi.parser);

    return name;
}

QString DTVLinuxDVB::getDVBChannelName()
{
    QString name;

    name = this->getDVBChannelNameSub(DVB_SDT_ACTUAL_TID);

    if (name.isEmpty())
        name = this->getDVBChannelNameSub(DVB_SDT_OTHER_TID);

    return name;
}

QString DTVLinuxDVB::getDVBChannelNameSub(int tid)
{
    QString name;

    if (!dvbpsi_AttachDemux(this->m_psi.parser, this->dvbSDTDemuxCallback, this))
        return name;

    this->pushPSIData(this->m_psi, tid, DVB_SDT_PID, 1000);

    name = this->m_psi.name;

    dvbpsi_sdt_detach(this->m_psi.parser, tid, this->m_psi.tsID);
    dvbpsi_DetachDemux(this->m_psi.parser);

    return name;
}

void DTVLinuxDVB::pushPSIData(DTVLinuxDVB::PSISignal &psi, int tid, int pid, int maxTime) const
{
    QTime timer;
    int demux = this->openNode(this->m_adapter, "demux", O_RDONLY);

    if (demux < 0)
        return;

    ioctl(demux, DMX_SET_BUFFER_SIZE, 1 << 20);

    dmx_pes_filter_params param;

    param.pid = pid;
    param.input = DMX_IN_FRONTEND;
    param.output = DMX_OUT_TSDEMUX_TAP;
    param.pes_type = DMX_PES_OTHER;
    param.flags = DMX_IMMEDIATE_START;

    if (ioctl(demux, DMX_SET_PES_FILTER, &param) < 0)
    {
        ::close(demux);
        return;
    }

    psi.tid = tid;
    timer.start();

    while (true)
    {
        pollfd fd;
        uint8_t buf[188];

        if (psi.got || timer.elapsed() > maxTime)
            break;

        fd.fd = demux;
        fd.events = POLLIN;

        if (poll(&fd, 1, 500) <= 0)
            continue;

        if (fd.revents)
        {
            int index = 187;
            ssize_t ret = 1;

            buf[0] = 0;

            while (buf[0] != DTV_SYNC_BYTE && ret > 0)
                ret = ::read(fd.fd, buf, 1);

            while (index != 0 && ret > 0)
            {
                ret = ::read(fd.fd, buf + 188 - index, index);

                if (ret > 0)
                    index -= ret;
            }

            dvbpsi_packet_push(psi.parser, buf);
        }
    }

    ::close(demux);
}

QString DTVLinuxDVB::decodeDVBString(uint8_t *str, int len) const
{
    QString string;
    QString encoding;
    int start = 1;

    /* https://www.dvb.org/resources/public/standards/a38_dvb-si_specification.pdf */
    /* Detect encoding */
    switch (str[0])
    {
        case 0x01:
            encoding = "ISO-8859-5";
            break;
        case 0x02:
            encoding = "ISO-8859-6";
            break;
        case 0x03:
            encoding = "ISO-8859-7";
            break;
        case 0x04:
            encoding = "ISO-8859-8";
            break;
        case 0x05:
            encoding = "ISO-8859-9";
            break;
        case 0x06:
            encoding = "ISO-8859-10";
            break;
        case 0x07:
            encoding = "ISO-8859-11";
            break;
        case 0x09:
            encoding = "ISO-8859-13";
            break;
        case 0x0A:
            encoding = "ISO-8859-14";
            break;
        case 0x0B:
            encoding = "ISO-8859-15";
            break;
        case 0x11:
            encoding = "UTF-16";
            break;
        case 0x12:
            encoding = "EUC-KR";
            break;
        case 0x13:
            encoding = "GB2312";
            break;
        case 0x14:
            encoding = "Big5";
            break;
        case 0x15:
            encoding = "UTF-8";
            break;
        default:
            break;
    }

    switch (str[0])
    {
        case 0x10:
        {
            start = 3;

            uint8_t second = str[1];
            uint8_t third = str[2];

            switch (second)
            {
                case 0x00:
                {
                    switch (third)
                    {
                        case 0x01:
                            encoding = "ISO-8859-1";
                            break;
                        case 0x02:
                            encoding = "ISO-8859-2";
                            break;
                        case 0x03:
                            encoding = "ISO-8859-3";
                            break;
                        case 0x04:
                            encoding = "ISO-8859-4";
                            break;
                        case 0x05:
                            encoding = "ISO-8859-5";
                            break;
                        case 0x06:
                            encoding = "ISO-8859-6";
                            break;
                        case 0x07:
                            encoding = "ISO-8859-7";
                            break;
                        case 0x08:
                            encoding = "ISO-8859-8";
                            break;
                        case 0x09:
                            encoding = "ISO-8859-9";
                            break;
                        case 0x0A:
                            encoding = "ISO-8859-10";
                            break;
                        case 0x0B:
                            encoding = "ISO-8859-11";
                            break;
                        case 0x0D:
                            encoding = "ISO-8859-13";
                            break;
                        case 0x0E:
                            encoding = "ISO-8859-14";
                            break;
                        case 0x0:
                            encoding = "ISO-8859-15";
                            break;
                        default:
                            break;
                    }

                    break;
                }
                default:
                {
                    break;
                }
            }

            break;
        }
        case 0x1F:
        {
            start = 2;
            /* nothing to do*/
            break;
        }
        default:
        {
            break;
        }
    }

    int cur = 0;
    char buf[len];

    /* Copy string */
    for (int i = start; i < len; i++)
    {
        switch (*(str + i))
        {
            case 0x80 ... 0x85:
            case 0x88 ... 0x89:
            case 0x8B ... 0x9F:
            case 0x8A:
            case 0x86:
            case 0x87:
            {
                continue;
            }
            case 0xC2:
            {
                if (encoding == "UTF-8" && i + 1 < len)
                {
                    uint16_t utf8CC;

                    utf8CC = *(str + i) << 8;
                    utf8CC += *(str + i + 1);

                    switch (utf8CC)
                    {
                        case 0xC280 ... 0xC285:
                        case 0xC288 ... 0xC289:
                        case 0xC28B ... 0xC29F:
                        case 0xC28A:
                        case 0xC286:
                        case 0xC287:
                            i++;
                            continue;
                        default:
                            break;
                    }
                }

                break;
            }
            case 0xEE:
            {
                if (encoding == "UTF-16" && i + 2 < len)
                {
                    uint32_t utf16CC;

                    utf16CC = *(str + i) << 16;
                    utf16CC += *(str + i + 1) << 8;
                    utf16CC += *(str + i + 2);

                    switch (utf16CC)
                    {
                        case 0xEE8280 ... 0xEE8285:
                        case 0xEE8288 ... 0xEE8289:
                        case 0xEE828B ... 0xEE829F:
                        case 0xEE828A:
                        case 0xEE8286:
                        case 0xEE8287:
                            i++;
                            i++;
                            continue;
                        default:
                            break;
                    }
                }

                break;
            }
            default:
            {
                buf[cur++] = *(str + i);
                continue;
            }
        }
    }

    QTextCodec *codec = QTextCodec::codecForName(encoding.toLatin1());

    if (!codec)
        codec = QTextCodec::codecForLocale();

    if (codec)
        string = codec->toUnicode(buf, cur - 1);

    return string;
}

fe_modulation_t DTVLinuxDVB::getModulation(const QString &mod, fe_modulation_t def) const
{
    if (mod == "128QAM")
        return QAM_128;
    else if (mod == "16APSK")
        return APSK_16;
    else if (mod == "16QAM")
        return QAM_16;
    else if (mod == "16VSB")
        return VSB_16;
    else if (mod == "32APSK")
        return APSK_32;
    else if (mod == "32QAM")
        return QAM_32;
    else if (mod == "64QAM")
        return QAM_64;
    else if (mod == "8PSK")
        return PSK_8;
    else if (mod == "8VSB")
        return VSB_8;
    else if (mod == "DQPSK")
        return DQPSK;
    else if (mod == "QAM")
        return QAM_AUTO;
    else if (mod == "QPSK")
        return QPSK;
    else
        return def;
}

fe_code_rate_t DTVLinuxDVB::getFEC(uint32_t fec, fe_code_rate_t def) const
{
    switch (fec)
    {
        case 0:
            return FEC_NONE;
        case DTV_FEC(1, 2):
            return FEC_1_2;
        case DTV_FEC(2, 3):
            return FEC_2_3;
        case DTV_FEC(3, 4):
            return FEC_3_4;
        case DTV_FEC(3, 5):
            return FEC_3_5;
        case DTV_FEC(4, 5):
            return FEC_4_5;
        case DTV_FEC(5, 6):
            return FEC_5_6;
        case DTV_FEC(6, 7):
            return FEC_6_7;
        case DTV_FEC(7, 8):
            return FEC_7_8;
        case DTV_FEC(8, 9):
            return FEC_8_9;
        case DTV_FEC(9, 10):
            return FEC_8_9;
        case DTV_FEC_AUTO:
            return FEC_AUTO;
        default:
            return def;
    }
}

uint32_t DTVLinuxDVB::getBandwidth(uint32_t bandwidth) const
{
    switch (bandwidth)
    {
        case  2:
            return 1712000;
        default:
            return bandwidth * 1000000;
    }
}

fe_transmit_mode_t DTVLinuxDVB::getTransmission(int transmission, fe_transmit_mode_t def) const
{
    switch (transmission)
    {
        case -1:
            return TRANSMISSION_MODE_AUTO;
        case 1:
            return TRANSMISSION_MODE_1K;
        case 2:
            return TRANSMISSION_MODE_2K;
        case 4:
            return TRANSMISSION_MODE_4K;
        case 8:
            return TRANSMISSION_MODE_8K;
        case 16:
            return TRANSMISSION_MODE_16K;
        case 32:
            return TRANSMISSION_MODE_32K;
        default:
            return def;
    }
}

fe_guard_interval_t DTVLinuxDVB::getGuard(uint32_t guard, fe_guard_interval_t def) const
{
    switch (guard)
    {
        case DTV_GUARD(1, 4):
            return GUARD_INTERVAL_1_4;
        case DTV_GUARD(1, 8):
            return GUARD_INTERVAL_1_8;
        case DTV_GUARD(1, 16):
            return GUARD_INTERVAL_1_16;
        case DTV_GUARD(1, 32):
            return GUARD_INTERVAL_1_32;
        case DTV_GUARD(1, 128):
            return GUARD_INTERVAL_1_128;
        case DTV_GUARD(19, 128):
            return GUARD_INTERVAL_19_128;
        case DTV_GUARD(19, 256):
            return GUARD_INTERVAL_19_256;
        case DTV_GUARD_AUTO:
            return GUARD_INTERVAL_AUTO;
        default:
            return def;
    }
}

fe_hierarchy_t DTVLinuxDVB::getHierarchy(int hierarchy, fe_hierarchy_t def) const
{
    switch (hierarchy)
    {
        case -1:
            return HIERARCHY_AUTO;
        case 0:
            return HIERARCHY_NONE;
        case 1:
            return HIERARCHY_1;
        case 2:
            return HIERARCHY_2;
        case 4:
            return HIERARCHY_4;
        default:
            return def;
    }
}

fe_sec_voltage_t DTVLinuxDVB::getPolarization(char pol, fe_sec_voltage_t def) const
{
    switch (pol)
    {
        case ' ':
            return SEC_VOLTAGE_OFF;
        case 'H':
        case 'L':
            return SEC_VOLTAGE_18;
        case 'R':
        case 'V':
            return SEC_VOLTAGE_13;
        default:
            return def;
    }
}

void DTVLinuxDVB::messageCallback(dvbpsi_t *parser, const dvbpsi_msg_level_t level, const char *msg)
{
    (void)parser;
    (void)level;
    (void)msg;
}

void DTVLinuxDVB::atscEITCallback(void *opaque, dvbpsi_atsc_eit_t *eit)
{
    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;
    dvbpsi_atsc_eit_event_t *event = eit->p_first_event;

    while (event)
    {
        DTVReaderInterface::EPG epg;

        epg.start = QDateTime(QDate(1980, 1, 6), QTime(0, 0), Qt::UTC).addSecs(event->i_start_time);
        epg.duration = QTime::fromMSecsSinceStartOfDay(event->i_length_seconds * 1000ull);
        epg.title = p->decodeMultipleStringStructure(event->i_title, event->i_title_length);

        p->m_psi.epgs.append(epg);

        event = event->p_next;
    }

    p->m_psi.got = true;
}

void DTVLinuxDVB::atscMGTCallback(void *opaque, dvbpsi_atsc_mgt_t *mgt)
{
    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;
    dvbpsi_atsc_mgt_table_t *mgtTable = mgt->p_first_table;

    while (mgtTable)
    {
        if (mgtTable->i_table_type >= ATSC_MGT_TYPE_START && mgtTable->i_table_type <= ATSC_MGT_TYPE_END)
            p->m_psi.pids.append(mgtTable->i_table_type_pid);

        mgtTable = mgtTable->p_next;
    }

    p->m_psi.got = true;
}

void DTVLinuxDVB::atscVCTCallback(void *opaque, dvbpsi_atsc_vct_t *vct)
{
    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;
    dvbpsi_atsc_vct_channel_t *channel = vct->p_first_channel;

    while (channel)
    {
        bool ok = false;
        QString channelName;
        uint16_t major = channel->i_major_number;
        uint16_t minor = channel->i_minor_number;
        int channelSize = sizeof(channel->i_short_name);
        uint16_t tmp[channelSize];

        memset(tmp, 0, sizeof(tmp));
        memcpy(tmp, channel->i_short_name, channelSize);

        for (int i = 0; i < channelSize; i++)
            tmp[i] = ntohs(tmp[i]);

        channelName = QString::fromUtf16(tmp).trimmed();

        p->m_psi.name = QString("%1 [%2-%3]")
                .arg(channelName)
                .arg(major)
                .arg(minor);

        if (!channelName.isEmpty() || (major > 0 && major <= 99) || (minor > 0 && minor <= 99))
            ok = true;

        if (ok)
            break;

        channel = channel->p_next;
    }

    p->m_psi.got = true;
}

void DTVLinuxDVB::atscSTTCallback(void *opaque, dvbpsi_atsc_stt_t *stt)
{
    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;

    p->m_psi.curDateTime = QDateTime(QDate(1980, 1, 6), QTime(0, 0), Qt::UTC).addSecs(stt->i_system_time);

    p->m_psi.got = true;
}

void DTVLinuxDVB::atscEITDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque)
{
    (void)parser;

    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;

    if (tid == p->m_psi.tid && p->m_psi.tsID == 0)
    {
        dvbpsi_atsc_AttachEIT(parser, tid, tsID, p->atscEITCallback, p);
        p->m_psi.tsID = tsID;
    }
}

void DTVLinuxDVB::atscMGTDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque)
{
    (void)parser;

    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;

    if (tid == p->m_psi.tid && p->m_psi.tsID == 0)
    {
        dvbpsi_atsc_AttachMGT(parser, tid, tsID, p->atscMGTCallback, p);
        p->m_psi.tsID = tsID;
    }
}

void DTVLinuxDVB::atscVCTDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque)
{
    (void)parser;

    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;

    if (tid == p->m_psi.tid && p->m_psi.tsID == 0)
    {
        dvbpsi_atsc_AttachVCT(parser, tid, tsID, p->atscVCTCallback, p);
        p->m_psi.tsID = tsID;
    }
}

void DTVLinuxDVB::atscSTTDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque)
{
    (void)parser;

    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;

    if (tid == p->m_psi.tid && p->m_psi.tsID == 0)
    {
        dvbpsi_atsc_AttachSTT(parser, tid, tsID, p->atscSTTCallback, p);
        p->m_psi.tsID = tsID;
    }
}

void DTVLinuxDVB::dvbSDTCallback(void *opaque, dvbpsi_sdt_t *sdt)
{
    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;
    dvbpsi_sdt_service_t *service = sdt->p_first_service;

    while (service)
    {
        bool ok = false;
        dvbpsi_descriptor_t *desc = service->p_first_descriptor;

        for (; desc; desc = desc->p_next)
        {
            dvbpsi_service_dr_t *serviceDesc = dvbpsi_DecodeServiceDr(desc);

            if (!serviceDesc)
                continue;

            QString channelName = p->decodeDVBString(serviceDesc->i_service_name, serviceDesc->i_service_name_length);

            p->m_psi.name = channelName;

            if (!channelName.isEmpty())
                ok = true;

            if (ok)
                break;
        }

        if (ok)
            break;

        service = service->p_next;
    }

    p->m_psi.got = true;
}

void DTVLinuxDVB::dvbSDTDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque)
{
    (void)parser;

    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;

    if (tid == p->m_psi.tid && p->m_psi.tsID == 0)
    {
        dvbpsi_sdt_attach(parser, tid, tsID, p->dvbSDTCallback, p);
        p->m_psi.tsID = tsID;
    }
}

void DTVLinuxDVB::dvbEITCallback(void *opaque, dvbpsi_eit_t *eit)
{
    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;
    dvbpsi_eit_event_t *event = eit->p_first_event;

    while (event)
    {
        dvbpsi_descriptor_t *desc = event->p_first_descriptor;
        DTVReaderInterface::EPG epg;
        uint32_t start = p->dvbDecodeTime((uint32_t)event->i_start_time);
        int year;
        int month;
        int day;

        p->dvbMJDToUTC(event->i_start_time >> 24, &year, &month, &day);

        epg.duration = QTime::fromMSecsSinceStartOfDay(p->dvbDecodeTime(event->i_duration) * 1000ull);
        epg.start = QDateTime(QDate(year, month, day), QTime(0, 0), Qt::UTC).addSecs(start);

        for (; desc; desc = desc->p_next)
        {
            dvbpsi_short_event_dr_t *shortEventDesc = dvbpsi_DecodeShortEventDr(desc);

            if (!shortEventDesc)
                continue;

            epg.title += p->decodeDVBString(shortEventDesc->i_event_name, shortEventDesc->i_event_name_length);
        }

        p->m_psi.epgs.append(epg);

        event = event->p_next;
    }

    p->m_psi.got = true;
}

void DTVLinuxDVB::dvbEITDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque)
{
    (void)parser;

    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;

    if (tid == p->m_psi.tid && p->m_psi.tsID == 0)
    {
        dvbpsi_eit_attach(parser, tid, tsID, p->dvbEITCallback, p);
        p->m_psi.tsID = tsID;
    }
}

void DTVLinuxDVB::dvbTDTCallback(void *opaque, dvbpsi_tot_t *tot)
{
    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;
    uint32_t time = p->dvbDecodeTime((uint32_t)tot->i_utc_time);
    int year;
    int month;
    int day;

    p->dvbMJDToUTC(tot->i_utc_time >> 24, &year, &month, &day);

    p->m_psi.curDateTime = QDateTime(QDate(year, month, day), QTime(0, 0), Qt::UTC).addSecs(time);
    p->m_psi.got = true;
}

void DTVLinuxDVB::dvbTDTDemuxCallback(dvbpsi_t *parser, uint8_t tid, uint16_t tsID, void *opaque)
{
    (void)parser;

    DTVLinuxDVB *p = (DTVLinuxDVB*)opaque;

    if (tid == p->m_psi.tid && p->m_psi.tsID == 0)
    {
        dvbpsi_tot_attach(parser, tid, tsID, p->dvbTDTCallback, p);
        p->m_psi.tsID = tsID;
    }
}

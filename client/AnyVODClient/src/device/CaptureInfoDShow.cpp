﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "CaptureInfoDShow.h"

#define WIN32_LEAN_AND_MEAN
#define NO_DSHOW_STRSAFE

#include <dshow.h>

CaptureInfoDShow::CaptureInfoDShow()
{
    CoInitialize(0);
}

CaptureInfoDShow::~CaptureInfoDShow()
{
    CoUninitialize();
}

void CaptureInfoDShow::getVideoDevices(QVector<CaptureInfo::Info> *ret) const
{
    this->getDevices(true, ret);
}

void CaptureInfoDShow::getAudioDevices(QVector<CaptureInfo::Info> *ret) const
{
    this->getDevices(false, ret);
}

void CaptureInfoDShow::getDevices(bool isVideo, QVector<CaptureInfo::Info> *ret) const
{
    HRESULT r;
    ICreateDevEnum *devEnum = NULL;

    r = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_ICreateDevEnum, (LPVOID*)&devEnum);

    if (r != S_OK)
        return;

    IEnumMoniker *classEnum = NULL;
    IMoniker *m = NULL;
    const GUID &guid = isVideo ? CLSID_VideoInputDeviceCategory : CLSID_AudioInputDeviceCategory;

    r = devEnum->CreateClassEnumerator(guid, (IEnumMoniker**)&classEnum, 0);

    if (r != S_OK)
        return;

    while (classEnum->Next(1, &m, NULL) == S_OK)
    {
        CaptureInfo::Info info;
        IPropertyBag *bag = NULL;
        VARIANT var;
        IBindCtx *bindCtx = NULL;
        LPOLESTR oleStr = NULL;
        LPMALLOC coMalloc = NULL;

        r = CoGetMalloc(1, &coMalloc);

        if (r != S_OK)
            goto fail;

        r = CreateBindCtx(0, &bindCtx);

        if (r != S_OK)
            goto fail;

        r = m->GetDisplayName(bindCtx, NULL, &oleStr);

        if (r != S_OK)
            goto fail;

        info.name = QString::fromWCharArray(oleStr);
        info.name = info.name.replace(":", "_");

        r = m->BindToStorage(0, 0, IID_IPropertyBag, (void**)&bag);

        if (r != S_OK)
            goto fail;

        var.vt = VT_BSTR;
        r = bag->Read(L"FriendlyName", &var, NULL);

        if (r != S_OK)
            goto fail;

        info.desc = QString::fromWCharArray(var.bstrVal);

fail:
        VariantClear(&var);

        if (oleStr && coMalloc)
            coMalloc->Free(oleStr);

        if (bindCtx)
            bindCtx->Release();

        if (bag)
            bag->Release();

        m->Release();

        if (!info.desc.isEmpty() || !info.name.isEmpty())
            ret->append(info);
    }

    classEnum->Release();
}

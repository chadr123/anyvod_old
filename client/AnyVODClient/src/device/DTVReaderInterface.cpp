﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "DTVReaderInterface.h"
#include "../../../common/network.h"

DTVReaderInterface::DTVReaderInterface() :
    m_adapter(-1),
    m_type(ST_NONE)
{

}

DTVReaderInterface::~DTVReaderInterface()
{

}

void DTVReaderInterface::setAdapter(long adapter, SystemType type)
{
    this->m_adapter = adapter;
    this->m_type = type;
}

void DTVReaderInterface::getAdapter(long *adapter, DTVReaderInterface::SystemType *type) const
{
    *adapter = this->m_adapter;
    *type = this->m_type;
}

void DTVReaderInterface::reset()
{
    this->m_adapter = -1;
    this->m_type = ST_NONE;
}

QString DTVReaderInterface::decodeMultipleStringStructure(uint8_t *str, int len) const
{
    /* http://atsc.org/wp-content/uploads/2015/03/Program-System-Information-Protocol-for-Terrestrial-Broadcast-and-Cable.pdf */
    /* Multiple string sturcture */
    (void)len;
    QString s;
    int strCount = *str++;

    for (int i = 0; i < strCount; i++)
    {
        QString sub;
        int langCode;
        int segCount;

        langCode = str[0] | (str[1] << 8) | (str[2] << 16);
        (void)langCode;
        str += 3;

        segCount = *str++;

        for (int j = 0; j < segCount; j++)
        {
            int compType = *str++;
            int mode = *str++;
            int count = *str++;
            char buf[count + 1];

            memset(buf, 0, sizeof(buf));

            switch (compType)
            {
                case 0x00:
                {
                    memcpy(buf, str, count);
                    break;
                }
                default:
                {
                    continue;
                }
            }

            switch (mode)
            {
                case 0x3F:
                {
                    int bufSize = count + 1;
                    uint16_t tmp[bufSize];

                    memset(tmp, 0, sizeof(tmp));
                    memcpy(tmp, buf, bufSize);

                    for (int l = 0; l < bufSize - 1; l++)
                        tmp[l] = ntohs(tmp[l]);

                    sub = QString::fromUtf16(tmp).trimmed();

                    break;
                }
                case 0x40:
                case 0x41:
                case 0x48:
                {
                    sub = QString::fromLocal8Bit(buf).trimmed();
                    break;
                }
                default:
                {
                    continue;
                }
            }
        }

        s += sub;
    }

    return s;
}

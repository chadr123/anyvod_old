#-------------------------------------------------
#
# Project created by QtCreator 2011-03-21T11:00:54
#
#-------------------------------------------------

QT += core gui widgets opengl network xml qml

win32: QT += winextras

TARGET = AnyVODClient
TEMPLATE = app

CODECFORTR = UTF-8

cache()

SOURCES += \
    ../../common/SubtitleFileNameGenerator.cpp \
    ../../common/RetreiveSubtitle.cpp \
    ../../common/MemBuffer.cpp \
    src/core/Utils.cpp \
    src/core/Settings.cpp \
    src/core/main.cpp \
    src/core/Common.cpp \
    src/core/AnyVOD.cpp \
    src/core/TextEncodingDetector.cpp \
    src/decoders/HWDecoderInterface.cpp \
    src/decoders/HWDecoder.cpp \
    src/media/VideoThread.cpp \
    src/media/SubtitleThread.cpp \
    src/media/ReadThread.cpp \
    src/media/PacketQueue.cpp \
    src/media/MediaPresenter.cpp \
    src/media/MediaPlayer.cpp \
    src/media/LastPlay.cpp \
    src/media/FrameExtractor.cpp \
    src/media/RefreshThread.cpp \
    src/net/VirtualFile.cpp \
    src/net/SyncHttp.cpp \
    src/net/SubtitleImpl.cpp \
    src/net/Socket.cpp \
    src/net/NetFile.cpp \
    src/net/HttpDownloader.cpp \
    src/net/YouTubeURLPicker.cpp \
    src/parsers/playlist/WPLParser.cpp \
    src/parsers/playlist/PLSParser.cpp \
    src/parsers/playlist/PlayListParserInterface.cpp \
    src/parsers/playlist/PlayListParserGenerator.cpp \
    src/parsers/playlist/M3UParser.cpp \
    src/parsers/playlist/CueParser.cpp \
    src/parsers/playlist/B4SParser.cpp \
    src/parsers/playlist/ASXParser.cpp \
    src/parsers/subtitle/SRTParser.cpp \
    src/parsers/subtitle/SAMIParser.cpp \
    src/parsers/subtitle/LRCParser.cpp \
    src/parsers/subtitle/AVParser.cpp \
    src/parsers/subtitle/ASSParser.cpp \
    src/parsers/subtitle/YouTubeParser.cpp \
    src/ui/UserAspectRatio.cpp \
    src/ui/Trackbar.cpp \
    src/ui/TimeEdit.cpp \
    src/ui/Spectrum.cpp \
    src/ui/Slider.cpp \
    src/ui/SkipRange.cpp \
    src/ui/ShortcutEdit.cpp \
    src/ui/Shader.cpp \
    src/ui/Screen.cpp \
    src/ui/RemoteFileList.cpp \
    src/ui/Popup.cpp \
    src/ui/PlayListUpdater.cpp \
    src/ui/PlayList.cpp \
    src/ui/OpenExternal.cpp \
    src/ui/MultipleCapture.cpp \
    src/ui/MainWindow.cpp \
    src/ui/Login.cpp \
    src/ui/FileAssociation.cpp \
    src/ui/Equalizer.cpp \
    src/ui/CustomShortcut.cpp \
    src/ui/TextEncodeSetting.cpp \
    src/ui/ScreenExplorer.cpp \
    src/ui/ScreenExplorerItem.cpp \
    src/ui/ScreenExplorerUpdater.cpp \
    src/ui/ServerSetting.cpp \
    src/ui/SubtitleDirectory.cpp \
    src/ui/OpenDevice.cpp \
    src/ui/OpenDTVScanChannel.cpp \
    src/ui/ViewEPG.cpp \
    src/ui/EPGItem.cpp \
    src/ui/ElidedLabel.cpp \
    src/video/USWCMemCopy.cpp \
    src/video/Font.cpp \
    src/video/Deinterlacer.cpp \
    src/video/ShaderCompositer.cpp \
    src/video/FilterGraph.cpp \
    src/audio/SPDIF.cpp \
    src/audio/SPDIFEncoder.cpp \
    src/audio/SPDIFInterface.cpp \
    src/device/CaptureInfo.cpp \
    src/device/CaptureInfoInterface.cpp \
    src/device/DTVReader.cpp \
    src/device/DTVReaderInterface.cpp \
    src/device/DTVChannelMap.cpp

win32: SOURCES += \
    src/decoders/DXVA2Decoder.cpp \
    src/audio/SPDIFDSound.cpp \
    src/device/CaptureInfoDShow.cpp \
    src/device/DTVDShow.cpp

mac: SOURCES += \
    src/decoders/VideoToolBoxDecoder.cpp \
    src/audio/SPDIFCoreAudio.cpp

OBJECTIVE_SOURCES += \
    src/device/CaptureInfoAVFoundation.mm

linux-*: SOURCES += \
    src/decoders/VAAPIDecoder.cpp \
    src/audio/SPDIFAlsa.cpp \
    src/device/CaptureInfoV4L2.cpp \
    src/device/DTVLinuxDVB.cpp

HEADERS  += \
    ../../common/SubtitleFileNameGenerator.h \
    ../../common/types.h \
    ../../common/RetreiveSubtitle.h \
    ../../common/packets.h \
    ../../common/packet_types.h \
    ../../common/size.h \
    ../../common/network.h \
    ../../common/size_types.h \
    ../../common/MemBuffer.h \
    src/core/Utils.h \
    src/core/Settings.h \
    src/core/Common.h \
    src/core/BuildNumber.h \
    src/core/AnyVOD.h \
    src/core/TextEncodingDetector.h \
    src/decoders/HWDecoderInterface.h \
    src/decoders/HWDecoder.h \
    src/media/VideoThread.h \
    src/media/SyncType.h \
    src/media/SubtitleThread.h \
    src/media/ReadThread.h \
    src/media/PacketQueue.h \
    src/media/MediaState.h \
    src/media/MediaPresenter.h \
    src/media/MediaPlayer.h \
    src/media/LastPlay.h \
    src/media/FrameExtractor.h \
    src/media/RefreshThread.h \
    src/net/VirtualFile.h \
    src/net/SyncHttp.h \
    src/net/SubtitleImpl.h \
    src/net/Socket.h \
    src/net/NetFile.h \
    src/net/HttpDownloader.h \
    src/net/YouTubeURLPicker.h \
    src/parsers/playlist/WPLParser.h \
    src/parsers/playlist/PLSParser.h \
    src/parsers/playlist/PlayListParserInterface.h \
    src/parsers/playlist/PlayListParserGenerator.h \
    src/parsers/playlist/M3UParser.h \
    src/parsers/playlist/CueParser.h \
    src/parsers/playlist/B4SParser.h \
    src/parsers/playlist/ASXParser.h \
    src/parsers/subtitle/SRTParser.h \
    src/parsers/subtitle/SAMIParser.h \
    src/parsers/subtitle/LRCParser.h \
    src/parsers/subtitle/AVParser.h \
    src/parsers/subtitle/ASSParser.h \
    src/parsers/subtitle/YouTubeParser.h \
    src/ui/UserAspectRatio.h \
    src/ui/Trackbar.h \
    src/ui/TimeEdit.h \
    src/ui/Spectrum.h \
    src/ui/Slider.h \
    src/ui/SkipRange.h \
    src/ui/ShortcutEdit.h \
    src/ui/Shader.h \
    src/ui/Screen.h \
    src/ui/RemoteFileList.h \
    src/ui/Popup.h \
    src/ui/PlayListUpdater.h \
    src/ui/PlayList.h \
    src/ui/OpenExternal.h \
    src/ui/MultipleCapture.h \
    src/ui/MainWindow.h \
    src/ui/Login.h \
    src/ui/FileAssociation.h \
    src/ui/Equalizer.h \
    src/ui/CustomShortcut.h \
    src/ui/TextEncodeSetting.h \
    src/ui/ScreenExplorer.h \
    src/ui/ScreenExplorerItem.h \
    src/ui/ScreenExplorerUpdater.h \
    src/ui/ServerSetting.h \
    src/ui/SubtitleDirectory.h \
    src/ui/OpenDevice.h \
    src/ui/OpenDTVScanChannel.h \
    src/ui/ViewEPG.h \
    src/ui/EPGItem.h \
    src/ui/ElidedLabel.h \
    src/video/USWCMemCopy.h \
    src/video/Font.h \
    src/video/Deinterlacer.h \
    src/video/ShaderCompositer.h \
    src/video/FilterGraph.h \
    src/audio/SPDIF.h \
    src/audio/SPDIFEncoder.h \
    src/audio/SPDIFInterface.h \
    src/device/CaptureInfo.h \
    src/device/CaptureInfoInterface.h \
    src/device/DTVReader.h \
    src/device/DTVReaderInterface.h \
    src/device/DTVChannelMap.h

win32: HEADERS += \
    src/decoders/DXVA2Decoder.h \
    src/audio/SPDIFDSound.h \
    src/device/CaptureInfoDShow.h \
    src/device/DTVDShow.h \
    src/device/DTVDShowDefs.h

mac: HEADERS += \
    src/decoders/VideoToolBoxDecoder.h \
    src/audio/SPDIFCoreAudio.h \
    src/device/CaptureInfoAVFoundation.h

linux-*: HEADERS += \
    src/decoders/VAAPIDecoder.h \
    src/audio/SPDIFAlsa.h \
    src/device/CaptureInfoV4L2.h \
    src/device/DTVLinuxDVB.h

FORMS    += \
    forms/useraspectratio.ui \
    forms/skiprange.ui \
    forms/shader.ui \
    forms/remotefilelist.ui \
    forms/popup.ui \
    forms/playlist.ui \
    forms/openexternal.ui \
    forms/multiplecapture.ui \
    forms/mainwindow.ui \
    forms/login.ui \
    forms/fileassociation.ui \
    forms/equalizer.ui \
    forms/customshortcut.ui \
    forms/textencodesetting.ui \
    forms/serversetting.ui \
    forms/screenexplorer.ui \
    forms/screenexploreritem.ui \
    forms/subtitledirectory.ui \
    forms/opendevice.ui \
    forms/opendtvscanchannel.ui \
    forms/viewepg.ui \
    forms/epgitem.ui

TRANSLATIONS    += \
    languages/anyvod_ko.ts \
    languages/anyvod_en.ts

DEFINES += __STDC_CONSTANT_MACROS

win32: QMAKE_POST_LINK += ..\\AnyVODClient\\scripts\\win\\postlink.bat
linux-*: QMAKE_POST_LINK += ../AnyVODClient/scripts/linux/postlink.sh
mac: QMAKE_POST_LINK += ../AnyVODClient/scripts/mac/postlink.sh

QMAKE_LFLAGS += -L$$PWD

QMAKE_CXXFLAGS += -fdiagnostics-show-option

win32: QMAKE_CXXFLAGS += -fopenmp -m32
linux-*: QMAKE_CXXFLAGS += -fopenmp

win32: QMAKE_CFLAGS += -fopenmp -m32
linux-*: QMAKE_CFLAGS += -fopenmp

win32: LIBS += -L$$PWD\\libs\\win
linux-*: LIBS += -L$$PWD/libs/linux
mac: LIBS += -L$$PWD/libs/mac

LIBS += -lmediainfo -lbass -lbass_fx -lbassmix -lavcodec -lavformat -lavutil -lavdevice \
        -lavfilter -lswscale -lswresample -lass

win32: LIBS += -lws2_32 -lImm32 -lShell32 -lAdvapi32 -lOle32 -lzdll -lgomp -ldsound -lStrmiids -luuid \
               -lOleAut32 -lopengl32
linux-*: LIBS += -ldl -lX11 -lva -lva-x11 -lz -lgomp -lasound -ldvbpsi -ldbus-1
mac: LIBS += -framework CoreFoundation -framework ApplicationServices -framework IOKit -framework CoreVideo \
             -framework CoreAudio -framework Foundation -framework AVFoundation -lz

linux-*: INCLUDEPATH += /usr/include/dbus-1.0 /usr/lib/x86_64-linux-gnu/dbus-1.0/include
linux-*: DEPENDPATH += /usr/include/dbus-1.0 /usr/lib/x86_64-linux-gnu/dbus-1.0/include

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

INCLUDEPATH += $$PWD/include/ffmpeg
DEPENDPATH += $$PWD/include/ffmpeg

INCLUDEPATH += $$PWD/src
DEPENDPATH += $$PWD/src

RESOURCES += \
    forms/resource.qrc

OTHER_FILES += \
    settings.ini \
    skins/default.skin \
    shaders/sample.glsl \
    settings_template.ini \
    equalizer_template.ini \
    languages/anyvod_en.ts \
    languages/anyvod_ko.ts

mac: ICON = ../../images/icons/app.icns

mac {
    PRIVATE_LIBS.files += \
        libs/mac/libass.5.dylib \
        libs/mac/libbass.dylib \
        libs/mac/libbass_fx.dylib \
        libs/mac/libbassmix.dylib \
        libs/mac/libmediainfo.0.dylib \
        libs/mac/libavcodec.57.dylib \
        libs/mac/libavformat.57.dylib \
        libs/mac/libavutil.55.dylib \
        libs/mac/libswresample.2.dylib \
        libs/mac/libswscale.4.dylib \
        libs/mac/libavdevice.57.dylib \
        libs/mac/libavfilter.6.dylib
    PRIVATE_LIBS.path = Contents/MacOS

    QMAKE_BUNDLE_DATA += PRIVATE_LIBS
    QMAKE_MAC_SDK = macosx10.11
}

mac: QMAKE_INFO_PLIST = resources/mac.plist
win32: RC_FILE = resources/win.rc

win32: QMAKE_RC += -F pe-i386

#! /bin/sh

version=`cat ../AnyVODClient/scripts/linux/version`
qt_path=`../AnyVODClient/scripts/linux/qt_path.sh`
export PATH=$HOME/$qt_path/$version/gcc_64/bin:$PATH

if [ ! -f ../AnyVODClient/equalizer.ini ]; then
  cp ../AnyVODClient/equalizer_template.ini ../AnyVODClient/equalizer.ini
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

if [ ! -f ../AnyVODClient/settings.ini ]; then
  cp ../AnyVODClient/settings_template.ini ../AnyVODClient/settings.ini
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

lrelease ../AnyVODClient/AnyVODClient.pro
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

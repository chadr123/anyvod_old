#! /bin/sh

version=`cat version`
qt_path=`./qt_path.sh`
export PATH=$HOME/$qt_path/$version/gcc_64/bin:$PATH

cd ../../../
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -e AnyVODClient-build-desktop ]; then
  rm -rf AnyVODClient-build-desktop
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir AnyVODClient-build-desktop
if [ $? -ne 0 ]; then
  exit $?
fi

cd AnyVODClient-build-desktop
if [ $? -ne 0 ]; then
  exit $?
fi

make distclean -w

qmake ../AnyVODClient/AnyVODClient.pro -r -spec linux-g++ CONFIG+=release
if [ $? -ne 0 ]; then
  exit $?
fi

make -w -j8
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../AnyVODClient/scripts/linux
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

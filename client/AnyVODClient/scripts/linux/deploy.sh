#! /bin/sh

version=`cat version`
qt_path=`./qt_path.sh`

if [ ! -d ../../../../package ]; then
  mkdir ../../../../package
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

if [ -d ../../../../package/client ]; then
  rm -rf ../../../../package/client
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

if [ -d ../../../../package/licenses ]; then
  rm -rf ../../../../package/licenses
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/imageformats
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/platforms
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/licenses
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../../../licenses/* ../../../../package/licenses
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ../../libs/linux/*.so ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ../../libs/linux/*.so.* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../equalizer_template.ini ../../../../package/client/equalizer.ini
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../settings_template.ini ../../../../package/client/settings.ini
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/fonts
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../fonts/* ../../../../package/client/fonts
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/skins
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../skins/* ../../../../package/client/skins
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/shaders
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../shaders/* ../../../../package/client/shaders
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a AnyVODClient.sh ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cd -
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ../../../AnyVODClient-build-desktop/AnyVODClient ../../../../package/client/AnyVODClient_bin
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp $HOME/$qt_path/$version/gcc_64/translations/qt_zh_??.qm ../../../../package/client/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp $HOME/$qt_path/$version/gcc_64/translations/qt_??.qm ../../../../package/client/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp $HOME/$qt_path/$version/gcc_64/translations/qtbase_??.qm ../../../../package/client/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../languages/*.qm ../../../../package/client/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?OpenGL.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Widgets.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Xml.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Network.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Gui.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Core.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?DBus.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?Qml.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libQt?XcbQpa.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libicui18n.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libicuuc.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libicudata.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/plugins/imageformats/libqjpeg.so ../../../../package/client/imageformats
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/plugins/imageformats/libqtiff.so ../../../../package/client/imageformats
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/plugins/platforms ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libicudata.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $HOME/$qt_path/$version/gcc_64/lib/libicudata.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

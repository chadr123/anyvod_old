call version.bat
call qt_path.bat

if exist "..\..\..\..\package\client" (
del ..\..\..\..\package\client\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\fonts" (
del ..\..\..\..\package\client\fonts\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\languages" (
del ..\..\..\..\package\client\languages\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\skins" (
del ..\..\..\..\package\client\skins\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\shaders" (
del ..\..\..\..\package\client\shaders\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\imageformats" (
del ..\..\..\..\package\client\imageformats\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\client\platforms" (
del ..\..\..\..\package\client\platforms\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if exist "..\..\..\..\package\licenses" (
del ..\..\..\..\package\licenses\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end
)

if not exist "..\..\..\..\package\client\imageformats" (
mkdir ..\..\..\..\package\client\imageformats
if %errorlevel% neq 0 goto end
)

if not exist "..\..\..\..\package\client\platforms" (
mkdir ..\..\..\..\package\client\platforms
if %errorlevel% neq 0 goto end
)

xcopy "..\..\libs\win\*.dll" "..\..\..\..\package\client" /y /i
if %errorlevel% neq 0 goto end

copy "..\..\equalizer_template.ini" "..\..\..\..\package\client\equalizer.ini"
if %errorlevel% neq 0 goto end

copy "..\..\settings_template.ini" "..\..\..\..\package\client\settings.ini"
if %errorlevel% neq 0 goto end

xcopy "..\..\fonts" "..\..\..\..\package\client\fonts" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "..\..\skins" "..\..\..\..\package\client\skins" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "..\..\shaders" "..\..\..\..\package\client\shaders" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "..\..\..\..\licenses" "..\..\..\..\package\licenses" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "..\..\..\AnyVODClient-build-desktop\release\AnyVODClient.exe" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\Tools\%mingw_tool_path%\bin\libgcc_s_dw2-1.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\Tools\%mingw_tool_path%\bin\libwinpthread-1.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\Tools\%mingw_tool_path%\bin\libstdc++-6.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\Tools\%mingw_tool_path%\bin\libgomp-1.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\Tools\%mingw_tool_path%\opt\bin\libeay32.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\Tools\%mingw_tool_path%\opt\bin\ssleay32.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Core.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Gui.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Widgets.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?OpenGL.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Network.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Xml.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?Qml.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\bin\Qt?WinExtras.dll" "..\..\..\..\package\client" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\plugins\imageformats\qjpeg.dll" "..\..\..\..\package\client\imageformats" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\plugins\imageformats\qtiff.dll" "..\..\..\..\package\client\imageformats" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\plugins\platforms\qwindows.dll" "..\..\..\..\package\client\platforms" /y
if %errorlevel% neq 0 goto end

xcopy "%qt_path%\%version%\%mingw_path%\translations\qt_zh_??.qm" "..\..\..\..\package\client\languages" /y /e /i
xcopy "%qt_path%\%version%\%mingw_path%\translations\qt_??.qm" "..\..\..\..\package\client\languages" /y /e /i
xcopy "%qt_path%\%version%\%mingw_path%\translations\qtbase_??.qm" "..\..\..\..\package\client\languages" /y /e /i
if %errorlevel% neq 0 goto end

xcopy "..\..\languages\*.qm" "..\..\..\..\package\client\languages" /y /e /i
if %errorlevel% neq 0 goto end

:end

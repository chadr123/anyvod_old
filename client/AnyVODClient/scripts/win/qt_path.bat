set qt_path_online_installer=C:\Qt
set qt_path_offline_installer=C:\Qt\Qt5.7.0

set mingw_path=mingw%mingw_version%_32
set mingw_tool_path=mingw%mingw_tool_version%_32

set qt_path=%qt_path_online_installer%

if exist %qt_path_online_installer% (
set qt_path=%qt_path_online_installer%
)

if exist %qt_path_offline_installer% (
set qt_path=%qt_path_offline_installer%
)


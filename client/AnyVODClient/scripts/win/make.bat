call version.bat
call qt_path.bat

set pwd=%cd%
call %qt_path%\%version%\%mingw_path%\bin\qtenv2.bat
cd /d %pwd%

cd ..\..\..\
if %errorlevel% neq 0 goto end

if not exist AnyVODClient-build-desktop (
mkdir AnyVODClient-build-desktop
if %errorlevel% neq 0 goto end
)

del AnyVODClient-build-desktop\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end

cd AnyVODClient-build-desktop
if %errorlevel% neq 0 goto end

mingw32-make.exe distclean -w

qmake.exe ..\AnyVODClient\AnyVODClient.pro -r -spec win32-g++ CONFIG+=release
if %errorlevel% neq 0 goto end

..\..\utils\jom.exe release
if %errorlevel% neq 0 goto end

cd ..\AnyVODClient\scripts\win
if %errorlevel% neq 0 goto end

:end

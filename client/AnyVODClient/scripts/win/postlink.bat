if not exist ..\AnyVODClient\equalizer.ini (
copy ..\AnyVODClient\equalizer_template.ini ..\AnyVODClient\equalizer.ini
if %errorlevel% neq 0 goto end
)

if not exist ..\AnyVODClient\settings.ini (
copy ..\AnyVODClient\settings_template.ini ..\AnyVODClient\settings.ini
if %errorlevel% neq 0 goto end
)

lrelease ..\AnyVODClient\AnyVODClient.pro
if %errorlevel% neq 0 goto end

:end

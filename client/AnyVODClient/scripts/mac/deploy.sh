#! /bin/sh

version=`cat ../linux/version`
qt_path=`../linux/qt_path.sh`
DEPLOY_PATH="../../../AnyVODClient-build-desktop/AnyVODClient.app/Contents/MacOS"

if [ ! -d ../../../../package ]; then
  mkdir ../../../../package
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

if [ -d ${DEPLOY_PATH}/licenses ]; then
  rm -rf ${DEPLOY_PATH}/licenses
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir ${DEPLOY_PATH}/licenses
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../../../licenses/* ${DEPLOY_PATH}/licenses
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -d ${DEPLOY_PATH}/notes ]; then
  rm -rf ${DEPLOY_PATH}/notes
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir ${DEPLOY_PATH}/notes
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../../../notes/* ${DEPLOY_PATH}/notes
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ../../libs/mac/*.dylib ${DEPLOY_PATH}
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../equalizer_template.ini ${DEPLOY_PATH}/equalizer.ini
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../settings_template.ini ${DEPLOY_PATH}/settings.ini
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -d ${DEPLOY_PATH}/fonts ]; then
  rm -rf ${DEPLOY_PATH}/fonts
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir ${DEPLOY_PATH}/fonts
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../fonts/* ${DEPLOY_PATH}/fonts
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -d ${DEPLOY_PATH}/skins ]; then
  rm -rf ${DEPLOY_PATH}/skins
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir ${DEPLOY_PATH}/skins
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../skins/* ${DEPLOY_PATH}/skins
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -d ${DEPLOY_PATH}/shaders ]; then
  rm -rf ${DEPLOY_PATH}/shaders
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir ${DEPLOY_PATH}/shaders
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../shaders/* ${DEPLOY_PATH}/shaders
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -d ${DEPLOY_PATH}/languages ]; then
  rm -rf ${DEPLOY_PATH}/languages
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir ${DEPLOY_PATH}/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp $HOME/$qt_path/$version/clang_64/translations/qt_zh_??.qm ${DEPLOY_PATH}/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp $HOME/$qt_path/$version/clang_64/translations/qt_??.qm ${DEPLOY_PATH}/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp $HOME/$qt_path/$version/clang_64/translations/qtbase_??.qm ${DEPLOY_PATH}/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../languages/*.qm ${DEPLOY_PATH}/languages
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -d ../../../../package/client ]; then
  rm -rf ../../../../package/client
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ${DEPLOY_PATH}/../../../AnyVODClient.app ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

rm -rf ../../../AnyVODClient-build-desktop
if [ $? -ne 0 ]; then
  exit $?
fi

rm -rf ../../../build-AnyVODClient-Desktop
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

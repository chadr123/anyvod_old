#! /bin/sh

version=`cat ../linux/version`
qt_path=`../linux/qt_path.sh`
export PATH=$HOME/$qt_path/$version/clang_64/bin:$PATH

cd ../../../
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -e AnyVODClient-build-desktop ]; then
  rm -rf AnyVODClient-build-desktop
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir AnyVODClient-build-desktop
if [ $? -ne 0 ]; then
  exit $?
fi

cd AnyVODClient-build-desktop
if [ $? -ne 0 ]; then
  exit $?
fi

make distclean -w

qmake ../AnyVODClient/AnyVODClient.pro -r -spec macx-clang CONFIG+=release CONFIG+=x86_64
if [ $? -ne 0 ]; then
  exit $?
fi

make -w -j8
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../AnyVODClient/scripts/mac
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

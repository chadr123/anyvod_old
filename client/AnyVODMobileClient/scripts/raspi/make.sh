#! /bin/sh

export PATH=/usr/local/qt5/bin:$PATH

./premake.sh
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../../../
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -e AnyVODMobileClient-build-raspi ]; then
  rm -rf AnyVODMobileClient-build-raspi
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir AnyVODMobileClient-build-raspi
if [ $? -ne 0 ]; then
  exit $?
fi

cd AnyVODMobileClient-build-raspi
if [ $? -ne 0 ]; then
  exit $?
fi

make distclean -w

qmake ../AnyVODMobileClient/AnyVODMobileClient.pro -r -spec devices/linux-rasp-pi2-g++
if [ $? -ne 0 ]; then
  exit $?
fi

make -w -j2
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../AnyVODMobileClient/scripts/android
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

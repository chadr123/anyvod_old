#! /bin/sh

export PATH=/usr/local/qt5/bin:$PATH

lrelease ../AnyVODMobileClient/AnyVODMobileClient.pro
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

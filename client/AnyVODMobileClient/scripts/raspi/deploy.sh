#! /bin/sh

qt_path=/usr/local/qt5

if [ ! -d ../../../../package ]; then
  mkdir ../../../../package
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

if [ -d ../../../../package/client ]; then
  rm -rf ../../../../package/client
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

if [ -d ../../../../package/client/licenses ]; then
  rm -rf ../../../../package/client/licenses
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/imageformats
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/platforms
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/licenses
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/qt_qml
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../../../licenses/noto.txt ../../../../licenses/bass.txt ../../../../licenses/ffmpeg.txt \
   ../../../../licenses/libass.txt ../../../../licenses/qt.txt ../../../../licenses/anyvod.txt ../../../../package/client/licenses
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ../../libs/raspi/*.so ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ../../libs/raspi/*.so.* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/fonts
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../fonts/* ../../../../package/client/fonts
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a AnyVODMobileClient.sh ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a qt.conf ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cd -
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ../../../AnyVODMobileClient-build-raspi/AnyVODMobileClient ../../../../package/client/AnyVODMobileClient_bin
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ../../../AnyVODMobileClient-build-raspi/qml ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir ../../../../package/client/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../../languages/*.qm ../../../../package/client/languages
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?Widgets.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?Xml.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?Network.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?Gui.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?Core.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?DBus.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?EglDeviceIntegration.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?Qml.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?Quick.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?Svg.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/lib/libQt?Sensors.so* ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/qml/* ../../../../package/client/qt_qml
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/plugins/imageformats/*.so ../../../../package/client/imageformats
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/plugins/platforms ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/plugins/platforminputcontexts ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a $qt_path/plugins/egldeviceintegrations ../../../../package/client
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

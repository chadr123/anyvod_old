#! /bin/sh

./make.sh

if [ $? -ne 0 ]; then
  exit $?
fi

./deploy.sh

if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

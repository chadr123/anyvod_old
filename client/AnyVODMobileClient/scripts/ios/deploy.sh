#! /bin/sh

version=`cat ../../../AnyVODClient/scripts/linux/version`
qt_path=`../../../AnyVODClient/scripts/linux/qt_path.sh`
developer=$1
app_name="AnyVODMobileClient"
release_path="Release-iphoneos"

export PATH=$HOME/$qt_path/$version/ios/bin:$PATH
export CODE_SIGN_RESOURCE_RULES_PATH=`xcrun --show-sdk-path -sdk iphoneos`/ResourceRules.plist

if [ ! -d ../../../../package ]; then
  mkdir ../../../../package
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

cd ../../../AnyVODMobileClient-build-ios
cp $CODE_SIGN_RESOURCE_RULES_PATH ${release_path}/${app_name}.app
if [ $? -ne 0 ]; then
  exit $?
fi

pwd=`pwd`

xcrun -sdk iphoneos PackageApplication -v "${release_path}/${app_name}.app" -o "${pwd}/${release_path}/${app_name}.ipa" --sign "${developer}" --embed "${release_path}/${app_name}.app/embedded.mobileprovision"
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

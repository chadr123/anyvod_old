#! /bin/sh

version=`cat ../AnyVODClient/scripts/linux/version`
qt_path=`../AnyVODClient/scripts/linux/qt_path.sh`
export PATH=$HOME/$qt_path/$version/ios/bin:$PATH

lrelease ../AnyVODMobileClient/AnyVODMobileClient.pro
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

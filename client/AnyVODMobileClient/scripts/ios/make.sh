#! /bin/sh

version=`cat ../../../AnyVODClient/scripts/linux/version`
qt_path=`../../../AnyVODClient/scripts/linux/qt_path.sh`
export PATH=$HOME/$qt_path/$version/ios/bin:$PATH

./premake.sh
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../../../
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -e AnyVODMobileClient-build-ios ]; then
  rm -rf AnyVODMobileClient-build-ios
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir AnyVODMobileClient-build-ios 
if [ $? -ne 0 ]; then
  exit $?
fi

cd AnyVODMobileClient-build-ios
if [ $? -ne 0 ]; then
  exit $?
fi

make distclean -w

qmake ../AnyVODMobileClient/AnyVODMobileClient.pro -r -spec macx-ios-clang CONFIG+=release CONFIG+=iphoneos
if [ $? -ne 0 ]; then
  exit $?
fi

make -w -j8
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../AnyVODMobileClient/scripts/ios
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

#! /bin/sh

version=`cat ../../../AnyVODClient/scripts/linux/version`
qt_path=`../../../AnyVODClient/scripts/linux/qt_path.sh`
arch=$1
export PATH=$HOME/$qt_path/$version/$arch/bin:$PATH

. ./toolchain.sh

lrelease ../../AnyVODMobileClient.pro
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

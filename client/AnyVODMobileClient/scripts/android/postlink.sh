#! /bin/sh

version=`cat ../AnyVODClient/scripts/linux/version`
qt_path=`../AnyVODClient/scripts/linux/qt_path.sh`
arch=$1
export PATH=$HOME/$qt_path/$version/$arch/bin:$PATH

. ../AnyVODMobileClient/scripts/android/toolchain.sh

lrelease ../AnyVODMobileClient/AnyVODMobileClient.pro
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

#! /bin/sh

version=`cat ../../../AnyVODClient/scripts/linux/version`
qt_path=`../../../AnyVODClient/scripts/linux/qt_path.sh`
arch=$1
export PATH=$HOME/$qt_path/$version/$arch/bin:$PATH

./premake.sh $1
if [ $? -ne 0 ]; then
  exit $?
fi

. ./toolchain.sh

cd ../../../
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -e AnyVODMobileClient-build-android ]; then
  rm -rf AnyVODMobileClient-build-android
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir AnyVODMobileClient-build-android 
if [ $? -ne 0 ]; then
  exit $?
fi

cd AnyVODMobileClient-build-android
if [ $? -ne 0 ]; then
  exit $?
fi

make distclean -w

qmake ../AnyVODMobileClient/AnyVODMobileClient.pro -r -spec android-g++
if [ $? -ne 0 ]; then
  exit $?
fi

make -w -j8
if [ $? -ne 0 ]; then
  exit $?
fi

make install INSTALL_ROOT=./android-build
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../AnyVODMobileClient/scripts/android
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

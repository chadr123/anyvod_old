#! /bin/sh

./make.sh $1
if [ $? -ne 0 ]; then
  exit $?
fi

./deploy.sh $1 $2 $3
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

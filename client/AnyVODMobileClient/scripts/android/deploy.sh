#! /bin/sh

version=`cat ../../../AnyVODClient/scripts/linux/version`
qt_path=`../../../AnyVODClient/scripts/linux/qt_path.sh`
arch=$1
password=$2
export PATH=$HOME/$qt_path/$version/$arch/bin:$PATH

. ./toolchain.sh

if [ ! -d ../../../../package ]; then
  mkdir ../../../../package
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

./replaceabi.sh replace $3
if [ $? -ne 0 ]; then
  exit $?
fi

androiddeployqt --input ../../../AnyVODMobileClient-build-android/android-libAnyVODMobileClient.so-deployment-settings.json --output ../../../AnyVODMobileClient-build-android/android-build --deployment bundled --android-platform android-23 --jdk $JDK --gradle --sign $HOME/android_release.keystore anyvod --storepass $password --keypass $password
if [ $? -ne 0 ]; then
  ./replaceabi.sh unreplace
  exit $?
fi

./replaceabi.sh unreplace
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

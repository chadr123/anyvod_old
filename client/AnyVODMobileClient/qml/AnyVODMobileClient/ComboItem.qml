﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtGraphicalEffects 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property int defaultValue: -1
    property alias listWidth: root.width
    property alias listHeight: root.height
    property alias title: title.text
    property alias model: list.model

    signal goBack
    signal itemSelected(int index)

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: goBack()
        onCanceled: goBack()
    }

    Rectangle {
        id: root
        anchors.centerIn: parent
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            Column {
                anchors.fill: parent

                Rectangle {
                    width: parent.width
                    height: 50

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "#1f88f9" }
                        GradientStop { position: 0.93; color: "#1f88f9" }
                        GradientStop { position: 1.0; color: "#1874d0" }
                    }

                    Row {
                        id: navigation
                        width: parent.width
                        height: parent.height

                        Rectangle {
                            id: spacer1
                            color: "transparent"
                            height: parent.height
                            width: 10
                        }

                        Text {
                            id: title
                            width: parent.width - spacer1.width
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            color: "white"
                            elide: Text.ElideMiddle
                            font.pixelSize: parent.height * 0.3
                        }
                    }
                }

                Rectangle {
                    id: listRoot
                    width: parent.width
                    height: parent.height - navigation.height - spacer.height - closeButton.height
                    color: "#f8f8f8"
                    clip: true

                    ListView {
                        id: list
                        width: parent.width
                        height: parent.height

                        property ImageCheckButton checkedItem: null

                        delegate: Rectangle {
                            width: parent.width
                            height: 46

                            Rectangle {
                                id: itemRoot
                                anchors.fill: parent
                                color: "white"

                                MouseArea {
                                    id: mouseArea
                                    anchors.fill: parent

                                    Text {
                                        x: 10
                                        width: parent.width - checkButton.width - checkButton.anchors.rightMargin - 15
                                        height: parent.height
                                        verticalAlignment: Text.AlignVCenter
                                        font.pixelSize: parent.height * 0.4
                                        text: model.desc
                                        elide: Text.ElideMiddle
                                    }

                                    ImageCheckButton {
                                        id: checkButton
                                        anchors.verticalCenter: parent.verticalCenter
                                        anchors.right: parent.right
                                        anchors.rightMargin: scrollbar.width + 20
                                        enabled: false
                                        width: 20
                                        height: width
                                        checked: model.value === defaultValue
                                        checkedSource: "assets/checked.png"
                                        unCheckedSource: "assets/unchecked.png"

                                        Component.onCompleted: {
                                            if (checked)
                                                list.checkedItem = this
                                        }
                                    }

                                    onClicked: {
                                        if (list.checkedItem != null)
                                            list.checkedItem.checked = false

                                        checkButton.checked = true
                                        itemSelected(index)
                                        goBack()
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = "white"
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }

                            Rectangle {
                                anchors.bottom: parent.bottom
                                width: parent.width
                                height: 1
                                color: model.index === list.count - 1 ? "white" : "lightgray"
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                Rectangle {
                    id: spacer
                    width: parent.width
                    height: 1
                    color: "gray"
                }

                Rectangle {
                    id: closeButton
                    width: parent.width
                    height: 50

                    ImageButton {
                        anchors.right: parent.right
                        anchors.rightMargin: 20
                        height: parent.height
                        text.text: qsTr("닫기")
                        text.color: "green"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignRight

                        onClicked: {
                            goBack()
                        }
                    }
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            goBack()
        }
    }
}

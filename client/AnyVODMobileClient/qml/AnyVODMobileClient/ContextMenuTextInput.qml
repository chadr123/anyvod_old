﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtGraphicalEffects 1.0

Rectangle {
    id: root

    property bool inputFocus: true
    property alias text: input.text
    property alias inputMethodHints: input.inputMethodHints
    signal finished

    function finish()
    {
        input.selectAll()
        input.deselect()
    }

    DropShadow {
        anchors.fill: source
        visible: source.visible
        opacity: source.opacity
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: menu
    }

    Rectangle {
        id: menu
        anchors.top: parent.top
        anchors.topMargin: -(height + 5)
        width: menuContent.width
        height: 40
        visible: false
        opacity: 0.0
        z: 10

        function show()
        {
            visible = true
            opacity = 1.0
        }

        function hide()
        {
            visible = false
            opacity = 0.0
        }

        Behavior on opacity {
            NumberAnimation {
                id: menuOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        Row {
            id: menuContent
            height: parent.height
            width: childWidth()
            spacing: 17

            function childWidth()
            {
                var width = firstSpacer.width + spacing

                if (cut.visible)
                    width += cut.width + spacing

                if (copy.visible)
                    width += copy.width + spacing

                if (paste.visible)
                    width += paste.width + spacing

                width -= spacing

                return width
            }

            Rectangle {
                id: firstSpacer
                color: "transparent"
                height: parent.height
                width: 0.1
            }

            ImageButton {
                id: cut
                paddingValue: 15
                height: parent.height
                visible: input.isSelected()
                text.text: qsTr("잘라내기")
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter

                onClicked: {
                    input.cut()
                    menu.hide()
                }
            }

            ImageButton {
                id: copy
                paddingValue: 15
                height: parent.height
                visible: input.isSelected()
                text.text: qsTr("복사하기")
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter

                onClicked: {
                    input.copy()
                    menu.hide()
                }
            }

            ImageButton {
                id: paste
                paddingValue: 15
                height: parent.height
                text.text: qsTr("붙여넣기")
                text.verticalAlignment: Text.AlignVCenter
                text.horizontalAlignment: Text.AlignHCenter

                onClicked: {
                    input.paste()
                    menu.hide()
                }
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        clip: true

        TextInput {
            id: input
            focus: true
            width: parent.width - cursorWidth
            height: parent.height - bottomLine.height * 2
            selectionColor: "lightblue"
            selectedTextColor: color
            cursorDelegate: Rectangle {
                width: parent.cursorWidth
                visible: parent.cursorVisible
                color: "#1f88f9"

                SequentialAnimation on opacity {
                    running: true
                    loops: Animation.Infinite

                    NumberAnimation {
                        to: 0
                        duration: 600
                    }

                    NumberAnimation {
                        to: 1
                        duration: 600
                    }
                }
            }

            property int cursorWidth: 2

            function isSelected()
            {
                return input.selectionStart != input.selectionEnd
            }

            onAccepted: {
                finished()
            }

            Component.onCompleted: {
                if (inputFocus)
                    forceActiveFocus()
            }
        }

        Rectangle {
            id: bottomLine
            anchors.bottom: parent.bottom
            width: parent.width
            height: 2
            color: "#1f88f9"
        }
    }

    MouseArea {
        anchors.fill: parent

        onPressAndHold: {
            input.selectAll()
            menu.show()
        }

        onClicked: {
            input.forceActiveFocus()
            input.cursorPosition = input.positionAt(mouse.x, mouse.y, TextInput.CursorOnCharacter)

            if (input.isSelected())
                input.deselect()

            menu.hide()

            if (!Qt.inputMethod.visible)
                Qt.inputMethod.show()
        }
    }
}

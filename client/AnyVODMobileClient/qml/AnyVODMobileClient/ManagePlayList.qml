﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import com.dcple.anyvod 1.0

Item {
    id: top
    anchors.fill: parent
    focus: true

    readonly property int settingListDefaultHeight: height * 0.8

    property alias title: title.text

    signal selected(string name)
    signal goBack

    function exit()
    {
        goBack()
    }

    function showMenu(parent, name)
    {
        var globalCoord = parent.mapToItem(null, parent.x, parent.y)
        var showX = parent.x - menu.menuWidth + parent.width
        var showY = globalCoord.y - parent.height
        var offset = 10
        var maxHeight = showY + menu.menuHeight + offset

        if (maxHeight >= root.height)
            showY = root.height - (menu.menuHeight + offset)

        menu.name = name
        menu.show(showX, showY)
    }

    function updateNoItem()
    {
        noItems.visible = managePlayListModel.rowCount() === 0
    }

    ManagePlayListModel {
        id: managePlayListModel

        onModelReset: {
            updateNoItem()
        }
    }

    Component {
        id: confirmDelete

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("확인")
            message: qsTr("삭제 하시겠습니까?")
            useCancel: true

            onOk: {
                managePlayListModel.deleteItem(name)
            }
        }
    }

    Menus {
        id: menu
        menuWidth: 80
        menuHeight: 40
        model: ListModel {
            ListElement {
                text: qsTr("삭제")
                menuID: AnyVODEnums.MID_DELETE
            }
        }

        property string name

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_DELETE:
                    messageBoxLoader.name = name
                    messageBoxLoader.sourceComponent = confirmDelete

                    break
            }

            if (messageBoxLoader.sourceComponent != undefined)
                messageBoxLoader.forceActiveFocus()
        }
    }

    Column {
        id: root
        anchors.fill: parent

        Rectangle {
            width: parent.width
            height: 50

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#1f88f9" }
                GradientStop { position: 0.93; color: "#1f88f9" }
                GradientStop { position: 1.0; color: "#1874d0" }
            }

            Row {
                id: navigation
                width: parent.width
                height: parent.height

                Rectangle {
                    id: spacer1
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                ImageButton {
                    id: back
                    width: 30
                    height: parent.height
                    image.width: 20
                    image.height: image.width
                    source: "assets/back.png"
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: {
                        top.exit()
                    }

                }

                Rectangle {
                    id: spacer2
                    color: "transparent"
                    height: parent.height
                    width: 0
                }

                Text {
                    id: title
                    width: parent.width - back.width - spacer1.width - spacer2.width
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    elide: Text.ElideMiddle
                    font.pixelSize: parent.height * 0.3
                }
            }
        }

        Rectangle {
            id: listRoot
            width: parent.width
            height: parent.height - navigation.height
            color: "white"
            clip: true

            Rectangle {
                anchors.fill: parent
                color: "#f8f8f8"

                ListView {
                    id: list
                    anchors.fill: parent
                    model: managePlayListModel

                    delegate: Rectangle {
                        id: itemRoot
                        width: parent.width
                        height: 50

                        MouseArea {
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            height: parent.height

                            Rectangle {
                                anchors.fill: parent
                                anchors.leftMargin: 15
                                anchors.rightMargin: 10
                                color: "transparent"

                                Row {
                                    anchors.fill: parent

                                    Text {
                                        width: parent.width - openItemMenu.width
                                        height: parent.height
                                        text: model.name
                                        verticalAlignment: Text.AlignVCenter
                                    }

                                    ImageButton {
                                        id: openItemMenu
                                        width: 30
                                        height: width
                                        image.width: width
                                        image.height: height
                                        source: "assets/itemMenu.png"
                                        anchors.verticalCenter: parent.verticalCenter

                                        onClicked: {
                                            showMenu(this, model.name)
                                        }
                                    }
                                }
                            }

                            onClicked: {
                                goBack()
                                selected(model.name)
                            }

                            onPressed: {
                                itemRoot.color = "lightgray"
                                itemRoot.opacity = 0.5
                            }

                            onCanceled: {
                                itemRoot.color = listRoot.color
                                itemRoot.opacity = 1.0
                            }

                            onReleased: {
                                onCanceled()
                            }
                        }

                        Rectangle {
                            anchors.bottom: parent.bottom
                            width: parent.width
                            height: 1
                            color: model.index === list.count - 1 ? "white" : "lightgray"
                        }
                    }
                }

                Text {
                    id: noItems
                    anchors.fill: parent
                    visible: false
                    text: qsTr("재생 목록이 없습니다.")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                Scrollbar {
                    id: scrollbar
                    flickable: list
                }
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        property Item focusParent: null
        property string name

        function exit()
        {
            if (focusParent != null)
            {
                focusParent.forceActiveFocus()
                focusParent = null
            }

            opacity = 0.0
            messageBoxHideTimer.restart()
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            onGoBack: {
                messageBoxLoader.exit()
            }
        }
    }

    Component.onCompleted: {
        updateNoItem()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            top.exit()
        }
    }
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import com.dcple.anyvod 1.0

ListModel {
    ListElement {
        icon: "assets/equalizer.png"
        text: qsTr("이퀄라이저")
        menuID: AnyVODEnums.MID_EQUALIZER
    }

    ListElement {
        icon: "assets/screen.png"
        text: qsTr("화면")
        menuID: AnyVODEnums.MID_SCREEN
    }

    ListElement {
        icon: "assets/play.png"
        text: qsTr("재생")
        menuID: AnyVODEnums.MID_PLAY
    }

    ListElement {
        icon: "assets/subtitle.png"
        text: qsTr("자막 / 가사")
        menuID: AnyVODEnums.MID_SUBTITLE_LYRICS
    }

    ListElement {
        icon: "assets/sound.png"
        text: qsTr("소리")
        menuID: AnyVODEnums.MID_SOUND
    }

    ListElement {
        icon: "assets/info.png"
        text: qsTr("정보")
        menuID: AnyVODEnums.MID_INFO
    }
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "transparent"

    property string desc
    property string link
    property alias title: title.text
    property alias windowX: root.x
    property alias windowY: root.y
    property alias windowWidth: root.width
    property alias windowHeight: root.height

    signal goBack

    function getDesc()
    {
        return desc.replace(/\n/gi, "<br>")
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: goBack()
        onCanceled: goBack()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    Rectangle {
        id: root
        color: "#7f000000"
        radius: 3

        Column {
            anchors.fill: parent
            spacing: 5

            Rectangle {
                id: titleSpacer
                color: "transparent"
                width: parent.width
                height: 0.1
            }

            Text {
                id: title
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 5
                anchors.rightMargin: 5
                font.pixelSize: 12
                verticalAlignment: Text.AlignVCenter
                color: "white"
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width - anchors.leftMargin - anchors.rightMargin
                height: parent.height - title.height - titleSpacer.height - parent.spacing * 3
                spacing: 5

                Rectangle {
                    id: spacer1
                    color: "transparent"
                    width: 0.1
                    height: parent.height
                }

                Text {
                    id: link
                    focus: true
                    text: "<a style='color: lightblue;text-decoration: none;' href='" + topRoot.link + "'>" + getDesc() + "</a>"
                    textFormat: Text.RichText
                    width: parent.width - parent.spacing * 2
                    height: parent.height
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: 13

                    onLinkActivated: {
                        Qt.openUrlExternally(topRoot.link)
                        goBack()
                    }
                }

                Rectangle {
                    id: spacer2
                    color: "transparent"
                    width: 0.1
                    height: parent.height
                }
            }
        }
    }
}

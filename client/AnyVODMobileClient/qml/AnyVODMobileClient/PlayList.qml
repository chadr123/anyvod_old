﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property PlayListModel playListModel: null
    property MediaDelegate mediaDelegate: null

    signal goBack
    signal itemSelected(int index)
    signal newQualitySelected(int index, int quality)

    function exit()
    {
        hideMenus()
        goBack()
    }

    function showItemMenu(menu, parent, index)
    {
        var globalCoord = parent.mapToItem(null, parent.x, parent.y)
        var showX = parent.x - menu.menuWidth + parent.width
        var showY = globalCoord.y - parent.height
        var offset = 10
        var maxHeight = showY + menu.menuHeight + offset

        if (maxHeight >= root.height)
            showY = root.height - (menu.menuHeight + offset)

        menu.itemIndex = index
        menu.show(showX, showY)
    }

    function hideMenus()
    {
        itemMenu.hide()
    }

    function updatePlayList()
    {
        playListModel.updateCurrentPlayList()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Component {
        id: notSupported

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("지원하지 않는 기능입니다.")
        }
    }

    Component {
        id: copyClipboard

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("클립보드로 경로가 복사 되었습니다.")
        }
    }

    Component {
        id: confirmName

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("이름을 입력 해 주세요.")
        }
    }

    Component {
        id: alreadyExist

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 170)
            title: qsTr("질문")
            message: qsTr("이미 동일한 재생 목록이 존재합니다.\n갱신 하시겠습니까?")
            useCancel: true

            onOk: {
                updatePlayList()
                settingDlgLoader.exit()
            }
        }
    }

    Component {
        id: viewPath

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 250)
            title: qsTr("정보")
            message: messageBoxLoader.text
            messageVAlign: Text.AlignTop
        }
    }

    Component {
        id: qualityList

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 400)
            listHeight: Math.min(settingListDefaultHeight, 423)
            title: qsTr("화질 목록")
            media: mediaDelegate
            type: AnyVODEnums.SK_OTHER_QUALITY
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < playListModel.getQualityCount(itemMenu.itemIndex); i++)
                    {
                        var desc = playListModel.getQualityDesc(itemMenu.itemIndex, i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }

            onItemSelected: {
                topRoot.exit()
                newQualitySelected(itemMenu.itemIndex, index)
            }
        }
    }

    Component {
        id: savePlayListAs

        SavePlayListAs {
            id: savePlayListAsObject
            windowHeight: Math.min(settingListDefaultHeight, 190)
            title: qsTr("재생 목록 신규 저장")

            onAccepted: {
                if (name.length <= 0)
                {
                    messageBoxLoader.focusParent = savePlayListAsObject
                    messageBoxLoader.sourceComponent = confirmName
                }
                else
                {
                    if (playListModel.existPlayList(name))
                    {
                        messageBoxLoader.sourceComponent = alreadyExist
                    }
                    else
                    {
                        playListModel.savePlayListAs(name)
                        settingDlgLoader.exit()
                    }
                }

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Menus {
        id: itemMenu
        menuWidth: 150
        menuHeight: 120
        model: PlayListMenuItems {}

        property int itemIndex

        onMenuItemSelected: {
            var item = model.get(index)
            var path

            switch (item.menuID)
            {
                case AnyVODEnums.MID_VIEW_PATH:
                    path = playListModel.getPath(itemIndex)

                    messageBoxLoader.text = path
                    messageBoxLoader.sourceComponent = viewPath

                    break
                case AnyVODEnums.MID_COPY_PATH:
                    path = playListModel.getPath(itemIndex)

                    playListModel.copyPath(path)
                    messageBoxLoader.sourceComponent = copyClipboard

                    break
                case AnyVODEnums.MID_OTHER_QUALITY:
                    if (playListModel.getQualityCount(itemIndex) > 0)
                        settingDlgLoader.sourceComponent = qualityList
                    else
                        messageBoxLoader.sourceComponent = notSupported

                    break
            }

            if (settingDlgLoader.sourceComponent != undefined)
                settingDlgLoader.forceActiveFocus()

            if (messageBoxLoader.sourceComponent != undefined)
                messageBoxLoader.forceActiveFocus()
        }
    }

    Rectangle {
        id: root
        anchors.centerIn: parent
        width: parent.width * 0.9
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            Column {
                anchors.fill: parent

                Rectangle {
                    width: parent.width
                    height: 50

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "#1f88f9" }
                        GradientStop { position: 0.93; color: "#1f88f9" }
                        GradientStop { position: 1.0; color: "#1874d0" }
                    }

                    Row {
                        id: navigation
                        width: parent.width
                        height: parent.height

                        Rectangle {
                            id: spacer1
                            color: "transparent"
                            height: parent.height
                            width: 10
                        }

                        Text {
                            id: title
                            width: parent.width - spacer1.width
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            color: "white"
                            text: qsTr("재생 목록")
                            font.pixelSize: parent.height * 0.3
                        }
                    }
                }

                Rectangle {
                    id: listRoot
                    width: parent.width
                    height: parent.height - navigation.height - spacer.height - closeButton.height
                    color: "white"
                    clip: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        model: playListModel

                        delegate: Rectangle {
                            id: itemRoot
                            width: parent.width
                            height: 40
                            color: listRoot.color

                            Rectangle {
                                x: 10
                                width: parent.width - x
                                height: parent.height
                                color: "transparent"

                                MouseArea {
                                    id: mouseArea
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.fill: parent

                                    Row {
                                        anchors.fill: parent
                                        spacing: 10

                                        Rectangle {
                                            id: thumbnailRoot
                                            anchors.verticalCenter: parent.verticalCenter
                                            width: 30
                                            height: width
                                            color: "transparent"
                                            border.width: 1
                                            border.color: "#d0d0d0"

                                            Image {
                                                id: thumbnail
                                                width: parent.width - thumbnailRoot.border.width * 2
                                                height: parent.height - thumbnailRoot.border.width * 2
                                                anchors.verticalCenter: parent.verticalCenter
                                                anchors.horizontalCenter: parent.horizontalCenter
                                                source: model.thumbnail
                                                fillMode: Image.PreserveAspectCrop
                                            }
                                        }

                                        Text {
                                            width: parent.width - thumbnail.width - openItemMenu.width -
                                                   scrollbar.width - parent.spacing * 2
                                            height: parent.height
                                            verticalAlignment: Text.AlignVCenter
                                            color: model.color
                                            font.pixelSize: parent.height * 0.38
                                            elide: Text.ElideMiddle
                                            text: model.title.length > 0 ? model.title : model.name
                                        }

                                        ImageButton {
                                            id: openItemMenu
                                            width: 30
                                            height: width
                                            image.width: width
                                            image.height: height
                                            source: "assets/itemMenu.png"
                                            anchors.verticalCenter: parent.verticalCenter

                                            onClicked: {
                                                showItemMenu(itemMenu, openItemMenu, model.index)
                                            }
                                        }
                                    }

                                    onClicked: {
                                        exit()
                                        itemSelected(model.index)
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = listRoot.color
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                Rectangle {
                    id: spacer
                    width: parent.width
                    height: 1
                    color: "gray"
                }

                Rectangle {
                    width: parent.width
                    height: 50

                    Row {
                        width: parent.width
                        height: parent.height
                        spacing: 10
                        layoutDirection: Qt.RightToLeft

                        ImageButton {
                            id: closeButton
                            paddingValue: 20
                            height: parent.height
                            text.text: qsTr("닫기")
                            text.color: "green"
                            text.verticalAlignment: Text.AlignVCenter
                            text.horizontalAlignment: Text.AlignHCenter

                            onClicked: {
                                exit()
                            }
                        }

                        ImageButton {
                            id: newButton
                            paddingValue: 20
                            height: parent.height
                            text.text: qsTr("신규 저장")
                            text.color: "green"
                            text.verticalAlignment: Text.AlignVCenter
                            text.horizontalAlignment: Text.AlignHCenter

                            onClicked: {
                                settingDlgLoader.sourceComponent = savePlayListAs
                                settingDlgLoader.forceActiveFocus()
                            }
                        }
                    }
                }
            }
        }
    }

    Loader {
        id: settingDlgLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        function exit()
        {
            root.forceActiveFocus()

            opacity = 0.0
            settingDlgHideTimer.restart()
        }

        Timer {
            id: settingDlgHideTimer
            interval: settingDlgOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingDlgOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingDlgLoader.item

            onGoBack: {
                settingDlgLoader.exit()
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        property Item focusParent: null
        property string text

        function exit()
        {
            if (focusParent != null)
            {
                focusParent.forceActiveFocus()
                focusParent = null
            }

            opacity = 0.0
            messageBoxHideTimer.restart()
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            onGoBack: {
                messageBoxLoader.exit()
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}

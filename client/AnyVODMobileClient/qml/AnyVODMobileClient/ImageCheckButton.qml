﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5

Rectangle {
    id: button
    color: "transparent"

    signal clicked
    signal pressed
    signal released
    signal toggled

    property Timer resetTimer: null
    property bool checked: false
    property alias enabled: button.enabled
    property string checkedSource: null
    property string unCheckedSource: null

    Image {
        id: img
        anchors.fill: parent
        fillMode: Image.Stretch
        source: checked ? button.checkedSource : button.unCheckedSource
    }

    MouseArea {
        id: mouseArea
        enabled: button.enabled
        anchors.fill: button
        hoverEnabled: true

        onClicked: {
            if (resetTimer != null)
                resetTimer.restart()

            button.clicked()

            button.checked = !button.checked
            button.toggled()
        }

        onCanceled: {
            button.clicked()
            button.released()
        }

        onPressed: button.pressed()
        onReleased: button.released()
    }

    onPressed: {
        opacity = 0.5
    }

    onReleased: {
        opacity = 1.0
    }

    onEnabledChanged: {
        if (enabled)
            opacity = 1.0
        else
            opacity = 0.3
    }
}

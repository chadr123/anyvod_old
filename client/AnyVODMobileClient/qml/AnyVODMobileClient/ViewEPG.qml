﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: top
    anchors.fill: parent
    focus: true
    color: "#88000000"

    property RenderScreen query: null
    property alias title: title.text
    property alias channel: channel.text

    signal goBack

    function exit()
    {
        goBack()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Timer {
        id: initTimer
        interval: 500

        onTriggered: {
            epgListModel.init()
        }
    }

    Timer {
        id: curUpdateTimer
        interval: 1000
        repeat: true

        onTriggered: {
            var date =  epgListModel.updateCurrentTime()

            curTime.text = date.toLocaleString(Qt.locale(), "yyyy-MM-dd hh:mm:ss")
        }
    }

    EPGListModel {
        id: epgListModel
        screen: query

        onItemSelected: {
            if (index !== -1)
            {
                updateToCurrentItem(index)
                list.contentX = list.itemSize * index
            }
            else
            {
                programTitle.text = " "
                period.text = " "
            }
        }

        onInitUpdateTimer: {
            curUpdateTimer.restart()
        }

        function updateToCurrentItem(i)
        {
            var mIndex = index(i, 0)

            programTitle.text = data(mIndex, EPGListModel.TitleRole)

            var start = data(mIndex, EPGListModel.StartTimeRole).toLocaleString(Qt.locale(), "yyyy-MM-dd hh:mm:ss")
            var end = data(mIndex, EPGListModel.EndTimeRole).toLocaleString(Qt.locale(), "hh:mm:ss")

            period.text = start + " ~ " + end
        }

        Component.onCompleted: {
            initTimer.restart()
        }
    }

    Rectangle {
        id: root
        anchors.centerIn: parent
        width: Math.min(parent.width * 0.8, 700)
        height: 310
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            Column {
                anchors.fill: parent

                Rectangle {
                    width: parent.width
                    height: 50

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "#1f88f9" }
                        GradientStop { position: 0.93; color: "#1f88f9" }
                        GradientStop { position: 1.0; color: "#1874d0" }
                    }

                    Row {
                        id: navigation
                        width: parent.width
                        height: parent.height

                        Rectangle {
                            id: spacer1
                            color: "transparent"
                            height: parent.height
                            width: 10
                        }

                        Text {
                            id: title
                            width: parent.width - spacer1.width
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            color: "white"
                            elide: Text.ElideMiddle
                            font.pixelSize: parent.height * 0.3
                        }
                    }
                }

                Rectangle {
                    id: brief
                    width: parent.width
                    height: 75

                    Grid {
                        columns: 2
                        columnSpacing: 10
                        x: 10
                        width: parent.width - x * 2

                        Text {
                            text: qsTr("채널")
                        }

                        Text {
                            id: channel
                        }

                        Text {
                            text: qsTr("제목")
                        }

                        Text {
                            id: programTitle
                            text: qsTr("업데이트 중...")
                        }

                        Text {
                            text: qsTr("방송 시간")
                        }

                        Text {
                            id: period
                            text: qsTr("업데이트 중...")
                        }
                    }
                }

                Rectangle {
                    id: listSpacer
                    width: parent.width
                    height: 20
                }

                Rectangle {
                    id: listRoot
                    width: parent.width
                    height: parent.height - navigation.height - brief.height - listSpacer.height -
                            closeSpacer.height - closeButton.height
                    clip: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        orientation: ListView.Horizontal
                        model: epgListModel

                        readonly property int itemSize: 220

                        delegate: Rectangle {
                            width: list.itemSize
                            height: parent.height

                            Rectangle {
                                id: itemRoot
                                anchors.fill: parent
                                color: "transparent"

                                MouseArea {
                                    anchors.fill: parent

                                    Column {
                                        anchors.fill: parent
                                        spacing: 3

                                        Text {
                                            anchors.horizontalCenter: parent.horizontalCenter
                                            text: model.startTime.toLocaleString(Qt.locale(), "[yyyy-MM-dd]")
                                            font.bold: true
                                        }

                                        Text {
                                            anchors.horizontalCenter: parent.horizontalCenter
                                            width: parent.width - 20
                                            text: model.title
                                            horizontalAlignment: Text.AlignHCenter
                                            elide: Text.ElideMiddle
                                        }

                                        Row {
                                            anchors.horizontalCenter: parent.horizontalCenter
                                            spacing: 10

                                            Text {
                                                anchors.verticalCenter: parent.verticalCenter
                                                text: model.startTime.toLocaleString(Qt.locale(), "hh:mm:ss")
                                            }

                                            ProgressBar {
                                                id: progress
                                                anchors.verticalCenter: parent.verticalCenter
                                                height: 10
                                                width: 50
                                            }

                                            Text {
                                                anchors.verticalCenter: parent.verticalCenter
                                                text: model.endTime.toLocaleString(Qt.locale(), "hh:mm:ss")
                                            }

                                            Component.onCompleted: {
                                                var curDate = new Date()

                                                if (model.startTime <= curDate && model.endTime >= curDate)
                                                {
                                                    progress.minimumValue = 0
                                                    progress.maximumValue = model.duration
                                                    progress.value = (curDate.getTime() - model.startTime.getTime()) / 1000
                                                }
                                                else if (curDate < model.startTime)
                                                {
                                                    progress.minimumValue = 0
                                                    progress.maximumValue = 1
                                                    progress.value = 0
                                                }
                                                else
                                                {
                                                    progress.minimumValue = 0
                                                    progress.maximumValue = 1
                                                    progress.value = 1
                                                }
                                            }
                                        }
                                    }

                                    onClicked: {
                                        epgListModel.updateToCurrentItem(model.index)
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = "transparent"
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        vertical: false
                        flickable: list
                    }
                }

                Rectangle {
                    id: closeSpacer
                    width: parent.width
                    height: 1
                    color: "gray"
                }

                Rectangle {
                    id: closeButton
                    width: parent.width
                    height: 50

                    Row {
                        height: parent.height
                        anchors.left: parent.left
                        anchors.leftMargin: 10
                        spacing: 10

                        Text {
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("현재 시각")
                        }

                        Text {
                            id: curTime
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("업데이트 중...")
                        }
                    }

                    ImageButton {
                        anchors.right: parent.right
                        anchors.rightMargin: 20
                        height: parent.height
                        text.text: qsTr("닫기")
                        text.color: "green"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignRight

                        onClicked: {
                            exit()
                        }
                    }
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            top.exit()
        }
    }
}

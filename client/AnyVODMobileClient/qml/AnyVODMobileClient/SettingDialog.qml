﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import com.dcple.anyvod 1.0

Item {
    anchors.fill: parent
    focus: true

    property FileListModel query: null
    property MediaDelegate media: null
    property alias title: title.text
    property alias model: list.model

    signal goBack
    signal checkItemSelected(int index, bool checked)
    signal itemSelected(int index)
    signal notSupportedFunction

    Column {
        id: root
        anchors.fill: parent

        Rectangle {
            width: parent.width
            height: 50

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#1f88f9" }
                GradientStop { position: 0.93; color: "#1f88f9" }
                GradientStop { position: 1.0; color: "#1874d0" }
            }

            Row {
                id: navigation
                width: parent.width
                height: parent.height

                Rectangle {
                    id: spacer1
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                ImageButton {
                    id: back
                    width: 30
                    height: parent.height
                    image.width: 20
                    image.height: image.width
                    source: "assets/back.png"
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: {
                        goBack()
                    }
                }

                Rectangle {
                    id: spacer2
                    color: "transparent"
                    height: parent.height
                    width: 0
                }

                Text {
                    id: title
                    width: parent.width - back.width - spacer1.width - spacer2.width
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    elide: Text.ElideMiddle
                    font.pixelSize: parent.height * 0.3
                }
            }
        }

        Rectangle {
            id: listRoot
            width: parent.width
            height: parent.height - navigation.height
            color: "white"
            clip: true

            Component {
                id: header

                Rectangle {
                    width: list.width
                    height: 40
                    color: "#f0f0f0"

                    Text {
                        x:10
                        width: parent.width
                        height: parent.height
                        text: section
                        verticalAlignment: Text.AlignVCenter
                        color: "gray"
                        font.pixelSize: parent.height * 0.35
                        font.bold: true
                    }
                }
            }

            Rectangle {
                anchors.fill: parent
                color: "#f8f8f8"

                ListView {
                    id: list
                    width: parent.width
                    height: parent.height

                    section.property: "category"
                    section.criteria: ViewSection.FullString
                    section.delegate: header

                    delegate: Rectangle {
                        width: parent.width
                        height: 50

                        Rectangle {
                            id: settingItemRoot
                            anchors.fill: parent
                            color: "white"

                            MouseArea {
                                id: mouseArea
                                anchors.fill: parent

                                Text {
                                    x: 10
                                    height: parent.height
                                    verticalAlignment: Text.AlignVCenter
                                    font.pixelSize: parent.height * 0.36
                                    text: model.desc
                                }

                                ImageCheckButton {
                                    id: checkButton
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.right: parent.right
                                    anchors.rightMargin: scrollbar.width + 20
                                    visible: model.check
                                    width: 20
                                    height: width
                                    checked: model.check ? (query == null ? media.getSetting(model.type) : query.getSetting(model.type)) : false
                                    checkedSource: "assets/checked.png"
                                    unCheckedSource: "assets/unchecked.png"

                                    onToggled: {
                                        if (query)
                                            query.setSetting(model.type, checked)
                                        else
                                            media.setSetting(model.type, checked)

                                        checkItemSelected(index, checked)
                                    }
                                }

                                onClicked: {
                                    var isSupported = (query == null ? media.isSupported(model.type) : query.isSupported(model.type))

                                    if (isSupported)
                                    {
                                        if (model.check)
                                        {
                                            checkButton.checked = !checkButton.checked
                                            checkButton.toggled()
                                        }
                                        else
                                        {
                                            itemSelected(index)
                                        }
                                    }
                                    else
                                    {
                                        notSupportedFunction()
                                    }
                                }

                                onPressed: {
                                    settingItemRoot.color = "lightgray"
                                    settingItemRoot.opacity = 0.5
                                }

                                onCanceled: {
                                    settingItemRoot.color = "white"
                                    settingItemRoot.opacity = 1.0
                                }

                                onReleased: {
                                    onCanceled()
                                }
                            }
                        }

                        Rectangle {
                            anchors.bottom: parent.bottom
                            width: parent.width
                            height: 1
                            color: model.index === list.count - 1 ? "white" : "lightgray"
                        }
                    }
                }

                Scrollbar {
                    id: scrollbar
                    flickable: list
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            goBack()
        }
    }
}

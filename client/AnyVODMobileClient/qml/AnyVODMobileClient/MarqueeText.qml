﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.0

Rectangle {
    id: root
    clip: true
    color: "transparent"

    property alias text: text
    property bool running: false

    readonly property int velocity: 30

    function restart()
    {
        stop()
        start()
    }

    function start()
    {
        movingArea.x = 0
        running = true

        leftMoveTimer.restart()
    }

    function stop()
    {
        running = false

        leftMove.stop()
        rightMove.stop()

        leftMoveTimer.stop()
        rightMoveTimer.stop()
    }

    Rectangle {
        id: movingArea
        width: text.width
        height: parent.height
        color: "transparent"

        function getMovingWidth()
        {
            var val = text.width - root.width

            if (val < 0)
                val = 0

            return val
        }

        Timer {
            id: leftMoveTimer
            interval: 2000

            onTriggered: {
                if (text.width <= root.width)
                {
                    root.stop()
                    return
                }

                if (root.running)
                    leftMove.start()
            }
        }

        Timer {
            id: rightMoveTimer
            interval: 2000

            onTriggered: {
                if (root.running)
                    rightMove.start()
            }
        }

        NumberAnimation on x {
            id: leftMove
            from: 0
            to: -movingArea.getMovingWidth()
            duration: movingArea.getMovingWidth() * root.velocity
            running: false

            onStopped: {
                if (root.running)
                    rightMoveTimer.restart()
            }
        }

        NumberAnimation on x {
            id: rightMove
            from: -movingArea.getMovingWidth()
            to: 0
            duration: movingArea.getMovingWidth() * root.velocity
            running: false

            onStopped: {
                if (root.running)
                    leftMoveTimer.restart()
            }
        }

        Text {
            id: text
            height: parent.height
        }
    }

    onWidthChanged: {
        restart()
    }
}

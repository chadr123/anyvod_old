﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import com.dcple.anyvod 1.0

Item {
    id: top
    anchors.fill: parent
    focus: true

    property FileListModel query: null
    property alias title: title.text

    readonly property variant bands: ["31Hz", "62Hz", "125Hz", "250Hz", "500Hz", "1kHz", "2kHz", "4kHz", "8kHz", "16kHz"]
    readonly property int settingListDefaultWidth: width * 0.8

    signal goBack

    function exit()
    {
        query.saveEqualizerPreamp(preamp.value)
        query.saveEqualizerBand()

        goBack()
    }

    function reloadList()
    {
        listModel.clear()
        query.initEqualizerValue()

        for (var i = 0; i < query.getEqualizerBandCount(); i++)
            listModel.append({ "band": bands[i] })
    }

    Component {
        id: presets

        ComboItem {
            listWidth: Math.min(settingListDefaultWidth, 300)
            title: qsTr("프리셋")
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < query.getEqualizerPresetCount(); i++)
                    {
                        var desc = query.getEqualizerPresetName(i)

                        append({ "desc": desc, "value": i })
                    }

                    defaultValue = query.getEqualizerCurrentIndex()
                }
            }

            onItemSelected: {
                query.setEqualizerPreset(index)
                reloadList()
            }
        }
    }

    Column {
        id: root
        anchors.fill: parent

        function valueTodB(value)
        {
            var v = query.valueTodB(value)

            return v.toFixed(1)
        }

        Rectangle {
            width: parent.width
            height: 50

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#1f88f9" }
                GradientStop { position: 0.93; color: "#1f88f9" }
                GradientStop { position: 1.0; color: "#1874d0" }
            }

            Row {
                id: navigation
                width: parent.width
                height: parent.height

                Rectangle {
                    id: spacer1
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                ImageButton {
                    id: back
                    width: 30
                    height: parent.height
                    image.width: 20
                    image.height: image.width
                    source: "assets/back.png"
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: {
                        top.exit()
                    }

                }

                Rectangle {
                    id: spacer2
                    color: "transparent"
                    height: parent.height
                    width: 0
                }

                Text {
                    id: title
                    width: parent.width - back.width - spacer1.width - spacer2.width
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    elide: Text.ElideMiddle
                    font.pixelSize: parent.height * 0.3
                }
            }
        }

        Rectangle {
            width: parent.width
            height: 50

            Row {
                id: menu
                anchors.fill: parent

                Rectangle {
                    id: spacerMenu1
                    color: "transparent"
                    height: parent.height
                    width: 15
                }

                Rectangle {
                    id: presetBox
                    width: preset.width + 20
                    height: parent.height - 20
                    anchors.verticalCenter: parent.verticalCenter
                    color: "gray"

                    ImageButton {
                        id: preset
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: parent.height
                        text.text: qsTr("프리셋")
                        text.color: "white"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignHCenter

                        onClicked: {
                            comboLoader.sourceComponent = presets
                            comboLoader.forceActiveFocus()
                        }
                    }
                }

                Rectangle {
                    id: spacerMenu2
                    color: "transparent"
                    height: parent.height
                    width: 15
                }

                Rectangle {
                    id: resetBox
                    width: reset.width + 20
                    height: parent.height - 20
                    anchors.verticalCenter: parent.verticalCenter
                    color: "gray"

                    ImageButton {
                        id: reset
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: parent.height
                        text.text: qsTr("초기화")
                        text.color: "white"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignHCenter

                        onClicked: {
                            preamp.value = 0
                            query.setEqualizerPreset(0)
                            reloadList()
                        }
                    }
                }

                Rectangle {
                    id: spacerMenu3
                    color: "transparent"
                    height: parent.height
                    width: parent.width - presetBox.width - resetBox.width - useEqualizer.width - spacerMenu1.width - spacerMenu2.width - spacerMenu4.width
                }

                Row {
                    id: useEqualizer
                    height: parent.height

                    Text {
                        height: parent.height
                        text: qsTr("이퀄라이저 사용")
                        verticalAlignment: Text.AlignVCenter
                    }

                    Rectangle {
                        color: "transparent"
                        height: parent.height
                        width: 15
                    }

                    ImageCheckButton {
                        anchors.verticalCenter: parent.verticalCenter
                        width: 20
                        height: width
                        checked: query.getUseEqualizer()
                        checkedSource: "assets/checked.png"
                        unCheckedSource: "assets/unchecked.png"

                        onToggled: {
                            query.setUseEqualizer(checked)
                        }
                    }
                }

                Rectangle {
                    id: spacerMenu4
                    color: "transparent"
                    height: parent.height
                    width: 20
                }
            }
        }

        Rectangle {
            id: mainSpacer1
            height: 10
            width: parent.width
        }

        Rectangle {
            width: parent.width
            height: 20

            Row {
                id: preampRoot
                anchors.fill: parent

                Rectangle {
                    id: preampSpacer1
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                Text {
                    id: preampLabel
                    width: 60
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    text: "PreAmp"
                    color: "gray"
                }

                Rectangle {
                    id: preampSpacer2
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                Slider {
                    id: preamp
                    width: parent.width - preampValue.width - preampLabel.width -
                           preampSpacer1.width - preampSpacer2.width - preampSpacer3.width - preampSpacer4.width
                    height: parent.height
                    minimumValue: -150
                    maximumValue: 150
                    stepSize: 1

                    style: SliderStyle {
                        groove: Rectangle {
                            width: control.height
                            height: control.height * 0.7
                            color: "lightgray"
                            radius: 8

                            Rectangle{
                                anchors.left: parent.left
                                anchors.verticalCenter: parent.verticalCenter
                                width: widthValue()
                                height: parent.height
                                color: "#1f88f9"
                                radius: parent.radius

                                function widthValue()
                                {
                                    if (control.value <= control.minimumValue)
                                        return 0
                                    else
                                        return styleData.handlePosition - anchors.leftMargin - control.height / 2 + radius + 2
                                }
                            }
                        }

                        handle: Rectangle {
                            anchors.centerIn: parent
                            color: control.pressed ? "white" : "lightgray"
                            border.color: "gray"
                            border.width: 2
                            width: control.height
                            height: width
                            radius: width
                        }
                    }

                    Component.onCompleted: {
                        var preamp = query.getEqualizerPreamp()

                        value = preamp
                        preampValue.text = root.valueTodB(value) + "dB"
                    }

                    onValueChanged: {
                        preampValue.text = root.valueTodB(value) + "dB"
                    }
                }

                Rectangle {
                    id: preampSpacer3
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                Text {
                    id: preampValue
                    width: 50
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    color: "gray"
                }

                Rectangle {
                    id: preampSpacer4
                    color: "transparent"
                    height: parent.height
                    width: 10
                }
            }
        }

        Rectangle {
            id: listRoot
            width: parent.width
            height: parent.height - navigation.height - menu.height - mainSpacer1.height - preampRoot.height
            color: "white"

            ListView {
                id: list
                anchors.fill: parent
                orientation: ListView.Horizontal

                function itemWidth()
                {
                    var w
                    var count = listModel.rowCount()

                    if (count)
                        w = width / count
                    else
                        w = 0

                    return Math.max(60, w)
                }

                model: ListModel {
                    id: listModel

                    Component.onCompleted: {
                        reloadList()
                    }
                }

                delegate: Rectangle {
                    width: list.itemWidth()
                    height: parent.height

                    Rectangle {
                        id: itemRoot
                        anchors.fill: parent
                        anchors.leftMargin: 20
                        anchors.rightMargin: 20
                        color: "white"

                        Column {
                            width: parent.width
                            height: parent.height

                            Rectangle {
                                id: equSpacer1
                                color: "transparent"
                                height: 20
                                width: parent.width
                            }

                            Text {
                                id: equValue
                                horizontalAlignment: Text.AlignHCenter
                                width: parent.width
                                height: 20
                                color: "gray"
                            }

                            Rectangle {
                                id: equSpacer2
                                color: "transparent"
                                height: 10
                                width: parent.height
                            }

                            Slider {
                                id: equlItem
                                anchors.horizontalCenter: parent.horizontalCenter
                                width: 17
                                height: parent.height - equValue.height - equLabel.height -
                                        equSpacer1.height - equSpacer2.height - equSpacer3.height - equSpacer4.height
                                minimumValue: -150
                                maximumValue: 150
                                stepSize: 1
                                orientation: Qt.Vertical

                                style: SliderStyle {
                                    groove: Rectangle {
                                        width: control.width * 0.7
                                        height: control.width
                                        color: "lightgray"
                                        radius: 8

                                        Rectangle{
                                            anchors.left: parent.left
                                            anchors.verticalCenter: parent.verticalCenter
                                            width: widthValue()
                                            height: parent.height
                                            color: "#1f88f9"
                                            radius: parent.radius

                                            function widthValue()
                                            {
                                                if (control.value <= control.minimumValue)
                                                    return 0
                                                else
                                                    return styleData.handlePosition - anchors.leftMargin - (control.width * 1.4) / 2 + radius + 3
                                            }
                                        }
                                    }

                                    handle: Rectangle {
                                        anchors.centerIn: parent
                                        color: control.pressed ? "white" : "lightgray"
                                        border.color: "gray"
                                        border.width: 3
                                        width: control.width * 1.4
                                        height: width
                                        radius: width
                                    }
                                }

                                Component.onCompleted: {
                                    var equl = query.getEqualizerBandGain(index)

                                    value = equl
                                    equValue.text = root.valueTodB(value) + "dB"
                                }

                                onValueChanged: {
                                    query.setEqualizerBandForSave(index, value)
                                    equValue.text = root.valueTodB(value) + "dB"
                                }
                            }

                            Rectangle {
                                id: equSpacer3
                                color: "transparent"
                                height: 10
                                width: parent.width
                            }

                            Text {
                                id: equLabel
                                horizontalAlignment: Text.AlignHCenter
                                width: parent.width
                                height: 20
                                text: model.band
                                color: "gray"
                            }

                            Rectangle {
                                id: equSpacer4
                                color: "transparent"
                                height: 10
                                width: parent.width
                            }
                        }

                    }
                }
            }

            Scrollbar {
                id: scrollbar
                vertical: false
                flickable: list
            }
        }
    }

    Loader {
        id: comboLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10

        function exit()
        {
            root.forceActiveFocus()

            opacity = 0.0
            comboHideTimer.restart()
        }

        Timer {
            id: comboHideTimer
            interval: comboOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: comboOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: comboLoader.item

            onGoBack: {
                comboLoader.exit()
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            top.exit()
        }
    }
}

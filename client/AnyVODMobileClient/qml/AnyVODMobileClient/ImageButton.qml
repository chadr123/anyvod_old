﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5

Rectangle {
    id: button
    color: "transparent"
    width: space.width + img.width + text.width + paddingValue

    signal clicked
    signal pressed
    signal released

    property alias image: img
    property alias text: txt
    property alias spacing: space.width
    property string source
    property string checkedSource
    property string textColor
    property string checkedTextColor
    property Timer resetTimer: null
    property bool checked: false
    property int paddingValue: 0

    function getImage()
    {
        if (checkedSource.length === 0)
            return source
        else
            return checked ? button.checkedSource : button.source
    }

    function getTextColor()
    {
        if (checkedTextColor.length === 0)
            return textColor
        else
            return checked ? button.checkedTextColor : button.textColor
    }

    Row {
        id: row
        anchors.fill: parent

        Image {
            id: img
            anchors.verticalCenter: parent.verticalCenter
            fillMode: Image.Stretch
            source: getImage()
        }

        Rectangle {
            id: space
            height: parent.height
            color: "transparent"
        }

        Text {
            id: txt
            height: parent.height
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
            font.bold: button.checked
            color: getTextColor()
        }
    }

    MouseArea {
        id: mouseArea
        enabled: button.enabled
        anchors.fill: button
        hoverEnabled: true

        onClicked: {
            if (resetTimer != null)
                resetTimer.restart()

            button.clicked()
        }

        onCanceled: {
            button.clicked()
            button.released()
        }

        onPressed: button.pressed()
        onReleased: button.released()
    }

    onPressed: {
        opacity = 0.5
    }

    onReleased: {
        opacity = 1.0
    }

    onEnabledChanged: {
        if (enabled)
            opacity = 1.0
        else
            opacity = 0.3
    }
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property string title
    property string curDirectory
    property string startDirectory
    property string selectedPath
    property bool onlyDirectory: false
    property bool onlySubtitle: false
    property string fileListObjName: fileListModel.objectName
    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property alias url: url.text

    signal goBack
    signal accepted
    signal ignore

    function updateTitle()
    {
        titleAndCurDirectory.text = title

        if (curDirectory.length > 0)
            titleAndCurDirectory.text += " - " + curDirectory
    }

    function resetToTop()
    {
        curDirectory = ""
        back.visible = false
        spacer2.visible = false
        fileListModel.clearScrollPos()
    }

    function showBackButton()
    {
        back.visible = true
        spacer2.visible = true
    }

    function updateCurrentPath()
    {
        selectedPath = fileListModel.curDirPath

        if (fileListModel.curDirName === fileListModel.topSignature)
            resetToTop()
        else
            curDirectory = fileListModel.curDirName

        url.text = curDirectory

        updateTitle()
    }

    function exit()
    {
        goBack()
        ignore()
    }

    VirtualKeyboardPanel {
        id: keyboard
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    FileListModel {
        id: fileListModel

        Component.onCompleted: {
            onlyDirectory = topRoot.onlyDirectory
            onlySubtitle  = topRoot.onlySubtitle
            startDirectory = topRoot.startDirectory

            init()

            if (startDirectory.length > 0)
            {
                topRoot.updateCurrentPath()
                topRoot.showBackButton()
            }
        }

        onModelReset: {
            noFiles.visible = rowCount() === 0
        }
    }

    Rectangle {
        id: root
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        width: parent.width * 0.9
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            Column {
                anchors.fill: parent

                Rectangle {
                    width: parent.width
                    height: 50

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "#1f88f9" }
                        GradientStop { position: 0.93; color: "#1f88f9" }
                        GradientStop { position: 1.0; color: "#1874d0" }
                    }

                    Row {
                        id: navigation
                        width: parent.width
                        height: parent.height

                        Rectangle {
                            id: spacer1
                            color: "transparent"
                            height: parent.height
                            width: 10
                        }

                        ImageButton {
                            id: back
                            width: 30
                            height: parent.height
                            image.width: 20
                            image.height: image.width
                            source: "assets/back.png"
                            visible: false
                            anchors.verticalCenter: parent.verticalCenter

                            onClicked: {
                                var contY = fileListModel.cdup()

                                fileListModel.update()

                                var maxHeight = list.contentHeight - list.height

                                if (maxHeight < 0.0)
                                    maxHeight = list.height

                                if (contY < 0.0)
                                    contY = 0.0
                                else if (contY > maxHeight)
                                    contY = maxHeight

                                list.contentY = contY

                                scrollbar.showScrollbar()
                                scrollbar.hideScrollbar()

                                topRoot.updateCurrentPath()
                                forceActiveFocus()
                            }
                        }

                        Rectangle {
                            id: spacer2
                            color: "transparent"
                            height: parent.height
                            width: 0
                            visible: false
                        }

                        Text {
                            id: titleAndCurDirectory
                            width: parent.width - back.width - spacer1.width - spacer2.width - spacer3.width
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            color: "white"
                            elide: Text.ElideMiddle
                            font.pixelSize: parent.height * 0.3
                        }

                        Rectangle {
                            id: spacer3
                            color: "transparent"
                            height: parent.height
                            width: 10
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    height: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? 1 : 40

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? "#c9c9c9" : "#f0f0f0" }
                        GradientStop { position: 0.98; color: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? "#c9c9c9" : "#f0f0f0" }
                        GradientStop { position: 1.0; color: "#c9c9c9" }
                    }

                    Grid {
                        id: quickbar
                        visible: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? false : true
                        anchors.fill: parent
                        columns: 2

                        Rectangle {
                            width: parent.width / parent.columns
                            height: parent.height
                            color: "transparent"

                            ImageButton {
                                id: primary
                                height: 20
                                spacing: 10
                                checked: true
                                image.width: 20
                                image.height: 20
                                text.text: qsTr("내장 메모리")
                                text.font.pixelSize: height * 0.7
                                anchors.centerIn: parent
                                source: "assets/internal.png"
                                checkedSource: "assets/internal_checked.png"
                                textColor: "black"
                                checkedTextColor: "#1f88f9"

                                onClicked: {
                                    fileListModel.update(fileListModel.primaryPath)

                                    selectedPath = fileListModel.curDirPath

                                    checked = true
                                    secondary.checked = false

                                    forceActiveFocus()

                                    topRoot.resetToTop()
                                    topRoot.updateTitle()
                                    url.text = curDirectory
                                }
                            }
                        }

                        Rectangle {
                            width: parent.width / parent.columns
                            height: parent.height
                            color: "transparent"

                            ImageButton {
                                id: secondary
                                height: 20
                                spacing: 10
                                checked: false
                                image.width: 20
                                image.height: 20
                                text.text: qsTr("외장 메모리")
                                text.font.pixelSize: height * 0.7
                                anchors.centerIn: parent
                                source: "assets/external.png"
                                checkedSource: "assets/external_checked.png"
                                textColor: "black"
                                checkedTextColor: "#1f88f9"

                                onClicked: {
                                    fileListModel.update(fileListModel.secondaryPath)

                                    selectedPath = fileListModel.curDirPath

                                    checked = true
                                    primary.checked = false

                                    forceActiveFocus()

                                    topRoot.resetToTop()
                                    topRoot.updateTitle()
                                    url.text = curDirectory
                                }
                            }
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    height: 35

                    ContextMenuTextInput {
                        id: url
                        anchors.fill: parent
                        anchors.topMargin: 5
                        anchors.bottomMargin: 5
                        anchors.leftMargin: 10
                        anchors.rightMargin: 10
                        inputFocus: false

                        onFinished: {
                            selectedPath = fileListModel.replaceLastEntry(selectedPath, text)
                        }
                    }
                }

                Rectangle {
                    id: storageRoot
                    width: parent.width
                    height: parent.height - navigation.height - quickbar.height - spacer.height - cancelButton.height
                    color: "white"
                    clip: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        model: fileListModel

                        delegate: Rectangle {
                            id: itemRoot
                            width: parent.width
                            height: 48
                            color: storageRoot.color

                            Rectangle {
                                x: 10
                                width: parent.width - x
                                height: parent.height
                                color: "transparent"

                                MouseArea {
                                    id: mouseArea
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.fill: parent

                                    Row {
                                        anchors.fill: parent
                                        spacing: 10

                                        Rectangle {
                                            id: thumbnailRoot
                                            anchors.verticalCenter: parent.verticalCenter
                                            width: height * 1.33
                                            height: parent.height - 12
                                            color: "transparent"
                                            border.width: model.type === FileListModel.Directory ? 0 : 1
                                            border.color: model.type === FileListModel.Directory ? "transparent" : "#d0d0d0"

                                            Image {
                                                id: thumbnail
                                                width: parent.width - thumbnailRoot.border.width * 2
                                                height: parent.height - thumbnailRoot.border.width * 2
                                                anchors.verticalCenter: parent.verticalCenter
                                                anchors.horizontalCenter: parent.horizontalCenter
                                                source: thumbnailImage()
                                                fillMode: Image.Stretch

                                                function thumbnailImage()
                                                {
                                                    if (model.type === FileListModel.Directory)
                                                    {
                                                        if (model.dirEntryCount === 0)
                                                            return fileListModel.defaultDirectoryIcon
                                                        else
                                                            return fileListModel.openDirectoryIcon
                                                    }
                                                    else
                                                    {
                                                        return model.thumbnail
                                                    }
                                                }
                                            }
                                        }

                                        Column {
                                            anchors.verticalCenter: parent.verticalCenter
                                            width: parent.width - parent.spacing - thumbnailRoot.width
                                            height: fileName.height + fileInfo.height + spacing * 1
                                            spacing: 1

                                            Text {
                                                id: fileName
                                                width: parent.width - scrollbar.width * 2
                                                text: model.fileName
                                                font.pixelSize: itemRoot.height * 0.25
                                                elide: Text.ElideMiddle
                                            }

                                            Text {
                                                id: fileInfo
                                                width: fileName.width
                                                color: "red"
                                                text: model.fileInfo.length === 0 ? qsTr("갱신 중...") : model.type === FileListModel.Directory ? qsTr("%1 항목").arg(model.fileInfo) : model.fileInfo
                                                font.pixelSize: fileName.font.pixelSize
                                                elide: Text.ElideMiddle
                                            }
                                        }
                                    }

                                    onClicked: {
                                        topRoot.showBackButton()

                                        selectedPath = model.path
                                        url.text = model.fileName

                                        forceActiveFocus()

                                        if (!fileListModel.isFile(model.path))
                                        {
                                            curDirectory = model.name
                                            topRoot.updateTitle()
                                            fileListModel.cd(model.path, list.contentY)
                                        }
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = storageRoot.color
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }
                        }
                    }

                    Rectangle {
                        id: noFiles
                        anchors.fill: parent
                        visible: false

                        Column {
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            spacing: 10

                            Image {
                                id: noFileImg
                                width: 100
                                source: "assets/nofiles.png"
                                fillMode: Image.PreserveAspectFit
                            }

                            Text {
                                width: noFileImg.width
                                horizontalAlignment: Text.AlignHCenter
                                text: qsTr("파일 또는 디렉토리가 없습니다")
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                Rectangle {
                    id: spacer
                    width: parent.width
                    height: 1
                    color: "gray"
                }

                Rectangle {
                    width: parent.width
                    height: 50

                    Row {
                        width: parent.width
                        height: parent.height
                        spacing: 10
                        layoutDirection: Qt.RightToLeft

                        ImageButton {
                            id: okButton
                            paddingValue: 20
                            height: parent.height
                            text.text: qsTr("열기")
                            text.color: "green"
                            text.verticalAlignment: Text.AlignVCenter
                            text.horizontalAlignment: Text.AlignHCenter

                            onClicked: {
                                url.finish()

                                goBack()
                                accepted()
                            }
                        }

                        ImageButton {
                            id: cancelButton
                            paddingValue: 20
                            height: parent.height
                            text.text: qsTr("취소")
                            text.color: "green"
                            text.verticalAlignment: Text.AlignVCenter
                            text.horizontalAlignment: Text.AlignHCenter

                            onClicked: {
                                exit()
                            }
                        }
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        updateTitle()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}

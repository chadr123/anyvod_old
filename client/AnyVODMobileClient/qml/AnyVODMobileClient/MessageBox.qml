﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtGraphicalEffects 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property alias title: title.text
    property alias message: message.text
    property alias messageVAlign: message.verticalAlignment

    property bool useCancel: false

    signal goBack
    signal ok
    signal cancel

    function exit()
    {
        goBack()
        cancel()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    Rectangle {
        id: root
        anchors.centerIn: parent
        width: parent.width * 0.8
        height: parent.height * 0.8
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            Column {
                anchors.fill: parent

                Rectangle {
                    width: parent.width
                    height: 50

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "#1f88f9" }
                        GradientStop { position: 0.93; color: "#1f88f9" }
                        GradientStop { position: 1.0; color: "#1874d0" }
                    }

                    Row {
                        id: navigation
                        width: parent.width
                        height: parent.height

                        Rectangle {
                            id: spacer1
                            color: "transparent"
                            height: parent.height
                            width: 10
                        }

                        Text {
                            id: title
                            width: parent.width - spacer1.width
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            color: "white"
                            elide: Text.ElideMiddle
                            font.pixelSize: parent.height * 0.3
                        }
                    }
                }

                Rectangle {
                    id: itemRoot
                    width: parent.width
                    height: parent.height - navigation.height - spacer.height - okButton.height
                    color: "white"

                    Text {
                        id: message
                        anchors.fill: parent
                        anchors.topMargin: 5
                        anchors.bottomMargin: 5
                        anchors.leftMargin: 20
                        anchors.rightMargin: 10
                        font.pixelSize: 15
                        wrapMode: Text.Wrap
                        verticalAlignment: Text.AlignVCenter
                    }
                }

                Rectangle {
                    id: spacer
                    width: parent.width
                    height: 1
                    color: "gray"
                }

                Rectangle {
                    width: parent.width
                    height: 50

                    Row {
                        width: parent.width
                        height: parent.height
                        spacing: 10
                        layoutDirection: Qt.RightToLeft

                        ImageButton {
                            id: okButton
                            paddingValue: 20
                            height: parent.height
                            text.text: qsTr("확인")
                            text.color: "green"
                            text.verticalAlignment: Text.AlignVCenter
                            text.horizontalAlignment: Text.AlignHCenter

                            onClicked: {
                                goBack()
                                ok()
                            }
                        }

                        ImageButton {
                            id: cancelButton
                            visible: useCancel
                            paddingValue: 20
                            height: parent.height
                            text.text: qsTr("취소")
                            text.color: "green"
                            text.verticalAlignment: Text.AlignVCenter
                            text.horizontalAlignment: Text.AlignHCenter

                            onClicked: {
                                exit()
                            }
                        }
                    }
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}

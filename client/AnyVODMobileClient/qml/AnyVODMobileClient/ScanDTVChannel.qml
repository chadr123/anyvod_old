﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: top
    anchors.fill: parent
    focus: true
    color: "#88000000"

    property int currentAdapter: delegate.getCurrentAdapterValue()
    property int currentSystemType: delegate.getCurrentSystemTypeValue()
    property int currentCountry: delegate.getCurrentCountryValue()

    property alias title: title.text

    readonly property int settingListDefaultWidth: width * 0.8

    signal goBack

    function exit()
    {
        goBack()
    }

    function updateChannelRange()
    {
        var exists = false

        exists = delegate.setChannelCountry(currentCountry, currentSystemType) && delegate.getChannelCount() > 0

        if (exists)
        {
            var first = delegate.getChannelFirst()
            var last = delegate.getChannelLast()

            start.minimumValue = first
            start.maximumValue = last

            end.minimumValue = first
            end.maximumValue = last

            start.value = first
            end.value = last

            scan.enabled = true
            stop.enabled = false
        }
        else
        {
            start.minimumValue = 0
            start.maximumValue = 0

            end.minimumValue = 0
            end.maximumValue = 0

            start.value = 0
            end.value = 0

            scan.enabled = false
            stop.enabled = false
        }
    }

    function adjustButtons()
    {
        if (start.value <= end.value)
        {
            scan.enabled = true
            stop.enabled = false
        }
        else
        {
            scan.enabled = false
            stop.enabled = false
        }
    }

    function restoreControls()
    {
        stop.enabled = false
        scan.enabled = true
        adapter.enabled = true
        systemType.enabled = true
        start.enabled = true
        end.enabled = true
        country.enabled = true
        save.enabled = true
        signalStrength.value = 0
    }

    VirtualKeyboardPanel {
        id: keyboard
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    DTVDelegate {
        id: delegate

        DTVListModel {
            id: dtvListModel
            objectName: "dtvListModel"

            onScanFinished: {
                restoreControls()
            }

            onSignalStrength: {
                signalStrength.value = value
            }

            onScannedChannel: {
                curChannel.text = current.toString()
                foundChannelCount.text = total.toString()
            }

            onInitProgress: {
                progress.minimumValue = start
                progress.maximumValue = end
                progress.value = start
            }

            onSetProgress: {
                progress.value = value
            }
        }

        Component.onCompleted: {
            init()
        }
    }

    Component {
        id: adapters

        ComboItem {
            listWidth: Math.min(settingListDefaultWidth, 500)
            title: qsTr("어댑터")
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < delegate.getAdapterInfoCount(); i++)
                    {
                        var desc = delegate.getAdapterInfo(i)
                        var value = delegate.getAdapterInfoValue(i)

                        append({ "desc": desc, "value": value })
                    }

                    defaultValue = currentAdapter
                }
            }

            onItemSelected: {
                currentAdapter = delegate.getAdapterInfoValue(index)
            }
        }
    }

    Component {
        id: systemTypes

        ComboItem {
            listWidth: Math.min(settingListDefaultWidth, 300)
            title: qsTr("형식")
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < delegate.getSystemTypeInfoCount(); i++)
                    {
                        var desc = delegate.getSystemTypeInfoDesc(i)
                        var value = delegate.getSystemTypeInfoValue(i)

                        append({ "desc": desc, "value": value })
                    }

                    defaultValue = currentSystemType
                }
            }

            onItemSelected: {
                currentSystemType = delegate.getSystemTypeInfoValue(index)
                updateChannelRange()
            }
        }
    }

    Component {
        id: countries

        ComboItem {
            listWidth: Math.min(settingListDefaultWidth, 300)
            title: qsTr("국가")
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < delegate.getCountryInfoCount(); i++)
                    {
                        var desc = delegate.getCountryInfoDesc(i)
                        var value = delegate.getCountryInfoValue(i)

                        append({ "desc": desc, "value": value })
                    }

                    defaultValue = currentCountry
                }
            }

            onItemSelected: {
                currentCountry = delegate.getCountryInfoValue(index)
                updateChannelRange()
            }
        }
    }

    Rectangle {
        id: root
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        width: Math.min(parent.width * 0.8, 700)
        height: Math.min(parent.height * 0.9, 400)
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            Column {
                anchors.fill: parent

                Rectangle {
                    width: parent.width
                    height: 50

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "#1f88f9" }
                        GradientStop { position: 0.93; color: "#1f88f9" }
                        GradientStop { position: 1.0; color: "#1874d0" }
                    }

                    Row {
                        id: navigation
                        width: parent.width
                        height: parent.height

                        Rectangle {
                            id: spacer1
                            color: "transparent"
                            height: parent.height
                            width: 10
                        }

                        Text {
                            id: title
                            width: parent.width - spacer1.width
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            color: "white"
                            elide: Text.ElideMiddle
                            font.pixelSize: parent.height * 0.3
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    height: 50

                    Row {
                        id: menu
                        anchors.fill: parent

                        Rectangle {
                            id: spacerMenu1
                            color: "transparent"
                            height: parent.height
                            width: 15
                        }

                        Rectangle {
                            id: adaptersBox
                            width: adapter.width + 20
                            height: parent.height - 20
                            anchors.verticalCenter: parent.verticalCenter
                            color: "gray"

                            ImageButton {
                                id: adapter
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("어댑터")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter

                                onClicked: {
                                    comboLoader.sourceComponent = adapters
                                    comboLoader.forceActiveFocus()
                                }
                            }
                        }

                        Rectangle {
                            id: spacerMenu2
                            color: "transparent"
                            height: parent.height
                            width: 15
                        }

                        Rectangle {
                            id: systemTypesBox
                            width: systemType.width + 20
                            height: parent.height - 20
                            anchors.verticalCenter: parent.verticalCenter
                            color: "gray"

                            ImageButton {
                                id: systemType
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("형식")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter

                                onClicked: {
                                    comboLoader.sourceComponent = systemTypes
                                    comboLoader.forceActiveFocus()
                                }
                            }
                        }

                        Rectangle {
                            id: spacerMenu3
                            color: "transparent"
                            height: parent.height
                            width: 15
                        }

                        Rectangle {
                            id: countriesBox
                            width: country.width + 20
                            height: parent.height - 20
                            anchors.verticalCenter: parent.verticalCenter
                            color: "gray"

                            ImageButton {
                                id: country
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("국가")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter

                                onClicked: {
                                    comboLoader.sourceComponent = countries
                                    comboLoader.forceActiveFocus()
                                }
                            }
                        }

                        Rectangle {
                            id: spacerMenu4
                            color: "transparent"
                            height: parent.height
                            width: 20
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    height: 50

                    Row {
                        id: scanChannel
                        anchors.fill: parent

                        Rectangle {
                            color: "transparent"
                            width: 10
                            height: parent.height
                        }

                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("채널 범위")
                        }

                        Rectangle {
                            color: "transparent"
                            width: 5
                            height: parent.height
                        }

                        SpinBox {
                            id: start
                            anchors.verticalCenter: parent.verticalCenter
                            width: 70
                            height: 30
                            value: 0
                            minimumValue: 0
                            maximumValue: 0
                            style: SpinBoxStyle {
                                background: Rectangle {
                                    width: control.width
                                    height: control.height
                                    border.color: "gray"
                                }
                            }

                            onValueChanged: {
                                adjustButtons()
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            width: 5
                            height: parent.height
                        }

                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            text: "~"
                        }

                        Rectangle {
                            color: "transparent"
                            width: 5
                            height: parent.height
                        }

                        SpinBox {
                            id: end
                            anchors.verticalCenter: parent.verticalCenter
                            width: 70
                            height: 30
                            value: 0
                            minimumValue: 0
                            maximumValue: 0
                            style: SpinBoxStyle {
                                background: Rectangle {
                                    width: control.width
                                    height: control.height
                                    border.color: "gray"
                                }
                            }

                            onValueChanged: {
                                adjustButtons()
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            width: 20
                            height: parent.height
                        }

                        Rectangle {
                            id: scanBox
                            width: scan.width + 20
                            height: parent.height - 20
                            anchors.verticalCenter: parent.verticalCenter
                            color: "gray"

                            ImageButton {
                                id: scan
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("검색")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter

                                onClicked: {
                                    enabled = false
                                    stop.enabled = true

                                    adapter.enabled = false
                                    systemType.enabled = false
                                    start.enabled = false
                                    end.enabled = false
                                    country.enabled = false
                                    save.enabled = false

                                    signalStrength.value = 0
                                    curChannel.text = "0"
                                    foundChannelCount.text = "0"


                                    delegate.scan(currentAdapter, currentSystemType, currentCountry, start.value, end.value)
                                }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            width: 10
                            height: parent.height
                        }

                        Rectangle {
                            id: stopBox
                            width: scan.width + 20
                            height: parent.height - 20
                            anchors.verticalCenter: parent.verticalCenter
                            color: "gray"

                            ImageButton {
                                id: stop
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("중지")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter
                                enabled: false

                                onClicked: {
                                    delegate.stop()
                                    restoreControls()
                                }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            height: parent.height
                            width: 10
                        }

                        Rectangle {
                            id: saveBox
                            width: save.width + 20
                            height: parent.height - 20
                            anchors.verticalCenter: parent.verticalCenter
                            color: "gray"

                            ImageButton {
                                id: save
                                anchors.horizontalCenter: parent.horizontalCenter
                                height: parent.height
                                text.text: qsTr("저장")
                                text.color: "white"
                                text.verticalAlignment: Text.AlignVCenter
                                text.horizontalAlignment: Text.AlignHCenter

                                onClicked: {
                                    delegate.save()
                                    top.exit()
                                }
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            width: 10
                            height: parent.height
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    height: 50

                    Row {
                        id: scanningChannel
                        anchors.fill: parent

                        Rectangle {
                            color: "transparent"
                            width: 10
                            height: parent.height
                        }

                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("현재 채널 : ")
                        }

                        Text {
                            id: curChannel
                            anchors.verticalCenter: parent.verticalCenter
                            text: "0"
                        }

                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr(", 찾은 채널 개수 : ")
                        }

                        Text {
                            id: foundChannelCount
                            anchors.verticalCenter: parent.verticalCenter
                            text: "0"
                        }

                        Rectangle {
                            color: "transparent"
                            width: 40
                            height: parent.height
                        }

                        Text {
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("신호 감도")
                        }

                        Rectangle {
                            color: "transparent"
                            width: 5
                            height: parent.height
                        }

                        ProgressBar {
                            id: signalStrength
                            anchors.verticalCenter: parent.verticalCenter
                            height: 20
                            width: 150
                            minimumValue: 0
                            maximumValue: 100
                        }

                        Rectangle {
                            color: "transparent"
                            width: 10
                            height: parent.height
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    height: 20

                    Row {
                        id: progressPanel
                        anchors.fill: parent

                        Rectangle {
                            id: progressLeftSpacer
                            color: "transparent"
                            width: 10
                            height: parent.height
                        }

                        ProgressBar {
                            id: progress
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width - progressLeftSpacer.width - progressRightSpacer.width
                            height: parent.height
                            minimumValue: 0
                            maximumValue: 100
                        }

                        Rectangle {
                            id: progressRightSpacer
                            color: "transparent"
                            width: 10
                            height: parent.height
                        }
                    }
                }

                Rectangle {
                    id: listSpacer
                    width: parent.width
                    height: 10
                }

                Rectangle {
                    id: listRoot
                    width: parent.width
                    height: parent.height - navigation.height - menu.height - progressPanel.height -
                            scanningChannel.height - scanChannel.height - listSpacer.height -
                            closeSpacer.height - closeButton.height
                    color: "white"
                    clip: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        model: dtvListModel

                        delegate: Rectangle {
                            width: parent.width
                            height: 60

                            Rectangle {
                                id: itemRoot
                                anchors.fill: parent
                                anchors.leftMargin: 20
                                anchors.rightMargin: 20
                                color: "white"

                                Column {
                                    anchors.fill: parent

                                    Text {
                                        text: qsTr("채널 ") + model.channel + qsTr(", 신호 강도 ") + model.signalStrength + "%"
                                    }

                                    Text {
                                        text: model.name === "" ? qsTr("이름 없음") : model.name
                                        color: "red"
                                    }
                                }
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                Rectangle {
                    id: closeSpacer
                    width: parent.width
                    height: 1
                    color: "gray"
                }

                Rectangle {
                    id: closeButton
                    width: parent.width
                    height: 50

                    ImageButton {
                        anchors.right: parent.right
                        anchors.rightMargin: 20
                        height: parent.height
                        text.text: qsTr("닫기")
                        text.color: "green"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignRight

                        onClicked: {
                            exit()
                        }
                    }
                }
            }
        }
    }

    Loader {
        id: comboLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10

        function exit()
        {
            root.forceActiveFocus()

            opacity = 0.0
            comboHideTimer.restart()
        }

        Timer {
            id: comboHideTimer
            interval: comboOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: comboOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: comboLoader.item

            onGoBack: {
                comboLoader.exit()
            }
        }
    }

    Component.onCompleted: {
        updateChannelRange()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            top.exit()
        }
    }
}

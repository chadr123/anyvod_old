﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import com.dcple.anyvod 1.0

Item {
    id: top
    anchors.fill: parent
    focus: true

    property string dtvPath
    property bool playDTV: false
    property alias title: title.text

    signal goBack

    function exit()
    {
        goBack()
    }

    function updateNoItem()
    {
        noItems.visible = dtvListModel.rowCount() === 0
    }

    DTVDelegate {
        id: delegate

        DTVListModel {
            id: dtvListModel
            objectName: "dtvListModel"

            onModelReset: {
                updateNoItem()
            }
        }

        Component.onCompleted: {
            init()
        }
    }

    Column {
        id: root
        anchors.fill: parent

        Rectangle {
            width: parent.width
            height: 50

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#1f88f9" }
                GradientStop { position: 0.93; color: "#1f88f9" }
                GradientStop { position: 1.0; color: "#1874d0" }
            }

            Row {
                id: navigation
                width: parent.width
                height: parent.height

                Rectangle {
                    id: spacer1
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                ImageButton {
                    id: back
                    width: 30
                    height: parent.height
                    image.width: 20
                    image.height: image.width
                    source: "assets/back.png"
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: {
                        top.exit()
                    }

                }

                Rectangle {
                    id: spacer2
                    color: "transparent"
                    height: parent.height
                    width: 0
                }

                Text {
                    id: title
                    width: parent.width - back.width - spacer1.width - spacer2.width
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    elide: Text.ElideMiddle
                    font.pixelSize: parent.height * 0.3
                }
            }
        }

        Rectangle {
            id: listRoot
            width: parent.width
            height: parent.height - navigation.height
            color: "white"
            clip: true

            Rectangle {
                anchors.fill: parent
                color: "#f8f8f8"

                ListView {
                    id: list
                    anchors.fill: parent
                    model: dtvListModel

                    delegate: Rectangle {
                        id: itemRoot
                        width: parent.width
                        height: 60

                        MouseArea {
                            anchors.verticalCenter: parent.verticalCenter
                            width: parent.width
                            height: parent.height - 10

                            Rectangle {
                                anchors.fill: parent
                                anchors.leftMargin: 20
                                anchors.rightMargin: 20
                                color: "transparent"

                                Column {
                                    anchors.fill: parent

                                    Text {
                                        text: qsTr("채널 ") + model.channel + qsTr(", 신호 강도 ") + model.signalStrength + "%"
                                    }

                                    Text {
                                        text: model.name === "" ? qsTr("이름 없음") : model.name
                                        color: "red"
                                    }
                                }
                            }

                            onClicked: {
                                dtvPath = delegate.getPath(model.index)
                                playDTV = true
                                exit()
                            }

                            onPressed: {
                                itemRoot.color = "lightgray"
                                itemRoot.opacity = 0.5
                            }

                            onCanceled: {
                                itemRoot.color = listRoot.color
                                itemRoot.opacity = 1.0
                            }

                            onReleased: {
                                onCanceled()
                            }
                        }

                        Rectangle {
                            anchors.bottom: parent.bottom
                            width: parent.width
                            height: 1
                            color: model.index === list.count - 1 ? "white" : "lightgray"
                        }
                    }
                }

                Text {
                    id: noItems
                    anchors.fill: parent
                    visible: false
                    text: qsTr("채널 목록이 없습니다.")
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                Scrollbar {
                    id: scrollbar
                    flickable: list
                }
            }
        }
    }

    Loader {
        id: comboLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10

        function exit()
        {
            root.forceActiveFocus()

            opacity = 0.0
            comboHideTimer.restart()
        }

        Timer {
            id: comboHideTimer
            interval: comboOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: comboOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: comboLoader.item

            onGoBack: {
                comboLoader.exit()
            }
        }
    }

    Component.onCompleted: {
        updateNoItem()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            top.exit()
        }
    }
}

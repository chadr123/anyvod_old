﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property alias windowWidth: root.width
    property alias windowHeight: root.height
    property alias title: title.text

    signal goBack
    signal accepted(string path)
    signal ignore

    function exit()
    {
        goBack()
        ignore()
    }

    VirtualKeyboardPanel {
        id: keyboard
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: exit()
        onCanceled: exit()
    }

    SearchMediaModel {
        id: searchMediaModel
    }

    Component {
        id: noMedia

        MessageBox {
            windowHeight: 150
            title: qsTr("정보")
            message: qsTr("검색 결과가 없습니다.")
        }
    }

    Rectangle {
        id: root
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 10
        width: parent.width * 0.95
        height: parent.height * 0.9
        radius: 3
        border.color: "gray"

        MouseArea {
            anchors.fill: parent

            Column {
                anchors.fill: parent

                Rectangle {
                    width: parent.width
                    height: 50

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "#1f88f9" }
                        GradientStop { position: 0.93; color: "#1f88f9" }
                        GradientStop { position: 1.0; color: "#1874d0" }
                    }

                    Row {
                        id: navigation
                        width: parent.width
                        height: parent.height

                        Rectangle {
                            id: spacer1
                            color: "transparent"
                            height: parent.height
                            width: 10
                        }

                        Text {
                            id: title
                            width: parent.width - spacer1.width
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            color: "white"
                            elide: Text.ElideMiddle
                            font.pixelSize: parent.height * 0.3
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    height: 50

                    ContextMenuTextInput {
                        id: searchText
                        anchors.fill: parent
                        anchors.topMargin: 10
                        anchors.bottomMargin: 5
                        anchors.leftMargin: 10
                        anchors.rightMargin: 10
                        inputMethodHints: Qt.ImhNone

                        onFinished: {
                            if (!searchMediaModel.search(fileListModel.primaryPath, text))
                            {
                                messageBoxLoader.sourceComponent = noMedia
                                messageBoxLoader.forceActiveFocus()
                            }
                        }
                    }
                }

                Rectangle {
                    id: searchRoot
                    width: parent.width
                    height: parent.height - navigation.height - searchText.height - spacer.height - closeButton.height
                    color: "white"
                    clip: true

                    ListView {
                        id: list
                        anchors.fill: parent
                        model: searchMediaModel

                        delegate: Rectangle {
                            id: itemRoot
                            width: parent.width
                            height: 38
                            color: searchRoot.color

                            Rectangle {
                                x: 10
                                width: parent.width - x
                                height: parent.height
                                color: "transparent"

                                MouseArea {
                                    id: mouseArea
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.fill: parent

                                    Row {
                                        anchors.fill: parent
                                        spacing: 10

                                        Rectangle {
                                            id: thumbnailRoot
                                            anchors.verticalCenter: parent.verticalCenter
                                            width: 30
                                            height: width
                                            color: "transparent"
                                            border.width: 1
                                            border.color: "#d0d0d0"

                                            Image {
                                                id: thumbnail
                                                width: parent.width - thumbnailRoot.border.width * 2
                                                height: parent.height - thumbnailRoot.border.width * 2
                                                anchors.verticalCenter: parent.verticalCenter
                                                anchors.horizontalCenter: parent.horizontalCenter
                                                source: model.thumbnail
                                                fillMode: Image.PreserveAspectCrop
                                            }
                                        }

                                        Text {
                                            width: parent.width - thumbnail.width - scrollbar.width - parent.spacing * 2
                                            height: parent.height
                                            verticalAlignment: Text.AlignVCenter
                                            font.pixelSize: parent.height * 0.38
                                            elide: Text.ElideMiddle
                                            text: model.fileName
                                        }
                                    }

                                    onClicked: {
                                        goBack()
                                        accepted(model.path)
                                    }

                                    onPressed: {
                                        itemRoot.color = "lightgray"
                                        itemRoot.opacity = 0.5
                                    }

                                    onCanceled: {
                                        itemRoot.color = searchRoot.color
                                        itemRoot.opacity = 1.0
                                    }

                                    onReleased: {
                                        onCanceled()
                                    }
                                }
                            }
                        }
                    }

                    Scrollbar {
                        id: scrollbar
                        flickable: list
                    }
                }

                Rectangle {
                    id: spacer
                    width: parent.width
                    height: 1
                    color: "gray"
                }

                Rectangle {
                    width: parent.width
                    height: 50

                    Row {
                        width: parent.width
                        height: parent.height
                        spacing: 10
                        layoutDirection: Qt.RightToLeft

                        ImageButton {
                            id: closeButton
                            paddingValue: 20
                            height: parent.height
                            text.text: qsTr("닫기")
                            text.color: "green"
                            text.verticalAlignment: Text.AlignVCenter
                            text.horizontalAlignment: Text.AlignHCenter

                            onClicked: {
                                exit()
                            }
                        }
                    }
                }
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        property Item focusParent: null
        property string text

        function exit()
        {
            if (focusParent != null)
            {
                focusParent.forceActiveFocus()
                focusParent = null
            }

            opacity = 0.0
            messageBoxHideTimer.restart()
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            onGoBack: {
                messageBoxLoader.exit()
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            exit()
        }
    }
}

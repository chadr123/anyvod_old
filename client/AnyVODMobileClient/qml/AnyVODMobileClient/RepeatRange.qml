﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    anchors.fill: parent
    opacity: 0.0
    visible: false
    z: 10
    color: "transparent"

    property MediaDelegate media: null
    property alias windowWidth: root.width
    property alias windowHeight: root.height

    function show(toX, toY)
    {
        move(toX, toY)
        visible = true
        opacity = 1.0
    }

    function move(toX, toY)
    {
        root.x = toX
        root.y = toY
    }

    function hide()
    {
        opacity = 0.0
        hideTimer.restart()
    }

    Timer {
        id: hideTimer
        interval: opacityAni.duration

        onTriggered: {
            visible = false
        }
    }

    Behavior on opacity {
        NumberAnimation {
            id: opacityAni
            duration: 200
            easing.type: Easing.OutQuad
        }
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: topRoot.hide()
        onCanceled: topRoot.hide()
    }

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    Rectangle {
        id: root
        color: "#7f000000"
        radius: 3

        function getTimeString(time, defaultValue)
        {
            return time === 0.0 ? defaultValue : media.getTimeStringWithMSec(time)
        }

        Column {
            anchors.fill: parent
            spacing: 5

            Rectangle {
                id: titleSpacer
                color: "transparent"
                width: parent.width
                height: 0.1
            }

            Text {
                id: title
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 5
                anchors.rightMargin: 5
                font.pixelSize: 12
                verticalAlignment: Text.AlignVCenter
                text: qsTr("구간 반복 설정")
                color: "white"
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width - anchors.leftMargin - anchors.rightMargin
                height: parent.height - title.height - titleSpacer.height - parent.spacing * 3
                spacing: 5

                Rectangle {
                    id: spacer1
                    color: "transparent"
                    width: 0.1
                    height: parent.height
                }

                Rectangle {
                    id: startBox
                    anchors.verticalCenter: parent.verticalCenter
                    width: start.width + 40
                    height: parent.height
                    color: "gray"

                    ImageButton {
                        id: start
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: parent.height
                        text.font.pixelSize: 15
                        text.text: root.getTimeString(media.getSetting(AnyVODEnums.SK_REPEAT_RANGE_START), qsTr("시작"))
                        text.color: "white"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignHCenter

                        onClicked: {
                            var start = media.getSetting(AnyVODEnums.SK_REPEAT_RANGE_START)
                            var end = media.getSetting(AnyVODEnums.SK_REPEAT_RANGE_END)

                            if (start === 0.0)
                                media.setSetting(AnyVODEnums.SK_REPEAT_RANGE_START, true)
                            else
                                media.setSetting(AnyVODEnums.SK_REPEAT_RANGE_START, false)

                            start = media.getSetting(AnyVODEnums.SK_REPEAT_RANGE_START)
                            media.setSetting(AnyVODEnums.SK_REPEAT_RANGE_ENABLE, start !== 0.0 && end !== 0.0)

                            text.text = root.getTimeString(start, qsTr("시작"))
                        }
                    }
                }

                Text {
                    text: "~"
                    color: "white"
                    width: parent.width - startBox.width - endBox.width -
                           spacer1.width - spacer2.width - parent.spacing * 4
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                Rectangle {
                    id: endBox
                    anchors.verticalCenter: parent.verticalCenter
                    width: end.width + 40
                    height: parent.height
                    color: "gray"

                    ImageButton {
                        id: end
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: parent.height
                        text.font.pixelSize: 15
                        text.text: root.getTimeString(media.getSetting(AnyVODEnums.SK_REPEAT_RANGE_END), qsTr("종료"))
                        text.color: "white"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignHCenter

                        onClicked: {
                            var start = media.getSetting(AnyVODEnums.SK_REPEAT_RANGE_START)
                            var end = media.getSetting(AnyVODEnums.SK_REPEAT_RANGE_END)

                            if (end === 0.0)
                                media.setSetting(AnyVODEnums.SK_REPEAT_RANGE_END, true)
                            else
                                media.setSetting(AnyVODEnums.SK_REPEAT_RANGE_END, false)

                            end = media.getSetting(AnyVODEnums.SK_REPEAT_RANGE_END)
                            media.setSetting(AnyVODEnums.SK_REPEAT_RANGE_ENABLE, start !== 0.0 && end !== 0.0)

                            text.text = root.getTimeString(end, qsTr("종료"))
                        }
                    }
                }

                Rectangle {
                    id: spacer2
                    color: "transparent"
                    width: 0.1
                    height: parent.height
                }
            }
        }
    }
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtQuick.Window 2.2
import com.dcple.anyvod 1.0

Item {
    focus: true

    property bool willExit: false

    readonly property int settingListDefaultHeight: height * 0.8
    readonly property int settingListDefaultWidth: width * 0.8

    Component {
        id: captureExt

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("캡처 확장자")
            query: fileListModel
            type: AnyVODEnums.SK_SELECT_CAPTURE_EXT_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < fileListModel.getCaptureExtCount(); i++)
                    {
                        var desc = fileListModel.getCaptureExt(i)

                        append({ "desc": desc, "value": desc })
                    }
                }
            }
        }
    }

    Component {
        id: font

        SettingList {
            title: qsTr("글꼴")
            query: fileListModel
            type: AnyVODEnums.SK_FONT
            model: ListModel {
                Component.onCompleted: {
                    var fonts = fileListModel.fontFamilies

                    for (var i = 0; i < fonts.length; i++)
                    {
                        var desc = fonts[i]

                        append({ "desc": desc, "value": desc , "font": desc})
                    }
                }
            }

            onItemSelected: {
                messageBoxLoader.activeItem = settingLoader
                messageBoxLoader.sourceComponent = restartFont
                messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: deinterlaceMethod

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("디인터레이스 활성화 기준")
            query: fileListModel
            type: AnyVODEnums.SK_DEINTERLACE_METHOD_ORDER
            model: ListModel {
                ListElement { desc: qsTr("자동"); value: AnyVODEnums.DM_AUTO }
                ListElement { desc: qsTr("사용"); value: AnyVODEnums.DM_USE }
                ListElement { desc: qsTr("사용 안 함"); value: AnyVODEnums.DM_NOUSE }
            }
        }
    }

    Component {
        id: deinterlaceAlgorithm

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 423)
            title: qsTr("디인터레이스 알고리즘")
            query: fileListModel
            type: AnyVODEnums.SK_DEINTERLACE_ALGORITHM_ORDER
            model: ListModel {
                ListElement { desc: qsTr("Blend"); value: AnyVODEnums.DA_BLEND }
                ListElement { desc: qsTr("BOB"); value: AnyVODEnums.DA_BOB }
                ListElement { desc: qsTr("YADIF"); value: AnyVODEnums.DA_YADIF }
                ListElement { desc: qsTr("YADIF(BOB)"); value: AnyVODEnums.DA_YADIF_BOB }
                ListElement { desc: qsTr("Weston 3 Field"); value: AnyVODEnums.DA_W3FDIF }
                ListElement { desc: qsTr("Adaptive Kernel"); value: AnyVODEnums.DA_KERNDEINT }
                ListElement { desc: qsTr("Motion Compensation"); value: AnyVODEnums.DA_MCDEINT }
                ListElement { desc: qsTr("Bob Weaver"); value: AnyVODEnums.DA_BWDIF }
                ListElement { desc: qsTr("Bob Weaver(BOB)"); value: AnyVODEnums.DA_BWDIF_BOB }
            }
        }
    }

    Component {
        id: subtitleHAlign

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 331)
            title: qsTr("자막 / 가사 가로 정렬")
            query: fileListModel
            type: AnyVODEnums.SK_SUBTITLE_HALIGN_ORDER
            model: ListModel {
                ListElement { desc: qsTr("기본 정렬"); value: AnyVODEnums.HAM_NONE }
                ListElement { desc: qsTr("자동 정렬"); value: AnyVODEnums.HAM_AUTO }
                ListElement { desc: qsTr("왼쪽 정렬"); value: AnyVODEnums.HAM_LEFT }
                ListElement { desc: qsTr("가운데 정렬"); value: AnyVODEnums.HAM_MIDDLE }
                ListElement { desc: qsTr("오른쪽 정렬"); value: AnyVODEnums.HAM_RIGHT }
            }
        }
    }

    Component {
        id: subtitleVAlign

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("자막 / 가사 세로 정렬")
            query: fileListModel
            type: AnyVODEnums.SK_SUBTITLE_VALIGN_ORDER
            model: ListModel {
                ListElement { desc: qsTr("기본 정렬"); value: AnyVODEnums.VAM_NONE }
                ListElement { desc: qsTr("상단 정렬"); value: AnyVODEnums.VAM_TOP }
                ListElement { desc: qsTr("하단 정렬"); value: AnyVODEnums.VAM_BOTTOM }
            }
        }
    }

    Component {
        id: subtitleEncoding

        SettingList {
            title: qsTr("자막 / 가사 텍스트 인코딩")
            query: fileListModel
            type: AnyVODEnums.SK_TEXT_ENCODING
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < fileListModel.getTextCodecCount(); i++)
                    {
                        var desc = fileListModel.getTextCodecName(i)

                        append({ "desc": desc, "value": desc })
                    }
                }
            }
        }
    }

    Component {
        id: lang

        SettingList {
            title: qsTr("Languages")
            query: fileListModel
            type: AnyVODEnums.SK_LANG
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < fileListModel.getLanguageCount(); i++)
                    {
                        var value = fileListModel.getLanguageValue(i)
                        var desc = fileListModel.getLanguageDesc(value)

                        append({ "desc": desc, "value": value })
                    }
                }
            }

            onItemSelected: {
                messageBoxLoader.activeItem = settingLoader
                messageBoxLoader.sourceComponent = restartLang
                messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: audioDevice

        SettingList {
            title: qsTr("오디오 장치")
            query: fileListModel
            type: AnyVODEnums.SK_AUDIO_DEVICE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var list = fileListModel.getAudioDevices();

                    append({ "desc": qsTr("기본 장치"), "value": -1 })

                    for (var i = 0; i < list.length; i++)
                    {
                        var desc = list[i]

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: spdifAudioDevice

        SettingList {
            title: qsTr("출력 장치")
            query: fileListModel
            type: AnyVODEnums.SK_SPDIF_AUDIO_DEVICE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var list = fileListModel.getSPDIFAudioDevices();

                    append({ "desc": qsTr("기본 장치"), "value": -1 })

                    for (var i = 0; i < list.length; i++)
                    {
                        var desc = list[i]

                        desc = desc.replace(/\n/gi, " ")

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: spdifEncoding

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("인코딩")
            query: fileListModel
            type: AnyVODEnums.SK_USE_SPDIF_ENCODING_ORDER
            model: ListModel {
                ListElement { desc: qsTr("사용 안 함"); value: AnyVODEnums.SEM_NONE }
                ListElement { desc: qsTr("AC3"); value: AnyVODEnums.SEM_AC3 }
                ListElement { desc: qsTr("DTS"); value: AnyVODEnums.SEM_DTS }
            }
        }
    }

    Component {
        id: spdifSampleRate

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("샘플링 속도")
            query: fileListModel
            type: AnyVODEnums.SK_SPDIF_USER_SAMPLE_RATE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < fileListModel.getUserSPDIFSampleRateCount(); i++)
                    {
                        var desc = fileListModel.getUserSPDIFSampleRateDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: equalizer

        Equalizer {
            title: qsTr("이퀄라이저")
            query: fileListModel
        }
    }

    Component {
        id: external

        OpenExternal {
            windowHeight: Math.min(settingListDefaultHeight, 200)
            title: qsTr("외부 열기")

            onAccepted: {
                if (url.length > 0)
                    root.openMedia(url)
            }
        }
    }

    Component {
        id: notSupported

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("지원하지 않는 기능입니다.")
        }
    }

    Component {
        id: failedPlayBox

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("파일을 열 수 없습니다.")
        }
    }

    Component {
        id: failedDeleteFileBox

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("파일을 삭제 할 수 없습니다.")
        }
    }

    Component {
        id: failedDeleteDirectoryBox

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("디렉토리를 삭제 할 수 없습니다.")
        }
    }

    Component {
        id: restartFont

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("변경된 글꼴로 적용하기 위해서는 앱을 재 시작 해주세요.")
        }
    }

    Component {
        id: restartLang

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("Info")
            message: qsTr("Restart the app to apply changes.")
        }
    }

    Component {
        id: confirmExit

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("확인")
            message: qsTr("종료 하시겠습니까?")
            useCancel: true

            onOk: {
                Qt.quit()
            }
        }
    }

    Component {
        id: confirmDirectoryDelete

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("확인")
            message: qsTr("삭제 하시겠습니까?")
            useCancel: true

            onOk: {
                if (fileListModel.deletePath(messageBoxLoader.path))
                    reload.clicked()
                else
                    messageBoxSecondLoader.sourceComponent = failedDeleteDirectoryBox

                if (messageBoxSecondLoader.sourceComponent != undefined)
                    messageBoxSecondLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: confirmFileDelete

        MessageBox {
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("확인")
            message: qsTr("삭제 하시겠습니까?")
            useCancel: true

            onOk: {
                if (fileListModel.deletePath(messageBoxLoader.path))
                    reload.clicked()
                else
                    messageBoxSecondLoader.sourceComponent = failedDeleteFileBox

                if (messageBoxSecondLoader.sourceComponent != undefined)
                    messageBoxSecondLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: info

        SettingTextViewer {
            title: qsTr("AnyVOD 정보")
            desc.text: fileListModel.getVersion()
            desc.wrapMode: TextEdit.Wrap
            flickDir: Flickable.VerticalFlick
        }
    }

    Component {
        id: license

        SettingTextViewer {
            title: qsTr("라이센스")
            desc.text: fileListModel.getLicenses()
            desc.font.pixelSize: 12
        }
    }

    Component {
        id: openCaptureSaveDir

        FileSystemDialog {
            fileListObjName: "openCaptureSaveDir"
            title: qsTr("저장 디렉토리 설정")
            onlyDirectory: true
            startDirectory: fileListModel.getSetting(AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY)

            onAccepted: {
                fileListModel.setSetting(AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY, selectedPath)
            }
        }
    }

    Component {
        id: settings

        SettingDialog {
            title: qsTr("설정")
            model: MainSettingItems {}
            query: fileListModel

            onNotSupportedFunction: {
                messageBoxLoader.sourceComponent = notSupported
                messageBoxLoader.forceActiveFocus()
            }

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_INFO:
                        settingDlgLoader.sourceComponent = info
                        break
                    case AnyVODEnums.SK_LICENSE:
                        settingDlgLoader.sourceComponent = license
                        break
                    case AnyVODEnums.SK_LANG:
                        settingDlgLoader.sourceComponent = lang
                        break
                    case AnyVODEnums.SK_FONT:
                        settingDlgLoader.sourceComponent = font
                        break
                    case AnyVODEnums.SK_SELECT_CAPTURE_EXT_ORDER:
                        settingDlgLoader.sourceComponent = captureExt
                        break
                    case AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY:
                        settingDlgLoader.sourceComponent = openCaptureSaveDir
                        break
                    case AnyVODEnums.SK_DEINTERLACE_METHOD_ORDER:
                        settingDlgLoader.sourceComponent = deinterlaceMethod
                        break
                    case AnyVODEnums.SK_DEINTERLACE_ALGORITHM_ORDER:
                        settingDlgLoader.sourceComponent = deinterlaceAlgorithm
                        break
                    case AnyVODEnums.SK_SUBTITLE_HALIGN_ORDER:
                        settingDlgLoader.sourceComponent = subtitleHAlign
                        break
                    case AnyVODEnums.SK_SUBTITLE_VALIGN_ORDER:
                        settingDlgLoader.sourceComponent = subtitleVAlign
                        break
                    case AnyVODEnums.SK_TEXT_ENCODING:
                        settingDlgLoader.sourceComponent = subtitleEncoding
                        break
                    case AnyVODEnums.SK_AUDIO_DEVICE_ORDER:
                        settingDlgLoader.sourceComponent = audioDevice
                        break
                    case AnyVODEnums.SK_SPDIF_AUDIO_DEVICE_ORDER:
                        if (Qt.platform.os === "linux")
                            settingDlgLoader.sourceComponent = spdifAudioDevice
                        else
                            messageBoxLoader.sourceComponent = notSupported

                        break
                    case AnyVODEnums.SK_USE_SPDIF_ENCODING_ORDER:
                        if (Qt.platform.os === "linux")
                            settingDlgLoader.sourceComponent = spdifEncoding
                        else
                            messageBoxLoader.sourceComponent = notSupported

                        break
                    case AnyVODEnums.SK_SPDIF_USER_SAMPLE_RATE_ORDER:
                        if (Qt.platform.os === "linux")
                            settingDlgLoader.sourceComponent = spdifSampleRate
                        else
                            messageBoxLoader.sourceComponent = notSupported

                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: scanDTV

        ScanDTVChannel {
            title: qsTr("채널 검색")
        }
    }

    Component {
        id: dtvChannelList

        DTVChannelList {
            title: qsTr("채널 목록")

            onGoBack: {
                if (playDTV)
                {
                    root.openMedia(dtvPath)
                    settingLoader.exit()
                }
            }
        }
    }

    Component {
        id: openDTV

        SettingDialog {
            title: qsTr("DTV 열기")
            model: DTVSettingItems {}
            query: fileListModel

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_OPEN_DTV_CHANNEL_LIST:
                        settingDlgLoader.sourceComponent = dtvChannelList
                        break
                    case AnyVODEnums.SK_OPEN_DTV_SCAN_CHANNEL:
                        settingDlgLoader.sourceComponent = scanDTV
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: managePlayList

        ManagePlayList {
            title: qsTr("재생 목록")

            onSelected: {
                root.openPlayList(name)
            }
        }
    }

    Component {
        id: searchMedia

        SearchMedia {
            title: qsTr("검색")

            onAccepted: {
                root.openMedia(path)
            }
        }
    }

    Menus {
        id: mainMenu
        menuWidth: 160
        menuHeight: 240
        model: MainMenuItems {}

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_EQUALIZER:
                    settingLoader.sourceComponent = equalizer
                    break
                case AnyVODEnums.MID_OPEN_EXTERNAL:
                    settingLoader.hideParent = false
                    settingLoader.sourceComponent = external
                    break
                case AnyVODEnums.MID_OPEN_DTV:
                    if (Qt.platform.os === "linux")
                        settingLoader.sourceComponent = openDTV
                    else
                        messageBoxLoader.sourceComponent = notSupported

                    break
                case AnyVODEnums.MID_SETTINGS:
                    settingLoader.sourceComponent = settings
                    break
                case AnyVODEnums.MID_SEARCH:
                    settingLoader.sourceComponent = searchMedia
                    break
                case AnyVODEnums.MID_MANAGE_PLAY_LIST:
                    settingLoader.sourceComponent = managePlayList
                    break
            }

            if (settingLoader.sourceComponent != undefined)
                settingLoader.forceActiveFocus()

            if (messageBoxLoader.sourceComponent != undefined)
                messageBoxLoader.forceActiveFocus()
        }
    }

    Menus {
        id: sortMenu
        menuWidth: 180
        menuHeight: 330
        model: SortMenuItems {}

        onMenuItemSelected: {
            var item = model.get(index)

            fileListModel.setSortType(item.menuID)
            root.updateCurrentFileList()
        }
    }

    Menus {
        id: dirItemMenu
        menuWidth: 120
        menuHeight: 80
        model: DirItemMenuItems {}

        property string path

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_PLAY:
                    visible = false
                    root.openMedia(path)

                    break
                case AnyVODEnums.MID_DELETE:
                    messageBoxLoader.path = path
                    messageBoxLoader.sourceComponent = confirmDirectoryDelete

                    break
            }

            if (messageBoxLoader.sourceComponent != undefined)
                messageBoxLoader.forceActiveFocus()
        }
    }

    Menus {
        id: fileItemMenu
        menuWidth: 80
        menuHeight: 40
        model: FileItemMenuItems {}

        property string path

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_DELETE:
                    messageBoxLoader.path = path
                    messageBoxLoader.sourceComponent = confirmFileDelete

                    break
            }

            if (messageBoxLoader.sourceComponent != undefined)
                messageBoxLoader.forceActiveFocus()
        }
    }

    Column {
        id: root
        anchors.fill: parent

        Behavior on opacity {
            NumberAnimation {
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        Rectangle {
            width: parent.width
            height: 50

            gradient: Gradient {
                GradientStop { position: 0.0; color: "#1f88f9" }
                GradientStop { position: 0.93; color: "#1f88f9" }
                GradientStop { position: 1.0; color: "#1874d0" }
            }

            Row {
                id: navigation
                width: parent.width
                height: parent.height

                Rectangle {
                    id: spacer1
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                ImageButton {
                    id: titleIcon
                    width: 30
                    height: width
                    image.width: 20
                    image.height: image.width
                    source: "assets/icon.png"
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: {
                        messageBoxLoader.sourceComponent = confirmExit
                        messageBoxLoader.forceActiveFocus()
                    }
                }

                ImageButton {
                    id: back
                    width: 30
                    height: parent.height
                    image.width: 20
                    image.height: image.width
                    source: "assets/back.png"
                    anchors.verticalCenter: parent.verticalCenter
                    visible: false

                    signal goBack

                    onClicked: {
                        goBack()
                    }

                    onGoBack: {
                        var contY = fileListModel.cdup()

                        fileListModel.update()

                        var maxHeight = storage.contentHeight - storage.height

                        if (maxHeight < 0.0)
                            maxHeight = storage.height

                        if (contY < 0.0)
                            contY = 0.0
                        else if (contY > maxHeight)
                            contY = maxHeight

                        storage.contentY = contY

                        scrollbar.showScrollbar()
                        scrollbar.hideScrollbar()

                        root.hideMenus()
                        root.setTitleText(fileListModel.curDirName)

                        if (fileListModel.curDirName === fileListModel.topSignature)
                            root.resetToTop()
                    }
                }

                Rectangle {
                    id: spacer2
                    color: "transparent"
                    height: parent.height
                    width: 0
                }

                MarqueeText {
                    id: title
                    width: parent.width - back.width - reload.width - sort.width - menu.width -
                           spacer1.width - spacer2.width - spacer3.width - spacer4.width - spacer5.width - spacer6.width
                    height: parent.height
                    text.verticalAlignment: Text.AlignVCenter
                    text.text: fileListModel.topSignature
                    text.color: "white"
                    text.font.pixelSize: parent.height * 0.3
                }

                Rectangle {
                    id: spacer3
                    color: "transparent"
                    height: parent.height
                    width: 7
                }

                ImageButton {
                    id: reload
                    width: 40
                    height: parent.height
                    image.width: 20
                    image.height: image.width
                    image.x: 10
                    source: "assets/reload.png"
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: {
                        root.updateCurrentFileList()
                    }
                }

                Rectangle {
                    id: spacer4
                    color: "transparent"
                    height: parent.height
                    width: 0
                }

                ImageButton {
                    id: sort
                    width: 40
                    height: parent.height
                    image.width: 20
                    image.height: image.width
                    image.x: 10
                    source: "assets/sort.png"
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: {
                        root.showSortMenu()
                    }
                }

                Rectangle {
                    id: spacer5
                    color: "transparent"
                    height: parent.height
                    width: 0
                }

                ImageButton {
                    id: menu
                    width: 40
                    height: parent.height
                    image.width: 20
                    image.height: image.width
                    image.x: 10
                    source: "assets/menu.png"
                    anchors.verticalCenter: parent.verticalCenter

                    onClicked: {
                        root.showMainMenu()
                    }
                }

                Rectangle {
                    id: spacer6
                    color: "transparent"
                    height: parent.height
                    width: 0
                }
            }
        }

        Rectangle {
            width: parent.width
            height: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? 1 : 40

            gradient: Gradient {
                GradientStop { position: 0.0; color: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? "#c9c9c9" : "#f0f0f0" }
                GradientStop { position: 0.98; color: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? "#c9c9c9" : "#f0f0f0" }
                GradientStop { position: 1.0; color: "#c9c9c9" }
            }

            Grid {
                id: quickbar
                visible: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? false : true
                width: primaryFrame.width + secondaryFrame.width + videoFrame.width + audioFrame.width
                height: parent.height
                anchors.horizontalCenter: parent.horizontalCenter
                columns: 4

                Rectangle {
                    id: primaryFrame
                    width: primary.width + 15
                    height: parent.height
                    color: "transparent"

                    ImageButton {
                        id: primary
                        height: parent.height / 2
                        spacing: 6
                        checked: false
                        image.width: height * 0.8
                        image.height: image.width
                        text.text: qsTr("내장메모리")
                        text.font.pixelSize: height * 0.6
                        anchors.centerIn: parent
                        source: "assets/internal.png"
                        checkedSource: "assets/internal_checked.png"
                        textColor: "black"
                        checkedTextColor: "#1f88f9"

                        onClicked: {
                            fileListModel.update(fileListModel.primaryPath)

                            root.resetToTop()
                            root.setTitleText(fileListModel.curDirName)

                            checked = true

                            video.checked = false
                            audio.checked = false
                            secondary.checked = false
                        }
                    }
                }

                Rectangle {
                    id: secondaryFrame
                    width: secondary.width + 15
                    height: parent.height
                    color: "transparent"

                    ImageButton {
                        id: secondary
                        height: parent.height / 2
                        spacing: primary.spacing
                        checked: false
                        image.width: primary.image.width
                        image.height: primary.image.height
                        text.text: qsTr("외장메모리")
                        text.font.pixelSize: primary.text.font.pixelSize
                        anchors.centerIn: parent
                        source: "assets/external.png"
                        checkedSource: "assets/external_checked.png"
                        textColor: "black"
                        checkedTextColor: "#1f88f9"

                        onClicked: {
                            fileListModel.update(fileListModel.secondaryPath)

                            root.resetToTop()
                            root.setTitleText(fileListModel.curDirName)

                            checked = true

                            video.checked = false
                            audio.checked = false
                            primary.checked = false
                        }
                    }
                }

                Rectangle {
                    id: videoFrame
                    width: video.width + 15
                    height: parent.height
                    color: "transparent"

                    ImageButton {
                        id: video
                        height: parent.height / 2
                        spacing: primary.spacing
                        checked: false
                        image.width: primary.image.width
                        image.height: primary.image.height
                        text.text: qsTr("동영상")
                        text.font.pixelSize: primary.text.font.pixelSize
                        anchors.centerIn: parent
                        source: "assets/video.png"
                        checkedSource: "assets/video_checked.png"
                        textColor: "black"
                        checkedTextColor: "#1f88f9"

                        onClicked: {
                            fileListModel.updateFromDatabase(FileListModel.Video)

                            root.resetToTop()
                            root.setTitleText(fileListModel.curDirName)

                            checked = true

                            audio.checked = false
                            primary.checked = false
                            secondary.checked = false
                        }
                    }
                }

                Rectangle {
                    id: audioFrame
                    width: audio.width + 15
                    height: parent.height
                    color: "transparent"

                    ImageButton {
                        id: audio
                        height: parent.height / 2
                        spacing: primary.spacing
                        checked: false
                        image.width: primary.image.width
                        image.height: primary.image.height
                        text.text: qsTr("음악")
                        text.font.pixelSize: primary.text.font.pixelSize
                        anchors.centerIn: parent
                        source: "assets/audio.png"
                        checkedSource: "assets/audio_checked.png"
                        textColor: "black"
                        checkedTextColor: "#1f88f9"

                        onClicked: {
                            fileListModel.updateFromDatabase(FileListModel.Audio)

                            root.resetToTop()
                            root.setTitleText(fileListModel.curDirName)

                            checked = true

                            video.checked = false
                            primary.checked = false
                            secondary.checked = false
                        }
                    }
                }
            }
        }

        FileListModel {
            id: fileListModel
            objectName: "mainFileList"

            Component.onCompleted: {
                init()

                switch (fileListModel.quickBarType)
                {
                    case FileListModel.InternalType:
                        primary.checked = true
                        break
                    case FileListModel.ExternalType:
                        secondary.checked = true
                        break
                    case FileListModel.VideoType:
                        video.checked = true
                        break
                    case FileListModel.AudioType:
                        audio.checked = true
                        break
                }
            }

            onModelReset: {
                noFiles.visible = rowCount() === 0
            }
        }

        Rectangle {
            id: storageRoot
            width: parent.width
            height: parent.height - navigation.height - quickbar.height
            color: "white"
            clip: true

            Rectangle {
                anchors.fill: parent
                color: "white"

                ListView {
                    id: storage
                    anchors.fill: parent
                    model: fileListModel

                    delegate: Rectangle {
                        id: itemRoot
                        width: parent.width
                        height: 69
                        color: storageRoot.color

                        Rectangle {
                            x: 10
                            width: parent.width - x
                            height: parent.height
                            color: "transparent"

                            MouseArea {
                                anchors.verticalCenter: parent.verticalCenter
                                width: parent.width
                                height: parent.height - 10

                                Row {
                                    anchors.fill: parent
                                    spacing: 10

                                    Rectangle {
                                        id: thumbnailRoot
                                        anchors.verticalCenter: parent.verticalCenter
                                        width: height * 1.33
                                        height: parent.height - 8
                                        color: "transparent"
                                        border.width: model.type === FileListModel.Directory ? 0 : 1
                                        border.color: model.type === FileListModel.Directory ? "transparent" : "#d0d0d0"

                                        Image {
                                            id: thumbnail
                                            width: parent.width - thumbnailRoot.border.width * 2
                                            height: parent.height - thumbnailRoot.border.width * 2
                                            anchors.verticalCenter: parent.verticalCenter
                                            anchors.horizontalCenter: parent.horizontalCenter
                                            source: thumbnailImage()
                                            fillMode: Image.Stretch

                                            function thumbnailImage()
                                            {
                                                if (model.type === FileListModel.Directory)
                                                {
                                                    if (model.dirEntryCount === 0)
                                                        return fileListModel.defaultDirectoryIcon
                                                    else
                                                        return fileListModel.openDirectoryIcon
                                                }
                                                else
                                                {
                                                    return model.thumbnail
                                                }
                                            }
                                        }
                                    }

                                    Column {
                                        anchors.verticalCenter: parent.verticalCenter
                                        width: parent.width - parent.spacing * 3 - thumbnailRoot.width - openItemMenu.width
                                        height: fileName.height + fileInfo.height + spacing * 1
                                        spacing: 1

                                        Text {
                                            id: fileName
                                            width: parent.width
                                            text: model.name
                                            font.pixelSize: itemRoot.height * 0.22
                                            elide: Text.ElideMiddle
                                        }

                                        Text {
                                            id: fileInfo
                                            width: fileName.width
                                            color: "red"
                                            text: model.fileInfo.length === 0 ? qsTr("갱신 중...") : model.type === FileListModel.Directory ? qsTr("%1 항목").arg(model.fileInfo) : model.fileInfo
                                            font.pixelSize: fileName.font.pixelSize
                                            elide: Text.ElideMiddle
                                        }
                                    }

                                    ImageButton {
                                        id: openItemMenu
                                        width: 30
                                        height: width
                                        image.width: width
                                        image.height: height
                                        source: "assets/itemMenu.png"
                                        anchors.verticalCenter: parent.verticalCenter

                                        onClicked: {
                                            if (model.type === FileListModel.Directory)
                                                root.showMediaItemMenu(dirItemMenu, this, model.path)
                                            else if (model.type === FileListModel.File)
                                                root.showMediaItemMenu(fileItemMenu, this, model.path)
                                        }
                                    }
                                }

                                onClicked: {
                                    if (fileListModel.isMedia(model.path))
                                    {
                                        root.openMedia(model.path)
                                    }
                                    else
                                    {
                                        titleIcon.visible = false
                                        back.visible = true

                                        root.setTitleText(model.name)

                                        fileListModel.cd(model.path, storage.contentY)
                                    }
                                }

                                onPressed: {
                                    itemRoot.color = "lightgray"
                                    itemRoot.opacity = 0.5
                                }

                                onCanceled: {
                                    itemRoot.color = storageRoot.color
                                    itemRoot.opacity = 1.0
                                }

                                onReleased: {
                                    onCanceled()
                                }
                            }
                        }
                    }
                }

                Scrollbar {
                    id: scrollbar
                    flickable: storage
                }
            }

            Rectangle {
                id: noFiles
                anchors.fill: parent
                visible: false

                Column {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: 10

                    Image {
                        id: noFileImg
                        width: 100
                        source: "assets/nofiles.png"
                        fillMode: Image.PreserveAspectFit
                    }

                    Text {
                        width: noFileImg.width
                        horizontalAlignment: Text.AlignHCenter
                        text: qsTr("파일이 없습니다")
                    }
                }
            }
        }

        function updateCurrentFileList()
        {
            switch (fileListModel.quickBarType)
            {
                case FileListModel.InternalType:
                case FileListModel.ExternalType:
                    fileListModel.update()
                    break
                case FileListModel.VideoType:
                    fileListModel.updateFromDatabase(FileListModel.Video)
                    break
                case FileListModel.AudioType:
                    fileListModel.updateFromDatabase(FileListModel.Audio)
                    break
            }
        }

        function showMainMenu()
        {
            var showX = menu.x - mainMenu.menuWidth + menu.width - 10
            var showY = menu.y + 10

            mainMenu.show(showX, showY)
        }

        function showSortMenu()
        {
            var showX = sort.x - sortMenu.menuWidth + sort.width - 10
            var showY = sort.y + 10

            sortMenu.show(showX, showY)
        }

        function showMediaItemMenu(menu, parent, path)
        {
            var globalCoord = parent.mapToItem(null, parent.x, parent.y)
            var showX = parent.x - menu.menuWidth + parent.width
            var showY = globalCoord.y - parent.height
            var offset = 10
            var maxHeight = showY + menu.menuHeight + offset

            if (maxHeight >= root.height)
                showY = root.height - (menu.menuHeight + offset)

            menu.path = path
            menu.show(showX, showY)
        }

        function hideMenus()
        {
            mainMenu.hide()
            dirItemMenu.hide()
            fileItemMenu.hide()
        }

        function resetToTop()
        {
            titleIcon.visible = true
            back.visible = false
            fileListModel.clearScrollPos()
        }

        function openMedia(filePath)
        {
            screenLoader.filePath = filePath
            screenLoader.source = "VideoScreen.qml"
            screenLoader.forceActiveFocus()
        }

        function openPlayList(name)
        {
            screenLoader.playListName = name
            screenLoader.source = "VideoScreen.qml"
            screenLoader.forceActiveFocus()
        }

        function setTitleText(text)
        {
            title.text.text = text
            title.restart()
        }
    }

    Loader {
        id: screenLoader
        anchors.fill: parent
        visible: false
        focus: true

        property string filePath
        property string playListName
        property FileListModel listModel: fileListModel
        property bool isFailed: false

        function exit()
        {
            source = ""
            filePath = ""
            playListName = ""

            root.visible = true
            root.opacity = 1.0

            title.restart()

            if (!isFailed)
                root.forceActiveFocus()

            visible = false
            isFailed = false
        }

        onLoaded: {
            root.visible = false
            root.opacity = 0.0

            visible = true
        }

        Connections {
            ignoreUnknownSignals: true
            target: screenLoader.item

            onGoBack: {
                if (screenLoader.isFailed)
                {
                    messageBoxLoader.sourceComponent = failedPlayBox
                    messageBoxLoader.forceActiveFocus()
                }

                screenLoader.exit()
            }

            onFailedPlay: {
                screenLoader.isFailed = true
            }
        }
    }

    Loader {
        id: settingDlgLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10

        function exit()
        {
            settingLoader.forceActiveFocus()

            opacity = 0.0
            settingDlgHideTimer.restart()
        }

        Timer {
            id: settingDlgHideTimer
            interval: settingDlgOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingDlgOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingDlgLoader.item

            onGoBack: {
                settingDlgLoader.exit()
            }
        }
    }

    Loader {
        id: settingLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        property bool hideParent: true

        function exit()
        {
            if (hideParent)
                root.visible = true

            root.forceActiveFocus()

            opacity = 0.0
            settingHideTimer.restart()
        }

        Timer {
            id: settingHideTimer
            interval: settingOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.hideParent = true

                parent.sourceComponent = undefined
            }
        }

        Timer {
            id: settingShowTimer
            interval: settingOpacityAni.duration

            onTriggered: {
                if (parent.hideParent)
                    root.visible = false
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0

            settingShowTimer.restart()
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingLoader.item

            onGoBack: {
                settingLoader.exit()
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        property Item activeItem: null
        property string path

        function exit()
        {
            if (activeItem == null)
                root.forceActiveFocus()
            else
                activeItem.forceActiveFocus()

            activeItem = null
            opacity = 0.0
            messageBoxHideTimer.restart()
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.visible = false

                if (parent.hideParent !== undefined)
                    parent.hideParent = true

                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            onGoBack: {
                messageBoxLoader.exit()
            }
        }
    }

    Loader {
        id: messageBoxSecondLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        property Item activeItem: null

        function exit()
        {
            if (activeItem == null)
                messageBoxLoader.forceActiveFocus()
            else
                activeItem.forceActiveFocus()

            activeItem = null
            opacity = 0.0
            messageBoxSecondHideTimer.restart()
        }

        Timer {
            id: messageBoxSecondHideTimer
            interval: messageBoxSecondOpacityAni.duration

            onTriggered: {
                parent.visible = false

                if (parent.hideParent !== undefined)
                    parent.hideParent = true

                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxSecondOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxSecondLoader.item

            onGoBack: {
                messageBoxSecondLoader.exit()
            }
        }
    }

    Timer {
        id: exitTimer

        onTriggered: {
            willExit = false
        }
    }

    Component.onCompleted: {
        fileListModel.setStatusBarStyle(true)
        fileListModel.hideSplashScreen()
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            if (fileListModel.curDirName === fileListModel.topSignature)
            {
                var toastLen = fileListModel.getToastLength()

                if (willExit || toastLen <= 0)
                {
                    Qt.quit()
                }
                else
                {
                    willExit = true

                    exitTimer.interval = toastLen
                    exitTimer.restart()

                    fileListModel.showToast(qsTr("뒤로 버튼을 한 번 더 누르시면 종료합니다."))
                }
            }
            else
            {
                back.goBack()
            }
        }
    }
}

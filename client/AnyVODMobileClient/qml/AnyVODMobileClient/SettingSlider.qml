﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import com.dcple.anyvod 1.0

Rectangle {
    id: topRoot
    focus: true
    color: "#88000000"

    property FileListModel query: null
    property MediaDelegate media: null
    property int type: -100
    property real roundValue: 10
    property real defaultValue: 0
    property real defaultStartValue: 0
    property real scaleValue: 1
    property string unitPostfix: ""
    property alias sliderWidth: root.width
    property alias sliderHeight: root.height
    property alias title: title.text
    property alias desc: desc.text
    property alias slider: sliderItem
    property alias label: sliderItemValue

    signal goBack

    DropShadow {
        anchors.fill: root
        radius: 16.0
        samples: 16
        color: "#80000000"
        source: root
    }

    MouseArea {
        anchors.fill: parent

        onWheel: wheel.accepted = true
        onClicked: goBack()
        onCanceled: goBack()
    }

    Rectangle {
        id: root
        anchors.centerIn: parent
        width: parent.width * 0.8
        height: Math.min(parent.height * 0.7, 210)
        radius: 3
        border.color: "gray"

        function getSliderValue()
        {
            var value = sliderItem.value * scaleValue + defaultStartValue

            return value.toFixed(roundValue) + unitPostfix
        }

        function reload()
        {
            if (query)
                sliderItem.value = query.getSetting(type)
            else
                sliderItem.value = media.getSetting(type)

            sliderItemValue.text = getSliderValue()
        }

        MouseArea {
            anchors.fill: parent

            Column {
                anchors.fill: parent

                Rectangle {
                    width: parent.width
                    height: 50

                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "#1f88f9" }
                        GradientStop { position: 0.93; color: "#1f88f9" }
                        GradientStop { position: 1.0; color: "#1874d0" }
                    }

                    Row {
                        id: navigation
                        width: parent.width
                        height: parent.height

                        Rectangle {
                            id: spacer1
                            color: "transparent"
                            height: parent.height
                            width: 10
                        }

                        Text {
                            id: title
                            width: parent.width - spacer1.width
                            height: parent.height
                            verticalAlignment: Text.AlignVCenter
                            color: "white"
                            elide: Text.ElideMiddle
                            font.pixelSize: parent.height * 0.3
                        }
                    }
                }

                Rectangle {
                    id: sliderRoot
                    width: parent.width
                    height: parent.height - navigation.height - spacer.height - cancelButton.height
                    color: "white"

                    Column {
                        anchors.fill: parent

                        Rectangle {
                            color: "transparent"
                            height: 10
                            width: parent.width
                        }

                        Row {
                            width: parent.width
                            height: 30

                            Rectangle {
                                id: resetSpacer1
                                color: "transparent"
                                height: parent.height
                                width: 10
                            }

                            Text {
                                id: desc
                                width: parent.width - resetBox.width - resetSpacer1.width - resetSpacer2.width - resetSpacer3.width
                                height: parent.height
                                color: "gray"
                                wrapMode: Text.WordWrap
                            }

                            Rectangle {
                                id: resetSpacer2
                                color: "transparent"
                                height: parent.height
                                width: 10
                            }

                            Rectangle {
                                id: resetBox
                                width: reset.width + 20
                                height: parent.height
                                anchors.topMargin: 10
                                color: "gray"

                                ImageButton {
                                    id: reset
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    height: parent.height
                                    text.text: qsTr("초기화")
                                    text.color: "white"
                                    text.verticalAlignment: Text.AlignVCenter
                                    text.horizontalAlignment: Text.AlignHCenter

                                    onClicked: {
                                        sliderItem.value = defaultValue

                                        if (query)
                                            query.setSetting(type, sliderItem.value)
                                        else
                                            media.setSetting(type, sliderItem.value)

                                        root.reload()
                                    }
                                }
                            }

                            Rectangle {
                                id: resetSpacer3
                                color: "transparent"
                                height: parent.height
                                width: 10
                            }
                        }

                        Rectangle {
                            color: "transparent"
                            height: 20
                            width: parent.width
                        }

                        Row {
                            width: parent.width
                            height: 25

                            Rectangle {
                                id: sliderItemSpacer1
                                color: "transparent"
                                height: parent.height
                                width: 10
                            }

                            ImageButton {
                                id: decrease
                                anchors.verticalCenter: parent.verticalCenter
                                width: parent.height
                                height: parent.height

                                Rectangle {
                                    anchors.fill: parent
                                    radius: width
                                    color: "lightgreen"

                                    Text {
                                        anchors.fill: parent
                                        text: "-"
                                        font.bold: true
                                        color: "white"
                                        verticalAlignment: Text.AlignVCenter
                                        horizontalAlignment: Text.AlignHCenter
                                    }
                                }

                                onClicked: {
                                    slider.value -= slider.stepSize
                                }
                            }

                            Rectangle {
                                id: sliderItemSpacer2
                                color: "transparent"
                                height: parent.height
                                width: 10
                            }

                            Slider {
                                id: sliderItem
                                width: parent.width - sliderItemValue.width - decrease.width - increase.width -
                                       sliderItemSpacer1.width - sliderItemSpacer2.width - sliderItemSpacer3.width -
                                       sliderItemSpacer4.width - sliderItemSpacer5.width
                                height: parent.height

                                style: SliderStyle {
                                    groove: Rectangle {
                                        width: control.height
                                        height: control.height * 0.7
                                        color: "lightgray"
                                        radius: 8

                                        Rectangle{
                                            anchors.left: parent.left
                                            anchors.leftMargin: 6
                                            anchors.verticalCenter: parent.verticalCenter
                                            width: styleData.handlePosition - anchors.leftMargin - control.height / 2 + 2
                                            height: control.height * 0.2
                                            color: "#1f88f9"
                                            radius: parent.radius
                                        }
                                    }

                                    handle: Rectangle {
                                        anchors.centerIn: parent
                                        color: control.pressed ? "white" : "lightgray"
                                        border.color: "gray"
                                        border.width: 2
                                        width: control.height
                                        height: width
                                        radius: width
                                    }
                                }

                                Component.onCompleted: {
                                    root.reload()
                                }

                                onValueChanged: {
                                    if (query)
                                        query.setSetting(type, value)
                                    else
                                        media.setSetting(type, value)

                                    sliderItemValue.text = root.getSliderValue()
                                }
                            }

                            Rectangle {
                                id: sliderItemSpacer3
                                color: "transparent"
                                height: parent.height
                                width: 10
                            }

                            Text {
                                id: sliderItemValue
                                height: parent.height
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                color: "gray"
                            }

                            Rectangle {
                                id: sliderItemSpacer4
                                color: "transparent"
                                height: parent.height
                                width: 10
                            }

                            ImageButton {
                                id: increase
                                anchors.verticalCenter: parent.verticalCenter
                                width: parent.height
                                height: parent.height

                                Rectangle {
                                    anchors.fill: parent
                                    radius: width
                                    color: "lightgreen"

                                    Text {
                                        anchors.fill: parent
                                        text: "+"
                                        font.bold: true
                                        color: "white"
                                        verticalAlignment: Text.AlignVCenter
                                        horizontalAlignment: Text.AlignHCenter
                                    }
                                }

                                onClicked: {
                                    slider.value += slider.stepSize
                                }
                            }

                            Rectangle {
                                id: sliderItemSpacer5
                                color: "transparent"
                                height: parent.height
                                width: 10
                            }
                        }
                    }
                }

                Rectangle {
                    id: spacer
                    width: parent.width
                    height: 1
                    color: "gray"
                }

                Rectangle {
                    id: cancelButton
                    width: parent.width
                    height: 50

                    ImageButton {
                        anchors.right: parent.right
                        anchors.rightMargin: 20
                        height: parent.height
                        text.text: qsTr("닫기")
                        text.color: "green"
                        text.verticalAlignment: Text.AlignVCenter
                        text.horizontalAlignment: Text.AlignRight

                        onClicked: {
                            goBack()
                        }
                    }
                }
            }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back)
        {
            event.accepted = true

            goBack()
        }
    }
}

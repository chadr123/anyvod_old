﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.2
import QtSensors 5.3
import com.dcple.anyvod 1.0

RenderScreen {
    id: screen
    objectName: "screen"
    focus: true
    anchors.fill: parent

    property int hookKeyCount: 0
    property bool hookKeyPressed: false
    property bool hookKeyPressedProcessing: false

    property real rotationZ: 0.0
    property real rotationY: 0.0
    property real preRotationZ: 0.0
    property real preRotationY: 0.0
    property int overZ: 0
    property int overY: 0
    property bool needCalibrateRotation: false

    readonly property int settingListDefaultHeight: height * 0.8
    readonly property int settingListDefaultWidth: width * 0.8

    readonly property int settingListLongHeight: height * 0.9
    readonly property int settingListLongWidth: width * 0.9

    function hideControlPanel()
    {
        controlPanel.opacity = 0.0
        hideTimer.restart()
    }

    function showPlayList()
    {
        playList.visible = true
        playList.opacity = 1
        playList.forceActiveFocus()
    }

    function hidePlayList()
    {
        playList.opacity = 0
        playListHideTimer.restart()
    }

    function determineToStartRotation()
    {
        rotation.active = (screen.getVRInputSource() !== AnyVODEnums.VRI_NONE) &&
                          (mediaDelegate.getSetting(AnyVODEnums.SK_VR_HEAD_TRACKING))
    }

    function determineToStartProximity()
    {
        proximity.active = (screen.getVRInputSource() !== AnyVODEnums.VRI_NONE) &&
                           (screen.isPlayOrPause())
    }

    function calibrateRotation()
    {
        if (screen.getVRInputSource() !== AnyVODEnums.VRI_NONE)
        {
            rotationZ = rotation.reading.z + 180
            rotationY = rotation.reading.y + 180

            preRotationZ = rotationZ
            preRotationY = rotationY

            overZ = 0
            overY = 0
        }
    }

    ProximitySensor {
        id: proximity
        active: false

        onReadingChanged: {
            if (!active)
                return

            if (reading.near)
            {
                if (screen.getStatus() === RenderScreen.Paused)
                    screen.resume()
            }
            else
            {
                if (screen.getStatus() === RenderScreen.Playing)
                    screen.pause()
            }
        }
    }

    RotationSensor {
        id: rotation
        active: false

        onReadingChanged: {
            if (!active)
                return

            if (screen.needCalibrateRotation)
            {
                screen.calibrateRotation()
                screen.needCalibrateRotation = false
            }

            var margin = 120
            var z = reading.z + 180
            var y = reading.y + 180
            var adjustedZ = z
            var adjustedY = y

            if ((preRotationZ >= (360 - margin) && preRotationZ <= 360) && (z >= 0 && z <= margin))
                overZ++
            else if ((preRotationZ >= 0 && preRotationZ <= margin) && (z >= (360 - margin) && z <= 360))
                overZ--

            if ((preRotationY >= (360 - margin) && preRotationY <= 360) && (y >= 0 && y <= margin))
                overY++
            else if ((preRotationY >= 0 && preRotationY <= margin) && (y >= (360 - margin) && y <= 360))
                overY--

            adjustedZ += overZ * 360
            adjustedY += overY * 360

            screen.setHorizontalScreenOffset((adjustedZ - rotationZ) * 3.0)
            screen.setVerticalScreenOffset((rotationY - adjustedY) * 3.0)

            preRotationZ = z
            preRotationY = y
        }
    }

    MediaDelegate {
        id: mediaDelegate
    }

    PlayListModel {
        id: playListModel
        objectName: "playListModel"
    }

    PlayList {
        id: playList
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        playListModel: playListModel
        mediaDelegate: mediaDelegate
        z: 10

        Timer {
            id: playListHideTimer
            interval: playListOpacityAni.duration

            onTriggered: {
                playList.visible = false
                screen.forceActiveFocus()
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: playListOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onGoBack: {
            hidePlayList()
        }

        onItemSelected: {
            screen.playAt(index)
        }

        onNewQualitySelected: {
            playListModel.selectOtherQuality(index, quality)
            itemSelected(index)
        }
    }

    BusyIndicator {
        id: empty
        objectName: "empty"
        anchors.centerIn: parent
        width: 50
        height: 50
        visible: false
    }

    RepeatRange {
        id: repeatRange
        media: mediaDelegate
        windowWidth: 300
        windowHeight: 70
    }

    Component {
        id: subtitlePopup

        Popup {
            windowX: popupLoader.width - windowWidth - 20
            windowY: popupLoader.height - windowHeight - 20
            windowWidth: 240
            windowHeight: 110
            title: qsTr("자막 찾음")
            desc: popupLoader.desc
            link: popupLoader.link
        }
    }

    Component {
        id: equalizer

        Equalizer {
            title: qsTr("이퀄라이저")
            query: listModel

            Component.onDestruction: {
                screen.loadEqualizer()
            }
        }
    }

    Component {
        id: epg

        ViewEPG {
            title: qsTr("채널 편성표")
            query: screen
            channel: screen.getTitle()
        }
    }

    Timer {
        id: hideTimer
        interval: controlPanelOpacityAni.duration

        onTriggered: {
            controlPanel.visible = false
            screen.setOptionDescY(0)
        }
    }

    Timer {
        id: showTimer
        interval: 3000

        onTriggered: {
            hideControlPanel()
        }
    }

    MouseArea {
        id: touchArea
        y: 0
        width: parent.width
        height: parent.height - y

        property int axis: Drag.None
        property real threshold: 10.0
        property real thresholdY: 20.0
        property real thresholdX: 50.0
        property int volumeStart: 0

        property real startX: 0
        property real startY: 0
        property bool dragging: false

        onPositionChanged: {
            var p = mouse

            if (pressed)
            {
                var xDelta = Math.abs(p.x - startX)
                var yDelta = Math.abs(p.y - startY)

                if (!dragging && (xDelta >= threshold || yDelta >= threshold))
                {
                    if (xDelta > yDelta)
                    {
                        axis = Drag.XAxis
                    }
                    else
                    {
                        axis = Drag.YAxis
                        volumeStart = screen.getVolume()
                    }

                    dragging = true
                }

                if (dragging)
                {
                    var delta = 0.0

                    if (axis == Drag.XAxis)
                    {
                        delta = p.x - startX

                        if (Math.abs(delta) >= thresholdX)
                        {
                            if (delta > 0)
                                screen.forwardProper()
                            else
                                screen.rewindProper()

                            startX = p.x
                        }
                    }
                    else if (axis == Drag.YAxis)
                    {
                        if (isUseSPDIF())
                            return

                        delta = startY - p.y

                        if (Math.abs(delta) >= thresholdY)
                        {
                            var step = 10
                            var value = volumeStart + (step * (delta / thresholdY))

                            if (value < 0)
                                value = 0
                            else if (value > screen.getMaxVolume())
                                value = screen.getMaxVolume()

                            screen.volume(value)
                            muteButton.checked = false
                        }
                    }

                    if (controlPanel.visible)
                        showTimer.restart()
                }
            }
        }

        onPressed: {
            var p = mouse

            startX = p.x
            startY = p.y
        }

        onReleased: {
            axis = Drag.None
            startX = 0
            startY = 0
            dragging = false
        }

        onClicked: {
            if (!dragging)
            {
                if (showTimer.running)
                {
                    showTimer.stop()
                    hideControlPanel()
                }
                else
                {
                    controlPanel.opacity = 1.0
                    controlPanel.visible = true
                    screen.setOptionDescYValue()

                    hideTimer.stop()
                    showTimer.restart()
                }
            }
        }

        onDoubleClicked: {
            screen.toggle()
        }

        onPressAndHold: {
            rotation.active = false

            screen.determineToStartRotation()

            if (rotation.active)
                screen.needCalibrateRotation = true
        }
    }

    Component {
        id: notSupported

        MessageBox {
            windowWidth: Math.min(settingListDefaultWidth, 350)
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("지원하지 않는 기능입니다.")
        }
    }

    Component {
        id: deinterlaceMethod

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("디인터레이스 활성화 기준")
            media: mediaDelegate
            type: AnyVODEnums.SK_DEINTERLACE_METHOD_ORDER
            model: ListModel {
                ListElement { desc: qsTr("자동"); value: AnyVODEnums.DM_AUTO }
                ListElement { desc: qsTr("사용"); value: AnyVODEnums.DM_USE }
                ListElement { desc: qsTr("사용 안 함"); value: AnyVODEnums.DM_NOUSE }
            }
        }
    }

    Component {
        id: deinterlaceAlgorithm

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 423)
            title: qsTr("디인터레이스 알고리즘")
            media: mediaDelegate
            type: AnyVODEnums.SK_DEINTERLACE_ALGORITHM_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.DA_COUNT; i++)
                    {
                        var desc = screen.getDeinterlaceDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: userAspectRatio

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 469)
            title: qsTr("화면 비율")
            media: mediaDelegate
            type: AnyVODEnums.SK_USER_ASPECT_RATIO_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < screen.getUserAspectRatioCount(); i++)
                    {
                        var desc = screen.getUserAspectRatioDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: customUserAspectRatio

        UserAspectRatio {
            windowWidth: Math.min(settingListDefaultWidth, 365)
            windowHeight: Math.min(settingListDefaultHeight, 160)
            title: qsTr("화면 비율 사용자 지정")
            media: mediaDelegate
            aspectSize: mediaDelegate.getSetting(AnyVODEnums.SK_USER_ASPECT_RATIO)
        }
    }

    Component {
        id: vrCoefficients

        SelectVector2D {
            windowWidth: Math.min(settingListDefaultWidth, 350)
            windowHeight: Math.min(settingListDefaultHeight, 160)
            title: qsTr("VR 왜곡 보정 계수 설정")
            firstTitle: qsTr("k1")
            secondTitle: qsTr("k2")
            media: mediaDelegate
            initValue: mediaDelegate.getSetting(AnyVODEnums.SK_VR_COEFFICIENTS)
            key: AnyVODEnums.SK_VR_COEFFICIENTS
            firstMinValue: -10.0
            firstMaxValue: 10.0
            secondMinValue: -10.0
            secondMaxValue: 10.0
        }
    }

    Component {
        id: vrLensCenter

        SelectVector2D {
            windowWidth: Math.min(width * 0.9, 400)
            windowHeight: Math.min(settingListDefaultHeight, 160)
            title: qsTr("VR 렌즈 센터 설정")
            firstTitle: qsTr("좌/우")
            secondTitle: qsTr("상/하")
            media: mediaDelegate
            initValue: mediaDelegate.getSetting(AnyVODEnums.SK_VR_LENS_CENTER)
            key: AnyVODEnums.SK_VR_LENS_CENTER
            firstMinValue: -1.0
            firstMaxValue: 1.0
            secondMinValue: -1.0
            secondMaxValue: 1.0
        }
    }

    Component {
        id: rotationDegree

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("영상 회전 각도")
            media: mediaDelegate
            type: AnyVODEnums.SK_SCREEN_ROTATION_DEGREE_ORDER
            model: ListModel {
                ListElement { desc: qsTr("사용 안 함"); value: AnyVODEnums.SRD_NONE }
                ListElement { desc: qsTr("90도"); value: AnyVODEnums.SRD_90 }
                ListElement { desc: qsTr("180도"); value: AnyVODEnums.SRD_180 }
                ListElement { desc: qsTr("270도"); value: AnyVODEnums.SRD_270 }
            }
        }
    }

    Component {
        id: captureExt

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("캡처 확장자")
            media: mediaDelegate
            type: AnyVODEnums.SK_SELECT_CAPTURE_EXT_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < screen.getCaptureExtCount(); i++)
                    {
                        var desc = screen.getCaptureExt(i)

                        append({ "desc": desc, "value": desc })
                    }
                }
            }
        }
    }

    Component {
        id: video3DMethod

        SettingList {
            title: qsTr("3D 영상 출력 방법")
            media: mediaDelegate
            type: AnyVODEnums.SK_3D_VIDEO_METHOD_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.V3M_COUNT; i++)
                    {
                        var desc = screen.get3DMethodDesc(i)
                        var cat = screen.get3DMethodCategory(i)

                        append({ "desc": desc, "value": i, "category": cat })
                    }
                }
            }
        }
    }

    Component {
        id: anaglyphAlgorithm

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("3D 영상 애너글리프 알고리즘")
            media: mediaDelegate
            type: AnyVODEnums.SK_ANAGLYPH_ALGORITHM_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.AGA_COUNT; i++)
                    {
                        var desc = screen.getAnaglyphAlgoritmDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: vrInputSource

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("VR 입력 영상")
            media: mediaDelegate
            type: AnyVODEnums.SK_VR_INPUT_SOURCE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.VRI_COUNT; i++)
                    {
                        var desc = screen.getVRInputSourceDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: playOrder

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 331)
            title: qsTr("재생 순서 설정")
            media: mediaDelegate
            type: AnyVODEnums.SK_PLAY_ORDER
            model: ListModel {
                ListElement { desc: qsTr("전체 순차 재생"); value: AnyVODEnums.PM_TOTAL }
                ListElement { desc: qsTr("전체 반복 재생"); value: AnyVODEnums.PM_TOTAL_REPEAT }
                ListElement { desc: qsTr("한 개 재생"); value: AnyVODEnums.PM_SINGLE }
                ListElement { desc: qsTr("한 개 반복 재생"); value: AnyVODEnums.PM_SINGLE_REPEAT }
                ListElement { desc: qsTr("무작위 재생"); value: AnyVODEnums.PM_RANDOM }
            }
        }
    }

    Component {
        id: brightness

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("밝기")
            media: mediaDelegate
            type: AnyVODEnums.SK_BRIGHTNESS
            defaultValue: 1
            roundValue: 2
            unitPostfix: qsTr("배")
            slider.minimumValue: 0
            slider.maximumValue: 3
            slider.stepSize: 0.01
        }
    }

    Component {
        id: saturation

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("채도")
            media: mediaDelegate
            type: AnyVODEnums.SK_SATURATION
            defaultValue: 1
            roundValue: 2
            unitPostfix: qsTr("배")
            slider.minimumValue: 0
            slider.maximumValue: 3
            slider.stepSize: 0.01
        }
    }

    Component {
        id: hue

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("색상")
            media: mediaDelegate
            type: AnyVODEnums.SK_HUE
            defaultValue: 0
            roundValue: 0
            unitPostfix: qsTr("°")
            slider.minimumValue: 0
            slider.maximumValue: 360
            slider.stepSize: 1
        }
    }

    Component {
        id: contrast

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("대비")
            media: mediaDelegate
            type: AnyVODEnums.SK_CONTRAST
            defaultValue: 1
            roundValue: 2
            unitPostfix: qsTr("배")
            slider.minimumValue: 0
            slider.maximumValue: 3
            slider.stepSize: 0.01
        }
    }

    Component {
        id: playbackSpeed

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("재생 속도 설정")
            media: mediaDelegate
            type: AnyVODEnums.SK_PLAYBACK_SPEED
            defaultValue: 0
            defaultStartValue: 1
            roundValue: 1
            scaleValue: 0.01
            unitPostfix: qsTr("배")
            slider.minimumValue: -90
            slider.maximumValue: 400
            slider.stepSize: 10
        }
    }

    Component {
        id: subtitleOpaque

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("투명도")
            media: mediaDelegate
            type: AnyVODEnums.SK_SUBTITLE_OPAQUE
            defaultValue: 100
            roundValue: 0
            unitPostfix: qsTr("%")
            slider.minimumValue: 0
            slider.maximumValue: 100
            slider.stepSize: 1
        }
    }

    Component {
        id: subtitleSize

        SettingSlider {
            sliderWidth: Math.min(settingListDefaultWidth, 350)
            title: qsTr("크기")
            media: mediaDelegate
            type: AnyVODEnums.SK_SUBTITLE_SIZE
            defaultValue: 100
            roundValue: 0
            unitPostfix: qsTr("%")
            slider.minimumValue: 0
            slider.maximumValue: 300
            slider.stepSize: 1

            onGoBack: {
                screen.applySubtitleSize()
            }
        }
    }

    Component {
        id: subtitleSync

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("싱크")
            media: mediaDelegate
            type: AnyVODEnums.SK_SUBTITLE_SYNC
            defaultValue: 0
            roundValue: 1
            unitPostfix: qsTr("초")
            slider.minimumValue: -300
            slider.maximumValue: 300
            slider.stepSize: 0.5
        }
    }

    Component {
        id: subtitleLeftRightPosition

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("가로 위치")
            desc: qsTr("값이 클 수록 오른쪽으로 이동합니다.")
            media: mediaDelegate
            type: AnyVODEnums.SK_LEFTRIGHT_SUBTITLE_POSITION
            defaultValue: 0
            roundValue: 0
            slider.minimumValue: -100
            slider.maximumValue: 100
            slider.stepSize: 1
        }
    }

    Component {
        id: subtitleUpDownPosition

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("세로 위치")
            desc: qsTr("값이 클 수록 윗쪽으로 이동합니다.")
            media: mediaDelegate
            type: AnyVODEnums.SK_UPDOWN_SUBTITLE_POSITION
            defaultValue: 0
            roundValue: 0
            slider.minimumValue: -100
            slider.maximumValue: 100
            slider.stepSize: 1
        }
    }

    Component {
        id: subtitleHAlign

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 331)
            title: qsTr("가로 정렬")
            media: mediaDelegate
            type: AnyVODEnums.SK_SUBTITLE_HALIGN_ORDER
            model: ListModel {
                ListElement { desc: qsTr("기본 정렬"); value: AnyVODEnums.HAM_NONE }
                ListElement { desc: qsTr("자동 정렬"); value: AnyVODEnums.HAM_AUTO }
                ListElement { desc: qsTr("왼쪽 정렬"); value: AnyVODEnums.HAM_LEFT }
                ListElement { desc: qsTr("가운데 정렬"); value: AnyVODEnums.HAM_MIDDLE }
                ListElement { desc: qsTr("오른쪽 정렬"); value: AnyVODEnums.HAM_RIGHT }
            }
        }
    }

    Component {
        id: subtitleVAlign

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("세로 정렬")
            media: mediaDelegate
            type: AnyVODEnums.SK_SUBTITLE_VALIGN_ORDER
            model: ListModel {
                ListElement { desc: qsTr("기본 정렬"); value: AnyVODEnums.VAM_NONE }
                ListElement { desc: qsTr("상단 정렬"); value: AnyVODEnums.VAM_TOP }
                ListElement { desc: qsTr("하단 정렬"); value: AnyVODEnums.VAM_BOTTOM }
            }
        }
    }

    Component {
        id: subtitleEncoding

        SettingList {
            title: qsTr("텍스트 인코딩")
            media: mediaDelegate
            type: AnyVODEnums.SK_TEXT_ENCODING
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < fileListModel.getTextCodecCount(); i++)
                    {
                        var desc = fileListModel.getTextCodecName(i)

                        append({ "desc": desc, "value": desc })
                    }
                }
            }
        }
    }

    Component {
        id: subtitleLanguage

        SettingList {
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("언어")
            media: mediaDelegate
            type: AnyVODEnums.SK_SUBTITLE_LANGUAGE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < screen.getSubtitleClassCount(); i++)
                    {
                        var desc = screen.getSubtitleClass(i)

                        append({ "desc": desc, "value": desc })
                    }
                }
            }
        }
    }

    Component {
        id: subtitle3DUpDownPosition

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("세로 거리")
            desc: qsTr("값이 클 수록 가까워집니다. VR 모드일 경우 반대 입니다.")
            media: mediaDelegate
            type: AnyVODEnums.SK_UPDOWN_3D_SUBTITLE_OFFSET
            defaultValue: 0
            roundValue: 0
            slider.minimumValue: -30
            slider.maximumValue: 30
            slider.stepSize: 1
        }
    }

    Component {
        id: subtitle3DLeftRightPosition

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("가로 거리")
            desc: qsTr("값이 클 수록 가까워집니다. VR 모드일 경우 반대 입니다.")
            media: mediaDelegate
            type: AnyVODEnums.SK_LEFTRIGHT_3D_SUBTITLE_OFFSET
            defaultValue: 2
            roundValue: 0
            slider.minimumValue: -30
            slider.maximumValue: 30
            slider.stepSize: 1
        }
    }

    Component {
        id: subtitle3DMethod

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 423)
            title: qsTr("3D 자막 출력 방법")
            media: mediaDelegate
            type: AnyVODEnums.SK_3D_SUBTITLE_METHOD_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < AnyVODEnums.S3M_COUNT; i++)
                    {
                        var desc = screen.getSubtitle3DMethodDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: audioSync

        SettingSlider {
            sliderWidth: Math.min(settingListLongWidth, 350)
            title: qsTr("싱크")
            media: mediaDelegate
            type: AnyVODEnums.SK_AUDIO_SYNC
            defaultValue: 0
            roundValue: 1
            unitPostfix: qsTr("초")
            slider.minimumValue: -300
            slider.maximumValue: 300
            slider.stepSize: 0.5
        }
    }

    Component {
        id: openSubtitle

        FileSystemDialog {
            fileListObjName: "openSubtitle"
            title: qsTr("열기")
            onlySubtitle: true

            onAccepted: {
                if (!screen.openSubtitle(selectedPath))
                {
                    messageBoxLoader.focusParent = settingLoader
                    messageBoxLoader.sourceComponent = failedOpenSubtitleBox
                    messageBoxLoader.forceActiveFocus()
                }
            }
        }
    }

    Component {
        id: openSaveAsSubtitle

        FileSystemDialog {
            fileListObjName: "openSaveAsSubtitle"
            title: qsTr("다른 이름으로 저장")
            onlySubtitle: true
            startDirectory: screen.getSubtitlePath()

            onAccepted: {
                messageBoxLoader.focusParent = settingLoader

                if (screen.saveSubtitleAs(selectedPath))
                    messageBoxLoader.sourceComponent = successSaveSubtitleBox
                else
                    messageBoxLoader.sourceComponent = failedSaveSubtitleBox

                messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: openCaptureSaveDir

        FileSystemDialog {
            fileListObjName: "openCaptureSaveDir"
            title: qsTr("저장 디렉토리 설정")
            onlyDirectory: true
            startDirectory: mediaDelegate.getSetting(AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY)

            onAccepted: {
                mediaDelegate.setSetting(AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY, selectedPath)
            }
        }
    }

    Component {
        id: audioLanguage

        SettingList {
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("언어")
            media: mediaDelegate
            type: AnyVODEnums.SK_AUDIO_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < screen.getAudioStreamInfoCount(); i++)
                    {
                        var desc = screen.getAudioStreamDesc(i)
                        var stream = screen.getAudioStreamIndex(i)

                        append({ "desc": desc, "value": stream })
                    }
                }
            }
        }
    }

    Component {
        id: audioDevice

        SettingList {
            title: qsTr("오디오 장치")
            media: mediaDelegate
            type: AnyVODEnums.SK_AUDIO_DEVICE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var list = screen.getAudioDevices();

                    append({ "desc": qsTr("기본 장치"), "value": -1 })

                    for (var i = 0; i < list.length; i++)
                    {
                        var desc = list[i]

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: spdifAudioDevice

        SettingList {
            title: qsTr("출력 장치")
            media: mediaDelegate
            type: AnyVODEnums.SK_SPDIF_AUDIO_DEVICE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    var list = screen.getSPDIFAudioDevices();

                    append({ "desc": qsTr("기본 장치"), "value": -1 })

                    for (var i = 0; i < list.length; i++)
                    {
                        var desc = list[i]

                        desc = desc.replace(/\n/gi, " ")

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: spdifEncoding

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 239)
            title: qsTr("인코딩")
            media: mediaDelegate
            type: AnyVODEnums.SK_USE_SPDIF_ENCODING_ORDER
            model: ListModel {
                ListElement { desc: qsTr("사용 안 함"); value: AnyVODEnums.SEM_NONE }
                ListElement { desc: qsTr("AC3"); value: AnyVODEnums.SEM_AC3 }
                ListElement { desc: qsTr("DTS"); value: AnyVODEnums.SEM_DTS }
            }
        }
    }

    Component {
        id: spdifSampleRate

        SettingList {
            listWidth: Math.min(settingListDefaultWidth, 300)
            listHeight: Math.min(settingListDefaultHeight, 285)
            title: qsTr("샘플링 속도")
            media: mediaDelegate
            type: AnyVODEnums.SK_SPDIF_USER_SAMPLE_RATE_ORDER
            model: ListModel {
                Component.onCompleted: {
                    for (var i = 0; i < screen.getUserSPDIFSampleRateCount(); i++)
                    {
                        var desc = screen.getUserSPDIFSampleRateDesc(i)

                        append({ "desc": desc, "value": i })
                    }
                }
            }
        }
    }

    Component {
        id: screenSettings

        SettingDialog {
            title: qsTr("화면")
            model: ScreenSettingItems {}
            media: mediaDelegate

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_DEINTERLACE_METHOD_ORDER:
                        settingDlgLoader.sourceComponent = deinterlaceMethod
                        break
                    case AnyVODEnums.SK_DEINTERLACE_ALGORITHM_ORDER:
                        settingDlgLoader.sourceComponent = deinterlaceAlgorithm
                        break
                    case AnyVODEnums.SK_USER_ASPECT_RATIO_ORDER:
                        settingDlgLoader.sourceComponent = userAspectRatio
                        break
                    case AnyVODEnums.SK_USER_ASPECT_RATIO:
                        settingDlgLoader.sourceComponent = customUserAspectRatio
                        break
                    case AnyVODEnums.SK_SCREEN_ROTATION_DEGREE_ORDER:
                        settingDlgLoader.sourceComponent = rotationDegree
                        break
                    case AnyVODEnums.SK_BRIGHTNESS:
                        if (Qt.platform.os === "linux")
                            messageBoxLoader.sourceComponent = notSupported
                        else
                            settingDlgLoader.sourceComponent = brightness

                        break
                    case AnyVODEnums.SK_SATURATION:
                        if (Qt.platform.os === "linux")
                            messageBoxLoader.sourceComponent = notSupported
                        else
                            settingDlgLoader.sourceComponent = saturation

                        break
                    case AnyVODEnums.SK_HUE:
                        if (Qt.platform.os === "linux")
                            messageBoxLoader.sourceComponent = notSupported
                        else
                            settingDlgLoader.sourceComponent = hue

                        break
                    case AnyVODEnums.SK_CONTRAST:
                        if (Qt.platform.os === "linux")
                            messageBoxLoader.sourceComponent = notSupported
                        else
                            settingDlgLoader.sourceComponent = contrast

                        break
                    case AnyVODEnums.SK_SELECT_CAPTURE_EXT_ORDER:
                        settingDlgLoader.sourceComponent = captureExt
                        break
                    case AnyVODEnums.SK_SELECT_CAPTURE_DIRECTORY:
                        settingDlgLoader.sourceComponent = openCaptureSaveDir
                        break
                    case AnyVODEnums.SK_3D_VIDEO_METHOD_ORDER:
                        settingDlgLoader.sourceComponent = video3DMethod
                        break
                    case AnyVODEnums.SK_ANAGLYPH_ALGORITHM_ORDER:
                        settingDlgLoader.sourceComponent = anaglyphAlgorithm
                        break
                    case AnyVODEnums.SK_VR_INPUT_SOURCE_ORDER:
                        settingDlgLoader.sourceComponent = vrInputSource
                        break
                    case AnyVODEnums.SK_VR_COEFFICIENTS:
                        settingDlgLoader.sourceComponent = vrCoefficients
                        break
                    case AnyVODEnums.SK_VR_LENS_CENTER:
                        settingDlgLoader.sourceComponent = vrLensCenter
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: playSettings

        SettingDialog {
            title: qsTr("재생")
            model: PlaySettingItems {}
            media: mediaDelegate

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_REPEAT_RANGE:
                        goBack()
                        screen.showRepeatRange()
                        break
                    case AnyVODEnums.SK_PLAYBACK_SPEED:
                        settingDlgLoader.sourceComponent = playbackSpeed
                        break
                    case AnyVODEnums.SK_PLAY_ORDER:
                        settingDlgLoader.sourceComponent = playOrder
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: subtitleLyricsSettings

        SettingDialog {
            title: qsTr("자막 / 가사")
            model: SubtitleLyricsSettingItems {}
            media: mediaDelegate

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_OPEN_SUBTITLE:
                        settingDlgLoader.sourceComponent = openSubtitle
                        break
                    case AnyVODEnums.SK_CLOSE_EXTERNAL_SUBTITLE:
                        screen.closeExternalSubtitle()
                        messageBoxLoader.focusParent = settingLoader
                        messageBoxLoader.sourceComponent = closeExternalSubtitleBox
                        break
                    case AnyVODEnums.SK_SUBTITLE_OPAQUE:
                        settingDlgLoader.sourceComponent = subtitleOpaque
                        break
                    case AnyVODEnums.SK_SUBTITLE_SIZE:
                        settingDlgLoader.sourceComponent = subtitleSize
                        break
                    case AnyVODEnums.SK_SUBTITLE_SYNC:
                        settingDlgLoader.sourceComponent = subtitleSync
                        break
                    case AnyVODEnums.SK_LEFTRIGHT_SUBTITLE_POSITION:
                        settingDlgLoader.sourceComponent = subtitleLeftRightPosition
                        break
                    case AnyVODEnums.SK_UPDOWN_SUBTITLE_POSITION:
                        settingDlgLoader.sourceComponent = subtitleUpDownPosition
                        break
                    case AnyVODEnums.SK_SUBTITLE_HALIGN_ORDER:
                        settingDlgLoader.sourceComponent = subtitleHAlign
                        break
                    case AnyVODEnums.SK_SUBTITLE_VALIGN_ORDER:
                        settingDlgLoader.sourceComponent = subtitleVAlign
                        break
                    case AnyVODEnums.SK_SUBTITLE_LANGUAGE_ORDER:
                        settingDlgLoader.sourceComponent = subtitleLanguage
                        break
                    case AnyVODEnums.SK_TEXT_ENCODING:
                        settingDlgLoader.sourceComponent = subtitleEncoding
                        break
                    case AnyVODEnums.SK_SAVE_SUBTITLE:
                        messageBoxLoader.focusParent = settingLoader

                        if (screen.saveSubtitle())
                            messageBoxLoader.sourceComponent = successSaveSubtitleBox
                        else
                            messageBoxLoader.sourceComponent = failedSaveSubtitleBox

                        break
                    case AnyVODEnums.SK_SAVE_AS_SUBTITLE:
                        settingDlgLoader.sourceComponent = openSaveAsSubtitle
                        break
                    case AnyVODEnums.SK_UPDOWN_3D_SUBTITLE_OFFSET:
                        settingDlgLoader.sourceComponent = subtitle3DUpDownPosition
                        break
                    case AnyVODEnums.SK_LEFTRIGHT_3D_SUBTITLE_OFFSET:
                        settingDlgLoader.sourceComponent = subtitle3DLeftRightPosition
                        break
                    case AnyVODEnums.SK_3D_SUBTITLE_METHOD_ORDER:
                        settingDlgLoader.sourceComponent = subtitle3DMethod
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: soundSettings

        SettingDialog {
            title: qsTr("소리")
            model: SoundSettingItems {}
            media: mediaDelegate

            onNotSupportedFunction: {
                messageBoxLoader.sourceComponent = notSupported
                messageBoxLoader.forceActiveFocus()
            }

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_AUDIO_ORDER:
                        settingDlgLoader.sourceComponent = audioLanguage
                        break
                    case AnyVODEnums.SK_AUDIO_SYNC:
                        settingDlgLoader.sourceComponent = audioSync
                        break
                    case AnyVODEnums.SK_AUDIO_DEVICE_ORDER:
                        settingDlgLoader.sourceComponent = audioDevice
                        break
                    case AnyVODEnums.SK_SPDIF_AUDIO_DEVICE_ORDER:
                        if (Qt.platform.os === "linux")
                            settingDlgLoader.sourceComponent = spdifAudioDevice
                        else
                            messageBoxLoader.sourceComponent = notSupported

                        break
                    case AnyVODEnums.SK_USE_SPDIF_ENCODING_ORDER:
                        if (Qt.platform.os === "linux")
                            settingDlgLoader.sourceComponent = spdifEncoding
                        else
                            messageBoxLoader.sourceComponent = notSupported

                        break
                    case AnyVODEnums.SK_SPDIF_USER_SAMPLE_RATE_ORDER:
                        if (Qt.platform.os === "linux")
                            settingDlgLoader.sourceComponent = spdifSampleRate
                        else
                            messageBoxLoader.sourceComponent = notSupported

                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()

                if (messageBoxLoader.sourceComponent != undefined)
                    messageBoxLoader.forceActiveFocus()
            }
        }
    }

    Component {
        id: info

        SettingTextViewer {
            title: qsTr("AnyVOD 정보")
            desc.text: fileListModel.getVersion()
            desc.wrapMode: TextEdit.Wrap
            flickDir: Flickable.VerticalFlick
        }
    }

    Component {
        id: infoSettings

        SettingDialog {
            title: qsTr("정보")
            model: InfoSettingItems {}
            media: mediaDelegate

            onItemSelected: {
                var item = model.get(index)

                switch (item.type)
                {
                    case AnyVODEnums.SK_INFO:
                        settingDlgLoader.sourceComponent = info
                        break
                }

                if (settingDlgLoader.sourceComponent != undefined)
                    settingDlgLoader.forceActiveFocus()
            }
        }
    }

    Menus {
        id: screenMenu
        timer: showTimer
        menuWidth: 170
        menuHeight: Math.min(screen.height - 20, 240)
        model: VideoScreenMenuItems {}
        boundsBehavior: Flickable.OvershootBounds

        onMenuItemSelected: {
            var item = model.get(index)

            switch (item.menuID)
            {
                case AnyVODEnums.MID_EQUALIZER:
                    settingLoader.sourceComponent = equalizer
                    break
                case AnyVODEnums.MID_SCREEN:
                    settingLoader.sourceComponent = screenSettings
                    break
                case AnyVODEnums.MID_PLAY:
                    settingLoader.sourceComponent = playSettings
                    break
                case AnyVODEnums.MID_SUBTITLE_LYRICS:
                    settingLoader.sourceComponent = subtitleLyricsSettings
                    break
                case AnyVODEnums.MID_SOUND:
                    settingLoader.sourceComponent = soundSettings
                    break
                case AnyVODEnums.MID_INFO:
                    settingLoader.sourceComponent = infoSettings
                    break
            }

            if (settingLoader.sourceComponent != undefined)
                settingLoader.forceActiveFocus()
        }
    }

    Component {
        id: closeExternalSubtitleBox

        MessageBox {
            windowWidth: Math.min(settingListDefaultWidth, 300)
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("외부 자막을 닫았습니다.")
        }
    }

    Component {
        id: successSaveSubtitleBox

        MessageBox {
            windowWidth: Math.min(settingListDefaultWidth, 350)
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("자막 / 가사가 저장 되었습니다.")
        }
    }

    Component {
        id: failedSaveSubtitleBox

        MessageBox {
            windowWidth: Math.min(settingListDefaultWidth, 350)
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("오류")
            message: qsTr("자막 / 가사가 저장 되지 않았습니다.")
        }
    }

    Component {
        id: failedOpenSubtitleBox

        MessageBox {
            windowWidth: Math.min(settingListDefaultWidth, 300)
            windowHeight: Math.min(settingListDefaultHeight, 150)
            title: qsTr("정보")
            message: qsTr("자막 / 가사를 열지 못했습니다.")
        }
    }

    Rectangle {
        id: lyrics
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height * 0.07
        height: 80
        visible: false
        radius: 5
        color: "#80000000"

        Column {
            anchors.centerIn: parent
            width: parent.width
            height: lyrics1.height + lyrics2.height + lyrics3.height + spacing * 2
            spacing: 5

            Text {
                id: lyrics1
                objectName: "lyrics1"
                elide: Text.ElideMiddle
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                style: Text.Outline
                styleColor: "black"
                width: parent.width
                height: 20
            }

            Text {
                id: lyrics2
                objectName: "lyrics2"
                elide: lyrics1.elide
                verticalAlignment: lyrics1.verticalAlignment
                horizontalAlignment: lyrics1.horizontalAlignment
                style: lyrics1.style
                styleColor: lyrics1.styleColor
                width: lyrics1.width
                height: lyrics1.height
            }

            Text {
                id: lyrics3
                objectName: "lyrics3"
                elide: lyrics1.elide
                verticalAlignment: lyrics1.verticalAlignment
                horizontalAlignment: lyrics1.horizontalAlignment
                style: lyrics1.style
                styleColor: lyrics1.styleColor
                width: lyrics1.width
                height: lyrics1.height
            }
        }
    }

    Rectangle {
        id: controlPanel
        anchors.fill: parent
        visible: false
        color: "transparent"
        opacity: 0.0

        onVisibleChanged: {
            if (repeatRange.visible)
                screen.showRepeatRange()
        }

        Behavior on opacity {
            NumberAnimation {
                id: controlPanelOpacityAni
                duration: 500
                easing.type: Easing.OutQuad
            }
        }

        Rectangle {
            id: topBar
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: 40
            color: "#7f000000"

            Grid {
                width: parent.width
                height: parent.height
                anchors.centerIn: parent
                horizontalItemAlignment: Grid.AlignHCenter
                verticalItemAlignment: Grid.AlignVCenter
                columns: 9

                Rectangle {
                    id: spacer1
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                ImageButton {
                    id: backButton
                    resetTimer: showTimer
                    width: 30
                    height: parent.height
                    image.width: 20
                    image.height: image.width
                    source: "assets/back.png"

                    onClicked: {
                        closeAndGoBack()
                    }
                }

                Rectangle {
                    id: spacer2
                    color: "transparent"
                    height: parent.height
                    width: 0
                }

                MarqueeText {
                    id: title
                    width: parent.width - backButton.width - muteButton.width - menuButton.width -
                           spacer1.width - spacer2.width - spacer3.width - spacer4.width - spacer5.width
                    height: parent.height
                    text.verticalAlignment: Text.AlignVCenter
                    text.color: "white"
                    text.font.pixelSize: parent.height * 0.4
                }

                Rectangle {
                    id: spacer3
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                ImageCheckButton {
                    id: muteButton
                    objectName: "muteButton"
                    resetTimer: showTimer
                    width: 20
                    height: width
                    checkedSource: "assets/speaker_mute.png"
                    unCheckedSource: "assets/speaker.png"

                    onToggled: {
                        screen.mute(checked)
                    }
                }

                Rectangle {
                    id: spacer4
                    color: "transparent"
                    height: parent.height
                    width: 20
                }

                ImageButton {
                    id: menuButton
                    width: 40
                    height: parent.height
                    image.width: 20
                    image.height: image.width
                    image.x: 10
                    source: "assets/menu.png"

                    onClicked: {
                        screen.showMenu()
                    }
                }

                Rectangle {
                    id: spacer5
                    color: "transparent"
                    height: parent.height
                    width: 0
                }
            }
        }

        ImageButton {
            id: rotateButton
            resetTimer: showTimer
            x: topBar.width - width - 15
            y: topBar.y + topBar.height + 15
            width: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? 0 : 30
            height: width
            image.width: width
            image.height: height
            source: "assets/rotate.png"
            visible: Qt.platform.os === "ios" || Qt.platform.os === "linux" ? false : true

            onClicked: {
                screen.setScreenOrientation(!screen.getScreenOrientation())
            }
        }

        ImageButton {
            id: playListButton
            resetTimer: showTimer
            x: topBar.width - width - 15
            y: rotateButton.y + rotateButton.height + 15
            width: 30
            height: width
            image.width: width
            image.height: height
            source: "assets/play_list.png"

            onClicked: {
                showPlayList()
            }
        }

        ImageButton {
            id: captureButton
            resetTimer: showTimer
            x: topBar.width - width - 15
            y: playListButton.y + playListButton.height + 15
            width: 30
            height: width
            image.width: width
            image.height: height
            source: "assets/capture.png"

            onClicked: {
                captureSingle()
            }
        }

        ImageButton {
            id: epgButton
            resetTimer: showTimer
            x: topBar.width - width - 15
            y: captureButton.y + captureButton.height + 15
            width: visible ? 30 : 0
            height: width
            image.width: width
            image.height: height
            source: "assets/epg.png"

            onClicked: {
                epgLoader.sourceComponent = epg
                epgLoader.forceActiveFocus()
            }
        }

        Rectangle {
            id: seekBarPanel
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: controlBar.top
            height: 40
            color: "#7f000000"

            Grid {
                anchors.fill: parent
                horizontalItemAlignment: Grid.AlignHCenter
                verticalItemAlignment: Grid.AlignVCenter
                columns: 7

                Rectangle {
                    id: seekSpacer1
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                Text {
                    id: curTime
                    objectName: "curTime"
                    width: 70
                    color : "white"
                    text: "00:00:00"
                }

                Rectangle {
                    id: seekSpacer2
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                Slider {
                    id: seekBar
                    objectName: "seekBar"
                    width: parent.width - curTime.width - totalTime.width -
                           seekSpacer1.width - seekSpacer2.width - seekSpacer3.width - seekSpacer4.width
                    height: 20
                    updateValueWhileDragging: false

                    property Timer resetTimer: showTimer
                    property bool handlePressed: false

                    style: SliderStyle {
                        groove: Rectangle {
                            width: control.height
                            height: control.height * 0.7
                            color: "lightgray"
                            radius: 8

                            Rectangle{
                                anchors.left: parent.left
                                anchors.verticalCenter: parent.verticalCenter
                                width: widthValue()
                                height: parent.height
                                color: "#1f88f9"
                                radius: parent.radius

                                function widthValue()
                                {
                                    if (control.value <= control.minimumValue)
                                        return 0
                                    else
                                        return styleData.handlePosition - anchors.leftMargin - control.height / 2 + radius + 2
                                }
                            }
                        }

                        handle: Rectangle {
                            anchors.centerIn: parent
                            color: control.pressed ? "white" : "lightgray"
                            border.color: "gray"
                            border.width: 2
                            width: control.height
                            height: width
                            radius: width
                        }
                    }

                    function seekTo(value) {
                        if (resetTimer != null)
                            resetTimer.restart()

                        screen.seek(value, true)
                    }

                    onValueChanged: {
                        if (handlePressed)
                        {
                            seekTo(value)
                            handlePressed = false
                        }
                    }

                    onPressedChanged: {
                        if (resetTimer != null)
                            resetTimer.restart()

                        if (pressed)
                           handlePressed = true
                    }
                }

                Rectangle {
                    id: seekSpacer3
                    color: "transparent"
                    height: parent.height
                    width: 10
                }

                Text {
                    id: totalTime
                    objectName: "totalTime"
                    width: 70
                    color : "white"
                    text: "00:00:00"
                }

                Rectangle {
                    id: seekSpacer4
                    color: "transparent"
                    height: parent.height
                    width: 10
                }
            }
        }

        Rectangle {
            id: controlBar
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            height: 60
            color: "#7f000000"

            Grid {
                anchors.centerIn: parent
                horizontalItemAlignment: Grid.AlignHCenter
                verticalItemAlignment: Grid.AlignTop
                columns: 5
                spacing: 30

                ImageButton {
                    id: prevButton
                    objectName: "prevButton"
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/prev.png"

                    onClicked: {
                        screen.prev(true)
                        screen.setOptionDescYValue()
                    }
                }

                ImageButton {
                    id: backwardButton
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/backward.png"

                    onClicked: {
                        screen.rewindProper()
                    }
                }

                ImageButton {
                    id: resumeButton
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/resume.png"
                    visible: false

                    onClicked: {
                        screen.resume()
                    }
                }

                ImageButton {
                    id: pauseButton
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/pause.png"

                    onClicked: {
                        screen.pause()
                    }
                }

                ImageButton {
                    id: forwardButton
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/forward.png"

                    onClicked: {
                        screen.forwardProper()
                    }
                }

                ImageButton {
                    id: nextButton
                    objectName: "nextButton"
                    resetTimer: showTimer
                    width: 40
                    height: width
                    image.width: width
                    image.height: height
                    source: "assets/next.png"

                    onClicked: {
                        screen.next(true)
                        screen.setOptionDescYValue()
                    }
                }
            }
        }
    }

    Rectangle {
        id: noVideo
        anchors.fill: parent
        visible: false
        color: "transparent"

        Image {
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: 100
            source: "assets/big_icon.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    Loader {
        id: settingDlgLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true
        z: 10

        function exit()
        {
            settingLoader.forceActiveFocus()

            opacity = 0.0
            settingDlgHideTimer.restart()
        }

        Timer {
            id: settingDlgHideTimer
            interval: settingDlgOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingDlgOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingDlgLoader.item

            onGoBack: {
                settingDlgLoader.exit()
            }
        }
    }

    Loader {
        id: settingLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        property bool useCustomControl: false
        property int playStatus: -1

        function exit()
        {
            screen.forceActiveFocus()

            opacity = 0.0
            settingHideTimer.restart()
        }

        Timer {
            id: settingHideTimer
            interval: settingOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined

                screen.setUseCustomControl(parent.useCustomControl)

                if (parent.playStatus !== RenderScreen.Paused && !screen.isAudio())
                    screen.resume()
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: settingOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0

            playStatus = screen.getStatus()

            if (playStatus !== RenderScreen.Paused && !screen.isAudio())
                screen.pause()

            useCustomControl = screen.getUseCustomControl()
            screen.setUseCustomControl(false)
        }

        Connections {
            ignoreUnknownSignals: true
            target: settingLoader.item

            onGoBack: {
                settingLoader.exit()
            }
        }
    }

    Loader {
        id: messageBoxLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        property Item focusParent: null

        function exit()
        {
            if (focusParent != null)
            {
                focusParent.forceActiveFocus()
                focusParent = null
            }

            opacity = 0.0
            messageBoxHideTimer.restart()
        }

        Timer {
            id: messageBoxHideTimer
            interval: messageBoxOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: messageBoxOpacityAni
                duration: 200
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: messageBoxLoader.item

            onGoBack: {
                messageBoxLoader.exit()
            }
        }
    }

    Loader {
        id: popupLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        property string desc
        property string link

        function exit()
        {
            opacity = 0.0
            popupHideTimer.restart()
        }

        Timer {
            id: popupHideTimer
            interval: popupOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Timer {
            id: popupShowTimer
            interval: 4000

            onTriggered: {
                popupLoader.exit()
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: popupOpacityAni
                duration: 500
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
            popupShowTimer.restart()
        }

        Connections {
            ignoreUnknownSignals: true
            target: popupLoader.item

            onGoBack: {
                popupLoader.exit()
            }
        }
    }

    Loader {
        id: epgLoader
        anchors.fill: parent
        visible: false
        opacity: 0.0
        focus: true

        function exit()
        {
            screen.forceActiveFocus()

            opacity = 0.0
            epgHideTimer.restart()
        }

        Timer {
            id: epgHideTimer
            interval: epgOpacityAni.duration

            onTriggered: {
                parent.visible = false
                parent.sourceComponent = undefined
            }
        }

        Behavior on opacity {
            NumberAnimation {
                id: epgOpacityAni
                duration: 500
                easing.type: Easing.OutQuad
            }
        }

        onLoaded: {
            visible = true
            opacity = 1.0
        }

        Connections {
            ignoreUnknownSignals: true
            target: epgLoader.item

            onGoBack: {
                epgLoader.exit()
            }
        }
    }

    Timer {
        id: fullScreenTimer
        interval: 100

        onTriggered: {
            screen.setFullScreen()
        }
    }

    Timer {
        id: headsetHookTimer
        interval: 500

        onTriggered: {
            switch (hookKeyCount)
            {
                case 1:
                    if (hookKeyPressed)
                    {
                        screen.hookKeyPressedProcessing = true
                        screen.forwardProper()

                        restart()
                    }
                    else
                    {
                        screen.toggle()
                        screen.hookKeyCount = 0
                    }

                    break
                case 2:
                    if (hookKeyPressed)
                    {
                        screen.hookKeyPressedProcessing = true
                        screen.rewindProper()

                        restart()
                    }
                    else
                    {
                        screen.next(true)
                        screen.hookKeyCount = 0
                    }

                    break
                case 3:
                    screen.prev(true)
                    screen.hookKeyCount = 0

                    break
            }
        }
    }

    function showMenu()
    {
        var showX = menu.x - screenMenu.menuWidth + menu.width - 10
        var showY = menu.y + 10

        screenMenu.show(showX, showY)
    }

    function showRepeatRange()
    {
        var showX
        var showY

        showX = seekBarPanel.width - repeatRange.windowWidth

        if (seekBarPanel.visible)
            showY = seekBarPanel.y - repeatRange.windowHeight
        else
            showY = screen.height - repeatRange.windowHeight

        if (repeatRange.visible)
            repeatRange.move(showX, showY)
        else
            repeatRange.show(showX, showY)
    }

    function setOptionDescYValue()
    {
        if (getVRInputSource() === AnyVODEnums.VRI_NONE)
            setOptionDescY(topBar.height + 3)
        else
            setOptionDescY(0)
    }

    function adjustSPDIFAudioUI()
    {
        var toggle = isUseSPDIF()

        muteButton.enabled = !toggle
    }

    onShowSubtitlePopup: {
        popupLoader.desc = qsTr("외부 서버에 자막이 존재합니다.\n이동하시려면 여기를 클릭하세요.")
        popupLoader.link = screen.getGOMSubtitleURL()
        popupLoader.sourceComponent = subtitlePopup
    }

    onViewLyrics: {
        resetLyrics()
        lyrics.visible = screen.existAudioSubtitle()
    }

    onFailedPlay: {
        closeAndGoBack()
    }

    onPlaying: {
        var pos = screen.getCurrentPosition()

        curTime.text = screen.getTimeString(pos)

        if (!seekBar.handlePressed)
            seekBar.value = pos
    }

    onStarted: {
        var dur = screen.getDuration()
        var playItemCount = screen.getPlayItemCount()
        var currItemIndex = screen.getCurrentPlayingIndex()

        curTime.text = screen.getTimeString(0.0)
        totalTime.text = screen.getTimeString(dur)

        seekBar.minimumValue = 0.0
        seekBar.maximumValue = dur

        if (playItemCount > 0)
            title.text.text = "[" + (currItemIndex + 1) + " / " + playItemCount + "] " + screen.getTitle()
        else
            title.text.text = screen.getTitle()

        title.restart()

        if (playItemCount <= 1)
        {
            prevButton.enabled = false
            nextButton.enabled = false
        }
        else if (currItemIndex - 1 < 0)
        {
            prevButton.enabled = false
            nextButton.enabled = true
        }
        else if (currItemIndex + 1 >= playItemCount)
        {
            prevButton.enabled = true
            nextButton.enabled = false
        }
        else
        {
            prevButton.enabled = true
            nextButton.enabled = true
        }

        noVideo.visible = !screen.isEnabledVideo()
        epgButton.visible = screen.isDTVOpened()
        touchArea.y = screen.isAudio() || Qt.platform.os === "linux" ? 0 : 20

        viewLyrics()

        fullScreenTimer.restart()
        fileListModel.setStatusBarStyle(false)

        calibrateRotation()
        vrInputSourceChanged()
        adjustSPDIFAudioUI()
    }

    onResumed: {
        resumeButton.visible = false
        pauseButton.visible = true
    }

    onPaused: {
        resumeButton.visible = true
        pauseButton.visible = false
    }

    onStopped: {
        title.text.text = ""

        resetLyrics()
    }

    onEnded: {
        resetLyrics()
    }

    onExit: {
        exitAtCppSide()
        fileListModel.setStatusBarStyle(true)

        goBack()
    }

    onRecovered: {
        noVideo.visible = !screen.isEnabledVideo()
    }

    onSpdifEnabled: {
        adjustSPDIFAudioUI()
    }

    onVrInputSourceChanged: {
        var before = rotation.active

        screen.determineToStartRotation()
        screen.determineToStartProximity()

        if (rotation.active)
        {
            if (!before)
                screen.needCalibrateRotation = true
        }
        else
        {
            screen.setHorizontalScreenOffset(0)
            screen.setVerticalScreenOffset(0)
        }
    }

    Component.onCompleted: {
        loadSettings()

        if (playListName.length > 0)
        {
            wantToPlay = true

            playListModel.setPlayList(playListName)
            setCurrentPlayingIndex(playListModel.getCurrentIndex())
        }
        else if (filePath.length > 0)
        {
            wantToPlay = open(filePath)
        }
    }

    Keys.onPressed: {
        if (event.key === 0)
            hookKeyPressed = true
    }

    Keys.onReleased: {
        switch (event.key)
        {
            case Qt.Key_Back:
                event.accepted = true
                closeAndGoBack()

                break
            case Qt.Key_MediaPlay:
                event.accepted = true
                resume()

                break
            case Qt.Key_MediaPause:
                event.accepted = true
                pause()

                break
            case Qt.Key_MediaStop:
                event.accepted = true
                stop()

                break
            case Qt.Key_MediaPrevious:
                event.accepted = true
                prev(true)

                break
            case Qt.Key_MediaNext:
                event.accepted = true
                next(true)

                break
            case Qt.Key_MediaTogglePlayPause:
                event.accepted = true
                toggle()

                break
            case 0:
                event.accepted = true
                hookKeyPressed = false

                if (hookKeyPressedProcessing)
                {
                    hookKeyCount = 0
                    hookKeyPressedProcessing = false
                }
                else
                {
                    hookKeyCount++
                    headsetHookTimer.restart()
                }

                break
        }
    }
}

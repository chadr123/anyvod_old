﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

import QtQuick 2.5
import com.dcple.anyvod 1.0

ListModel {
    ListElement {
        icon: "assets/search.png"
        text: qsTr("검색")
        menuID: AnyVODEnums.MID_SEARCH
    }

    ListElement {
        icon: "assets/open_playlist.png"
        text: qsTr("재생 목록")
        menuID: AnyVODEnums.MID_MANAGE_PLAY_LIST
    }

    ListElement {
        icon: "assets/external_path.png"
        text: qsTr("외부 열기")
        menuID: AnyVODEnums.MID_OPEN_EXTERNAL
    }

    ListElement {
        icon: "assets/dtv.png"
        text: qsTr("DTV 열기")
        menuID: AnyVODEnums.MID_OPEN_DTV
    }

    ListElement {
        icon: "assets/equalizer.png"
        text: qsTr("이퀄라이저")
        menuID: AnyVODEnums.MID_EQUALIZER
    }

    ListElement {
        icon: "assets/setting.png"
        text: qsTr("설정")
        menuID: AnyVODEnums.MID_SETTINGS
    }
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include <QGuiApplication>

#ifdef Q_OS_IOS
#include "IOSDelegate.h"
#endif

#include "AnyVODWindow.h"
#include "../models/FileListModel.h"
#include "../models/PlayListModel.h"
#include "../models/DTVListModel.h"
#include "../models/EPGListModel.h"
#include "../models/SearchMediaModel.h"
#include "../models/ManagePlayListModel.h"
#include "../ui/RenderScreen.h"
#include "../ui/MediaDelegate.h"
#include "../ui/DTVDelegate.h"
#include "media/LastPlay.h"
#include "core/Settings.h"
#include "core/Utils.h"

#include "../../../../common/version.h"

#include <QString>
#include <QDir>
#include <QFontDatabase>
#include <QTranslator>
#include <QDebug>

int main(int argc, char *argv[])
{
#ifdef Q_OS_RASPBERRY_PI
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));
    qputenv("QT_QPA_EGLFS_FORCE888", QByteArray("1"));
#ifndef Q_PROCESSOR_X86
    qputenv("QT_QPA_PLATFORM", QByteArray("eglfs"));
#endif
#endif

    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    qRegisterMetaTypeStreamOperators<PlayItem>("PlayItem");
    qRegisterMetaTypeStreamOperators<MediaPresenter::Range>("MediaPresenter::Range");
    qRegisterMetaTypeStreamOperators<LastPlay::Item>("LastPlay::Item");
    qRegisterMetaTypeStreamOperators<RenderScreen::EqualizerItem>("RenderScreen::EqualizerItem");
    qRegisterMetaTypeStreamOperators<MediaPresenter::Range>("MediaPresenter::Range");
    qRegisterMetaTypeStreamOperators<DTVReader::ChannelInfo>("DTVReader::ChannelInfo");

    qmlRegisterType<FileListModel>("com.dcple.anyvod", 1, 0, "FileListModel");
    qmlRegisterType<PlayListModel>("com.dcple.anyvod", 1, 0, "PlayListModel");
    qmlRegisterType<DTVListModel>("com.dcple.anyvod", 1, 0, "DTVListModel");
    qmlRegisterType<EPGListModel>("com.dcple.anyvod", 1, 0, "EPGListModel");
    qmlRegisterType<SearchMediaModel>("com.dcple.anyvod", 1, 0, "SearchMediaModel");
    qmlRegisterType<ManagePlayListModel>("com.dcple.anyvod", 1, 0, "ManagePlayListModel");

    qmlRegisterType<MediaDelegate>("com.dcple.anyvod", 1, 0, "MediaDelegate");
    qmlRegisterType<DTVDelegate>("com.dcple.anyvod", 1, 0, "DTVDelegate");

    qmlRegisterType<RenderScreen>("com.dcple.anyvod", 1, 0, "RenderScreen");
    qmlRegisterType<AnyVODEnums>("com.dcple.anyvod", 1, 0, "AnyVODEnums");

    QSurfaceFormat format = QSurfaceFormat::defaultFormat();

    format.setRenderableType(QSurfaceFormat::OpenGLES);

    QSurfaceFormat::setDefaultFormat(format);

    QGuiApplication app(argc, argv);
    AnyVODWindow viewer;

#if defined Q_OS_RASPBERRY_PI && defined QT_NO_DEBUG
    QDir::setCurrent(app.applicationDirPath());
#endif

#ifdef Q_OS_IOS
    IOSDelegate::initHeadsetConnectivity();
#endif

    QDir fonts(ASSETS_PREFIX"/fonts");
    QString defaultFamily;
    QString fontPath;

    foreach (const QString &font, fonts.entryList(QDir::NoDotAndDotDot | QDir::Files))
    {
        int id;

        fontPath = fonts.absoluteFilePath(font);
        id = QFontDatabase::addApplicationFont(fontPath);

        if (id < 0)
            continue;

        QStringList families = QFontDatabase::applicationFontFamilies(id);

        if (families.isEmpty())
            continue;

        defaultFamily = families.first();
    }

    Utils::FontInfo fontInfo;

    fontInfo.path = fontPath;
    fontInfo.family = defaultFamily;

    Utils::setDefaultFont(fontInfo);

    QSettings *settings = new QSettings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);
    QFont font;
    QString family = settings->value(SETTING_FONT, defaultFamily).toString();

    if (!family.isEmpty())
    {
        font.setFamily(family);

        settings->setValue(SETTING_FONT, family);
    }

    QString localName = QLocale::system().name();
    QString localLang = localName.split('_').first();
    QString templeate = "%1_%2";
    QString lang = settings->value(SETTING_LANGUAGE, localLang).toString();
    QTranslator trans;

    if (trans.load(templeate.arg(FileListModel::LANG_PREFIX).arg(lang), FileListModel::LANG_DIR))
    {
        app.installTranslator(&trans);
        settings->setValue(SETTING_LANGUAGE, lang);
    }

    delete settings;

    srand(time(NULL));

    app.setApplicationName("anyvod");
    app.setOrganizationDomain(COMPANY);
    app.setOrganizationName(COMPANY);
    app.setFont(font);

    viewer.setMainQmlFile(QStringLiteral("qml/AnyVODMobileClient/main.qml"));
    viewer.showExpanded();

    int ret = app.exec();

#ifdef Q_OS_IOS
    IOSDelegate::unInitHeadsetConnectivity();
#endif

    return ret;
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "AnyVODWindow.h"

#include <QDebug>

AnyVODWindow::AnyVODWindow() :
    m_useCustomControl(false),
    m_accept(true)
{

}

void AnyVODWindow::setUseCustomControl(bool use)
{
    this->m_useCustomControl = use;
}

bool AnyVODWindow::getUseCustomControl() const
{
    return this->m_useCustomControl;
}

void AnyVODWindow::setAcceptEvent(bool accept)
{
    this->m_accept = accept;
}

bool AnyVODWindow::event(QEvent *event)
{
    if (this->m_useCustomControl)
    {
        if (event->type() == QEvent::UpdateRequest)
        {
            if (this->m_accept)
            {
                event->accept();
                return true;
            }
        }
    }

    return QQuickView::event(event);
}

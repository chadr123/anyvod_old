﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include <objc/objc.h>

#include "IOSDelegate.h"
#include "../ui/RenderScreen.h"

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

bool IOSDelegate::m_isPauseByNative = false;

void *IOSDelegate::m_routeChangeObserver = nullptr;
void *IOSDelegate::m_playTarget = nullptr;
void *IOSDelegate::m_pauseTarget = nullptr;
void *IOSDelegate::m_toggleTarget = nullptr;
void *IOSDelegate::m_stopTarget = nullptr;
void *IOSDelegate::m_prevTarget = nullptr;
void *IOSDelegate::m_nextTarget = nullptr;
void *IOSDelegate::m_backwardTarget = nullptr;
void *IOSDelegate::m_forwardTarget = nullptr;

static UIImage *s_cover = nullptr;

void IOSDelegate::disableIdleTimer()
{
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

void IOSDelegate::enableIdleTimer()
{
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

void IOSDelegate::setStatusBarStyle(bool enable, int color)
{
    UIApplication *app = [UIApplication sharedApplication];
    UIView *statusBar = [[app valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];

    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)])
    {
        [app setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];

        if (enable)
            statusBar.backgroundColor = [UIColor colorWithRed:((float)((color & 0xff0000) >> 16)) / 255.0
                    green:((float)((color & 0x00ff00) >> 8)) / 255.0
                    blue:((float)((color & 0x0000ff) >> 0)) / 255.0
                    alpha:1.0];
        else
            statusBar.backgroundColor = [UIColor blackColor];
    }
}

void IOSDelegate::initHeadsetConnectivity()
{
    void (^routeChangeBlock)(NSNotification*) =
          ^(NSNotification *notification)
        {
            RenderScreen *screen = RenderScreen::getSelf();

            if (!screen)
                return;

            NSNumber *reasonNumber = notification.userInfo[AVAudioSessionRouteChangeReasonKey];
            AVAudioSessionRouteChangeReason reason = (AVAudioSessionRouteChangeReason)reasonNumber.unsignedIntegerValue;

            switch (reason)
            {
                case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
                {
                    if (!IOSDelegate::m_isPauseByNative)
                        return;

                    if (screen->getStatus() == RenderScreen::Paused)
                    {
                        IOSDelegate::m_isPauseByNative = false;
                        screen->resume();
                    }

                    break;
                }
                case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
                {
                    if (IOSDelegate::m_isPauseByNative)
                        return;

                    if (screen->getStatus() == RenderScreen::Playing || screen->getStatus() == RenderScreen::Started)
                    {
                        IOSDelegate::m_isPauseByNative = true;
                        screen->pause();
                    }

                    break;
                }
                default:
                {
                    break;
                }
            }
        };

    AVAudioSession *audioSession = [AVAudioSession sharedInstance];

    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setActive:YES error:nil];

    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    id routeChangeObserver = [center addObserverForName:AVAudioSessionRouteChangeNotification
                              object:nil
                              queue:[NSOperationQueue mainQueue]
                              usingBlock:routeChangeBlock];

    IOSDelegate::m_routeChangeObserver = (__bridge_retained void*)routeChangeObserver;
}

void IOSDelegate::unInitHeadsetConnectivity()
{
    if (IOSDelegate::m_routeChangeObserver != nullptr)
    {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        id observer = (__bridge_transfer id)IOSDelegate::m_routeChangeObserver;

        [center removeObserver:observer];
        IOSDelegate::m_routeChangeObserver = nullptr;

        AVAudioSession *audioSession = [AVAudioSession sharedInstance];

        [audioSession setActive:NO error:nil];
    }
}

void IOSDelegate::registerMediaCenterCommand()
{
    if (![MPRemoteCommandCenter class])
    {
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        return;
    }

    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];
    RenderScreen *screen = RenderScreen::getSelf();
    BOOL enableSkip = screen->getPlayItemCount() <= 1;

    if (!screen)
        return;

    commandCenter.playCommand.enabled = YES;
    id playTarget = [commandCenter.playCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        screen->resume();
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    IOSDelegate::m_playTarget = (__bridge_retained void*)playTarget;

    commandCenter.pauseCommand.enabled = YES;
    id pauseTarget = [commandCenter.pauseCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        screen->pause();
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    IOSDelegate::m_pauseTarget = (__bridge_retained void*)pauseTarget;

    commandCenter.togglePlayPauseCommand.enabled = YES;
    id toggleTarget = [commandCenter.togglePlayPauseCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        screen->toggle();
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    IOSDelegate::m_toggleTarget = (__bridge_retained void*)toggleTarget;

    commandCenter.stopCommand.enabled = YES;
    id stopTarget = [commandCenter.stopCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        screen->closeAndGoBack();
        return MPRemoteCommandHandlerStatusSuccess;
    }];
    IOSDelegate::m_stopTarget = (__bridge_retained void*)stopTarget;

    id prevTarget = [commandCenter.previousTrackCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        return screen->prev(false) ? MPRemoteCommandHandlerStatusSuccess : MPRemoteCommandHandlerStatusNoSuchContent;
    }];
    IOSDelegate::m_prevTarget = (__bridge_retained void*)prevTarget;

    id nextTarget = [commandCenter.nextTrackCommand addTargetWithHandler:^(MPRemoteCommandEvent *)
    {
        return screen->next(false) ? MPRemoteCommandHandlerStatusSuccess : MPRemoteCommandHandlerStatusNoSuchContent;
    }];
    IOSDelegate::m_nextTarget = (__bridge_retained void*)nextTarget;

    commandCenter.skipBackwardCommand.enabled = enableSkip;
    commandCenter.skipBackwardCommand.preferredIntervals = @[screen->isAudio() ? @(5.0) : @(30.0)];
    id backwardTarget = [commandCenter.skipBackwardCommand addTargetWithHandler:^(MPRemoteCommandEvent *event)
    {
        if ([event isKindOfClass:[MPSkipIntervalCommandEvent class]])
        {
            MPSkipIntervalCommandEvent *skipEvent = (MPSkipIntervalCommandEvent*)event;

            screen->rewind(skipEvent.interval);

            return MPRemoteCommandHandlerStatusSuccess;
        }
        else
        {
            return MPRemoteCommandHandlerStatusCommandFailed;
        }
    }];
    IOSDelegate::m_backwardTarget = (__bridge_retained void*)backwardTarget;

    commandCenter.skipForwardCommand.enabled = enableSkip;
    commandCenter.skipForwardCommand.preferredIntervals = @[screen->isAudio() ? @(5.0) : @(30.0)];
    id forwardTarget = [commandCenter.skipForwardCommand addTargetWithHandler:^(MPRemoteCommandEvent *event)
    {
        if ([event isKindOfClass:[MPSkipIntervalCommandEvent class]])
        {
            MPSkipIntervalCommandEvent *skipEvent = (MPSkipIntervalCommandEvent*)event;

            screen->forward(skipEvent.interval);

            return MPRemoteCommandHandlerStatusSuccess;
        }
        else
        {
            return MPRemoteCommandHandlerStatusCommandFailed;
        }
    }];
    IOSDelegate::m_forwardTarget = (__bridge_retained void*)forwardTarget;

    int totalCount = screen->getPlayItemCount();
    int curIndex = screen->getCurrentPlayingIndex();

    if (totalCount <= 1)
    {
        commandCenter.previousTrackCommand.enabled = NO;
        commandCenter.nextTrackCommand.enabled = NO;
    }
    else if (curIndex - 1 < 0)
    {
        commandCenter.previousTrackCommand.enabled = NO;
        commandCenter.nextTrackCommand.enabled = YES;
    }
    else if (curIndex + 1 >= totalCount)
    {
        commandCenter.previousTrackCommand.enabled = YES;
        commandCenter.nextTrackCommand.enabled = NO;
    }
    else
    {
        commandCenter.previousTrackCommand.enabled = YES;
        commandCenter.nextTrackCommand.enabled = YES;
    }

    s_cover = [UIImage imageWithContentsOfFile:screen->getLargeCoverFilePath().toNSString()];
}

void IOSDelegate::unRegisterMediaCenterCommand()
{
    if (![MPRemoteCommandCenter class])
    {
        [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
        return;
    }

    MPRemoteCommandCenter *commandCenter = [MPRemoteCommandCenter sharedCommandCenter];

    if (IOSDelegate::m_playTarget != nullptr)
    {
        id playTarget = (__bridge_transfer id)IOSDelegate::m_playTarget;

        [commandCenter.playCommand removeTarget:playTarget];
        IOSDelegate::m_playTarget = nullptr;
    }

    if (IOSDelegate::m_pauseTarget != nullptr)
    {
        id pauseTarget = (__bridge_transfer id)IOSDelegate::m_pauseTarget;

        [commandCenter.pauseCommand removeTarget:pauseTarget];
        IOSDelegate::m_pauseTarget = nullptr;
    }

    if (IOSDelegate::m_toggleTarget != nullptr)
    {
        id toggleTarget = (__bridge_transfer id)IOSDelegate::m_toggleTarget;

        [commandCenter.togglePlayPauseCommand removeTarget:toggleTarget];
        IOSDelegate::m_toggleTarget = nullptr;
    }

    if (IOSDelegate::m_stopTarget != nullptr)
    {
        id stopTarget = (__bridge_transfer id)IOSDelegate::m_stopTarget;

        [commandCenter.stopCommand removeTarget:stopTarget];
        IOSDelegate::m_stopTarget = nullptr;
    }

    if (IOSDelegate::m_prevTarget != nullptr)
    {
        id prevTarget = (__bridge_transfer id)IOSDelegate::m_prevTarget;

        [commandCenter.previousTrackCommand removeTarget:prevTarget];
        IOSDelegate::m_prevTarget = nullptr;
    }

    if (IOSDelegate::m_nextTarget != nullptr)
    {
        id nextTarget = (__bridge_transfer id)IOSDelegate::m_nextTarget;

        [commandCenter.nextTrackCommand removeTarget:nextTarget];
        IOSDelegate::m_nextTarget = nullptr;
    }

    if (IOSDelegate::m_backwardTarget != nullptr)
    {
        id backwardTarget = (__bridge_transfer id)IOSDelegate::m_backwardTarget;

        [commandCenter.skipBackwardCommand removeTarget:backwardTarget];
        IOSDelegate::m_backwardTarget = nullptr;
    }

    if (IOSDelegate::m_forwardTarget != nullptr)
    {
        id forwardTarget = (__bridge_transfer id)IOSDelegate::m_forwardTarget;

        [commandCenter.skipForwardCommand removeTarget:forwardTarget];
        IOSDelegate::m_forwardTarget = nullptr;
    }

    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = nil;

    s_cover = nullptr;
}

void IOSDelegate::updateNowPlayingInfo()
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    double tempo = 1.0 + screen->getTempo() / 100.0;
    MPMediaItemArtwork *cover = (s_cover == nil ? nil : [[MPMediaItemArtwork alloc] initWithImage:s_cover]);

    info[MPMediaItemPropertyPlaybackDuration] = @(screen->getDuration());
    info[MPMediaItemPropertyTitle] = screen->getTitle().toNSString();

    info[MPNowPlayingInfoPropertyElapsedPlaybackTime] = @(screen->getCurrentPosition());
    info[MPNowPlayingInfoPropertyPlaybackRate] = @(screen->getStatus() == RenderScreen::Playing ? tempo : 0.0);
    info[MPNowPlayingInfoPropertyPlaybackQueueIndex] = @((NSUInteger)screen->getCurrentPlayingIndex());
    info[MPNowPlayingInfoPropertyPlaybackQueueCount] = @((NSUInteger)screen->getPlayItemCount());

    if (cover != nil)
        info[MPMediaItemPropertyArtwork] = cover;

    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = info;
}

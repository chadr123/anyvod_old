﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "IOSViewController.h"
#include "../ui/RenderScreen.h"

#include <QDebug>

@implementation QIOSViewController (IOSViewController)

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];

    [self becomeFirstResponder];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    [self resignFirstResponder];
}

- (BOOL) canBecomeFirstResponder
{
    return YES;
}

- (void) remoteControlReceivedWithEvent:(UIEvent *)event
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (event.type == UIEventTypeRemoteControl)
    {
        switch (event.subtype)
        {
            case UIEventSubtypeRemoteControlTogglePlayPause:
                screen->toggle();
                break;
            case UIEventSubtypeRemoteControlPause:
                screen->pause();
                break;
            case UIEventSubtypeRemoteControlPlay:
                screen->resume();
                break;
            case UIEventSubtypeRemoteControlPreviousTrack:
                screen->prev(false);
                break;
            case UIEventSubtypeRemoteControlNextTrack:
                screen->next(false);
                break;
            default:
                break;
        }
    }
}

@end

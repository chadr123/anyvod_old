﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "DTVDelegate.h"
#include "../models/DTVListModel.h"

#include "core/Settings.h"
#include "core/Utils.h"

#include <QSettings>

DTVDelegate::DTVDelegate() :
    m_model(NULL)
{
    this->loadChannelInfo();
}

DTVDelegate::~DTVDelegate()
{

}

void DTVDelegate::init()
{
    this->findModel();
}

void DTVDelegate::scan(int adapter, int systemType, int country, int start, int end)
{
    this->m_model->clear();
    this->m_model->update(adapter, (DTVReaderInterface::SystemType)systemType, (QLocale::Country)country, start, end);
}

void DTVDelegate::stop()
{
    this->m_model->stop();
}

void DTVDelegate::save()
{
    this->m_reader.setScannedChannels(this->m_model->getChannelInfo());
    this->saveChannelInfo();
}

QString DTVDelegate::getPath(int index)
{
#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
    QVector<DTVReader::ChannelInfo> infos = this->m_model->getChannelInfo();

    return Utils::makeDTVPath(infos[index]);
#else
    (void)index;

    return QString();
#endif
}

int DTVDelegate::getAdapterInfoCount()
{
    QVector<DTVReaderInterface::Info> list;

    this->m_reader.getAdapterList(&list);

    return list.count();
}

QString DTVDelegate::getAdapterInfo(int index)
{
    DTVReaderInterface::Info info;

    if (this->m_reader.getAdapterInfo(index, &info))
        return info.name;
    else
        return QString();
}

int DTVDelegate::getAdapterInfoValue(int index)
{
    DTVReaderInterface::Info info;

    if (this->m_reader.getAdapterInfo(index, &info))
        return info.index;
    else
        return -1;
}

int DTVDelegate::getSystemTypeInfoCount() const
{
    return this->getSystemTypeList().count();
}

QString DTVDelegate::getSystemTypeInfoDesc(int index) const
{
    QVector<DTVReaderInterface::SystemTypeInfo> list = this->getSystemTypeList();

    return list[index].name;
}

int DTVDelegate::getSystemTypeInfoValue(int index) const
{
    QVector<DTVReaderInterface::SystemTypeInfo> list = this->getSystemTypeList();

    return list[index].type;
}

int DTVDelegate::getCountryInfoCount() const
{
    return this->getCountryList().count();
}

QString DTVDelegate::getCountryInfoDesc(int index) const
{
    QVector<DTVReaderInterface::CountryInfo> list = this->getCountryList();

    return list[index].name;
}

int DTVDelegate::getCountryInfoValue(int index) const
{
    QVector<DTVReaderInterface::CountryInfo> list = this->getCountryList();

    return list[index].country;
}

int DTVDelegate::getCurrentAdapterValue()
{
    return this->getAdapterInfoValue(0);
}

int DTVDelegate::getCurrentSystemTypeValue()
{
    QLocale::Country country;
    DTVReaderInterface::SystemType type;

    this->m_reader.getChannelCountry(&country, &type);

    return type;
}

int DTVDelegate::getCurrentCountryValue()
{
    QLocale::Country country;
    DTVReaderInterface::SystemType type;

    this->m_reader.getChannelCountry(&country, &type);

    return country;
}

bool DTVDelegate::setChannelCountry(int country, int type)
{
    return this->m_reader.setChannelCountry((QLocale::Country)country, (DTVReaderInterface::SystemType)type);
}

int DTVDelegate::getChannelCount()
{
    QVector<DTVChannelMap::Channel> channels;

    this->m_reader.getChannels(&channels);

    return channels.count();
}

int DTVDelegate::getChannelFirst()
{
    QVector<DTVChannelMap::Channel> channels;

    this->m_reader.getChannels(&channels);

    return channels.first().channel;
}

int DTVDelegate::getChannelLast()
{
    QVector<DTVChannelMap::Channel> channels;

    this->m_reader.getChannels(&channels);

    return channels.last().channel;
}

QVector<DTVReaderInterface::SystemTypeInfo> DTVDelegate::getSystemTypeList() const
{
    QVector<DTVReaderInterface::SystemTypeInfo> list;

    for (int i = DTVReaderInterface::ST_NONE + 1; i < DTVReaderInterface::ST_COUNT; i++)
    {
        QString name = this->m_reader.systemTypeToString((DTVReaderInterface::SystemType)i);

        if (!name.isEmpty())
        {
            DTVReaderInterface::SystemTypeInfo item;

            item.name = name;
            item.type = (DTVReaderInterface::SystemType)i;

            list.append(item);
        }
    }

    return list;
}

QVector<DTVReaderInterface::CountryInfo> DTVDelegate::getCountryList() const
{
    QVector<DTVReaderInterface::CountryInfo> list;

    for (int i = QLocale::AnyCountry + 1; i < QLocale::LastCountry; i++)
    {
        QString name = QLocale::countryToString((QLocale::Country)i);

        if (!name.isEmpty())
        {
            DTVReaderInterface::CountryInfo item;

            item.name = name;
            item.country = (QLocale::Country)i;

            list.append(item);
        }
    }

    return list;
}

void DTVDelegate::findModel()
{
    if (!this->m_model)
    {
        QVector<DTVReader::ChannelInfo> list;

        this->m_reader.getScannedChannels(&list);

        this->m_model = this->findChild<DTVListModel*>("dtvListModel");

        this->m_model->setReader(&this->m_reader);
        this->m_model->setChannelInfo(list);
    }
}

void DTVDelegate::loadChannelInfo()
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);
    QVariantList list = settings.value(SETTING_DTV_SCANNED_CHANNELS, QVariantList()).toList();
    QVector<DTVReader::ChannelInfo> channels;

    for (int i = 0; i < list.count(); i++)
        channels.append(list[i].value<DTVReader::ChannelInfo>());

    this->m_reader.setScannedChannels(channels);

    QLocale::Country country = (QLocale::Country)settings.value(SETTING_DTV_CURRENT_COUNTRY, QLocale::SouthKorea).toInt();
    DTVReaderInterface::SystemType type = (DTVReaderInterface::SystemType)settings.value(SETTING_DTV_CURRENT_TYPE, DTVReaderInterface::ST_ATSC_T).toInt();

    this->m_reader.setChannelCountry(country, type);
}

void DTVDelegate::saveChannelInfo()
{
    QVariantList list;
    QVector<DTVReader::ChannelInfo> channels;
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);

    this->m_reader.getScannedChannels(&channels);

    for (int i = 0; i < channels.length(); i++)
        list.append(qVariantFromValue(channels[i]));

    settings.setValue(SETTING_DTV_SCANNED_CHANNELS, list);

    QLocale::Country country;
    DTVReaderInterface::SystemType type;

    this->m_reader.getChannelCountry(&country, &type);

    settings.setValue(SETTING_DTV_CURRENT_COUNTRY, country);
    settings.setValue(SETTING_DTV_CURRENT_TYPE, type);
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "device/DTVReader.h"

#include <QQuickItem>

class DTVListModel;

class DTVDelegate : public QQuickItem
{
    Q_OBJECT
public:
    DTVDelegate();
    ~DTVDelegate();

    Q_INVOKABLE void init();
    Q_INVOKABLE void scan(int adapter, int systemType, int country, int start, int end);
    Q_INVOKABLE void stop();
    Q_INVOKABLE void save();

    Q_INVOKABLE QString getPath(int index);

    Q_INVOKABLE int getAdapterInfoCount();
    Q_INVOKABLE QString getAdapterInfo(int index);
    Q_INVOKABLE int getAdapterInfoValue(int index);

    Q_INVOKABLE int getSystemTypeInfoCount() const;
    Q_INVOKABLE QString getSystemTypeInfoDesc(int index) const;
    Q_INVOKABLE int getSystemTypeInfoValue(int index) const;

    Q_INVOKABLE int getCountryInfoCount() const;
    Q_INVOKABLE QString getCountryInfoDesc(int index) const;
    Q_INVOKABLE int getCountryInfoValue(int index) const;

    Q_INVOKABLE int getCurrentAdapterValue();
    Q_INVOKABLE int getCurrentSystemTypeValue();
    Q_INVOKABLE int getCurrentCountryValue();

    Q_INVOKABLE bool setChannelCountry(int country, int type);
    Q_INVOKABLE int getChannelCount();
    Q_INVOKABLE int getChannelFirst();
    Q_INVOKABLE int getChannelLast();

    QVector<DTVReaderInterface::SystemTypeInfo> getSystemTypeList() const;
    QVector<DTVReaderInterface::CountryInfo> getCountryList() const;

private:
    void findModel();
    void loadChannelInfo();
    void saveChannelInfo();

private:
    DTVReader m_reader;
    DTVListModel *m_model;
};

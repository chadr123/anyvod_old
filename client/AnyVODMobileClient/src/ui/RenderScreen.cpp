﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "RenderScreen.h"
#include "core/Settings.h"
#include "net/SubtitleImpl.h"
#include "device/DTVReader.h"
#include "media/FrameExtractor.h"
#include "../core/AnyVODWindow.h"
#include "../../../../common/SubtitleFileNameGenerator.h"
#include "../../../../common/version.h"

#include <QQuickWindow>
#include <QQmlProperty>
#include <QGuiApplication>
#include <QDir>
#include <QTemporaryFile>
#include <QTextCodec>

#ifdef Q_OS_ANDROID
#include <QAndroidJniObject>
#include <QtAndroid>
#endif

#ifdef Q_OS_IOS
#include "../core/IOSDelegate.h"
#endif

#include <QDebug>

const QEvent::Type RenderScreen::PLAYING_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RenderScreen::EMPTY_BUFFER_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RenderScreen::AUDIO_OPTION_DESC_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RenderScreen::AUDIO_SUBTITLE_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type RenderScreen::ABORT_EVENT = (QEvent::Type)QEvent::registerEventType();

const QString RenderScreen::MOVIE_EXTS = QString::fromStdWString(VIDEO_EXTENSION);
const QString RenderScreen::AUDIO_EXTS = QString::fromStdWString(AUDIO_EXTENSION);
const QString RenderScreen::SUBTITLE_EXTS = QString::fromStdWString(SUBTITLE_EXTENSION);
const QString RenderScreen::PLAYLIST_EXTS = QString::fromStdWString(PLAYLIST_EXTENSION);
const QString RenderScreen::FONT_EXTS = QString::fromStdWString(FONT_EXTENSION);
const QStringList RenderScreen::MOVIE_EXTS_LIST = MOVIE_EXTS.split(" ", QString::SkipEmptyParts);
const QStringList RenderScreen::AUDIO_EXTS_LIST = AUDIO_EXTS.split(" ", QString::SkipEmptyParts);
const QStringList RenderScreen::SUBTITLE_EXTS_LIST = SUBTITLE_EXTS.split(" ", QString::SkipEmptyParts);
const QStringList RenderScreen::PLAYLIST_EXTS_LIST = PLAYLIST_EXTS.split(" ", QString::SkipEmptyParts);
const QStringList RenderScreen::MEDIA_EXTS_LIST = MOVIE_EXTS_LIST + AUDIO_EXTS_LIST + PLAYLIST_EXTS_LIST;
const QStringList RenderScreen::ALL_EXTS_LIST = MOVIE_EXTS_LIST + AUDIO_EXTS_LIST + SUBTITLE_EXTS_LIST + PLAYLIST_EXTS_LIST;
const QString RenderScreen::CAPTURE_FORMAT = QString::fromStdWString(CAPTURE_FORMAT_EXTENSION);
const QStringList RenderScreen::CAPTURE_FORMAT_LIST = CAPTURE_FORMAT.split(" ", QString::SkipEmptyParts);
const QStringList RenderScreen::FONT_EXTS_LIST = FONT_EXTS.split(" ", QString::SkipEmptyParts);

RenderScreen* RenderScreen::m_self = NULL;
QMutex RenderScreen::m_selfLock;

#ifdef Q_OS_RASPBERRY_PI
const char RenderScreen::DBUS_SERVICE[][40] =
{
    "org.freedesktop.ScreenSaver",
    "org.freedesktop.PowerManagement.Inhibit",
    "org.mate.SessionManager",
    "org.gnome.SessionManager",
};

const char RenderScreen::DBUS_PATH[][33] =
{
    "/ScreenSaver",
    "/org/freedesktop/PowerManagement",
    "/org/mate/SessionManager",
    "/org/gnome/SessionManager",
};
#endif

QDataStream& operator << (QDataStream &out, const RenderScreen::EqualizerItem &item)
{
    out << item.gain;

    return out;
}

QDataStream& operator >> (QDataStream &in, RenderScreen::EqualizerItem &item)
{
    in >> item.gain;

    return in;
}

RenderScreen::RenderScreen() :
    m_renderer(&this->m_presenter, this),
    m_presenter(0, 0),
    m_wantToPlay(false),
    m_status(Stopped),
    m_gotoLastPlay(false),
    m_priorSubtitleDirectory(false),
    m_useSearchSubtitleComplex(false),
    m_settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat),
    m_playingMethod(AnyVODEnums::PM_TOTAL),
    m_pausedByInactive(false),
    m_isLandscape(false),
    m_useHeadTracking(false),
    m_subtitleSize(-1.0f),
    m_currentPlayingIndex(-1),
    m_playList(NULL),
    m_statusUpdater(this),
#if defined Q_OS_RASPBERRY_PI
    m_dbusConn(NULL),
    m_dbusPending(NULL),
    m_dbusCookie(0),
    m_screenSaverAPI(SSAPI_COUNT),
#endif
#ifdef Q_OS_IOS
    m_isPausedByIOSNotify(false),
#endif
    m_lyrics1(NULL),
    m_lyrics2(NULL),
    m_lyrics3(NULL),
    m_empty(NULL)
{
    connect(qApp, SIGNAL(applicationStateChanged(Qt::ApplicationState)), this, SLOT(handleApplicationStateChanged(Qt::ApplicationState)));
    connect(this, SIGNAL(windowChanged(QQuickWindow*)), this, SLOT(handleWindowChanged(QQuickWindow*)));
    connect(this, SIGNAL(exitFromRenderer()), this, SLOT(handleExitFromRenderer()));

#ifdef Q_OS_IOS
    this->m_presenter.setIOSNotifyCallback(RenderScreen::iosNotifyCallback);
#endif

    MediaPresenter::EventCallback endedcb;
    MediaPresenter::EventCallback playingcb;

    endedcb.callback = RenderScreen::endedCallback;
    endedcb.userData = this;

    playingcb.callback = RenderScreen::playingCallback;
    playingcb.userData = this;

    this->m_presenter.setStatusChangedCallback(&playingcb, &endedcb);

    MediaPresenter::EmptyBufferCallback ecb;

    ecb.callback = RenderScreen::emptyBufferCallback;
    ecb.userData = this;
    this->m_presenter.setEmptyBufferCallback(ecb);

    MediaPresenter::ShowAudioOptionDescCallback acb;

    acb.callback = RenderScreen::showAudioOptionDescCallback;
    acb.userData = this;
    this->m_presenter.setShowAudioOptionDescCallback(acb);

    MediaPresenter::AudioSubtitleCallback asc;

    asc.callback = RenderScreen::audioSubtitleCallback;
    asc.userData = this;
    this->m_presenter.setAudioSubtitleCallback(asc);

    MediaPresenter::PaintCallback pcb;

    pcb.callback = RenderScreen::paintCallback;
    pcb.userData = this;
    this->m_presenter.setPaintCallback(pcb);

    MediaPresenter::AbortCallback atcb;

    atcb.callback = RenderScreen::abortCallback;
    atcb.userData = this;
    this->m_presenter.setAbortCallback(atcb);

    MediaPresenter::EventCallback rccb;

    rccb.callback = RenderScreen::recoverCallback;
    rccb.userData = this;
    this->m_presenter.setRecoverCallback(rccb);

    this->setup3DMethod();
    this->setupSubtitle3DMethod();
    this->setupAnaglyphAlgorithm();
    this->setupDeinterlaceMethod();
    this->setupVRInputSource();
}

RenderScreen::~RenderScreen()
{
    this->close();
}

#ifdef Q_OS_RASPBERRY_PI
bool RenderScreen::debusInit()
{
    DBusError err;

    dbus_error_init(&err);

    this->m_dbusConn = dbus_bus_get_private(DBUS_BUS_SESSION, &err);

    if (this->m_dbusConn == NULL)
    {
        dbus_error_free(&err);
        return false;
    }

    for (int i = 0; i < SSAPI_COUNT; i++)
    {
        if (dbus_bus_name_has_owner(this->m_dbusConn, DBUS_SERVICE[i], NULL))
        {
            this->m_screenSaverAPI = (ScreenSaverAPI)i;
            return true;
        }
    }

    return false;
}

void RenderScreen::debusDeinit()
{
    if (this->m_dbusPending != NULL)
    {
        dbus_pending_call_cancel(this->m_dbusPending);
        dbus_pending_call_unref(this->m_dbusPending);

        this->m_dbusPending = NULL;
    }

    if (this->m_dbusConn != NULL)
    {
        dbus_connection_close(this->m_dbusConn);
        dbus_connection_unref(this->m_dbusConn);

        this->m_dbusConn = NULL;
    }
}
#endif

bool RenderScreen::open(const QString &path, ExtraPlayData &data, const QUuid &unique)
{
    QFileInfo info(path);
    QString fontFamily = QGuiApplication::font().family();
    PlayItem item = this->m_playList->getPlayItem(this->m_playList->findIndex(unique));
    QString audioPath;

    this->close();

    RenderScreen::m_selfLock.lock();
    RenderScreen::m_self = this;
    RenderScreen::m_selfLock.unlock();

    this->m_presenter.initProtocol();

#ifdef Q_OS_RASPBERRY_PI
    if (!this->debusInit())
    {
        this->debusDeinit();
        this->m_xdgDisabler.start();
    }
#endif

    QString cacheDir = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    Utils::FontInfo fontInfo = Utils::getDefaultFont();
    QFile fontFile(fontInfo.path);
    QString fontPath = cacheDir + QDir::separator() + QFileInfo(fontInfo.path).fileName();
    bool existFont = false;

    if (QFileInfo::exists(fontPath))
        existFont = true;
    else
        existFont = fontFile.copy(fontPath);

    if (existFont)
    {
        this->m_presenter.setASSFontPath(fontPath);
        this->m_presenter.setASSFontFamily(fontInfo.family);
    }

    if (!item.youtubeData.isEmpty())
    {
        foreach (const YouTubeURLPicker::Item &yItem, item.youtubeData)
        {
            if (!yItem.dashmpd.isEmpty() && yItem.url == path)
            {
                YouTubeURLPicker picker;

                picker.getAudioStreamByMimeType(yItem.dashmpd, yItem.mimeType, &audioPath);

                break;
            }
        }
    }

    QString title = item.title.isEmpty() ? info.fileName() : item.title;

    if (this->m_presenter.open(path, title, data, fontFamily, 15, 2, audioPath))
    {
        if (data.userData.isEmpty())
            this->m_lastPlayPath = path;
        else
            this->m_lastPlayPath = data.userData;

        this->m_filePath = path;
        this->m_unique = unique;
        this->m_title = title;
        this->m_playData = data;

        if (!this->m_presenter.isRemoteFile() && !this->m_presenter.isRemoteProtocol())
            this->tryOpenSubtitle(Utils::adjustNetworkPath(path));

        this->retreiveExternalSubtitle(path);
        this->setCurrentPlayingIndexByUnique();

        if (this->play())
        {
            this->checkGOMSubtitle();
            return true;
        }
        else
        {
            return false;
        }
    }

    return false;
}

bool RenderScreen::open(const QString &path)
{
    if (Utils::determinRemoteProtocol(path))
        this->openExternal(path);
    else
        this->updatePlayList(path);

    return true;
}

bool RenderScreen::openExternal(const QString &path)
{
    QVector<PlayItem> itemVector;

    this->getYouTubePlayItems(path, &itemVector);
    this->setPlayList(itemVector);

    if (!itemVector.isEmpty())
    {
        this->m_unique = itemVector.first().unique;
        this->setCurrentPlayingIndexByUnique();
    }

    return !itemVector.isEmpty();
}

bool RenderScreen::play()
{
    bool success = this->m_presenter.play();

    this->m_status = Started;
    this->callChanged();

    if (success)
        this->m_status = Playing;
    else
        this->m_status = Stopped;

    this->callChanged();

    if (success)
    {
        QString desc;

        if (this->m_playData.userData.isEmpty())
            desc = QFileInfo(Utils::removeFFMpegSeparator(this->m_filePath)).fileName();
        else
            desc = this->m_title;

        desc += " (";

        if (this->isMovieSubtitleVisiable())
        {
            if (this->m_presenter.existSubtitle())
                desc += trUtf8("자막 있음");
            else
                desc += trUtf8("자막 없음");
        }
        else
        {
            if (this->m_presenter.existSubtitle())
                desc += trUtf8("가사 있음");
            else
                desc += trUtf8("가사 없음");
        }

        desc += ")";

        this->m_presenter.showOptionDesc(desc);
    }

    if (this->m_gotoLastPlay)
    {
        double lastPos = this->m_lastPlay.get(Utils::removeFFMpegSeparator(this->m_lastPlayPath));

        if (lastPos > 0.0)
            this->seek(lastPos, true);
    }

    return success;
}

void RenderScreen::pause()
{
    if (this->m_presenter.isRunning())
    {
        this->m_presenter.pause();
        this->m_status = Paused;
    }
    else
    {
        this->m_status = Stopped;
    }

    this->callChanged();

    this->m_presenter.showOptionDesc(trUtf8("일시정지"));
}

void RenderScreen::resume()
{
    if (this->m_presenter.isRunning())
    {
        this->m_presenter.resume();
        this->m_status = Playing;
    }
    else
    {
        this->m_status = Stopped;
    }

    this->callChanged();

    this->m_presenter.showOptionDesc(trUtf8("재생"));
}

void RenderScreen::stop()
{
    this->updateLastPlay();

    this->m_presenter.stop();
    this->m_status = Stopped;

    this->callChanged();
}

void RenderScreen::close()
{
    RenderScreen::m_selfLock.lock();
    RenderScreen::m_self = NULL;
    RenderScreen::m_selfLock.unlock();

    if (!this->m_filePath.isEmpty())
    {
        this->updateLastPlay();

        this->m_presenter.close();
        this->m_filePath.clear();
        this->m_unique = QUuid();

        this->setScreenKeepOn(false);

        this->m_status = Stopped;
        this->callChanged();
    }

#ifdef Q_OS_RASPBERRY_PI
    this->debusDeinit();
    this->m_xdgDisabler.stop();
#endif
}

void RenderScreen::closeAndGoBack()
{
    this->m_renderer.scheduleCleanup();
}

void RenderScreen::prevFrame(int count)
{
    this->m_presenter.prevFrame(count);

    if (this->m_status != Paused)
    {
        this->m_status = Paused;
        this->callChanged();
    }

    QString desc = trUtf8("이전으로 %1 프레임 이동").arg(count);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::nextFrame(int count)
{
    this->m_presenter.nextFrame(count);

    if (this->m_status != Paused)
    {
        this->m_status = Paused;
        this->callChanged();
    }

    QString desc = trUtf8("다음으로 %1 프레임 이동").arg(count);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::navigate(double distance)
{
    if (!this->hasDuration())
        return;

    double current = this->m_presenter.getCurrentPosition();

    if (this->m_status == Ended)
    {
        this->m_status = Paused;
        this->callChanged();
    }

    double dist = current + distance;
    double duration = this->m_presenter.getDuration();

    if (dist < 0.0)
        dist = 0.0;
    else if (dist > duration)
        dist = duration;

    this->seek(dist, false);

    QString desc;
    QString time;

    if (this->isMovieSubtitleVisiable() && this->isSeekKeyFrame())
    {
        if (distance > 0.0)
            desc = trUtf8("앞으로 %1초 키프레임 이동");
        else
            desc = trUtf8("뒤로 %1초 키프레임 이동");
    }
    else
    {
        if (distance > 0.0)
            desc = trUtf8("앞으로 %1초 이동");
        else
            desc = trUtf8("뒤로 %1초 이동");
    }

    Utils::getTimeString((uint32_t)dist, Utils::TIME_HH_MM_SS, &time);

    desc = desc.arg(fabs(distance));
    desc += QString(" (%1, %2%)").arg(time).arg(dist / duration * 100, 0, 'f', 2);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::seek(double time, bool any)
{
    this->m_presenter.seek(time, any);

    double current = this->m_presenter.getCurrentPosition();
    double max = this->m_presenter.getDuration();

    if (current < max && this->m_status == Ended)
    {
        this->m_status = Paused;
        this->callChanged();
    }
}

bool RenderScreen::setScreenKeepOn(bool on)
{
#if defined Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
    {
        if (on)
            activity.callMethod<void>("disableIdleTimer");
        else
            activity.callMethod<void>("enableIdleTimer");

        return true;
    }

    return false;
#elif defined Q_OS_IOS
    if (on)
        IOSDelegate::disableIdleTimer();
    else
        IOSDelegate::enableIdleTimer();

    return true;
#elif defined Q_OS_RASPBERRY_PI
    if (this->m_dbusConn == NULL)
        return false;

    if (this->m_dbusPending != NULL)
    {
        DBusMessage *reply;

        dbus_pending_call_block(this->m_dbusPending);
        reply = dbus_pending_call_steal_reply(this->m_dbusPending);
        dbus_pending_call_unref(this->m_dbusPending);
        this->m_dbusPending = NULL;

        if (reply != NULL)
        {
            if (!dbus_message_get_args(reply, NULL, DBUS_TYPE_UINT32, &this->m_dbusCookie, DBUS_TYPE_INVALID))
                this->m_dbusCookie = 0;

            dbus_message_unref(reply);
        }
    }

    const char *method = on ? "Inhibit" : "UnInhibit";
    ScreenSaverAPI api = this->m_screenSaverAPI;
    DBusMessage *msg = dbus_message_new_method_call(DBUS_SERVICE[api], DBUS_PATH[api], DBUS_SERVICE[api], method);

    if (msg == NULL)
        return false;

    if (on)
    {
        const char *app = "AnyVOD";
        const char *reason = "playback";
        dbus_bool_t ret;

        switch (api)
        {
            case SSAPI_MATE:
            case SSAPI_GNOME:
            {
                dbus_uint32_t xid = 0;
                dbus_uint32_t gflags = 0xC;

                ret = dbus_message_append_args(msg, DBUS_TYPE_STRING, &app, DBUS_TYPE_UINT32, &xid,
                                               DBUS_TYPE_STRING, &reason, DBUS_TYPE_UINT32, &gflags,
                                               DBUS_TYPE_INVALID);
                break;
            }
            default:
            {
                ret = dbus_message_append_args(msg, DBUS_TYPE_STRING, &app, DBUS_TYPE_STRING, &reason,
                                               DBUS_TYPE_INVALID);
                break;
            }
        }

        if (!ret || !dbus_connection_send_with_reply(this->m_dbusConn, msg, &this->m_dbusPending, -1))
            this->m_dbusPending = NULL;
    }
    else
    {
        if (!dbus_message_append_args(msg, DBUS_TYPE_UINT32, &this->m_dbusCookie, DBUS_TYPE_INVALID) ||
                !dbus_connection_send(this->m_dbusConn, msg, NULL))
            this->m_dbusCookie = 0;
    }

    dbus_connection_flush(this->m_dbusConn);
    dbus_message_unref(msg);

    return true;
#else
    (void)on;
    return false;
#endif
}

bool RenderScreen::setKeepActive(bool on)
{
#if defined Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
    {
        if (on)
            activity.callMethod<void>("setKeepActive");
        else
            activity.callMethod<void>("setKeepInActive");

        return true;
    }

    return false;
#else
    (void)on;
    return false;
#endif
}

bool RenderScreen::setScreenOrientation(bool landscape)
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
    {
        if (landscape)
            activity.callMethod<void>("setOrientationLandscape");
        else
            activity.callMethod<void>("setOrientationPortrait");

        this->m_isLandscape = landscape;

        return true;
    }

    return false;
#else
    (void)landscape;
    return false;
#endif
}

bool RenderScreen::setScreenOrientationRestore()
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
    {
        activity.callMethod<void>("setOrientationRestore");
        this->m_isLandscape = false;

        return true;
    }

    return false;
#else
    return false;
#endif
}

bool RenderScreen::getScreenOrientation() const
{
    return this->m_isLandscape;
}

bool RenderScreen::wantToPlay() const
{
    return this->m_wantToPlay;
}

void RenderScreen::setWantToPlay(bool want)
{
    this->m_wantToPlay = want;
}

void RenderScreen::sync()
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    if (!window)
        return;

    QSize viewPort = window->size() * window->devicePixelRatio();

    this->m_renderer.setViewPortSize(viewPort, window);

    if (this->m_wantToPlay)
    {
        this->m_wantToPlay = false;

        if (!this->playCurrent())
        {
            emit this->failedPlay();

            if (!this->getPlayItemCount())
                emit this->screenUpdated();
        }
    }
}

void RenderScreen::handleWindowChanged(QQuickWindow *window)
{
    if (window)
    {
        connect(window, SIGNAL(beforeSynchronizing()), this, SLOT(sync()), Qt::DirectConnection);
        connect(window, SIGNAL(sceneGraphInvalidated()), &this->m_renderer, SLOT(cleanup()), Qt::DirectConnection);
        connect(window, SIGNAL(beforeRendering()), &this->m_renderer, SLOT(paint()), Qt::DirectConnection);
        connect(this, SIGNAL(screenUpdated()), window, SLOT(update()));

        this->m_playList = this->findChild<PlayListModel*>("playListModel");
        this->m_empty = this->findChild<QQuickItem*>("empty");

        this->m_lyrics1 = this->findChild<QQuickItem*>("lyrics1");
        this->m_lyrics2 = this->findChild<QQuickItem*>("lyrics2");
        this->m_lyrics3 = this->findChild<QQuickItem*>("lyrics3");

        this->m_presenter.setDevicePixelRatio(window->devicePixelRatio());
        this->m_playList->setSettings(&this->m_settings);

        window->setColor(Qt::black);
    }
}

void RenderScreen::handleApplicationStateChanged(Qt::ApplicationState state)
{
    switch (state)
    {
        case Qt::ApplicationSuspended:
        {
            this->saveSettings();

            break;
        }
        case Qt::ApplicationHidden:
        case Qt::ApplicationInactive:
        {
            if (!this->m_presenter.isAudio() && this->m_status == Playing && !this->m_pausedByInactive)
            {
                this->pause();
                this->m_pausedByInactive = true;
            }

            break;
        }
        case Qt::ApplicationActive:
        {
            if (!this->m_presenter.isAudio() && this->m_status == Paused && this->m_pausedByInactive)
            {
                this->resume();
                this->m_pausedByInactive = false;
            }

            break;
        }
    }
}

void RenderScreen::handleExitFromRenderer()
{
    this->close();

    this->m_status = Exit;
    this->callChanged();
}

#ifdef Q_OS_IOS
void CALLBACK RenderScreen::iosNotifyCallback(DWORD status)
{
    RenderScreen *screen = RenderScreen::getSelf();

    if (!screen)
        return;

    if (status == BASS_IOSNOTIFY_INTERRUPT)
    {
        if (screen->m_isPausedByIOSNotify)
            return;

        if (screen->getStatus() == RenderScreen::Playing || screen->getStatus() == RenderScreen::Started)
        {
            screen->m_isPausedByIOSNotify = true;
            screen->pause();
        }
    }
    else if (status == BASS_IOSNOTIFY_INTERRUPT_END)
    {
        if (!screen->m_isPausedByIOSNotify)
            return;

        if (screen->getStatus() == RenderScreen::Paused)
        {
            screen->m_isPausedByIOSNotify = false;
            screen->resume();
        }
    }
}
#endif

void RenderScreen::customEvent(QEvent *event)
{
    if (event->type() == PLAYING_EVENT)
    {
#ifdef Q_OS_IOS
        if (this->isAudio())
            IOSDelegate::updateNowPlayingInfo();
#endif
        emit this->playing();
    }
    else if (event->type() == AUDIO_OPTION_DESC_EVENT)
    {
        ShowAudioOptionDescEvent *e = (ShowAudioOptionDescEvent*)event;
        (void)e;
    }
    else if (event->type() == EMPTY_BUFFER_EVENT)
    {
        EmptyBufferEvent *e = (EmptyBufferEvent*)event;

        this->m_empty->setProperty("visible", e->isEmpty());
        this->m_empty->setProperty("running", e->isEmpty());
    }
    else if (event->type() == AUDIO_SUBTITLE_EVENT)
    {
        if (!this->m_presenter.existExternalSubtitle())
            return;

        AudioSubtitleEvent *e = (AudioSubtitleEvent*)event;
        QVector<Lyrics> lines = e->getLines();
        QVector<Lyrics> mergedLines;
        QVector<Lyrics> *realLine = NULL;
        QVector<QQuickItem*> outputs;
        int i;

        outputs.append(this->m_lyrics1);
        outputs.append(this->m_lyrics2);
        outputs.append(this->m_lyrics3);

        if (lines.count() > 0 && lines[0].text.indexOf('\n') >= 0)
        {
            QStringList texts = lines[0].text.split('\n');
            Lyrics lyrics;

            for (i = 0; i < texts.count(); i++)
            {
                lyrics.color = lines[0].color;
                lyrics.text = texts[i];

                mergedLines.append(lyrics);
            }

            realLine = &mergedLines;
        }
        else
        {
            realLine = &lines;
        }

        for (i = 0; i < outputs.size() && i < realLine->size(); i++)
        {
            QColor color;

            if (this->existAudioSubtitleGender())
            {
                color = (*realLine)[i].color;
            }
            else
            {
                if (i == 0)
                    color = Qt::white;
                else
                    color = Qt::gray;
            }

            color.setAlphaF(this->getSubtitleOpaque());

            outputs[i]->setProperty("color", color);
            outputs[i]->setProperty("text", (*realLine)[i].text.trimmed().remove(QRegExp("\\r|\\n")));
        }

        for (; i < outputs.size(); i++)
            outputs[i]->setProperty("text", QString());
    }
    else if (event->type() == ABORT_EVENT)
    {
        char buf[2048] = {0, };
        AbortEvent *e = (AbortEvent*)event;

        av_strerror(e->getReason(), buf, sizeof(buf));

        this->closeAndGoBack();
    }
    else
    {
        QQuickItem::customEvent(event);
    }
}

void RenderScreen::callChanged()
{
    this->m_statusUpdater.putStatus(this->getStatus());
}

void RenderScreen::playingCallback(void *userData)
{
    QGuiApplication::postEvent((QObject*)userData, new PlayingEvent());
}

void RenderScreen::endedCallback(void *userData)
{
    RenderScreen *parent = (RenderScreen*)userData;

    parent->m_status = Ended;
    parent->callChanged();
}

void RenderScreen::emptyBufferCallback(void *userData, bool empty)
{
    QGuiApplication::postEvent((QObject*)userData, new EmptyBufferEvent(empty));
}

void RenderScreen::showAudioOptionDescCallback(void *userData, const QString &desc, bool show)
{
    QGuiApplication::postEvent((QObject*)userData, new ShowAudioOptionDescEvent(desc, show));
}

void RenderScreen::audioSubtitleCallback(void *userData, const QVector<Lyrics> &lines)
{
    QGuiApplication::postEvent((QObject*)userData, new AudioSubtitleEvent(lines));
}

void RenderScreen::paintCallback(void *userData)
{
    RenderScreen *parent = (RenderScreen*)userData;
    AnyVODWindow *window = (AnyVODWindow*)parent->window();

    if (window)
    {
        window->setAcceptEvent(false);
        emit parent->screenUpdated();
    }
}

void RenderScreen::abortCallback(void *userData, int reason)
{
    QGuiApplication::postEvent((QObject*)userData, new AbortEvent(reason));
}

void RenderScreen::recoverCallback(void *userData)
{
    RenderScreen *parent = (RenderScreen*)userData;

    emit parent->recovered();
}

bool RenderScreen::isOpened() const
{
    return !this->m_filePath.isEmpty();
}

QString RenderScreen::getSubtitlePath() const
{
    return this->m_presenter.getSubtitlePath();
}

bool RenderScreen::openSubtitle(const QString &filePath)
{
    QFileInfo info(filePath);
    bool success = false;

    this->m_presenter.closeAllExternalSubtitles();
    success = this->m_presenter.openSubtitle(filePath);

    if (success)
    {
        if (this->isMovieSubtitleVisiable())
            this->m_presenter.showOptionDesc(trUtf8("자막 열기 : %1").arg(info.fileName()));
        else
            this->m_presenter.showOptionDesc(trUtf8("가사 열기 : %1").arg(info.fileName()));
    }

    return success;
}

bool RenderScreen::saveSubtitleAs(const QString &filePath)
{
    if (!this->existFileSubtitle())
        return false;

    QString desc;
    QFileInfo info(filePath);
    bool success = this->m_presenter.saveSubtitleAs(filePath);

    if (success)
    {
        if (this->isMovieSubtitleVisiable())
            desc = trUtf8("자막이 저장 되었습니다 : %1");
        else
            desc = trUtf8("가사가 저장 되었습니다 : %1");
    }
    else
    {
        if (this->isMovieSubtitleVisiable())
            desc = trUtf8("자막이 저장 되지 않았습니다 : %1");
        else
            desc = trUtf8("가사가 저장 되지 않았습니다 : %1");
    }

    this->m_presenter.showOptionDesc(desc.arg(info.fileName()));

    return success;
}

bool RenderScreen::saveSubtitle()
{
    return this->saveSubtitleAs(this->getSubtitlePath());
}

void RenderScreen::closeAllExternalSubtitles()
{
    this->m_presenter.closeAllExternalSubtitles();

    if (this->isMovieSubtitleVisiable())
        this->m_presenter.showOptionDesc(trUtf8("외부 자막 닫기"));
    else
        this->m_presenter.showOptionDesc(trUtf8("외부 가사 닫기"));
}

QString RenderScreen::getFileName() const
{
    QFileInfo info(this->m_filePath);

    return info.fileName();
}

QString RenderScreen::getTitle() const
{
    return this->m_title;
}

QString RenderScreen::getFilePath() const
{
    return this->m_filePath;
}

const QVector<ChapterInfo>& RenderScreen::getChapters() const
{
    return this->m_presenter.getChapters();
}

void RenderScreen::tryOpenSubtitle(const QString &filePath)
{
    QFileInfo info(filePath);
    QString path = info.absolutePath();

    Utils::appendDirSeparator(&path);

    this->m_presenter.closeAllExternalSubtitles();

    vector_wstring fileNames;
    SubtitleFileNameGenerator gen;
    bool opened = false;
    QStringList searchPaths = this->m_subtitleDirectory;

    if (this->m_priorSubtitleDirectory)
        searchPaths.append(path);
    else
        searchPaths.prepend(path);

    foreach (const QString &searchPath, searchPaths)
    {
        fileNames.clear();
        gen.getFileNamesWithExt(filePath.toStdWString(), &fileNames);

        for (size_t i = 0; i < fileNames.size(); i++)
        {
            QString subtitleFilePath = searchPath + QString::fromStdWString(fileNames[i]);

            if (this->m_presenter.openSubtitle(subtitleFilePath))
            {
                opened = true;
                break;
            }
        }

        if (!opened && this->m_useSearchSubtitleComplex)
        {
            fileNames.clear();
            gen.getFileNames(filePath.toStdWString(), &fileNames);

            for (size_t i = 0; i < fileNames.size(); i++)
            {
                QString subtitleFilePath = searchPath + QString::fromStdWString(fileNames[i]);

                if (this->m_presenter.openSubtitle(subtitleFilePath))
                {
                    opened = true;
                    break;
                }
            }
        }

        if (opened)
            break;
    }
}

void RenderScreen::retreiveLyrics(const QString &filePath)
{
    SubtitleImpl subtitle;
    wstring ret;

    if (!this->isEnableSearchLyrics())
        return;

    if (subtitle.getLyrics(filePath.toStdWString(), &ret))
    {
        QString tmpFilePath = QDir::tempPath();

        Utils::appendDirSeparator(&tmpFilePath);
        tmpFilePath += "XXXXXX";
        tmpFilePath += ".lrc";

        QTemporaryFile tmpFile(tmpFilePath);

        if (tmpFile.open())
        {
            QFile file(tmpFile.fileName());

            if (file.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                QTextStream stream(&file);

                stream.setCodec("UTF-8");
                stream.setGenerateByteOrderMark(true);

                stream << QString::fromStdWString(ret);

                file.close();
                tmpFile.close();

                this->openSubtitle(tmpFile.fileName());
            }
        }
    }
}

void RenderScreen::retreiveSubtitleURL(const QString &filePath)
{
    SubtitleImpl subtitle;
    QFileInfo info(filePath);
    string url;

    if (!this->isEnableSearchSubtitle())
        return;

    if (subtitle.existSubtitle(filePath.toStdWString(), info.fileName().toLocal8Bit().constData(), &url))
        this->m_presenter.setSubtitleURL(QString::fromStdString(url));
}

void RenderScreen::retreiveExternalSubtitle(const QString &filePath)
{
    if (!this->m_presenter.existExternalSubtitle())
    {
        if (this->m_presenter.isRemoteProtocol())
        {
            if (!this->m_playData.userData.isEmpty())
                this->m_presenter.openYouTube(this->m_playData.userData);
        }
        else
        {
            if (this->m_presenter.isAudio())
                this->retreiveLyrics(filePath);
            else
                this->retreiveSubtitleURL(filePath);
        }
    }
}

void RenderScreen::showDetail(bool show)
{
    this->m_presenter.showDetail(show);
}

bool RenderScreen::isShowDetail() const
{
    return this->m_presenter.isShowDetail();
}

const MediaPresenter::Detail& RenderScreen::getDetail() const
{
    return this->m_presenter.getDetail();
}

void RenderScreen::showSubtitle(bool show)
{
    this->m_presenter.showSubtitle(show);

    QString desc;

    if (this->isMovieSubtitleVisiable())
    {
        if (show)
            desc = trUtf8("자막 보이기");
        else
            desc = trUtf8("자막 숨기기");
    }
    else
    {
        if (show)
            desc = trUtf8("가사 보이기");
        else
            desc = trUtf8("가사 숨기기");
    }

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isShowSubtitle() const
{
    return this->m_presenter.isShowSubtitle();
}

void RenderScreen::setSearchSubtitleComplex(bool use)
{
    this->m_useSearchSubtitleComplex = use;

    QString desc;

    if (this->isMovieSubtitleVisiable())
    {
        if (use)
            desc = trUtf8("고급 자막 검색 사용");
        else
            desc = trUtf8("고급 자막 검색 사용 안 함");
    }
    else
    {
        if (use)
            desc = trUtf8("고급 가사 검색 사용");
        else
            desc = trUtf8("고급 가사 검색 사용 안 함");
    }

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::getSearchSubtitleComplex() const
{
    return this->m_useSearchSubtitleComplex;
}

bool RenderScreen::existSubtitle()
{
    return this->m_presenter.existSubtitle();
}

bool RenderScreen::existFileSubtitle()
{
    return this->m_presenter.existFileSubtitle();
}

bool RenderScreen::existExternalSubtitle()
{
    return this->m_presenter.existExternalSubtitle();
}

bool RenderScreen::existAudioSubtitle()
{
    return this->m_presenter.existAudioSubtitle();
}

bool RenderScreen::existAudioSubtitleGender()
{
    return this->m_presenter.existAudioSubtitleGender();
}

QStringList RenderScreen::getSubtitleClasses()
{
    QStringList classNames;

    this->m_presenter.getSubtitleClasses(&classNames);
    return classNames;
}

QString RenderScreen::getCurrentSubtitleClass()
{
    QString className;

    this->m_presenter.getCurrentSubtitleClass(&className);
    return className;
}

bool RenderScreen::setCurrentSubtitleClass(const QString &className)
{
    QString desc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 언어 변경");
    else
        desc = trUtf8("가사 언어 변경");

    desc += QString(" (%1)").arg(className);

    bool success = this->m_presenter.setCurrentSubtitleClass(className);

    this->m_presenter.showOptionDesc(desc);

    return success;
}

int RenderScreen::getSubtitleClassCount()
{
    return this->getSubtitleClasses().count();
}

QString RenderScreen::getSubtitleClass(int index)
{
    QStringList classes = this->getSubtitleClasses();

    return classes[index];
}

void RenderScreen::resetSubtitlePosition()
{
    if (this->isSubtitleMoveable())
    {
        this->m_presenter.resetSubtitlePosition();
        this->m_presenter.showOptionDesc(trUtf8("자막 위치 초기화"));
    }
}

void RenderScreen::setVerticalSubtitlePosition(int pos)
{
    QString desc;

    this->m_presenter.setVerticalSubtitlePosition(pos);

    if (pos > 0)
        desc = trUtf8("자막 위치 위로 : %1");
    else
        desc = trUtf8("자막 위치 아래로 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getVerticalSubtitlePosition()));
}

void RenderScreen::setHorizontalSubtitlePosition(int pos)
{
    QString desc;

    this->m_presenter.setHorizontalSubtitlePosition(pos);

    if (pos > 0)
        desc = trUtf8("자막 위치 왼쪽으로 : %1");
    else
        desc = trUtf8("자막 위치 오른쪽으로 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getHorizontalSubtitlePosition()));
}

void RenderScreen::setVerticalSubtitleAbsolutePosition(int pos)
{
    QString desc;

    this->m_presenter.setVerticalSubtitleAbsolutePosition(pos);

    desc = trUtf8("세로 자막 위치 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getVerticalSubtitlePosition()));
}

void RenderScreen::setHorizontalSubtitleAbsolutePosition(int pos)
{
    QString desc;

    this->m_presenter.setHorizontalSubtitleAbsolutePosition(pos);

    desc = trUtf8("가로 자막 위치 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getHorizontalSubtitlePosition()));
}

int RenderScreen::getVerticalSubtitlePosition()
{
    return this->m_presenter.getVerticalSubtitlePosition();
}

int RenderScreen::getHorizontalSubtitlePosition()
{
    return this->m_presenter.getHorizontalSubtitlePosition();
}

void RenderScreen::reset3DSubtitleOffset()
{
    if (this->is3DSubtitleMoveable())
    {
        this->m_presenter.reset3DSubtitleOffset();
        this->m_presenter.showOptionDesc(trUtf8("3D 자막 위치 초기화"));
    }
}

void RenderScreen::setVertical3DSubtitleOffset(int pos)
{
    QString desc;
    QString far = trUtf8("3D 자막 위치 멀게(세로) : %1");
    QString near = trUtf8("3D 자막 위치 가깝게(세로) : %1");

    this->m_presenter.setVertical3DSubtitleOffset(pos);

    if (pos > 0)
    {
        if (this->getVRInputSource() == AnyVODEnums::VRI_NONE)
            desc = near;
        else
            desc = far;
    }
    else
    {
        if (this->getVRInputSource() == AnyVODEnums::VRI_NONE)
            desc = far;
        else
            desc = near;
    }

    this->m_presenter.showOptionDesc(desc.arg(this->getVertical3DSubtitleOffset()));
}

void RenderScreen::setHorizontal3DSubtitleOffset(int pos)
{
    QString desc;
    QString far = trUtf8("3D 자막 위치 멀게(가로) : %1");
    QString near = trUtf8("3D 자막 위치 가깝게(가로) : %1");

    this->m_presenter.setHorizontal3DSubtitleOffset(pos);

    if (pos > 0)
    {
        if (this->getVRInputSource() == AnyVODEnums::VRI_NONE)
            desc = near;
        else
            desc = far;
    }
    else
    {
        if (this->getVRInputSource() == AnyVODEnums::VRI_NONE)
            desc = far;
        else
            desc = near;
    }

    this->m_presenter.showOptionDesc(desc.arg(this->getHorizontal3DSubtitleOffset()));
}

void RenderScreen::setVertical3DSubtitleAbsoluteOffset(int offset)
{
    QString desc;

    this->m_presenter.setVertical3DSubtitleAbsoluteOffset(offset);

    desc = trUtf8("세로 3D 자막 위치 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getVertical3DSubtitleOffset()));
}

void RenderScreen::setHorizontal3DSubtitleAbsoluteOffset(int offset)
{
    QString desc;

    this->m_presenter.setHorizontal3DSubtitleAbsoluteOffset(offset);

    desc = trUtf8("가로 3D 자막 위치 : %1");

    this->m_presenter.showOptionDesc(desc.arg(this->getHorizontal3DSubtitleOffset()));
}

int RenderScreen::getVertical3DSubtitleOffset()
{
    return this->m_presenter.getVertical3DSubtitleOffset();
}

int RenderScreen::getHorizontal3DSubtitleOffset()
{
    return this->m_presenter.getHorizontal3DSubtitleOffset();
}

void RenderScreen::setRepeatStart(double start)
{
    QString desc;
    QString time;

    Utils::getTimeString(start, Utils::TIME_HH_MM_SS_ZZZ, &time);

    desc = trUtf8("구간 반복 시작 : %1").arg(time);

    this->m_presenter.setRepeatStart(start);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::setRepeatEnd(double end)
{
    QString desc;
    QString time;

    Utils::getTimeString(end, Utils::TIME_HH_MM_SS_ZZZ, &time);

    desc = trUtf8("구간 반복 끝 : %1").arg(time);

    this->m_presenter.setRepeatEnd(end);
    this->m_presenter.setRepeatEnable(true);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::setRepeatEnable(bool enable)
{
    QString desc;
    QString en;
    QString start;
    QString end;

    if (enable)
        en = trUtf8("구간 반복 활성화");
    else
        en = trUtf8("구간 반복 비활성화");

    Utils::getTimeString(this->getRepeatStart(), Utils::TIME_HH_MM_SS_ZZZ, &start);
    Utils::getTimeString(this->getRepeatEnd(), Utils::TIME_HH_MM_SS_ZZZ, &end);

    if (start == end && enable)
        desc = trUtf8("시작과 끝 시각이 같으므로 활성화 되지 않습니다");
    else
        desc = QString("%1 : %2 ~ %3").arg(en).arg(start).arg(end);

    this->m_presenter.setRepeatEnable(enable);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::getRepeatEnable() const
{
    return this->m_presenter.getRepeatEnable();
}

double RenderScreen::getRepeatStart() const
{
    return this->m_presenter.getRepeatStart();
}

double RenderScreen::getRepeatEnd() const
{
    return this->m_presenter.getRepeatEnd();
}

void RenderScreen::setRepeatStartMove(double offset)
{
    QString desc;

    if (this->getRepeatStart() != this->getRepeatEnd())
    {
        double moveToTime = this->getRepeatStart() + offset;

        if (moveToTime >= 0.0 && moveToTime < this->getRepeatEnd())
        {
            QString time;

            Utils::getTimeString(moveToTime, Utils::TIME_HH_MM_SS_ZZZ, &time);

            if (offset < 0.0)
                desc = trUtf8("구간 반복 시작 위치 %1초 뒤로 이동 (%2)").arg(fabs(offset)).arg(time);
            else
                desc = trUtf8("구간 반복 시작 위치 %1초 앞으로 이동 (%2)").arg(offset).arg(time);

            this->m_presenter.setRepeatStart(moveToTime);
        }
        else
        {
            desc = trUtf8("구간 반복 시작 위치가 범위를 벗어났습니다");
        }
    }
    else
    {
        desc = trUtf8("구간 반복이 설정 되지 않았습니다");
    }

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::setRepeatEndMove(double offset)
{
    QString desc;

    if (this->getRepeatStart() != this->getRepeatEnd())
    {
        double moveToTime = this->getRepeatEnd() + offset;

        if (moveToTime > this->getRepeatStart() && moveToTime <= this->getDuration())
        {
            QString time;

            Utils::getTimeString(moveToTime, Utils::TIME_HH_MM_SS_ZZZ, &time);

            if (offset < 0.0)
                desc = trUtf8("구간 반복 끝 위치 %1초 뒤로 이동 (%2)").arg(fabs(offset)).arg(time);
            else
                desc = trUtf8("구간 반복 끝 위치 %1초 앞으로 이동 (%2)").arg(offset).arg(time);

            this->m_presenter.setRepeatEnd(moveToTime);
        }
        else
        {
            desc = trUtf8("구간 반복 끝 위치가 범위를 벗어났습니다");
        }
    }
    else
    {
        desc = trUtf8("구간 반복이 설정 되지 않았습니다");
    }

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::setRepeatMove(double offset)
{
    QString desc;

    if (this->getRepeatStart() != this->getRepeatEnd())
    {
        double moveToStartTime = this->getRepeatStart() + offset;
        double moveToEndTime = this->getRepeatEnd() + offset;

        if ((moveToStartTime >= 0.0 && moveToStartTime < this->getRepeatEnd()) &&
                (moveToEndTime > this->getRepeatStart() && moveToEndTime <= this->getDuration()))
        {
            QString startTime;
            QString endTime;

            Utils::getTimeString(moveToStartTime, Utils::TIME_HH_MM_SS_ZZZ, &startTime);
            Utils::getTimeString(moveToEndTime, Utils::TIME_HH_MM_SS_ZZZ, &endTime);

            if (offset < 0.0)
                desc = trUtf8("구간 반복 위치 %1초 뒤로 이동 (%2 ~ %3)").arg(fabs(offset)).arg(startTime).arg(endTime);
            else
                desc = trUtf8("구간 반복 위치 %1초 앞으로 이동 (%2 ~ %3)").arg(offset).arg(startTime).arg(endTime);

            this->m_presenter.setRepeatStart(moveToStartTime);
            this->m_presenter.setRepeatEnd(moveToEndTime);
        }
        else
        {
            desc = trUtf8("구간 반복 위치가 범위를 벗어났습니다");
        }
    }
    else
    {
        desc = trUtf8("구간 반복이 설정 되지 않았습니다");
    }

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::setSeekKeyFrame(bool keyFrame)
{
    QString desc;

    if (keyFrame)
        desc = trUtf8("키프레임 단위로 이동 함");
    else
        desc = trUtf8("키프레임 단위로 이동 안 함");

    this->m_presenter.setSeekKeyFrame(keyFrame);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isSeekKeyFrame() const
{
    return this->m_presenter.isSeekKeyFrame();
}

void RenderScreen::setSubtitleDirectory(const QStringList &paths, bool prior)
{
    this->m_subtitleDirectory = paths;
    this->m_priorSubtitleDirectory = prior;
}

QStringList RenderScreen::getSubtitleDirectoryPath() const
{
    return this->m_subtitleDirectory;
}

bool RenderScreen::getSubtitleDirectoryPrior() const
{
    return this->m_priorSubtitleDirectory;
}

void RenderScreen::getSubtitleDirectory(QStringList *path, bool *prior) const
{
    *path = this->m_subtitleDirectory;
    *prior = this->m_priorSubtitleDirectory;
}

void RenderScreen::set3DMethod(AnyVODEnums::Video3DMethod method)
{
    QString methodDesc = this->get3DMethodDesc(method);
    QString cat = this->get3DMethodCategory(method);

    methodDesc += " (" + cat + ")";

    this->setScheduleRecomputeSubtitleSize();

    this->m_presenter.set3DMethod(method);
    this->m_presenter.showOptionDesc(trUtf8("3D 영상 (%1)").arg(methodDesc));
}

AnyVODEnums::Video3DMethod RenderScreen::get3DMethod() const
{
    return this->m_presenter.get3DMethod();
}

void RenderScreen::setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method)
{
    QString methodDesc = this->getSubtitle3DMethodDesc(method);

    this->m_presenter.setSubtitle3DMethod(method);
    this->m_presenter.showOptionDesc(trUtf8("3D 자막 (%1)").arg(methodDesc));
}

AnyVODEnums::Subtitle3DMethod RenderScreen::getSubtitle3DMethod() const
{
    return this->m_presenter.getSubtitle3DMethod();
}

void RenderScreen::setVRInputSource(AnyVODEnums::VRInputSource source)
{
    QString sourceDesc = this->getVRInputSourceDesc(source);

    this->m_presenter.setVRInputSource(source);
    this->m_presenter.showOptionDesc(trUtf8("VR 입력 영상 (%1)").arg(sourceDesc));

    emit this->vrInputSourceChanged();
}

int RenderScreen::getVRInputSource() const
{
    return this->m_presenter.getVRInputSource();
}

void RenderScreen::useHeadTracking(bool use)
{
    QString desc;

    this->m_useHeadTracking = use;

    if (use)
        desc = trUtf8("VR 헤드 트래킹 사용 함");
    else
        desc = trUtf8("VR 헤드 트래킹 사용 안 함");

    this->m_presenter.showOptionDesc(desc);

    emit this->vrInputSourceChanged();
}

bool RenderScreen::isUseHeadTracking() const
{
    return this->m_useHeadTracking;
}

void RenderScreen::useBarrelDistortion(bool use)
{
    QString desc;

    this->m_presenter.useBarrelDistortion(use);

    if (use)
        desc = trUtf8("VR 왜곡 보정 사용 함");
    else
        desc = trUtf8("VR 왜곡 보정 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseBarrelDistortion() const
{
    return this->m_presenter.isUseBarrelDistortion();
}

void RenderScreen::setBarrelDistortionCoefficients(const QVector2D &coefficients)
{
    QString desc = trUtf8("VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)");

    this->m_presenter.setBarrelDistortionCoefficients(coefficients);

    desc = desc.arg(coefficients.x()).arg(coefficients.y());

    this->m_presenter.showOptionDesc(desc);
}

QVector2D RenderScreen::getBarrelDistortionCoefficients() const
{
    return this->m_presenter.getBarrelDistortionCoefficients();
}

void RenderScreen::setBarrelDistortionLensCenter(const QVector2D &lensCenter)
{
    QString desc = trUtf8("VR 렌즈 센터 설정 (X : %1, Y : %2)");

    this->m_presenter.setBarrelDistortionLensCenter(lensCenter);

    desc = desc.arg(lensCenter.x()).arg(lensCenter.y());

    this->m_presenter.showOptionDesc(desc);
}

QVector2D RenderScreen::getBarrelDistortionLensCenter() const
{
    return this->m_presenter.getBarrelDistortionLensCenter();
}

void RenderScreen::setSkipRanges(const QVector<MediaPresenter::Range> &ranges)
{
    this->m_presenter.setSkipRanges(ranges);
}

QVector<MediaPresenter::Range> RenderScreen::getSkipRanges() const
{
    QVector<MediaPresenter::Range> ret;

    this->m_presenter.getSkipRanges(&ret);
    return ret;
}

void RenderScreen::setSkipOpening(bool skip)
{
    QString desc;
    QString enable;
    QString time;

    Utils::getTimeString(this->m_presenter.getOpeningSkipTime(), Utils::TIME_HH_MM_SS, &time);

    if (skip)
        enable = trUtf8("오프닝 스킵 사용 함");
    else
        enable = trUtf8("오프닝 스킵 사용 안 함");

    desc = QString("%1 : %2").arg(enable).arg(time);

    this->m_presenter.setSkipOpening(skip);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::getSkipOpening() const
{
    return this->m_presenter.getSkipOpening();
}

void RenderScreen::setSkipEnding(bool skip)
{
    QString desc;
    QString enable;
    QString time;

    Utils::getTimeString(this->m_presenter.getEndingSkipTime(), Utils::TIME_HH_MM_SS, &time);

    if (skip)
        enable = trUtf8("엔딩 스킵 사용 함");
    else
        enable = trUtf8("엔딩 스킵 사용 안 함");

    desc = QString("%1 : %2").arg(enable).arg(time);

    this->m_presenter.setSkipEnding(skip);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::getSkipEnding() const
{
    return this->m_presenter.getSkipEnding();
}

void RenderScreen::setUseSkipRange(bool use)
{
    QString desc;

    if (use)
        desc = trUtf8("재생 스킵 사용 함");
    else
        desc = trUtf8("재생 스킵 사용 안 함");

    this->m_presenter.setUseSkipRange(use);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::getUseSkipRange() const
{
    return this->m_presenter.getUseSkipRange();
}

void RenderScreen::useNormalizer(bool use)
{
    this->m_presenter.useNormalizer(use);

    QString desc;

    if (use)
        desc = trUtf8("노멀라이저 켜짐");
    else
        desc = trUtf8("노멀라이저 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUsingNormalizer() const
{
    return this->m_presenter.isUsingNormalizer();
}

void RenderScreen::useEqualizer(bool use)
{
    this->m_presenter.useEqualizer(use);

    QString desc;

    if (use)
        desc = trUtf8("이퀄라이저 켜짐");
    else
        desc = trUtf8("이퀄라이저 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUsingEqualizer() const
{
    return this->m_presenter.isUsingEqualizer();
}

void RenderScreen::useLowerMusic(bool use)
{
    this->m_presenter.useLowerMusic(use);

    QString desc;

    if (use)
        desc = trUtf8("음악 줄임 켜짐");
    else
        desc = trUtf8("음악 줄임 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUsingLowerMusic() const
{
    return this->m_presenter.isUsingLowerMusic();
}

void RenderScreen::addSubtitleOpaque(float inc)
{
    float opaque = this->m_presenter.getSubtitleOpaque() + inc;

    this->setSubtitleOpaque(opaque);
}

void RenderScreen::setSubtitleOpaque(float opaque)
{
    QString desc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 투명도");
    else
        desc = trUtf8("가사 투명도");

    opaque = Utils::zeroDouble(opaque);

    if (opaque < 0.0f)
        opaque = 0.0f;

    if (opaque > 1.0f)
        opaque = 1.0f;

    this->m_presenter.setSubtitleOpaque(opaque);
    this->m_presenter.showOptionDesc(QString("%1 (%2%)").arg(desc).arg((int)(ceil(opaque * 100))));
}

float RenderScreen::getSubtitleOpaque() const
{
    return this->m_presenter.getSubtitleOpaque();
}

void RenderScreen::resetSubtitleOpaque()
{
    if (!this->existSubtitle())
        return;

    QString desc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 투명도 초기화");
    else
        desc = trUtf8("가사 투명도 초기화");

    this->m_presenter.setSubtitleOpaque(1.0f);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::addSubtitleSize(float inc)
{
    float size = this->m_presenter.getSubtitleSize() + inc;

    this->setSubtitleSize(size);
}

float RenderScreen::getSubtitleSize() const
{
    if (Utils::zeroDouble(this->m_subtitleSize) >= 0.0f)
        return this->m_subtitleSize;
    else
        return this->m_presenter.getSubtitleSize();
}

void RenderScreen::setSubtitleSize(float size)
{
    QString desc = trUtf8("자막 크기");

    size = Utils::zeroDouble(size);

    if (size < 0.0f)
        size = 0.0f;

    this->m_presenter.setSubtitleSize(size);
    this->m_presenter.showOptionDesc(QString("%1 (%2%)").arg(desc).arg((int)(ceil(size * 100))));
}

void RenderScreen::resetSubtitleSize()
{
    QString desc = trUtf8("자막 크기 초기화");

    this->m_presenter.setSubtitleSize(1.0f);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::setScheduleRecomputeSubtitleSize()
{
    this->m_presenter.setScheduleRecomputeSubtitleSize();
}

void RenderScreen::saveSubtitleSize(float size)
{
    this->m_subtitleSize = size;
}

void RenderScreen::applySubtitleSize()
{
    if (Utils::zeroDouble(this->m_subtitleSize) >= 0.0)
    {
        this->setScheduleRecomputeSubtitleSize();
        this->setSubtitleSize(this->m_subtitleSize);

        this->m_subtitleSize = -1.0f;
    }
}

float RenderScreen::getSavedSubtitleSize() const
{
    return this->m_subtitleSize;
}

void RenderScreen::useHWDecoder(bool enable)
{
    QString desc;

    this->m_presenter.useHWDecoder(enable);

    if (enable)
    {
        desc = trUtf8("하드웨어 디코더 사용 함");

        if (!this->m_presenter.isOpenedHWDecoder())
            desc += trUtf8(" (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)");
    }
    else
    {
        desc = trUtf8("하드웨어 디코더 사용 안 함");
    }

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseHWDecoder() const
{
    return this->m_presenter.isUseHWDecoder();
}

void RenderScreen::useLowQualityMode(bool enable)
{
    QString desc;

    this->m_presenter.useLowQualityMode(enable);

    if (enable)
        desc = trUtf8("저화질 모드 사용 함");
    else
        desc = trUtf8("저화질 모드 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseLowQualityMode() const
{
    return this->m_presenter.isUseLowQualityMode();
}

void RenderScreen::useFrameDrop(bool enable)
{
    QString desc;

    this->m_presenter.useFrameDrop(enable);

    if (enable)
        desc = trUtf8("프레임 드랍 사용 함");
    else
        desc = trUtf8("프레임 드랍 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseFrameDrop() const
{
    return this->m_presenter.isUseFrameDrop();
}

void RenderScreen::useBufferingMode(bool enable)
{
    QString desc;

    this->m_presenter.useBufferingMode(enable);

    if (enable)
        desc = trUtf8("버퍼링 모드 사용 함");
    else
        desc = trUtf8("버퍼링 모드 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseBufferingMode() const
{
    return this->m_presenter.isUseBufferingMode();
}

void RenderScreen::use3DFull(bool enable)
{
    this->setScheduleRecomputeSubtitleSize();
    this->m_presenter.use3DFull(enable);

    QString desc;

    if (enable)
        desc = trUtf8("3D 전체 해상도 사용");
    else
        desc = trUtf8("3D 전체 해상도 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUse3DFull() const
{
    return this->m_presenter.isUse3DFull();
}

void RenderScreen::useLowerVoice(bool use)
{
    this->m_presenter.useLowerVoice(use);

    QString desc;

    if (use)
        desc = trUtf8("음성 줄임 켜짐");
    else
        desc = trUtf8("음성 줄임 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUsingLowerVoice() const
{
    return this->m_presenter.isUsingLowerVoice();
}

void RenderScreen::useHigherVoice(bool use)
{
    this->m_presenter.useHigherVoice(use);

    QString desc;

    if (use)
        desc = trUtf8("음성 강조 켜짐");
    else
        desc = trUtf8("음성 강조 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUsingHigherVoice() const
{
    return this->m_presenter.isUsingHigherVoice();
}

bool RenderScreen::setPreAmp(float dB)
{
    return this->m_presenter.setPreAmp(dB);
}

float RenderScreen::getPreAmp() const
{
    return this->m_presenter.getPreAmp();
}

bool RenderScreen::setEqualizerGain(int band, float gain)
{
    return this->m_presenter.setEqualizerGain(band, gain);
}

float RenderScreen::getEqualizerGain(int band) const
{
    return this->m_presenter.getEqualizerGain(band);
}

int RenderScreen::getBandCount() const
{
    return this->m_presenter.getBandCount();
}

bool RenderScreen::recover()
{
    return this->m_presenter.recover(this->m_presenter.getCurrentPosition());
}

void RenderScreen::toggle()
{
    if (this->m_status == Paused || this->m_status == Ended)
        this->resume();
    else if (this->m_status == Playing)
        this->pause();
}

void RenderScreen::updateLastPlay()
{
    if (this->m_gotoLastPlay)
    {
        if (this->m_status != Stopped && this->m_status != Ended)
            this->m_lastPlay.update(Utils::removeFFMpegSeparator(this->m_lastPlayPath), this->getCurrentPosition());
    }
    else
    {
        this->m_lastPlay.clear(Utils::removeFFMpegSeparator(this->m_lastPlayPath));
    }
}

bool RenderScreen::setCurrentPlayingIndexByUnique()
{
    if (this->m_unique == QUuid())
    {
        this->resetCurrentPlayingIndex();
        return false;
    }
    else
    {
        this->m_currentPlayingIndex = this->m_playList->findIndex(this->m_unique);
        return true;
    }
}

bool RenderScreen::updatePlayList(const QString &path)
{
    QVector<PlayItem> playItems;

#if !defined Q_OS_ANDROID && !defined Q_OS_IOS
    if (Utils::determinDevice(path))
    {
        QVector<DTVReader::ChannelInfo> scanned;

        this->m_presenter.getDTVReader().getScannedChannels(&scanned);

        foreach (const DTVReader::ChannelInfo &channelInfo, scanned)
        {
            QStringList urlList;
            QVector<PlayItem> single;
            QString url = Utils::makeDTVPath(channelInfo);

            urlList.append(url);

            Utils::getPlayItemFromFileNames(urlList, &single);

            if (!single.isEmpty())
            {
                DTVReaderInterface::Info info;

                if (this->m_presenter.getDTVReader().getAdapterInfo(channelInfo.adapter, &info))
                {
                    if (channelInfo.name.isEmpty())
                    {
                        single[0].title = QString("%1 (%2)")
                                .arg(channelInfo.channel)
                                .arg(info.name);
                    }
                    else
                    {
                        single[0].title = QString("%1 (%2, %3)")
                                .arg(channelInfo.name)
                                .arg(channelInfo.channel)
                                .arg(info.name);
                    }

                    single[0].extraData.userData = url.toUtf8();
                }

                playItems += single;
            }
        }
    }
    else
#endif
    {
        QFileInfo info(path);

        if (info.isDir())
        {
            QStringList filePathList;

            Utils::getLocalFileListOnlyMedia(path, MEDIA_EXTS_LIST, false, &filePathList);
            Utils::getPlayItemFromFileNames(filePathList, &playItems);
        }
        else
        {
            QDir dir(info.absoluteDir());

            foreach (const QFileInfo &file, dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot, QDir::Name))
            {
                QString filePath = file.absoluteFilePath();
                QStringList filePathList;

                Utils::getLocalFileListOnlyMedia(filePath, MEDIA_EXTS_LIST, false, &filePathList);

                if (!filePathList.empty())
                {
                    QVector<PlayItem> itemVector;

                    Utils::getPlayItemFromFileNames(filePathList, &itemVector);

                    playItems += itemVector;
                }
            }
        }
    }

    this->setPlayList(playItems);

    for (int i = 0; i < this->getPlayItemCount(); i++)
    {
        PlayItem item = this->m_playList->getPlayItem(i);

        if (path == item.path)
        {
            this->m_unique = item.unique;
            this->setCurrentPlayingIndexByUnique();

            break;
        }
    }

    return !playItems.isEmpty();
}

void RenderScreen::getYouTubePlayItems(const QString &address, QVector<PlayItem> *itemVector)
{
    QVector<YouTubeURLPicker::Item> youtube;
    YouTubeURLPicker picker;

    youtube = picker.pickURL(address);

    if (youtube.isEmpty())
    {
        QStringList list;

        list.append(address);
        Utils::getPlayItemFromFileNames(list, itemVector);
    }
    else
    {
        bool isPlayList = picker.isPlayList();

        if (isPlayList)
        {
            QVector<YouTubeURLPicker::Item> playlist;

            if (youtube.length() > 0)
                playlist = picker.pickURL(youtube.first().url);

            youtube = playlist.mid(0, 1) + youtube.mid(1);
        }

        Utils::getPlayItemFromYouTube(youtube, isPlayList, itemVector);
    }
}

void RenderScreen::setPlayList(const QVector<PlayItem> &list)
{
    this->addToPlayList(list);
}

void RenderScreen::addToPlayList(const QVector<PlayItem> &list)
{
    this->m_playList->addPlayList(list);
    this->setCurrentPlayingIndexByUnique();
}

bool RenderScreen::isRotatable() const
{
    return !Utils::isPortrait(this->m_presenter.getRotation()) && !this->m_presenter.isAudio();
}

#ifdef Q_OS_ANDROID
void RenderScreen::callActivityEvent(const QString &funcName) const
{
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
        activity.callMethod<void>(funcName.toLatin1());
}
#endif

bool RenderScreen::isPlayOrPause() const
{
    return this->m_status == Playing || this->m_status == Paused;
}

bool RenderScreen::isRemoteFile() const
{
    return this->m_presenter.isRemoteFile();
}

bool RenderScreen::canMoveChapter() const
{
    return this->isPlayOrPause() && this->hasDuration() && this->getChapters().count() > 0;
}

bool RenderScreen::canMoveFrame() const
{
    return this->isVideo() && this->hasDuration();
}

bool RenderScreen::isTempoUsable() const
{
    return this->m_presenter.isTempoUsable();
}

float RenderScreen::getTempo() const
{
    return this->m_presenter.getTempo();
}

void RenderScreen::setTempo(float percent)
{
    QString desc;

    if (Utils::zeroDouble(percent) == 0.0)
    {
        percent = 0.0f;
        desc = trUtf8("재생 속도 초기화");
    }
    else
    {
        desc = trUtf8("재생 속도 : %1배").arg(1.0f + percent / 100.0f);
    }

    this->m_presenter.setTempo(percent);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::setUserAspectRatio(MediaPresenter::UserAspectRatio &ratio)
{
    QString desc;

    if (ratio.use)
    {
        desc = trUtf8("화면 비율 사용 함");

        if (ratio.fullscreen)
            desc += trUtf8(" (화면 채우기)");
        else
            desc += QString(" (%1:%2)").arg(ratio.width).arg(ratio.height);
    }
    else
    {
        desc = trUtf8("화면 비율 사용 안 함");
    }

    this->setScheduleRecomputeSubtitleSize();

    this->m_presenter.setUserAspectRatio(ratio);
    this->m_presenter.showOptionDesc(desc);
}

MediaPresenter::UserAspectRatio RenderScreen::getUserAspectRatio() const
{
    MediaPresenter::UserAspectRatio ret;

    this->m_presenter.getUserAspectRatio(&ret);
    return ret;
}

QString RenderScreen::getGOMSubtitleURL() const
{
    QString ret;

    this->m_presenter.getSubtitleURL(&ret);

    QUrl encode(ret, QUrl::StrictMode);
    QString encoded = encode.toEncoded();

    return encoded;
}

int RenderScreen::getCurrentAudioStreamIndex() const
{
    return this->m_presenter.getCurrentAudioStreamIndex();
}

QVector<AudioStreamInfo> RenderScreen::getAudioStreamInfo() const
{
    QVector<AudioStreamInfo> ret;

    this->m_presenter.getAudioStreamInfo(&ret);
    return ret;
}

int RenderScreen::getAudioStreamInfoCount() const
{
    return this->getAudioStreamInfo().count();
}

QString RenderScreen::getAudioStreamDesc(int index) const
{
    return this->getAudioStreamInfo()[index].name;
}

int RenderScreen::getAudioStreamIndex(int index) const
{
    return this->getAudioStreamInfo()[index].index;
}

bool RenderScreen::changeAudioStream(const int index)
{
    QVector<AudioStreamInfo> info;
    QString desc;

    this->m_presenter.getAudioStreamInfo(&info);

    for (int i = 0; i < info.count(); i++)
    {
        if (info[i].index == index)
        {
            desc = info[i].name;
            break;
        }
    }

    bool success = this->m_presenter.changeAudioStream(index);

    this->m_presenter.showOptionDesc(trUtf8("음성 변경 (%1)").arg(desc));

    return success;
}

HSTREAM RenderScreen::getAudioHandle() const
{
    return this->m_presenter.getAudioHandle();
}

void RenderScreen::setDeinterlaceMethod(AnyVODEnums::DeinterlaceMethod method)
{
    QString methodDesc;

    this->m_presenter.getDeinterlacer().setMethod(method);

    switch (method)
    {
        case AnyVODEnums::DM_AUTO:
            methodDesc = trUtf8("자동 판단");
            break;
        case AnyVODEnums::DM_USE:
            methodDesc = trUtf8("항상 사용");
            break;
        case AnyVODEnums::DM_NOUSE:
            methodDesc = trUtf8("사용 안 함");
            break;
        default:
            break;
    }

    this->m_presenter.showOptionDesc(trUtf8("디인터레이스 (%1)").arg(methodDesc));
}

void RenderScreen::setDeinterlaceAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm)
{
    QString algorithmDesc = this->getDeinterlaceDesc(algorithm);

    this->m_presenter.setDeinterlacerAlgorithm(algorithm);
    this->m_presenter.showOptionDesc(trUtf8("디인터레이스 알고리즘 (%1)").arg(algorithmDesc));
}

AnyVODEnums::DeinterlaceMethod RenderScreen::getDeinterlaceMethod()
{
    return this->m_presenter.getDeinterlacer().getMethod();
}

AnyVODEnums::DeinterlaceAlgorithm RenderScreen::getDeinterlaceAlgorithm()
{
    return this->m_presenter.getDeinterlacer().getAlgorithm();
}

void RenderScreen::setHAlign(AnyVODEnums::HAlignMethod align)
{
    QString desc;
    QString alignDesc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 가로 정렬 변경");
    else
        desc = trUtf8("가사 가로 정렬 변경");

    switch (align)
    {
        case AnyVODEnums::HAM_AUTO:
            alignDesc = trUtf8("자동 정렬");
            break;
        case AnyVODEnums::HAM_LEFT:
            alignDesc = trUtf8("왼쪽 정렬");
            break;
        case AnyVODEnums::HAM_RIGHT:
            alignDesc = trUtf8("오른쪽 정렬");
            break;
        case AnyVODEnums::HAM_MIDDLE:
            alignDesc = trUtf8("가운데 정렬");
            break;
        case AnyVODEnums::HAM_NONE:
            alignDesc = trUtf8("기본 정렬");
            break;
        default:
            break;
    }

    desc += QString(" (%1)").arg(alignDesc);

    this->m_presenter.setHAlign(align);
    this->m_presenter.showOptionDesc(desc);
}

AnyVODEnums::HAlignMethod RenderScreen::getHAlign() const
{
    return this->m_presenter.getHAlign();
}

void RenderScreen::setVAlign(AnyVODEnums::VAlignMethod align)
{
    QString desc;
    QString alignDesc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 세로 정렬 변경");
    else
        desc = trUtf8("가사 세로 정렬 변경");

    switch (align)
    {
        case AnyVODEnums::VAM_TOP:
            alignDesc = trUtf8("상단 정렬");
            break;
        case AnyVODEnums::VAM_BOTTOM:
            alignDesc = trUtf8("하단 정렬");
            break;
        case AnyVODEnums::VAM_NONE:
            alignDesc = trUtf8("기본 정렬");
            break;
        default:
            break;
    }

    desc += QString(" (%1)").arg(alignDesc);

    this->m_presenter.setVAlign(align);
    this->m_presenter.showOptionDesc(desc);
}

AnyVODEnums::VAlignMethod RenderScreen::getVAlign() const
{
    return this->m_presenter.getVAlign();
}

void RenderScreen::prevSubtitleSync(double amount)
{
    this->subtitleSync(-amount);
}

void RenderScreen::nextSubtitleSync(double amount)
{
    this->subtitleSync(amount);
}

void RenderScreen::resetSubtitleSync()
{
    if (!this->m_presenter.existSubtitle())
        return;

    QString desc;

    if (this->isMovieSubtitleVisiable())
        desc = trUtf8("자막 싱크 초기화");
    else
        desc = trUtf8("가사 싱크 초기화");

    this->m_presenter.setSubtitleSync(0.0);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::subtitleSync(double value)
{
    double current = this->m_presenter.getSubtitleSync();
    double target = value + current;

    this->m_presenter.setSubtitleSync(target);

    QString desc;

    if (this->isMovieSubtitleVisiable())
    {
        if (value > 0.0)
            desc = trUtf8("자막 싱크 %1초 빠르게");
        else
            desc = trUtf8("자막 싱크 %1초 느리게");
    }
    else
    {
        if (value > 0.0)
            desc = trUtf8("가사 싱크 %1초 빠르게");
        else
            desc = trUtf8("가사 싱크 %1초 느리게");
    }

    desc = desc.arg(fabs(value));

    target = Utils::zeroDouble(target);
    desc += trUtf8(" (%1초)").arg(target);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::prevAudioSync(double amount)
{
    this->audioSync(-amount);
}

void RenderScreen::nextAudioSync(double amount)
{
    this->audioSync(amount);
}

void RenderScreen::resetAudioSync()
{
    QString desc;

    desc = trUtf8("소리 싱크 초기화");

    this->m_presenter.audioSync(0.0);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isEnableSearchSubtitle() const
{
    return this->m_presenter.isEnableSearchSubtitle();
}

bool RenderScreen::isEnableSearchLyrics() const
{
    return this->m_presenter.isEnableSearchLyrics();
}

void RenderScreen::enableSearchSubtitle(bool enable)
{
    QString desc;

    if (enable)
        desc = trUtf8("자막 찾기 켜짐");
    else
        desc = trUtf8("자막 찾기 꺼짐");

    this->m_presenter.enableSearchSubtitle(enable);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::enableSearchLyrics(bool enable)
{
    QString desc;

    if (enable)
        desc = trUtf8("가사 찾기 켜짐");
    else
        desc = trUtf8("가사 찾기 꺼짐");

    this->m_presenter.enableSearchLyrics(enable);
    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::audioSync(double value)
{
    double current = this->m_presenter.getAudioSync();
    double target = value + current;

    this->m_presenter.audioSync(target);

    QString desc;

    if (value > 0.0)
        desc = trUtf8("소리 싱크 %1초 빠르게");
    else
        desc = trUtf8("소리 싱크 %1초 느리게");

    desc = desc.arg(fabs(value));

    target = Utils::zeroDouble(target);
    desc += trUtf8(" (%1초)").arg(target);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::rewind(double distance)
{
    this->navigate(-distance);
}

void RenderScreen::forward(double distance)
{
    this->navigate(distance);
}

int RenderScreen::getMaxVolume() const
{
    return this->m_presenter.getMaxVolume();
}

void RenderScreen::volume(int volume)
{
    this->m_presenter.volume((uint8_t)volume);

    int perVolume = (int)((float)volume / this->m_presenter.getMaxVolume() * 100);
    QString desc = trUtf8("소리 (%1%)").arg(perVolume);

    this->m_presenter.showOptionDesc(desc);
}

void RenderScreen::mute(bool mute)
{
    this->m_presenter.mute(mute);

    QString desc;

    if (mute)
        desc = trUtf8("소리 꺼짐");
    else
        desc = trUtf8("소리 켜짐");

    this->m_presenter.showOptionDesc(desc);
}

int RenderScreen::getVolume() const
{
    return this->m_presenter.getVolume();
}

double RenderScreen::getDuration() const
{
    return this->m_presenter.getDuration();
}

double RenderScreen::getCurrentPosition()
{
    return this->m_presenter.getCurrentPosition();
}

bool RenderScreen::hasDuration() const
{
    return this->m_presenter.hasDuration();
}

double RenderScreen::getAspectRatio(bool widthPrio) const
{
    return this->m_presenter.getAspectRatio(widthPrio);
}

void RenderScreen::setMaxTextureSize(int size)
{
    this->m_presenter.setMaxTextureSize(size);
}

int RenderScreen::getMaxTextureSize() const
{
    return this->m_presenter.getMaxTextureSize();
}

RenderScreen::Status RenderScreen::getStatus()
{
    if (!this->m_presenter.isRunning() && this->m_status != Exit)
        this->m_status = Stopped;

    return this->m_status;
}

bool RenderScreen::isEnabledVideo() const
{
    return this->m_presenter.isEnabledVideo();
}

bool RenderScreen::isAudio() const
{
    return this->m_presenter.isAudio();
}

bool RenderScreen::isVideo() const
{
    return this->m_presenter.isVideo();
}

bool RenderScreen::isAlignable()
{
    return this->m_presenter.isAlignable();
}

bool RenderScreen::isSubtitleMoveable()
{
    return this->existSubtitle() && this->isVideo();
}

bool RenderScreen::is3DSubtitleMoveable()
{
    return this->isSubtitleMoveable() && this->m_presenter.getSubtitle3DMethod() != AnyVODEnums::S3M_NONE;
}

bool RenderScreen::isMovieSubtitleVisiable() const
{
    return this->isVideo() || !this->isOpened();
}

void RenderScreen::showAlbumJacket(bool show)
{
    QString desc;

    if (show)
        desc = trUtf8("앨범 자켓 보이기");
    else
        desc = trUtf8("앨범 자켓 숨기기");

    this->m_presenter.showAlbumJacket(show);
    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isShowAlbumJacket() const
{
    return this->m_presenter.isShowAlbumJacket();
}

QSize RenderScreen::getFrameSize() const
{
    int width;
    int height;

    if (this->m_presenter.getFrameSize(&width, &height))
        return QSize(width, height);
    else
        return QSize();
}

QRect RenderScreen::getPictureRect() const
{
    QRect rect;

    if (this->m_presenter.getPictureRect(&rect))
        return rect;
    else
        return QRect();
}

void RenderScreen::enableGotoLastPos(bool enable)
{
    this->m_gotoLastPlay = enable;

    QString desc;

    if (enable)
        desc = trUtf8("재생 위치 기억 켜짐");
    else
        desc = trUtf8("재생 위치 기억 꺼짐");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isGotoLastPos() const
{
    return this->m_gotoLastPlay;
}

void RenderScreen::setPlayingMethod(AnyVODEnums::PlayingMethod method)
{
    this->m_playingMethod = method;
}

void RenderScreen::selectPlayingMethod(int method)
{
    this->setPlayingMethod((AnyVODEnums::PlayingMethod)method);
}

AnyVODEnums::PlayingMethod RenderScreen::getPlayingMethod() const
{
    return this->m_playingMethod;
}

QString RenderScreen::get3DMethodDesc(int method) const
{
    return this->m_3DMethodDesc[method];
}

QString RenderScreen::get3DMethodCategory(int method) const
{
    return this->m_3DMethodCategory[method];
}

QString RenderScreen::getSubtitle3DMethodDesc(int method) const
{
    return this->m_subtitle3DMethodDesc[method];
}

QString RenderScreen::getAnaglyphAlgoritmDesc(int algorithm) const
{
    return this->m_anaglyphAlgorithmDesc[algorithm];
}

QString RenderScreen::getDeinterlaceDesc(int algorithm) const
{
    return this->m_deinterlaceDesc[algorithm];
}

QString RenderScreen::getVRInputSourceDesc(int source) const
{
    return this->m_vrInputSourceDesc[source];
}

int RenderScreen::getCaptureExtCount()
{
    return CAPTURE_FORMAT_LIST.count();
}

QString RenderScreen::getCaptureExt(int index)
{
    return CAPTURE_FORMAT_LIST[index].toUpper();
}

void RenderScreen::audioNormalize()
{
    this->m_presenter.useNormalizer(!this->m_presenter.isUsingNormalizer());
}

void RenderScreen::audioEqualizer()
{
    this->m_presenter.useEqualizer(!this->m_presenter.isUsingEqualizer());
}

void RenderScreen::upSubtitlePosition()
{
    if (this->isSubtitleMoveable())
        this->setVerticalSubtitlePosition(1);
}

void RenderScreen::downSubtitlePosition()
{
    if (this->isSubtitleMoveable())
        this->setVerticalSubtitlePosition(-1);
}

void RenderScreen::leftSubtitlePosition()
{
    if (this->isSubtitleMoveable())
        this->setHorizontalSubtitlePosition(1);
}

void RenderScreen::rightSubtitlePosition()
{
    if (this->isSubtitleMoveable())
        this->setHorizontalSubtitlePosition(-1);
}

void RenderScreen::rewindProper()
{
    if (this->isAudio())
        this->rewind5();
    else
        this->rewind30();
}

void RenderScreen::forwardProper()
{
    if (this->isAudio())
        this->forward5();
    else
        this->forward30();
}

void RenderScreen::rewind5()
{
    this->rewind(5.0);
}

void RenderScreen::forward5()
{
    this->forward(5.0);
}

void RenderScreen::rewind30()
{
    this->rewind(30.0);
}

void RenderScreen::forward30()
{
    this->forward(30.0);
}

void RenderScreen::rewind60()
{
    this->rewind(60.0);
}

void RenderScreen::forward60()
{
    this->forward(60.0);
}

void RenderScreen::gotoBegin()
{
    if (this->hasDuration())
    {
        this->seek(0.0, false);
        this->m_presenter.showOptionDesc(trUtf8("처음으로 이동"));
    }
}

bool RenderScreen::prev(bool check)
{
    if (check)
    {
        if (this->isPrevEnabled())
            return this->playPrev();
    }
    else
    {
        return this->playPrev();
    }

    return false;
}

bool RenderScreen::next(bool check)
{
    if (check)
    {
        if (this->isNextEnabled())
            return this->playNext();
    }
    else
    {
        return this->playNext();
    }

    return false;
}

void RenderScreen::playOrder()
{
    AnyVODEnums::PlayingMethod method = this->getPlayingMethod();

    method = (AnyVODEnums::PlayingMethod)(method + 1);

    if (method >= AnyVODEnums::PM_COUNT)
        method = AnyVODEnums::PM_TOTAL;

    this->setPlayingMethod(method);
}

void RenderScreen::subtitleToggle()
{
    this->m_presenter.showSubtitle(!this->m_presenter.isShowSubtitle());
}

void RenderScreen::prevSubtitleSync()
{
    if (this->existSubtitle())
        this->prevSubtitleSync(0.5);
}

void RenderScreen::nextSubtitleSync()
{
    if (this->existSubtitle())
        this->nextSubtitleSync(0.5);
}

void RenderScreen::selectSubtitleClass(const QString &className)
{
    QString currentName;

    this->m_presenter.getCurrentSubtitleClass(&currentName);

    if (currentName != className)
    {
        if (!this->m_presenter.setCurrentSubtitleClass(className))
        {
            QString desc;

            if (this->isMovieSubtitleVisiable())
                desc = trUtf8("자막을 변경 할 수 없습니다");
            else
                desc = trUtf8("가사를 변경 할 수 없습니다");

            this->m_presenter.showOptionDesc(desc);
        }
    }
}

void RenderScreen::selectAudioStream(int index)
{
    if (this->m_presenter.getCurrentAudioStreamIndex() != index)
    {
        if (!this->m_presenter.changeAudioStream(index))
            this->m_presenter.showOptionDesc(trUtf8("음성을 변경 할 수 없습니다"));
    }
}

void RenderScreen::subtitleLanguageOrder()
{
    QStringList classes;
    QString curClass;

    this->m_presenter.getSubtitleClasses(&classes);
    this->m_presenter.getCurrentSubtitleClass(&curClass);

    if (curClass.isEmpty() && classes.count() > 0)
    {
        this->selectSubtitleClass(classes[0]);
        return;
    }

    for (int i = 0; i < classes.count(); i++)
    {
        if (curClass == classes[i])
        {
            QString selClass;

            if (i + 1 >= classes.count())
                selClass = classes[0];
            else
                selClass = classes[i + 1];

            this->selectSubtitleClass(selClass);

            break;
        }
    }
}

void RenderScreen::audioOrder()
{
    QVector<AudioStreamInfo> info;
    int index = this->m_presenter.getCurrentAudioStreamIndex();

    this->m_presenter.getAudioStreamInfo(&info);

    if (index < 0 && info.count() > 0)
    {
        this->selectAudioStream(info[0].index);
        return;
    }

    for (int i = 0; i < info.count(); i++)
    {
        if (index == info[i].index)
        {
            int selIndex;

            if (i + 1 >= info.count())
                selIndex = info[0].index;
            else
                selIndex = info[i + 1].index;

            this->selectAudioStream(selIndex);

            break;
        }
    }
}

void RenderScreen::detail()
{
    if (this->isEnabledVideo())
        this->showDetail(!this->isShowDetail());
}

void RenderScreen::deinterlaceMethodOrder()
{
    AnyVODEnums::DeinterlaceMethod method = this->getDeinterlaceMethod();

    method = (AnyVODEnums::DeinterlaceMethod)(method + 1);

    if (method >= AnyVODEnums::DM_COUNT)
        method = AnyVODEnums::DM_AUTO;

    this->setDeinterlaceMethod(method);
}

void RenderScreen::deinterlaceAlgorithmOrder()
{
    AnyVODEnums::DeinterlaceAlgorithm algorithm = this->getDeinterlaceAlgorithm();

    algorithm = (AnyVODEnums::DeinterlaceAlgorithm)(algorithm + 1);

    if (algorithm >= AnyVODEnums::DA_COUNT)
        algorithm = AnyVODEnums::DA_BLEND;

    this->setDeinterlaceAlgorithm(algorithm);
}

void RenderScreen::selectDeinterlacerMethod(int method)
{
    this->setDeinterlaceMethod((AnyVODEnums::DeinterlaceMethod)method);
}

void RenderScreen::selectDeinterlacerAlgorithem(int algorithm)
{
    this->setDeinterlaceAlgorithm((AnyVODEnums::DeinterlaceAlgorithm)algorithm);
}

void RenderScreen::prevAudioSync()
{
    this->prevAudioSync(0.05);
}

void RenderScreen::nextAudioSync()
{
    this->nextAudioSync(0.05);
}

void RenderScreen::subtitleHAlignOrder()
{
    if (this->isAlignable())
    {
        AnyVODEnums::HAlignMethod align = this->getHAlign();

        align = (AnyVODEnums::HAlignMethod)(align + 1);

        if (align >= AnyVODEnums::HAM_COUNT)
            align = AnyVODEnums::HAM_NONE;

        this->setHAlign(align);
    }
}

void RenderScreen::selectSubtitleHAlignMethod(int method)
{
    if (this->isAlignable())
        this->setHAlign((AnyVODEnums::HAlignMethod)method);
}

void RenderScreen::repeatRangeStart()
{
    if (this->hasDuration())
    {
        double pos = this->getCurrentPosition();

        this->setRepeatStart(pos);
    }
}

void RenderScreen::repeatRangeEnd()
{
    if (this->hasDuration())
    {
        double pos = this->getCurrentPosition();

        this->setRepeatEnd(pos);
    }
}

void RenderScreen::repeatRangeEnable()
{
    this->setRepeatEnable(!this->getRepeatEnable());
}

void RenderScreen::seekKeyFrame()
{
    this->setSeekKeyFrame(!this->isSeekKeyFrame());
}

void RenderScreen::skipOpening()
{
    this->setSkipOpening(!this->getSkipOpening());
}

void RenderScreen::skipEnding()
{
    this->setSkipEnding(!this->getSkipEnding());
}

void RenderScreen::useSkipRange()
{
    this->setUseSkipRange(!this->getUseSkipRange());
}

void RenderScreen::captureExtOrder()
{
    GLRenderer::CaptureInfo info;
    int index = 0;

    info = this->m_renderer.getCaptureInfo();

    for (; index < CAPTURE_FORMAT_LIST.count(); index++)
    {
        if (CAPTURE_FORMAT_LIST.at(index).toUpper() == info.ext)
            break;
    }

    index++;

    if (index >= CAPTURE_FORMAT_LIST.count())
        index = 0;

    info.ext = CAPTURE_FORMAT_LIST.at(index);

    this->m_renderer.setCaptureInfo(info);
}

void RenderScreen::captureSingle()
{
    if (this->isEnabledVideo())
    {
        GLRenderer::CaptureInfo info;

        info = this->m_renderer.getCaptureInfo();

        info.capture = true;
        info.captureOrg = true;
        info.captureCount = 1;
        info.totalCount = 1;

        this->m_renderer.setCaptureInfo(info);
    }
}

void RenderScreen::selectCaptureExt(const QString &ext)
{
    GLRenderer::CaptureInfo info = this->m_renderer.getCaptureInfo();

    info.ext = ext.toUpper();

    this->m_renderer.setCaptureInfo(info);
}

QString RenderScreen::getCaptureExt() const
{
    return this->m_renderer.getCaptureInfo().ext;
}

void RenderScreen::setCaptureDirectory(const QString &dir)
{
    GLRenderer::CaptureInfo info = this->m_renderer.getCaptureInfo();

    info.savePath = dir;

    Utils::appendDirSeparator(&info.savePath);
    this->m_renderer.setCaptureInfo(info);
}

QString RenderScreen::getCaptureDirectory() const
{
    return this->m_renderer.getCaptureInfo().savePath;
}

void RenderScreen::prevFrame()
{
    if (this->canMoveFrame())
        this->prevFrame(1);
}

void RenderScreen::nextFrame()
{
    if (this->canMoveFrame())
        this->nextFrame(1);
}

void RenderScreen::slowerPlayback()
{
    if (this->isTempoUsable())
    {
        int cur = (int)this->getTempo();

        if (cur > -90)
            this->setTempo(cur - 10.0f);
    }
}

void RenderScreen::fasterPlayback()
{
    if (this->isTempoUsable())
    {
        int cur = (int)this->getTempo();

        this->setTempo(cur + 10.0f);
    }
}

void RenderScreen::normalPlayback()
{
    if (this->isTempoUsable())
        this->setTempo(0.0f);
}

void RenderScreen::resetVideoAttribute()
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && this->isEnabledVideo())
    {
        shader->setBrightness(1.0);
        shader->setContrast(1.0);
        shader->setHue(0.0);
        shader->setSaturation(1.0);

        this->m_presenter.showOptionDesc(trUtf8("영상 속성 초기화"));
    }
}

void RenderScreen::brightnessDown()
{
    this->brightness(-0.01);
}

void RenderScreen::brightnessUp()
{
    this->brightness(0.01);
}

void RenderScreen::brightness(double step)
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && this->isEnabledVideo())
    {
        double value = shader->getBrightness();

        value += step;
        value = Utils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        shader->setBrightness(value);
        this->m_presenter.showOptionDesc(trUtf8("영상 밝기 %1배").arg(value));
    }
}

void RenderScreen::saturationDown()
{
   this->saturation(-0.01);
}

void RenderScreen::saturationUp()
{
   this->saturation(0.01);
}

void RenderScreen::saturation(double step)
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && this->isEnabledVideo())
    {
        double value = shader->getSaturation();

        value += step;
        value = Utils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        shader->setSaturation(value);
        this->m_presenter.showOptionDesc(trUtf8("영상 채도 %1배").arg(value));
    }
}

void RenderScreen::hueDown()
{
    this->hue(-1.0 / 360.0);
}

void RenderScreen::hueUp()
{
    this->hue(1.0 / 360.0);
}

void RenderScreen::hue(double step)
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && this->isEnabledVideo())
    {
        double value = shader->getHue();

        value += step;
        value = Utils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        if (value > 1.0)
            value = 1.0;

        shader->setHue(value);
        this->m_presenter.showOptionDesc(trUtf8("영상 색상 각도 : %1°").arg((int)(value * 360.0)));
    }
}

void RenderScreen::contrastDown()
{
    this->contrast(-0.01);
}

void RenderScreen::contrastUp()
{
   this->contrast(0.01);
}

void RenderScreen::contrast(double step)
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && this->isEnabledVideo())
    {
        double value = shader->getContrast();

        value += step;
        value = Utils::zeroDouble(value);

        if (value < 0.0)
            value = 0.0;

        shader->setContrast(value);
        this->m_presenter.showOptionDesc(trUtf8("영상 대비 %1배").arg(value));
    }
}

void RenderScreen::lowerMusic()
{
    this->useLowerMusic(!this->isUsingLowerMusic());
}

void RenderScreen::lowerVoice()
{
    this->useLowerVoice(!this->isUsingLowerVoice());
}

void RenderScreen::higherVoice()
{
    this->useHigherVoice(!this->isUsingHigherVoice());
}

void RenderScreen::sharply()
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && this->isEnabledVideo())
    {
        bool use = !shader->isUsingSharply();

        shader->useSharply(use);

        QString desc;

        if (use)
            desc = trUtf8("영상 날카롭게 켜짐");
        else
            desc = trUtf8("영상 날카롭게 꺼짐");

        this->m_presenter.showOptionDesc(desc);
    }
}

void RenderScreen::sharpen()
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && this->isEnabledVideo())
    {
        bool use = !shader->isUsingSharpen();

        shader->useSharpen(use);

        QString desc;

        if (use)
            desc = trUtf8("영상 선명하게 켜짐");
        else
            desc = trUtf8("영상 선명하게 꺼짐");

        this->m_presenter.showOptionDesc(desc);
    }
}

void RenderScreen::soften()
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && this->isEnabledVideo())
    {
        bool use = !shader->isUsingSoften();

        shader->useSoften(use);

        QString desc;

        if (use)
            desc = trUtf8("영상 부드럽게 켜짐");
        else
            desc = trUtf8("영상 부드럽게 꺼짐");

        this->m_presenter.showOptionDesc(desc);
    }
}

void RenderScreen::leftRightInvert()
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && this->isEnabledVideo())
    {
        bool use = !shader->isUsingLeftRightInvert();

        shader->useLeftRightInvert(use);

        QString desc;

        if (use)
            desc = trUtf8("영상 좌우 반전 켜짐");
        else
            desc = trUtf8("영상 좌우 반전 꺼짐");

        this->m_presenter.showOptionDesc(desc);
    }
}

void RenderScreen::topBottomInvert()
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && this->isEnabledVideo())
    {
        bool use = !shader->isUsingTopBottomInvert();

        shader->useTopBottomInvert(use);

        QString desc;

        if (use)
            desc = trUtf8("영상 상하 반전 켜짐");
        else
            desc = trUtf8("영상 상하 반전 꺼짐");

        this->m_presenter.showOptionDesc(desc);
    }
}

void RenderScreen::incSubtitleOpaque()
{
    if (this->existSubtitle())
        this->addSubtitleOpaque(0.05f);
}

void RenderScreen::decSubtitleOpaque()
{
    if (this->existSubtitle())
        this->addSubtitleOpaque(-0.05f);
}

void RenderScreen::audioDeviceOrder()
{
    QStringList list;
    int curDevice = this->getCurrentAudioDevice() + 1;

    list = this->getAudioDevices();

    if (curDevice >= list.count())
        curDevice = -1;

    this->selectAudioDevice(curDevice);
}

void RenderScreen::selectAudioDevice(int device)
{
    this->setAudioDevice(device);
}

void RenderScreen::enableSearchSubtitle()
{
    this->enableSearchSubtitle(!this->isEnableSearchSubtitle());

    if (this->isEnableSearchSubtitle())
    {
        this->retreiveExternalSubtitle(this->getFilePath());
        this->checkGOMSubtitle();
    }
}

void RenderScreen::enableSearchLyrics()
{
    this->enableSearchLyrics(!this->isEnableSearchLyrics());

    if (this->isEnableSearchLyrics())
    {
        this->retreiveExternalSubtitle(this->getFilePath());

        if (this->existAudioSubtitle())
        {
            this->setScreenKeepOn(true);
            this->setScreenOrientation(true);

            emit this->viewLyrics();
        }
    }
}

void RenderScreen::subtitleVAlignOrder()
{
    if (this->isAlignable())
    {
        AnyVODEnums::VAlignMethod align = this->getVAlign();

        align = (AnyVODEnums::VAlignMethod)(align + 1);

        if (align >= AnyVODEnums::VAM_COUNT)
            align = AnyVODEnums::VAM_NONE;

        this->setVAlign(align);
    }
}

void RenderScreen::selectSubtitleVAlignMethod(int method)
{
    if (this->isAlignable())
        this->setVAlign((AnyVODEnums::VAlignMethod)method);
}

void RenderScreen::incSubtitleSize()
{
    if (this->isAlignable())
        this->addSubtitleSize(0.01f);
}

void RenderScreen::decSubtitleSize()
{
    if (this->isAlignable())
        this->addSubtitleSize(-0.01f);
}

void RenderScreen::userAspectRatioOrder()
{
    int index = this->m_userAspectRatioList.curIndex + 1;

    if (index >= this->m_userAspectRatioList.list.count())
        index = 0;

    this->selectUserAspectRatio(index);
}

void RenderScreen::selectUserAspectRatio(int index)
{
    MediaPresenter::UserAspectRatio ratio;
    UserAspectRatioItem &item = this->m_userAspectRatioList.list[index];

    if (item.isInvalid())
    {
        ratio.use = false;
    }
    else if (item.isFullScreen())
    {
        ratio.use = true;
        ratio.fullscreen = true;
    }
    else
    {
        ratio.use = true;

        ratio.width = item.width;
        ratio.height = item.height;
    }

    this->m_userAspectRatioList.curIndex = index;
    this->setUserAspectRatio(ratio);
}

int RenderScreen::getCurrentUserAspectRatio() const
{
    return this->m_userAspectRatioList.curIndex;
}

int RenderScreen::getUserAspectRatioCount() const
{
    return this->m_userAspectRatioList.list.count();
}

void RenderScreen::setCustomUserAspectRatio(const QSizeF &size)
{
    UserAspectRatioItem &item = this->m_userAspectRatioList.list.last();

    item.width = size.width();
    item.height = size.height();
}

QSizeF RenderScreen::getCustomUserAspectRatio() const
{
    const UserAspectRatioItem &item = this->m_userAspectRatioList.list.last();

    return QSizeF(item.width, item.height);
}

QString RenderScreen::getUserAspectRatioDesc(int index) const
{
    const UserAspectRatioItem &item = this->m_userAspectRatioList.list[index];
    QString desc;

    if (item.isInvalid())
        desc = trUtf8("사용 안 함");
    else if (item.isFullScreen())
        desc = trUtf8("화면 채우기");
    else if (index == this->m_userAspectRatioList.list.count() - 1)
        desc = trUtf8("사용자 지정 (%1:%2)").arg(item.width).arg(item.height);
    else
        desc = QString("%1:%2").arg(item.width).arg(item.height);

    return desc;
}

void RenderScreen::useHWDecoder()
{
    this->useHWDecoder(!this->isUseHWDecoder());
}

void RenderScreen::method3DOrder()
{
    AnyVODEnums::Video3DMethod method = this->get3DMethod();

    method = (AnyVODEnums::Video3DMethod)(method + 1);

    if (method >= AnyVODEnums::V3M_COUNT)
        method = AnyVODEnums::V3M_NONE;

    this->select3DMethod(method);
}

void RenderScreen::select3DMethod(int method)
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader)
    {
        this->set3DMethod((AnyVODEnums::Video3DMethod)method);
        shader->set3DMethod((AnyVODEnums::Video3DMethod)method);
    }
}

void RenderScreen::selectVRInputSource(int source)
{
    this->setVRInputSource((AnyVODEnums::VRInputSource)source);
}

void RenderScreen::prevChapter()
{
    if (this->canMoveChapter())
    {
        double pos = this->getCurrentPosition();
        QString desc;

        if (this->moveChapter(this->getChapterIndex(pos) - 1, &desc))
            this->m_presenter.showOptionDesc(trUtf8("이전 챕터로 이동 (%1)").arg(desc));
    }
}

void RenderScreen::nextChapter()
{
    if (this->canMoveChapter())
    {
        double pos = this->getCurrentPosition();
        QString desc;

        if (this->moveChapter(this->getChapterIndex(pos) + 1, &desc))
            this->m_presenter.showOptionDesc(trUtf8("다음 챕터로 이동 (%1)").arg(desc));
    }
}

bool RenderScreen::moveChapter(int index, QString *desc)
{
    const QVector<ChapterInfo> &list = this->getChapters();

    if (index < 0 || index >= list.count())
        return false;

    this->seek(list[index].start, true);
    *desc = list[index].desc;

    return true;
}

int RenderScreen::getChapterIndex(double pos)
{
    return Utils::findChapter(pos, this->getChapters());
}

void RenderScreen::closeExternalSubtitle()
{
    if (this->existExternalSubtitle())
    {
        this->closeAllExternalSubtitles();
        this->resetLyrics();
    }
}

void RenderScreen::showAlbumJacket()
{
    this->showAlbumJacket(!this->isShowAlbumJacket());
}

void RenderScreen::lastPlay()
{
    this->enableGotoLastPos(!this->isGotoLastPos());
}

void RenderScreen::repeatRangeStartBack100MS()
{
    if (this->hasDuration())
        this->setRepeatStartMove(-0.1);
}

void RenderScreen::repeatRangeStartForw100MS()
{
    if (this->hasDuration())
        this->setRepeatStartMove(0.1);
}

void RenderScreen::repeatRangeEndBack100MS()
{
    if (this->hasDuration())
        this->setRepeatEndMove(-0.1);
}

void RenderScreen::repeatRangeEndForw100MS()
{
    if (this->hasDuration())
        this->setRepeatEndMove(0.1);
}

void RenderScreen::repeatRangeBack100MS()
{
    if (this->hasDuration())
        this->setRepeatMove(-0.1);
}

void RenderScreen::repeatRangeForw100MS()
{
    if (this->hasDuration())
        this->setRepeatMove(0.1);
}

void RenderScreen::useFrameDrop()
{
    this->useFrameDrop(!this->isUseFrameDrop());
}

void RenderScreen::anaglyphAlgorithmOrder()
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader && shader->canUseAnaglyphAlgorithm())
    {
        AnyVODEnums::AnaglyphAlgorithm algorithm = shader->getAnaglyphAlgorithm();

        algorithm = (AnyVODEnums::AnaglyphAlgorithm)(algorithm + 1);

        if (algorithm >= AnyVODEnums::AGA_COUNT)
            algorithm = AnyVODEnums::AGA_DEFAULT;

        this->selectAnaglyphAlgorithm(algorithm);

        this->m_presenter.showOptionDesc(trUtf8("애너글리프 알고리즘 (%1)").arg(this->getAnaglyphAlgoritmDesc(algorithm)));
    }
}

void RenderScreen::selectAnaglyphAlgorithm(int algorithm)
{
    ShaderCompositer *shader = this->m_renderer.getShader();

    if (shader)
        shader->setAnaglyphAlgorithm((AnyVODEnums::AnaglyphAlgorithm)algorithm);
}

void RenderScreen::method3DSubtitleOrder()
{
    AnyVODEnums::Subtitle3DMethod method = this->getSubtitle3DMethod();

    method = (AnyVODEnums::Subtitle3DMethod)(method + 1);

    if (method >= AnyVODEnums::S3M_COUNT)
        method = AnyVODEnums::S3M_NONE;

    this->setSubtitle3DMethod(method);
}

void RenderScreen::select3DSubtitleMethod(int method)
{
    this->setSubtitle3DMethod((AnyVODEnums::Subtitle3DMethod)method);
}

void RenderScreen::use3DFull()
{
    if (this->get3DMethod() != AnyVODEnums::V3M_NONE)
        this->use3DFull(!this->isUse3DFull());
}

void RenderScreen::up3DSubtitleOffset()
{
    if (this->is3DSubtitleMoveable())
        this->setVertical3DSubtitleOffset(1);
}

void RenderScreen::down3DSubtitleOffset()
{
    if (this->is3DSubtitleMoveable())
        this->setVertical3DSubtitleOffset(-1);
}

void RenderScreen::left3DSubtitleOffset()
{
    if (this->is3DSubtitleMoveable())
        this->setHorizontal3DSubtitleOffset(1);
}

void RenderScreen::right3DSubtitleOffset()
{
    if (this->is3DSubtitleMoveable())
        this->setHorizontal3DSubtitleOffset(-1);
}

void RenderScreen::screenRotationDegreeOrder()
{
    AnyVODEnums::ScreenRotationDegree degree = this->getScreenRotationDegree();

    degree = (AnyVODEnums::ScreenRotationDegree)(degree + 1);

    if (degree >= AnyVODEnums::SRD_COUNT)
        degree = AnyVODEnums::SRD_NONE;

    this->selectScreenRotationDegree(degree);
}

void RenderScreen::selectScreenRotationDegree(int degree)
{
    this->setScreenRotationDegree((AnyVODEnums::ScreenRotationDegree)degree);
}

void RenderScreen::useSearchSubtitleComplex()
{
    this->setSearchSubtitleComplex(!this->getSearchSubtitleComplex());
}

void RenderScreen::histEQ()
{
    if (this->isEnabledVideo())
    {
        FilterGraph &graph = this->m_presenter.getFilterGraph();
        bool use = !graph.getEnableHistEQ();

        graph.setEnableHistEQ(use);

        QString desc;

        if (use)
            desc = trUtf8("히스토그램 이퀄라이저 켜짐");
        else
            desc = trUtf8("히스토그램 이퀄라이저 꺼짐");

        this->m_presenter.showOptionDesc(desc);
        this->m_presenter.configFilterGraph();
    }
}

void RenderScreen::hq3DDenoise()
{
    if (this->isEnabledVideo())
    {
        FilterGraph &graph = this->m_presenter.getFilterGraph();
        bool use = !graph.getEnableHighQuality3DDenoise();

        graph.setEnableHighQuality3DDenoise(use);

        QString desc;

        if (use)
            desc = trUtf8("3D 노이즈 제거 켜짐");
        else
            desc = trUtf8("3D 노이즈 제거 꺼짐");

        this->m_presenter.showOptionDesc(desc);
        this->m_presenter.configFilterGraph();
    }
}

void RenderScreen::ataDenoise()
{
    if (this->isEnabledVideo())
    {
        FilterGraph &graph = this->m_presenter.getFilterGraph();
        bool use = !graph.getEnableATADenoise();

        graph.setEnableATADenoise(use);

        QString desc;

        if (use)
            desc = trUtf8("적응 시간 평균 노이즈 제거 켜짐");
        else
            desc = trUtf8("적응 시간 평균 노이즈 제거 꺼짐");

        this->m_presenter.showOptionDesc(desc);
        this->m_presenter.configFilterGraph();
    }
}

void RenderScreen::owDenoise()
{
    if (this->isEnabledVideo())
    {
        FilterGraph &graph = this->m_presenter.getFilterGraph();
        bool use = !graph.getEnableOWDenoise();

        graph.setEnableOWDenoise(use);

        QString desc;

        if (use)
            desc = trUtf8("Overcomplete Wavelet 노이즈 제거 켜짐");
        else
            desc = trUtf8("Overcomplete Wavelet 노이즈 제거 꺼짐");

        this->m_presenter.showOptionDesc(desc);
        this->m_presenter.configFilterGraph();
    }
}

void RenderScreen::deband()
{
    if (this->isEnabledVideo())
    {
        FilterGraph &graph = this->m_presenter.getFilterGraph();
        bool use = !graph.getEnableDeBand();

        graph.setEnableDeBand(use);

        QString desc;

        if (use)
            desc = trUtf8("디밴드 켜짐");
        else
            desc = trUtf8("디밴드 꺼짐");

        this->m_presenter.showOptionDesc(desc);
        this->m_presenter.configFilterGraph();
    }
}

void RenderScreen::useBufferingMode()
{
    this->useBufferingMode(!this->isUseBufferingMode());
}

bool RenderScreen::isPrevEnabled() const
{
    QQuickItem *prev = this->findChild<QQuickItem*>("prevButton");

    if (prev)
        return prev->isEnabled();

    return false;
}

bool RenderScreen::isNextEnabled() const
{
    QQuickItem *next = this->findChild<QQuickItem*>("nextButton");

    if (next)
        return next->isEnabled();

    return false;
}

bool RenderScreen::isMute() const
{
    QQuickItem *mute = this->findChild<QQuickItem*>("muteButton");

    if (mute)
        return mute->isEnabled();

    return false;
}

bool RenderScreen::isValid() const
{
    return this->m_presenter.isValid();
}

bool RenderScreen::playNext()
{
    this->m_currentPlayingIndex++;
    return this->playCurrent();
}

bool RenderScreen::playPrev()
{
    this->m_currentPlayingIndex--;
    return this->playCurrent();
}

bool RenderScreen::playCurrent()
{
    bool success = false;

    this->adjustPlayIndex();

    if (this->m_playList->exist())
    {
        PlayItem item = this->m_playList->getPlayItem(this->m_currentPlayingIndex);

        if (item.path.startsWith(Utils::DTV_PROTOCOL))
        {
            item.path += Utils::FFMPEG_SEPARATOR;
            item.path += QString().setNum((qulonglong)&this->m_presenter.getDTVReader());
        }

        QVector<YouTubeURLPicker::Item> youtube;
        YouTubeURLPicker picker;
        QString path;

        youtube = picker.pickURL(item.path);

        if (youtube.isEmpty())
        {
            path = item.path;
        }
        else
        {
            int index = this->m_playList->updateYouTubeData(item.unique, youtube);

            path = youtube[index].url;
        }

        success = this->open(path, item.extraData, item.unique);
    }

    return success;
}

ShaderCompositer *RenderScreen::getShader() const
{
    return this->m_renderer.getShader();
}

FilterGraph& RenderScreen::getFilterGraph()
{
    return this->m_presenter.getFilterGraph();
}

bool RenderScreen::configFilterGraph()
{
    return this->m_presenter.configFilterGraph();
}

int RenderScreen::getCurrentYoutubeIndex() const
{
    const PlayItem &item = this->m_playList->getPlayItem(this->m_currentPlayingIndex);

    return item.youtubeIndex;
}

DTVReader& RenderScreen::getDTVReader()
{
    return this->m_presenter.getDTVReader();
}

QString RenderScreen::getLargeCoverFilePath() const
{
    QString path = QStandardPaths::writableLocation(QStandardPaths::TempLocation) + QDir::separator();

    return path + "anyvod." + COMPANY + "large_cover.png";
}

RenderScreen* RenderScreen::getSelf()
{
    RenderScreen* self;

    RenderScreen::m_selfLock.lock();
    self = RenderScreen::m_self;
    RenderScreen::m_selfLock.unlock();

    return self;
}

void RenderScreen::scheduleRebuildShader()
{
    this->m_renderer.scheduleRebuildShader();
}

void RenderScreen::checkGOMSubtitle()
{
    QString url;

    url = this->getGOMSubtitleURL();

    if (url.count() > 0)
        emit this->showSubtitlePopup();
}

void RenderScreen::callStartedEvent()
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    this->setScreenKeepOn(!this->isAudio() || this->existAudioSubtitle());
    this->setScreenOrientation(this->isRotatable() || this->existAudioSubtitle());
    this->setKeepActive(true);

    if (window)
    {
        window->setClearBeforeRendering(false);

        this->setUseCustomControl(this->isEnabledVideo() && !this->isAudio());
    }

#if !defined Q_OS_IOS
    this->setFullScreen();
#endif

    QString coverPath = this->getLargeCoverFilePath();
    QFile cover(coverPath);

    if (cover.exists())
        cover.remove();

    if (this->isAudio())
    {
        FrameExtractor extractor;
        FrameExtractor::FRAME_ITEM item;

        if (extractor.open(this->getFilePath()) && extractor.getFrame(0.0, true, &item))
        {
            if (!item.frame.scaledToWidth(512).save(coverPath, Q_NULLPTR, 100))
            {
                QFile cover(coverPath);

                if (cover.exists())
                    cover.remove();
            }

            extractor.close();
        }
    }

#if defined Q_OS_IOS
    if (this->isAudio())
    {
        IOSDelegate::registerMediaCenterCommand();
        IOSDelegate::updateNowPlayingInfo();
    }
#elif defined Q_OS_ANDROID
    this->callActivityEvent("started");
#endif

    this->m_playList->setCurrentIndex(this->getCurrentPlayingIndex());

    emit this->started();
}

void RenderScreen::callPlayingEvent()
{
#if defined Q_OS_ANDROID
    this->callActivityEvent("playing");
#endif

    emit this->resumed();
}

void RenderScreen::callPausedEvent()
{
#if defined Q_OS_ANDROID
    this->callActivityEvent("paused");
#endif

    emit this->paused();
}

void RenderScreen::callStoppedEvent()
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    if (window)
    {
        this->setUseCustomControl(false);

        window->setAcceptEvent(true);
#ifdef Q_OS_IOS
        window->setVisibility(QQuickWindow::AutomaticVisibility);
#endif
    }

    this->setKeepActive(false);

    QFile cover(this->getLargeCoverFilePath());

    if (cover.exists())
        cover.remove();

#if defined Q_OS_IOS
    if (this->isAudio())
    {
        IOSDelegate::updateNowPlayingInfo();
        IOSDelegate::unRegisterMediaCenterCommand();
    }
#elif defined Q_OS_ANDROID
    this->callActivityEvent("stopped");
#endif

    this->saveSettings();

    emit this->stopped();
}

void RenderScreen::callEndEvent()
{
    this->m_lastPlay.clear(Utils::removeFFMpegSeparator(this->m_lastPlayPath));

    emit this->ended();

    switch (this->m_playingMethod)
    {
        case AnyVODEnums::PM_TOTAL:
        {
            if (this->m_currentPlayingIndex + 1 < this->getPlayItemCount())
                this->playNext();
            else
                this->closeAndGoBack();

            break;
        }
        case AnyVODEnums::PM_TOTAL_REPEAT:
        {
            if (this->m_currentPlayingIndex + 1 >= this->getPlayItemCount())
                this->m_currentPlayingIndex = -1;

            this->playNext();

            break;
        }
        case AnyVODEnums::PM_SINGLE:
        {
            this->closeAndGoBack();
            break;
        }
        case AnyVODEnums::PM_SINGLE_REPEAT:
        {
            this->playCurrent();
            break;
        }
        case AnyVODEnums::PM_RANDOM:
        {
            this->m_currentPlayingIndex = rand() % this->getPlayItemCount();
            this->playCurrent();

            break;
        }
        default:
        {
            break;
        }
    }
}

void RenderScreen::callExitEvent()
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    if (window)
    {
        window->setClearBeforeRendering(true);
#ifndef Q_OS_IOS
        window->setVisibility(QQuickWindow::AutomaticVisibility);
#endif
    }

    this->m_playList->stop();
    this->setScreenOrientationRestore();

#if defined Q_OS_ANDROID
    this->callActivityEvent("exit");
#endif

    emit this->exit();
}

void RenderScreen::loadSettings()
{
    uint8_t volume = this->m_settings.value(SETTING_VOLUME, this->m_presenter.getVolume()).toInt();
    this->m_presenter.volume(volume);

    bool showSubtitle = this->m_settings.value(SETTING_SHOW_SUBTITLE, this->m_presenter.isShowSubtitle()).toBool();
    this->m_presenter.showSubtitle(showSubtitle);

    bool useNormalizer = this->m_settings.value(SETTING_USE_NORMALIZER, this->m_presenter.isUsingNormalizer()).toBool();
    this->m_presenter.useNormalizer(useNormalizer);

    AnyVODEnums::PlayingMethod method = (AnyVODEnums::PlayingMethod)this->m_settings.value(SETTING_MAINWINDOW_PLAYING_METHOD, (int)this->getPlayingMethod()).toInt();
    this->setPlayingMethod(method);

    AnyVODEnums::DeinterlaceMethod deMethod = (AnyVODEnums::DeinterlaceMethod)this->m_settings.value(SETTING_DEINTERLACER_METHOD, this->m_presenter.getDeinterlacer().getMethod()).toInt();
    this->m_presenter.getDeinterlacer().setMethod(deMethod);

    AnyVODEnums::DeinterlaceAlgorithm deAlgorithm = (AnyVODEnums::DeinterlaceAlgorithm)this->m_settings.value(SETTING_DEINTERLACER_ALGORITHM, this->m_presenter.getDeinterlacer().getAlgorithm()).toInt();
    this->m_presenter.setDeinterlacerAlgorithm(deAlgorithm);

    AnyVODEnums::HAlignMethod halign = (AnyVODEnums::HAlignMethod)this->m_settings.value(SETTING_HALIGN_SUBTITLE, this->m_presenter.getHAlign()).toInt();
    this->m_presenter.setHAlign(halign);

    bool seekKeyFrame = this->m_settings.value(SETTING_SEEK_KEYFRAME, this->m_presenter.isSeekKeyFrame()).toBool();
    this->m_presenter.setSeekKeyFrame(seekKeyFrame);

    GLRenderer::CaptureInfo captureInfo;
    captureInfo.savePath = this->m_settings.value(SETTING_SCREEN_CAPTURE_DIR, captureInfo.savePath).toString();
    captureInfo.ext = this->m_settings.value(SETTING_SCREEN_CAPTURE_EXT, captureInfo.ext).toString();

    QDir capDir(captureInfo.savePath);

    if (!capDir.exists())
    {
        GLRenderer::CaptureInfo tmp;

        captureInfo.savePath = tmp.savePath;
    }

    this->m_renderer.setCaptureInfo(captureInfo);

    bool searchSubtitle = this->m_settings.value(SETTING_ENABLE_SEARCH_SUBTITLE, this->m_presenter.isEnableSearchSubtitle()).toBool();
    this->m_presenter.enableSearchSubtitle(searchSubtitle);

    bool searchLyrics = this->m_settings.value(SETTING_ENABLE_SEARCH_LYRICS, this->m_presenter.isEnableSearchLyrics()).toBool();
    this->m_presenter.enableSearchLyrics(searchLyrics);

    AnyVODEnums::VAlignMethod valign = (AnyVODEnums::VAlignMethod)this->m_settings.value(SETTING_VALIGN_SUBTITLE, this->m_presenter.getVAlign()).toInt();
    this->m_presenter.setVAlign(valign);

    float subtitleSize = (float)this->m_settings.value(SETTING_SUBTITLE_SIZE, this->m_presenter.getSubtitleSize()).toFloat();
    this->m_presenter.setSubtitleSize(subtitleSize);

    UserAspectRatioItem &item = this->m_userAspectRatioList.list.last();
    QSizeF aspectSize(item.width, item.height);
    QSizeF aspectRatio = this->m_settings.value(SETTING_USER_ASPECT_RATIO, aspectSize).toSizeF();
    item.width = aspectRatio.width();
    item.height = aspectRatio.height();

    bool useHWDecoder = this->m_settings.value(SETTING_USE_HW_DECODER, this->m_presenter.isUseHWDecoder()).toBool();
    this->m_presenter.useHWDecoder(useHWDecoder);

    bool showAlbumJacket = this->m_settings.value(SETTING_SHOW_ALBUM_JACKET, this->m_presenter.isShowAlbumJacket()).toBool();
    this->m_presenter.showAlbumJacket(showAlbumJacket);

    bool enableLastPlay = this->m_settings.value(SETTING_ENABLE_LAST_PLAY, this->isGotoLastPos()).toBool();
    this->enableGotoLastPos(enableLastPlay);

    Utils::setSubtitleCodecName(this->m_settings.value(SETTING_TEXT_ENCODING, QTextCodec::codecForLocale()->name()).toString());

    bool useFrameDrop = this->m_settings.value(SETTING_USE_FRAME_DROP, this->m_presenter.isUseFrameDrop()).toBool();
    this->m_presenter.useFrameDrop(useFrameDrop);

    QStringList subtitleDirectory;
    bool priorSubtitleDirectory;

    this->getSubtitleDirectory(&subtitleDirectory, &priorSubtitleDirectory);

    subtitleDirectory = this->m_settings.value(SETTING_SUBTITLE_DIRECTORY, subtitleDirectory).toStringList();
    priorSubtitleDirectory = this->m_settings.value(SETTING_PRIOR_SUBTITLE_DIRECTORY, priorSubtitleDirectory).toBool();

    this->setSubtitleDirectory(subtitleDirectory, priorSubtitleDirectory);

    bool searchSubtitleComplex = this->m_settings.value(SETTING_SEARCH_SUBTITLE_COMPLEX, this->getSearchSubtitleComplex()).toBool();
    this->setSearchSubtitleComplex(searchSubtitleComplex);

    bool useBufferingMode = this->m_settings.value(SETTING_USE_BUFFERING_MODE, this->m_presenter.isUseBufferingMode()).toBool();
    this->m_presenter.useBufferingMode(useBufferingMode);

    bool useSubtitleCacheMode = this->m_settings.value(SETTING_USE_SUBTITLE_CACHE_MODE, this->m_presenter.isUseSubtitleCacheMode()).toBool();
    this->m_presenter.useSubtitleCacheMode(useSubtitleCacheMode);

    bool useLowQualityMode = this->m_settings.value(SETTING_USE_LOW_QUALITY_MODE, this->m_presenter.isUseLowQualityMode()).toBool();
    this->m_presenter.useLowQualityMode(useLowQualityMode);

    int audioDevice = this->m_settings.value(SETTING_AUDIO_DEVICE, this->m_presenter.getCurrentAudioDevice()).toInt();
    this->m_presenter.setAudioDevice(audioDevice);

    int spdifDevice = this->m_settings.value(SETTING_SPDIF_DEVICE, this->m_presenter.getCurrentSPDIFAudioDevice()).toInt();
    QStringList tmpList;
    this->m_presenter.getSPDIFAudioDevices(&tmpList);
    this->m_presenter.setSPDIFAudioDevice(spdifDevice);

    AnyVODEnums::SPDIFEncodingMethod spdifEncodingMethod = (AnyVODEnums::SPDIFEncodingMethod)this->m_settings.value(SETTING_SPDIF_ENCODING, this->m_presenter.getSPDIFEncodingMethod()).toInt();
    this->m_presenter.setSPDIFEncodingMethod(spdifEncodingMethod);

    int spdifSampleRate = this->m_settings.value(SETTING_SPDIF_SAMPLE_RATE, this->m_presenter.getUserSPDIFSampleRate()).toInt();
    this->m_presenter.setUserSPDIFSampleRate(spdifSampleRate);

    bool useSPDIF = this->m_settings.value(SETTING_USE_SPDIF, this->m_presenter.isUseSPDIF()).toBool();
    this->m_presenter.useSPDIF(useSPDIF);

    bool useBarrelDistortion = this->m_settings.value(SETTING_VR_USE_BARREL_DISTORTION, this->m_presenter.isUseBarrelDistortion()).toBool();
    this->m_presenter.useBarrelDistortion(useBarrelDistortion);

    QVector2D coefficients(this->m_settings.value(SETTING_VR_BARREL_DISTORTION_COEFFICIENTS, this->m_presenter.getBarrelDistortionCoefficients().toPointF()).toPointF());
    this->m_presenter.setBarrelDistortionCoefficients(coefficients);

    QVector2D lensCenter(this->m_settings.value(SETTING_VR_BARREL_DISTORTION_LENS_CENTER, this->m_presenter.getBarrelDistortionLensCenter().toPointF()).toPointF());
    this->m_presenter.setBarrelDistortionLensCenter(lensCenter);

    this->loadEqualizer();
    this->loadSkipRange();
    this->loadDTVScannedChannels();
}

void RenderScreen::saveSettings()
{
    this->m_settings.setValue(SETTING_FIRST_LOADING, false);

    this->m_settings.setValue(SETTING_VOLUME, this->m_presenter.getVolume());
    this->m_settings.setValue(SETTING_SHOW_SUBTITLE, this->m_presenter.isShowSubtitle());
    this->m_settings.setValue(SETTING_USE_NORMALIZER, this->m_presenter.isUsingNormalizer());
    this->m_settings.setValue(SETTING_MAINWINDOW_PLAYING_METHOD, (int)this->getPlayingMethod());
    this->m_settings.setValue(SETTING_DEINTERLACER_METHOD, (int)this->m_presenter.getDeinterlacer().getMethod());
    this->m_settings.setValue(SETTING_DEINTERLACER_ALGORITHM, (int)this->m_presenter.getDeinterlacer().getAlgorithm());
    this->m_settings.setValue(SETTING_HALIGN_SUBTITLE, (int)this->m_presenter.getHAlign());
    this->m_settings.setValue(SETTING_SEEK_KEYFRAME, this->m_presenter.isSeekKeyFrame());

    this->m_settings.setValue(SETTING_SCREEN_CAPTURE_DIR, this->m_renderer.getCaptureInfo().savePath);
    this->m_settings.setValue(SETTING_SCREEN_CAPTURE_EXT, this->m_renderer.getCaptureInfo().ext);

    this->m_settings.setValue(SETTING_ENABLE_SEARCH_SUBTITLE, this->m_presenter.isEnableSearchSubtitle());
    this->m_settings.setValue(SETTING_ENABLE_SEARCH_LYRICS, this->m_presenter.isEnableSearchLyrics());

    this->m_settings.setValue(SETTING_VALIGN_SUBTITLE, (int)this->m_presenter.getVAlign());
    this->m_settings.setValue(SETTING_SUBTITLE_SIZE, this->m_presenter.getSubtitleSize());

    UserAspectRatioItem &item = this->m_userAspectRatioList.list.last();
    QSizeF aspectSize(item.width, item.height);
    this->m_settings.setValue(SETTING_USER_ASPECT_RATIO, aspectSize);

    this->m_settings.setValue(SETTING_USE_HW_DECODER, this->m_presenter.isUseHWDecoder());
    this->m_settings.setValue(SETTING_SHOW_ALBUM_JACKET, this->m_presenter.isShowAlbumJacket());
    this->m_settings.setValue(SETTING_ENABLE_LAST_PLAY, this->isGotoLastPos());
    this->m_settings.setValue(SETTING_TEXT_ENCODING, Utils::getSubtitleCodecName());
    this->m_settings.setValue(SETTING_USE_FRAME_DROP, this->m_presenter.isUseFrameDrop());

    QStringList subtitleDirectory;
    bool priorSubtitleDirectory;

    this->getSubtitleDirectory(&subtitleDirectory, &priorSubtitleDirectory);
    this->m_settings.setValue(SETTING_SUBTITLE_DIRECTORY, subtitleDirectory);
    this->m_settings.setValue(SETTING_PRIOR_SUBTITLE_DIRECTORY, priorSubtitleDirectory);

    this->m_settings.setValue(SETTING_SEARCH_SUBTITLE_COMPLEX, this->getSearchSubtitleComplex());
    this->m_settings.setValue(SETTING_USE_BUFFERING_MODE, this->m_presenter.isUseBufferingMode());
    this->m_settings.setValue(SETTING_USE_SUBTITLE_CACHE_MODE, this->m_presenter.isUseSubtitleCacheMode());
    this->m_settings.setValue(SETTING_USE_LOW_QUALITY_MODE, this->m_presenter.isUseLowQualityMode());
    this->m_settings.setValue(SETTING_AUDIO_DEVICE, this->m_presenter.getCurrentAudioDevice());

    this->m_settings.setValue(SETTING_SPDIF_DEVICE, this->m_presenter.getCurrentSPDIFAudioDevice());
    this->m_settings.setValue(SETTING_SPDIF_ENCODING, this->m_presenter.getSPDIFEncodingMethod());
    this->m_settings.setValue(SETTING_SPDIF_SAMPLE_RATE, this->m_presenter.getUserSPDIFSampleRate());
    this->m_settings.setValue(SETTING_USE_SPDIF, this->m_presenter.isUseSPDIF());

    this->m_settings.setValue(SETTING_VR_USE_BARREL_DISTORTION, this->m_presenter.isUseBarrelDistortion());
    this->m_settings.setValue(SETTING_VR_BARREL_DISTORTION_COEFFICIENTS, this->m_presenter.getBarrelDistortionCoefficients().toPointF());
    this->m_settings.setValue(SETTING_VR_BARREL_DISTORTION_LENS_CENTER, this->m_presenter.getBarrelDistortionLensCenter().toPointF());

    this->saveEqualizer();
    this->saveSkipRange();

    if (!this->m_playList->getName().isEmpty())
        this->m_playList->updateCurrentPlayList();

    this->m_settings.sync();
}

int RenderScreen::getOptionDescY() const
{
    return this->m_presenter.getOptionDescY();
}

void RenderScreen::setOptionDescY(int y)
{
    this->m_presenter.setOptionDescY(y);
}

void RenderScreen::setUseCustomControl(bool use)
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    window->setUseCustomControl(use);
}

bool RenderScreen::getUseCustomControl() const
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    return window->getUseCustomControl();
}

QString RenderScreen::getTimeString(double value) const
{
    QString time;

    Utils::getTimeString(value, Utils::TIME_HH_MM_SS, &time);
    return time;
}

void RenderScreen::setSubtitleSync(double sync)
{
    QString desc;

    this->m_presenter.setSubtitleSync(sync);

    if (this->isMovieSubtitleVisiable())
    {
        if (sync > 0.0)
            desc = trUtf8("자막 싱크 %1초 빠르게");
        else
            desc = trUtf8("자막 싱크 %1초 느리게");
    }
    else
    {
        if (sync > 0.0)
            desc = trUtf8("가사 싱크 %1초 빠르게");
        else
            desc = trUtf8("가사 싱크 %1초 느리게");
    }

    desc = desc.arg(fabs(sync));

    this->m_presenter.showOptionDesc(desc);
}

double RenderScreen::getSubtitleSync() const
{
    return this->m_presenter.getSubtitleSync();
}

void RenderScreen::setAudioSync(double sync)
{
    QString desc;

    this->m_presenter.audioSync(sync);

    if (sync > 0.0)
        desc = trUtf8("소리 싱크 %1초 빠르게");
    else
        desc = trUtf8("소리 싱크 %1초 느리게");

    desc = desc.arg(fabs(sync));

    this->m_presenter.showOptionDesc(desc);
}

double RenderScreen::getAudioSync() const
{
    return this->m_presenter.getAudioSync();
}

void RenderScreen::useSubtitleCacheMode(bool use)
{
    QString desc;

    this->m_presenter.useSubtitleCacheMode(use);

    if (use)
        desc = trUtf8("자막 캐시 모드 사용");
    else
        desc = trUtf8("자막 캐시 모드 사용 안 함");

    this->m_presenter.showOptionDesc(desc);
}

bool RenderScreen::isUseSubtitleCacheMode() const
{
    return this->m_presenter.isUseSubtitleCacheMode();
}

int RenderScreen::getPlayItemCount() const
{
    return this->m_playList->getCount();
}

void RenderScreen::loadEqualizer()
{
    bool useEqualizer = this->m_settings.value(SETTING_USE_EQUALIZER, this->m_presenter.isUsingEqualizer()).toBool();
    this->m_presenter.useEqualizer(useEqualizer);

    float preamp = this->m_settings.value(SETTING_EQUALIZER_PREAMP, this->m_presenter.getPreAmp()).toFloat();
    this->m_presenter.setPreAmp(preamp);

    QVariantList list = this->m_settings.value(SETTING_EQUALIZER_GAINS, QVariantList()).toList();

    for (int i = 0; i < list.count(); i++)
        this->m_presenter.setEqualizerGain(i, list[i].value<EqualizerItem>().gain);
}

void RenderScreen::saveEqualizer()
{
    this->m_settings.setValue(SETTING_USE_EQUALIZER, this->m_presenter.isUsingEqualizer());
    this->m_settings.setValue(SETTING_EQUALIZER_PREAMP, this->m_presenter.getPreAmp());

    QVariantList list;
    QVector<EqualizerItem> eqGains;

    for (int i = 0; i < this->m_presenter.getBandCount(); i++)
    {
        EqualizerItem item;

        item.gain = this->m_presenter.getEqualizerGain(i);

        eqGains.append(item);
    }

    for (int i = 0; i < eqGains.count(); i++)
        list.append(qVariantFromValue(eqGains[i]));

    this->m_settings.setValue(SETTING_EQUALIZER_GAINS, list);
}

void RenderScreen::loadSkipRange()
{
    bool skipOpening = this->m_settings.value(SETTING_SKIP_OPENING, this->m_presenter.getSkipOpening()).toBool();
    this->m_presenter.setSkipOpening(skipOpening);

    bool skipEnding = this->m_settings.value(SETTING_SKIP_ENDING, this->m_presenter.getSkipEnding()).toBool();
    this->m_presenter.setSkipEnding(skipEnding);

    bool useSkipRange = this->m_settings.value(SETTING_SKIP_USING, this->m_presenter.getUseSkipRange()).toBool();
    this->m_presenter.setUseSkipRange(useSkipRange);

    QVariantList list = this->m_settings.value(SETTING_SKIP_RANGE, QVariantList()).toList();
    QVector<MediaPresenter::Range> ranges;

    for (int i = 0; i < list.count(); i++)
        ranges.append(list[i].value<MediaPresenter::Range>());

    this->m_presenter.setSkipRanges(ranges);
}

void RenderScreen::saveSkipRange()
{
    this->m_settings.setValue(SETTING_SKIP_OPENING, this->m_presenter.getSkipOpening());
    this->m_settings.setValue(SETTING_SKIP_ENDING, this->m_presenter.getSkipEnding());
    this->m_settings.setValue(SETTING_SKIP_USING, this->m_presenter.getUseSkipRange());

    QVariantList list;
    QVector<MediaPresenter::Range> ranges;

    this->m_presenter.getSkipRanges(&ranges);

    for (int i = 0; i < ranges.count(); i++)
        list.append(qVariantFromValue(ranges[i]));

    this->m_settings.setValue(SETTING_SKIP_RANGE, list);
}

void RenderScreen::loadDTVScannedChannels()
{
    QVariantList list = this->m_settings.value(SETTING_DTV_SCANNED_CHANNELS, QVariantList()).toList();
    QVector<DTVReader::ChannelInfo> channels;

    for (int i = 0; i < list.count(); i++)
        channels.append(list[i].value<DTVReader::ChannelInfo>());

    this->m_presenter.getDTVReader().setScannedChannels(channels);

    QLocale::Country country = (QLocale::Country)this->m_settings.value(SETTING_DTV_CURRENT_COUNTRY, QLocale::SouthKorea).toInt();
    DTVReaderInterface::SystemType type = (DTVReaderInterface::SystemType)this->m_settings.value(SETTING_DTV_CURRENT_TYPE, DTVReaderInterface::ST_ATSC_T).toInt();

    this->m_presenter.getDTVReader().setChannelCountry(country, type);
}

bool RenderScreen::isDTVOpened() const
{
    const DTVReader &reader = this->m_presenter.getDTVReader();

    return reader.isOpened();
}

void RenderScreen::setup3DMethod()
{
    this->m_3DMethodDesc[AnyVODEnums::V3M_NONE] = trUtf8("사용 안 함");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_LEFT] = trUtf8("왼쪽 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_RIGHT] = trUtf8("오른쪽 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_TOP] = trUtf8("상단 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_HALF_BOTTOM] = trUtf8("하단 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_FULL_LEFT_RIGHT] = trUtf8("좌우 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_FULL_TOP_BOTTOM] = trUtf8("상하 영상 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("왼쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("오른쪽 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR] = trUtf8("상단 영상 우선 사용");
    this->m_3DMethodDesc[AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("하단 영상 우선 사용");

    this->m_3DMethodCategory[AnyVODEnums::V3M_NONE] = trUtf8("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_HALF_LEFT] = trUtf8("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_HALF_RIGHT] = trUtf8("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_HALF_TOP] = trUtf8("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_HALF_BOTTOM] = trUtf8("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_FULL_LEFT_RIGHT] = trUtf8("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_FULL_TOP_BOTTOM] = trUtf8("일반");
    this->m_3DMethodCategory[AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("Page Flipping");
    this->m_3DMethodCategory[AnyVODEnums::V3M_PAGE_FLIP_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("Page Flipping");
    this->m_3DMethodCategory[AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_TOP_PRIOR] = trUtf8("Page Flipping");
    this->m_3DMethodCategory[AnyVODEnums::V3M_PAGE_FLIP_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("Page Flipping");
    this->m_3DMethodCategory[AnyVODEnums::V3M_ROW_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("Row Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_ROW_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("Row Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_ROW_TOP_BOTTOM_TOP_PRIOR] = trUtf8("Row Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_ROW_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("Row Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_COL_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("Column Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_COL_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("Column Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_COL_TOP_BOTTOM_TOP_PRIOR] = trUtf8("Column Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_COL_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("Column Interlaced");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("Red-Cyan Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_CYAN_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("Red-Cyan Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_TOP_PRIOR] = trUtf8("Red-Cyan Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_CYAN_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("Red-Cyan Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("Green-Magenta Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_GREEN_MAGENTA_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("Green-Magenta Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_TOP_PRIOR] = trUtf8("Green-Magenta Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_GREEN_MAGENTA_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("Green-Magenta Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("Yellow-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_YELLOW_BLUE_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("Yellow-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_TOP_PRIOR] = trUtf8("Yellow-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_YELLOW_BLUE_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("Yellow-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("Red-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_BLUE_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("Red-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_TOP_PRIOR] = trUtf8("Red-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_BLUE_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("Red-Blue Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("Red-Green Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_GREEN_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("Red-Green Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_TOP_PRIOR] = trUtf8("Red-Green Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_RED_GREEN_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("Red-Green Anaglyph");
    this->m_3DMethodCategory[AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_LEFT_PRIOR] = trUtf8("Checker Board");
    this->m_3DMethodCategory[AnyVODEnums::V3M_CHECKER_BOARD_LEFT_RIGHT_RIGHT_PRIOR] = trUtf8("Checker Board");
    this->m_3DMethodCategory[AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_TOP_PRIOR] = trUtf8("Checker Board");
    this->m_3DMethodCategory[AnyVODEnums::V3M_CHECKER_BOARD_TOP_BOTTOM_BOTTOM_PRIOR] = trUtf8("Checker Board");
}

void RenderScreen::setupSubtitle3DMethod()
{
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_NONE] = trUtf8("사용 안 함");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_TOP_BOTTOM] = trUtf8("상/하");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_LEFT_RIGHT] = trUtf8("좌/우");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_PAGE_FLIP] = trUtf8("페이지 플리핑");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_INTERLACED] = trUtf8("인터레이스");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_ANAGLYPH] = trUtf8("애너글리프");
    this->m_subtitle3DMethodDesc[AnyVODEnums::S3M_CHECKER_BOARD] = trUtf8("체커 보드");
}

void RenderScreen::setupAnaglyphAlgorithm()
{
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_GRAY] = "Gray";
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_COLORED] = "Colored";
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_HALF_COLORED] = "Half Colored";
    this->m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_DUBOIS] = "Dubois";
}

void RenderScreen::setupDeinterlaceMethod()
{
    this->m_deinterlaceDesc[AnyVODEnums::DA_BLEND] = "Blend";
    this->m_deinterlaceDesc[AnyVODEnums::DA_BOB] = "BOB";
    this->m_deinterlaceDesc[AnyVODEnums::DA_YADIF] = "YADIF";
    this->m_deinterlaceDesc[AnyVODEnums::DA_YADIF_BOB] = "YADIF(BOB)";
    this->m_deinterlaceDesc[AnyVODEnums::DA_W3FDIF] = "Weston 3 Field";
    this->m_deinterlaceDesc[AnyVODEnums::DA_KERNDEINT] = "Adaptive Kernel";
    this->m_deinterlaceDesc[AnyVODEnums::DA_MCDEINT] = "Motion Compensation";
    this->m_deinterlaceDesc[AnyVODEnums::DA_BWDIF] = "Bob Weaver";
    this->m_deinterlaceDesc[AnyVODEnums::DA_BWDIF_BOB] = "Bob Weaver(BOB)";
}

void RenderScreen::setupVRInputSource()
{
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_NONE] = trUtf8("사용 안 함");
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_LEFT_RIGHT] = trUtf8("좌우 영상");
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_TOP_BOTTOM] = trUtf8("상하 영상");
    this->m_vrInputSourceDesc[AnyVODEnums::VRI_COPY] = trUtf8("복제");
}

void RenderScreen::resetLyrics()
{
    this->m_lyrics1->setProperty("color", QColor(Qt::black));
    this->m_lyrics2->setProperty("color", QColor(Qt::black));
    this->m_lyrics3->setProperty("color", QColor(Qt::black));

    this->m_lyrics1->setProperty("text", QString());
    this->m_lyrics2->setProperty("text", QString());
    this->m_lyrics3->setProperty("text", QString());
}

void RenderScreen::exitAtCppSide()
{
    QQuickWindow *window = this->window();

    if (window)
        window->setColor(Qt::white);
}

void RenderScreen::setFullScreen()
{
    AnyVODWindow *window = (AnyVODWindow*)this->window();

    if (!window)
        return;

    if (this->isAudio())
        window->setVisibility(QQuickWindow::AutomaticVisibility);
    else
        window->setVisibility(QQuickWindow::FullScreen);
}

QStringList RenderScreen::getSPDIFAudioDevices()
{
    QStringList ret;

    this->m_presenter.getSPDIFAudioDevices(&ret);

    return ret;
}

bool RenderScreen::setSPDIFAudioDevice(int device)
{
    QStringList list;
    QString name;

    this->m_presenter.getSPDIFAudioDevices(&list);

    if (device == -1)
    {
        name = trUtf8("기본 장치");
    }
    else
    {
        for (int i = 0; i < list.count(); i++)
        {
            if (i == device)
            {
                name = list[i];
                break;
            }
        }
    }

    bool useSPDIF = this->isUseSPDIF();
    bool success = this->m_presenter.setSPDIFAudioDevice(device);
    QString desc;

    if (this->isUseSPDIF() == useSPDIF)
        desc = trUtf8("S/PDIF 소리 출력 장치 변경 (%1)").arg(name);
    else
        desc = trUtf8("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");

    this->m_presenter.showOptionDesc(desc);

    emit this->spdifEnabled();

    return success;
}

int RenderScreen::getCurrentSPDIFAudioDevice() const
{
    return this->m_presenter.getCurrentSPDIFAudioDevice();
}

void RenderScreen::useSPDIF(bool enable)
{
    this->m_presenter.useSPDIF(enable);

    QString desc;

    if (this->isUseSPDIF() == enable)
    {
        if (enable)
            desc = trUtf8("S/PDIF 출력 사용");
        else
            desc = trUtf8("S/PDIF 출력 사용 안 함");
    }
    else
    {
        desc = trUtf8("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");
    }

    this->m_presenter.showOptionDesc(desc);

    emit this->spdifEnabled();
}

bool RenderScreen::isUseSPDIF() const
{
    return this->m_presenter.isUseSPDIF();
}

bool RenderScreen::isOpenedSPDIF() const
{
    return this->m_presenter.isOpenedSPDIF();
}

bool RenderScreen::isSPDIFAvailable() const
{
    return this->m_presenter.isSPDIFAvailable();
}

void RenderScreen::setSPDIFEncodingMethod(int method)
{
    QString desc;

    this->m_presenter.setSPDIFEncodingMethod((AnyVODEnums::SPDIFEncodingMethod)method);

    if (this->getSPDIFEncodingMethod() == method)
    {
        switch (method)
        {
            case AnyVODEnums::SEM_NONE:
                desc = trUtf8("S/PDIF 출력 시 인코딩 사용 안 함");
                break;
            case AnyVODEnums::SEM_AC3:
                desc = trUtf8("S/PDIF 출력 시 AC3 인코딩 사용");
                break;
            case AnyVODEnums::SEM_DTS:
                desc = trUtf8("S/PDIF 출력 시 DTS 인코딩 사용");
                break;
            default:
                break;
        }
    }
    else
    {
        desc = trUtf8("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");
    }

    this->m_presenter.showOptionDesc(desc);

    emit this->spdifEnabled();
}

int RenderScreen::getSPDIFEncodingMethod() const
{
    return this->m_presenter.getSPDIFEncodingMethod();
}

void RenderScreen::setUserSPDIFSampleRate(int sampleRate)
{
    bool useSPDIF = this->isUseSPDIF();
    QString desc;

    this->m_presenter.setUserSPDIFSampleRate(sampleRate);

    if (this->isUseSPDIF() == useSPDIF)
    {
        desc = trUtf8("S/PDIF 샘플 속도 (%1)");

        if (sampleRate == 0)
            desc = desc.arg(trUtf8("기본 속도"));
        else
            desc = desc.arg(QString("%1kHz").arg(sampleRate / 1000.0f, 0, 'f', 1));
    }
    else
    {
        desc = trUtf8("S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.");
    }

    this->m_presenter.showOptionDesc(desc);

    emit this->spdifEnabled();
}

int RenderScreen::getUserSPDIFSampleRate() const
{
    return this->m_presenter.getUserSPDIFSampleRate();
}

void RenderScreen::userSPDIFSampleRateOrder()
{
    int index = this->m_userSPDIFSampleRateList.curIndex + 1;

    if (index >= this->m_userSPDIFSampleRateList.list.count())
        index = 0;

    this->selectUserSPDIFSampleRate(index);
}

void RenderScreen::selectUserSPDIFSampleRate(int index)
{
    this->m_userSPDIFSampleRateList.curIndex = index;

    this->setUserSPDIFSampleRate(this->m_userSPDIFSampleRateList.list[index]);
}

int RenderScreen::getCurrentUserSPDIFSampleRate() const
{
    return this->m_userSPDIFSampleRateList.curIndex;
}

int RenderScreen::getUserSPDIFSampleRateCount() const
{
    return this->m_userSPDIFSampleRateList.list.count();
}

QString RenderScreen::getUserSPDIFSampleRateDesc(int index) const
{
    int item = this->m_userSPDIFSampleRateList.list[index];
    QString desc;

    if (item == 0)
        desc = trUtf8("기본 속도");
    else
        desc = QString("%1kHz").arg(item / 1000.0f, 0, 'f', 1);

    return desc;
}

void RenderScreen::setVerticalScreenOffset(int offset)
{
    this->m_presenter.setVerticalScreenOffset(offset);
}

void RenderScreen::setHorizontalScreenOffset(int offset)
{
    this->m_presenter.setHorizontalScreenOffset(offset);
}

QStringList RenderScreen::getAudioDevices() const
{
    QStringList ret;

    this->m_presenter.getAudioDevices(&ret);
    return ret;
}

bool RenderScreen::setAudioDevice(int device)
{
    QStringList list;
    QString name;

    this->m_presenter.getAudioDevices(&list);

    if (device == -1)
    {
        name = trUtf8("기본 장치");
    }
    else
    {
        for (int i = 0; i < list.count(); i++)
        {
            if (i == device)
            {
                name = list[i];
                break;
            }
        }
    }

    bool success = this->m_presenter.setAudioDevice(device);

    this->m_presenter.showOptionDesc(trUtf8("소리 출력 장치 변경 (%1)").arg(name));

    return success;
}

int RenderScreen::getCurrentAudioDevice() const
{
    return this->m_presenter.getCurrentAudioDevice();
}

void RenderScreen::setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree)
{
    QString desc;

    switch (degree)
    {
        case AnyVODEnums::SRD_NONE:
            desc = trUtf8("사용 안 함");
            break;
        case AnyVODEnums::SRD_90:
            desc = trUtf8("90도");
            break;
        case AnyVODEnums::SRD_180:
            desc = trUtf8("180도");
            break;
        case AnyVODEnums::SRD_270:
            desc = trUtf8("270도");
            break;
        default:
            break;
    }

    this->setScheduleRecomputeSubtitleSize();

    this->m_presenter.setScreenRotationDegree(degree);
    this->m_presenter.showOptionDesc(trUtf8("화면 회전 각도 (%1)").arg(desc));
}

AnyVODEnums::ScreenRotationDegree RenderScreen::getScreenRotationDegree() const
{
    return this->m_presenter.getScreenRotationDegree();
}

int RenderScreen::getCurrentPlayingIndex() const
{
    return this->m_currentPlayingIndex;
}

void RenderScreen::resetCurrentPlayingIndex()
{
    this->m_currentPlayingIndex = -1;
}

void RenderScreen::setCurrentPlayingIndex(int index)
{
    this->m_currentPlayingIndex = index;
}

void RenderScreen::adjustPlayIndex()
{
    if (this->m_currentPlayingIndex >= this->getPlayItemCount())
        this->m_currentPlayingIndex = this->getPlayItemCount() - 1;
    else if (this->m_currentPlayingIndex < 0)
        this->m_currentPlayingIndex = 0;
    else if (this->getPlayItemCount() <= 0)
        this->resetCurrentPlayingIndex();
}

bool RenderScreen::playAt(int index)
{
    this->m_currentPlayingIndex = index;
    return this->playCurrent();
}

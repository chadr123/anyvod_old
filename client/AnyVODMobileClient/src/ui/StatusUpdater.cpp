﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "StatusUpdater.h"
#include "RenderScreen.h"

#ifdef Q_OS_MAC
#include <dispatch/dispatch.h>
#endif

StatusUpdater::StatusUpdater(RenderScreen *parent) :
    m_parent(parent),
    m_running(true)
{
    this->start();
}

StatusUpdater::~StatusUpdater()
{
    this->m_running = false;
    this->m_cond.wakeOne();

    if (this->isRunning())
        this->wait();
}

void StatusUpdater::putStatus(int status)
{
    this->m_lock.lock();

    this->m_status.enqueue(status);

    this->m_cond.wakeOne();
    this->m_lock.unlock();
}

void StatusUpdater::run()
{
    while (this->m_running)
    {
        this->m_lock.lock();
        this->m_cond.wait(&this->m_lock);
        this->m_lock.unlock();

        while (true)
        {
            this->m_lock.lock();

            if (this->m_status.isEmpty())
            {
                this->m_lock.unlock();
                break;
            }

            RenderScreen::Status status = (RenderScreen::Status)this->m_status.dequeue();

            this->m_lock.unlock();
#ifdef Q_OS_MAC
            dispatch_async(dispatch_get_main_queue(), ^{
#endif
            switch (status)
            {
                case RenderScreen::Started:
                {
                    this->m_parent->callStartedEvent();
                    break;
                }
                case RenderScreen::Playing:
                {
                    this->m_parent->callPlayingEvent();
                    break;
                }
                case RenderScreen::Paused:
                {
                    this->m_parent->callPausedEvent();
                    break;
                }
                case RenderScreen::Stopped:
                {
                    this->m_parent->callStoppedEvent();
                    break;
                }
                case RenderScreen::Ended:
                {
                    this->m_parent->callEndEvent();
                    break;
                }
                case RenderScreen::Exit:
                {
                    this->m_parent->callExitEvent();
                    break;
                }
                default:
                {
                    break;
                }
            }
#ifdef Q_OS_MAC
            });
#endif
        }
    }
}

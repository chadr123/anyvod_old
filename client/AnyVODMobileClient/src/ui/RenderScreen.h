﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "../video/GLRenderer.h"
#include "../models/PlayListModel.h"
#include "media/MediaPresenter.h"
#include "media/LastPlay.h"
#include "StatusUpdater.h"
#include "XDGScreenSaverDisabler.h"

#ifdef Q_OS_RASPBERRY_PI
#include <dbus/dbus.h>
#endif

#include <QQuickItem>
#include <QStandardPaths>
#include <QMutex>

class DTVReader;

class RenderScreen : public QQuickItem
{
    Q_OBJECT
public:
    static const QEvent::Type PLAYING_EVENT;
    static const QEvent::Type EMPTY_BUFFER_EVENT;
    static const QEvent::Type AUDIO_OPTION_DESC_EVENT;
    static const QEvent::Type AUDIO_SUBTITLE_EVENT;
    static const QEvent::Type ABORT_EVENT;

public:
    enum Status
    {
        Started = 0,
        Playing,
        Paused,
        Stopped,
        Ended,
        Exit
    };
    Q_ENUM(Status)

    class PlayingEvent : public QEvent
    {
    public:
        PlayingEvent() :
            QEvent(PLAYING_EVENT)
        {

        }
    };

    class EmptyBufferEvent : public QEvent
    {
    public:
        EmptyBufferEvent(bool empty) :
            QEvent(EMPTY_BUFFER_EVENT),
            m_empty(empty)
        {

        }

        bool isEmpty() const { return this->m_empty; }

    private:
        bool m_empty;
    };

    class ShowAudioOptionDescEvent : public QEvent
    {
    public:
        ShowAudioOptionDescEvent(const QString &desc, bool show) :
            QEvent(AUDIO_OPTION_DESC_EVENT),
            m_desc(desc),
            m_show(show)
        {

        }

        QString getDesc() const { return this->m_desc; }
        bool getShow() const { return this->m_show; }

    private:
        QString m_desc;
        bool m_show;
    };

    class AudioSubtitleEvent : public QEvent
    {
    public:
        AudioSubtitleEvent(const QVector<Lyrics> &lines) :
            QEvent(AUDIO_SUBTITLE_EVENT),
            m_lines(lines)
        {

        }

        QVector<Lyrics> getLines() const { return this->m_lines; }

    private:
        QVector<Lyrics> m_lines;
    };

    class AbortEvent : public QEvent
    {
    public:
        AbortEvent(int reason) :
            QEvent(ABORT_EVENT),
            m_reason(reason)
        {

        }

        int getReason() const { return this->m_reason; }

    private:
        int m_reason;
    };

    struct UserAspectRatioItem
    {
        UserAspectRatioItem()
        {
            width = 1.0;
            height = 1.0;
        }

        UserAspectRatioItem(double _width, double _height)
        {
            width = _width;
            height = _height;
        }

        bool isInvalid() const
        {
            return width == 0.0 || height == 0.0;
        }

        bool isFullScreen() const
        {
            return width == -1.0 || height == -1.0;
        }

        double width;
        double height;
    };

    struct UserAspectRatioList
    {
        UserAspectRatioList()
        {
            curIndex = 0;

            list.append(UserAspectRatioItem(0.0, 0.0));
            list.append(UserAspectRatioItem(-1.0, -1.0));
            list.append(UserAspectRatioItem(4.0, 3.0));
            list.append(UserAspectRatioItem(16.0, 9.0));
            list.append(UserAspectRatioItem(16.0, 10.0));
            list.append(UserAspectRatioItem(1.85, 1.0));
            list.append(UserAspectRatioItem(2.35, 1.0));
            list.append(UserAspectRatioItem(4.0, 3.0));
        }

        int curIndex;
        QVector<UserAspectRatioItem> list;
    };

    struct UserSPDIFSampleRateList
    {
        UserSPDIFSampleRateList()
        {
            curIndex = 0;

            list.append(0);
            list.append(44100);
            list.append(48000);
            list.append(88200);
            list.append(96000);
            list.append(176400);
            list.append(192000);
        }

        int curIndex;
        QVector<int> list;
    };

    struct EqualizerItem
    {
        EqualizerItem()
        {
            gain = 0.0f;
        }

        float gain;
    };

public:
    RenderScreen();
    ~RenderScreen();

    Q_INVOKABLE bool open(const QString &path);
    Q_INVOKABLE bool openExternal(const QString &path);
    Q_INVOKABLE bool play();
    Q_INVOKABLE void pause();
    Q_INVOKABLE void resume();
    Q_INVOKABLE void stop();
    Q_INVOKABLE void close();
    Q_INVOKABLE void closeAndGoBack();

    Q_INVOKABLE bool recover();
    Q_INVOKABLE void toggle();

    Q_INVOKABLE void prevFrame(int count);
    Q_INVOKABLE void nextFrame(int count);

    Q_INVOKABLE void seek(double time, bool any);
    Q_INVOKABLE void rewind(double distance);
    Q_INVOKABLE void forward(double distance);

    Q_INVOKABLE bool isOpened() const;

    Q_INVOKABLE QString getSubtitlePath() const;
    Q_INVOKABLE bool openSubtitle(const QString &filePath);
    Q_INVOKABLE bool saveSubtitleAs(const QString &filePath);
    Q_INVOKABLE bool saveSubtitle();
    Q_INVOKABLE void closeAllExternalSubtitles();

    Q_INVOKABLE QString getFileName() const;
    Q_INVOKABLE QString getTitle() const;
    Q_INVOKABLE QString getFilePath() const;

    Q_INVOKABLE const QVector<ChapterInfo>& getChapters() const;

    Q_INVOKABLE bool isPlayOrPause() const;
    Q_INVOKABLE bool isRemoteFile() const;

    Q_INVOKABLE bool canMoveChapter() const;
    Q_INVOKABLE bool canMoveFrame() const;

    Q_INVOKABLE bool isTempoUsable() const;
    Q_INVOKABLE float getTempo() const;
    Q_INVOKABLE void setTempo(float percent);

    Q_INVOKABLE void setUserAspectRatio(MediaPresenter::UserAspectRatio &ratio);
    Q_INVOKABLE MediaPresenter::UserAspectRatio getUserAspectRatio() const;

    Q_INVOKABLE QString getGOMSubtitleURL() const;
    Q_INVOKABLE void retreiveExternalSubtitle(const QString &filePath);

    Q_INVOKABLE int getCurrentAudioStreamIndex() const;
    Q_INVOKABLE QVector<AudioStreamInfo> getAudioStreamInfo() const;
    Q_INVOKABLE int getAudioStreamInfoCount() const;
    Q_INVOKABLE QString getAudioStreamDesc(int index) const;
    Q_INVOKABLE int getAudioStreamIndex(int index) const;
    Q_INVOKABLE bool changeAudioStream(const int index);
    Q_INVOKABLE HSTREAM getAudioHandle() const;

    Q_INVOKABLE void setDeinterlaceMethod(AnyVODEnums::DeinterlaceMethod method);
    Q_INVOKABLE void setDeinterlaceAlgorithm(AnyVODEnums::DeinterlaceAlgorithm algorithm);
    Q_INVOKABLE AnyVODEnums::DeinterlaceMethod getDeinterlaceMethod();
    Q_INVOKABLE AnyVODEnums::DeinterlaceAlgorithm getDeinterlaceAlgorithm();

    Q_INVOKABLE void setHAlign(AnyVODEnums::HAlignMethod align);
    Q_INVOKABLE AnyVODEnums::HAlignMethod getHAlign() const;
    Q_INVOKABLE void setVAlign(AnyVODEnums::VAlignMethod align);
    Q_INVOKABLE AnyVODEnums::VAlignMethod getVAlign() const;
    Q_INVOKABLE bool isAlignable();

    Q_INVOKABLE void showDetail(bool show);
    Q_INVOKABLE bool isShowDetail() const;
    const MediaPresenter::Detail& getDetail() const;

    Q_INVOKABLE QStringList getAudioDevices() const;
    Q_INVOKABLE bool setAudioDevice(int device);
    Q_INVOKABLE int getCurrentAudioDevice() const;

    Q_INVOKABLE void setScreenRotationDegree(AnyVODEnums::ScreenRotationDegree degree);
    Q_INVOKABLE AnyVODEnums::ScreenRotationDegree getScreenRotationDegree() const;

    Q_INVOKABLE void showSubtitle(bool show);
    Q_INVOKABLE bool isShowSubtitle() const;

    Q_INVOKABLE void setSearchSubtitleComplex(bool use);
    Q_INVOKABLE bool getSearchSubtitleComplex() const;

    Q_INVOKABLE bool existSubtitle();
    Q_INVOKABLE bool existFileSubtitle();
    Q_INVOKABLE bool existExternalSubtitle();
    Q_INVOKABLE bool existAudioSubtitle();
    Q_INVOKABLE bool existAudioSubtitleGender();

    Q_INVOKABLE QStringList getSubtitleClasses();
    Q_INVOKABLE QString getCurrentSubtitleClass();
    Q_INVOKABLE bool setCurrentSubtitleClass(const QString &className);
    Q_INVOKABLE int getSubtitleClassCount();
    Q_INVOKABLE QString getSubtitleClass(int index);

    Q_INVOKABLE void resetSubtitlePosition();
    Q_INVOKABLE void setVerticalSubtitlePosition(int pos);
    Q_INVOKABLE void setHorizontalSubtitlePosition(int pos);
    Q_INVOKABLE void setVerticalSubtitleAbsolutePosition(int pos);
    Q_INVOKABLE void setHorizontalSubtitleAbsolutePosition(int pos);
    Q_INVOKABLE int getVerticalSubtitlePosition();
    Q_INVOKABLE int getHorizontalSubtitlePosition();

    Q_INVOKABLE void reset3DSubtitleOffset();
    Q_INVOKABLE void setVertical3DSubtitleOffset(int offset);
    Q_INVOKABLE void setHorizontal3DSubtitleOffset(int offset);
    Q_INVOKABLE void setVertical3DSubtitleAbsoluteOffset(int offset);
    Q_INVOKABLE void setHorizontal3DSubtitleAbsoluteOffset(int offset);
    Q_INVOKABLE int getVertical3DSubtitleOffset();
    Q_INVOKABLE int getHorizontal3DSubtitleOffset();

    Q_INVOKABLE void setRepeatStart(double start);
    Q_INVOKABLE void setRepeatEnd(double end);
    Q_INVOKABLE void setRepeatEnable(bool enable);
    Q_INVOKABLE bool getRepeatEnable() const;
    Q_INVOKABLE double getRepeatStart() const;
    Q_INVOKABLE double getRepeatEnd() const;

    Q_INVOKABLE void setRepeatStartMove(double offset);
    Q_INVOKABLE void setRepeatEndMove(double offset);
    Q_INVOKABLE void setRepeatMove(double offset);

    Q_INVOKABLE void setSeekKeyFrame(bool keyFrame);
    Q_INVOKABLE bool isSeekKeyFrame() const;

    Q_INVOKABLE void setSubtitleDirectory(const QStringList &paths, bool prior);
    Q_INVOKABLE QStringList getSubtitleDirectoryPath() const;
    Q_INVOKABLE bool getSubtitleDirectoryPrior() const;
    void getSubtitleDirectory(QStringList *path, bool *prior) const;

    Q_INVOKABLE void set3DMethod(AnyVODEnums::Video3DMethod method);
    Q_INVOKABLE AnyVODEnums::Video3DMethod get3DMethod() const;

    Q_INVOKABLE void setSubtitle3DMethod(AnyVODEnums::Subtitle3DMethod method);
    Q_INVOKABLE AnyVODEnums::Subtitle3DMethod getSubtitle3DMethod() const;

    Q_INVOKABLE void setVRInputSource(AnyVODEnums::VRInputSource source);
    Q_INVOKABLE int getVRInputSource() const;

    Q_INVOKABLE void useHeadTracking(bool use);
    Q_INVOKABLE bool isUseHeadTracking() const;

    Q_INVOKABLE void useBarrelDistortion(bool use);
    Q_INVOKABLE bool isUseBarrelDistortion() const;

    Q_INVOKABLE void setBarrelDistortionCoefficients(const QVector2D &coefficients);
    Q_INVOKABLE QVector2D getBarrelDistortionCoefficients() const;

    Q_INVOKABLE void setBarrelDistortionLensCenter(const QVector2D &lensCenter);
    Q_INVOKABLE QVector2D getBarrelDistortionLensCenter() const;

    Q_INVOKABLE void setSkipRanges(const QVector<MediaPresenter::Range> &ranges);
    Q_INVOKABLE QVector<MediaPresenter::Range> getSkipRanges() const;

    Q_INVOKABLE void setSkipOpening(bool skip);
    Q_INVOKABLE bool getSkipOpening() const;
    Q_INVOKABLE void setSkipEnding(bool skip);
    Q_INVOKABLE bool getSkipEnding() const;
    Q_INVOKABLE void setUseSkipRange(bool use);
    Q_INVOKABLE bool getUseSkipRange() const;

    Q_INVOKABLE void useNormalizer(bool use);
    Q_INVOKABLE bool isUsingNormalizer() const;

    Q_INVOKABLE void useEqualizer(bool use);
    Q_INVOKABLE bool isUsingEqualizer() const;

    Q_INVOKABLE void useLowerVoice(bool use);
    Q_INVOKABLE bool isUsingLowerVoice() const;

    Q_INVOKABLE void useHigherVoice(bool use);
    Q_INVOKABLE bool isUsingHigherVoice() const;

    Q_INVOKABLE void useLowerMusic(bool use);
    Q_INVOKABLE bool isUsingLowerMusic() const;

    Q_INVOKABLE void addSubtitleOpaque(float inc);
    Q_INVOKABLE void setSubtitleOpaque(float opaque);
    Q_INVOKABLE float getSubtitleOpaque() const;
    Q_INVOKABLE void resetSubtitleOpaque();

    Q_INVOKABLE void addSubtitleSize(float inc);
    Q_INVOKABLE float getSubtitleSize() const;
    Q_INVOKABLE void setSubtitleSize(float size);
    Q_INVOKABLE void resetSubtitleSize();
    Q_INVOKABLE void setScheduleRecomputeSubtitleSize();
    Q_INVOKABLE void saveSubtitleSize(float size);
    Q_INVOKABLE void applySubtitleSize();
    Q_INVOKABLE float getSavedSubtitleSize() const;

    Q_INVOKABLE void useHWDecoder(bool enable);
    Q_INVOKABLE bool isUseHWDecoder() const;

    Q_INVOKABLE void useLowQualityMode(bool enable);
    Q_INVOKABLE bool isUseLowQualityMode() const;

    Q_INVOKABLE void useFrameDrop(bool enable);
    Q_INVOKABLE bool isUseFrameDrop() const;

    Q_INVOKABLE void useBufferingMode(bool enable);
    Q_INVOKABLE bool isUseBufferingMode() const;

    Q_INVOKABLE void use3DFull(bool enable);
    Q_INVOKABLE bool isUse3DFull() const;

    Q_INVOKABLE bool setPreAmp(float dB);
    Q_INVOKABLE float getPreAmp() const;

    Q_INVOKABLE bool setEqualizerGain(int band, float gain);
    Q_INVOKABLE float getEqualizerGain(int band) const;
    Q_INVOKABLE int getBandCount() const;

    Q_INVOKABLE void prevSubtitleSync(double amount);
    Q_INVOKABLE void nextSubtitleSync(double amount);
    Q_INVOKABLE void resetSubtitleSync();

    Q_INVOKABLE void prevAudioSync(double amount);
    Q_INVOKABLE void nextAudioSync(double amount);
    Q_INVOKABLE void resetAudioSync();

    Q_INVOKABLE bool isEnableSearchSubtitle() const;
    Q_INVOKABLE bool isEnableSearchLyrics() const;
    Q_INVOKABLE void enableSearchSubtitle(bool enable);
    Q_INVOKABLE void enableSearchLyrics(bool enable);

    Q_INVOKABLE void volume(int volume);
    Q_INVOKABLE void mute(bool mute);

    Q_INVOKABLE int getMaxVolume() const;
    Q_INVOKABLE int getVolume() const;

    Q_INVOKABLE double getDuration() const;
    Q_INVOKABLE double getCurrentPosition();
    Q_INVOKABLE bool hasDuration() const;

    Q_INVOKABLE double getAspectRatio(bool widthPrio) const;

    Q_INVOKABLE void setMaxTextureSize(int size);
    Q_INVOKABLE int getMaxTextureSize() const;

    Q_INVOKABLE Status getStatus();

    Q_INVOKABLE bool isEnabledVideo() const;
    Q_INVOKABLE bool isAudio() const;
    Q_INVOKABLE bool isVideo() const;
    Q_INVOKABLE bool isSubtitleMoveable();
    Q_INVOKABLE bool is3DSubtitleMoveable();
    Q_INVOKABLE bool isMovieSubtitleVisiable() const;

    Q_INVOKABLE void showAlbumJacket(bool show);
    Q_INVOKABLE bool isShowAlbumJacket() const;

    Q_INVOKABLE QSize getFrameSize() const;
    Q_INVOKABLE QRect getPictureRect() const;

    Q_INVOKABLE void enableGotoLastPos(bool enable);
    Q_INVOKABLE bool isGotoLastPos() const;

    Q_INVOKABLE void setPlayingMethod(AnyVODEnums::PlayingMethod method);
    Q_INVOKABLE void selectPlayingMethod(int method);
    Q_INVOKABLE AnyVODEnums::PlayingMethod getPlayingMethod() const;

    Q_INVOKABLE QString get3DMethodDesc(int method) const;
    Q_INVOKABLE QString get3DMethodCategory(int method) const;
    Q_INVOKABLE QString getSubtitle3DMethodDesc(int method) const;
    Q_INVOKABLE QString getAnaglyphAlgoritmDesc(int algorithm) const;
    Q_INVOKABLE QString getDeinterlaceDesc(int algorithm) const;
    Q_INVOKABLE QString getVRInputSourceDesc(int source) const;

    Q_INVOKABLE void audioNormalize();
    Q_INVOKABLE void audioEqualizer();
    Q_INVOKABLE void upSubtitlePosition();
    Q_INVOKABLE void downSubtitlePosition();
    Q_INVOKABLE void leftSubtitlePosition();
    Q_INVOKABLE void rightSubtitlePosition();
    Q_INVOKABLE void rewindProper();
    Q_INVOKABLE void forwardProper();
    Q_INVOKABLE void rewind5();
    Q_INVOKABLE void forward5();
    Q_INVOKABLE void rewind30();
    Q_INVOKABLE void forward30();
    Q_INVOKABLE void rewind60();
    Q_INVOKABLE void forward60();
    Q_INVOKABLE void gotoBegin();
    Q_INVOKABLE bool prev(bool check);
    Q_INVOKABLE bool next(bool check);
    Q_INVOKABLE void playOrder();
    Q_INVOKABLE void subtitleToggle();
    Q_INVOKABLE void prevSubtitleSync();
    Q_INVOKABLE void nextSubtitleSync();
    Q_INVOKABLE void selectSubtitleClass(const QString &className);
    Q_INVOKABLE void selectAudioStream(int index);
    Q_INVOKABLE void subtitleLanguageOrder();
    Q_INVOKABLE void audioOrder();
    Q_INVOKABLE void detail();
    Q_INVOKABLE void deinterlaceMethodOrder();
    Q_INVOKABLE void deinterlaceAlgorithmOrder();
    Q_INVOKABLE void selectDeinterlacerMethod(int method);
    Q_INVOKABLE void selectDeinterlacerAlgorithem(int algorithm);
    Q_INVOKABLE void prevAudioSync();
    Q_INVOKABLE void nextAudioSync();
    Q_INVOKABLE void subtitleHAlignOrder();
    Q_INVOKABLE void selectSubtitleHAlignMethod(int method);
    Q_INVOKABLE void repeatRangeStart();
    Q_INVOKABLE void repeatRangeEnd();
    Q_INVOKABLE void repeatRangeEnable();
    Q_INVOKABLE void seekKeyFrame();
    Q_INVOKABLE void skipOpening();
    Q_INVOKABLE void skipEnding();
    Q_INVOKABLE void useSkipRange();
    Q_INVOKABLE void captureExtOrder();
    Q_INVOKABLE void captureSingle();
    Q_INVOKABLE void selectCaptureExt(const QString &ext);
    Q_INVOKABLE QString getCaptureExt() const;
    Q_INVOKABLE void setCaptureDirectory(const QString &dir);
    Q_INVOKABLE QString getCaptureDirectory() const;
    Q_INVOKABLE void prevFrame();
    Q_INVOKABLE void nextFrame();
    Q_INVOKABLE void slowerPlayback();
    Q_INVOKABLE void fasterPlayback();
    Q_INVOKABLE void normalPlayback();
    Q_INVOKABLE void resetVideoAttribute();
    Q_INVOKABLE void brightnessDown();
    Q_INVOKABLE void brightnessUp();
    Q_INVOKABLE void saturationDown();
    Q_INVOKABLE void saturationUp();
    Q_INVOKABLE void hueDown();
    Q_INVOKABLE void hueUp();
    Q_INVOKABLE void contrastDown();
    Q_INVOKABLE void contrastUp();
    Q_INVOKABLE void brightness(double step);
    Q_INVOKABLE void saturation(double step);
    Q_INVOKABLE void hue(double step);
    Q_INVOKABLE void contrast(double step);
    Q_INVOKABLE void lowerMusic();
    Q_INVOKABLE void lowerVoice();
    Q_INVOKABLE void higherVoice();
    Q_INVOKABLE void sharply();
    Q_INVOKABLE void sharpen();
    Q_INVOKABLE void soften();
    Q_INVOKABLE void leftRightInvert();
    Q_INVOKABLE void topBottomInvert();
    Q_INVOKABLE void incSubtitleOpaque();
    Q_INVOKABLE void decSubtitleOpaque();
    Q_INVOKABLE void audioDeviceOrder();
    Q_INVOKABLE void selectAudioDevice(int device);
    Q_INVOKABLE void enableSearchSubtitle();
    Q_INVOKABLE void enableSearchLyrics();
    Q_INVOKABLE void subtitleVAlignOrder();
    Q_INVOKABLE void selectSubtitleVAlignMethod(int method);
    Q_INVOKABLE void incSubtitleSize();
    Q_INVOKABLE void decSubtitleSize();
    Q_INVOKABLE void userAspectRatioOrder();
    Q_INVOKABLE void selectUserAspectRatio(int index);
    Q_INVOKABLE int getCurrentUserAspectRatio() const;
    Q_INVOKABLE int getUserAspectRatioCount() const;
    Q_INVOKABLE void setCustomUserAspectRatio(const QSizeF &size);
    Q_INVOKABLE QSizeF getCustomUserAspectRatio() const;
    Q_INVOKABLE QString getUserAspectRatioDesc(int index) const;
    Q_INVOKABLE void useHWDecoder();
    Q_INVOKABLE void method3DOrder();
    Q_INVOKABLE void select3DMethod(int method);
    Q_INVOKABLE void selectVRInputSource(int source);
    Q_INVOKABLE void prevChapter();
    Q_INVOKABLE void nextChapter();
    Q_INVOKABLE bool moveChapter(int index, QString *desc);
    Q_INVOKABLE int getChapterIndex(double pos);
    Q_INVOKABLE void closeExternalSubtitle();
    Q_INVOKABLE void showAlbumJacket();
    Q_INVOKABLE void lastPlay();
    Q_INVOKABLE void repeatRangeStartBack100MS();
    Q_INVOKABLE void repeatRangeStartForw100MS();
    Q_INVOKABLE void repeatRangeEndBack100MS();
    Q_INVOKABLE void repeatRangeEndForw100MS();
    Q_INVOKABLE void repeatRangeBack100MS();
    Q_INVOKABLE void repeatRangeForw100MS();
    Q_INVOKABLE void useFrameDrop();
    Q_INVOKABLE void anaglyphAlgorithmOrder();
    Q_INVOKABLE void selectAnaglyphAlgorithm(int algorithm);
    Q_INVOKABLE void method3DSubtitleOrder();
    Q_INVOKABLE void select3DSubtitleMethod(int method);
    Q_INVOKABLE void use3DFull();
    Q_INVOKABLE void up3DSubtitleOffset();
    Q_INVOKABLE void down3DSubtitleOffset();
    Q_INVOKABLE void left3DSubtitleOffset();
    Q_INVOKABLE void right3DSubtitleOffset();
    Q_INVOKABLE void screenRotationDegreeOrder();
    Q_INVOKABLE void selectScreenRotationDegree(int degree);
    Q_INVOKABLE void useSearchSubtitleComplex();
    Q_INVOKABLE void histEQ();
    Q_INVOKABLE void hq3DDenoise();
    Q_INVOKABLE void ataDenoise();
    Q_INVOKABLE void owDenoise();
    Q_INVOKABLE void deband();
    Q_INVOKABLE void useBufferingMode();

    Q_INVOKABLE bool setScreenOrientation(bool landscape);
    Q_INVOKABLE bool setScreenOrientationRestore();
    Q_INVOKABLE bool getScreenOrientation() const;

    Q_INVOKABLE void loadSettings();
    Q_INVOKABLE void saveSettings();

    Q_INVOKABLE void loadEqualizer();
    Q_INVOKABLE void saveEqualizer();

    Q_INVOKABLE void loadSkipRange();
    Q_INVOKABLE void saveSkipRange();

    Q_INVOKABLE void loadDTVScannedChannels();
    Q_INVOKABLE bool isDTVOpened() const;

    Q_INVOKABLE int getOptionDescY() const;
    Q_INVOKABLE void setOptionDescY(int y);

    Q_INVOKABLE void setUseCustomControl(bool use);
    Q_INVOKABLE bool getUseCustomControl() const;

    Q_INVOKABLE QString getTimeString(double value) const;

    Q_INVOKABLE void setSubtitleSync(double sync);
    Q_INVOKABLE double getSubtitleSync() const;

    Q_INVOKABLE void setAudioSync(double sync);
    Q_INVOKABLE double getAudioSync() const;

    Q_INVOKABLE void useSubtitleCacheMode(bool use);
    Q_INVOKABLE bool isUseSubtitleCacheMode() const;

    Q_INVOKABLE int getPlayItemCount() const;
    Q_INVOKABLE int getCurrentPlayingIndex() const;
    Q_INVOKABLE void setCurrentPlayingIndex(int index);
    Q_INVOKABLE void resetCurrentPlayingIndex();
    Q_INVOKABLE void adjustPlayIndex();
    Q_INVOKABLE bool playAt(int index);
    Q_INVOKABLE void resetLyrics();
    Q_INVOKABLE void exitAtCppSide();
    Q_INVOKABLE void setFullScreen();

    Q_INVOKABLE QStringList getSPDIFAudioDevices();
    Q_INVOKABLE bool setSPDIFAudioDevice(int device);
    Q_INVOKABLE int getCurrentSPDIFAudioDevice() const;

    Q_INVOKABLE void useSPDIF(bool enable);
    Q_INVOKABLE bool isUseSPDIF() const;
    Q_INVOKABLE bool isOpenedSPDIF() const;
    Q_INVOKABLE bool isSPDIFAvailable() const;

    Q_INVOKABLE void setSPDIFEncodingMethod(int method);
    Q_INVOKABLE int getSPDIFEncodingMethod() const;

    Q_INVOKABLE void setUserSPDIFSampleRate(int sampleRate);
    Q_INVOKABLE int getUserSPDIFSampleRate() const;

    Q_INVOKABLE void userSPDIFSampleRateOrder();
    Q_INVOKABLE void selectUserSPDIFSampleRate(int index);
    Q_INVOKABLE int getCurrentUserSPDIFSampleRate() const;
    Q_INVOKABLE int getUserSPDIFSampleRateCount() const;
    Q_INVOKABLE QString getUserSPDIFSampleRateDesc(int index) const;

    Q_INVOKABLE void setVerticalScreenOffset(int offset);
    Q_INVOKABLE void setHorizontalScreenOffset(int offset);

    bool isPrevEnabled() const;
    bool isNextEnabled() const;
    bool isMute() const;
    bool isValid() const;

    Q_INVOKABLE bool playNext();
    Q_INVOKABLE bool playPrev();
    Q_INVOKABLE bool playCurrent();

    bool open(const QString &path, ExtraPlayData &data, const QUuid &unique);
    ShaderCompositer* getShader() const;
    FilterGraph& getFilterGraph();
    bool configFilterGraph();
    int getCurrentYoutubeIndex() const;
    DTVReader& getDTVReader();

    QString getLargeCoverFilePath() const;

    void scheduleRebuildShader();
    void checkGOMSubtitle();
    void tryOpenSubtitle(const QString &filePath);

    void callStartedEvent();
    void callPlayingEvent();
    void callPausedEvent();
    void callStoppedEvent();
    void callEndEvent();
    void callExitEvent();

public:
    Q_INVOKABLE static int getCaptureExtCount();
    Q_INVOKABLE static QString getCaptureExt(int index);
    static RenderScreen* getSelf();

public:
    Q_PROPERTY(bool wantToPlay READ wantToPlay WRITE setWantToPlay)

public:
    static const QString MOVIE_EXTS;
    static const QString AUDIO_EXTS;
    static const QString SUBTITLE_EXTS;
    static const QString PLAYLIST_EXTS;
    static const QString FONT_EXTS;
    static const QStringList MOVIE_EXTS_LIST;
    static const QStringList AUDIO_EXTS_LIST;
    static const QStringList SUBTITLE_EXTS_LIST;
    static const QStringList PLAYLIST_EXTS_LIST;
    static const QStringList MEDIA_EXTS_LIST;
    static const QStringList ALL_EXTS_LIST;
    static const QString CAPTURE_FORMAT;
    static const QStringList CAPTURE_FORMAT_LIST;
    static const QStringList FONT_EXTS_LIST;

private:
#ifdef Q_OS_RASPBERRY_PI
    static const char DBUS_SERVICE[][40];
    static const char DBUS_PATH[][33];
#endif

private:
    void setup3DMethod();
    void setupSubtitle3DMethod();
    void setupAnaglyphAlgorithm();
    void setupDeinterlaceMethod();
    void setupVRInputSource();

    bool setScreenKeepOn(bool on);
    bool setKeepActive(bool on);
    bool wantToPlay() const;
    void setWantToPlay(bool want);

    void callChanged();

    void navigate(double distance);
    void subtitleSync(double value);
    void audioSync(double value);

    void retreiveLyrics(const QString &filePath);
    void retreiveSubtitleURL(const QString &filePath);

    void updateLastPlay();

    bool setCurrentPlayingIndexByUnique();
    bool updatePlayList(const QString &path);
    void getYouTubePlayItems(const QString &address, QVector<PlayItem> *itemVector);
    void setPlayList(const QVector<PlayItem> &list);
    void addToPlayList(const QVector<PlayItem> &list);

    bool isRotatable() const;

#ifdef Q_OS_ANDROID
    void callActivityEvent(const QString &funcName) const;
#endif

#ifdef Q_OS_RASPBERRY_PI
    bool debusInit();
    void debusDeinit();
#endif

private:
    virtual void customEvent(QEvent *event);

signals:
    void viewLyrics();
    void showSubtitlePopup();
    void failedPlay();
    void goBack();
    void screenUpdated();
    void playing();
    void resumed();
    void started();
    void paused();
    void stopped();
    void ended();
    void exit();
    void recovered();
    void exitFromRenderer();
    void spdifEnabled();
    void vrInputSourceChanged();

private slots:
    void sync();
    void handleWindowChanged(QQuickWindow *window);
    void handleApplicationStateChanged(Qt::ApplicationState state);
    void handleExitFromRenderer();

private:
#ifdef Q_OS_IOS
    static void CALLBACK iosNotifyCallback(DWORD status);
#endif
    static void playingCallback(void *userData);
    static void endedCallback(void *userData);
    static void emptyBufferCallback(void *userData, bool empty);
    static void showAudioOptionDescCallback(void *userData, const QString &desc, bool show);
    static void audioSubtitleCallback(void *userData, const QVector<Lyrics> &lines);
    static void paintCallback(void *userData);
    static void abortCallback(void *userData, int reason);
    static void recoverCallback(void *userData);

private:
    static RenderScreen *m_self;
    static QMutex m_selfLock;

private:
#ifdef Q_OS_RASPBERRY_PI
    enum ScreenSaverAPI
    {
        SSAPI_FDO_SS,
        SSAPI_FDO_PM,
        SSAPI_MATE,
        SSAPI_GNOME,
        SSAPI_COUNT
    };
#endif

private:
    GLRenderer m_renderer;
    MediaPresenter m_presenter;
    QString m_filePath;
    bool m_wantToPlay;
    Status m_status;
    LastPlay m_lastPlay;
    bool m_gotoLastPlay;
    bool m_priorSubtitleDirectory;
    bool m_useSearchSubtitleComplex;
    QStringList m_subtitleDirectory;
    QString m_lastPlayPath;
    QSettings m_settings;
    AnyVODEnums::PlayingMethod m_playingMethod;
    QString m_3DMethodDesc[AnyVODEnums::V3M_COUNT];
    QString m_3DMethodCategory[AnyVODEnums::V3M_COUNT];
    QString m_subtitle3DMethodDesc[AnyVODEnums::S3M_COUNT];
    QString m_anaglyphAlgorithmDesc[AnyVODEnums::AGA_COUNT];
    QString m_deinterlaceDesc[AnyVODEnums::DA_COUNT];
    QString m_vrInputSourceDesc[AnyVODEnums::VRI_COUNT];
    UserAspectRatioList m_userAspectRatioList;
    UserSPDIFSampleRateList m_userSPDIFSampleRateList;
    bool m_pausedByInactive;
    bool m_isLandscape;
    bool m_useHeadTracking;
    float m_subtitleSize;
    int m_currentPlayingIndex;
    PlayListModel *m_playList;
    QUuid m_unique;
    QString m_title;
    ExtraPlayData m_playData;
    StatusUpdater m_statusUpdater;
#ifdef Q_OS_RASPBERRY_PI
    DBusConnection *m_dbusConn;
    DBusPendingCall *m_dbusPending;
    dbus_uint32_t m_dbusCookie;
    ScreenSaverAPI m_screenSaverAPI;
    XDGScreenSaverDisabler m_xdgDisabler;
#endif
#ifdef Q_OS_IOS
    bool m_isPausedByIOSNotify;
#endif
    QQuickItem *m_lyrics1;
    QQuickItem *m_lyrics2;
    QQuickItem *m_lyrics3;
    QQuickItem *m_empty;
};

Q_DECLARE_METATYPE(RenderScreen::EqualizerItem)
QDataStream& operator << (QDataStream &out, const RenderScreen::EqualizerItem &item);
QDataStream& operator >> (QDataStream &in, RenderScreen::EqualizerItem &item);

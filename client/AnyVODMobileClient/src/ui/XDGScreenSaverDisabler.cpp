﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "XDGScreenSaverDisabler.h"

#include <QProcess>
#include <QDebug>

XDGScreenSaverDisabler::XDGScreenSaverDisabler() :
    m_stop(false)
{

}

void XDGScreenSaverDisabler::stop()
{
    this->m_stop = true;

    this->m_lock.lock();
    this->m_wait.wakeOne();
    this->m_lock.unlock();

    if (this->isRunning())
        this->wait();

    this->m_stop = false;
}

void XDGScreenSaverDisabler::run()
{
    while (!this->m_stop)
    {
        QStringList args;

        args << "reset";

        QProcess::execute("xdg-screensaver", args);

        this->m_lock.lock();
        this->m_wait.wait(&this->m_lock, 30000);
        this->m_lock.unlock();
    }
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "PlayListItemUpdater.h"
#include "../models/PlayListModel.h"
#include "core/Utils.h"

#include <QGuiApplication>

PlayListItemUpdater::PlayListItemUpdater(PlayListModel *parent) :
    m_parent(parent),
    m_stop(false)
{

}

void PlayListItemUpdater::setStop(bool stop)
{
    this->m_stop = stop;
}

void PlayListItemUpdater::run()
{
    QVector<PlayItem> list;

    this->m_parent->getPlayList(&list);

    foreach (const PlayItem &item, list)
    {
        if (this->m_stop)
            break;

        if (item.itemUpdated)
            continue;

        bool itemUpdated = false;
        QString title;

        if (Utils::determinRemoteProtocol(item.path) && !Utils::determinRemoteFile(item.path))
        {
            YouTubeURLPicker picker;
            QVector<YouTubeURLPicker::Item> youtube;

            youtube = picker.pickURL(item.path);

            if (!youtube.isEmpty())
            {
                title = youtube.first().title;
                itemUpdated = true;
            }
        }
        else
        {
            title = item.title;
            itemUpdated = true;
        }

        if (itemUpdated)
            QGuiApplication::postEvent(this->m_parent, new PlayListModel::UpdateItemEvent(title, item.unique));
        else
            QGuiApplication::postEvent(this->m_parent, new PlayListModel::UpdateIncCountEvent(item.unique));
    }
}

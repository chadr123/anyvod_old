﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "ThumbnailCache.h"
#include "../models/FileListModel.h"

#include <core/Utils.h>

#include <QGuiApplication>
#include <QStandardPaths>
#include <QDir>
#include <QFile>
#include <QTextStream>

const QString ThumbnailCache::CONFIG_EXTENSION = "config";

ThumbnailCache::ThumbnailCache() :
    m_reciever(NULL),
    m_stop(false),
    m_onlyDirectory(false),
    m_onlySubtitle(false)
{

}

ThumbnailCache::~ThumbnailCache()
{
    this->stop();

    QString root = this->getConfigRoot();
    QDir d(root);
    QStringList filter;

    filter << "*." + CONFIG_EXTENSION;

    foreach (const QString &config, d.entryList(filter))
    {
        QFile fconfig(root + QDir::separator() + config);

        if (!fconfig.open(QFile::ReadWrite))
            continue;

        QTextStream stream(&fconfig);
        QString path;
        QString imgPath;
        bool isDir = false;

        path = stream.readLine();
        isDir = (bool)stream.readLine().toInt();
        imgPath = stream.readLine();

        QFileInfo fileInfo(path);

        if (fileInfo.exists())
            continue;

        if (!isDir && !imgPath.isEmpty())
            QFile::remove(imgPath);

        fconfig.close();
        fconfig.remove();
    }
}

void ThumbnailCache::setReciever(QObject *reciever)
{
    this->m_reciever = reciever;
}

void ThumbnailCache::setOnlyDirectory(bool only)
{
    this->m_onlyDirectory = only;
}

void ThumbnailCache::setOnlySubtitle(bool only)
{
    this->m_onlySubtitle = only;
}

void ThumbnailCache::stop()
{
    this->m_stop = true;
    this->m_cond.wakeOne();

    if (this->isRunning())
        this->wait();

    this->clear();
}

void ThumbnailCache::clear()
{
    this->m_mutex.lock();
    this->m_reqList.clear();
    this->m_mutex.unlock();
}

bool ThumbnailCache::exists(const QString &moviePath)
{
    return !this->getConfigFile(moviePath, true).isEmpty();
}

bool ThumbnailCache::getThumbnailInfo(const QString &moviePath, INFO *ret)
{
    QString config = this->getConfigFile(moviePath, false);

    if (config.isEmpty())
        return false;

    QFile fconfig(config);

    if (!fconfig.open(QFile::ReadWrite))
        return false;

    QTextStream stream(&fconfig);
    QFileInfo fileInfo(moviePath);

    if (fconfig.size())
    {
        ret->path = stream.readLine();
        stream.readLine();
        ret->imgPath = stream.readLine();
        ret->duration = stream.readLine().toDouble();
        ret->width = stream.readLine().toInt();
        ret->height = stream.readLine().toInt();
        ret->fileSize = stream.readLine().toULongLong();
        ret->isVideo = (bool)stream.readLine().toInt();

        QDateTime lastmodDate = QDateTime::fromString(stream.readLine());

        if (lastmodDate == fileInfo.lastModified())
        {
            return true;
        }
        else
        {
            stream.reset();
            stream.resetStatus();
            stream.seek(0);
            stream.setDevice(NULL);

            fconfig.close();
            fconfig.remove();

            if (!fconfig.open(QFile::ReadWrite))
                return false;

            stream.setDevice(&fconfig);
        }
    }

    QString root = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    QFileInfo moviePathInfo(moviePath);
    QString filePath = root + QDir::separator() + QString::number(qHash(moviePath)) + ".jpg";
    bool isVideo = false;

    if (root.isEmpty())
        return false;

    if (!this->openWithType(moviePath, &isVideo) && isVideo)
        return false;

    double time = 0.0;
    FRAME_ITEM item;
    bool success = true;

    if (isVideo)
        time = FrameExtractor::getDuration() / 3;

    while (!this->getFrame(time, !isVideo, &item))
    {
        if (Utils::zeroDouble(time) <= 0.0)
        {
            success = false;
            break;
        }

        time /= 3;
    }

    double duration = 0.0;
    unsigned long long fileSize = moviePathInfo.size();

    if (success)
    {
        QImage resized = item.frame.scaledToWidth(200);
        QTransform trans;

        trans.translate((resized.width() / 2.0f), (resized.height() / 2.0f));
        trans.rotate(item.rotation);
        trans.translate((-resized.width() / 2.0f), (-resized.height() / 2.0f));

        resized = resized.transformed(trans);

        if (resized.isNull() || !resized.save(filePath))
        {
            this->close();
            return false;
        }

        duration = FrameExtractor::getDuration();
    }
    else
    {
        if (isVideo)
        {
            filePath = FileListModel::DEFAULT_MOVIE_ICON;
            duration = FrameExtractor::getDuration();
        }
        else
        {
            if (FrameExtractor::getDuration() <= 0.0)
                duration = this->getDuration(moviePath);

            filePath = FileListModel::DEFAULT_AUDIO_ICON;
        }
    }

    stream << moviePath << endl;
    stream << false << endl;
    stream << filePath << endl;
    stream << duration << endl;
    stream << this->getWidth() << endl;
    stream << this->getHeight() << endl;
    stream << fileSize << endl;
    stream << isVideo << endl;
    stream << fileInfo.lastModified().toString() << endl;

    ret->path = moviePath;
    ret->imgPath = filePath;
    ret->duration = duration;
    ret->width = this->getWidth();
    ret->height = this->getHeight();
    ret->fileSize = fileSize;
    ret->isVideo = isVideo;

    this->close();

    return true;
}

void ThumbnailCache::requestToUpdate(const QString &moviePath, const QModelIndex &index, int role, const QUuid &signature)
{
    if (!this->isRunning())
        this->start();

    REQUEST_INFO info;

    info.index = index;
    info.path = moviePath;
    info.role = role;
    info.signature = signature;

    this->m_mutex.lock();

    this->m_reqList.enqueue(info);

    this->m_cond.wakeOne();
    this->m_mutex.unlock();
}

QString ThumbnailCache::getConfigRoot() const
{
    QString root = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);

    if (root.isEmpty())
        return QString();

    QDir rootDir(root);
    const QString thumbnailName = "thumbnail" + this->m_reciever->objectName();

    if (!rootDir.mkpath(thumbnailName))
        return QString();

    root += QDir::separator() + thumbnailName;

    return root;
}

QString ThumbnailCache::getConfigFile(const QString &moviePath, bool exists) const
{
    QString root = this->getConfigRoot();

    if (root.isEmpty())
        return QString();

    QString configName = QString::number(qHash(moviePath));
    QString filePath = root + QDir::separator() + configName + "." + CONFIG_EXTENSION;
    QFile f(filePath);

    if (exists)
    {
        if (f.exists())
            return filePath;
        else
            return QString();
    }

    if (f.exists())
    {
        return filePath;
    }
    else
    {
        if (f.open(QFile::ReadWrite))
            return filePath;
        else
            return QString();
    }
}

double ThumbnailCache::getDuration(const QString &moviePath) const
{
    AVFormatContext *format = NULL;
    QString path = moviePath;

    path = path.replace("mms://", "mmst://", Qt::CaseInsensitive);

    if (avformat_open_input(&format, path.toUtf8(), NULL, NULL) != 0)
        return 0.0;

    if (avformat_find_stream_info(format, NULL) < 0)
    {
        avformat_close_input(&format);
        return 0.0;
    }

    double duration = format->duration / (double)AV_TIME_BASE;

    if (duration < 0.0)
        duration = 0.0;

    avformat_close_input(&format);

    return duration;
}

int ThumbnailCache::getEntryCount(const QString &path) const
{
    QString config = this->getConfigFile(path, false);

    if (config.isEmpty())
        return 0;

    QFile fconfig(config);

    if (!fconfig.open(QFile::ReadWrite))
        return 0;

    QTextStream stream(&fconfig);
    int count = 0;
    QFileInfo dirInfo(path);

    if (fconfig.size())
    {
        stream.readLine();
        stream.readLine();
        count = stream.readLine().toInt();

        QDateTime lastmodDate = QDateTime::fromString(stream.readLine());

        if (lastmodDate == dirInfo.lastModified())
        {
            return count;
        }
        else
        {
            stream.reset();
            stream.resetStatus();
            stream.seek(0);
            stream.setDevice(NULL);

            fconfig.close();
            fconfig.remove();

            if (!fconfig.open(QFile::ReadWrite))
                return 0;

            count = 0;
            stream.setDevice(&fconfig);
        }
    }

    QString root = QStandardPaths::writableLocation(QStandardPaths::CacheLocation);

    if (root.isEmpty())
        return 0;

    QDir start(path);
    int filter = QDir::NoDotAndDotDot;

    if (this->m_onlyDirectory)
        filter |= QDir::Dirs | QDir::Drives;
    else
        filter |= QDir::AllEntries;

    foreach (const QFileInfo &dir, start.entryInfoList((QDir::Filter)filter))
    {
        if (dir.isDir())
        {
            QString dirPath = dir.absoluteFilePath();
            QDir d(dirPath);
            QFileInfoList list = d.entryInfoList((QDir::Filter)filter);

            foreach (const QFileInfo &subPath, list)
            {
                bool exists = false;

                if (subPath.isFile())
                {
                    QString suffix = subPath.suffix();

                    if (this->m_onlySubtitle)
                        exists = Utils::isExtension(suffix, Utils::MT_SUBTITLE);
                    else
                        exists = Utils::isMediaExtension(suffix);
                }
                else if (subPath.isDir())
                {
                    exists = true;
                }

                if (exists)
                {
                    count++;
                    break;
                }
            }

            if (list.count() == 0 && this->m_onlyDirectory)
                count++;
        }
        else
        {
            QString suffix = dir.suffix();
            bool matched = false;

            if (this->m_onlySubtitle)
                matched = Utils::isExtension(suffix, Utils::MT_SUBTITLE);
            else
                matched = Utils::isMediaExtension(suffix);

            if (matched)
                count++;
        }
    }

    stream << path << endl;
    stream << true << endl;
    stream << count << endl;
    stream << dirInfo.lastModified().toString() << endl;

    return count;
}

void ThumbnailCache::run()
{
    while (true)
    {
        this->m_mutex.lock();
        this->m_cond.wait(&this->m_mutex);
        this->m_mutex.unlock();

        if (this->m_stop)
        {
            this->m_stop = false;
            break;
        }

        while (true)
        {
            if (this->m_stop)
            {
                this->m_stop = false;
                return;
            }

            if (!this->m_reciever)
            {
                this->msleep(500);
                continue;
            }

            this->m_mutex.lock();

            if (this->m_reqList.isEmpty())
            {
                this->m_mutex.unlock();
                break;
            }

            REQUEST_INFO reqInfo = this->m_reqList.dequeue();
            INFO info;

            this->m_mutex.unlock();

            QFileInfo fileInfo(reqInfo.path);
            int entryCount = 0;

            if (fileInfo.isDir())
            {
                entryCount = this->getEntryCount(reqInfo.path);
            }
            else
            {
                if (!this->getThumbnailInfo(reqInfo.path, &info))
                    continue;
            }

            QVariant var;

            switch (reqInfo.role)
            {
                case FileListModel::ThumbnailRole:
                {
                    var = info.imgPath;
                    break;
                }
                case FileListModel::DirEntryCountRole:
                {
                    if (fileInfo.isDir())
                        var = entryCount;
                    else
                        var = 0;

                    break;
                }
                case FileListModel::FileInfoRole:
                {
                    if (fileInfo.isDir())
                    {
                        var = entryCount;
                    }
                    else
                    {
                        QString fInfo;

                        Utils::getTimeString(info.duration, Utils::TIME_HH_MM_SS, &fInfo);

                        fInfo += " - ";

                        if (info.isVideo)
                            fInfo += QString::number(info.width) + "X" + QString::number(info.height) + ", ";

                        fInfo += Utils::sizeToString(info.fileSize) + "B";

                        var = fInfo;
                    }

                    break;
                }
                default:
                {
                    continue;
                }
            }

            QGuiApplication::postEvent(this->m_reciever, new FileListModel::UpdateItemEvent(var, reqInfo.index, reqInfo.role, reqInfo.signature));
        }
    }
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "DTVListItemUpdater.h"
#include "../models/DTVListModel.h"

#include <QGuiApplication>

DTVListItemUpdater::DTVListItemUpdater(DTVListModel *parent) :
    m_parent(parent),
    m_reader(NULL),
    m_stop(false),
    m_adapter(-1),
    m_type(DTVReaderInterface::ST_NONE),
    m_country(QLocale::AnyCountry),
    m_start(-1),
    m_end(-1)
{

}

void DTVListItemUpdater::setData(long adapter, DTVReaderInterface::SystemType systemType, QLocale::Country country,
                                 int start, int end)
{
    this->m_adapter = adapter;
    this->m_type = systemType;
    this->m_country = country;
    this->m_start = start;
    this->m_end = end;
}

void DTVListItemUpdater::setReader(DTVReader *reader)
{
    this->m_reader = reader;
}

void DTVListItemUpdater::setStop(bool stop)
{
    this->m_stop = stop;
}

void DTVListItemUpdater::run()
{
    QVector<DTVChannelMap::Channel> channels;
    int startChannelIndex = 0;
    int endChannelIndex = 0;

    if (this->m_reader->setChannelCountry(this->m_country, this->m_type))
        this->m_reader->getChannels(&channels);

    for (int i = 0; i < channels.count(); i++)
    {
        if (channels[i].channel == this->m_start)
        {
            startChannelIndex = i;
            break;
        }
    }

    for (int i = channels.count() - 1; i >= 0; i--)
    {
        if (channels[i].channel == this->m_end)
        {
            endChannelIndex = i;
            break;
        }
    }

    QGuiApplication::postEvent(this->m_parent, new DTVListModel::InitProgressEvent(startChannelIndex, endChannelIndex));

    for (int i = startChannelIndex; i <= endChannelIndex && i < channels.count(); i++)
    {
        const DTVChannelMap::Channel &channel = channels[i];
        DTVReader::ChannelInfo info;

        if (!this->m_parent->existScannedChannel(channel.channel))
        {
            bool ok = this->m_reader->scan(this->m_adapter, this->m_country, this->m_type, channel, i, &info);

            if (ok)
            {
                info.index = i;
                info.country = this->m_country;

                QGuiApplication::postEvent(this->m_parent, new DTVListModel::AddItemEvent(info));
            }

            QGuiApplication::postEvent(this->m_parent, new DTVListModel::SignalStrengthEvent((int)info.signal));
            QGuiApplication::postEvent(this->m_parent, new DTVListModel::CurrentChannelEvent(info.channel));
        }

        QGuiApplication::postEvent(this->m_parent, new DTVListModel::SetProgressEvent(i));

        if (this->m_stop)
            break;
    }

    this->m_stop = false;

    QGuiApplication::postEvent(this->m_parent, new DTVListModel::ScanFinishedEvent());
}

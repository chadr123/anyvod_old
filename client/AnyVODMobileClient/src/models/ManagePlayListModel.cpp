﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "ManagePlayListModel.h"
#include "core/Settings.h"
#include "core/Utils.h"

#include <QDebug>

ManagePlayListModel::ManagePlayListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat)
{
    QVariantHash hash = this->m_settings.value(SETTING_PLAYLIST, QVariantHash()).toHash();

    this->reload(hash);

    this->m_roleNames[NameRole] = "name";
}

ManagePlayListModel::~ManagePlayListModel()
{

}

void ManagePlayListModel::deleteItem(const QString &name)
{
    QVariantHash hash = this->m_settings.value(SETTING_PLAYLIST, QVariantHash()).toHash();

    hash.remove(name);

    this->m_settings.setValue(SETTING_PLAYLIST, hash);

    this->reload(hash);
}

void ManagePlayListModel::reload(const QVariantHash &hash)
{
    this->beginResetModel();

    this->m_names = hash.keys();
    this->m_names.sort();

    this->endResetModel();
}

int ManagePlayListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_names.count();
}

QVariant ManagePlayListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_names.count())
        return QVariant();

    QVariant ret;
    QString item = this->m_names[row];

    switch (role)
    {
        case NameRole:
        {
            ret = item;
            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

QHash<int, QByteArray> ManagePlayListModel::roleNames() const
{
    return this->m_roleNames;
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "core/Common.h"
#include "net/YouTubeURLPicker.h"
#include "../media/PlayListItemUpdater.h"

#include <QEvent>
#include <QFileInfo>
#include <QMutex>
#include <QAbstractListModel>

class QSettings;

class PlayListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    static const QEvent::Type UPDATE_ITEM_EVENT;
    static const QEvent::Type UPDATE_INC_COUNT;

public:
    enum RoleName
    {
        NameRole = Qt::UserRole,
        TitleRole,
        YoutubeRole,
        PathRole,
        FileNameRole,
        ThumbnailRole,
        ColorRole,
    };

    class UpdateItemEvent : public QEvent
    {
    public:
        UpdateItemEvent(const QString &title, const QUuid &unique) :
            QEvent(UPDATE_ITEM_EVENT),
            m_title(title),
            m_unique(unique)
        {

        }

        QString getTitle() const { return this->m_title; }
        QUuid getUnique() const { return this->m_unique; }

    private:
        QString m_title;
        QUuid m_unique;
    };

    class UpdateIncCountEvent : public QEvent
    {
    public:
        UpdateIncCountEvent(const QUuid &unique) :
            QEvent(UPDATE_INC_COUNT),
            m_unique(unique)
        {

        }

        QUuid getUnique() const { return this->m_unique; }

    private:
        QUuid m_unique;
    };

public:
    explicit PlayListModel(QObject *parent = NULL);
    ~PlayListModel();

    void setSettings(QSettings *settings);
    void setPlayList(const QVector<PlayItem> &list);
    bool addPlayList(const QVector<PlayItem> &list);
    void getPlayList(QVector<PlayItem> *ret);
    void clearPlayList();
    void stop();

    int updateYouTubeData(const QUuid &unique, const QVector<YouTubeURLPicker::Item> &items);

    int getCount() const;
    bool exist() const;

    QString getFileName(int index) const;
    PlayItem getPlayItem(int index) const;

    int findIndex(const QUuid &unique) const;

    Q_INVOKABLE bool selectOtherQuality(int index, int quality);
    Q_INVOKABLE int getQualityCount(int index) const;
    Q_INVOKABLE QString getQualityDesc(int index, int quality) const;

    Q_INVOKABLE QString getPath(int index) const;
    Q_INVOKABLE void copyPath(const QString &text);

    Q_INVOKABLE void setCurrentIndex(int index);
    Q_INVOKABLE int getCurrentIndex() const;

    Q_INVOKABLE void setPlayList(const QString &name);
    Q_INVOKABLE bool existPlayList(const QString &name) const;
    Q_INVOKABLE void savePlayListAs(const QString &name);
    Q_INVOKABLE void updateCurrentPlayList();
    Q_INVOKABLE QString getName() const;

public:
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);

protected:
    virtual QHash<int, QByteArray> roleNames() const;

private:
    virtual void customEvent(QEvent *event);
    virtual void timerEvent(QTimerEvent *event);

private:
    static const int UPDATE_PLAYLIST_TIME;
    static const int MAX_RETRY_COUNT;

private:
    void startUpdateThread();
    void addItem(QFileInfo &info, PlayItem &item);
    int findYouTubeIndex(const QString &quality, const QString &mime, const QVector<YouTubeURLPicker::Item> &items) const;

private:
    QHash<int, QByteArray> m_roleNames;
    QString m_lastYouTubeQuality;
    QString m_lastYouTubeMime;
    QVector<PlayItem> m_playItems;
    QMutex m_getPlayListMutex;
    PlayListItemUpdater m_updater;
    int m_updateTimerID;
    int m_currentIndex;
    int m_prevIndex;
    QString m_name;
    QSettings *m_settings;
};

Q_DECLARE_METATYPE(PlayItem)
QDataStream& operator << (QDataStream &out, const PlayItem &item);
QDataStream& operator >> (QDataStream &in, PlayItem &item);

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "PlayListModel.h"
#include "core/Utils.h"
#include "core/Settings.h"
#include "parsers/playlist/PlayListParserInterface.h"
#include "parsers/playlist/PlayListParserGenerator.h"
#include "../models/FileListModel.h"

#include <QFileInfo>
#include <QGuiApplication>
#include <QClipboard>
#include <QDir>

const QEvent::Type PlayListModel::UPDATE_ITEM_EVENT = (QEvent::Type)QEvent::registerEventType();
const QEvent::Type PlayListModel::UPDATE_INC_COUNT = (QEvent::Type)QEvent::registerEventType();
const int PlayListModel::UPDATE_PLAYLIST_TIME = 5000;
const int PlayListModel::MAX_RETRY_COUNT = 5;

QDataStream& operator << (QDataStream &out, const PlayItem &item)
{
    out << item.path;
    out << item.extraData.duration;
    out << item.extraData.totalFrame;
    out << item.extraData.valid;
    out << item.unique;
    out << item.title;
    out << item.totalTime;
    out << item.itemUpdated;
    out << item.extraData.userData;
    out << item.retry;

    return out;
}

QDataStream& operator >> (QDataStream &in, PlayItem &item)
{
    in >> item.path;
    in >> item.extraData.duration;
    in >> item.extraData.totalFrame;
    in >> item.extraData.valid;
    in >> item.unique;
    in >> item.title;
    in >> item.totalTime;
    in >> item.itemUpdated;
    in >> item.extraData.userData;
    in >> item.retry;

    if (item.unique.isNull())
        item.unique = QUuid::createUuid();

    return in;
}

PlayListModel::PlayListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_updater(this),
    m_updateTimerID(0),
    m_currentIndex(-1),
    m_prevIndex(-1),
    m_settings(NULL)
{
    this->m_roleNames[NameRole] = "name";
    this->m_roleNames[PathRole] = "path";
    this->m_roleNames[TitleRole] = "title";
    this->m_roleNames[YoutubeRole] = "isYoutube";
    this->m_roleNames[FileNameRole] = "fileName";
    this->m_roleNames[ThumbnailRole] = "thumbnail";
    this->m_roleNames[ColorRole] = "color";

    this->m_updateTimerID = this->startTimer(UPDATE_PLAYLIST_TIME);
}

PlayListModel::~PlayListModel()
{

}

void PlayListModel::setSettings(QSettings *settings)
{
    this->m_settings = settings;
}

int PlayListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_playItems.count();
}

QVariant PlayListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_playItems.count())
        return QVariant();

    QVariant ret;
    const PlayItem &item = this->m_playItems[row];

    switch (role)
    {
        case FileNameRole:
        {
            ret = QFileInfo(item.path).fileName();
            break;
        }
        case NameRole:
        {
            ret = QFileInfo(item.path).completeBaseName();
            break;
        }
        case TitleRole:
        {
            ret = item.title;
            break;
        }
        case YoutubeRole:
        {
            ret = !item.extraData.userData.isEmpty();
            break;
        }
        case PathRole:
        {
            ret = item.path;
            break;
        }
        case ThumbnailRole:
        {
            QFileInfo info(item.path);
            QString suffix = info.suffix();

            if (Utils::isExtension(suffix, Utils::MT_VIDEO))
                ret = FileListModel::DEFAULT_MOVIE_ICON;
            else if (Utils::isExtension(suffix, Utils::MT_AUDIO))
                ret = FileListModel::DEFAULT_AUDIO_ICON;
            else if (Utils::isExtension(suffix, Utils::MT_SUBTITLE))
                ret = FileListModel::DEFAULT_SUBTITLE_ICON;
            else
                ret = FileListModel::DEFAULT_ETC_ICON;

            break;
        }
        case ColorRole:
        {
            ret = this->m_currentIndex == row ? "red" : "black";
            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

bool PlayListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    int row = index.row();

    if (row < 0 || row >= this->m_playItems.count())
        return false;

    PlayItem &item = this->m_playItems[row];
    QVector<int> roles;

    roles.append(role);

    switch (role)
    {
        case TitleRole:
        {
            item.title = value.toString();
            break;
        }
        case ColorRole:
        {
            break;
        }
        default:
        {
            return false;
        }
    }

    emit dataChanged(index, index, roles);

    return true;
}

QHash<int, QByteArray> PlayListModel::roleNames() const
{
    return this->m_roleNames;
}

void PlayListModel::customEvent(QEvent *event)
{
    if (event->type() == UPDATE_ITEM_EVENT)
    {
        UpdateItemEvent *e = (UpdateItemEvent*)event;
        int index = this->findIndex(e->getUnique());

        if (index >= 0)
        {
            PlayItem &data = this->m_playItems[index];
            QString title = e->getTitle();

            data.itemUpdated = true;

            this->setData(this->index(index), title, TitleRole);
        }
    }
    else if (event->type() == UPDATE_INC_COUNT)
    {
        UpdateIncCountEvent *e = (UpdateIncCountEvent*)event;
        int index = this->findIndex(e->getUnique());

        if (index >= 0)
        {
            PlayItem &data = this->m_playItems[index];

            data.retry++;

            if (data.retry > MAX_RETRY_COUNT)
                data.itemUpdated = true;
        }
    }
    else
    {
        QAbstractListModel::customEvent(event);
    }
}

void PlayListModel::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == this->m_updateTimerID)
        this->startUpdateThread();
}

void PlayListModel::startUpdateThread()
{
    if (!this->m_updater.isRunning())
    {
        this->m_updater.setStop(false);
        this->m_updater.start();
    }
}

void PlayListModel::addItem(QFileInfo &info, PlayItem &item)
{
    item.unique = QUuid::createUuid();
    item.path = Utils::adjustNetworkPath(info.filePath());

    this->m_playItems.append(item);
}

int PlayListModel::findYouTubeIndex(const QString &quality, const QString &mime, const QVector<YouTubeURLPicker::Item> &items) const
{
    for (int i = 0; i < items.count(); i++)
    {
        const YouTubeURLPicker::Item &item = items[i];

        if (item.quality == quality && item.mimeType == mime)
            return i;
    }

    return 0;
}

void PlayListModel::setPlayList(const QVector<PlayItem> &list)
{
    this->clearPlayList();
    this->addPlayList(list);
}

bool PlayListModel::addPlayList(const QVector<PlayItem> &list)
{
    bool added = false;

    this->beginResetModel();

    for (int i = 0; i < list.count(); i++)
    {
        PlayItem playItem = list[i];
        QFileInfo info(playItem.path);
        QString suffix = info.suffix();
        PlayListParserGenerator gen;
        PlayListParserInterface *parser = gen.getParser(suffix);

        if (parser)
        {
            QVector<PlayListParserInterface::PlayListItem> fileList;

            parser->setRoot(info.absolutePath() + QDir::separator());

            if (parser->open(playItem.path))
            {
                parser->getFileList(&fileList);
                parser->close();

                foreach (const PlayListParserInterface::PlayListItem &item, fileList)
                {
                    QFileInfo fileInfo(item.path);
                    PlayItem fileItem;

                    fileItem.title = item.title;

                    this->addItem(fileInfo, fileItem);
                    added = true;
                }
            }
            else if (!parser->isSuccess())
            {
                this->addItem(info, playItem);
                added = true;
            }

            delete parser;
        }
        else
        {
            this->addItem(info, playItem);
            added = true;
        }
    }

    this->endResetModel();
    this->startUpdateThread();

    return added;
}

void PlayListModel::getPlayList(QVector<PlayItem> *ret)
{
    QMutexLocker locker(&this->m_getPlayListMutex);

    for (int i = 0; i < this->getCount(); i++)
        ret->append(this->getPlayItem(i));
}

int PlayListModel::getCount() const
{
    return this->m_playItems.count();
}

bool PlayListModel::exist() const
{
    return this->getCount() > 0;
}

QString PlayListModel::getFileName(int index) const
{
    if (this->exist() && index >= 0 && index < this->getCount())
        return QFileInfo(this->m_playItems[index].path).fileName();
    else
        return QString();
}

PlayItem PlayListModel::getPlayItem(int index) const
{
    return this->m_playItems[index];
}

int PlayListModel::findIndex(const QUuid &unique) const
{
    for (int i = 0; i < this->getCount(); i++)
    {
        PlayItem item = this->getPlayItem(i);

        if (item.unique == unique)
            return i;
    }

    return -1;
}

bool PlayListModel::selectOtherQuality(int index, int quality)
{
    PlayItem &playItem = this->m_playItems[index];

    if (playItem.youtubeData.isEmpty() || playItem.youtubeIndex == quality)
        return false;

    playItem.youtubeIndex = quality;

    this->m_lastYouTubeMime = playItem.youtubeData[quality].mimeType;
    this->m_lastYouTubeQuality = playItem.youtubeData[quality].quality;

    return true;
}

int PlayListModel::getQualityCount(int index) const
{
    const PlayItem &playItem = this->m_playItems[index];

    return playItem.youtubeData.count();
}

QString PlayListModel::getQualityDesc(int index, int quality) const
{
    const PlayItem &playItem = this->m_playItems[index];

    return playItem.youtubeData[quality].shortDesc;
}

QString PlayListModel::getPath(int index) const
{
    return this->m_playItems[index].path;
}

void PlayListModel::copyPath(const QString &text)
{
    QClipboard *clipboard = QGuiApplication::clipboard();
    QString clip;

    if (Utils::determinRemoteProtocol(text))
        clip = text;
    else
        clip = QDir::toNativeSeparators(text);

    clipboard->setText(clip);
}

void PlayListModel::setCurrentIndex(int index)
{
    this->m_currentIndex = index;

    if (index >= 0)
        this->setData(this->index(index), QVariant(), ColorRole);

    if (this->m_prevIndex >= 0)
        this->setData(this->index(this->m_prevIndex), QVariant(), ColorRole);

    this->m_prevIndex = index;
}

int PlayListModel::getCurrentIndex() const
{
    return this->m_currentIndex;
}

void PlayListModel::setPlayList(const QString &name)
{
    QVariantHash hash = this->m_settings->value(SETTING_PLAYLIST, QVariantHash()).toHash();
    QVector<PlayItem> playList;
    int curIndex = 0;
    QVariantList list = hash[name].toList();

    if (list.isEmpty())
        return;

    foreach (const QVariant &item, list)
    {
        if (item.canConvert<int>())
            curIndex = item.toInt();
        else
            playList.append(item.value<PlayItem>());
    }

    this->setPlayList(playList);
    this->setCurrentIndex(curIndex);

    this->m_name = name;
}

bool PlayListModel::existPlayList(const QString &name) const
{
    QVariantHash hash = this->m_settings->value(SETTING_PLAYLIST, QVariantHash()).toHash();

    return hash.contains(name);
}

void PlayListModel::savePlayListAs(const QString &name)
{
    QVariantHash hash = this->m_settings->value(SETTING_PLAYLIST, QVariantHash()).toHash();
    QVariantList list;

    foreach (const PlayItem &item, this->m_playItems)
        list.append(qVariantFromValue(item));

    list.append(this->m_currentIndex);

    hash[name] = list;
    this->m_settings->setValue(SETTING_PLAYLIST, hash);

    this->m_name = name;
}

void PlayListModel::updateCurrentPlayList()
{
    this->savePlayListAs(this->m_name);
}

QString PlayListModel::getName() const
{
    return this->m_name;
}

void PlayListModel::clearPlayList()
{
    this->stop();
    this->m_name.clear();

    this->beginResetModel();
    this->m_playItems.clear();
    this->endResetModel();
}

void PlayListModel::stop()
{
    this->m_updater.setStop(true);
    this->m_updater.wait();
}

int PlayListModel::updateYouTubeData(const QUuid &unique, const QVector<YouTubeURLPicker::Item> &items)
{
    int index = this->findIndex(unique);

    if (index >= 0)
    {
        PlayItem &data = this->m_playItems[index];

        data.youtubeData = items;
        data.youtubeIndex = this->findYouTubeIndex(this->m_lastYouTubeQuality, this->m_lastYouTubeMime, items);

        if (data.youtubeIndex == 0)
        {
            this->m_lastYouTubeMime.clear();
            this->m_lastYouTubeQuality.clear();
        }

        return data.youtubeIndex;
    }

    return 0;
}

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "device/DTVReaderInterface.h"

#include <QAbstractListModel>
#include <QEvent>

class RenderScreen;

class EPGListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum RoleName
    {
        TitleRole = Qt::UserRole,
        StartTimeRole,
        EndTimeRole,
        DurationRole,
    };
    Q_ENUM(RoleName)

public:
    explicit EPGListModel(QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

public:
    Q_INVOKABLE void init();
    Q_INVOKABLE QDateTime updateCurrentTime();

public:
    Q_PROPERTY(RenderScreen* screen MEMBER m_screen)

protected:
    virtual QHash<int, QByteArray> roleNames() const;

signals:
    void itemSelected(int index);
    void initUpdateTimer();

private:
    QHash<int, QByteArray> m_roleNames;
    QVector<DTVReaderInterface::EPG> m_epgs;
    QDateTime m_curDateTime;
    QDateTime m_startDateTime;
    RenderScreen *m_screen;
};

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "core/Common.h"
#include "../media/ThumbnailCache.h"
#include "../ui/RenderScreen.h"

#include <QAbstractListModel>
#include <QEvent>
#include <QUuid>
#include <QStack>

class FileListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    static const QEvent::Type UPDATE_ITEM_EVENT;

public:
    class UpdateItemEvent : public QEvent
    {
    public:
        UpdateItemEvent(QVariant var, QModelIndex &index, int role, QUuid &signature) :
            QEvent(UPDATE_ITEM_EVENT),
            m_var(var),
            m_index(index),
            m_role(role),
            m_signature(signature)
        {

        }

        QVariant getVar() const { return this->m_var; }
        QModelIndex getIndex() const { return this->m_index; }
        int getRole() const { return this->m_role; }
        QUuid getSignature() const { return this->m_signature; }

    private:
        QVariant m_var;
        QModelIndex m_index;
        int m_role;
        QUuid m_signature;
    };

    enum Type
    {
        Directory,
        File,
    };
    Q_ENUM(Type)

    enum MediaType
    {
        Video,
        Audio,
    };
    Q_ENUM(MediaType)

    enum QuickBarType
    {
        InternalType,
        ExternalType,
        VideoType,
        AudioType,
    };
    Q_ENUM(QuickBarType)

    enum SortType
    {
        NameAscending,
        TimeAscending,
        SizeAscending,
        TypeAscending,
        NameDescending,
        TimeDescending,
        SizeDescending,
        TypeDescending,
    };
    Q_ENUM(SortType)

    enum RoleName
    {
        ThumbnailRole = Qt::UserRole,
        NameRole,
        TypeRole,
        PathRole,
        FileInfoRole,
        FileNameRole,
        DirEntryCountRole,
    };

private:
    struct ITEM
    {
        ITEM()
        {
            dirEntryCount = -1;
        }

        QString path;
        QString imgPath;
        QString fileInfo;
        int dirEntryCount;
    };

    struct Preset
    {
        QString desc;
        QVector<RenderScreen::EqualizerItem> gains;
    };

public:
    explicit FileListModel(QObject *parent = NULL);
    ~FileListModel();

public:
    virtual int rowCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);

public:
    Q_INVOKABLE void init();
    Q_INVOKABLE double cdup();
    Q_INVOKABLE void update();
    Q_INVOKABLE void update(const QString &path);
    Q_INVOKABLE void updateFromDatabase(int type);
    Q_INVOKABLE void clearScrollPos();
    Q_INVOKABLE void cd(const QString &path, double scrollPos);
    Q_INVOKABLE QString getFirstFilePath(const QString &path) const;
    Q_INVOKABLE bool deletePath(const QString &path) const;

    Q_INVOKABLE bool isMedia(const QString &path) const;
    Q_INVOKABLE bool isSubtitle(const QString &path) const;
    Q_INVOKABLE bool isFile(const QString &path) const;
    Q_INVOKABLE QString replaceLastEntry(const QString &base, const QString& entry) const;
    Q_INVOKABLE QString getVersion() const;
    Q_INVOKABLE QString getLicenses() const;

    Q_INVOKABLE void setSetting(int key, QVariant value);
    Q_INVOKABLE QVariant getSetting(int key) const;
    Q_INVOKABLE bool isSupported(int key) const;

    Q_INVOKABLE int getTextCodecCount() const;
    Q_INVOKABLE QString getTextCodecName(int index) const;

    Q_INVOKABLE void setUseEqualizer(bool use) const;
    Q_INVOKABLE bool getUseEqualizer() const;

    Q_INVOKABLE int getEqualizerPresetCount() const;
    Q_INVOKABLE QString getEqualizerPresetName(int index) const;
    Q_INVOKABLE int getEqualizerPresetBandCount(int preset) const;
    Q_INVOKABLE int getEqualizerPresetBandGain(int preset, int band) const;

    Q_INVOKABLE int getEqualizerBandCount() const;
    Q_INVOKABLE int getEqualizerBandGain(int band) const;
    Q_INVOKABLE int getEqualizerCurrentIndex() const;
    Q_INVOKABLE int getEqualizerPreamp() const;

    Q_INVOKABLE void initEqualizerValue();
    Q_INVOKABLE void setEqualizerPreset(int preset) const;
    Q_INVOKABLE void setEqualizerBandForSave(int band, int value);

    Q_INVOKABLE void saveEqualizerPreamp(int value) const;
    Q_INVOKABLE void saveEqualizerBand() const;

    Q_INVOKABLE float valueTodB(int value) const;
    Q_INVOKABLE int dBToValue(float dB) const;

    Q_INVOKABLE void showToast(const QString &msg) const;
    Q_INVOKABLE int getToastLength() const;

    Q_INVOKABLE int getLanguageCount() const;
    Q_INVOKABLE QString getLanguageDesc(const QString &lang) const;
    Q_INVOKABLE QString getLanguageValue(int index) const;

    Q_INVOKABLE void setStatusBarStyle(bool enable);

    Q_INVOKABLE void setSortType(int type);
    Q_INVOKABLE int getSortType() const;

    Q_INVOKABLE QStringList getAudioDevices() const;

    Q_INVOKABLE QStringList getSPDIFAudioDevices() const;
    Q_INVOKABLE int getUserSPDIFSampleRateCount() const;
    Q_INVOKABLE QString getUserSPDIFSampleRateDesc(int index) const;

    QStringList getLanguages() const;

public:
    Q_INVOKABLE static int getCaptureExtCount();
    Q_INVOKABLE static QString getCaptureExt(int index);
    Q_INVOKABLE static void hideSplashScreen();

public:
    static const QString DEFAULT_MOVIE_ICON;
    static const QString DEFAULT_AUDIO_ICON;
    static const QString DEFAULT_SUBTITLE_ICON;
    static const QString DEFAULT_ETC_ICON;
    static const QString DEFAULT_DIRECTORY_ICON;
    static const QString OPEN_DIRECTORY_ICON;
    static const QString LANG_PREFIX;
    static const QString LANG_DIR;

public:
    Q_PROPERTY(QString curDirName READ curDirName CONSTANT)
    Q_PROPERTY(QString curDirPath READ curDirPath CONSTANT)
    Q_PROPERTY(QString topSignature READ topSignature CONSTANT)
    Q_PROPERTY(QString primaryPath READ primaryPath CONSTANT)
    Q_PROPERTY(QString secondaryPath READ secondaryPath CONSTANT)

    Q_PROPERTY(QString startDirectory MEMBER m_startDirectory NOTIFY startDirectoryChanged)
    Q_PROPERTY(bool onlyDirectory MEMBER m_onlyDirectory NOTIFY onlyDirectoryChanged)
    Q_PROPERTY(bool onlySubtitle MEMBER m_onlySubtitle NOTIFY onlySubtitleChanged)

    Q_PROPERTY(QString defaultMovieIcon MEMBER DEFAULT_MOVIE_ICON CONSTANT)
    Q_PROPERTY(QString defaultAudioIcon MEMBER DEFAULT_AUDIO_ICON CONSTANT)
    Q_PROPERTY(QString defaultSubtitleIcon MEMBER DEFAULT_SUBTITLE_ICON CONSTANT)
    Q_PROPERTY(QString defaultEtcIcon MEMBER DEFAULT_ETC_ICON CONSTANT)
    Q_PROPERTY(QString defaultDirectoryIcon MEMBER DEFAULT_DIRECTORY_ICON CONSTANT)
    Q_PROPERTY(QString openDirectoryIcon MEMBER OPEN_DIRECTORY_ICON CONSTANT)

    Q_PROPERTY(QStringList fontFamilies MEMBER m_fontFamilies CONSTANT)
    Q_PROPERTY(int quickBarType MEMBER m_quickBarType CONSTANT)

signals:
    void startDirectoryChanged();
    void onlyDirectoryChanged();
    void onlySubtitleChanged();

protected:
    QString curDirName() const;
    QString curDirPath() const;
    QString topSignature() const;

    QString primaryPath() const;
    QString secondaryPath() const;

    bool existsMediaFiles(const QString &path) const;

private:
    void initEqualizerPreset(int maxBand);
    QString findSecondaryPath() const;
    QString shortcutkeyToName(AnyVODEnums::ShortcutKey key) const;
    void saveQuickBarType(QuickBarType type);

private:
    virtual void customEvent(QEvent *event);

protected:
    virtual QHash<int, QByteArray> roleNames() const;

protected:
    QVector<ITEM> m_curList;

private:
    QHash<int, QByteArray> m_roleNames;
    QString m_curDir;
    QString m_rootDirPrimary;
    QString m_rootDirSecondary;
    ThumbnailCache m_cache;
    QUuid m_signature;
    QVector<QString> m_textCodecNames;
    QVector<Preset> m_presets;
    QVector<RenderScreen::EqualizerItem> m_saveGains;
    QStringList m_fontFamilies;
    QString m_startDirectory;
    bool m_onlyDirectory;
    bool m_onlySubtitle;
    QStack<double> m_scrollPos;
    QuickBarType m_quickBarType;
    bool m_deleteMediaWithSubtitle;
    SortType m_sortType;
};

﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "FileListModel.h"
#include "core/Utils.h"
#include "core/Settings.h"
#include "../ui/RenderScreen.h"
#include "../core/BuildNumber.h"

#ifdef Q_OS_IOS
#include "../core/IOSDelegate.h"
#endif

extern "C"
{
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libswresample/swresample.h>
    #include <libswscale/swscale.h>
    #include <libavfilter/avfilter.h>

    #include <zlib.h>

    #include <bass/bass.h>
    #include <bass/bassmix.h>
    #include <bass/bass_fx.h>

    #include <ass/ass.h>
}

#ifdef Q_OS_ANDROID
#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>
#include <QtAndroid>
#endif

#include <QDir>
#include <QTextCodec>
#include <QFontDatabase>
#include <QDebug>

const QEvent::Type FileListModel::UPDATE_ITEM_EVENT = (QEvent::Type)QEvent::registerEventType();
const QString FileListModel::DEFAULT_MOVIE_ICON = "assets/defaultMovie.png";
const QString FileListModel::DEFAULT_AUDIO_ICON = "assets/defaultAudio.png";
const QString FileListModel::DEFAULT_SUBTITLE_ICON = "assets/defaultSubtitle.png";
const QString FileListModel::DEFAULT_ETC_ICON = "assets/defaultETC.png";
const QString FileListModel::DEFAULT_DIRECTORY_ICON = "assets/dirClosed.png";
const QString FileListModel::OPEN_DIRECTORY_ICON = "assets/dir.png";
const QString FileListModel::LANG_PREFIX = "anyvod";
const QString FileListModel::LANG_DIR = ASSETS_PREFIX"/languages";

FileListModel::FileListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_onlyDirectory(false),
    m_onlySubtitle(false),
    m_quickBarType(InternalType),
    m_deleteMediaWithSubtitle(false),
    m_sortType(NameAscending)
{
    this->m_roleNames[ThumbnailRole] = "thumbnail";
    this->m_roleNames[NameRole] = "name";
    this->m_roleNames[TypeRole] = "type";
    this->m_roleNames[PathRole] = "path";
    this->m_roleNames[FileInfoRole] = "fileInfo";
    this->m_roleNames[FileNameRole] = "fileName";
    this->m_roleNames[DirEntryCountRole] = "dirEntryCount";
}

FileListModel::~FileListModel()
{

}

int FileListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_curList.count();
}

QVariant FileListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_curList.count())
        return QVariant();

    FileListModel *me = (FileListModel*)this;
    QVariant ret;
    const ITEM &item = this->m_curList[row];

    switch (role)
    {
        case ThumbnailRole:
        {
            if (item.imgPath.isEmpty())
            {
                QFileInfo info(item.path);
                const QString defaultImg = DEFAULT_ETC_ICON;
                const QString dirImg = DEFAULT_DIRECTORY_ICON;

                if (info.isFile() && (Utils::isExtension(info.suffix(), Utils::MT_VIDEO) ||
                                      Utils::isExtension(info.suffix(), Utils::MT_AUDIO)))
                {
                    me->m_cache.requestToUpdate(item.path, index, role, this->m_signature);
                    ret = defaultImg;
                }
                else if (info.isFile() && Utils::isExtension(info.suffix(), Utils::MT_SUBTITLE))
                {
                    ret = DEFAULT_SUBTITLE_ICON;
                }
                else if (info.isDir())
                {
                    ret = dirImg;
                }
                else
                {
                    ret = defaultImg;
                }
            }
            else
            {
                if (item.imgPath.startsWith("assets"))
                    ret = item.imgPath;
                else
                    ret = "file://" + item.imgPath;
            }

            break;
        }
        case DirEntryCountRole:
        {
            ret = 0;

            if (item.dirEntryCount == -1)
            {
                QFileInfo info(item.path);

                if (info.isDir())
                    me->m_cache.requestToUpdate(item.path, index, role, this->m_signature);
            }
            else
            {
                ret = item.dirEntryCount;
            }

            break;
        }
        case FileInfoRole:
        {
            if (item.fileInfo.isEmpty())
            {
                QFileInfo info(item.path);

                if (info.isDir() || (info.isFile() && (Utils::isExtension(info.suffix(), Utils::MT_VIDEO) ||
                                                       Utils::isExtension(info.suffix(), Utils::MT_AUDIO))))
                {
                    me->m_cache.requestToUpdate(item.path, index, role, this->m_signature);
                    ret = QString();
                }
                else if (info.isFile() && Utils::isExtension(info.suffix(), Utils::MT_SUBTITLE))
                {
                    ret = Utils::sizeToString(info.size()) + "B";
                }
            }
            else
            {
                ret = item.fileInfo;
            }

            break;
        }
        case FileNameRole:
        {
            ret = QFileInfo(item.path).fileName();
            break;
        }
        case NameRole:
        {
            ret = QFileInfo(item.path).completeBaseName();
            break;
        }
        case TypeRole:
        {
            QFileInfo info(item.path);

            if (info.isDir())
                ret = Directory;
            else if (info.isFile())
                ret = File;

            break;
        }
        case PathRole:
        {
            ret = item.path;
            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

bool FileListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    int row = index.row();

    if (row < 0 || row >= this->m_curList.count())
        return false;

    ITEM &item = this->m_curList[row];
    QVector<int> roles;

    roles.append(role);

    switch (role)
    {
        case ThumbnailRole:
        {
            item.imgPath = value.toString();
            break;
        }
        case DirEntryCountRole:
        {
            item.dirEntryCount = value.toInt();
            break;
        }
        case FileInfoRole:
        {
            item.fileInfo = value.toString();
            break;
        }
        default:
        {
            return false;
        }
    }

    emit dataChanged(index, index, roles);

    return true;
}

void FileListModel::init()
{
    QString primaryPath;
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);

#if defined Q_OS_ANDROID
    QAndroidJniObject extDir = QAndroidJniObject::callStaticObjectMethod("android/os/Environment", "getExternalStorageDirectory", "()Ljava/io/File;");
    QAndroidJniObject extPath = extDir.callObjectMethod("getAbsolutePath", "()Ljava/lang/String;");

    primaryPath = extPath.toString();
#elif defined Q_OS_IOS
    primaryPath = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).first();
#elif defined Q_OS_RASPBERRY_PI
    primaryPath = QStandardPaths::standardLocations(QStandardPaths::HomeLocation).first();
#endif

    this->m_rootDirPrimary = primaryPath;
    this->m_rootDirSecondary = this->findSecondaryPath();

    if (this->m_startDirectory.isEmpty())
    {
        QuickBarType type = (QuickBarType)settings.value(SETTING_CURRENT_QUICKBAR_ITEM, InternalType).toInt();

        switch (type)
        {
            case InternalType:
                this->update(this->m_rootDirPrimary);
                break;
            case ExternalType:
                this->update(this->m_rootDirSecondary);
                break;
            case VideoType:
                this->updateFromDatabase(Video);
                break;
            case AudioType:
                this->updateFromDatabase(Audio);
                break;
            default:
                break;
        }

        this->m_quickBarType = type;
    }
    else
    {
        this->update(this->m_startDirectory);
    }

    QList<int> mibList = QTextCodec::availableMibs();
    QStringList codecList;

    foreach (const int mib, mibList)
        codecList.append(QTextCodec::codecForMib(mib)->name());

    qStableSort(codecList);
    codecList.removeDuplicates();

    this->m_textCodecNames.clear();

    foreach (const QString &name, codecList)
        this->m_textCodecNames.append(name);

    QFontDatabase fontDatabase;

    foreach (const QString &family, fontDatabase.families())
    {
        if (fontDatabase.isPrivateFamily(family))
            continue;

        this->m_fontFamilies.append(family);
    }

    RenderScreen tmp;
    int maxBand = tmp.getBandCount();

    if (settings.value(SETTING_FIRST_LOADING, true).toBool())
        tmp.saveSettings();

    this->m_deleteMediaWithSubtitle = settings.value(SETTING_DELETE_MEDIA_WITH_SUBTITLE, this->m_deleteMediaWithSubtitle).toBool();

    this->initEqualizerPreset(maxBand);
    this->initEqualizerValue();
}

double FileListModel::cdup()
{
    if (this->m_rootDirPrimary == this->m_curDir ||
        this->m_rootDirSecondary == this->m_curDir ||
        this->m_curDir.isEmpty())
        return 0.0;

    QDir dir(this->m_curDir);

    dir.cdUp();
    this->m_curDir = dir.absolutePath();

    if (this->m_scrollPos.isEmpty())
        return 0.0;
    else
        return this->m_scrollPos.pop();
}

void FileListModel::update()
{
    this->update(this->m_curDir);
}

void FileListModel::cd(const QString &path, double scrollPos)
{
    this->update(path);
    this->m_scrollPos.push(scrollPos);
}

QString FileListModel::getFirstFilePath(const QString &path) const
{
    QDir dir(path);

    foreach (const QFileInfo &file, dir.entryInfoList(QDir::Files, QDir::Name))
    {
        QString suffix = file.suffix();

        if (Utils::isMediaExtension(suffix))
            return file.absoluteFilePath();
    }

    return QString();
}

bool FileListModel::deletePath(const QString &path) const
{
    QFileInfo info(path);

    if (info.isDir())
    {
        QDir d(path);

        return d.removeRecursively();
    }
    else
    {
        QFile f(path);
        bool removed = f.remove();

        if (this->m_deleteMediaWithSubtitle)
        {
            RenderScreen temp;

            temp.tryOpenSubtitle(path);

            if (temp.existExternalSubtitle())
            {
                QFile subtitle(temp.getSubtitlePath());

                subtitle.remove();
            }
        }

        return removed;
    }
}

bool FileListModel::isMedia(const QString &path) const
{
    return Utils::isMediaExtension(QFileInfo(path).suffix());
}

bool FileListModel::isSubtitle(const QString &path) const
{
    return Utils::isExtension(QFileInfo(path).suffix(), Utils::MT_SUBTITLE);
}

bool FileListModel::isFile(const QString &path) const
{
    return QFileInfo(path).isFile();
}

QString FileListModel::replaceLastEntry(const QString &base, const QString &entry) const
{
    QDir path(base);

    path.cdUp();

    return path.absolutePath() + QDir::separator() + entry;
}

QString FileListModel::getVersion() const
{
    QString desc;
    QString version;
    uint versionInt;
    const QString VERSION_TEMPLATE = "%1.%2.%3.%4";

    version = VERSION_TEMPLATE.arg(MAJOR).arg(MINOR).arg(PATCH).arg(BUILDNUM) + " " + STRFILEVER;
    desc += "AnyVOD : " + version;
    desc += "\n";

    desc += "Built on " __DATE__ " at " __TIME__;
    desc += "\n";

    desc += "Author : DongRyeol Cha (chadr@dcple.com)";
    desc += "\n\n";

    desc += "Qt - ";
    desc += qVersion();
    desc += "\n";

    desc += "ass - ";
    versionInt = ass_library_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 28).arg(((versionInt >> 24 & 0x0f) * 10) + (versionInt >> 20 & 0x0f)).arg(versionInt >> 12 & 0x0f).arg(0);
    desc += version;
    desc += "\n";

    desc += "avcodec - ";
    versionInt = avcodec_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "avformat - ";
    versionInt = avformat_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "avutil - ";
    versionInt = avutil_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "avfilter - ";
    versionInt = avfilter_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "swscale - ";
    versionInt = swscale_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "swresample - ";
    versionInt = swresample_version();
    version = VERSION_TEMPLATE.arg(versionInt >> 16).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff).arg(0);
    desc += version;
    desc += "\n";

    desc += "bass - ";
    versionInt = BASS_GetVersion();
    version = VERSION_TEMPLATE.arg(versionInt >> 24).arg(versionInt >> 16 & 0xff).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff);
    desc += version;
    desc += "\n";

    desc += "bass_fx - ";
    versionInt = BASS_FX_GetVersion();
    version = VERSION_TEMPLATE.arg(versionInt >> 24).arg(versionInt >> 16 & 0xff).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff);
    desc += version;
    desc += "\n";

    desc += "bassmix - ";
    versionInt = BASS_Mixer_GetVersion();
    version = VERSION_TEMPLATE.arg(versionInt >> 24).arg(versionInt >> 16 & 0xff).arg(versionInt >> 8 & 0xff).arg(versionInt & 0xff);
    desc += version;
    desc += "\n";

    desc += "zlib - ";
    desc += zlibVersion();
    desc += "\n";

    return desc;
}

QString FileListModel::getLicenses() const
{
    QDir dir(ASSETS_PREFIX"/licenses");
    QString licenses;

    foreach (const QFileInfo &fileName, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files, QDir::Name))
    {
        QFile file(fileName.absoluteFilePath());

        if (!file.open(QFile::ReadOnly))
            continue;

        QString content = file.readAll();

        licenses += fileName.baseName().toUpper() + "\n";

        for (int i = 0; i < 130; i++)
            licenses += "-";

        licenses += "\n";
        licenses += content;
        licenses += "\n\n";
    }

    return licenses;
}

void FileListModel::setSetting(int key, QVariant value)
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);
    QString name = this->shortcutkeyToName((AnyVODEnums::ShortcutKey)key);

    if (!name.isEmpty())
    {
        settings.setValue(name, value);
        settings.sync();
    }

    if (key == AnyVODEnums::SK_DELETE_MEDIA_WITH_SUBTITLE)
        this->m_deleteMediaWithSubtitle = value.toBool();
}

QVariant FileListModel::getSetting(int key) const
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);
    QVariant value;
    QString name = this->shortcutkeyToName((AnyVODEnums::ShortcutKey)key);

    if (!name.isEmpty())
    {
        value = settings.value(name);

        if (value.type() == QVariant::String)
        {
            QString b = value.toString().toLower();

            if (b == "false" || b == "true")
                return value.toBool();
        }
    }

    if (value.isNull())
        value = "";

    if (key == AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY)
    {
        QDir capDir(value.toString());

        if (!capDir.exists())
        {
            GLRenderer::CaptureInfo tmp;

            value = tmp.savePath;
        }
    }

    return value;
}

bool FileListModel::isSupported(int key) const
{
    return Utils::isSupported(key);
}

int FileListModel::getTextCodecCount() const
{
    return this->m_textCodecNames.count();
}

QString FileListModel::getTextCodecName(int index) const
{
    return this->m_textCodecNames[index];
}

int FileListModel::getCaptureExtCount()
{
    return RenderScreen::getCaptureExtCount();
}

QString FileListModel::getCaptureExt(int index)
{
    return RenderScreen::getCaptureExt(index);
}

void FileListModel::hideSplashScreen()
{
#if defined Q_OS_ANDROID
    QtAndroid::hideSplashScreen();
#endif
}

void FileListModel::setUseEqualizer(bool use) const
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);

    settings.setValue(SETTING_USE_EQUALIZER, use);
    settings.sync();
}

bool FileListModel::getUseEqualizer() const
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);

    return settings.value(SETTING_USE_EQUALIZER).toBool();
}

int FileListModel::getEqualizerPresetCount() const
{
    return this->m_presets.count();
}

QString FileListModel::getEqualizerPresetName(int index) const
{
    return this->m_presets[index].desc;
}

int FileListModel::getEqualizerPresetBandGain(int preset, int band) const
{
    return this->dBToValue(this->m_presets[preset].gains[band].gain);
}

int FileListModel::getEqualizerBandCount() const
{
    return this->m_saveGains.count();
}

int FileListModel::getEqualizerBandGain(int band) const
{
    return this->dBToValue(this->m_saveGains[band].gain);
}

int FileListModel::getEqualizerCurrentIndex() const
{
    for (int i = 0; i < this->m_presets.count(); i++)
    {
        const Preset &preset = this->m_presets[i];
        bool same = true;

        for (int j = 0; j < preset.gains.count() && j < this->m_saveGains.count(); j++)
            same &= preset.gains[j].gain == this->m_saveGains[j].gain;

        if (same)
            return i;
    }

    return -1;
}

int FileListModel::getEqualizerPreamp() const
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);

    return this->dBToValue(settings.value(SETTING_EQUALIZER_PREAMP).toFloat());
}

void FileListModel::initEqualizerValue()
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);
    QVariantList list = settings.value(SETTING_EQUALIZER_GAINS, QVariantList()).toList();

    this->m_saveGains.clear();

    for (int i = 0; i < list.count(); i++)
    {
        RenderScreen::EqualizerItem item;

        item.gain = list[i].value<RenderScreen::EqualizerItem>().gain;
        this->m_saveGains.append(item);
    }
}

void FileListModel::setEqualizerPreset(int preset) const
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);
    QVariantList list;
    QVector<RenderScreen::EqualizerItem> eqGains;

    for (int i = 0; i < this->getEqualizerPresetBandCount(preset); i++)
    {
        RenderScreen::EqualizerItem item;

        item.gain = this->valueTodB(this->getEqualizerPresetBandGain(preset, i));

        eqGains.append(item);
    }

    for (int i = 0; i < eqGains.count(); i++)
        list.append(qVariantFromValue(eqGains[i]));

    settings.setValue(SETTING_EQUALIZER_GAINS, list);
}

void FileListModel::setEqualizerBandForSave(int band, int value)
{
    this->m_saveGains[band].gain = this->valueTodB(value);
}

void FileListModel::saveEqualizerPreamp(int value) const
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);

    settings.setValue(SETTING_EQUALIZER_PREAMP, this->valueTodB(value));
}

void FileListModel::saveEqualizerBand() const
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);
    QVariantList list;

    for (int i = 0; i < this->m_saveGains.count(); i++)
        list.append(qVariantFromValue(this->m_saveGains[i]));

    settings.setValue(SETTING_EQUALIZER_GAINS, list);
}

int FileListModel::getEqualizerPresetBandCount(int preset) const
{
    return this->m_presets[preset].gains.count();
}

void FileListModel::initEqualizerPreset(int maxBand)
{
    QString presets =
            "기본 값/0/0/0/0/0/0/0/0/0/0\n"
            "클래식/0/0/0/0/0/0/-4.8/-4.8/-4.8/-6.3\n"
            "베이스/5.9/5.9/5.9/3.6/1/-2.9/-5.5/-6.7/-7/-7\n"
            "베이스 & 트레블/4.4/3.6/0/-4.8/-3.2/1/5.1/6.7/7.4/7.4\n"
            "트레블/-6.3/-6.3/-6.3/-2.9/1.7/6.7/9.7/9.7/9.7/10.5\n"
            "헤드폰/2.9/6.7/3.2/-2.5/-1.7/1/2.9/5.9/7.8/9\n"
            "홀/6.7/6.7/3.6/3.6/0/-3.2/-3.2/-3.2/0/0\n"
            "소프트 락/2.5/2.5/1.3/-0.6/-2.9/-3.6/-2.5/-0.6/1.7/5.5\n"
            "클럽/0/0/2.5/3.6/3.6/3.6/2.5/0/0/0\n"
            "댄스/5.9/4.4/1.3/0/0/-3.6/-4.8/-4.8/0/0\n"
            "라이브/-3.2/0/2.5/3.2/3.6/3.6/2.5/1.7/1.7/1.3\n"
            "파티/4.4/4.4/0/0/0/0/0/0/4.4/4.4\n"
            "팝/-1.3/2.9/4.4/4.8/3.2/-1/-1.7/-1.7/-1.3/-1.3\n"
            "레게/0/0/-0.6/-3.6/0/4.4/4.4/0/0/0\n"
            "락/4.8/2.9/-3.6/-5.1/-2.5/2.5/5.5/6.7/6.7/6.7\n"
            "스카/-1.7/-3.2/-2.9/-0.6/2.5/3.6/5.5/5.9/6.7/5.9\n"
            "소프트/2.9/1/-1/-1.7/-1/2.5/5.1/5.9/6.7/7.4\n"
            "테크노/4.8/3.6/0/3.6/3.2/0/4.8/5.9/5.9/5.5\n"
            "보컬/-4.2/-2.7/-1.2/3.6/5.4/5.4/3.2/2.1/1.4/0.6\n"
            "재즈/2.1/3.9/3.2/-4.5/-0.9/2.9/5.4/3.9/-0.9/2.9\n";

    trUtf8("기본 값");
    trUtf8("클래식");
    trUtf8("베이스");
    trUtf8("베이스 & 트레블");
    trUtf8("트레블");
    trUtf8("헤드폰");
    trUtf8("홀");
    trUtf8("소프트 락");
    trUtf8("클럽");
    trUtf8("댄스");
    trUtf8("라이브");
    trUtf8("파티");
    trUtf8("팝");
    trUtf8("레게");
    trUtf8("락");
    trUtf8("스카");
    trUtf8("소프트");
    trUtf8("테크노");
    trUtf8("보컬");
    trUtf8("재즈");

    QTextStream stream(&presets);

    this->m_presets.clear();

    while (!stream.atEnd())
    {
        QString line = stream.readLine();
        Preset preset;
        RenderScreen::EqualizerItem gain;

        line = line.trimmed();

        if (line.length() == 0)
            continue;

        if (line.startsWith(Utils::COMMENT_PREFIX))
            continue;

        QStringList pair = line.split('/');

        if (pair.length() < maxBand + 1)
            continue;

        preset.desc = trUtf8(pair[0].toUtf8());

        for (int i = 0; i < maxBand; i++)
        {
            gain.gain = pair[i + 1].toFloat();
            preset.gains.append(gain);
        }

        this->m_presets.append(preset);
    }
}

void FileListModel::update(const QString &path)
{
    this->m_cache.setReciever(this);
    this->m_cache.setOnlyDirectory(this->m_onlyDirectory);
    this->m_cache.setOnlySubtitle(this->m_onlySubtitle);
    this->m_cache.clear();

    if (path == this->m_rootDirPrimary)
        this->saveQuickBarType(InternalType);
    else if (path == this->m_rootDirSecondary)
        this->saveQuickBarType(ExternalType);

    this->beginResetModel();
    this->m_curList.clear();

    if (!path.isEmpty())
    {
        QDir sdcard(path);
        QVector<ITEM> dirList;
        QVector<ITEM> fileList;
        int filter = QDir::NoDotAndDotDot;
        int sort;

        if (this->m_onlyDirectory)
            filter |= QDir::Dirs | QDir::Drives;
        else
            filter |= QDir::AllEntries;

        switch (this->m_sortType)
        {
            case NameAscending:
                sort = QDir::Name;
                break;
            case TimeAscending:
                sort = QDir::Time | QDir::Reversed;
                break;
            case SizeAscending:
                sort = QDir::Size | QDir::Reversed;
                break;
            case TypeAscending:
                sort = QDir::Type;
                break;
            case NameDescending:
                sort = QDir::Name | QDir::Reversed;
                break;
            case TimeDescending:
                sort = QDir::Time;
                break;
            case SizeDescending:
                sort = QDir::Size;
                break;
            case TypeDescending:
                sort = QDir::Type | QDir::Reversed;
                break;
            default:
                sort = QDir::Name;
                break;
        }

        foreach (const QFileInfo &dir, sdcard.entryInfoList((QDir::Filter)filter, (QDir::SortFlag)sort))
        {
            ITEM item;
            QString dirPath = dir.absoluteFilePath();

            if (dir.isDir())
            {
                QDir d(dirPath);
                QFileInfoList list = d.entryInfoList((QDir::Filter)filter);
                bool exists = false;

                foreach (const QFileInfo &subPath, list)
                {
                    if (subPath.isFile())
                    {
                        QString suffix = subPath.suffix();

                        if (this->m_onlySubtitle)
                            exists = Utils::isExtension(suffix, Utils::MT_SUBTITLE);
                        else
                            exists = Utils::isMediaExtension(suffix);

                        if (exists)
                            break;
                    }
                    else if (subPath.isDir())
                    {
                        exists = true;
                        break;
                    }
                }

                if (exists || this->m_onlyDirectory)
                {
                    item.path = dirPath;
                    dirList.append(item);
                }
            }
            else
            {
                QString suffix = dir.suffix();
                bool matched = false;

                if (this->m_onlySubtitle)
                    matched = Utils::isExtension(suffix, Utils::MT_SUBTITLE);
                else
                    matched = Utils::isMediaExtension(suffix);

                if (matched)
                {
                    item.path = dirPath;
                    fileList.append(item);
                }
            }
        }

        this->m_curList.append(dirList);
        this->m_curList.append(fileList);
    }

    this->m_curDir = path;
    this->m_signature = QUuid::createUuid();

    this->endResetModel();
}

void FileListModel::updateFromDatabase(int type)
{
    this->m_cache.setReciever(this);
    this->m_cache.setOnlyDirectory(false);
    this->m_cache.setOnlySubtitle(false);
    this->m_cache.clear();

    switch (type)
    {
        case Video:
            this->saveQuickBarType(VideoType);
            break;
        case Audio:
            this->saveQuickBarType(AudioType);
            break;
        default:
            break;
    }

    this->beginResetModel();
    this->m_curList.clear();

#ifdef Q_OS_ANDROID
    const char *func = NULL;
    QString field;
    QString descending;

    switch (type)
    {
        case Video:
            func = "getVideoURLs";
            break;
        case Audio:
            func = "getAudioURLs";
            break;
        default:
            break;
    }

    switch (this->m_sortType)
    {
        case NameAscending:
            field = "name";
            break;
        case TimeAscending:
            field = "time";
            break;
        case SizeAscending:
            field = "size";
            break;
        case TypeAscending:
            field = "type";
            break;
        case NameDescending:
            field = "name";
            descending = "desc";
            break;
        case TimeDescending:
            field = "time";
            descending = "desc";
            break;
        case SizeDescending:
            field = "size";
            descending = "desc";
            break;
        case TypeDescending:
            field = "type";
            descending = "desc";
            break;
        default:
            field = "name";
            break;
    }

    QAndroidJniEnvironment env;
    QAndroidJniObject activity = QtAndroid::androidActivity();
    QAndroidJniObject paths = activity.callObjectMethod(func, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;",
                                                        QAndroidJniObject::fromString(field).object<jstring>(),
                                                        QAndroidJniObject::fromString(descending).object<jstring>(),
                                                        NULL);

    if (paths.isValid())
    {
        int pathCount = env->GetArrayLength(paths.object<jarray>());
        jobjectArray pathsJava = paths.object<jobjectArray>();

        for (int i = 0; i < pathCount; i++)
        {
            jobject pathJava = env->GetObjectArrayElement(pathsJava, i);
            QAndroidJniObject path = pathJava;
            ITEM item;

            item.path = path.toString();

            this->m_curList.append(item);

            env->DeleteLocalRef(pathJava);
        }
    }
#else
    (void)type;
#endif

    this->m_curDir.clear();
    this->m_signature = QUuid::createUuid();

    this->endResetModel();
}

void FileListModel::clearScrollPos()
{
    this->m_scrollPos.clear();
}

QString FileListModel::curDirName() const
{
    QDir d(this->m_curDir);
    QString name = d.dirName();

    if (name == QDir(this->m_rootDirPrimary).dirName() ||
        name == QDir(this->m_rootDirSecondary).dirName() ||
        this->m_curDir.isEmpty())
        return this->topSignature();
    else
        return name;
}

QString FileListModel::curDirPath() const
{
    return this->m_curDir;
}

QString FileListModel::topSignature() const
{
    return "AnyVOD Mobile";
}

QString FileListModel::primaryPath() const
{
    return this->m_rootDirPrimary;
}

QString FileListModel::secondaryPath() const
{
    return this->m_rootDirSecondary;
}

bool FileListModel::existsMediaFiles(const QString &path) const
{
    QDir d(path);
    QFileInfoList list = d.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::NoSymLinks);

    if (list.isEmpty())
    {
        return false;
    }
    else
    {
        bool exist = false;

        foreach (const QFileInfo &subPath, list)
        {
            QString suffix = subPath.suffix();

            if (subPath.isFile() && Utils::isMediaExtension(suffix))
                exist |= true;
            else
                exist |= this->existsMediaFiles(subPath.absoluteFilePath());
        }

        return exist;
    }
}

QString FileListModel::findSecondaryPath() const
{
#ifdef Q_OS_ANDROID
    QString path = getenv("SECONDARY_STORAGE");

    if (path.isEmpty())
    {
        QStringList list;
        QAndroidJniEnvironment env;
        QAndroidJniObject activity = QtAndroid::androidActivity();
        QAndroidJniObject extDirs = activity.callObjectMethod("getExternalFilesDirs",
                                                              "(Ljava/lang/String;)[Ljava/io/File;",
                                                              NULL);

        if (extDirs.isValid())
        {
            int dirCount = env->GetArrayLength(extDirs.object<jarray>());
            jobjectArray dirsJava = extDirs.object<jobjectArray>();

            if (dirCount > 1)
            {
                jobject dirJava = env->GetObjectArrayElement(dirsJava, 1);
                QAndroidJniObject dir = dirJava;
                QAndroidJniObject extPath = dir.callObjectMethod("getAbsolutePath", "()Ljava/lang/String;");
                QDir path(extPath.toString());

                while (path.dirName() != "Android")
                {
                    if (!path.cdUp())
                        break;
                }

                path.cdUp();

                list.append(path.absolutePath());

                env->DeleteLocalRef(dirJava);
            }
        }

        list.append("/storage/sdcard0/ext_sd");
        list.append("/sdcard2");
        list.append("/mnt/sdcard/ext_sd");
        list.append("/storage/sdcard1");
        list.append("/storage/extsdcard");
        list.append("/storage/sdcard0/external_sdcard");
        list.append("/mnt/extsdcard");
        list.append("/mnt/sdcard/external_sd");
        list.append("/mnt/external_sd");
        list.append("/mnt/media_rw/sdcard1");
        list.append("/removable/microsd");
        list.append("/mnt/emmc");
        list.append("/storage/external_SD");
        list.append("/storage/ext_sd");
        list.append("/storage/removable/sdcard1");
        list.append("/data/sdext");
        list.append("/data/sdext2");
        list.append("/data/sdext3");
        list.append("/data/sdext4");
        list.append("/storage/3E39-1DE6");

        foreach (const QString &item, list)
        {
            QDir d(item);

            if (!d.entryList(QDir::AllEntries | QDir::NoDotAndDotDot).isEmpty())
                return item;
        }
    }
    else
    {
        QStringList pathList = path.split(":", QString::SkipEmptyParts);

        if (!pathList.isEmpty())
        {
            foreach (const QString &item, pathList)
            {
                QDir d(item);

                if (!d.entryList(QDir::AllEntries | QDir::NoDotAndDotDot).isEmpty())
                    return item;
            }
        }
    }

    return QString();
#else
    return QString();
#endif
}

QString FileListModel::shortcutkeyToName(AnyVODEnums::ShortcutKey key) const
{
    QString name;

    switch (key)
    {
        case AnyVODEnums::SK_LANG:
            name = SETTING_LANGUAGE;
            break;
        case AnyVODEnums::SK_FONT:
            name = SETTING_FONT;
            break;
        case AnyVODEnums::SK_USE_HW_DECODER:
            name = SETTING_USE_HW_DECODER;
            break;
        case AnyVODEnums::SK_USE_FRAME_DROP:
            name = SETTING_USE_FRAME_DROP;
            break;
        case AnyVODEnums::SK_SHOW_ALBUM_JACKET:
            name = SETTING_SHOW_ALBUM_JACKET;
            break;
        case AnyVODEnums::SK_SELECT_CAPTURE_EXT_ORDER:
            name = SETTING_SCREEN_CAPTURE_EXT;
            break;
        case AnyVODEnums::SK_SELECT_CAPTURE_DIRECTORY:
            name = SETTING_SCREEN_CAPTURE_DIR;
            break;
        case AnyVODEnums::SK_DEINTERLACE_METHOD_ORDER:
            name = SETTING_DEINTERLACER_METHOD;
            break;
        case AnyVODEnums::SK_DEINTERLACE_ALGORITHM_ORDER:
            name = SETTING_DEINTERLACER_ALGORITHM;
            break;
        case AnyVODEnums::SK_LAST_PLAY:
            name = SETTING_ENABLE_LAST_PLAY;
            break;
        case AnyVODEnums::SK_SEEK_KEYFRAME:
            name = SETTING_SEEK_KEYFRAME;
            break;
        case AnyVODEnums::SK_USE_BUFFERING_MODE:
            name = SETTING_USE_BUFFERING_MODE;
            break;
        case AnyVODEnums::SK_SUBTITLE_TOGGLE:
            name = SETTING_SHOW_SUBTITLE;
            break;
        case AnyVODEnums::SK_SEARCH_SUBTITLE_COMPLEX:
            name = SETTING_SEARCH_SUBTITLE_COMPLEX;
            break;
        case AnyVODEnums::SK_SUBTITLE_HALIGN_ORDER:
            name = SETTING_HALIGN_SUBTITLE;
            break;
        case AnyVODEnums::SK_SUBTITLE_VALIGN_ORDER:
            name = SETTING_VALIGN_SUBTITLE;
            break;
        case AnyVODEnums::SK_TEXT_ENCODING:
            name = SETTING_TEXT_ENCODING;
            break;
        case AnyVODEnums::SK_ENABLE_SEARCH_SUBTITLE:
            name = SETTING_ENABLE_SEARCH_SUBTITLE;
            break;
        case AnyVODEnums::SK_ENABLE_SEARCH_LYRICS:
            name = SETTING_ENABLE_SEARCH_LYRICS;
            break;
        case AnyVODEnums::SK_AUDIO_NORMALIZE:
            name = SETTING_USE_NORMALIZER;
            break;
        case AnyVODEnums::SK_AUDIO_EQUALIZER:
            name = SETTING_USE_EQUALIZER;
            break;
        case AnyVODEnums::SK_SUBTITLE_CACHE_MODE:
            name = SETTING_USE_SUBTITLE_CACHE_MODE;
            break;
        case AnyVODEnums::SK_DELETE_MEDIA_WITH_SUBTITLE:
            name = SETTING_DELETE_MEDIA_WITH_SUBTITLE;
            break;
        case AnyVODEnums::SK_USE_LOW_QUALITY_MODE:
            name = SETTING_USE_LOW_QUALITY_MODE;
            break;
        case AnyVODEnums::SK_AUDIO_DEVICE_ORDER:
            name = SETTING_AUDIO_DEVICE;
            break;
        case AnyVODEnums::SK_SPDIF_AUDIO_DEVICE_ORDER:
            name = SETTING_SPDIF_DEVICE;
            break;
        case AnyVODEnums::SK_USE_SPDIF_ENCODING_ORDER:
            name = SETTING_SPDIF_ENCODING;
            break;
        case AnyVODEnums::SK_SPDIF_USER_SAMPLE_RATE_ORDER:
            name = SETTING_SPDIF_SAMPLE_RATE;
            break;
        case AnyVODEnums::SK_USE_SPDIF:
            name = SETTING_USE_SPDIF;
            break;
        default:
            break;
    }

    return name;
}

void FileListModel::saveQuickBarType(FileListModel::QuickBarType type)
{
    QSettings settings(Utils::getSettingPath() + SETTING_FILE_NAME, QSettings::IniFormat);

    settings.setValue(SETTING_CURRENT_QUICKBAR_ITEM, type);
    this->m_quickBarType = type;
}

float FileListModel::valueTodB(int value) const
{
    return (float)value / 10.0f;
}

int FileListModel::dBToValue(float dB) const
{
    return (int)(dB * 10.0f);
}

void FileListModel::showToast(const QString &msg) const
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
        activity.callMethod<void>("showToast", "(Ljava/lang/String;)V", QAndroidJniObject::fromString(msg).object<jstring>());
#else
    (void)msg;
#endif
}

int FileListModel::getToastLength() const
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
        return activity.callMethod<int>("getToastLength");
#else
#endif
    return 0;
}

int FileListModel::getLanguageCount() const
{
    return this->getLanguages().count();
}

QString FileListModel::getLanguageDesc(const QString &lang) const
{
    QLocale locale(lang);

    return QString("%1 (%2)").arg(locale.nativeLanguageName()).arg(locale.nativeCountryName());
}

QString FileListModel::getLanguageValue(int index) const
{
    QString lang = this->getLanguages().at(index);

    lang.truncate(lang.lastIndexOf('.'));
    lang.remove(0, lang.indexOf('_') + 1);

    return lang;
}

void FileListModel::setStatusBarStyle(bool enable)
{
    int color = 0x1874d0;

#if defined Q_OS_IOS
    IOSDelegate::setStatusBarStyle(enable, color);
#elif defined Q_OS_ANDROID
    QAndroidJniObject activity = QtAndroid::androidActivity();

    if (activity.isValid())
        activity.callMethod<void>("setStatusBarStyle", "(ZI)V", enable, color);
#else
    (void)enable;
    (void)color;
#endif
}

void FileListModel::setSortType(int type)
{
    this->m_sortType = (SortType)type;
}

int FileListModel::getSortType() const
{
    return this->m_sortType;
}

QStringList FileListModel::getAudioDevices() const
{
    RenderScreen tmp;

    return tmp.getAudioDevices();
}

QStringList FileListModel::getSPDIFAudioDevices() const
{
    RenderScreen tmp;

    return tmp.getSPDIFAudioDevices();
}

int FileListModel::getUserSPDIFSampleRateCount() const
{
    RenderScreen tmp;

    return tmp.getUserSPDIFSampleRateCount();
}

QString FileListModel::getUserSPDIFSampleRateDesc(int index) const
{
    RenderScreen tmp;

    return tmp.getUserSPDIFSampleRateDesc(index);
}

QStringList FileListModel::getLanguages() const
{
    QDir dir(LANG_DIR);
    QStringList fileNames = dir.entryList(QStringList(QString("%1_*.qm").arg(LANG_PREFIX)));

    return fileNames;
}

void FileListModel::customEvent(QEvent *event)
{
    if (event->type() == UPDATE_ITEM_EVENT)
    {
        UpdateItemEvent *e = (UpdateItemEvent*)event;

        if (this->m_signature == e->getSignature())
            this->setData(e->getIndex(), e->getVar(), e->getRole());
    }
    else
    {
        QAbstractListModel::customEvent(event);
    }
}

QHash<int, QByteArray> FileListModel::roleNames() const
{
    return this->m_roleNames;
}

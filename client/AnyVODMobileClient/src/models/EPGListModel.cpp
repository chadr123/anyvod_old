﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "EPGListModel.h"
#include "../ui/RenderScreen.h"

EPGListModel::EPGListModel(QObject *parent) :
    QAbstractListModel(parent),
    m_screen(NULL)
{
    this->m_roleNames[TitleRole] = "title";
    this->m_roleNames[StartTimeRole] = "startTime";
    this->m_roleNames[EndTimeRole] = "endTime";
    this->m_roleNames[DurationRole] = "duration";
}

int EPGListModel::rowCount(const QModelIndex &parent) const
{
    (void)parent;

    return this->m_epgs.count();
}

QVariant EPGListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();

    if (row < 0 || row >= this->m_epgs.count())
        return QVariant();

    QVariant ret;
    const DTVReaderInterface::EPG &item = this->m_epgs[row];

    switch (role)
    {
        case TitleRole:
        {
            ret = item.title;
            break;
        }
        case StartTimeRole:
        {
            ret = item.start.toLocalTime();
            break;
        }
        case EndTimeRole:
        {
            QDateTime startDate = item.start.toLocalTime();

            ret = startDate.addSecs(QTime(0, 0).secsTo(item.duration));
            break;
        }
        case DurationRole:
        {
            ret = QTime(0, 0).secsTo(item.duration);
            break;
        }
        default:
        {
            ret = QVariant();
            break;
        }
    }

    return ret;
}

void EPGListModel::init()
{
    int curIndex = -1;

    this->beginResetModel();

    this->m_screen->getDTVReader().getEPG(&this->m_epgs);

    for (int i = 0; i < this->m_epgs.count(); i++)
    {
        const DTVReaderInterface::EPG &epg = this->m_epgs[i];
        QDateTime curDate = QDateTime::currentDateTime();
        QDateTime startDate = epg.start.toLocalTime();
        QDateTime endDate = startDate.addSecs(QTime(0, 0).secsTo(epg.duration));

        if (startDate <= curDate && endDate >= curDate)
            curIndex = i;
    }

    this->endResetModel();

    QDateTime curDate = this->m_screen->getDTVReader().getCurrentDateTime();

    if (curDate.isValid())
        this->m_curDateTime = curDate.toLocalTime();
    else
        this->m_curDateTime = QDateTime::currentDateTime();

    this->m_startDateTime = QDateTime::currentDateTime();

    emit this->itemSelected(curIndex);
    emit this->initUpdateTimer();
}

QDateTime EPGListModel::updateCurrentTime()
{
    QDateTime time = QDateTime::currentDateTime();
    qint64 diff = this->m_startDateTime.secsTo(time);
    QDateTime cur = this->m_curDateTime.addSecs(diff);

    return cur;
}

QHash<int, QByteArray> EPGListModel::roleNames() const
{
    return this->m_roleNames;
}

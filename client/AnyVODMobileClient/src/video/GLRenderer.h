﻿/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "core/Common.h"
#include "core/Utils.h"

#include <QOpenGLFunctions>
#include <QStandardPaths>
#include <QSize>

class MediaPresenter;
class ShaderCompositer;
class AnyVODWindow;
class RenderScreen;
class QOpenGLFramebufferObject;

class GLRenderer : public QObject, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    struct CaptureInfo
    {
        CaptureInfo()
        {
            savePath = QStandardPaths::writableLocation(QStandardPaths::PicturesLocation) + "/AnyVOD/";
            ext = QString("JPG");
            capture = false;
            captureOrg = false;
            captureCount = 0;
            totalCount = 0;
            quality = -1;
            captureSize = QSize(1, 1);
            paused = false;

            Utils::createDirectory(savePath);
        }

        QString savePath;
        QString ext;
        bool capture;
        bool captureOrg;
        int captureCount;
        int totalCount;
        int quality;
        QSize captureSize;
        bool paused;
    };

public:
    GLRenderer(MediaPresenter *presenter, RenderScreen *parent);
    ~GLRenderer();

    void scheduleRebuildShader();
    void scheduleCleanup();

    void setViewPortSize(const QSize &size, AnyVODWindow *window);
    ShaderCompositer* getShader() const;
    void setCaptureInfo(const CaptureInfo &info);
    CaptureInfo getCaptureInfo() const;

public slots:
    void paint();
    void cleanup();
    void setup();

private:
    void deleteTexture(TextureID type);
    void genTexture(TextureID type, int size);
    void genTextureLuma(TextureID type, int size);
    void initTextures(bool autoSize);
    void genOffScreen(int size);
    bool isForceRGBA();

    void capturePrologue(bool captureOrg, const QSize &orgSize);
    void captureEpilogue(bool captureOrg, bool captureBound, const QSize &orgSize);
    void renderCapturePicture(const QRect &picArea, const QSizeF &texSize);

private:
    static const QStringList BGRA_BLACK_LIST;

private:
    QSize m_viewPortSize;
    MediaPresenter *m_presenter;
    bool m_isReady;
    TextureInfo m_texInfo[TEX_COUNT];
    ShaderCompositer *m_shader;
    AnyVODWindow *m_window;
    bool m_rebuildShader;
    CaptureInfo m_captureInfo;
    QOpenGLFramebufferObject *m_offScreen;
    RenderScreen *m_parent;
    bool m_wantToCleanup;
};

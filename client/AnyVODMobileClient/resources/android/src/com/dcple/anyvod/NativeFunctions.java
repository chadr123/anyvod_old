/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

package com.dcple.anyvod;

public class NativeFunctions
{
    public static native void onResumeMediaOwnState();
    public static native void onPauseMediaOwnState();

    public static native void onResumeMedia();
    public static native void onPauseMedia();
    public static native void onToggleMedia();
    public static native void onStopMedia();
    public static native void onRewindMedia();
    public static native void onForwardMedia();
    public static native void onPreviousMedia();
    public static native void onNextMedia();

    public static native boolean isVideo();
    public static native boolean isValid();

    public static native String getTitle();
    public static native long getDuration();
    public static native long getTotalPlayListCount();
    public static native long getCurrentPlayIndex();
    public static native String getCoverFilePath();
}

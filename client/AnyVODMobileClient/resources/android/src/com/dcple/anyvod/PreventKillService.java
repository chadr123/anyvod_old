/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

package com.dcple.anyvod;

import android.app.Service;
import android.app.Notification;

import android.os.IBinder;
import android.content.Intent;
import android.util.Log;

public class PreventKillService extends Service
{
    private static final int NOTIFICATION_ID = 2;

    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId)
    {
        super.onStart(intent, startId);

        this.startForeground(NOTIFICATION_ID, new Notification());
    }

    @Override
    public void onDestroy()
    {
        this.stopForeground(true);

        super.onDestroy();
    }
}

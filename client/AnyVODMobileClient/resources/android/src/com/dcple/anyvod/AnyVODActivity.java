/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

package com.dcple.anyvod;

import android.view.WindowManager;
import android.view.Window;

import android.widget.Toast;

import android.graphics.Color;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.provider.Settings;
import android.provider.MediaStore;

import android.net.Uri;
import android.database.Cursor;

import android.telephony.TelephonyManager;
import android.telephony.PhoneStateListener;

import android.util.Log;
import android.Manifest;

import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Bundle;
import android.os.Build;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.ComponentName;

import android.app.PendingIntent;

import android.support.v4.content.ContextCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class AnyVODActivity extends org.qtproject.qt5.android.bindings.QtActivity
{
    public static final String ACTION_REMOTE_GENERIC = BuildConfig.APPLICATION_ID + ".remote.";
    public static final String ACTION_REMOTE_PLAY = ACTION_REMOTE_GENERIC + "Play";
    public static final String ACTION_REMOTE_TOGGLE = ACTION_REMOTE_GENERIC + "Toggle";
    public static final String ACTION_REMOTE_PAUSE = ACTION_REMOTE_GENERIC + "Pause";
    public static final String ACTION_REMOTE_STOP = ACTION_REMOTE_GENERIC + "Stop";
    public static final String ACTION_REMOTE_PREVIOUS = ACTION_REMOTE_GENERIC + "Previous";
    public static final String ACTION_REMOTE_NEXT = ACTION_REMOTE_GENERIC + "Next";
    public static final String ACTION_REMOTE_REWIND = ACTION_REMOTE_GENERIC + "Rewind";
    public static final String ACTION_REMOTE_FORWARD = ACTION_REMOTE_GENERIC + "Forward";

    private static final int REQUEST_PERMS = 1;
    private static final int NOTIFICATION_ID = 1;
    private static final String[] NO_MEDIA_STYLE_MANUFACTURERS = {"huawei", "symphony teleca"};

    private WakeLock m_wakeLock = null;
    private MediaSessionCompat m_mediaSession = null;
    private PhoneStateListener m_phoneListener = null;
    private BroadcastReceiver m_headPhonePlugReceiver = null;
    private BroadcastReceiver m_remoteReceiver = null;
    private RemoteControlReceiver m_remoteControlReceiver = null;
    private Intent m_preventKillService = null;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

        if (tm != null)
        {
            this.m_phoneListener = new PhoneStateListener()
            {
                @Override
                public void onCallStateChanged(int state, String incomingNumber)
                {
                    switch (state)
                    {
                        case TelephonyManager.CALL_STATE_IDLE:
                            NativeFunctions.onResumeMediaOwnState();
                            break;
                        case TelephonyManager.CALL_STATE_OFFHOOK:
                        case TelephonyManager.CALL_STATE_RINGING:
                            NativeFunctions.onPauseMediaOwnState();
                            break;
                        default:
                            break;
                    }
                }
            };

            tm.listen(this.m_phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
        }

        PowerManager pm = (PowerManager)this.getSystemService(Context.POWER_SERVICE);

        this.m_wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "AnyVOD_AWake_CPU");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            ArrayList<String> requiredPermissions = new ArrayList<String>();

            requiredPermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            requiredPermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            requiredPermissions.add(Manifest.permission.READ_PHONE_STATE);

            String[] perms = this.getPerms(requiredPermissions);

            if (perms != null && perms.length > 0)
                ActivityCompat.requestPermissions(this, perms, REQUEST_PERMS);

            String pkgName = this.getPackageName();

            if (!pm.isIgnoringBatteryOptimizations(pkgName))
            {
                Intent intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);

                intent.setData(Uri.parse("package:" + pkgName));

                this.startActivity(intent);
            }
        }

        this.m_headPhonePlugReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                boolean isPlugged = (intent.getIntExtra("state", 0) > 0) ? true : false;

                if (isPlugged)
                    NativeFunctions.onResumeMediaOwnState();
                else
                    NativeFunctions.onPauseMediaOwnState();
            }
        };
        this.registerReceiver(this.m_headPhonePlugReceiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));

        IntentFilter filter = new IntentFilter();

        filter.setPriority(Integer.MAX_VALUE);

        filter.addAction(ACTION_REMOTE_PLAY);
        filter.addAction(ACTION_REMOTE_TOGGLE);
        filter.addAction(ACTION_REMOTE_PAUSE);
        filter.addAction(ACTION_REMOTE_STOP);
        filter.addAction(ACTION_REMOTE_PREVIOUS);
        filter.addAction(ACTION_REMOTE_NEXT);
        filter.addAction(ACTION_REMOTE_REWIND);
        filter.addAction(ACTION_REMOTE_FORWARD);

        this.m_remoteReceiver = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context context, Intent intent)
            {
                String action = intent.getAction();
                TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

                if (tm != null && tm.getCallState() != TelephonyManager.CALL_STATE_IDLE)
                    return;

                if (action.equalsIgnoreCase(ACTION_REMOTE_TOGGLE))
                    NativeFunctions.onToggleMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_PLAY))
                    NativeFunctions.onResumeMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_PAUSE))
                    NativeFunctions.onPauseMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_STOP))
                    NativeFunctions.onStopMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_REWIND))
                    NativeFunctions.onRewindMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_FORWARD))
                    NativeFunctions.onForwardMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_PREVIOUS))
                    NativeFunctions.onPreviousMedia();
                else if (action.equalsIgnoreCase(ACTION_REMOTE_NEXT))
                    NativeFunctions.onNextMedia();
            }
        };
        this.registerReceiver(this.m_remoteReceiver, filter);

        filter = new IntentFilter();

        filter.setPriority(Integer.MAX_VALUE);
        filter.addAction(Intent.ACTION_MEDIA_BUTTON);

        this.m_remoteControlReceiver = new RemoteControlReceiver();
        this.registerReceiver(this.m_remoteControlReceiver, filter);

        this.setRemoteControlReceiverEnabled(false);
        this.hideNotification();
    }

    @Override
    public void onNewIntent(Intent i)
    {

    }

    @Override
    public void onDestroy()
    {
        this.hideNotification();
        this.stopPreventKillService();

        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);

        if (tm != null && this.m_phoneListener != null)
            tm.listen(this.m_phoneListener, PhoneStateListener.LISTEN_NONE);

        if (this.m_headPhonePlugReceiver != null)
            this.unregisterReceiver(this.m_headPhonePlugReceiver);

        if (this.m_remoteReceiver != null)
            this.unregisterReceiver(this.m_remoteReceiver);

        if (this.m_remoteControlReceiver != null)
            this.unregisterReceiver(this.m_remoteControlReceiver);

        this.unInitMediaSession();

        super.onDestroy();
    }

    private void startPreventKillService()
    {
        if (this.m_preventKillService != null)
            return;

        this.m_preventKillService = new Intent(this, PreventKillService.class);
        this.startService(this.m_preventKillService);
    }

    private void stopPreventKillService()
    {
        if (this.m_preventKillService != null)
        {
            this.stopService(this.m_preventKillService);
            this.m_preventKillService = null;
        }
    }

    private void showNotification(boolean isPlaying)
    {
        if (this.m_mediaSession == null)
            return;

        MediaMetadataCompat metaData = this.m_mediaSession.getController().getMetadata();
        String title = metaData.getString(MediaMetadataCompat.METADATA_KEY_TITLE);
        long trackIndex = metaData.getLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER) - 1;
        long trackCount = metaData.getLong(MediaMetadataCompat.METADATA_KEY_NUM_TRACKS);
        long duration = metaData.getLong(MediaMetadataCompat.METADATA_KEY_DURATION);
        Bitmap cover = metaData.getBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART);
        PendingIntent toggle = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_REMOTE_TOGGLE), PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent stop = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_REMOTE_STOP), PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent prev = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_REMOTE_PREVIOUS), PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent next = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_REMOTE_NEXT), PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent content = PendingIntent.getActivity(this, 0, new Intent(this, AnyVODActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        String timeFormat;

        if (duration >= 360000)
            timeFormat = "HH:mm:ss";
        else
            timeFormat = "mm:ss";

        DateFormat formatter = new SimpleDateFormat(timeFormat);
        String durString = formatter.format(new Date(duration));

        if (cover == null)
            cover = BitmapFactory.decodeResource(this.getResources(), R.drawable.icon);

        builder.setSmallIcon(R.drawable.icon);
        builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        builder.setContentTitle("[" + (trackIndex + 1) + " / " + trackCount + "] - " + durString);
        builder.setContentText(title);
        builder.setAutoCancel(!isPlaying);
        builder.setOngoing(isPlaying);
        builder.setDeleteIntent(stop);
        builder.setContentIntent(content);
        builder.setLargeIcon(cover);

        boolean prevEnabled = false;
        boolean nextEnabled = false;

        if (trackCount <= 1)
        {
            prevEnabled = false;
            nextEnabled = false;
        }
        else if (trackIndex - 1 < 0)
        {
            prevEnabled = false;
            nextEnabled = true;
        }
        else if (trackIndex + 1 >= trackCount)
        {
            prevEnabled = true;
            nextEnabled = false;
        }
        else
        {
            prevEnabled = true;
            nextEnabled = true;
        }

        int[] buttonIndex = new int[prevEnabled && nextEnabled ? 3 : 2];

        if (prevEnabled)
        {
            builder.addAction(R.drawable.prev, "Previous", prev);
            buttonIndex[0] = 0;
        }

        if (isPlaying)
            builder.addAction(R.drawable.pause, "Pause", toggle);
        else
            builder.addAction(R.drawable.resume, "Resume", toggle);

        if (prevEnabled)
            buttonIndex[1] = 1;
        else
            buttonIndex[0] = 0;

        if (nextEnabled)
        {
            builder.addAction(R.drawable.next, "Next", next);

            if (prevEnabled)
                buttonIndex[2] = 2;
            else
                buttonIndex[1] = 1;
        }

        if (!this.isManufacturerBannedForMediastyleNotifications())
        {
            builder.setStyle(new NotificationCompat.MediaStyle()
                            .setMediaSession(this.m_mediaSession.getSessionToken())
                            .setShowActionsInCompactView(buttonIndex)
                            .setShowCancelButton(true)
                            .setCancelButtonIntent(stop)
            );
        }

        NotificationManagerCompat.from(this).notify(NOTIFICATION_ID, builder.build());
    }

    private void hideNotification()
    {
        NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID);
    }

    private boolean isManufacturerBannedForMediastyleNotifications()
    {
        for (String manufacturer : NO_MEDIA_STYLE_MANUFACTURERS)
            if (Build.MANUFACTURER.toLowerCase(Locale.getDefault()).contains(manufacturer))
                return true;

        return false;
    }

    private String[] getPerms(ArrayList<String> permList)
    {
        ArrayList<String> requiredPermissions = new ArrayList<String>();

        for (String perm : permList)
        {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED)
                requiredPermissions.add(perm);
        }

        return requiredPermissions.toArray(new String[requiredPermissions.size()]);
    }

    public void setRemoteControlReceiverEnabled(boolean enabled)
    {
        this.getPackageManager().setComponentEnabledSetting(new ComponentName(this, RemoteControlReceiver.class),
            enabled ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED : PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP);
    }

    private void publishState(String state)
    {
        if (this.m_mediaSession == null)
            return;

        PlaybackStateCompat.Builder bob = new PlaybackStateCompat.Builder();
        long actions = PlaybackStateCompat.ACTION_PAUSE |
            PlaybackStateCompat.ACTION_PLAY |
            PlaybackStateCompat.ACTION_STOP |
            PlaybackStateCompat.ACTION_SKIP_TO_NEXT |
            PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS |
            PlaybackStateCompat.ACTION_FAST_FORWARD |
            PlaybackStateCompat.ACTION_REWIND;

        bob.setActions(actions);

        if (ACTION_REMOTE_PLAY.equals(state))
            bob.setState(PlaybackStateCompat.STATE_PLAYING, -1, 1);
        else if (ACTION_REMOTE_PAUSE.equals(state))
            bob.setState(PlaybackStateCompat.STATE_PAUSED, -1, 0);
        else if (ACTION_REMOTE_STOP.equals(state))
            bob.setState(PlaybackStateCompat.STATE_STOPPED, -1, 0);

        this.m_mediaSession.setPlaybackState(bob.build());
    }

    private void updateMetaData()
    {
        if (this.m_mediaSession == null)
            return;

        Bitmap cover = BitmapFactory.decodeFile(NativeFunctions.getCoverFilePath());
        MediaMetadataCompat.Builder bob = new MediaMetadataCompat.Builder();

        bob.putString(MediaMetadataCompat.METADATA_KEY_TITLE, NativeFunctions.getTitle());
        bob.putLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER, NativeFunctions.getCurrentPlayIndex() + 1);
        bob.putLong(MediaMetadataCompat.METADATA_KEY_NUM_TRACKS, NativeFunctions.getTotalPlayListCount());
        bob.putLong(MediaMetadataCompat.METADATA_KEY_DURATION, NativeFunctions.getDuration());

        if (cover != null && cover.getConfig() != null)
            bob.putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, cover.copy(cover.getConfig(), false));

        this.m_mediaSession.setMetadata(bob.build());
    }

    private void initMediaSession()
    {
        this.unInitMediaSession();

        ComponentName mediaButtonEventReceiver = new ComponentName(this, RemoteControlReceiver.class);

        this.m_mediaSession = new MediaSessionCompat(this, "anyvod", mediaButtonEventReceiver, null);

        this.m_mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        this.m_mediaSession.setCallback(new MediaSessionCompat.Callback()
        {
            @Override
            public void onPlay()
            {
                NativeFunctions.onResumeMedia();
            }

            @Override
            public void onPause()
            {
                NativeFunctions.onPauseMedia();
            }

            @Override
            public void onStop()
            {
                NativeFunctions.onStopMedia();
            }

            @Override
            public void onSkipToNext()
            {
                NativeFunctions.onNextMedia();
            }

            @Override
            public void onSkipToPrevious()
            {
                NativeFunctions.onPreviousMedia();
            }

            @Override
            public void onFastForward()
            {
                NativeFunctions.onForwardMedia();
            }

            @Override
            public void onRewind()
            {
                NativeFunctions.onRewindMedia();
            }
        });

        try
        {
            this.m_mediaSession.setActive(true);
        }
        catch (NullPointerException e)
        {
            this.m_mediaSession.setActive(false);
            this.m_mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
            this.m_mediaSession.setActive(true);
        }
    }

    private void unInitMediaSession()
    {
        if (this.m_mediaSession != null)
        {
            this.m_mediaSession.setActive(false);
            this.m_mediaSession.release();
            this.m_mediaSession = null;
        }
    }

    public void started()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (!NativeFunctions.isValid())
                    return;

                boolean isVideo = NativeFunctions.isVideo();

                setRemoteControlReceiverEnabled(!isVideo);

                if (!isVideo)
                {
                    initMediaSession();
                    updateMetaData();
                    startPreventKillService();
                }
            }
        } );
    }

    public void playing()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                publishState(ACTION_REMOTE_PLAY);
                showNotification(true);
            }
        } );
    }

    public void paused()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                publishState(ACTION_REMOTE_PAUSE);
                showNotification(false);
            }
        } );
    }

    public void stopped()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                publishState(ACTION_REMOTE_STOP);
                unInitMediaSession();
                setRemoteControlReceiverEnabled(false);
            }
        } );
    }

    public void exit()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                hideNotification();
                stopPreventKillService();
            }
        } );
    }

    public void setKeepActive()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                setKeepInActive();
                m_wakeLock.acquire();
            }
        } );
    }

    public void setKeepInActive()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (m_wakeLock.isHeld())
                    m_wakeLock.release();
            }
        } );
    }

    public void disableIdleTimer()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        } );
    }

    public void enableIdleTimer()
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        } );
    }

    public void showToast(final String msg)
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        } );
    }

    public int getToastLength()
    {
        return 2000;
    }

    public void setOrientationLandscape()
    {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
    }

    public void setOrientationPortrait()
    {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void setOrientationRestore()
    {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    private String[] getMediaURLs(Uri uri, String field, String desc, String nameFilter)
    {
        ArrayList<String> list = new ArrayList<String>();
        String[] proj = { MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DATE_MODIFIED,
                          MediaStore.MediaColumns.MIME_TYPE, MediaStore.MediaColumns.SIZE,
                          MediaStore.MediaColumns.DISPLAY_NAME };
        String[] filterArg;
        String sortColumn;
        String filterColumn;

        if (field.equals("name"))
            sortColumn = MediaStore.MediaColumns.DISPLAY_NAME;
        else if (field.equals("time"))
            sortColumn = MediaStore.MediaColumns.DATE_MODIFIED;
        else if (field.equals("type"))
            sortColumn = MediaStore.MediaColumns.MIME_TYPE;
        else if (field.equals("size"))
            sortColumn = MediaStore.MediaColumns.SIZE;
        else
            sortColumn = MediaStore.MediaColumns.DISPLAY_NAME;

        if (nameFilter == null)
        {
            filterColumn = null;
            filterArg = null;
        }
        else
        {
            filterColumn = MediaStore.MediaColumns.DISPLAY_NAME + " like ?";
            filterArg = new String[] { "%" + nameFilter + "%" };
        }

        Cursor cursor = this.getApplicationContext().getContentResolver().query(uri, proj, filterColumn, filterArg, sortColumn + " " + desc);

        if (cursor != null && cursor.moveToFirst())
        {
            int colIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);

            do
            {
                String path = cursor.getString(colIndex);

                list.add(path);
            }
            while (cursor.moveToNext());

            cursor.close();
        }

        return list.toArray(new String[list.size()]);
    }

    public String[] getVideoURLs(String field, String desc, String nameFilter)
    {
        return this.getMediaURLs(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, field, desc, nameFilter);
    }

    public String[] getAudioURLs(String field, String desc, String nameFilter)
    {
        return this.getMediaURLs(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, field, desc, nameFilter);
    }

    public void setStatusBarStyle(final boolean enable, final int color)
    {
        this.runOnUiThread(new Runnable()
        {
            public void run()
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    Window window = getWindow();

                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

                    if (enable)
                        window.setStatusBarColor(Color.rgb((color & 0xff0000) >> 16, (color & 0x00ff00) >> 8, (color & 0x0000ff) >> 0));
                    else
                        window.setStatusBarColor(Color.BLACK);
                }
            }
        } );
    }
}

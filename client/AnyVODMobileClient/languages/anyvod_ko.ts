﻿<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>ComboItem</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ComboItem.qml" line="210"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>ContextMenuTextInput</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ContextMenuTextInput.qml" line="112"/>
        <source>잘라내기</source>
        <translation>잘라내기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ContextMenuTextInput.qml" line="127"/>
        <source>복사하기</source>
        <translation>복사하기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ContextMenuTextInput.qml" line="141"/>
        <source>붙여넣기</source>
        <translation>붙여넣기</translation>
    </message>
</context>
<context>
    <name>DTVChannelList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="158"/>
        <source>채널 </source>
        <translation>채널 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="158"/>
        <source>, 신호 강도 </source>
        <translation>, 신호 강도 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="162"/>
        <source>이름 없음</source>
        <translation>이름 없음</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVChannelList.qml" line="202"/>
        <source>채널 목록이 없습니다.</source>
        <translation>채널 목록이 없습니다.</translation>
    </message>
</context>
<context>
    <name>DTVSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVSettingItems.qml" line="23"/>
        <source>채널 목록</source>
        <translation>채널 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVSettingItems.qml" line="23"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DTVSettingItems.qml" line="24"/>
        <source>채널 검색</source>
        <translation>채널 검색</translation>
    </message>
</context>
<context>
    <name>DirItemMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/DirItemMenuItems.qml" line="24"/>
        <source>전체 재생</source>
        <translation>전체 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/DirItemMenuItems.qml" line="29"/>
        <source>삭제</source>
        <translation>삭제</translation>
    </message>
</context>
<context>
    <name>Equalizer</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="59"/>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="173"/>
        <source>프리셋</source>
        <translation>프리셋</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="203"/>
        <source>초기화</source>
        <translation>초기화</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Equalizer.qml" line="229"/>
        <source>이퀄라이저 사용</source>
        <translation>이퀄라이저 사용</translation>
    </message>
</context>
<context>
    <name>FileItemMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileItemMenuItems.qml" line="24"/>
        <source>삭제</source>
        <translation>삭제</translation>
    </message>
</context>
<context>
    <name>FileListModel</name>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="807"/>
        <source>기본 값</source>
        <translation>기본 값</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="808"/>
        <source>클래식</source>
        <translation>클래식</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="809"/>
        <source>베이스</source>
        <translation>베이스</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="810"/>
        <source>베이스 &amp; 트레블</source>
        <translation>베이스 &amp; 트레블</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="811"/>
        <source>트레블</source>
        <translation>트레블</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="812"/>
        <source>헤드폰</source>
        <translation>헤드폰</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="813"/>
        <source>홀</source>
        <translation>홀</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="814"/>
        <source>소프트 락</source>
        <translation>소프트 락</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="815"/>
        <source>클럽</source>
        <translation>클럽</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="816"/>
        <source>댄스</source>
        <translation>댄스</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="817"/>
        <source>라이브</source>
        <translation>라이브</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="818"/>
        <source>파티</source>
        <translation>파티</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="819"/>
        <source>팝</source>
        <translation>팝</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="820"/>
        <source>레게</source>
        <translation>레게</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="821"/>
        <source>락</source>
        <translation>락</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="822"/>
        <source>스카</source>
        <translation>스카</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="823"/>
        <source>소프트</source>
        <translation>소프트</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="824"/>
        <source>테크노</source>
        <translation>테크노</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="825"/>
        <source>보컬</source>
        <translation>보컬</translation>
    </message>
    <message>
        <location filename="../src/models/FileListModel.cpp" line="826"/>
        <source>재즈</source>
        <translation>재즈</translation>
    </message>
</context>
<context>
    <name>FileSystemDialog</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="255"/>
        <source>내장 메모리</source>
        <translation>내장 메모리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="292"/>
        <source>외장 메모리</source>
        <translation>외장 메모리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="424"/>
        <source>%1 항목</source>
        <translation>%1 항목</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="424"/>
        <source>갱신 중...</source>
        <translation>갱신 중..</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="485"/>
        <source>파일 또는 디렉토리가 없습니다</source>
        <translation>파일 또는 디렉토리가 없습니다</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="517"/>
        <source>열기</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/FileSystemDialog.qml" line="534"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>GLRenderer</name>
    <message>
        <location filename="../src/video/GLRenderer.cpp" line="352"/>
        <source>캡쳐 성공</source>
        <translation>캡쳐 성공</translation>
    </message>
    <message>
        <location filename="../src/video/GLRenderer.cpp" line="354"/>
        <source>캡쳐 실패</source>
        <translation>캡쳐 실패</translation>
    </message>
    <message>
        <location filename="../src/video/GLRenderer.cpp" line="468"/>
        <source>캡쳐를 시작하지 못했습니다</source>
        <translation>캡쳐를 시작하지 못했습니다</translation>
    </message>
</context>
<context>
    <name>InfoSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/InfoSettingItems.qml" line="23"/>
        <source>재생 정보 보기</source>
        <translation>재생 정보 보기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/InfoSettingItems.qml" line="23"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/InfoSettingItems.qml" line="24"/>
        <source>AnyVOD 정보</source>
        <translation>AnyVOD 정보</translation>
    </message>
</context>
<context>
    <name>Initial</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="37"/>
        <source>캡처 확장자</source>
        <translation>캡처 확장자</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="57"/>
        <source>글꼴</source>
        <translation>글꼴</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="87"/>
        <source>디인터레이스 활성화 기준</source>
        <translation>디인터레이스 활성화 기준</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="91"/>
        <source>자동</source>
        <translation>자동</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="92"/>
        <source>사용</source>
        <translation>사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="93"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="264"/>
        <source>사용 안 함</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="104"/>
        <source>디인터레이스 알고리즘</source>
        <translation>디인터레이스 알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="108"/>
        <source>Blend</source>
        <translation>Blend</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="109"/>
        <source>BOB</source>
        <translation>BOB</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="110"/>
        <source>YADIF</source>
        <translation>YADIF</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="111"/>
        <source>YADIF(BOB)</source>
        <translation>YADIF(BOB)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="112"/>
        <source>Weston 3 Field</source>
        <translation>Weston 3 Field</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="113"/>
        <source>Adaptive Kernel</source>
        <translation>Adaptive Kernel</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="114"/>
        <source>Motion Compensation</source>
        <translation>Motion Compensation</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="115"/>
        <source>Bob Weaver</source>
        <translation>Bob Weaver</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="116"/>
        <source>Bob Weaver(BOB)</source>
        <translation>Bob Weaver(BOB)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="127"/>
        <source>자막 / 가사 가로 정렬</source>
        <translation>자막 / 가사 가로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="131"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="150"/>
        <source>기본 정렬</source>
        <translation>기본 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="132"/>
        <source>자동 정렬</source>
        <translation>자동 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="133"/>
        <source>왼쪽 정렬</source>
        <translation>왼쪽 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="134"/>
        <source>가운데 정렬</source>
        <translation>가운데 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="135"/>
        <source>오른쪽 정렬</source>
        <translation>오른쪽 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="146"/>
        <source>자막 / 가사 세로 정렬</source>
        <translation>자막 / 가사 세로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="151"/>
        <source>상단 정렬</source>
        <translation>상단 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="152"/>
        <source>하단 정렬</source>
        <translation>하단 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="161"/>
        <source>자막 / 가사 텍스트 인코딩</source>
        <translation>자막 / 가사 텍스트 인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="181"/>
        <source>Languages</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="208"/>
        <source>오디오 장치</source>
        <translation>오디오 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="215"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="239"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="232"/>
        <source>출력 장치</source>
        <translation>출력 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="260"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="265"/>
        <source>AC3</source>
        <translation>AC3</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="266"/>
        <source>DTS</source>
        <translation>DTS</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="277"/>
        <source>샘플링 속도</source>
        <translation>샘플링 속도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="297"/>
        <source>이퀄라이저</source>
        <translation>이퀄라이저</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="307"/>
        <source>외부 열기</source>
        <translation>외부 열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="321"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="331"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="361"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="322"/>
        <source>지원하지 않는 기능입니다.</source>
        <translation>지원하지 않는 기능입니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="332"/>
        <source>파일을 열 수 없습니다.</source>
        <translation>파일을 열 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="341"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="351"/>
        <source>오류</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="342"/>
        <source>파일을 삭제 할 수 없습니다.</source>
        <translation>파일을 삭제 할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="352"/>
        <source>디렉토리를 삭제 할 수 없습니다.</source>
        <translation>디렉토리를 삭제 할 수 없습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="362"/>
        <source>변경된 글꼴로 적용하기 위해서는 앱을 재 시작 해주세요.</source>
        <translation>변경된 글꼴로 적용하기 위해서는 앱을 재 시작 해주세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="371"/>
        <source>Info</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="372"/>
        <source>Restart the app to apply changes.</source>
        <translation>변경 사항을 적용하기 위해서 앱을 재 시작 해주세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="381"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="396"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="417"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="382"/>
        <source>종료 하시겠습니까?</source>
        <translation>종료 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="397"/>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="418"/>
        <source>삭제 하시겠습니까?</source>
        <translation>삭제 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="437"/>
        <source>AnyVOD 정보</source>
        <translation>AnyVOD 정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="448"/>
        <source>라이센스</source>
        <translation>라이센스</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="459"/>
        <source>저장 디렉토리 설정</source>
        <translation>저장 디렉토리 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="473"/>
        <source>설정</source>
        <translation>설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="559"/>
        <source>채널 검색</source>
        <translation>채널 검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="567"/>
        <source>채널 목록</source>
        <translation>채널 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="583"/>
        <source>DTV 열기</source>
        <translation>DTV 열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="610"/>
        <source>재생 목록</source>
        <translation>재생 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="622"/>
        <source>검색</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="959"/>
        <source>내장메모리</source>
        <translation>내장메모리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="995"/>
        <source>외장메모리</source>
        <translation>외장메모리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1031"/>
        <source>동영상</source>
        <translation>동영상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1067"/>
        <source>음악</source>
        <translation>음악</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1211"/>
        <source>%1 항목</source>
        <translation>%1 항목</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1211"/>
        <source>갱신 중...</source>
        <translation>갱신 중..</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1295"/>
        <source>파일이 없습니다</source>
        <translation>파일이 없습니다</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/Initial.qml" line="1706"/>
        <source>뒤로 버튼을 한 번 더 누르시면 종료합니다.</source>
        <translation>뒤로 버튼을 한 번 더 누르시면 종료합니다.</translation>
    </message>
</context>
<context>
    <name>MainMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="25"/>
        <source>검색</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="31"/>
        <source>재생 목록</source>
        <translation>재생 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="49"/>
        <source>이퀄라이저</source>
        <translation>이퀄라이저</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="37"/>
        <source>외부 열기</source>
        <translation>외부 열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="43"/>
        <source>DTV 열기</source>
        <translation>DTV 열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainMenuItems.qml" line="55"/>
        <source>설정</source>
        <translation>설정</translation>
    </message>
</context>
<context>
    <name>MainSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="23"/>
        <source>AnyVOD 정보</source>
        <translation>AnyVOD 정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="24"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="26"/>
        <source>일반</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="24"/>
        <source>라이센스</source>
        <translation>라이센스</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="25"/>
        <source>Languages</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="26"/>
        <source>글꼴</source>
        <translation>글꼴</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="27"/>
        <source>하드웨어 디코더 사용</source>
        <translation>하드웨어 디코더 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="28"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="29"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="30"/>
        <source>화면</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="28"/>
        <source>저화질 모드 사용</source>
        <translation>저화질 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="29"/>
        <source>프레임 드랍 사용</source>
        <translation>프레임 드랍 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="30"/>
        <source>앨범 자켓 보기</source>
        <translation>앨범 자켓 보기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="31"/>
        <source>확장자</source>
        <translation>확장자</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="32"/>
        <source>캡처</source>
        <translation>캡처</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="32"/>
        <source>저장 디렉토리 설정</source>
        <translation>저장 디렉토리 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="33"/>
        <source>활성화 기준</source>
        <translation>활성화 기준</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="33"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="34"/>
        <source>디인터레이스</source>
        <translation>디인터레이스</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="34"/>
        <source>알고리즘</source>
        <translation>알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="35"/>
        <source>재생 위치 기억</source>
        <translation>재생 위치 기억</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="35"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="36"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="37"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="36"/>
        <source>키프레임 단위 이동</source>
        <translation>키프레임 단위 이동</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="37"/>
        <source>버퍼링 모드 사용</source>
        <translation>버퍼링 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="38"/>
        <source>보이기</source>
        <translation>보이기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="38"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="39"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="40"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="41"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="42"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="43"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="44"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="45"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="46"/>
        <source>자막 / 가사</source>
        <translation>자막 / 가사</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="39"/>
        <source>고급 검색</source>
        <translation>고급 검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="40"/>
        <source>자막 캐시 모드 사용</source>
        <translation>자막 캐시 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="41"/>
        <source>가로 정렬</source>
        <translation>가로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="42"/>
        <source>세로 정렬</source>
        <translation>세로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="43"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="51"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="44"/>
        <source>자막 찾기</source>
        <translation>자막 찾기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="45"/>
        <source>가사 찾기</source>
        <translation>가사 찾기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="46"/>
        <source>미디어 삭제 시 자막/가사 삭제</source>
        <translation>미디어 삭제 시 자막/가사 삭제</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="47"/>
        <source>오디오 장치</source>
        <translation>오디오 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="48"/>
        <source>노멀라이저 사용</source>
        <translation>노멀라이저 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="50"/>
        <source>출력 장치</source>
        <translation>출력 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="50"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="51"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="52"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="53"/>
        <source>S/PDIF</source>
        <translation>S/PDIF</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="52"/>
        <source>샘플링 속도</source>
        <translation>샘플링 속도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="53"/>
        <source>사용 하기</source>
        <translation>사용 하기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="47"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="48"/>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="49"/>
        <source>소리</source>
        <translation>소리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MainSettingItems.qml" line="49"/>
        <source>이퀄라이저 사용</source>
        <translation>이퀄라이저 사용</translation>
    </message>
</context>
<context>
    <name>ManagePlayList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="74"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="75"/>
        <source>삭제 하시겠습니까?</source>
        <translation>삭제 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="90"/>
        <source>삭제</source>
        <translation>삭제</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ManagePlayList.qml" line="265"/>
        <source>재생 목록이 없습니다.</source>
        <translation>재생 목록이 없습니다.</translation>
    </message>
</context>
<context>
    <name>MediaPresenter</name>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2718"/>
        <source>가사 있음</source>
        <translation>가사 있음</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2720"/>
        <source>자막 있음</source>
        <translation>자막 있음</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2725"/>
        <source>가사 없음</source>
        <translation>가사 없음</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2727"/>
        <source>자막 없음</source>
        <translation>자막 없음</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2733"/>
        <source>파일 이름 : </source>
        <translation>파일 이름 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2746"/>
        <source>재생 위치 : </source>
        <translation>재생 위치 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2762"/>
        <source>비디오 코덱 : </source>
        <translation>비디오 코덱 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2773"/>
        <source>하드웨어 디코더 : </source>
        <translation>하드웨어 디코더 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2781"/>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2916"/>
        <source>입력 : </source>
        <translation>입력 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2796"/>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2930"/>
        <source>출력 : </source>
        <translation>출력 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2817"/>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2828"/>
        <source>프레임 : </source>
        <translation>프레임 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2854"/>
        <source>DTV 신호 감도 : </source>
        <translation>DTV 신호 감도 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2862"/>
        <source>CPU 사용률 : </source>
        <translation>CPU 사용률 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2872"/>
        <source>오디오 코덱 : </source>
        <translation>오디오 코덱 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2883"/>
        <source>S/PDIF 오디오 장치 : </source>
        <translation>S/PDIF 오디오 장치 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2890"/>
        <source>인코딩 사용</source>
        <translation>인코딩 사용</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2948"/>
        <source>가사 코덱 : </source>
        <translation>가사 코덱 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="2950"/>
        <source>자막 코덱 : </source>
        <translation>자막 코덱 : </translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="5302"/>
        <source>프레임 저하가 일어나고 있습니다. 성능에 영향을 미치는 옵션 또는 수직 동기화를 꺼주세요.</source>
        <translation>프레임 저하가 일어나고 있습니다. 성능에 영향을 미치는 옵션 또는 수직 동기화를 꺼주세요.</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="6288"/>
        <source>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="7811"/>
        <source>오프닝 스킵 : %1</source>
        <translation>오프닝 스킵 : %1</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="7832"/>
        <source>재생 스킵 : %1 ~ %2</source>
        <translation>재생 스킵 : %1 ~ %2</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="7940"/>
        <source>구간 반복 : %1 ~ %2</source>
        <translation>구간 반복 : %1 ~ %2</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/media/MediaPresenter.cpp" line="7948"/>
        <source>S/PDIF 출력을 지원하지 않은 포맷이므로 PCM 출력으로 전환합니다</source>
        <translation>S/PDIF 출력을 지원하지 않은 포맷이므로 PCM 출력으로 전환합니다</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/MessageBox.qml" line="149"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/MessageBox.qml" line="165"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>OpenExternal</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/OpenExternal.qml" line="128"/>
        <source>외부 주소를 입력 하세요.
예) http://somehost.com/movie.mp4</source>
        <translation>외부 주소를 입력 하세요.
예) http://somehost.com/movie.mp4</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/OpenExternal.qml" line="170"/>
        <source>열기</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/OpenExternal.qml" line="187"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>PlayList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="89"/>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="99"/>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="135"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="90"/>
        <source>지원하지 않는 기능입니다.</source>
        <translation>지원하지 않는 기능입니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="100"/>
        <source>클립보드로 경로가 복사 되었습니다.</source>
        <translation>클립보드로 경로가 복사 되었습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="119"/>
        <source>질문</source>
        <translation>질문</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="109"/>
        <source>오류</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="110"/>
        <source>이름을 입력 해 주세요.</source>
        <translation>이름을 입력 해 주세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="120"/>
        <source>이미 동일한 재생 목록이 존재합니다.
갱신 하시겠습니까?</source>
        <translation>이미 동일한 재생 목록이 존재합니다.
갱신 하시겠습니까?</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="147"/>
        <source>화질 목록</source>
        <translation>화질 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="174"/>
        <source>재생 목록 신규 저장</source>
        <translation>재생 목록 신규 저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="288"/>
        <source>재생 목록</source>
        <translation>재생 목록</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="423"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayList.qml" line="437"/>
        <source>신규 저장</source>
        <translation>신규 저장</translation>
    </message>
</context>
<context>
    <name>PlayListMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="24"/>
        <source>경로 보기</source>
        <translation>경로 보기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="29"/>
        <source>경로 복사</source>
        <translation>경로 복사</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlayListMenuItems.qml" line="34"/>
        <source>다른 화질</source>
        <translation>다른 화질</translation>
    </message>
</context>
<context>
    <name>PlaySettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="23"/>
        <source>재생 위치 기억</source>
        <translation>재생 위치 기억</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="24"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="28"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="24"/>
        <source>키프레임 단위 이동</source>
        <translation>키프레임 단위 이동</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="25"/>
        <source>버퍼링 모드 사용</source>
        <translation>버퍼링 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="26"/>
        <source>재생 속도 설정</source>
        <translation>재생 속도 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="27"/>
        <source>재생 순서 설정</source>
        <translation>재생 순서 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/PlaySettingItems.qml" line="28"/>
        <source>구간 반복 설정</source>
        <translation>구간 반복 설정</translation>
    </message>
</context>
<context>
    <name>RenderScreen</name>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="390"/>
        <source>자막 있음</source>
        <translation>자막 있음</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="392"/>
        <source>자막 없음</source>
        <translation>자막 없음</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="397"/>
        <source>가사 있음</source>
        <translation>가사 있음</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="399"/>
        <source>가사 없음</source>
        <translation>가사 없음</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="432"/>
        <source>일시정지</source>
        <translation>일시정지</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="449"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="503"/>
        <source>이전으로 %1 프레임 이동</source>
        <translation>이전으로 %1 프레임 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="518"/>
        <source>다음으로 %1 프레임 이동</source>
        <translation>다음으로 %1 프레임 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="552"/>
        <source>앞으로 %1초 키프레임 이동</source>
        <translation>앞으로 %1초 키프레임 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="554"/>
        <source>뒤로 %1초 키프레임 이동</source>
        <translation>뒤로 %1초 키프레임 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="559"/>
        <source>앞으로 %1초 이동</source>
        <translation>앞으로 %1초 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="561"/>
        <source>뒤로 %1초 이동</source>
        <translation>뒤로 %1초 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1064"/>
        <source>자막 열기 : %1</source>
        <translation>자막 열기 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1066"/>
        <source>가사 열기 : %1</source>
        <translation>가사 열기 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1084"/>
        <source>자막이 저장 되었습니다 : %1</source>
        <translation>자막이 저장 되었습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1086"/>
        <source>가사가 저장 되었습니다 : %1</source>
        <translation>가사가 저장 되었습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1091"/>
        <source>자막이 저장 되지 않았습니다 : %1</source>
        <translation>자막이 저장 되지 않았습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1093"/>
        <source>가사가 저장 되지 않았습니다 : %1</source>
        <translation>가사가 저장 되지 않았습니다 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1111"/>
        <source>외부 자막 닫기</source>
        <translation>외부 자막 닫기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1113"/>
        <source>외부 가사 닫기</source>
        <translation>외부 가사 닫기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1291"/>
        <source>자막 보이기</source>
        <translation>자막 보이기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1293"/>
        <source>자막 숨기기</source>
        <translation>자막 숨기기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1298"/>
        <source>가사 보이기</source>
        <translation>가사 보이기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1300"/>
        <source>가사 숨기기</source>
        <translation>가사 숨기기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1320"/>
        <source>고급 자막 검색 사용</source>
        <translation>고급 자막 검색 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1322"/>
        <source>고급 자막 검색 사용 안 함</source>
        <translation>고급 자막 검색 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1327"/>
        <source>고급 가사 검색 사용</source>
        <translation>고급 가사 검색 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1329"/>
        <source>고급 가사 검색 사용 안 함</source>
        <translation>고급 가사 검색 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1386"/>
        <source>자막 언어 변경</source>
        <translation>자막 언어 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1388"/>
        <source>가사 언어 변경</source>
        <translation>가사 언어 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1416"/>
        <source>자막 위치 초기화</source>
        <translation>자막 위치 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1427"/>
        <source>자막 위치 위로 : %1</source>
        <translation>자막 위치 위로 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1429"/>
        <source>자막 위치 아래로 : %1</source>
        <translation>자막 위치 아래로 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1441"/>
        <source>자막 위치 왼쪽으로 : %1</source>
        <translation>자막 위치 왼쪽으로 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1443"/>
        <source>자막 위치 오른쪽으로 : %1</source>
        <translation>자막 위치 오른쪽으로 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1454"/>
        <source>세로 자막 위치 : %1</source>
        <translation>세로 자막 위치 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1465"/>
        <source>가로 자막 위치 : %1</source>
        <translation>가로 자막 위치 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1485"/>
        <source>3D 자막 위치 초기화</source>
        <translation>3D 자막 위치 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1493"/>
        <source>3D 자막 위치 가깝게(세로) : %1</source>
        <translation>3D 자막 위치 가깝게(세로) : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1492"/>
        <source>3D 자막 위치 멀게(세로) : %1</source>
        <translation>3D 자막 위치 멀게(세로) : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1519"/>
        <source>3D 자막 위치 가깝게(가로) : %1</source>
        <translation>3D 자막 위치 가깝게(가로) : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1518"/>
        <source>3D 자막 위치 멀게(가로) : %1</source>
        <translation>3D 자막 위치 멀게(가로) : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1547"/>
        <source>세로 3D 자막 위치 : %1</source>
        <translation>세로 3D 자막 위치 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1558"/>
        <source>가로 3D 자막 위치 : %1</source>
        <translation>가로 3D 자막 위치 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1580"/>
        <source>구간 반복 시작 : %1</source>
        <translation>구간 반복 시작 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1593"/>
        <source>구간 반복 끝 : %1</source>
        <translation>구간 반복 끝 : %1</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1609"/>
        <source>구간 반복 활성화</source>
        <translation>구간 반복 활성화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1611"/>
        <source>구간 반복 비활성화</source>
        <translation>구간 반복 비활성화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1617"/>
        <source>시작과 끝 시각이 같으므로 활성화 되지 않습니다</source>
        <translation>시작과 끝 시각이 같으므로 활성화 되지 않습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1655"/>
        <source>구간 반복 시작 위치 %1초 뒤로 이동 (%2)</source>
        <translation>구간 반복 시작 위치 %1초 뒤로 이동 (%2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1657"/>
        <source>구간 반복 시작 위치 %1초 앞으로 이동 (%2)</source>
        <translation>구간 반복 시작 위치 %1초 앞으로 이동 (%2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1663"/>
        <source>구간 반복 시작 위치가 범위를 벗어났습니다</source>
        <translation>구간 반복 시작 위치가 범위를 벗어났습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1668"/>
        <location filename="../src/ui/RenderScreen.cpp" line="1702"/>
        <location filename="../src/ui/RenderScreen.cpp" line="1741"/>
        <source>구간 반복이 설정 되지 않았습니다</source>
        <translation>구간 반복이 설정 되지 않았습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1689"/>
        <source>구간 반복 끝 위치 %1초 뒤로 이동 (%2)</source>
        <translation>구간 반복 끝 위치 %1초 뒤로 이동 (%2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1691"/>
        <source>구간 반복 끝 위치 %1초 앞으로 이동 (%2)</source>
        <translation>구간 반복 끝 위치 %1초 앞으로 이동 (%2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1697"/>
        <source>구간 반복 끝 위치가 범위를 벗어났습니다</source>
        <translation>구간 반복 끝 위치가 범위를 벗어났습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1727"/>
        <source>구간 반복 위치 %1초 뒤로 이동 (%2 ~ %3)</source>
        <translation>구간 반복 위치 %1초 뒤로 이동 (%2 ~ %3)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1729"/>
        <source>구간 반복 위치 %1초 앞으로 이동 (%2 ~ %3)</source>
        <translation>구간 반복 위치 %1초 앞으로 이동 (%2 ~ %3)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1736"/>
        <source>구간 반복 위치가 범위를 벗어났습니다</source>
        <translation>구간 반복 위치가 범위를 벗어났습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1752"/>
        <source>키프레임 단위로 이동 함</source>
        <translation>키프레임 단위로 이동 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1754"/>
        <source>키프레임 단위로 이동 안 함</source>
        <translation>키프레임 단위로 이동 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1797"/>
        <source>3D 영상 (%1)</source>
        <translation>3D 영상 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1810"/>
        <source>3D 자막 (%1)</source>
        <translation>3D 자막 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1823"/>
        <source>VR 입력 영상 (%1)</source>
        <translation>VR 입력 영상 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1840"/>
        <source>VR 헤드 트래킹 사용 함</source>
        <translation>VR 헤드 트래킹 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1842"/>
        <source>VR 헤드 트래킹 사용 안 함</source>
        <translation>VR 헤드 트래킹 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1861"/>
        <source>VR 왜곡 보정 사용 함</source>
        <translation>VR 왜곡 보정 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1863"/>
        <source>VR 왜곡 보정 사용 안 함</source>
        <translation>VR 왜곡 보정 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1875"/>
        <source>VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)</source>
        <translation>VR 왜곡 보정 계수 설정 (k1 : %1, k2 : %2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1891"/>
        <source>VR 렌즈 센터 설정 (X : %1, Y : %2)</source>
        <translation>VR 렌즈 센터 설정 (X : %1, Y : %2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1927"/>
        <source>오프닝 스킵 사용 함</source>
        <translation>오프닝 스킵 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1929"/>
        <source>오프닝 스킵 사용 안 함</source>
        <translation>오프닝 스킵 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1951"/>
        <source>엔딩 스킵 사용 함</source>
        <translation>엔딩 스킵 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1953"/>
        <source>엔딩 스킵 사용 안 함</source>
        <translation>엔딩 스킵 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1971"/>
        <source>재생 스킵 사용 함</source>
        <translation>재생 스킵 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1973"/>
        <source>재생 스킵 사용 안 함</source>
        <translation>재생 스킵 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1991"/>
        <source>노멀라이저 켜짐</source>
        <translation>노멀라이저 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="1993"/>
        <source>노멀라이저 꺼짐</source>
        <translation>노멀라이저 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2010"/>
        <source>이퀄라이저 켜짐</source>
        <translation>이퀄라이저 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2012"/>
        <source>이퀄라이저 꺼짐</source>
        <translation>이퀄라이저 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2029"/>
        <source>음악 줄임 켜짐</source>
        <translation>음악 줄임 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2031"/>
        <source>음악 줄임 꺼짐</source>
        <translation>음악 줄임 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2053"/>
        <source>자막 투명도</source>
        <translation>자막 투명도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2055"/>
        <source>가사 투명도</source>
        <translation>가사 투명도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2082"/>
        <source>자막 투명도 초기화</source>
        <translation>자막 투명도 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2084"/>
        <source>가사 투명도 초기화</source>
        <translation>가사 투명도 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2107"/>
        <source>자막 크기</source>
        <translation>자막 크기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2120"/>
        <source>자막 크기 초기화</source>
        <translation>자막 크기 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2160"/>
        <source>하드웨어 디코더 사용 함</source>
        <translation>하드웨어 디코더 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2163"/>
        <source> (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)</source>
        <translation> (그래픽 카드 또는 코덱이 지원하지 않을 경우 활성화가 안 될 수 있습니다)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2167"/>
        <source>하드웨어 디코더 사용 안 함</source>
        <translation>하드웨어 디코더 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2185"/>
        <source>저화질 모드 사용 함</source>
        <translation>저화질 모드 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2187"/>
        <source>저화질 모드 사용 안 함</source>
        <translation>저화질 모드 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2204"/>
        <source>프레임 드랍 사용 함</source>
        <translation>프레임 드랍 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2206"/>
        <source>프레임 드랍 사용 안 함</source>
        <translation>프레임 드랍 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2223"/>
        <source>버퍼링 모드 사용 함</source>
        <translation>버퍼링 모드 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2225"/>
        <source>버퍼링 모드 사용 안 함</source>
        <translation>버퍼링 모드 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2243"/>
        <source>3D 전체 해상도 사용</source>
        <translation>3D 전체 해상도 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2245"/>
        <source>3D 전체 해상도 사용 안 함</source>
        <translation>3D 전체 해상도 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2262"/>
        <source>음성 줄임 켜짐</source>
        <translation>음성 줄임 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2264"/>
        <source>음성 줄임 꺼짐</source>
        <translation>음성 줄임 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2281"/>
        <source>음성 강조 켜짐</source>
        <translation>음성 강조 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2283"/>
        <source>음성 강조 꺼짐</source>
        <translation>음성 강조 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2554"/>
        <source>재생 속도 초기화</source>
        <translation>재생 속도 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2558"/>
        <source>재생 속도 : %1배</source>
        <translation>재생 속도 : %1배</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2571"/>
        <source>화면 비율 사용 함</source>
        <translation>화면 비율 사용 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2574"/>
        <source> (화면 채우기)</source>
        <translation> (화면 채우기)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2580"/>
        <source>화면 비율 사용 안 함</source>
        <translation>화면 비율 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2655"/>
        <source>음성 변경 (%1)</source>
        <translation>음성 변경 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2674"/>
        <source>자동 판단</source>
        <translation>자동 판단</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2677"/>
        <source>항상 사용</source>
        <translation>항상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2680"/>
        <location filename="../src/ui/RenderScreen.cpp" line="4030"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5103"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5194"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5226"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5524"/>
        <source>사용 안 함</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2686"/>
        <source>디인터레이스 (%1)</source>
        <translation>디인터레이스 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2694"/>
        <source>디인터레이스 알고리즘 (%1)</source>
        <translation>디인터레이스 알고리즘 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2713"/>
        <source>자막 가로 정렬 변경</source>
        <translation>자막 가로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2715"/>
        <source>가사 가로 정렬 변경</source>
        <translation>가사 가로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2720"/>
        <source>자동 정렬</source>
        <translation>자동 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2723"/>
        <source>왼쪽 정렬</source>
        <translation>왼쪽 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2726"/>
        <source>오른쪽 정렬</source>
        <translation>오른쪽 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2729"/>
        <source>가운데 정렬</source>
        <translation>가운데 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2732"/>
        <location filename="../src/ui/RenderScreen.cpp" line="2768"/>
        <source>기본 정렬</source>
        <translation>기본 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2755"/>
        <source>자막 세로 정렬 변경</source>
        <translation>자막 세로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2757"/>
        <source>가사 세로 정렬 변경</source>
        <translation>가사 세로 정렬 변경</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2762"/>
        <source>상단 정렬</source>
        <translation>상단 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2765"/>
        <source>하단 정렬</source>
        <translation>하단 정렬</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2803"/>
        <source>자막 싱크 초기화</source>
        <translation>자막 싱크 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2805"/>
        <source>가사 싱크 초기화</source>
        <translation>가사 싱크 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2823"/>
        <location filename="../src/ui/RenderScreen.cpp" line="4937"/>
        <source>자막 싱크 %1초 빠르게</source>
        <translation>자막 싱크 %1초 빠르게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2825"/>
        <location filename="../src/ui/RenderScreen.cpp" line="4939"/>
        <source>자막 싱크 %1초 느리게</source>
        <translation>자막 싱크 %1초 느리게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2830"/>
        <location filename="../src/ui/RenderScreen.cpp" line="4944"/>
        <source>가사 싱크 %1초 빠르게</source>
        <translation>가사 싱크 %1초 빠르게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2832"/>
        <location filename="../src/ui/RenderScreen.cpp" line="4946"/>
        <source>가사 싱크 %1초 느리게</source>
        <translation>가사 싱크 %1초 느리게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2838"/>
        <location filename="../src/ui/RenderScreen.cpp" line="2916"/>
        <source> (%1초)</source>
        <translation> (%1초)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2857"/>
        <source>소리 싱크 초기화</source>
        <translation>소리 싱크 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2878"/>
        <source>자막 찾기 켜짐</source>
        <translation>자막 찾기 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2880"/>
        <source>자막 찾기 꺼짐</source>
        <translation>자막 찾기 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2891"/>
        <source>가사 찾기 켜짐</source>
        <translation>가사 찾기 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2893"/>
        <source>가사 찾기 꺼짐</source>
        <translation>가사 찾기 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2909"/>
        <location filename="../src/ui/RenderScreen.cpp" line="4966"/>
        <source>소리 싱크 %1초 빠르게</source>
        <translation>소리 싱크 %1초 빠르게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2911"/>
        <location filename="../src/ui/RenderScreen.cpp" line="4968"/>
        <source>소리 싱크 %1초 느리게</source>
        <translation>소리 싱크 %1초 느리게</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2941"/>
        <source>소리 (%1%)</source>
        <translation>소리 (%1%)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2953"/>
        <source>소리 꺼짐</source>
        <translation>소리 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="2955"/>
        <source>소리 켜짐</source>
        <translation>소리 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3043"/>
        <source>앨범 자켓 보이기</source>
        <translation>앨범 자켓 보이기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3045"/>
        <source>앨범 자켓 숨기기</source>
        <translation>앨범 자켓 숨기기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3084"/>
        <source>재생 위치 기억 켜짐</source>
        <translation>재생 위치 기억 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3086"/>
        <source>재생 위치 기억 꺼짐</source>
        <translation>재생 위치 기억 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3236"/>
        <source>처음으로 이동</source>
        <translation>처음으로 이동</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3312"/>
        <source>자막을 변경 할 수 없습니다</source>
        <translation>자막을 변경 할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3314"/>
        <source>가사를 변경 할 수 없습니다</source>
        <translation>가사를 변경 할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3326"/>
        <source>음성을 변경 할 수 없습니다</source>
        <translation>음성을 변경 할 수 없습니다</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3628"/>
        <source>영상 속성 초기화</source>
        <translation>영상 속성 초기화</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3657"/>
        <source>영상 밝기 %1배</source>
        <translation>영상 밝기 %1배</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3686"/>
        <source>영상 채도 %1배</source>
        <translation>영상 채도 %1배</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3718"/>
        <source>영상 색상 각도 : %1°</source>
        <translation>영상 색상 각도 : %1°</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3747"/>
        <source>영상 대비 %1배</source>
        <translation>영상 대비 %1배</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3779"/>
        <source>영상 날카롭게 켜짐</source>
        <translation>영상 날카롭게 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3781"/>
        <source>영상 날카롭게 꺼짐</source>
        <translation>영상 날카롭게 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3800"/>
        <source>영상 선명하게 켜짐</source>
        <translation>영상 선명하게 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3802"/>
        <source>영상 선명하게 꺼짐</source>
        <translation>영상 선명하게 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3821"/>
        <source>영상 부드럽게 켜짐</source>
        <translation>영상 부드럽게 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3823"/>
        <source>영상 부드럽게 꺼짐</source>
        <translation>영상 부드럽게 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3842"/>
        <source>영상 좌우 반전 켜짐</source>
        <translation>영상 좌우 반전 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3844"/>
        <source>영상 좌우 반전 꺼짐</source>
        <translation>영상 좌우 반전 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3863"/>
        <source>영상 상하 반전 켜짐</source>
        <translation>영상 상하 반전 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="3865"/>
        <source>영상 상하 반전 꺼짐</source>
        <translation>영상 상하 반전 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4032"/>
        <source>화면 채우기</source>
        <translation>화면 채우기</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4034"/>
        <source>사용자 지정 (%1:%2)</source>
        <translation>사용자 지정 (%1:%2)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4082"/>
        <source>이전 챕터로 이동 (%1)</source>
        <translation>이전 챕터로 이동 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4094"/>
        <source>다음 챕터로 이동 (%1)</source>
        <translation>다음 챕터로 이동 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4191"/>
        <source>애너글리프 알고리즘 (%1)</source>
        <translation>애너글리프 알고리즘 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4284"/>
        <source>히스토그램 이퀄라이저 켜짐</source>
        <translation>히스토그램 이퀄라이저 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4286"/>
        <source>히스토그램 이퀄라이저 꺼짐</source>
        <translation>히스토그램 이퀄라이저 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4305"/>
        <source>3D 노이즈 제거 켜짐</source>
        <translation>3D 노이즈 제거 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4307"/>
        <source>3D 노이즈 제거 꺼짐</source>
        <translation>3D 노이즈 제거 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4326"/>
        <source>적응 시간 평균 노이즈 제거 켜짐</source>
        <translation>적응 시간 평균 노이즈 제거 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4328"/>
        <source>적응 시간 평균 노이즈 제거 꺼짐</source>
        <translation>적응 시간 평균 노이즈 제거 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4347"/>
        <source>Overcomplete Wavelet 노이즈 제거 켜짐</source>
        <translation>Overcomplete Wavelet 노이즈 제거 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4349"/>
        <source>Overcomplete Wavelet 노이즈 제거 꺼짐</source>
        <translation>Overcomplete Wavelet 노이즈 제거 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4368"/>
        <source>디밴드 켜짐</source>
        <translation>디밴드 켜짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4370"/>
        <source>디밴드 꺼짐</source>
        <translation>디밴드 꺼짐</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4987"/>
        <source>자막 캐시 모드 사용</source>
        <translation>자막 캐시 모드 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="4989"/>
        <source>자막 캐시 모드 사용 안 함</source>
        <translation>자막 캐시 모드 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5104"/>
        <source>왼쪽 영상 사용</source>
        <translation>왼쪽 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5105"/>
        <source>오른쪽 영상 사용</source>
        <translation>오른쪽 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5106"/>
        <source>상단 영상 사용</source>
        <translation>상단 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5107"/>
        <source>하단 영상 사용</source>
        <translation>하단 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5108"/>
        <source>좌우 영상 사용</source>
        <translation>좌우 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5109"/>
        <source>상하 영상 사용</source>
        <translation>상하 영상 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5110"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5114"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5118"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5122"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5126"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5130"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5134"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5138"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5142"/>
        <source>왼쪽 영상 우선 사용</source>
        <translation>왼쪽 영상 우선 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5111"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5115"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5119"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5123"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5127"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5131"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5135"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5139"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5143"/>
        <source>오른쪽 영상 우선 사용</source>
        <translation>오른쪽 영상 우선 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5112"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5116"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5120"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5124"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5128"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5132"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5136"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5140"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5144"/>
        <source>상단 영상 우선 사용</source>
        <translation>상단 영상 우선 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5113"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5117"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5121"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5125"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5129"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5133"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5137"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5141"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5145"/>
        <source>하단 영상 우선 사용</source>
        <translation>하단 영상 우선 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5147"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5148"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5149"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5150"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5151"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5152"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5153"/>
        <source>일반</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5154"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5155"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5156"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5157"/>
        <source>Page Flipping</source>
        <translation>Page Flipping</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5158"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5159"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5160"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5161"/>
        <source>Row Interlaced</source>
        <translation>Row Interlaced</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5162"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5163"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5164"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5165"/>
        <source>Column Interlaced</source>
        <translation>Column Interlaced</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5166"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5167"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5168"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5169"/>
        <source>Red-Cyan Anaglyph</source>
        <translation>Red-Cyan Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5170"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5171"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5172"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5173"/>
        <source>Green-Magenta Anaglyph</source>
        <translation>Green-Magenta Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5174"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5175"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5176"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5177"/>
        <source>Yellow-Blue Anaglyph</source>
        <translation>Yellow-Blue Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5178"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5179"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5180"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5181"/>
        <source>Red-Blue Anaglyph</source>
        <translation>Red-Blue Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5182"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5183"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5184"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5185"/>
        <source>Red-Green Anaglyph</source>
        <translation>Red-Green Anaglyph</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5186"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5187"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5188"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5189"/>
        <source>Checker Board</source>
        <translation>Checker Board</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5195"/>
        <source>상/하</source>
        <translation>상/하</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5196"/>
        <source>좌/우</source>
        <translation>좌/우</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5197"/>
        <source>페이지 플리핑</source>
        <translation>페이지 플리핑</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5198"/>
        <source>인터레이스</source>
        <translation>인터레이스</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5199"/>
        <source>애너글리프</source>
        <translation>애너글리프</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5200"/>
        <source>체커 보드</source>
        <translation>체커 보드</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5227"/>
        <source>좌우 영상</source>
        <translation>좌우 영상</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5228"/>
        <source>상하 영상</source>
        <translation>상하 영상</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5229"/>
        <source>복제</source>
        <translation>복제</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5282"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5491"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5301"/>
        <source>S/PDIF 소리 출력 장치 변경 (%1)</source>
        <translation>S/PDIF 소리 출력 장치 변경 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5303"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5332"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5380"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5411"/>
        <source>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</source>
        <translation>S/PDIF 출력 초기화를 실패 하였습니다. PCM 출력으로 전환합니다.</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5326"/>
        <source>S/PDIF 출력 사용</source>
        <translation>S/PDIF 출력 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5328"/>
        <source>S/PDIF 출력 사용 안 함</source>
        <translation>S/PDIF 출력 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5366"/>
        <source>S/PDIF 출력 시 인코딩 사용 안 함</source>
        <translation>S/PDIF 출력 시 인코딩 사용 안 함</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5369"/>
        <source>S/PDIF 출력 시 AC3 인코딩 사용</source>
        <translation>S/PDIF 출력 시 AC3 인코딩 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5372"/>
        <source>S/PDIF 출력 시 DTS 인코딩 사용</source>
        <translation>S/PDIF 출력 시 DTS 인코딩 사용</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5402"/>
        <source>S/PDIF 샘플 속도 (%1)</source>
        <translation>S/PDIF 샘플 속도 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5405"/>
        <location filename="../src/ui/RenderScreen.cpp" line="5457"/>
        <source>기본 속도</source>
        <translation>기본 속도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5507"/>
        <source>소리 출력 장치 변경 (%1)</source>
        <translation>소리 출력 장치 변경 (%1)</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5527"/>
        <source>90도</source>
        <translation>90도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5530"/>
        <source>180도</source>
        <translation>180도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5533"/>
        <source>270도</source>
        <translation>270도</translation>
    </message>
    <message>
        <location filename="../src/ui/RenderScreen.cpp" line="5542"/>
        <source>화면 회전 각도 (%1)</source>
        <translation>화면 회전 각도 (%1)</translation>
    </message>
</context>
<context>
    <name>RepeatRange</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="116"/>
        <source>구간 반복 설정</source>
        <translation>구간 반복 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="145"/>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="162"/>
        <source>시작</source>
        <translation>시작</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="189"/>
        <location filename="../qml/AnyVODMobileClient/RepeatRange.qml" line="206"/>
        <source>종료</source>
        <translation>종료</translation>
    </message>
</context>
<context>
    <name>SPDIFInterface</name>
    <message>
        <location filename="../../AnyVODClient/src/audio/SPDIFInterface.cpp" line="92"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
</context>
<context>
    <name>SavePlayListAs</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SavePlayListAs.qml" line="128"/>
        <source>재생 목록 이름을 입력하세요.</source>
        <translation>재생 목록 이름을 입력하세요.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SavePlayListAs.qml" line="170"/>
        <source>저장</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SavePlayListAs.qml" line="185"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>ScanDTVChannel</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="173"/>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="324"/>
        <source>어댑터</source>
        <translation>어댑터</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="199"/>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="354"/>
        <source>형식</source>
        <translation>형식</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="226"/>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="384"/>
        <source>국가</source>
        <translation>국가</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="421"/>
        <source>채널 범위</source>
        <translation>채널 범위</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="506"/>
        <source>검색</source>
        <translation>검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="549"/>
        <source>중지</source>
        <translation>중지</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="579"/>
        <source>저장</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="615"/>
        <source>현재 채널 : </source>
        <translation>현재 채널 : </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="626"/>
        <source>, 찾은 채널 개수 : </source>
        <translation>, 찾은 채널 개수 : </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="643"/>
        <source>신호 감도</source>
        <translation>신호 감도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="737"/>
        <source>채널 </source>
        <translation>채널 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="737"/>
        <source>, 신호 강도 </source>
        <translation>, 신호 강도 </translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="741"/>
        <source>이름 없음</source>
        <translation>이름 없음</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScanDTVChannel.qml" line="771"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>ScreenSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="23"/>
        <source>비율</source>
        <translation>비율</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="24"/>
        <source>화면 비율</source>
        <translation>화면 비율</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="24"/>
        <source>사용자 지정</source>
        <translation>사용자 지정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="25"/>
        <source>회전 각도</source>
        <translation>회전 각도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="25"/>
        <source>영상</source>
        <translation>영상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="26"/>
        <source>밝기</source>
        <translation>밝기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="28"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="29"/>
        <source>영상 속성</source>
        <translation>영상 속성</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="27"/>
        <source>채도</source>
        <translation>채도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="28"/>
        <source>색상</source>
        <translation>색상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="29"/>
        <source>대비</source>
        <translation>대비</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="30"/>
        <source>날카롭게</source>
        <translation>날카롭게</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="30"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="32"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="33"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="34"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="35"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="36"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="37"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="38"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="39"/>
        <source>영상 효과</source>
        <translation>영상 효과</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="31"/>
        <source>선명하게</source>
        <translation>선명하게</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="32"/>
        <source>부드럽게</source>
        <translation>부드럽게</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="33"/>
        <source>히스토그램 이퀄라이저</source>
        <translation>히스토그램 이퀄라이저</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="34"/>
        <source>디밴드</source>
        <translation>디밴드</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="35"/>
        <source>3D 노이즈 제거</source>
        <translation>3D 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="36"/>
        <source>적응 시간 평균 노이즈 제거</source>
        <translation>적응 시간 평균 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="37"/>
        <source>Overcomplete Wavelet 노이즈 제거</source>
        <translation>Overcomplete Wavelet 노이즈 제거</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="38"/>
        <source>좌우 반전</source>
        <translation>좌우 반전</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="39"/>
        <source>상하 반전</source>
        <translation>상하 반전</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="40"/>
        <source>캡처 확장자</source>
        <translation>캡처 확장자</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="40"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="41"/>
        <source>캡처</source>
        <translation>캡처</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="41"/>
        <source>캡처 저장 디렉토리 설정</source>
        <translation>캡처 저장 디렉토리 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="42"/>
        <source>활성화 기준</source>
        <translation>활성화 기준</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="42"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="43"/>
        <source>디인터레이스</source>
        <translation>디인터레이스</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="43"/>
        <source>알고리즘</source>
        <translation>알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="44"/>
        <source>하드웨어 디코더 사용</source>
        <translation>하드웨어 디코더 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="44"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="45"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="46"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="47"/>
        <source>화면</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="45"/>
        <source>저화질 모드 사용</source>
        <translation>저화질 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="46"/>
        <source>프레임 드랍 사용</source>
        <translation>프레임 드랍 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="47"/>
        <source>앨범 자켓 보기</source>
        <translation>앨범 자켓 보기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="48"/>
        <source>전체 해상도 사용</source>
        <translation>전체 해상도 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="48"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="49"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="50"/>
        <source>3D 영상</source>
        <translation>3D 영상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="49"/>
        <source>출력 방법</source>
        <translation>출력 방법</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="50"/>
        <source>애너글리프 알고리즘</source>
        <translation>애너글리프 알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="51"/>
        <source>입력 영상</source>
        <translation>입력 영상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="51"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="52"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="53"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="54"/>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="55"/>
        <source>VR 영상</source>
        <translation>VR 영상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="52"/>
        <source>헤드 트래킹 사용</source>
        <translation>헤드 트래킹 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="53"/>
        <source>왜곡 보정 사용</source>
        <translation>왜곡 보정 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="54"/>
        <source>왜곡 보정 계수</source>
        <translation>왜곡 보정 계수</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ScreenSettingItems.qml" line="55"/>
        <source>렌즈 센터 설정</source>
        <translation>렌즈 센터 설정</translation>
    </message>
</context>
<context>
    <name>SearchMedia</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SearchMedia.qml" line="71"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SearchMedia.qml" line="72"/>
        <source>검색 결과가 없습니다.</source>
        <translation>검색 결과가 없습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SearchMedia.qml" line="262"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>SelectVector2D</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SelectVector2D.qml" line="143"/>
        <source>첫째</source>
        <translation>첫째</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SelectVector2D.qml" line="183"/>
        <source>둘째</source>
        <translation>둘째</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SelectVector2D.qml" line="242"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SelectVector2D.qml" line="258"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>SettingList</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SettingList.qml" line="262"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>SettingSlider</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SettingSlider.qml" line="180"/>
        <source>초기화</source>
        <translation>초기화</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SettingSlider.qml" line="380"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>SettingTextViewer</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SettingTextViewer.qml" line="148"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
</context>
<context>
    <name>SortMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="24"/>
        <source>이름(오름차순)</source>
        <translation>이름(오름차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="29"/>
        <source>변경 시각(오름차순)</source>
        <translation>변경 시각(오름차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="34"/>
        <source>크기(오름차순)</source>
        <translation>크기(오름차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="39"/>
        <source>종류(오름차순)</source>
        <translation>종류(오름차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="48"/>
        <source>이름(내림차순)</source>
        <translation>이름(내림차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="53"/>
        <source>변경 시각(내림차순)</source>
        <translation>변경 시각(내림차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="58"/>
        <source>크기(내림차순)</source>
        <translation>크기(내림차순)</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SortMenuItems.qml" line="63"/>
        <source>종류(내림차순)</source>
        <translation>종류(내림차순)</translation>
    </message>
</context>
<context>
    <name>SoundSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="23"/>
        <source>언어</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="24"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="25"/>
        <source>일반</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="24"/>
        <source>싱크</source>
        <translation>싱크</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="25"/>
        <source>오디오 장치</source>
        <translation>오디오 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="26"/>
        <source>노멀라이저 사용</source>
        <translation>노멀라이저 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="28"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="29"/>
        <source>효과</source>
        <translation>효과</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="27"/>
        <source>음성 줄임</source>
        <translation>음성 줄임</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="28"/>
        <source>음성 강조</source>
        <translation>음성 강조</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="29"/>
        <source>음악 줄임</source>
        <translation>음악 줄임</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="30"/>
        <source>출력 장치</source>
        <translation>출력 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="30"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="32"/>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="33"/>
        <source>S/PDIF</source>
        <translation>S/PDIF</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="31"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="32"/>
        <source>샘플링 속도</source>
        <translation>샘플링 속도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SoundSettingItems.qml" line="33"/>
        <source>사용 하기</source>
        <translation>사용 하기</translation>
    </message>
</context>
<context>
    <name>SubtitleLyricsSettingItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="23"/>
        <source>열기</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="23"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="24"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="25"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="26"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="27"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="28"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="29"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="30"/>
        <source>일반</source>
        <translation>일반</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="24"/>
        <source>외부 닫기</source>
        <translation>외부 닫기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="25"/>
        <source>보이기</source>
        <translation>보이기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="26"/>
        <source>고급 검색</source>
        <translation>고급 검색</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="27"/>
        <source>투명도</source>
        <translation>투명도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="28"/>
        <source>크기</source>
        <translation>크기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="29"/>
        <source>싱크</source>
        <translation>싱크</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="30"/>
        <source>자막 캐시 모드 사용</source>
        <translation>자막 캐시 모드 사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="31"/>
        <source>가로 위치</source>
        <translation>가로 위치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="31"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="32"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="33"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="34"/>
        <source>정렬</source>
        <translation>정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="32"/>
        <source>세로 위치</source>
        <translation>세로 위치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="33"/>
        <source>가로 정렬</source>
        <translation>가로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="34"/>
        <source>세로 정렬</source>
        <translation>세로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="35"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="36"/>
        <source>언어</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="36"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="37"/>
        <source>자막 찾기</source>
        <translation>자막 찾기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="37"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="38"/>
        <source>찾기</source>
        <translation>찾기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="38"/>
        <source>가사 찾기</source>
        <translation>가사 찾기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="39"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="40"/>
        <source>저장</source>
        <translation>저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="40"/>
        <source>다른 이름으로 저장</source>
        <translation>다른 이름으로 저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="41"/>
        <source>가로 거리</source>
        <translation>가로 거리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="41"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="42"/>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="43"/>
        <source>3D 자막</source>
        <translation>3D 자막</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="42"/>
        <source>세로 거리</source>
        <translation>세로 거리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml" line="43"/>
        <source>출력 방법</source>
        <translation>출력 방법</translation>
    </message>
</context>
<context>
    <name>UserAspectRatio</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/UserAspectRatio.qml" line="136"/>
        <source>넓이</source>
        <translation>넓이</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/UserAspectRatio.qml" line="174"/>
        <source>높이</source>
        <translation>높이</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/UserAspectRatio.qml" line="231"/>
        <source>확인</source>
        <translation>확인</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/UserAspectRatio.qml" line="248"/>
        <source>취소</source>
        <translation>취소</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../../AnyVODClient/src/core/Utils.cpp" line="226"/>
        <source>오류</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/Utils.cpp" line="231"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/Utils.cpp" line="236"/>
        <source>질문</source>
        <translation>질문</translation>
    </message>
    <message>
        <location filename="../../AnyVODClient/src/core/Utils.cpp" line="643"/>
        <source>%1로 열기</source>
        <translation>%1로 열기</translation>
    </message>
</context>
<context>
    <name>VideoScreen</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="233"/>
        <source>자막 찾음</source>
        <translation>자막 찾음</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="243"/>
        <source>이퀄라이저</source>
        <translation>이퀄라이저</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="256"/>
        <source>채널 편성표</source>
        <translation>채널 편성표</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="421"/>
        <source>지원하지 않는 기능입니다.</source>
        <translation>지원하지 않는 기능입니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="431"/>
        <source>디인터레이스 활성화 기준</source>
        <translation>디인터레이스 활성화 기준</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="435"/>
        <source>자동</source>
        <translation>자동</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="436"/>
        <source>사용</source>
        <translation>사용</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="437"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="546"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1124"/>
        <source>사용 안 함</source>
        <translation>사용 안 함</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="448"/>
        <source>디인터레이스 알고리즘</source>
        <translation>디인터레이스 알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="470"/>
        <source>화면 비율</source>
        <translation>화면 비율</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="492"/>
        <source>화면 비율 사용자 지정</source>
        <translation>화면 비율 사용자 지정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="504"/>
        <source>VR 왜곡 보정 계수 설정</source>
        <translation>VR 왜곡 보정 계수 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="505"/>
        <source>k1</source>
        <translation>k1</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="506"/>
        <source>k2</source>
        <translation>k2</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="523"/>
        <source>VR 렌즈 센터 설정</source>
        <translation>VR 렌즈 센터 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="524"/>
        <source>좌/우</source>
        <translation>좌/우</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="525"/>
        <source>상/하</source>
        <translation>상/하</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="542"/>
        <source>영상 회전 각도</source>
        <translation>영상 회전 각도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="547"/>
        <source>90도</source>
        <translation>90도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="548"/>
        <source>180도</source>
        <translation>180도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="549"/>
        <source>270도</source>
        <translation>270도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="560"/>
        <source>캡처 확장자</source>
        <translation>캡처 확장자</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="580"/>
        <source>3D 영상 출력 방법</source>
        <translation>3D 영상 출력 방법</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="603"/>
        <source>3D 영상 애너글리프 알고리즘</source>
        <translation>3D 영상 애너글리프 알고리즘</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="625"/>
        <source>VR 입력 영상</source>
        <translation>VR 입력 영상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="647"/>
        <source>재생 순서 설정</source>
        <translation>재생 순서 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="651"/>
        <source>전체 순차 재생</source>
        <translation>전체 순차 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="652"/>
        <source>전체 반복 재생</source>
        <translation>전체 반복 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="653"/>
        <source>한 개 재생</source>
        <translation>한 개 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="654"/>
        <source>한 개 반복 재생</source>
        <translation>한 개 반복 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="655"/>
        <source>무작위 재생</source>
        <translation>무작위 재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="665"/>
        <source>밝기</source>
        <translation>밝기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="670"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="687"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="721"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="740"/>
        <source>배</source>
        <translation>배</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="682"/>
        <source>채도</source>
        <translation>채도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="699"/>
        <source>색상</source>
        <translation>색상</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="704"/>
        <source>°</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="716"/>
        <source>대비</source>
        <translation>대비</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="733"/>
        <source>재생 속도 설정</source>
        <translation>재생 속도 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="752"/>
        <source>투명도</source>
        <translation>투명도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="757"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="774"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="769"/>
        <source>크기</source>
        <translation>크기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="790"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="974"/>
        <source>싱크</source>
        <translation>싱크</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="795"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="979"/>
        <source>초</source>
        <translation>초</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="807"/>
        <source>가로 위치</source>
        <translation>가로 위치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="808"/>
        <source>값이 클 수록 오른쪽으로 이동합니다.</source>
        <translation>값이 클 수록 오른쪽으로 이동합니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="824"/>
        <source>세로 위치</source>
        <translation>세로 위치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="825"/>
        <source>값이 클 수록 윗쪽으로 이동합니다.</source>
        <translation>값이 클 수록 윗쪽으로 이동합니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="842"/>
        <source>가로 정렬</source>
        <translation>가로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="846"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="865"/>
        <source>기본 정렬</source>
        <translation>기본 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="847"/>
        <source>자동 정렬</source>
        <translation>자동 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="848"/>
        <source>왼쪽 정렬</source>
        <translation>왼쪽 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="849"/>
        <source>가운데 정렬</source>
        <translation>가운데 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="850"/>
        <source>오른쪽 정렬</source>
        <translation>오른쪽 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="861"/>
        <source>세로 정렬</source>
        <translation>세로 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="866"/>
        <source>상단 정렬</source>
        <translation>상단 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="867"/>
        <source>하단 정렬</source>
        <translation>하단 정렬</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="876"/>
        <source>텍스트 인코딩</source>
        <translation>텍스트 인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="897"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1047"/>
        <source>언어</source>
        <translation>언어</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="918"/>
        <source>세로 거리</source>
        <translation>세로 거리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="935"/>
        <source>가로 거리</source>
        <translation>가로 거리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="953"/>
        <source>3D 자막 출력 방법</source>
        <translation>3D 자막 출력 방법</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="991"/>
        <source>열기</source>
        <translation>열기</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1010"/>
        <source>다른 이름으로 저장</source>
        <translation>다른 이름으로 저장</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1032"/>
        <source>저장 디렉토리 설정</source>
        <translation>저장 디렉토리 설정</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1068"/>
        <source>오디오 장치</source>
        <translation>오디오 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1075"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1099"/>
        <source>기본 장치</source>
        <translation>기본 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1092"/>
        <source>출력 장치</source>
        <translation>출력 장치</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1120"/>
        <source>인코딩</source>
        <translation>인코딩</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1125"/>
        <source>AC3</source>
        <translation>AC3</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1126"/>
        <source>DTS</source>
        <translation>DTS</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1137"/>
        <source>샘플링 속도</source>
        <translation>샘플링 속도</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1157"/>
        <source>화면</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1245"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1276"/>
        <source>자막 / 가사</source>
        <translation>자막 / 가사</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1356"/>
        <source>소리</source>
        <translation>소리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1415"/>
        <source>AnyVOD 정보</source>
        <translation>AnyVOD 정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="420"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1426"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1490"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1501"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1523"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="919"/>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="936"/>
        <source>값이 클 수록 가까워집니다. VR 모드일 경우 반대 입니다.</source>
        <translation>값이 클 수록 가까워집니다. VR 모드일 경우 반대 입니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1491"/>
        <source>외부 자막을 닫았습니다.</source>
        <translation>외부 자막을 닫았습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1502"/>
        <source>자막 / 가사가 저장 되었습니다.</source>
        <translation>자막 / 가사가 저장 되었습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1512"/>
        <source>오류</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1513"/>
        <source>자막 / 가사가 저장 되지 않았습니다.</source>
        <translation>자막 / 가사가 저장 되지 않았습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="1524"/>
        <source>자막 / 가사를 열지 못했습니다.</source>
        <translation>자막 / 가사를 열지 못했습니다.</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreen.qml" line="2401"/>
        <source>외부 서버에 자막이 존재합니다.
이동하시려면 여기를 클릭하세요.</source>
        <translation>외부 서버에 자막이 존재합니다.
이동하시려면 여기를 클릭하세요.</translation>
    </message>
</context>
<context>
    <name>VideoScreenMenuItems</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="25"/>
        <source>이퀄라이저</source>
        <translation>이퀄라이저</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="31"/>
        <source>화면</source>
        <translation>화면</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="37"/>
        <source>재생</source>
        <translation>재생</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="43"/>
        <source>자막 / 가사</source>
        <translation>자막 / 가사</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="49"/>
        <source>소리</source>
        <translation>소리</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/VideoScreenMenuItems.qml" line="55"/>
        <source>정보</source>
        <translation>정보</translation>
    </message>
</context>
<context>
    <name>ViewEPG</name>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="176"/>
        <source>채널</source>
        <translation>채널</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="184"/>
        <source>제목</source>
        <translation>제목</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="189"/>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="198"/>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="357"/>
        <source>업데이트 중...</source>
        <translation>업데이트 중...</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="193"/>
        <source>방송 시간</source>
        <translation>방송 시간</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="350"/>
        <source>현재 시각</source>
        <translation>현재 시각</translation>
    </message>
    <message>
        <location filename="../qml/AnyVODMobileClient/ViewEPG.qml" line="365"/>
        <source>닫기</source>
        <translation>닫기</translation>
    </message>
</context>
</TS>

# Add more folders to ship with the application, here
anyvod.source = qml/AnyVODMobileClient
anyvod.target = qml

DEPLOYMENTFOLDERS += anyvod

android-* {
    languages.source = languages/*.qm
    languages.target = languages

    fonts.source = fonts/*.*
    fonts.target = fonts

    licenses.target = licenses
    licenses.source = ../../licenses/noto.txt ../../licenses/bass.txt ../../licenses/ffmpeg.txt \
                      ../../licenses/libass.txt ../../licenses/qt.txt ../../licenses/anyvod.txt

    DEPLOYMENTFOLDERS += languages licenses
}

ios {
    icon.source = ../../images/icons/app.icns
    icon.target = ./

    fonts.source = fonts
    fonts.target = ./

    DEPLOYMENTFOLDERS += icon

    languages.files = $$files($$PWD/languages/*.qm)
    languages.path = languages

    licenses.files = ../../licenses/noto.txt ../../licenses/bass.txt ../../licenses/ffmpeg.txt \
                     ../../licenses/libass.txt ../../licenses/qt.txt ../../licenses/anyvod.txt
    licenses.path = licenses

    icons.files = $$files($$PWD/resources/ios/icons/*.png)
    launches.files = resources/ios/splash/AnyVODLaunchScreen.xib $$files($$PWD/resources/ios/splash/*.png)

    QMAKE_BUNDLE_DATA += languages licenses icons launches
}

!linux-rasp-pi2-* : !linux-* : DEPLOYMENTFOLDERS += fonts

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

QT += core gui network qml quick xml

android-*: QT += androidextras

HEADERS += \
    ../../common/RetreiveSubtitle.h \
    ../../common/SubtitleFileNameGenerator.h \
    ../AnyVODClient/src/core/Utils.h \
    ../AnyVODClient/src/core/Common.h \
    ../AnyVODClient/src/core/TextEncodingDetector.h \
    ../AnyVODClient/src/core/Settings.h \
    ../AnyVODClient/src/parsers/playlist/ASXParser.h \
    ../AnyVODClient/src/parsers/playlist/B4SParser.h \
    ../AnyVODClient/src/parsers/playlist/CueParser.h \
    ../AnyVODClient/src/parsers/playlist/M3UParser.h \
    ../AnyVODClient/src/parsers/playlist/PlayListParserGenerator.h \
    ../AnyVODClient/src/parsers/playlist/PlayListParserInterface.h \
    ../AnyVODClient/src/parsers/playlist/PLSParser.h \
    ../AnyVODClient/src/parsers/playlist/WPLParser.h \
    ../AnyVODClient/src/parsers/subtitle/ASSParser.h \
    ../AnyVODClient/src/parsers/subtitle/AVParser.h \
    ../AnyVODClient/src/parsers/subtitle/LRCParser.h \
    ../AnyVODClient/src/parsers/subtitle/SAMIParser.h \
    ../AnyVODClient/src/parsers/subtitle/SRTParser.h \
    ../AnyVODClient/src/parsers/subtitle/YouTubeParser.h \
    ../AnyVODClient/src/audio/SPDIF.h \
    ../AnyVODClient/src/audio/SPDIFEncoder.h \
    ../AnyVODClient/src/audio/SPDIFInterface.h \
    ../AnyVODClient/src/decoders/HWDecoder.h \
    ../AnyVODClient/src/decoders/HWDecoderInterface.h \
    ../AnyVODClient/src/media/PacketQueue.h \
    ../AnyVODClient/src/media/SyncType.h \
    ../AnyVODClient/src/media/MediaPresenter.h \
    ../AnyVODClient/src/media/MediaState.h \
    ../AnyVODClient/src/media/ReadThread.h \
    ../AnyVODClient/src/media/SubtitleThread.h \
    ../AnyVODClient/src/media/VideoThread.h \
    ../AnyVODClient/src/media/LastPlay.h \
    ../AnyVODClient/src/media/RefreshThread.h \
    ../AnyVODClient/src/media/FrameExtractor.h \
    ../AnyVODClient/src/net/HttpDownloader.h \
    ../AnyVODClient/src/net/SubtitleImpl.h \
    ../AnyVODClient/src/net/SyncHttp.h \
    ../AnyVODClient/src/net/YouTubeURLPicker.h \
    ../AnyVODClient/src/video/Deinterlacer.h \
    ../AnyVODClient/src/video/Font.h \
    ../AnyVODClient/src/video/ShaderCompositer.h \
    ../AnyVODClient/src/video/FilterGraph.h \
    src/models/FileListModel.h \
    src/models/PlayListModel.h \
    src/models/DTVListModel.h \
    src/models/EPGListModel.h \
    src/models/SearchMediaModel.h \
    src/models/ManagePlayListModel.h \
    src/media/ThumbnailCache.h \
    src/media/PlayListItemUpdater.h \
    src/media/DTVListItemUpdater.h \
    src/video/GLRenderer.h \
    src/ui/RenderScreen.h \
    src/ui/MediaDelegate.h \
    src/ui/DTVDelegate.h \
    src/ui/StatusUpdater.h \
    src/core/AnyVODWindow.h \
    src/core/BuildNumber.h

android-*: HEADERS += \
    src/decoders/MediaCodec.h

ios: HEADERS += \
    ../AnyVODClient/src/decoders/VideoToolBoxDecoder.h \
    src/core/IOSDelegate.h \
    src/core/IOSViewController.h

linux-rasp-pi2-* | linux-*: HEADERS += \
    ../AnyVODClient/src/audio/SPDIFAlsa.h \
    ../AnyVODClient/src/device/DTVLinuxDVB.h \
    ../AnyVODClient/src/device/DTVReader.h \
    ../AnyVODClient/src/device/DTVReaderInterface.h \
    ../AnyVODClient/src/device/DTVChannelMap.h \
    src/ui/XDGScreenSaverDisabler.h

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += \
    ../../common/RetreiveSubtitle.cpp \
    ../../common/SubtitleFileNameGenerator.cpp \
    ../AnyVODClient/src/core/Utils.cpp \
    ../AnyVODClient/src/core/Common.cpp \
    ../AnyVODClient/src/core/TextEncodingDetector.cpp \
    ../AnyVODClient/src/core/Settings.cpp \
    ../AnyVODClient/src/parsers/playlist/ASXParser.cpp \
    ../AnyVODClient/src/parsers/playlist/B4SParser.cpp \
    ../AnyVODClient/src/parsers/playlist/CueParser.cpp \
    ../AnyVODClient/src/parsers/playlist/M3UParser.cpp \
    ../AnyVODClient/src/parsers/playlist/PlayListParserGenerator.cpp \
    ../AnyVODClient/src/parsers/playlist/PlayListParserInterface.cpp \
    ../AnyVODClient/src/parsers/playlist/PLSParser.cpp \
    ../AnyVODClient/src/parsers/playlist/WPLParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/ASSParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/AVParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/LRCParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/SAMIParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/SRTParser.cpp \
    ../AnyVODClient/src/parsers/subtitle/YouTubeParser.cpp \
    ../AnyVODClient/src/audio/SPDIF.cpp \
    ../AnyVODClient/src/audio/SPDIFEncoder.cpp \
    ../AnyVODClient/src/audio/SPDIFInterface.cpp \
    ../AnyVODClient/src/decoders/HWDecoder.cpp \
    ../AnyVODClient/src/decoders/HWDecoderInterface.cpp \
    ../AnyVODClient/src/media/MediaPresenter.cpp \
    ../AnyVODClient/src/media/ReadThread.cpp \
    ../AnyVODClient/src/media/SubtitleThread.cpp \
    ../AnyVODClient/src/media/VideoThread.cpp \
    ../AnyVODClient/src/media/LastPlay.cpp \
    ../AnyVODClient/src/media/RefreshThread.cpp \
    ../AnyVODClient/src/media/PacketQueue.cpp \
    ../AnyVODClient/src/media/FrameExtractor.cpp \
    ../AnyVODClient/src/net/SubtitleImpl.cpp \
    ../AnyVODClient/src/net/SyncHttp.cpp \
    ../AnyVODClient/src/net/YouTubeURLPicker.cpp \
    ../AnyVODClient/src/net/HttpDownloader.cpp \
    ../AnyVODClient/src/video/Deinterlacer.cpp \
    ../AnyVODClient/src/video/Font.cpp \
    ../AnyVODClient/src/video/ShaderCompositer.cpp \
    ../AnyVODClient/src/video/FilterGraph.cpp \
    ../AnyVODClient/src/device/DTVReader.cpp \
    ../AnyVODClient/src/device/DTVReaderInterface.cpp \
    ../AnyVODClient/src/device/DTVChannelMap.cpp \
    src/core/main.cpp \
    src/core/AnyVODWindow.cpp \
    src/models/FileListModel.cpp \
    src/models/PlayListModel.cpp \
    src/models/DTVListModel.cpp \
    src/models/EPGListModel.cpp \
    src/models/SearchMediaModel.cpp \
    src/models/ManagePlayListModel.cpp \
    src/media/ThumbnailCache.cpp \
    src/media/PlayListItemUpdater.cpp \
    src/media/DTVListItemUpdater.cpp \
    src/video/GLRenderer.cpp \
    src/ui/RenderScreen.cpp \
    src/ui/MediaDelegate.cpp \
    src/ui/StatusUpdater.cpp \
    src/ui/DTVDelegate.cpp

android-*: SOURCES += \
    src/core/AndroidNative.cpp \
    src/decoders/MediaCodec.cpp

ios: SOURCES += \
    ../AnyVODClient/src/decoders/VideoToolBoxDecoder.cpp

linux-rasp-pi2-* | linux-*: SOURCES += \
    ../AnyVODClient/src/audio/SPDIFAlsa.cpp \
    ../AnyVODClient/src/device/DTVLinuxDVB.cpp \
    src/ui/XDGScreenSaverDisabler.cpp

OBJECTIVE_SOURCES += \
    src/core/IOSDelegate.mm \
    src/core/IOSViewController.mm

TRANSLATIONS += \
    languages/anyvod_en.ts \
    languages/anyvod_ko.ts

lupdate_only {
DISTFILES += \
    qml/AnyVODMobileClient/main.qml \
    qml/AnyVODMobileClient/Scrollbar.qml \
    qml/AnyVODMobileClient/ImageButton.qml \
    qml/AnyVODMobileClient/ImageCheckButton.qml \
    qml/AnyVODMobileClient/Menus.qml \
    qml/AnyVODMobileClient/SettingDialog.qml \
    qml/AnyVODMobileClient/SettingList.qml \
    qml/AnyVODMobileClient/ComboItem.qml \
    qml/AnyVODMobileClient/SettingSlider.qml \
    qml/AnyVODMobileClient/SettingTextViewer.qml \
    qml/AnyVODMobileClient/ContextMenuTextInput.qml \
    qml/AnyVODMobileClient/MessageBox.qml \
    qml/AnyVODMobileClient/FileSystemDialog.qml \
    qml/AnyVODMobileClient/Popup.qml \
    qml/AnyVODMobileClient/Initial.qml \
    qml/AnyVODMobileClient/VideoScreen.qml \
    qml/AnyVODMobileClient/Equalizer.qml \
    qml/AnyVODMobileClient/PlayList.qml \
    qml/AnyVODMobileClient/OpenExternal.qml \
    qml/AnyVODMobileClient/UserAspectRatio.qml \
    qml/AnyVODMobileClient/RepeatRange.qml \
    qml/AnyVODMobileClient/MainMenuItems.qml \
    qml/AnyVODMobileClient/VideoScreenMenuItems.qml \
    qml/AnyVODMobileClient/DirItemMenuItems.qml \
    qml/AnyVODMobileClient/FileItemMenuItems.qml \
    qml/AnyVODMobileClient/PlayListMenuItems.qml \
    qml/AnyVODMobileClient/SortMenuItems.qml \
    qml/AnyVODMobileClient/MainSettingItems.qml \
    qml/AnyVODMobileClient/ScreenSettingItems.qml \
    qml/AnyVODMobileClient/PlaySettingItems.qml \
    qml/AnyVODMobileClient/SubtitleLyricsSettingItems.qml \
    qml/AnyVODMobileClient/SoundSettingItems.qml \
    qml/AnyVODMobileClient/InfoSettingItems.qml \
    qml/AnyVODMobileClient/ScanDTVChannel.qml \
    qml/AnyVODMobileClient/DTVChannelList.qml \
    qml/AnyVODMobileClient/ViewEPG.qml \
    qml/AnyVODMobileClient/VirtualKeyboardPanel.qml \
    qml/AnyVODMobileClient/VirtualKeyboardPanelInternal.qml \
    qml/AnyVODMobileClient/SearchMedia.qml \
    qml/AnyVODMobileClient/MarqueeText.qml \
    qml/AnyVODMobileClient/SavePlayListAs.qml \
    qml/AnyVODMobileClient/ManagePlayList.qml \
    qml/AnyVODMobileClient/SelectVector2D.qml
}

android-*: DISTFILES += \
    resources/android/gradle/wrapper/gradle-wrapper.jar \
    resources/android/gradlew \
    resources/android/res/values/libs.xml \
    resources/android/res/values/theme.xml \
    resources/android/res/drawable/splash.xml \
    resources/android/build.gradle \
    resources/android/gradle/wrapper/gradle-wrapper.properties \
    resources/android/gradlew.bat

ios: DISTFILES += \
    resources/ios/ios.plist

DISTFILES += \
    languages/anyvod_en.ts \
    languages/anyvod_ko.ts

linux-rasp-pi2-*: INCLUDEPATH += /usr/include/dbus-1.0 /usr/lib/arm-linux-gnueabihf/dbus-1.0/include
linux-rasp-pi2-*: DEPENDPATH += /usr/include/dbus-1.0 /usr/lib/arm-linux-gnueabihf/dbus-1.0/include

linux-*: INCLUDEPATH += /usr/include/dbus-1.0 /usr/lib/x86_64-linux-gnu/dbus-1.0/include
linux-*: DEPENDPATH += /usr/include/dbus-1.0 /usr/lib/x86_64-linux-gnu/dbus-1.0/include

linux-rasp-pi2-* | linux-*: DEFINES += Q_OS_RASPBERRY_PI

INCLUDEPATH += $$PWD/../AnyVODClient/src \
               $$PWD/include \
               $$PWD/include/ffmpeg

DEFINES += __STDC_CONSTANT_MACROS Q_OS_MOBILE

LIBS += -lavcodec -lavformat -lswresample -lavutil -lavfilter -lswscale -lbass -lbassmix -lbass_fx -lass

android-*: LIBS += -lgomp
android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
    LIBS += -L$$PWD/libs/android/arm
android-*: equals(ANDROID_TARGET_ARCH, x86): \
    LIBS += -L$$PWD/libs/android/x86

QMAKE_OBJECTIVE_CFLAGS += -fobjc-arc

ios: LIBS += -framework CoreVideo -framework CoreMedia -framework VideoToolBox -framework AudioToolbox \
             -framework MediaPlayer -framework AVFoundation -framework SystemConfiguration -framework CFNetwork \
             -framework Accelerate
ios: LIBS += -lcrypto -lssl -lbz2 -liconv -lexpat -lfribidi -lfreetype -lspeex -lilbc
ios: LIBS += -L$$PWD/libs/ios

linux-rasp-pi2-* | linux-*: LIBS += -ldl -lX11 -lz -lgomp -lasound -ldvbpsi -ldbus-1
linux-rasp-pi2-*: LIBS += -L$$PWD/libs/raspi
linux-*: LIBS += -L$$PWD/../AnyVODClient/libs/linux

android-*: QMAKE_CXXFLAGS += -fopenmp
android-*: QMAKE_CFLAGS += -fopenmp
android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
    QMAKE_CXXFLAGS += -mtune=cortex-a8
android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
    QMAKE_CFLAGS += -mtune=cortex-a8
android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
    QMAKE_LFLAGS += -mtune=cortex-a8

linux-rasp-pi2-* | linux-*: QMAKE_CXXFLAGS += -fopenmp
linux-rasp-pi2-* | linux-*: QMAKE_CFLAGS += -fopenmp

android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
    QMAKE_POST_LINK += ../AnyVODMobileClient/scripts/android/postlink.sh android_armv7
android-*: equals(ANDROID_TARGET_ARCH, x86): \
    QMAKE_POST_LINK += ../AnyVODMobileClient/scripts/android/postlink.sh android_x86

ios: QMAKE_POST_LINK += ../AnyVODMobileClient/scripts/ios/postlink.sh
ios: QMAKE_INFO_PLIST = resources/ios/ios.plist

linux-rasp-pi2-*: QMAKE_POST_LINK += ../AnyVODMobileClient/scripts/raspi/postlink.sh

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

QMAKE_IOS_DEPLOYMENT_TARGET = 7.0

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/resources/android

android-*: OTHER_FILES += \
    resources/android/AndroidManifest.xml \
    resources/android/src/com/dcple/anyvod/AnyVODActivity.java \
    resources/android/src/com/dcple/anyvod/NativeFunctions.java \
    resources/android/src/com/dcple/anyvod/RemoteControlReceiver.java \
    resources/android/src/com/dcple/anyvod/PreventKillService.java

android-*: equals(ANDROID_TARGET_ARCH, armeabi-v7a): \
    ANDROID_EXTRA_LIBS += \
        $$PWD/libs/android/arm/libavutil-55.so \
        $$PWD/libs/android/arm/libswscale-4.so \
        $$PWD/libs/android/arm/libswresample-2.so \
        $$PWD/libs/android/arm/libavcodec-57.so \
        $$PWD/libs/android/arm/libavformat-57.so \
        $$PWD/libs/android/arm/libavfilter-6.so \
        $$PWD/libs/android/arm/libbass.so \
        $$PWD/libs/android/arm/libbassmix.so \
        $$PWD/libs/android/arm/libbass_fx.so \
        $$PWD/libs/android/arm/libass.so

android-*: equals(ANDROID_TARGET_ARCH, x86): \
    ANDROID_EXTRA_LIBS += \
        $$PWD/libs/android/x86/libavutil-55.so \
        $$PWD/libs/android/x86/libswscale-4.so \
        $$PWD/libs/android/x86/libswresample-2.so \
        $$PWD/libs/android/x86/libavcodec-57.so \
        $$PWD/libs/android/x86/libavformat-57.so \
        $$PWD/libs/android/x86/libavfilter-6.so \
        $$PWD/libs/android/x86/libbass.so \
        $$PWD/libs/android/x86/libbassmix.so \
        $$PWD/libs/android/x86/libbass_fx.so \
        $$PWD/libs/android/x86/libass.so

/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "..\..\..\..\common\Analyser.h"
#include <stdint.h>

class AnyVODAnalyserMKV : public Analyser
{
public:
  AnyVODAnalyserMKV(HMODULE handle);
  virtual ~AnyVODAnalyserMKV();

  virtual void GetName(wchar_t ret[MAX_ANALYSER_NAME_SIZE]);
  virtual void GetDescription(wchar_t ret[MAX_ANALYSER_DESC_SIZE]);
  virtual bool Analyse(const wstring &path, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret);

private:
  struct ELEMENT
  {
    uint64_t id;
    uint64_t size;

  };

private:
  bool GetInteger(HANDLE file, bool id, uint64_t *ret);
  bool ConvertInteger(const uint8_t integer[8], int len, uint64_t *ret);
  int  GetIntegerLength(uint8_t header);
  bool ReadElement(HANDLE file, ELEMENT *ret);
  bool FindSegment(HANDLE file, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret);
  void AddElements(HANDLE file, VECTOR_ANYVOD_ANALYSED_DATA_BLOCK *ret);
  bool IsValidElement(uint64_t id);

private:
  static const uint8_t EBML_ID[4];
  static const uint8_t SEGMENT_ID[4];
  static const uint8_t SEEK_HEAD_ID[4];
  static const uint8_t SEGMENT_INFO_ID[4];
  static const uint8_t TRACKS_ID[4];
  static const uint8_t CUES_ID[4];
  static const uint8_t ATTACHMENTS_ID[4];
  static const uint8_t CHAPTERS_ID[4];
  static const uint8_t TAGS_ID[4];
  static const uint8_t VOID_ID[1];
  static const uint8_t CRC32_ID[1];

};

@ECHO OFF

cd AnyVODAnalyserAVI
call build.bat

if %errorlevel% neq 0 goto end

cd ..

cd AnyVODAnalyserMP4
call build.bat

if %errorlevel% neq 0 goto end

cd ..

cd AnyVODAnalyserMP3
call build.bat

if %errorlevel% neq 0 goto end

cd ..

cd AnyVODAnalyserMKV
call build.bat

if %errorlevel% neq 0 goto end

cd ..

:end
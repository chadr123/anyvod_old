/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

// dllmain.cpp : DLL 응용 프로그램의 진입점을 정의합니다.
#include "stdafx.h"
#include "..\..\..\..\common\Analyser.h"
#include "AnyVODAnalyserAVI.h"

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
		                 )
{
  switch (ul_reason_for_call)
  {
  case DLL_PROCESS_ATTACH:
    {
      Analyser::SetInstance(new AnyVODAnalyserAVI(hModule));

      break;

    }
  case DLL_THREAD_ATTACH:
    {
      break;

    }
  case DLL_THREAD_DETACH:
    {
      break;

    }
  case DLL_PROCESS_DETACH:
    {
      delete Analyser::GetInstance();
      Analyser::SetInstance(NULL);

      break;

    }

  }

  return TRUE;

}

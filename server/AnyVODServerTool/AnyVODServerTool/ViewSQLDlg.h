/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

// CViewSQLDlg 대화 상자입니다.

class CViewSQLDlg : public CDialog
{
	DECLARE_DYNAMIC(CViewSQLDlg)

public:
	CViewSQLDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CViewSQLDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_VIEW_SQL_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
  CRichEditCtrl m_sql;

  afx_msg void OnBnClickedButtonViewSqlClose();
  virtual BOOL OnInitDialog();
};

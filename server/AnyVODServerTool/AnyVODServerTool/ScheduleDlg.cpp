/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

// ScheduleDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "ScheduleDlg.h"
#include "AnyVODServerToolDlg.h"
#include "ScheduleAddDlg.h"
#include "ScheduleModifyDlg.h"
#include "Socket.h"
#include "Util.h"

// CScheduleDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CScheduleDlg, CDialog)

CScheduleDlg::CScheduleDlg(CWnd* pParent /*=NULL*/)
	: CParentDlg(CScheduleDlg::IDD, pParent), m_itemCount(0), m_itemRunning(0), m_itemSleeping(0)
{

}

CScheduleDlg::~CScheduleDlg()
{
}

void CScheduleDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LIST_SCHEDULE_SCHEDULELIST, m_scheduleList);
  DDX_Control(pDX, IDC_STATIC_SCHEDULE_TOTAL, m_total);
  DDX_Control(pDX, IDC_STATIC_SCHEDULE_RUNNING, m_running);
  DDX_Control(pDX, IDC_STATIC_SCHEDULE_SLEEPING, m_sleeping);
  DDX_Control(pDX, IDC_BUTTON_SCHEDULE_RESTART, m_restart);
  DDX_Control(pDX, IDC_BUTTON_SCHEDULE_CLOSE, m_close);
  DDX_Control(pDX, IDC_BUTTON_SCHEDULE_STARTNOW, m_startNow);
  DDX_Control(pDX, IDC_BUTTON_SCHEDULE_DELETE, m_delete);
  DDX_Control(pDX, IDC_BUTTON_SCHEDULE_MODIFY, m_modify);
  DDX_Control(pDX, IDC_BUTTON_SCHEDULE_ADD, m_add);
  DDX_Control(pDX, IDC_BUTTON_SCHEDULE_REFRESH, m_refresh);
}


BEGIN_MESSAGE_MAP(CScheduleDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_REFRESH, &CScheduleDlg::OnBnClickedButtonScheduleRefresh)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_ADD, &CScheduleDlg::OnBnClickedButtonScheduleAdd)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_MODIFY, &CScheduleDlg::OnBnClickedButtonScheduleModify)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_DELETE, &CScheduleDlg::OnBnClickedButtonScheduleDelete)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_STARTNOW, &CScheduleDlg::OnBnClickedButtonScheduleStartnow)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_CLOSE, &CScheduleDlg::OnBnClickedButtonScheduleClose)
  ON_BN_CLICKED(IDC_BUTTON_SCHEDULE_RESTART, &CScheduleDlg::OnBnClickedButtonScheduleRestart)
  ON_NOTIFY(NM_CLICK, IDC_LIST_SCHEDULE_SCHEDULELIST, &CScheduleDlg::OnNMClickListScheduleSchedulelist)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST_SCHEDULE_SCHEDULELIST, &CScheduleDlg::OnNMDblclkListScheduleSchedulelist)
  ON_NOTIFY(NM_RETURN, IDC_LIST_SCHEDULE_SCHEDULELIST, &CScheduleDlg::OnNMReturnListScheduleSchedulelist)
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_SCHEDULE_SCHEDULELIST, &CScheduleDlg::OnLvnItemchangedListScheduleSchedulelist)
END_MESSAGE_MAP()

CListCtrl& CScheduleDlg::GetScheduleList()
{
  return this->m_scheduleList;

}

void CScheduleDlg::InitControls()
{
  this->UpdateTotal();

  this->m_scheduleList.InsertColumn(0, L"종류", LVCFMT_LEFT, 126);
  this->m_scheduleList.InsertColumn(1, L"분", LVCFMT_CENTER, 30);
  this->m_scheduleList.InsertColumn(2, L"시", LVCFMT_CENTER, 30);
  this->m_scheduleList.InsertColumn(3, L"일", LVCFMT_CENTER, 30);
  this->m_scheduleList.InsertColumn(4, L"월", LVCFMT_CENTER, 30);
  this->m_scheduleList.InsertColumn(5, L"요일", LVCFMT_CENTER, 37);
  this->m_scheduleList.InsertColumn(6, L"상태", LVCFMT_CENTER, 55);
  this->m_scheduleList.InsertColumn(7, L"시작/종료 시각", LVCFMT_CENTER, 144);

  this->m_scheduleList.SetExtendedStyle(LVS_EX_GRIDLINES| LVS_EX_FULLROWSELECT);
  this->m_scheduleList.SetImageList(&Util::GetIconImageList(), LVSIL_SMALL);

  this->EnableControls(true);

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendScheduleList();
  this->ProcessPacket();

}

void CScheduleDlg::EnableControls(bool enable)
{
  this->m_total.EnableWindow(enable);
  this->m_running.EnableWindow(enable);
  this->m_sleeping.EnableWindow(enable);
  this->m_restart.EnableWindow(enable);
  this->m_close.EnableWindow(enable);
  this->m_scheduleList.EnableWindow(enable);
  this->m_refresh.EnableWindow(enable);
  this->m_add.EnableWindow(enable);

  UINT command = MF_BYCOMMAND;

  command |= enable ? MF_ENABLED : MF_DISABLED;

  this->GetSystemMenu(FALSE)->EnableMenuItem(SC_CLOSE, command);

  if (enable)
  {
    if (this->m_scheduleList.GetSelectedCount() != 0)
    {
      this->m_startNow.EnableWindow(true);
      this->m_delete.EnableWindow(true);
      this->m_modify.EnableWindow(true);

      return;

    }

  }

  this->m_startNow.EnableWindow(false);
  this->m_delete.EnableWindow(false);
  this->m_modify.EnableWindow(false);

  if (this->GetFocus() == NULL)
  {
    this->SetFocus();

  }

}

void CScheduleDlg::UpdateTotal()
{
  CString count;

  count.Format(L"%d", this->m_itemCount);
  Util::NumberFormatting(&count);
  count += L" 개";
  this->m_total.SetWindowText(count);

  count.Format(L"%d", this->m_itemRunning);
  Util::NumberFormatting(&count);
  count += L" 개";
  this->m_running.SetWindowText(count);

  count.Format(L"%d", this->m_itemSleeping);
  Util::NumberFormatting(&count);
  count += L" 개";
  this->m_sleeping.SetWindowText(count);


}

void CScheduleDlg::AddScheduleToList(unsigned int number, char name[MAX_SCHEDULE_NAME_CHAR_SIZE],
                                     char elements[TI_COUNT][MAX_SCHEDULE_FIELD_SIZE], bool running,
                                     ANYVOD_TIME &startedTime, ANYVOD_TIME &endedTime)
{
  LVITEM lvi;
  wstring tmp;

  Util::ConvertAsciiToUnicode(name, &tmp);

  lvi.mask = LVIF_TEXT | LVIF_PARAM | LVIF_IMAGE;
  lvi.iItem = number;
  lvi.iSubItem = 0;
  lvi.pszText = (LPWSTR)tmp.c_str();
  lvi.lParam = number;
  lvi.iImage = ICON_INDEX_SCHEDULE;

  this->m_scheduleList.InsertItem(&lvi);

  lvi.mask = LVIF_TEXT;

  for (int i = TI_MINUTE; i < TI_COUNT; i++)
  {
    wstring element;

    Util::ConvertAsciiToUnicode(elements[i], &element);

    lvi.iSubItem = i+1;
    lvi.pszText = (LPWSTR)element.c_str();

    this->m_scheduleList.SetItem(&lvi);

  }

  lvi.iSubItem++;

  wstringstream ss;

  if (running)
  {
    lvi.pszText = L"실행 중";
    this->m_itemRunning++;

    ss << setw(4) << setfill(L'0') <<
      startedTime.year << L"/" << setw(2) << setfill(L'0') <<
      startedTime.month << L"/" << setw(2) << setfill(L'0') <<
      startedTime.day << L" " << setw(2) << setfill(L'0') <<
      startedTime.hour << L":" << setw(2) << setfill(L'0') <<
      startedTime.minute << L":" << setw(2) << setfill(L'0') <<
      startedTime.second << L"." << setw(4) << setfill(L'0') <<
      startedTime.milliseconds << endl;

  }
  else
  {
    lvi.pszText = L"대기 중";
    this->m_itemSleeping++;

    ss << setw(4) << setfill(L'0') <<
      endedTime.year << L"/" << setw(2) << setfill(L'0') <<
      endedTime.month << L"/" << setw(2) << setfill(L'0') <<
      endedTime.day << L" " << setw(2) << setfill(L'0') <<
      endedTime.hour << L":" << setw(2) << setfill(L'0') <<
      endedTime.minute << L":" << setw(2) << setfill(L'0') <<
      endedTime.second << L"." << setw(4) << setfill(L'0') <<
      endedTime.milliseconds << endl;

  }

  this->m_scheduleList.SetItem(&lvi);

  lvi.iSubItem++;

  wstring time = ss.str();

  lvi.pszText = (LPWSTR)time.c_str();

  this->m_scheduleList.SetItem(&lvi);

}

bool CScheduleDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_ADMIN_S2C_SCHEDULE_DELETE:
    {
      break;

    }
  case PT_ADMIN_S2C_SCHEDULE_STARTNOW:
    {
      break;

    }
  case PT_ADMIN_S2C_SCHEDULELIST:
    {
      if (packet->s2c_header.success)
      {
        this->m_itemCount = ntohl(packet->admin_s2c_schedulelist.count);
        this->m_itemRunning = 0;
        this->m_itemSleeping = 0;

        this->m_scheduleList.DeleteAllItems();

        return this->m_itemCount > 0;

      }

      break;

    }
  case PT_ADMIN_S2C_SCHEDULE_ITEM:
    {
      if (packet->s2c_header.success)
      {
        ANYVOD_TIME startTime;
        ANYVOD_TIME endTime;

        startTime.day = ntohs(packet->admin_s2c_schedule_item.startedTime.day);
        startTime.hour = ntohs(packet->admin_s2c_schedule_item.startedTime.hour);
        startTime.milliseconds = ntohs(packet->admin_s2c_schedule_item.startedTime.milliseconds);
        startTime.minute = ntohs(packet->admin_s2c_schedule_item.startedTime.minute);
        startTime.month = ntohs(packet->admin_s2c_schedule_item.startedTime.month);
        startTime.second = ntohs(packet->admin_s2c_schedule_item.startedTime.second);
        startTime.weekday = ntohs(packet->admin_s2c_schedule_item.startedTime.weekday);
        startTime.year = ntohs(packet->admin_s2c_schedule_item.startedTime.year);

        endTime.day = ntohs(packet->admin_s2c_schedule_item.endedTime.day);
        endTime.hour = ntohs(packet->admin_s2c_schedule_item.endedTime.hour);
        endTime.milliseconds = ntohs(packet->admin_s2c_schedule_item.endedTime.milliseconds);
        endTime.minute = ntohs(packet->admin_s2c_schedule_item.endedTime.minute);
        endTime.month = ntohs(packet->admin_s2c_schedule_item.endedTime.month);
        endTime.second = ntohs(packet->admin_s2c_schedule_item.endedTime.second);
        endTime.weekday = ntohs(packet->admin_s2c_schedule_item.endedTime.weekday);
        endTime.year = ntohs(packet->admin_s2c_schedule_item.endedTime.year);

        int number = ntohl(packet->admin_s2c_schedule_item.number);

        this->AddScheduleToList(number,
          (char*)packet->admin_s2c_schedule_item.name,
          (char(*)[MAX_SCHEDULE_FIELD_SIZE])packet->admin_s2c_schedule_item.elements,
          packet->admin_s2c_schedule_item.running != 0,
          startTime, endTime);

        if (number == this->m_itemCount-1)
        {
          this->UpdateTotal();

          return false;

        }
        else
        {
          return true;

        }

      }

      break;

    }
  case PT_ADMIN_S2C_RESTART_SCHEDULER:
    {
      return true;

    }
  case PT_ADMIN_S2C_RESTART_SCHEDULER_COMPLETE:
    {
      if (packet->s2c_header.success)
      {
        AfxMessageBox(L"스케줄러가 정상적으로 재시작 되었습니다.");

      }

      this->EnableControls(true);

      break;

    }

  }

  return false;

}

bool CScheduleDlg::ProcessPacket()
{
  bool nextProcess = false;
  bool success = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, &success))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

  return success;

}

// CScheduleDlg 메시지 처리기입니다.

void CScheduleDlg::OnBnClickedButtonScheduleRefresh()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

  dlg->GetSocket().SendScheduleList();
  this->ProcessPacket();

  this->EnableControls(true);

}

void CScheduleDlg::OnBnClickedButtonScheduleAdd()
{
  CScheduleAddDlg dlg(this);
  INT_PTR result = dlg.DoModal();

  if (result == IDCLOSE)
  {
    this->EndDialog(IDCLOSE);

  }
  else if (result == IDOK)
  {
    this->m_itemCount++;

    this->UpdateTotal();

    Util::ListControlScrollToEnd(this->m_scheduleList);

  }

  this->EnableControls(true);

}

void CScheduleDlg::OnBnClickedButtonScheduleModify()
{
  CScheduleModifyDlg dlg(this);
  int count = this->m_scheduleList.GetSelectedCount();

  for (int i = 0; i < count; i++)
  {
    POSITION pos = this->m_scheduleList.GetFirstSelectedItemPosition();
    int item = this->m_scheduleList.GetNextSelectedItem(pos);
    INT_PTR result = dlg.DoModal();

    if (result == IDCLOSE)
    {
      this->EndDialog(IDCLOSE);

      break;

    }
    else if (result == IDABORT)
    {
      break;

    }

    this->m_scheduleList.SetItemState(item, 0, LVIS_SELECTED | LVIS_FOCUSED);

  }

  this->EnableControls(true);

}

void CScheduleDlg::OnBnClickedButtonScheduleDelete()
{
  POSITION pos = this->m_scheduleList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    if (AfxMessageBox(L"스케줄을 삭제 하시겠습니까?", MB_YESNO) == IDYES)
    {
      CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

      while (pos)
      {
        int item = this->m_scheduleList.GetNextSelectedItem(pos);
        unsigned int index = this->m_scheduleList.GetItemData(item);

        dlg->GetSocket().SendScheduleDelete(index);

        if (this->ProcessPacket())
        {
          this->m_scheduleList.DeleteItem(item);
          this->m_itemCount--;
          this->m_itemSleeping--;

          for (int i = item; i < this->m_scheduleList.GetItemCount(); i++)
          {
            DWORD_PTR data = this->m_scheduleList.GetItemData(item);

            data--;

            this->m_scheduleList.SetItemData(item, data);

          }

          pos = this->m_scheduleList.GetFirstSelectedItemPosition();

        }
        else
        {
          if (pos != NULL)
          {
            if (AfxMessageBox(L"스케줄을 삭제 하는 도중 오류가 발생 했습니다. 계속 하시겠습니까?", MB_YESNO) == IDNO)
            {
              break;

            }

          }

        }

      }

      this->UpdateTotal();

    }

    this->EnableWindow(true);

  }
  else
  {
    AfxMessageBox(L"선택된 스케줄이 없습니다.");

  }

}

void CScheduleDlg::OnBnClickedButtonScheduleStartnow()
{
  POSITION pos = this->m_scheduleList.GetFirstSelectedItemPosition();

  if (pos != NULL)
  {
    if (AfxMessageBox(L"지금 시작 하시겠습니까?", MB_YESNO) == IDYES)
    {
      CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

      while (pos)
      {
        int item = this->m_scheduleList.GetNextSelectedItem(pos);
        unsigned int index = this->m_scheduleList.GetItemData(item);

        dlg->GetSocket().SendScheduleStartNow(index);

        if (!this->ProcessPacket())
        {
          if (pos != NULL)
          {
            if (AfxMessageBox(L"스케줄을 지금 시작 하는 도중 오류가 발생 했습니다. 계속 하시겠습니까?", MB_YESNO) == IDNO)
            {
              break;

            }

          }

        }

      }

      AfxMessageBox(L"스케줄이 시작 되었습니다.");

      this->OnBnClickedButtonScheduleRefresh();

    }

  }
  else
  {
    AfxMessageBox(L"선택된 스케줄이 없습니다.");

  }

}

void CScheduleDlg::OnBnClickedButtonScheduleClose()
{
  this->EndDialog(IDOK);

}

void CScheduleDlg::OnBnClickedButtonScheduleRestart()
{
  if (AfxMessageBox(L"재시작 하시겠습니까?", MB_YESNO) == IDYES)
  {
    CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd;

    this->EnableControls(false);

    dlg->GetSocket().SendRestartScheduler();
    this->ProcessPacket();

    dlg->GetSocket().SendScheduleList();
    this->ProcessPacket();

  }

}

void CScheduleDlg::OnNMClickListScheduleSchedulelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->EnableControls(true);

  *pResult = 0;
}

void CScheduleDlg::OnNMDblclkListScheduleSchedulelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

  this->OnBnClickedButtonScheduleModify();

  *pResult = 0;
}

void CScheduleDlg::OnNMReturnListScheduleSchedulelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  this->OnBnClickedButtonScheduleModify();

  *pResult = 0;
}

void CScheduleDlg::OnLvnItemchangedListScheduleSchedulelist(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

  if (pNMLV->uOldState == 0)
  {
    this->EnableControls(true);

  }

  *pResult = 0;
}

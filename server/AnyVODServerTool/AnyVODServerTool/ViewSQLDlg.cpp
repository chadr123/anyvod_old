/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

// ViewSQLDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "ViewSQLDlg.h"
#include "DatabaseStatementDlg.h"

// CViewSQLDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CViewSQLDlg, CDialog)

CViewSQLDlg::CViewSQLDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CViewSQLDlg::IDD, pParent)
{

}

CViewSQLDlg::~CViewSQLDlg()
{
}

void CViewSQLDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_RICHEDIT_VIEW_SQL_SQL, m_sql);
}


BEGIN_MESSAGE_MAP(CViewSQLDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_VIEW_SQL_CLOSE, &CViewSQLDlg::OnBnClickedButtonViewSqlClose)
END_MESSAGE_MAP()


// CViewSQLDlg 메시지 처리기입니다.

void CViewSQLDlg::OnBnClickedButtonViewSqlClose()
{
  this->EndDialog(IDCANCEL);
}

BOOL CViewSQLDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  CDatabaseStatementDlg *parent = (CDatabaseStatementDlg*)this->m_pParentWnd;
  CListCtrl &list = parent->GetStatementList();
  POSITION pos = list.GetFirstSelectedItemPosition();

  this->m_sql.SendMessage(EM_SETLANGOPTIONS, 0,
    (LPARAM)(this->m_sql.SendMessage(EM_GETLANGOPTIONS, 0, 0) & ~IMF_AUTOFONT));

  if (pos != NULL)
  {
    int item = list.GetNextSelectedItem(pos);
    wstring *sql = (wstring*)list.GetItemData(item);

    this->m_sql.SetWindowText(sql->c_str());

  }
  else
  {
    AfxMessageBox(L"항목을 선택 해 주세요.");

    this->EndDialog(IDCANCEL);

  }

  return TRUE;  // return TRUE unless you set the focus to a control
  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

// GroupManagementAddDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "GroupManagementAddDlg.h"
#include "GroupManagementDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CGroupManagementAddDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CGroupManagementAddDlg, CDialog)

CGroupManagementAddDlg::CGroupManagementAddDlg(CWnd* pParent /*=NULL*/)
	: CParentDlg(CGroupManagementAddDlg::IDD, pParent)
{

}

CGroupManagementAddDlg::~CGroupManagementAddDlg()
{
}

void CGroupManagementAddDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_EDIT_GROUP_MANAGEMENT_ADD_NAME, m_name);
}


BEGIN_MESSAGE_MAP(CGroupManagementAddDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_ADD_CONFIRM_EXIST, &CGroupManagementAddDlg::OnBnClickedButtonGroupManagementAddConfirmExist)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_ADD_ADD, &CGroupManagementAddDlg::OnBnClickedButtonGroupManagementAddAdd)
  ON_BN_CLICKED(IDC_BUTTON_GROUP_MANAGEMENT_ADD_CANCEL, &CGroupManagementAddDlg::OnBnClickedButtonGroupManagementAddCancel)
END_MESSAGE_MAP()

void CGroupManagementAddDlg::InitControls()
{
  this->m_name.SetLimitText(MAX_GROUP_NAME_CHAR_SIZE);

}

bool CGroupManagementAddDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  ANYVOD_PACKET_TYPE type = (ANYVOD_PACKET_TYPE)ntohl(packet->s2c_header.header.type);

  if (type == PT_ADMIN_S2C_GROUP_ADD)
  {
    if (packet->s2c_header.success)
    {
      CGroupManagementDlg *dlg = (CGroupManagementDlg*)this->m_pParentWnd;
      ANYVOD_PACKET_GROUP_INFO info;
      CString name;

      this->m_name.GetWindowText(name);

      Util::ConvertUnicodeToUTF8(name.GetString(), (char*)info.name, MAX_GROUP_NAME_SIZE);

      dlg->AddGroupToList(ntohll(packet->admin_s2c_group_add.pid), info);

      this->EndDialog(IDOK);

    }

  }
  else if (type == PT_ADMIN_S2C_GROUP_EXIST)
  {
    if (packet->s2c_header.success)
    {
      if (packet->admin_s2c_group_exist.exist)
      {
        AfxMessageBox(L"사용 불가능한 그룹 이름입니다.");

      }
      else
      {
        AfxMessageBox(L"사용 가능한 그룹 이름입니다.");

      }

    }

  }

  return false;

}

void CGroupManagementAddDlg::ProcessPacket()
{
  bool nextProcess = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, NULL))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

}

// CGroupManagementAddDlg 메시지 처리기입니다.

BOOL CGroupManagementAddDlg::PreTranslateMessage(MSG* pMsg)
{
  return CDialog::PreTranslateMessage(pMsg);
}

void CGroupManagementAddDlg::OnOK()
{
  this->OnBnClickedButtonGroupManagementAddAdd();

  //CParentDlg::OnOK();
}

void CGroupManagementAddDlg::OnCancel()
{
  this->OnBnClickedButtonGroupManagementAddCancel();

  CParentDlg::OnCancel();
}

void CGroupManagementAddDlg::OnBnClickedButtonGroupManagementAddConfirmExist()
{
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  CString name;

  this->m_name.GetWindowText(name);

  name.Trim();

  if (name.GetLength() > 0)
  {
    dlg->GetSocket().SendGroupExist(name);
    this->ProcessPacket();

  }
  else
  {
    AfxMessageBox(L"이름을 입력해 주세요.");

    this->m_name.SetFocus();

  }

}

void CGroupManagementAddDlg::OnBnClickedButtonGroupManagementAddAdd()
{
  CString name;

  this->m_name.GetWindowText(name);

  name.Trim();

  if (name.GetLength() == 0)
  {
    AfxMessageBox(L"이름을 입력해 주세요.");

    this->m_name.SetFocus();

    return;


  }

  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  ANYVOD_GROUP_INFO info;

  info.name = name.GetString();

  dlg->GetSocket().SendGroupAdd(info);
  this->ProcessPacket();

}

void CGroupManagementAddDlg::OnBnClickedButtonGroupManagementAddCancel()
{
  this->EndDialog(IDCANCEL);

}


/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

// LogViewDlg.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "AnyVODServerTool.h"
#include "LogViewDlg.h"
#include "LogDlg.h"
#include "AnyVODServerToolDlg.h"
#include "Socket.h"
#include "Util.h"

// CLogViewDlg 대화 상자입니다.

IMPLEMENT_DYNAMIC(CLogViewDlg, CDialog)

CLogViewDlg::CLogViewDlg(CWnd* pParent /*=NULL*/)
	: CParentDlg(CLogViewDlg::IDD, pParent)
{

}

CLogViewDlg::~CLogViewDlg()
{
}

void CLogViewDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_STATIC_LOG_VIEW_SIZE, m_size);
  DDX_Control(pDX, IDC_EDIT_LOG_VIEW_LOG, m_log);
  DDX_Control(pDX, IDC_BUTTON_LOG_VIEW_CLOSE, m_close);
}



BEGIN_MESSAGE_MAP(CLogViewDlg, CDialog)
  ON_BN_CLICKED(IDC_BUTTON_LOG_VIEW_CLOSE, &CLogViewDlg::OnBnClickedButtonLogViewClose)
  ON_BN_CLICKED(IDC_BUTTON_LOG_VIEW_REFRESH, &CLogViewDlg::OnBnClickedButtonLogViewRefresh)
  ON_WM_CTLCOLOR()
END_MESSAGE_MAP()

void CLogViewDlg::InitControls()
{
  CLogDlg *dlg = (CLogDlg*)this->m_pParentWnd;
  CListCtrl &list = dlg->GetLogList();
  POSITION pos = list.GetFirstSelectedItemPosition();

  this->m_size.SetWindowText(L"0 Bytes");
  this->m_log.SetWindowText(L"");

  this->m_log.SendMessage(EM_SETLANGOPTIONS, 0,
    (LPARAM)(this->m_log.SendMessage(EM_GETLANGOPTIONS, 0, 0) & ~IMF_AUTOFONT));

  if (pos != NULL)
  {
    int item = list.GetNextSelectedItem(pos);
    CString fileName = list.GetItemText(item, 0);
    CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();

    this->m_log.SetLimitText(-1);

    dlg->GetSocket().SendLogView(fileName.GetString());
    this->ProcessPacket();

    wstring tmp;

    Util::ConvertUTF8ToUnicode((const char*)&this->m_logBuf[0], this->m_logBuf.size(), &tmp);
    Util::AppendContentsToEdit(tmp, this->m_log);

    this->m_size.SetFocus();

  }
  else
  {
    AfxMessageBox(L"선택된 로그가 없습니다.");

    this->EndDialog(IDCANCEL);

  }

}

bool CLogViewDlg::HandlePacket(ANYVOD_PACKET *packet)
{
  switch (ntohl(packet->s2c_header.header.type))
  {
  case PT_ADMIN_S2C_FILE_START:
    {
      if (packet->s2c_header.success)
      {
        FILE_START_TYPE type = (FILE_START_TYPE)ntohl(packet->admin_s2c_file_start.type);
        unsigned long long size = ntohll(packet->admin_s2c_file_start.totalSize);

        if (type == FST_LOG)
        {
          CString totalSize;

          totalSize.Format(L"%llu", size);
          Util::NumberFormatting(&totalSize);
          totalSize += L" Bytes";

          this->m_size.SetWindowText(totalSize);
          this->m_log.SetWindowText(L"");
          this->m_logBuf.clear();

        }
        else
        {
        }

        return true;

      }

      break;

    }
  case PT_ADMIN_S2C_FILE_STREAM:
    {
      if (packet->s2c_header.success)
      {
        uint32_t size = ntohl(packet->admin_s2c_file_stream.size);

        if (packet->admin_s2c_file_stream.type == FST_LOG)
        {
          this->m_logBuf.insert(this->m_logBuf.end(),
            &packet->admin_s2c_file_stream.data[0], &packet->admin_s2c_file_stream.data[size]);

        }
        else
        {
        }

        return true;

      }

      break;

    }
  case PT_ADMIN_S2C_FILE_END:
    {
      break;

    }
  case PT_ADMIN_S2C_LOG_VIEW:
    {
      if (packet->s2c_header.success)
      {
        return true;

      }

      break;

    }

  }

  return false;

}

void CLogViewDlg::ProcessPacket()
{
  bool nextProcess = false;
  CAnyVODServerToolDlg *dlg = (CAnyVODServerToolDlg*)this->m_pParentWnd->GetParent();
  Socket &socket = dlg->GetSocket();

  do
  {
    if (!Util::HandlePacket(socket, this, &nextProcess, NULL))
    {
      this->EndDialog(IDCLOSE);

      break;

    }

  } while (nextProcess);

}

// CLogViewDlg 메시지 처리기입니다.

void CLogViewDlg::OnBnClickedButtonLogViewClose()
{
  this->EndDialog(IDCANCEL);

}

void CLogViewDlg::OnBnClickedButtonLogViewRefresh()
{
  this->InitControls();

}

HBRUSH CLogViewDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
  HBRUSH hbr = CParentDlg::OnCtlColor(pDC, pWnd, nCtlColor);

  if (nCtlColor == CTLCOLOR_STATIC)
  {
    UINT id = pWnd->GetDlgCtrlID();

    if (id == IDC_EDIT_LOG_VIEW_LOG)
    {
      if (pWnd->GetStyle() & ES_READONLY && pWnd->IsWindowEnabled())
      {
        pDC->SetBkColor(RGB(255, 255, 255));
        hbr = (HBRUSH)::GetStockObject(WHITE_BRUSH);

      }

    }

  }

  return hbr;
}

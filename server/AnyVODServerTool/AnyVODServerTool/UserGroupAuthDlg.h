/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "ParentDlg.h"

struct ANYVOD_USER_GROUP_INFO;
union ANYVOD_PACKET;

// CUserGroupAuthDlg 대화 상자입니다.

class CUserGroupAuthDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CUserGroupAuthDlg)

public:
	CUserGroupAuthDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CUserGroupAuthDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_USER_GRUOP_AUTH_DIALOG };

private:
  unsigned int m_groupItemCount;
  unsigned int m_groupItemCurCount;
  unsigned int m_userItemCount;
  unsigned int m_userItemCurCount;
  unsigned int m_authItemCount;
  unsigned int m_authItemCurCount;
  wstring m_lastAddUserID;
  wstring m_lastAddGroupName;

private:
  void DeleteGroupItems();
  void DeleteUserItems();
  void DeleteUserGroupAuthItems();
  void ToggleAddControls();
  void ToggleDeleteControls();
  void DeselectAddControls();
  void DeselectDeleteControls();
  bool ProcessPacket();
  void AddAuthItemToList(ANYVOD_USER_GROUP_INFO *info);
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
  CListCtrl m_userList;
  CListCtrl m_groupList;
  CListCtrl m_authList;
  CButton m_addAuth;
  CButton m_deleteAuth;
  afx_msg void OnBnClickedButtonUserGroupAuthClose();
  afx_msg void OnClose();
  afx_msg void OnNMClickListUserGroupAuthUserlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMClickListUserGroupAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMClickListUserGroupAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnGetdispinfoListUserGroupAuthUserlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnBnClickedButtonUserGroupAuthAddauth();
  afx_msg void OnBnClickedButtonUserGroupAuthDeleteauth();
  afx_msg void OnBnClickedButtonUserGroupAuthRefresh();
  afx_msg void OnLvnItemchangedListUserGroupAuthUserlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedListUserGroupAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedListUserGroupAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReturnListUserGroupAuthUserlist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReturnListUserGroupAuthGrouplist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReturnListUserGroupAuthAuthlist(NMHDR *pNMHDR, LRESULT *pResult);
};

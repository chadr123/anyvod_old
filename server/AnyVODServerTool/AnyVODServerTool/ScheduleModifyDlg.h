/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "ParentDlg.h"

union ANYVOD_PACKET;

// CScheduleModifyDlg 대화 상자입니다.

class CScheduleModifyDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CScheduleModifyDlg)

public:
	CScheduleModifyDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CScheduleModifyDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SCHEDULE_MODIFY_DIALOG };

private:
  unsigned int m_number;

private:
  void ProcessPacket();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
  CEdit m_minute;
  CEdit m_hour;
  CEdit m_day;
  CEdit m_month;
  CEdit m_weekday;
  CButton m_minuteAll;
  CButton m_hourAll;
  CButton m_dayAll;
  CButton m_monthAll;
  CButton m_weekdayAll;
  CComboBox m_types;
  CButton m_stopMultiModify;

  afx_msg void OnBnClickedButtonScheduleModifyClose();
  afx_msg void OnBnClickedButtonScheduleModifyModify();
  afx_msg void OnBnClickedCheckScheduleModifyMinuteAll();
  afx_msg void OnBnClickedCheckScheduleModifyHourAll();
  afx_msg void OnBnClickedCheckScheduleModifyDayAll();
  afx_msg void OnBnClickedCheckScheduleModifyMonthAll();
  afx_msg void OnBnClickedCheckScheduleModifyWeekdayAll();
  afx_msg void OnBnClickedButtonScheduleModifyStopMultipleModify();
  virtual BOOL PreTranslateMessage(MSG* pMsg);
protected:
  virtual void OnOK();
  virtual void OnCancel();

};

/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "ParentDlg.h"
#include "..\..\..\common\types.h"

union ANYVOD_PACKET;

// CScheduleDlg 대화 상자입니다.

class CScheduleDlg : public CParentDlg
{
	DECLARE_DYNAMIC(CScheduleDlg)

public:
	CScheduleDlg(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CScheduleDlg();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_SCHEDULE_DIALOG };

public:
  CListCtrl& GetScheduleList();
  void AddScheduleToList(unsigned int number, char name[MAX_SCHEDULE_NAME_CHAR_SIZE],
    char elements[TI_COUNT][MAX_SCHEDULE_FIELD_SIZE], bool running,
    ANYVOD_TIME &startedTime, ANYVOD_TIME &endedTime);

private:
  void EnableControls(bool enable);
  bool ProcessPacket();
  void UpdateTotal();
  virtual bool HandlePacket(ANYVOD_PACKET *packet);
  virtual void InitControls();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
  CStatic m_total;
  CStatic m_running;
  CStatic m_sleeping;
  CButton m_restart;
  CButton m_close;
  CButton m_startNow;
  CButton m_delete;
  CButton m_modify;
  CButton m_add;
  CButton m_refresh;
  CListCtrl m_scheduleList;
  unsigned int m_itemCount;
  unsigned int m_itemRunning;
  unsigned int m_itemSleeping;

  afx_msg void OnBnClickedButtonScheduleRefresh();
  afx_msg void OnBnClickedButtonScheduleAdd();
  afx_msg void OnBnClickedButtonScheduleModify();
  afx_msg void OnBnClickedButtonScheduleDelete();
  afx_msg void OnBnClickedButtonScheduleStartnow();
  afx_msg void OnBnClickedButtonScheduleClose();
  afx_msg void OnBnClickedButtonScheduleRestart();
  afx_msg void OnNMClickListScheduleSchedulelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMDblclkListScheduleSchedulelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMReturnListScheduleSchedulelist(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnItemchangedListScheduleSchedulelist(NMHDR *pNMHDR, LRESULT *pResult);
};

/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "..\..\..\common\AsyncWinSocket.h"

union ANYVOD_PACKET;

class Socket : public AsyncWinSocket
{
public:
  Socket();
  virtual ~Socket();

  bool Create();
  bool Connect(int nPort, const char* szIP = NULL);
  void Disconnect();
  bool BeginResult(ANYVOD_PACKET **packet);
  void EndResult();
  void WaitForRecv();
  void SetTicket(string &ticket);
  void GetTicket(string *ticket);

  void SendLogin(CString &id, CString &pass);
  void SendLogout();
  void SendFileList(const wstring &path);
  void SendNetworkStatistics();
  void SendLastLog();
  void SendEnv();
  void SendRestartScheduler();
  void SendScheduleList();
  void SendScheduleStartNow(unsigned int index);
  void SendScheduleTypes();
  void SendScheduleDelete(unsigned int index);
  void SendScheduleAdd(string &script);
  void SendScheduleModify(unsigned int number, string &script);
  void SendLogList();
  void SendLogDelete(const wstring &fileName);
  void SendLogView(const wstring &fileName);
  void SendUserList();
  void SendUserDelete(unsigned long long pid);
  void SendUserExist(CString &id);
  void SendUserAdd(ANYVOD_USER_INFO &info);
  void SendUserModify(unsigned long long pid, ANYVOD_USER_INFO &info);
  void SendClientStatusList();
  void SendClientLogout(const wstring &ip, unsigned short port);
  void SendGroupList();
  void SendGroupDelete(unsigned long long pid);
  void SendGroupExist(CString &name);
  void SendGroupAdd(ANYVOD_GROUP_INFO &info);
  void SendGroupModify(unsigned long long pid, ANYVOD_GROUP_INFO &info);
  void SendUserGroupList();
  void SendUserGroupAdd(unsigned long long userPID, wstring &groupName);
  void SendUserGroupDelete(unsigned long long pid);
  void SendFileGroupList(const wstring &filePath);
  void SendFileGroupAdd(const wstring &filePath, const wstring &groupName);
  void SendFileGroupDelete(const wstring &filePath, const wstring &groupName);
  void SendStatementList();
  void SendSubtitleExist(const wstring &filePath);
  void SendSubtitleURL(const wstring &filePath);
  void SendRebuildMetaData();
  void SendRebuildMediaData();

private:
  HANDLE m_connectEvent;
  HANDLE m_recvEvent;
  HANDLE m_pickEvent;
  char *m_buf;
  string m_ticket;

private:
  virtual void OnReceive(const ANYVOD_PACKET& packet);
  virtual void OnDisconnect();
  virtual void OnConnect(bool bRet);

  void FillTicket(ANYVOD_PACKET *packet);

};

@echo off

rem Usage: AnyVODServerStarter.exe OPTIONS LOCATION [EMAIL SMTP PORT ID PASSWORD STARTTLS]

rem OPTIONS  : Arguments that pass to server process.
rem            Currently available options are "start" or ""(empty)
rem LOCATION : Path of server executable.(relative path or absolute path)
rem EMAIL    : Email address that is used to report crashing logs of server process.
rem SMTP     : SMTP server address that is used to report crashing logs of server process.
rem PORT     : SMTP server port that is used to report crashing logs of server process.
rem ID       : Account of email that is used to report crashing logs of server process.
rem PASSWORD : Password of email that is used to report crashing logs of server process.
rem STARTTLS : Use StartTLS("true" or "false")

AnyVODServerStarter.exe start AnyVODServer.exe recv@email.com smtp.gmail.com 587 emailID emailPassword true
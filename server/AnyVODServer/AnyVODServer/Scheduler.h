/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "Thread.h"
#include "Common.h"

class ScheduleInvoker;
class Server;

class Scheduler : public Thread
{
public:
  struct JOB_STATUS
  {
    wstring name;
    bool run;
    SYSTEMTIME startedTime;
    SYSTEMTIME endedTime;
    wstring script;

  };

  enum SCHEDULE_FAIL_REASON
  {
    SFR_NONE = 0,
    SFR_RUNNING,
    SFR_NOT_EXIST,
    SFR_INVALID_SCRIPT,
    SFR_FILE_WRITE_FAILD,

    SFR_COUNT

  };

  typedef vector<JOB_STATUS> VECTOR_JOB_STATUS;
  typedef VECTOR_JOB_STATUS::iterator VECTOR_JOB_STATUS_ITERATOR;

  Scheduler();
  ~Scheduler();

  bool Start();
  void Stop();

  bool RunningSchedules(unsigned int *index);

  void SetServer(Server *server);
  void GetJobStatus(Scheduler::VECTOR_JOB_STATUS *ret);
  bool StartNow(int index, SCHEDULE_FAIL_REASON *reason);
  bool Delete(int index, SCHEDULE_FAIL_REASON *reason);
  unsigned int Add(const wstring &script, bool *success, SCHEDULE_FAIL_REASON *reason);
  bool Modify(int index, const wstring &script, SCHEDULE_FAIL_REASON *reason);

private:
  struct TIME_TRAIT
  {
    VECTOR_TIME times;
    bool all;

  };

  struct JOB
  {
    JOB()
    {
      invoker = NULL;
      lastTime.QuadPart = 0ull;
      startNow = false;

    }

    bool startNow;
    ULARGE_INTEGER lastTime;
    TIME_TRAIT elements[TI_COUNT];
    wstring invokerName;
    wstring script;
    ScheduleInvoker *invoker;

  };

  typedef vector<JOB> VECTOR_JOB;
  typedef VECTOR_JOB::iterator VECTOR_JOB_ITERATOR;

private:
  virtual bool Run();
  bool ParseCommand(const wstring &filePath);
  bool ParseLine(const wstring &line, JOB *ret);
  bool ParseSub(const wstring &token, TIME_INDEX index, VECTOR_TIME *times);
  bool IsMatchTime(const TIME_TRAIT &times, WORD value);
  bool SaveJobsToFile(bool useLock);

private:
  VECTOR_JOB m_jobs;
  LOCK_OBJECT m_jobLocker;
  VECTOR_TIME m_timeLimits;
  Server *m_server;

};

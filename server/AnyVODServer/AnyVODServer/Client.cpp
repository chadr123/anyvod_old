/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "stdafx.h"
#include "Client.h"
#include "Common.h"
#include "Server.h"
#include "AutoLocker.h"
#include "PacketHandler.h"
#include "Log.h"
#include "Util.h"
#include "..\..\..\common\packets.h"

Client::Client(SOCKET socket, SOCKADDR_STORAGE *clientAddr, Server *server, bool isSteam)
:m_socket(socket), m_server(server), m_totalRecvBytes(0), m_totalSentBytes(0),
m_recvBytesPerSec(0), m_sentBytesPerSec(0), m_accumRecvBytesPerSec(0),
m_accumSentBytesPerSec(0), m_state(CS_LOGOUT), m_lastPacketType(PT_HEAD),
m_forceClose(false), m_refIOCount(0), m_toBeClose(false), m_lastTime(timeGetTime()),
m_isStream(isSteam), m_searchSubtitle(true), m_searchLyrics(true)
{
  this->m_userInfo.admin = false;

  memcpy(&this->m_clientAddr, clientAddr, sizeof(this->m_clientAddr));

  this->m_recvBuffer.Create();
  this->m_movieTrans.Init(this);

  InitializeCriticalSectionAndSpinCount(&this->m_recvLocker, MAX_SPIN_COUNT);
  InitializeCriticalSectionAndSpinCount(&this->m_sendLocker, MAX_SPIN_COUNT);
  InitializeCriticalSectionAndSpinCount(&this->m_thisLocker, MAX_SPIN_COUNT);

}

Client::~Client()
{
  this->Clear();

  this->m_recvBuffer.Destroy();

  DeleteCriticalSection(&this->m_recvLocker);
  DeleteCriticalSection(&this->m_sendLocker);
  DeleteCriticalSection(&this->m_thisLocker);

  if (!this->m_forceClose)
    closesocket(this->m_socket);

  Log::GetInstance().Write(L"Client disconnected (IP : %s, Port : %hu, Socket : %u)",
    Util::AddressToString(this->m_clientAddr).c_str(),
    Util::PortFromAddress(this->m_clientAddr),
    this->m_socket);

}

bool
Client::ProcessOwnTasks()
{
  DWORD curTime = timeGetTime();
  DWORD delta = curTime - this->m_lastTime;
  DWORD ratio = delta / 1000;

  if (delta >= 1000)
  {
    this->m_recvBytesPerSec = (this->m_recvBytesPerSec + this->m_accumRecvBytesPerSec) / 2 / ratio;
    this->m_accumRecvBytesPerSec = 0;
    this->m_sentBytesPerSec = (this->m_sentBytesPerSec + this->m_accumSentBytesPerSec) / 2 / ratio;
    this->m_accumSentBytesPerSec = 0;

    this->m_lastTime = curTime;

  }

  return this->m_movieTrans.Transmit();

}

MovieTransmitter&
Client::GetMovieTransmitter()
{
  return this->m_movieTrans;

}

SOCKET
Client::GetSocket() const
{
  return this->m_socket;

}

const SOCKADDR_STORAGE&
Client::GetSockAddr() const
{
  return this->m_clientAddr;

}

unsigned long long
Client::GetTotalRecvBytes() const
{
  return this->m_totalRecvBytes;

}

unsigned long long
Client::GetTotalSentBytes() const
{
  return this->m_totalSentBytes;

}

unsigned int
Client::GetRecvBytesPerSec() const
{
  return this->m_recvBytesPerSec;

}

unsigned int
Client::GetSentBytesPerSec() const
{
  return this->m_sentBytesPerSec;

}

bool
Client::Init()
{
  return this->RecvHeaderPost(C2S_HEADER_SIZE);

}

bool
Client::RecvHeaderPost(ULONG size)
{
  return this->RecvPostInteral(size, OP_RECV_HEADER);

}

bool
Client::RecvBodyPost(ULONG size)
{
  return this->RecvPostInteral(size, OP_RECV_BODY);

}

bool
Client::RecvPostInteral(ULONG size, IO_OPERATION_TYPE op)
{
  AutoLocker autoLocker(&this->m_recvLocker);

  if (this->m_recvQueue.size() > MAX_RECV_REQUEST_COUNT)
  {
    Log::GetInstance().Write(L"Receiving requests are exceeded (IP : %s, Port : %hu, Socket : %u)",
      Util::AddressToString(this->m_clientAddr).c_str(),
      Util::PortFromAddress(this->m_clientAddr),
      this->m_socket);

    return false;

  }
  else
  {
    IO_CONTEXT *context = new IO_CONTEXT;

    memset(context, 0x00, sizeof(IO_CONTEXT));

    context->buf.buf = new char[size];
    context->buf.len = size;

    context->op = op;

    bool success = this->m_server->RecvPost(this->m_socket, &context->buf, (LPWSAOVERLAPPED)context);

    if (success)
    {
      this->m_recvQueue.push_back(context);

      this->IncreaseIORef();

    }
    else
    {
      delete context;

    }

    return success;

  }

}

bool
Client::IsSendable() const
{
  return this->m_sendQueue.size() < MAX_SEND_REQUEST_COUNT;

}

bool
Client::SendPostPacket(ANYVOD_PACKET &packet)
{
  int size = packet.s2c_header.header.size;

  packet.s2c_header.header.size = htonl(size);
  packet.s2c_header.header.type = htonl(packet.s2c_header.header.type);

  return this->SendPost((char*)&packet, size);

}

bool
Client::SendPost(const char *data, ULONG size)
{
  AutoLocker autoLocker(&this->m_sendLocker);

  if (this->m_sendQueue.size() > MAX_SEND_REQUEST_COUNT)
  {
    Log::GetInstance().Write(L"Sending requests are exceeded (IP : %s, Port : %hu, Socket : %u)",
      Util::AddressToString(this->m_clientAddr).c_str(),
      Util::PortFromAddress(this->m_clientAddr),
      this->m_socket);

    return false;

  }
  else
  {
    IO_CONTEXT *context = new IO_CONTEXT;

    memset(context, 0x00, sizeof(IO_CONTEXT));

    context->buf.buf = new char[size];
    context->buf.len = size;

    memcpy(context->buf.buf, data, size);

    context->op = OP_SEND;

    bool success = this->m_server->SendPost(this->m_socket, &context->buf, (LPWSAOVERLAPPED)context);

    if (success)
    {
      this->m_sendQueue.push_back(context);

      this->IncreaseIORef();

    }
    else
    {
      delete context;

    }

    return success;

  }

}

void
Client::RemoveContext(IO_OPERATION_TYPE op, const IO_CONTEXT *context)
{
  CRITICAL_SECTION *locker = NULL;
  LIST_PACKET *list = NULL;

  if (op == OP_RECV_HEADER || op == OP_RECV_BODY)
  {
    locker = &this->m_recvLocker;
    list = &this->m_recvQueue;

  }
  else if (op == OP_SEND)
  {
    locker = &this->m_sendLocker;
    list = &this->m_sendQueue;

  }

  AutoLocker autoLocker(locker);

  LIST_PACKET_ITERATOR i = find(list->begin(), list->end(), context);

  delete context;

  list->erase(i);

  this->DecreaseIORef();

}

void
Client::SetToBeClosed()
{
  this->m_toBeClose = true;

}

bool
Client::IsToBeClose() const
{
  return this->m_toBeClose;

}

bool
Client::IsAdmin()
{
  if (!this->m_userInfo.admin)
  {
    DataBase::DATABASE_FAIL_REASON reason;

    this->m_userInfo.admin = this->m_server->GetDataBase().IsAdmin(this->m_userInfo.id, &reason);

  }

  return this->m_userInfo.admin;

}


bool
Client::IsStream() const
{
  return this->m_isStream;

}

bool
Client::IsSearchSubtitle() const
{
  return this->m_searchSubtitle;

}

bool
Client::isSearchLyrics() const
{
  return this->m_searchLyrics;

}

void
Client::IncreaseIORef()
{
  InterlockedIncrement(&this->m_refIOCount);

}

void
Client::DecreaseIORef()
{
  InterlockedDecrement(&this->m_refIOCount);

}

LONG
Client::GetIORefCount() const
{
  return this->m_refIOCount;

}

ANYVOD_PACKET_TYPE
Client::GetLastPacketType() const
{
  return this->m_lastPacketType;

}

void
Client::SetLastPacketType(ANYVOD_PACKET_TYPE packetType)
{
  this->m_lastPacketType = packetType;

}

CLIENT_STATE
Client::GetState() const
{
  return this->m_state;

}

void
Client::SetState(CLIENT_STATE state)
{
  this->m_state = state;

}

Server*
Client::GetServer()
{
  return this->m_server;

}

const Client::USER_INFO&
Client::GetUserInfo() const
{
  return this->m_userInfo;

}

void
Client::SetUserInfo(const Client::USER_INFO &info)
{
  this->m_userInfo = info;

}

size_t
Client::GetRecvBufferCount()
{
  AutoLocker locker(&this->m_recvLocker);

  return this->m_recvQueue.size();

}

size_t
Client::GetSendBufferCount()
{
  AutoLocker locker(&this->m_sendLocker);

  return this->m_sendQueue.size();

}

void
Client::Shutdown()
{
  if (shutdown(this->m_socket, SD_SEND) == SOCKET_ERROR)
  {
    Log::GetInstance().Write(L"Shutdown failed (IP : %s, Port : %hu, Socket : %u)",
      Util::AddressToString(this->m_clientAddr).c_str(),
      Util::PortFromAddress(this->m_clientAddr),
      this->m_socket);

    this->SetForceClose();

  }

}

void
Client::SetForceClose()
{
  closesocket(this->m_socket);

  this->m_socket = INVALID_SOCKET;

  this->m_forceClose = true;

}

bool
Client::IsForceClose() const
{
  return this->m_forceClose;

}

bool
Client::IsMatchTicket(const string &ticket) const
{
  return this->m_userInfo.ticket == ticket;

}

void
Client::SetTicket(const string &ticket)
{
  this->m_userInfo.ticket = ticket;

}

void
Client::ResetAdmin()
{
  this->m_userInfo.admin = false;

}

void
Client::SetSearchSubtitle(bool use)
{
  this->m_searchSubtitle = use;

}

void
Client::SetSearchLyrics(bool use)
{
  this->m_searchLyrics = use;

}

bool
Client::IsLogined() const
{
  return this->m_userInfo.ticket.size() != 0 && this->m_state != CS_LOGOUT;

}

bool
Client::ProcessContext(IO_OPERATION_TYPE op, const IO_CONTEXT *context,
                       DWORD bytesTransferred, bool removeContextOnly)
{
  bool success = true;

  if (!removeContextOnly)
  {
    if (op == OP_RECV_HEADER || op == OP_RECV_BODY)
    {
      this->m_totalRecvBytes += bytesTransferred;
      this->m_accumRecvBytesPerSec += bytesTransferred;

      if (this->m_recvBuffer.Write(context->buf.buf, bytesTransferred) < bytesTransferred)
      {
        Log::GetInstance().Write(L"Not enough recv buffer memory");

        this->SetForceClose();

      }
      else
      {
        size_t bufSize = this->m_recvBuffer.GetReadableSize();

        if (bufSize < C2S_HEADER_SIZE)
        {
          success = this->RecvHeaderPost(C2S_HEADER_SIZE - (ULONG)bufSize);

        }
        else
        {
          ANYVOD_PACKET_C2S_BASIC header;
		      int32_t size;

          this->m_recvBuffer.Read((char*)&header, C2S_HEADER_SIZE, true);

		      size = ntohl(header.header.size);

          if (size < 0 || size > TOTAL_PACKET_SIZE)
          {
            Log::GetInstance().Write(L"Invalid header size : %d", size);

            this->SetForceClose();

          }
          else
          {
            if (bufSize < (size_t)size)
            {
              if (!(this->IsForceClose() || this->IsToBeClose()))
              {
                success = this->RecvBodyPost(size - (ULONG)bufSize);

              }

            }
            else
            {
              if (!(this->IsForceClose() || this->IsToBeClose()))
              {
                ANYVOD_PACKET packet;

                this->m_recvBuffer.Read((char*)&packet, size, false);

                success = this->RecvHeaderPost(C2S_HEADER_SIZE);

                if (success)
                {
                  PacketHandler handler(this);

                  success = handler.HandlePacket(packet);

                  if (!success)
                  {
                    Log::GetInstance().Write(L"Cannot handle packet : %d", ntohl(header.header.type));

                  }

                }

              }

            }

          }

        }

      }

    }
    else if (op == OP_SEND)
    {
      if (context->buf.len != bytesTransferred)
      {
        ANYVOD_PACKET &packet = (ANYVOD_PACKET&)*(context->buf.buf);

        Log::GetInstance().Write(L"Cannot send packet successfully len : %lu, sent : %lu, type : %d",
          context->buf.len,
          bytesTransferred,
          htonl(packet.s2c_header.header.type));

        this->SetForceClose();

      }

      this->m_totalSentBytes += bytesTransferred;
      this->m_accumSentBytesPerSec += bytesTransferred;

    }

  }

  this->RemoveContext(op, context);

  return success;

}

CRITICAL_SECTION*
Client::GetLocker()
{
  return &this->m_thisLocker;

}

void
Client::Clear()
{
  AutoLocker sendLocker(&this->m_sendLocker);
  AutoLocker recvLocker(&this->m_recvLocker);

  LIST_PACKET_ITERATOR i = this->m_sendQueue.begin();

  for (; i != this->m_sendQueue.end(); i++)
  {
    IO_CONTEXT *context = *i;

    delete context;

  }

  i = this->m_recvQueue.begin();

  for (; i != this->m_recvQueue.end(); i++)
  {
    IO_CONTEXT *context = *i;

    delete context;

  }

  this->m_sendQueue.clear();
  this->m_recvQueue.clear();

}

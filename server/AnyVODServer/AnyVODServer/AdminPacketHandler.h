/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "Scheduler.h"
#include "Log.h"

class PacketHandler;
class Client;
union ANYVOD_PACKET;

class AdminPacketHandler
{
public:
  AdminPacketHandler(Client *client, PacketHandler *parent);
  ~AdminPacketHandler();

  bool HandlePacket(const ANYVOD_PACKET &packet);

  bool SendNetworkStatistics(bool success);//PT_ADMIN_S2C_NETWORK_STATISTICS
  bool SendLastLog(bool success, ifstream &log, const wstring &fileName);//PT_ADMIN_C2S_LASTLOG
  bool SendEnv(bool success, ifstream &env, const wstring &fileName);//PT_ADMIN_C2S_ENV
  bool SendScheduleList(bool success);//PT_ADMIN_C2S_SCHEDULELIST
  bool SendScheduleItem(bool success, unsigned int number, const Scheduler::JOB_STATUS &job);//PT_ADMIN_C2S_SCHEDULE_ITEM

  bool SendFile(ifstream &ifs, FILE_START_TYPE type, const wstring &fileName);

  bool SendFileStart(bool success, unsigned int totalSize, FILE_START_TYPE type, const wstring &fileName);//PT_ADMIN_S2C_FILE_START
  bool SendFileStream(bool success, const char buf[MAX_STREAM_SIZE], unsigned int size, FILE_START_TYPE type);//PT_ADMIN_S2C_FILE_STREAM
  bool SendFileEnd(bool success, FILE_START_TYPE type);//PT_ADMIN_S2C_FILE_END

  bool SendRestartScheduler();//PT_ADMIN_S2C_RESTART_SCHEDULER
  bool SendRestartSchedulerComplete(bool success);//PT_ADMIN_S2C_RESTART_SCHEDULER_COMPLETE

  bool SendScheduleTypes();//PT_ADMIN_S2C_SCHEDULE_TYPES

  bool SendScheduleStartNow(unsigned int index);//PT_ADMIN_S2C_SCHEDULE_STARTNOW
  bool SendScheduleDelete(unsigned int index);//PT_ADMIN_S2C_SCHEDULE_DELETE
  bool SendScheduleAdd(const wstring &script);//PT_ADMIN_S2C_SCHEDULE_ADD
  bool SendScheduleModify(unsigned int index, const wstring &script);//PT_ADMIN_S2C_SCHEDULE_MODIFY

  bool SendLogList();//PT_ADMIN_C2S_LOGLIST
  bool SendLogItem(bool success, const Log::FILE_ITEM &item);//PT_ADMIN_C2S_LOG_ITEM
  bool SendLogDelete(bool success);
  bool SendLogView(bool success, ifstream &log, const wstring &fileName);

  bool SendUserList();//PT_ADMIN_C2S_USERLIST
  bool SendUserItem(bool success, const ANYVOD_USER_INFO &item);//PT_ADMIN_C2S_USER_ITEM

  bool SendUserAdd(const ANYVOD_USER_INFO &info);//PT_ADMIN_C2S_USER_ADD
  bool SendUserModify(const ANYVOD_USER_INFO &info);//PT_ADMIN_C2S_USER_MODIFY
  bool SendUserDelete(unsigned long long pid);//PT_ADMIN_C2S_USER_DELETE
  bool SendUserExist(const wstring &id);//PT_ADMIN_C2S_USER_EXIST

  bool SendClientStatusList();//PT_ADMIN_C2S_CLIENT_STATUSLIST
  bool SendClientStatusItem(bool success, const ANYVOD_CLIENT_STATUS &item);//PT_ADMIN_C2S_CLIENT_STATUS_ITEM

  bool SendClientLogout(const wstring &ip, unsigned short port);//PT_ADMIN_C2S_CLIENT_LOGOUT

  bool SendGroupList();//PT_ADMIN_C2S_GROUPLIST
  bool SendGroupItem(bool success, const ANYVOD_GROUP_INFO &item);//PT_ADMIN_C2S_GROUP_ITEM

  bool SendGroupAdd(const ANYVOD_GROUP_INFO &info);//PT_ADMIN_C2S_GROUP_ADD
  bool SendGroupModify(const ANYVOD_GROUP_INFO &info);//PT_ADMIN_C2S_GROUP_MODIFY
  bool SendGroupDelete(unsigned long long pid);//PT_ADMIN_C2S_GROUP_DELETE
  bool SendGroupExist(const wstring &name);//PT_ADMIN_C2S_GROUP_EXIST

  bool SendUserGroupList();//PT_ADMIN_C2S_USER_GROUPLIST
  bool SendUserGroupItem(bool success, const ANYVOD_USER_GROUP_INFO &item);//PT_ADMIN_C2S_USER_GROUP_ITEM
  bool SendUserGroupAdd(unsigned long long userPID, const wstring &groupName);//PT_ADMIN_C2S_USER_GROUP_ADD
  bool SendUserGroupDelete(unsigned long long pid);//PT_ADMIN_C2S_USER_GROUP_DELETE

  bool SendFileGroupList(const wstring &filePath);//PT_ADMIN_C2S_FILE_GROUPLIST
  bool SendFileGroupItem(bool success, const ANYVOD_FILE_GROUP_INFO &item);//PT_ADMIN_C2S_FILE_GROUP_ITEM
  bool SendFileGroupAdd(const wstring &filePath, const wstring &groupName);//PT_ADMIN_C2S_FILE_GROUP_ADD
  bool SendFileGroupDelete(const wstring &filePath, const wstring &groupName);//PT_ADMIN_C2S_FILE_GROUP_DELETE

  bool SendStatementStatusList();//PT_ADMIN_C2S_STATEMENT_STATUSLIST
  bool SendStatementStatusItem(bool success, const ANYVOD_STATEMENT_STATUS &item);//PT_ADMIN_C2S_STATEMENT_STATUS_ITEM

  bool SendSubtitleExist(bool success, bool exist, const wstring &fileName);//PT_ADMIN_C2S_SUBTITLE_EXIST

  bool SendRebuildMetaData();//PT_ADMIN_C2S_REBUILD_META_DATA
  bool SendRebuildMovieData();//PT_ADMIN_C2S_REBUILD_MOVIE_DATA

private:
  Client *m_client;
  PacketHandler *m_parent;

};

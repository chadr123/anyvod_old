/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"

#define _CRT_SECURE_NO_DEPRECATE
#define _SECURE_SCL 0

#include <winsock2.h>
#include <ws2tcpip.h>
#include <mswsock.h>
#include <windows.h>
#include <shlobj.h>
#include <Wininet.h>
#include <Shlwapi.h>

#include <process.h>
#include <io.h>
#include <fcntl.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <map>
#include <string>
#include <sstream>
#include <algorithm>
#include <iomanip>
#include <codecvt>

using namespace std;

#include <sqlite3.h>
#include "..\..\..\libs\MediaInfo\include\MediaInfoDLL_Static.h"
#include "..\..\..\libs\zlib\include\zlib.h"

#ifdef NDEBUG
#include "..\..\..\libs\tbb\include\tbb\tbbmalloc_proxy.h"
#endif // NDEBUG

#define USE_VISTA

/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "..\..\..\common\types.h"

class Log
{
public:
  struct FILE_ITEM
  {
    wstring fileName;
    unsigned long long fileSize;

  };

  typedef vector<FILE_ITEM> VECTOR_FILE_ITEM;
  typedef VECTOR_FILE_ITEM::iterator VECTOR_FILE_ITEM_ITERATOR;

  static Log& GetInstance();
  static void GetNewLogDate(wstring *ret);
  static bool GetLogNames(const wstring &root, VECTOR_FILE_ITEM *ret);
  static bool DeleteLog(const wstring &root, const wstring &fileName);

public:
  bool StartLog(const wstring &filePath);
  void EndLog();
  bool RestartLog(const wstring &filePath);
  wstring Write(const wchar_t* szFmt, ...);
  void GetFileName(wstring *ret);
  void Flush();

private:
  Log();
  ~Log();

  void GetTime(wstring *ret);

private:
  static const int MAX_LOG_BUFFER_SIZE;
  static Log instance;

private:
  wstring m_fileName;
  FILE *m_ofs;
  CRITICAL_SECTION m_lock;

};

/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "..\..\..\common\types.h"

class PacketHandler;
class Client;
union ANYVOD_PACKET;

class UserPacketHandler
{
public:
  UserPacketHandler(Client *client, PacketHandler *parent);
  ~UserPacketHandler();

  bool HandlePacket(const ANYVOD_PACKET &packet);

  bool CreateTicket(string *ret);
  bool SendStartStream(long long startOffset, int size);//PT_S2C_STREAM_REQUEST
  bool SendStopStream();//PT_S2C_STOP_MOVIE
  bool SendStream(long long blockOffset, int size, const char data[MAX_STREAM_SIZE]);//PT_S2C_STREAM_DATA
  bool SendMetaStream(long long blockOffset, int size, const char data[MAX_STREAM_SIZE]);//PT_S2C_STREAM_META_DATA
  bool SendStreamEnd();//PT_S2C_STREAM_END
  bool SendLogin(bool success);//PT_S2C_LOGIN
  bool SendJoin(bool success);//PT_S2C_JOIN
  bool SendLogout(bool success);//PT_S2C_LOGOUT
  bool SendFileList(const wstring &path);//PT_S2C_FILELIST
  bool SendStartMovie(const wstring &filePath);//PT_S2C_START_MOVIE
  bool SendSubtitle(bool success, ifstream &subtitle, const wstring &fileName);//PT_S2C_SUBTITLE
  bool SendSubtitleURL(bool success, const string &url);//PT_S2C_SUBTITLE_URL

private:
  Client *m_client;
  PacketHandler *m_parent;

};

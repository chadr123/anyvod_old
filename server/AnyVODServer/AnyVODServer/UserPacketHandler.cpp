/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "stdafx.h"
#include "UserPacketHandler.h"
#include "AdminPacketHandler.h"
#include "PacketHandler.h"
#include "Server.h"
#include "Util.h"
#include "FindHandleCloser.h"
#include "SubtitleImpl.h"
#include "..\..\..\common\SubtitleFileNameGenerator.h"

UserPacketHandler::UserPacketHandler(Client *client, PacketHandler *parent)
:m_client(client), m_parent(parent)
{

}

UserPacketHandler::~UserPacketHandler()
{

}

bool
UserPacketHandler::HandlePacket(const ANYVOD_PACKET &packet)
{
  bool success = true;
  ANYVOD_PACKET_TYPE type = (ANYVOD_PACKET_TYPE)ntohl(packet.c2s_header.header.type);

  switch (type)
  {
  case PT_C2S_LOGIN:
    {
      if (this->m_client->IsLogined())
      {
        success = this->SendLogin(false) && this->m_parent->SendError(ET_ALREADY_LOGINED);

        Log::GetInstance().Write(L"Already logined (IP : %s, Port : %hu, Socket : %u, ID : %s)",
          Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
          Util::PortFromAddress(this->m_client->GetSockAddr()),
          this->m_client->GetSocket(),
          this->m_client->GetUserInfo().id.c_str());

      }
      else
      {
        ANYVOD_USER_INFO info;
        wstring id;
        DataBase::DATABASE_FAIL_REASON reason;

        Util::ConvertUTF8ToUnicode((char*)packet.c2s_login.id, MAX_ID_SIZE, &id);

        if (this->m_client->GetServer()->GetDataBase().FindUser(id, &info, &reason))
        {
          wstring pass;
          wstring tmp;

          Util::ConvertUTF8ToUnicode((char*)packet.c2s_login.pass, MAX_PASS_SIZE, &tmp);

          this->m_client->GetServer()->GetDataBase().GetPassword(tmp, &pass);

          if (pass == info.pass)
          {
            Client::USER_INFO userInfo;

            userInfo.admin = false;
            userInfo.id = info.id;
            userInfo.name = info.name;
            userInfo.pass = info.pass;

            this->m_client->SetUserInfo(userInfo);

            success = this->SendLogin(true);

            Log::GetInstance().Write(L"User login (IP : %s, Port : %hu, Socket : %u, ID : %s, Ticket : %S)",
              Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
              Util::PortFromAddress(this->m_client->GetSockAddr()),
              this->m_client->GetSocket(),
              this->m_client->GetUserInfo().id.c_str(),
              this->m_client->GetUserInfo().ticket.c_str());

          }
          else
          {
            success = this->SendLogin(false) && this->m_parent->SendError(ET_INVALID_PASS);

            Log::GetInstance().Write(L"Invalid password (IP : %s, Port : %hu, Socket : %u, ID : %s, Password : %s)",
              Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
              Util::PortFromAddress(this->m_client->GetSockAddr()),
              this->m_client->GetSocket(),
              id.c_str(),
              pass.c_str());

          }

        }
        else
        {
          success = this->SendLogin(false) && this->m_parent->SendError(ET_INVALID_ID);

          Log::GetInstance().Write(L"Invalid id (IP : %s, Port : %hu, Socket : %u, ID : %s)",
            Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
            Util::PortFromAddress(this->m_client->GetSockAddr()),
            this->m_client->GetSocket(),
            id.c_str());

        }

      }

      break;

    }
  case PT_C2S_JOIN:
    {
      if (this->m_client->IsLogined())
      {
        success = this->SendJoin(false) && this->m_parent->SendError(ET_ALREADY_JOINED);

        Log::GetInstance().Write(L"Already joined (IP : %s, Port : %hu, Socket : %u, ID : %s)",
          Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
          Util::PortFromAddress(this->m_client->GetSockAddr()),
          this->m_client->GetSocket(),
          this->m_client->GetUserInfo().id.c_str());

      }
      else
      {
        Client::USER_INFO userInfo;
        char ticket[MAX_TICKET_SIZE];

        strncpy_s(ticket, MAX_TICKET_SIZE, (char*)packet.c2s_join.ticket, MAX_TICKET_CHAR_SIZE);

        if (this->m_client->GetServer()->GetClientProcessor().FindUserByTicket(ticket, &userInfo))
        {
          this->m_client->SetUserInfo(userInfo);
          this->m_client->SetState(CS_LOGIN);

          success = this->SendJoin(true);

          Log::GetInstance().Write(L"User join (IP : %s, Port : %hu, Socket : %u, ID : %s, Ticket : %S)",
            Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
            Util::PortFromAddress(this->m_client->GetSockAddr()),
            this->m_client->GetSocket(),
            this->m_client->GetUserInfo().id.c_str(),
            this->m_client->GetUserInfo().ticket.c_str());

        }
        else
        {
          success = this->SendJoin(false) && this->m_parent->SendError(ET_INVALID_TICKET);

          Log::GetInstance().Write(L"Invalid ticket (IP : %s, Port : %hu, Socket : %u, Ticket : %s)",
            Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
            Util::PortFromAddress(this->m_client->GetSockAddr()),
            this->m_client->GetSocket(),
            ticket);

        }

      }

      break;

    }
  case PT_C2S_LOGOUT:
    {
      if (this->m_client->IsLogined())
      {
        success = this->SendLogout(true);

        this->m_client->SetToBeClosed();
        this->m_client->Shutdown();

        Log::GetInstance().Write(L"User logout (IP : %s, Port : %hu, Socket : %u, ID : %s)",
          Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
          Util::PortFromAddress(this->m_client->GetSockAddr()),
          this->m_client->GetSocket(),
          this->m_client->GetUserInfo().id.c_str());

      }
      else
      {
        success = this->SendLogout(false) && this->m_parent->SendError(ET_NOT_LOGINED);

        Log::GetInstance().Write(L"Not logined (IP : %s, Port : %hu, Socket : %u)",
          Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
          Util::PortFromAddress(this->m_client->GetSockAddr()),
          this->m_client->GetSocket());

      }

      break;

    }
  case PT_C2S_FILELIST:
    {
      wstring path;

      Util::ConvertUTF8ToUnicode((char*)packet.c2s_filelist.path, MAX_PATH_SIZE, &path);

      success = this->SendFileList(path);

      Log::GetInstance().Write(L"Send file list (IP : %s, Port : %hu, Socket : %u, ID : %s, Path : %s)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket(),
        this->m_client->GetUserInfo().id.c_str(),
        path.c_str());

      break;

    }
  case PT_C2S_START_MOVIE:
    {
      wstring filePath;

      Util::ConvertUTF8ToUnicode((char*)packet.c2s_start_movie.filePath, MAX_FILEPATH_SIZE, &filePath);

      success = this->SendStartMovie(filePath);

      Log::GetInstance().Write(L"Start movie (IP : %s, Port : %hu, Socket : %u, ID : %s, FilePath : %s)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket(),
        this->m_client->GetUserInfo().id.c_str(),
        filePath.c_str());

      break;

    }
  case PT_C2S_STREAM_REQUEST:
    {
      success = this->SendStartStream(ntohll(packet.c2s_stream_request.startOffset), ntohl(packet.c2s_stream_request.size));

      break;

    }
  case PT_C2S_STOP_MOVIE:
    {
      success = this->SendStopStream();

      Log::GetInstance().Write(L"Stop movie (IP : %s, Port : %hu, Socket : %u, ID : %s)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket(),
        this->m_client->GetUserInfo().id.c_str());

      break;

    }
  case PT_C2S_SUBTITLE:
    {
      SubtitleFileNameGenerator gen;
      vector_wstring names;
      wstring filePath;
      wstring fileName;
      wstring dirPath;
      wstring subtitlePath;
      Server *server = this->m_client->GetServer();
      wstring movieDir = server->GetEnv().movie_dir;
      bool found = false;

      this->m_client->SetSearchSubtitle(packet.c2s_subtitle.searchSubtitle != 0);
      this->m_client->SetSearchLyrics(packet.c2s_subtitle.searchLyrics != 0);

      Util::ConvertUTF8ToUnicode((char*)packet.c2s_subtitle.filePath, MAX_PATH_SIZE, &filePath);
      replace(filePath.begin(), filePath.end(), UNIX_DIRECTORY_DELIMITER, DIRECTORY_DELIMITER);
      Util::SplitFilePath(filePath, &dirPath, &fileName);
      Util::AppendDirSeparator(&dirPath);

      gen.getFileNamesWithExt(fileName, &names);

      for (size_t i = 0; i < names.size(); i++)
      {
        ifstream ifs;

        Util::BindPath(movieDir, dirPath, &subtitlePath);
        Util::BindPath(subtitlePath, names[i], &subtitlePath);

        ifs.open(subtitlePath.c_str(), ios::binary);

        if (ifs.is_open())
        {
          success = this->SendSubtitle(true, ifs, names[i]);
          found = true;

          subtitlePath = dirPath;
          subtitlePath.append(names[i]);
          replace(subtitlePath.begin(), subtitlePath.end(), DIRECTORY_DELIMITER, UNIX_DIRECTORY_DELIMITER);

          break;

        }

      }

      if (found == false)
      {
        names.clear();
        gen.getFileNames(fileName, &names);

        for (size_t i = 0; i < names.size(); i++)
        {
          ifstream ifs;

          Util::BindPath(movieDir, dirPath, &subtitlePath);
          Util::BindPath(subtitlePath, names[i], &subtitlePath);

          ifs.open(subtitlePath.c_str(), ios::binary);

          if (ifs.is_open())
          {
            success = this->SendSubtitle(true, ifs, names[i]);
            found = true;

            subtitlePath = dirPath;
            subtitlePath.append(names[i]);
            replace(subtitlePath.begin(), subtitlePath.end(), DIRECTORY_DELIMITER, UNIX_DIRECTORY_DELIMITER);

            break;

          }

        }

        bool isAudio = Util::IsMatchExt(filePath, server->GetAudioExts());

        if (found == false)
        {
          SubtitleImpl subtitle;
          wstring ret;
          wstring moviePath;
          DataBase::MOVIE_INFO info;
          DataBase::DATABASE_FAIL_REASON reason;

          Util::BindPath(movieDir, filePath, &moviePath);
          server->GetDataBase().GetMovieInfo(filePath, &info, &reason);

          if (reason == DataBase::DFR_NONE && info.totalFrame <= 0 && isAudio &&
            this->m_client->isSearchLyrics() && subtitle.getLyrics(moviePath, &ret))
          {
            wstring lyricName;
            wstring lyricPath;

            gen.getFileBaseName(fileName, true, &lyricName);
            lyricName += EXTENSION_SEPERATOR;
            lyricName += L"lrc";

            Util::BindPath(movieDir, dirPath, &lyricPath);
            Util::BindPath(lyricPath, lyricName, &lyricPath);

            FILE *file = _wfopen(lyricPath.c_str(), L"w, ccs=UTF-8");

            if (file)
            {
              ifstream ifs;

              fwprintf(file, L"%s", ret.c_str());
              fclose(file);

              ifs.open(lyricPath.c_str(), ios::binary);

              if (ifs.is_open())
              {
                success = this->SendSubtitle(true, ifs, lyricName);
                found = true;

              }

            }

          }

          if (found == false)
          {
            ifstream ifs;

            success = this->SendSubtitle(false, ifs, fileName) && this->m_parent->SendError(ET_CANNOT_OPEN_FILE);

            replace(filePath.begin(), filePath.end(), DIRECTORY_DELIMITER, UNIX_DIRECTORY_DELIMITER);

            if (isAudio)
            {
              if (this->m_client->isSearchLyrics())
              {
                subtitlePath = L"Not exist " + filePath;

              }
              else
              {
                subtitlePath = L"Not want " + filePath;

              }

            }
            else
            {
              subtitlePath = L"Not audio " + filePath;

            }

          }

        }

      }

      Log::GetInstance().Write(L"Send subtitle : %s (IP : %s, Port : %hu, Socket : %u)",
        subtitlePath.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  case PT_C2S_SUBTITLE_URL:
    {
      string url;
      wstring movieDir = this->m_client->GetServer()->GetEnv().movie_dir;
      wstring filePath;
      wstring fileName;
      wstring dirPath;
      wstring moviePath;

      this->m_client->SetSearchSubtitle(packet.c2s_subtitle_url.searchSubtitle != 0);

      Util::ConvertUTF8ToUnicode((char*)packet.c2s_subtitle_url.filePath, MAX_PATH_SIZE, &filePath);
      replace(filePath.begin(), filePath.end(), UNIX_DIRECTORY_DELIMITER, DIRECTORY_DELIMITER);
      Util::SplitFilePath(filePath, &dirPath, &fileName);

      Util::BindPath(movieDir, filePath, &moviePath);

      if (this->m_client->IsSearchSubtitle())
      {
        SubtitleImpl subtitle;
        string localFileName;

        Util::ConvertUnicodeToAscii(fileName, &localFileName);

        if (!subtitle.existSubtitle(moviePath, localFileName, &url))
          url.clear();

      }

      success = this->SendSubtitleURL(true, url);

      if (url.empty())
      {
        if (this->m_client->IsSearchSubtitle())
        {
          url = "Not exist";

        }
        else
        {
          url = "Not want";

        }
      }

      replace(filePath.begin(), filePath.end(), DIRECTORY_DELIMITER, UNIX_DIRECTORY_DELIMITER);

      Log::GetInstance().Write(L"Send subtitle url : %S, %s (IP : %s, Port : %hu, Socket : %u)",
        url.c_str(),
        filePath.c_str(),
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      break;

    }
  default:
    {
      Log::GetInstance().Write(L"Invalid user packet. Plan to close (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      this->m_client->SetForceClose();

      break;

    }

  }

  return success;

}

bool
UserPacketHandler::CreateTicket(string *ret)
{
  GUID guid;
  OLECHAR clsid[40];

  if (CoCreateGuid(&guid) == S_OK)
  {
    if (StringFromGUID2(guid, clsid, 39) > 0)
    {
      char tmp[MAX_TICKET_SIZE];

      if (WideCharToMultiByte(CP_ACP, NULL, clsid, MAX_TICKET_SIZE, tmp, MAX_TICKET_SIZE, NULL, NULL) != 0)
      {
        ret->assign(tmp, MAX_TICKET_CHAR_SIZE);

      }
      else
      {
        return false;

      }

    }

  }

  return true;

}

bool
UserPacketHandler::SendLogin(bool success)
{
  bool sent = false;
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_LOGIN);
  packet.s2c_header.header.type = PT_S2C_LOGIN;
  packet.s2c_header.success = success;

  if (success)
  {
    string ticket;

    if (this->CreateTicket(&ticket))
    {
      strncpy((char*)packet.s2c_login.ticket, ticket.c_str(), MAX_TICKET_SIZE);

      sent = this->m_client->SendPostPacket(packet);

      if (sent)
      {
        this->m_client->SetTicket(ticket);
        this->m_client->SetState(CS_LOGIN);

      }

    }
    else
    {
      packet.s2c_header.success = false;

      sent = this->m_client->SendPostPacket(packet) &&
        this->m_parent->SendError(ET_CANNOT_CREATE_TICKET);

    }

  }
  else
  {
    sent = this->m_client->SendPostPacket(packet);

  }

  return sent;

}

bool
UserPacketHandler::SendJoin(bool success)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_JOIN);
  packet.s2c_header.header.type = PT_S2C_JOIN;
  packet.s2c_header.success = success;

  return this->m_client->SendPostPacket(packet);

}

bool
UserPacketHandler::SendLogout(bool success)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_LOGOUT);
  packet.s2c_header.header.type = PT_S2C_LOGOUT;
  packet.s2c_header.success = success;

  this->m_client->SetTicket("");
  this->m_client->ResetAdmin();
  this->m_client->SetState(CS_LOGOUT);

  return this->m_client->SendPostPacket(packet);

}

bool
UserPacketHandler::SendFileList(const wstring &path)
{
  Server *server = this->m_client->GetServer();
  DataBase &db = server->GetDataBase();
  wstring orgPath = path;
  wstring logicalPath;
  wstring physicalPath;
  wstring searchPath;
  wstring movieDir = server->GetEnv().movie_dir;
  DataBase::DATABASE_FAIL_REASON reason;
  vector<ANYVOD_FILE_ITEM> items;
  bool sent = false;
  bool success = false;

  this->m_parent->ConvertToNativePath(path, &logicalPath);
  Util::AppendDirSeparator(&movieDir);
  Util::AppendDirSeparator(&logicalPath);

  if (db.HasDirectoryPermission(logicalPath, this->m_client->GetUserInfo().id, &reason))
  {
    Util::BindPath(movieDir, logicalPath, &physicalPath);
    Util::AppendDirSeparator(&physicalPath);

    searchPath = physicalPath;
    searchPath.append(L"*");

    DWORD attr = GetFileAttributesW(physicalPath.c_str());

    if (attr != INVALID_FILE_ATTRIBUTES &&
      !IS_BIT_SET(attr, FILE_ATTRIBUTE_HIDDEN) &&
      !IS_BIT_SET(attr, FILE_ATTRIBUTE_SYSTEM) || Util::IsRoot(physicalPath))
    {
      WIN32_FIND_DATAW findData;
      HANDLE find = FindFirstFileW(searchPath.c_str(), &findData);
      FindHandleCloser closer(find);
      const ENV_INFO &env = server->GetEnv();

      if (find == INVALID_HANDLE_VALUE)
      {
        success = false;

        Log::GetInstance().Write(L"FindFirstFileW failed : \"%s\" Path : %s",
          Util::SystemErrorToString(GetLastError()).c_str(), searchPath.c_str());

      }
      else
      {
        do
        {
          if (IS_BIT_SET(findData.dwFileAttributes, FILE_ATTRIBUTE_HIDDEN) ||
            IS_BIT_SET(findData.dwFileAttributes, FILE_ATTRIBUTE_SYSTEM))
          {
            continue;

          }

          ANYVOD_FILE_ITEM item;

          if (IS_BIT_SET(findData.dwFileAttributes, FILE_ATTRIBUTE_DIRECTORY))
          {
            if (wcscmp(findData.cFileName, L".") == 0 || wcscmp(findData.cFileName, L"..") == 0)
            {
              continue;

            }

            item.type = FT_DIR;

          }
          else
          {
            item.type = FT_FILE;

          }

          if (item.type == FT_FILE)
          {
            if (!Util::IsMatchExt(findData.cFileName, env.movie_exts))
              continue;

          }

          findData.cFileName[MAX_PATH-1] = L'\0';

          item.fileName.assign(findData.cFileName);

          DataBase::MOVIE_INFO movieInfo;
          wstring tmp = logicalPath;

          tmp.append(item.fileName);

          if (item.type == FT_FILE)
          {
            if (!db.GetMovieInfo(tmp, &movieInfo, &reason))
            {
              continue;

            }

            item.permission = db.HasPermission(tmp, this->m_client->GetUserInfo().id, &reason);

          }
          else
          {
            item.permission = db.HasDirectoryPermission(tmp, this->m_client->GetUserInfo().id, &reason);

          }

          item.totalSize = movieInfo.totalSize;
          item.totalFrame = movieInfo.totalFrame;
          item.bitRate = movieInfo.bitRate;
          item.totalTime = movieInfo.totalTime;
          item.title = movieInfo.title;
          item.movieType = movieInfo.movieType;

          items.push_back(item);

        } while (FindNextFileW(find, &findData));

        if (GetLastError() != ERROR_NO_MORE_FILES)
        {
          Log::GetInstance().Write(L"FindNextFileW failed : \"%s\" Path : %s",
            Util::SystemErrorToString(GetLastError()).c_str(), physicalPath.c_str());

          success = false;

          items.clear();

        }
        else
        {
          success = true;

        }

      }

    }

  }

  int size = (int)(sizeof(ANYVOD_PACKET_S2C_FILELIST) + (sizeof(ANYVOD_PACKET_FILE_ITEM) * items.size()));

  char *tmp = new char[size];
  ANYVOD_PACKET &packet = (ANYVOD_PACKET&)*tmp;

  packet.s2c_header.header.size = size;
  packet.s2c_header.header.type = PT_S2C_FILELIST;
  packet.s2c_header.success = success;

  packet.s2c_filelist.count = htonl((int)items.size());

  Util::ConvertUnicodeToUTF8(orgPath, (char*)packet.s2c_filelist.path, MAX_PATH_SIZE);

  for (size_t i = 0; i < items.size(); i++)
  {
    ANYVOD_FILE_ITEM &item = items[i];

    packet.s2c_filelist.Items[i].bitRate = htonl(item.bitRate);
    packet.s2c_filelist.Items[i].permission = item.permission;
    packet.s2c_filelist.Items[i].totalSize = htonll(item.totalSize);
    packet.s2c_filelist.Items[i].totalFrame = htonl(item.totalFrame);
    packet.s2c_filelist.Items[i].totalTime = htonl(item.totalTime);
    packet.s2c_filelist.Items[i].type = htonl(item.type);

    Util::ConvertUnicodeToUTF8(item.fileName, (char*)packet.s2c_filelist.Items[i].fileName, MAX_FILENAME_SIZE);
    Util::ConvertUnicodeToUTF8(item.title, (char*)packet.s2c_filelist.Items[i].title, MAX_TITLE_SIZE);
    Util::ConvertUnicodeToUTF8(item.movieType, (char*)packet.s2c_filelist.Items[i].movieType, MAX_MOVIETYPE_SIZE);

  }

  sent = this->m_client->SendPostPacket(packet);

  delete[] tmp;

  if (!success)
  {
    sent &= this->m_parent->SendError(ET_NOT_EXIST_PATH);

  }

  this->m_client->SetState(CS_FILELIST);

  return sent;

}

bool
UserPacketHandler::SendStartMovie(const wstring &filePath)
{
  ANYVOD_PACKET packet;
  bool success = true;
  DataBase::MOVIE_INFO info;
  wstring localPath;
  ERROR_TYPE error = ET_NONE;

  this->m_parent->ConvertToNativePath(filePath, &localPath);

  if (!this->m_client->GetMovieTransmitter().InitializeTransmission(localPath, &error))
  {
    success &= false;

  }

  if (success)
  {
    DataBase &db = this->m_client->GetServer()->GetDataBase();
    DataBase::DATABASE_FAIL_REASON reason = DataBase::DFR_NONE;

    if (!db.GetMovieInfo(localPath, &info, &reason))
    {
      success &= false;

      if (reason != DataBase::DFR_NONE)
      {
        error = DataBase::FailResonToErrorType(reason);

      }
      else
      {
        error = ET_NOT_SUPPORTED_FORMAT;

      }

    }

  }

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_START_MOVIE);
  packet.s2c_header.header.type = PT_S2C_START_MOVIE;
  packet.s2c_header.success = success;

  packet.s2c_start_movie.bitRateMultiplier = this->m_client->GetServer()->GetEnv().buffering_multiplier;
  packet.s2c_start_movie.totalSize = htonll(info.totalSize);
  packet.s2c_start_movie.bitRate = htonl(info.bitRate);

  string tmp;

  Util::ConvertUnicodeToAscii(info.movieType, &tmp);
  strncpy_s((char*)packet.s2c_start_movie.movieType, MAX_MOVIE_TYPE_SIZE, tmp.c_str(), MAX_MOVIE_TYPE_CHAR_SIZE);
  Util::ConvertUnicodeToAscii(info.codecVideo, &tmp);
  strncpy_s((char*)packet.s2c_start_movie.codecVideo, MAX_CODEC_NAME_SIZE, tmp.c_str(), MAX_CODEC_NAME_CHAR_SIZE);

  bool sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    sent &= this->m_parent->SendError(error);

  }

  return sent;

}

bool
UserPacketHandler::SendStream(long long blockOffset, int size, const char data[MAX_STREAM_SIZE])
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_STREAM_DATA);
  packet.s2c_header.header.type = PT_S2C_STREAM_DATA;
  packet.s2c_header.success = true;

  packet.s2c_stream_data.blockOffset = htonll(blockOffset);
  packet.s2c_stream_data.size = htonl(size);
  memcpy(packet.s2c_stream_data.data, data, size);

  packet.s2c_header.header.size -= sizeof(packet.s2c_stream_data.data) - size;

  return this->m_client->SendPostPacket(packet);

}

bool
UserPacketHandler::SendMetaStream(long long blockOffset, int size, const char data[MAX_STREAM_SIZE])
{
  ANYVOD_PACKET packet;
  bool sent = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_STREAM_META_DATA);
  packet.s2c_header.header.type = PT_S2C_STREAM_META_DATA;

  packet.s2c_stream_meta_data.blockOffset = htonll(blockOffset);

  unsigned long destLen = sizeof(packet.s2c_stream_meta_data.data);
  int zipError = compress2((Bytef*)packet.s2c_stream_meta_data.data, &destLen,
    (Bytef*)data, size, Z_DEFAULT_COMPRESSION);
  bool success = zipError == Z_OK;

  packet.s2c_header.success = success;

  if (!success)
  {
    destLen = 0;

  }

  packet.s2c_stream_meta_data.size = htonl(destLen);
  packet.s2c_header.header.size -= sizeof(packet.s2c_stream_meta_data.data) - destLen;

  sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    sent &= this->m_parent->SendError(ET_COMPRESSION_ERROR, zipError);

  }

  return sent;

}

bool
UserPacketHandler::SendStreamEnd()
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_STREAM_END);
  packet.s2c_header.header.type = PT_S2C_STREAM_END;
  packet.s2c_header.success = true;

  return this->m_client->SendPostPacket(packet);

}

bool
UserPacketHandler::SendStartStream(long long startOffset, int size)
{
  ANYVOD_PACKET packet;
  ERROR_CODE error;
  bool success = this->m_client->GetMovieTransmitter().StartTransmission(startOffset, size, &error);

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_STREAM_REQUEST);
  packet.s2c_header.header.type = PT_S2C_STREAM_REQUEST;
  packet.s2c_header.success = success;

  bool sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    return this->m_parent->SendError(error.type, error.errorCode);

  }

  this->m_client->SetState(CS_SENDING_STREAM);

  return sent;

}

bool
UserPacketHandler::SendStopStream()
{
  ANYVOD_PACKET packet;
  bool success = this->m_client->GetMovieTransmitter().StopTransmission();

  packet.s2c_header.header.type = PT_S2C_STOP_MOVIE;
  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_STOP_MOVIE);
  packet.s2c_header.success = success;

  bool sent = this->m_client->SendPostPacket(packet);

  if (!success)
  {
    return this->m_parent->SendError(ET_TRANSMIT_ALREADY_STOPPED);

  }

  this->m_client->SetState(CS_LOGIN);

  return sent;

}

bool
UserPacketHandler::SendSubtitle(bool success, ifstream &subtitle, const wstring &fileName)
{
  ANYVOD_PACKET packet;
  bool sent = false;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_SUBTITLE);
  packet.s2c_header.header.type = PT_S2C_SUBTITLE;
  packet.s2c_header.success = success;

  sent = this->m_client->SendPostPacket(packet);

  if (success)
  {
    AdminPacketHandler handler(this->m_client, this->m_parent);

    return handler.SendFile(subtitle, FST_SUBTITLE, fileName);

  }
  else
  {
    return sent;

  }

}

bool
UserPacketHandler::SendSubtitleURL(bool success, const string &url)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_SUBTITLE_URL);
  packet.s2c_header.header.type = PT_S2C_SUBTITLE_URL;
  packet.s2c_header.success = success;

  strncpy_s((char*)packet.s2c_subtitle_url.url, MAX_URL_SIZE, url.c_str(), MAX_URL_CHAR_SIZE);

  bool sent = this->m_client->SendPostPacket(packet);

  return sent;

}


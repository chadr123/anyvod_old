/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "DataBase.h"
#include "ClientProcessor.h"
#include "Scheduler.h"
#include "Thread.h"
#include "AnalyserLoader.h"
#include "Stream.h"

class Worker;

class Server : public Thread
{
public:
  Server();
  ~Server();

  bool Start();
  void Stop();

  bool RecvPost(SOCKET sock, WSABUF *buf, LPWSAOVERLAPPED overlapped);
  bool SendPost(SOCKET sock, WSABUF *buf, LPWSAOVERLAPPED overlapped);

  const ENV_INFO& GetEnv() const;
  DataBase& GetDataBase();
  ClientProcessor& GetClientProcessor();
  Scheduler& GetScheduler();
  AnalyserLoader& GetAnalyserLoader();
  const vector_wstring& GetAudioExts() const;
  bool Accept(SOCKET socket, bool isStream);

private:
  typedef vector<Worker*> VECTOR_WORKER;
  typedef VECTOR_WORKER::iterator VECTOR_WORKER_ITERATOR;

private:
  void Clear();
  bool ReadEnv();
  bool CheckMovieDir();
  bool SplitFileLine(const wstring &fileName, VECTOR_KEY_VALUE *ret);

private:
  virtual bool Run();

private:
  WSADATA m_wsd;
  HANDLE m_completionPort;
  VECTOR_WORKER m_workers;
  SOCKET m_listenSocket;
  SOCKET m_listenStreamSocket;
  ENV_INFO m_env;
  DataBase m_db;
  ClientProcessor m_client;
  Scheduler m_scheduler;
  AnalyserLoader m_analyserLoader;
  vector_wstring m_audioExts;
  Stream m_stream;

};

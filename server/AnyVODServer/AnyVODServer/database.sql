﻿create table sequence (DUMMY TEXT);
create table account (PID INTEGER primary key, ID TEXT unique not null, Pass TEXT not null, Name TEXT not null);
create table movie_info (PID INTEGER primary key, Path TEXT collate nocase unique not null, Title TEXT not null, TotalSize INTEGER not null, TotalFrame INTEGER not null, TotalTime INTEGER not null, BitRate INTEGER not null, MovieType TEXT not null, CodecVideo TEXT not null, LastWriteTime INTEGER not null);
create table movie_meta (PID INTEGER primary key, movie_info_PID INTEGER not null, Offset INTEGER not null, Size INTEGER not null);
create table groups (PID INTEGER primary key, Name TEXT unique not null, PermissionOrder INTEGER not null);
create table user_group (PID INTEGER primary key, account_PID INTEGER not null, groups_PID INTEGER not null);
create table movie_permission (PID INTEGER primary key, movie_info_PID INTEGER not null, groups_PID INTEGER not null);
create table admins (PID INTEGER primary key, account_PID INTEGER unique not null);

create        index IDX_movie_meta_movie_info_PID on movie_meta(movie_info_PID);
create unique index IDX_user_group_ALL on user_group(account_PID, groups_PID);
create unique index IDX_movie_permission_ALL on movie_permission(movie_info_PID, groups_PID);
create        index IDX_movie_permission_movie_info_PID on movie_permission(movie_info_PID);

insert into sequence (DUMMY) values ('d');
insert into groups (PID, Name, PermissionOrder) values ((select max(ROWID) from sequence), 'public', 1);

insert into sequence (DUMMY) values ('d');
insert into account (PID, ID, Pass, Name) values ((select max(ROWID) from sequence), 'admin', '0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c', '관리자');

insert into sequence (DUMMY) values ('d');
insert into groups (PID, Name, PermissionOrder) values ((select max(ROWID) from sequence), 'admin', 0);

insert into sequence (DUMMY) values ('d');
insert into user_group (PID, account_PID, groups_PID) values ((select max(ROWID) from sequence), (select PID from account where ID='admin'), (select PID from groups where Name='public'));

insert into sequence (DUMMY) values ('d');
insert into user_group (PID, account_PID, groups_PID) values ((select max(ROWID) from sequence), (select PID from account where ID='admin'), (select PID from groups where Name='admin'));

insert into sequence (DUMMY) values ('d');
insert into admins (PID, account_PID) values ((select max(ROWID) from sequence), (select PID from account where ID='admin'));

insert into sequence (DUMMY) values ('d');
insert into account (PID, ID, Pass, Name) values ((select max(ROWID) from sequence), 'guest', '0ffe1abd1a08215353c233d6e009613e95eec4253832a761af28ff37ac5a150c', '손님');

insert into sequence (DUMMY) values ('d');
insert into groups (PID, Name, PermissionOrder) values ((select max(ROWID) from sequence), 'guest', 0);

insert into sequence (DUMMY) values ('d');
insert into user_group (PID, account_PID, groups_PID) values ((select max(ROWID) from sequence), (select PID from account where ID='guest'), (select PID from groups where Name='public'));

insert into sequence (DUMMY) values ('d');
insert into user_group (PID, account_PID, groups_PID) values ((select max(ROWID) from sequence), (select PID from account where ID='guest'), (select PID from groups where Name='guest'));

/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "stdafx.h"
#include "PacketHandler.h"
#include "UserPacketHandler.h"
#include "AdminPacketHandler.h"
#include "Client.h"
#include "Util.h"
#include "..\..\..\common\packets.h"

PacketHandler::PacketHandler(Client *client)
:m_client(client)
{

}

PacketHandler::~PacketHandler()
{

}

bool
PacketHandler::HandlePacket(const ANYVOD_PACKET &packet)
{
  ANYVOD_PACKET_TYPE type = (ANYVOD_PACKET_TYPE)ntohl(packet.c2s_header.header.type);

  this->m_client->SetLastPacketType(type);

  if (type != PT_C2S_LOGIN && type != PT_C2S_JOIN && type > PT_HEAD && type < PT_COUNT)
  {
    char ticket[MAX_TICKET_SIZE];

    strncpy_s(ticket, MAX_TICKET_SIZE, (char*)packet.c2s_header.ticket, MAX_TICKET_CHAR_SIZE);

    if (!this->CheckTicket(ticket))
    {
      Log::GetInstance().Write(L"Invalid ticket. Plan to close (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      if (this->SendError(ET_INVALID_TICKET))
      {
        this->m_client->SetToBeClosed();
        this->m_client->Shutdown();

        return true;

      }
      else
      {
        return false;

      }

    }

  }

  bool invalidOp = false;

  if (type != PT_C2S_LOGOUT)
  {
    if (type == PT_C2S_JOIN || type == PT_C2S_START_MOVIE || type == PT_C2S_STREAM_REQUEST || type == PT_C2S_STOP_MOVIE)
    {
      invalidOp = !this->m_client->IsStream();

    }
    else
    {
      invalidOp = this->m_client->IsStream();

    }

  }

  if (invalidOp)
  {
    Log::GetInstance().Write(L"Invalid socket operation. Plan to close (IP : %s, Port : %hu, Socket : %u, Type : %d, Stream : %s)",
      Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
      Util::PortFromAddress(this->m_client->GetSockAddr()),
      this->m_client->GetSocket(),
      type,
      this->m_client->IsStream() ? L"true" : L"false");

    if (this->SendError(ET_INVALID_SOCKET_OPERATION))
    {
      this->m_client->SetToBeClosed();
      this->m_client->Shutdown();

      return true;

    }
    else
    {
      return false;

    }
  }

  if (type > PT_ADMIN_HEAD)
  {
    if (!this->m_client->IsAdmin())
    {
      Log::GetInstance().Write(L"Not a administrator. Plan to close (IP : %s, Port : %hu, Socket : %u)",
        Util::AddressToString(this->m_client->GetSockAddr()).c_str(),
        Util::PortFromAddress(this->m_client->GetSockAddr()),
        this->m_client->GetSocket());

      if (this->SendError(ET_NOT_ADMIN))
      {
        this->m_client->SetToBeClosed();
        this->m_client->Shutdown();

        return true;

      }
      else
      {
        return false;

      }

    }
    else
    {
      AdminPacketHandler handler(this->m_client, this);

      return handler.HandlePacket(packet);

    }

  }
  else
  {
    UserPacketHandler handler(this->m_client, this);

    return handler.HandlePacket(packet);

  }

}

bool
PacketHandler::CheckTicket(const string &ticket)
{
  return this->m_client->IsMatchTicket(ticket) && ticket.size() != 0;

}

void
PacketHandler::ConvertToNativePath(const wstring &path, wstring *ret)
{
  wstring tmp = path;

  replace(tmp.begin(), tmp.end(), UNIX_DIRECTORY_DELIMITER, DIRECTORY_DELIMITER);

  if (tmp.size() == 0)
  {
    tmp.append(1, DIRECTORY_DELIMITER);

  }
  else if (tmp[0] != DIRECTORY_DELIMITER)
  {
    tmp = DIRECTORY_DELIMITER + tmp;

  }

  *ret = tmp;

}

bool
PacketHandler::SendError(ERROR_TYPE type, DWORD errorCode)
{
  ANYVOD_PACKET packet;

  packet.s2c_header.header.size = sizeof(ANYVOD_PACKET_S2C_ERROR);
  packet.s2c_header.header.type = PT_S2C_ERROR;
  packet.s2c_header.success = true;

  packet.s2c_error.type = htonl(type);
  packet.s2c_error.errorCode = htonl(errorCode);

  return this->m_client->SendPostPacket(packet);

}

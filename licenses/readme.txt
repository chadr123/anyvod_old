﻿******** READ CAREFULLY ********

- The information of server accounts -

1. Initial administrator of ID/PASSWORD are fallowing.
   ID       : admin
   PASSWORD : 1111

2. Initial guest of ID/PASSWORD are fallowing.
   ID       : guest
   PASSWORD : 1111

If you want to add accounts, use server tool.


- Default fonts path -

When you use ass subtitle, you can add fonts for ass subtitle.
The font path is belowing.

<installed-path>\client\fonts

"<installed-path>" is maybe "C:\Program Files (x86)\AnyVOD".


******** 반드시 읽어주세요 ********

- 서버 계정 정보 -

1. 초기 관리자 ID와 Password는 다음과 같습니다.
   ID       : admin
   PASSWORD : 1111

2. 초기 손님 ID와 Password는 다음과 같습니다.
   ID       : guest
   PASSWORD : 1111

만약에 계정을 추가하고 싶다면 서버 툴을 이용하세요.


- 기본 폰트 경로 -

ass 자막을 사용할 때 자막에서 필요로 하는 폰트를 추가할 수 있습니다.
경로는 다음과 같습니다.

<설치된 경로>\client\fonts

"<설치된 경로>"는 설치시 수정하지 않았다면 "C:\Program Files (x86)\AnyVOD"가 될 것입니다.
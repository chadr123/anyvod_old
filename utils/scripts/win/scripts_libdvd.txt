common
./configure --enable-static --disable-shared --host=i686-w64-mingw32 --prefix=/usr/i686-w64-mingw32

libdvdread
./configure --enable-static --disable-shared --host=i686-w64-mingw32 --prefix=/usr/i686-w64-mingw32 CFLAGS="-DHAVE_DVDCSS_DVDCSS_H"

libdvdnav
./configure --enable-shared --disable-static --host=i686-w64-mingw32 --with-dvdread-config=/usr/i686-w64-mingw32/bin/dvdread-config --prefix=/home/chadr/libdvdnav_cross/build CFLAGS="-march=pentium4"
make
i686-w64-mingw32-gcc -g -march=pentium4 -shared -Wl,--output-def,dvdnav.def -o dvdnav.dll -L/usr/i686-w64-mingw32/lib src/.libs/dvdnav.o src/.libs/read_cache.o  src/.libs/searching.o src/.libs/highlight.o  src/.libs/navigation.o src/.libs/remap.o src/.libs/settings.o src/vm/.libs/libdvdvm.a -ldvdread -ldvdcss

lib.exe /machine:i386 /def:dvdnav.def
i686-w64-mingw32-strip dvdnav.dll

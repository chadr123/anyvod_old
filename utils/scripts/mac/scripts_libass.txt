common
./configure --prefix=/Users/chadr/Downloads/tools --disable-shared --enable-static

freetype
./configure --prefix=/Users/chadr/Downloads/tools --disable-shared --enable-static --with-png=no --with-harfbuzz=no

fribidi
./configure --prefix=/Users/chadr/Downloads/tools --disable-shared --enable-static --with-glib=no

pkg-config
./configure --prefix=/Users/chadr/Downloads/tools --with-internal-glib

zlib
./configure --prefix=/Users/chadr/Downloads/tools --disable-static --enable-shared

libass
./configure --prefix=/Users/chadr/Downloads/libass/build --disable-static --enable-shared --disable-harfbuzz --disable-fontconfig PKG_CONFIG="/Users/chadr/Downloads/tools/bin/pkg-config" LIBS="-lexpat -lbz2 -lz"

make install-strip  

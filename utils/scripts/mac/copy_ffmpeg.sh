#!/bin/bash

cd build/lib

install_name_tool -id @executable_path/libavcodec.57.dylib libavcodec.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.55.dylib @executable_path/libavutil.55.dylib libavcodec.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswresample.2.dylib @executable_path/libswresample.2.dylib libavcodec.57.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libavcodec.57.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libavcodec.57.dylib

install_name_tool -id @executable_path/libavformat.57.dylib libavformat.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavcodec.57.dylib @executable_path/libavcodec.57.dylib libavformat.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.55.dylib @executable_path/libavutil.55.dylib libavformat.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswresample.2.dylib @executable_path/libswresample.2.dylib libavformat.57.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libavformat.57.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libavformat.57.dylib

install_name_tool -id @executable_path/libavutil.55.dylib libavutil.55.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libavutil.55.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libavutil.55.dylib

install_name_tool -id @executable_path/libavfilter.6.dylib libavfilter.6.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavformat.57.dylib @executable_path/libavformat.57.dylib libavfilter.6.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavcodec.57.dylib @executable_path/libavcodec.57.dylib libavfilter.6.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.55.dylib @executable_path/libavutil.55.dylib libavfilter.6.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswresample.2.dylib @executable_path/libswresample.2.dylib libavfilter.6.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswscale.4.dylib @executable_path/libswscale.4.dylib libavfilter.6.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libavfilter.6.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libavfilter.6.dylib

install_name_tool -id @executable_path/libswresample.2.dylib libswresample.2.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.55.dylib @executable_path/libavutil.55.dylib libswresample.2.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libswresample.2.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libswresample.2.dylib
 
install_name_tool -id @executable_path/libswscale.4.dylib libswscale.4.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.55.dylib @executable_path/libavutil.55.dylib libswscale.4.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libswscale.4.dylib 
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libswscale.4.dylib

install_name_tool -id @executable_path/libavdevice.57.dylib libavdevice.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavformat.57.dylib @executable_path/libavformat.57.dylib libavdevice.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavcodec.57.dylib @executable_path/libavcodec.57.dylib libavdevice.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswresample.2.dylib @executable_path/libswresample.2.dylib libavdevice.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavutil.55.dylib @executable_path/libavutil.55.dylib libavdevice.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libswscale.4.dylib @executable_path/libswscale.4.dylib libavdevice.57.dylib
install_name_tool -change /Users/chadr/Downloads/ffmpeg/build/lib/libavfilter.6.dylib @executable_path/libavfilter.6.dylib libavdevice.57.dylib
install_name_tool -change /opt/local/lib/libz.1.dylib /usr/lib/libz.1.dylib libavdevice.57.dylib
install_name_tool -change /Users/chadr/Downloads/tools/lib/libz.1.dylib /usr/lib/libz.1.dylib libavdevice.57.dylib

cp libavdevice.57.dylib libavcodec.57.dylib libavformat.57.dylib libavutil.55.dylib libavfilter.6.dylib libswresample.2.dylib libswscale.4.dylib /Users/chadr/project/anyvod/client/AnyVODClient/libs/mac

cd -



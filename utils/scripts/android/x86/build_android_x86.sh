#!/bin/bash
NDK=/Users/chadr/Downloads/android-ndk-r10e
SYSROOT=$NDK/platforms/android-16/arch-x86/
TOOLCHAIN=$NDK/toolchains/x86-4.8/prebuilt/darwin-x86_64

ln -s /Users/chadr/Downloads/tools/bin/pkg-config $TOOLCHAIN/bin/i686-linux-android-pkg-config

function build_one
{
CFLAGS="-Os" \
LDFLAGS="-L$SYSROOT/usr/lib" \
 ./configure \
    --prefix=$PREFIX \
    --enable-shared \
    --disable-static \
    --disable-doc \
    --disable-ffmpeg \
    --disable-ffplay \
    --disable-ffprobe \
    --disable-ffserver \
    --disable-avdevice \
    --disable-postproc \
    --disable-encoders \
    --enable-encoder="ac3,dca" \
    --disable-muxers \
    --enable-gpl \
    --enable-runtime-cpudetect \
    --enable-memalign-hack \
    --enable-openssl \
    --enable-libspeex \
    --enable-libilbc \
    --disable-doc \
    --disable-symver \
    --arch=x86 \
    --cross-prefix=$TOOLCHAIN/bin/i686-linux-android- \
    --target-os=linux \
    --enable-cross-compile \
    --enable-pic \
    --sysroot=$SYSROOT \
    --extra-cflags="$ADDI_CFLAGS" \
    --extra-ldflags="$ADDI_LDFLAGS" \
    $ADDITIONAL_CONFIGURE_FLAG
make clean
make -j4
make install
}
CPU=x86
PREFIX=$(pwd)/android/$CPU 
ADDI_CFLAGS="-Os"
ADDI_LDFLAGS=""
build_one

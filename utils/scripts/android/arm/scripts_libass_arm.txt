toolchain

goto ndk directory

build/tools/make-standalone-toolchain.sh --toolchain=arm-linux-androideabi-4.8 --platform=android-16 --install-dir=../toolchain/arm

common

export NDK=/Users/chadr/Downloads/android-ndk-r10e
export SYSROOT=$NDK/platforms/android-16/arch-arm
export TOOLCHAIN=/Users/chadr/Downloads/toolchain/arm
export PATH=$TOOLCHAIN/bin:$PATH

./configure --prefix=$SYSROOT/usr --disable-shared --enable-static --host=arm-linux-androideabi CFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8" LDFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8"

iconv
libcharset/build-aux/config.sub, build-aux/config.sub에 androideabi 추가 (Decode aliases for certain CPU-COMPANY combinations, Decode manufacturer-specific aliases for certain operating systems)

./configure --prefix=$SYSROOT/usr --disable-shared --enable-static --host=arm-linux-androideabi CFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8" LDFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8"

freetype
./configure --prefix=$SYSROOT/usr --disable-shared --enable-static --host=arm-linux-androideabi --with-png=no --with-harfbuzz=no --with-bzip2=no CFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8" LDFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8"

fontconfig
src/fcxml.c의 FcStrtod함수에 strtod만 빼고 제거

./configure --prefix=$SYSROOT/usr --disable-shared --enable-static --host=arm-linux-androideabi --with-libiconv=$SYSROOT/usr --with-expat=$SYSROOT/usr FREETYPE_LIBS="-L$SYSROOT/usr/lib -lfreetype" FREETYPE_CFLAGS=-I$SYSROOT/usr/include/freetype2 CFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8" LDFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8"

fribidi
./configure --prefix=$SYSROOT/usr --disable-shared --enable-static --host=arm-linux-androideabi --with-glib=no CFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8" LDFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8"

libass
configure에 "# This must be glibc/ELF." 항목에 다음으로 변경
library_names_spec='${libname}${versuffix}${shared_ext} ${libname}${major}${shared_ext} $libname${shared_ext}'
soname_spec='${libname}${major}${shared_ext}'

./configure --prefix=/Users/chadr/Downloads/libass/head/android/build --disable-static --enable-shared --host=arm-linux-androideabi --enable-fontconfig --disable-harfbuzz LIBS="-L$SYSROOT/usr/lib -lexpat -lz" FREETYPE_LIBS="-L$SYSROOT/usr/lib -lfreetype" FREETYPE_CFLAGS=-I$SYSROOT/usr/include/freetype2 FRIBIDI_CFLAGS=-I$SYSROOT/usr/include/fribidi FRIBIDI_LIBS="-L$SYSROOT/usr/lib -lfribidi" FONTCONFIG_CFLAGS=-I$SYSROOT/usr/include FONTCONFIG_LIBS="-L$SYSROOT/usr/lib -lfontconfig" CFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8" LDFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8"

make install-strip  

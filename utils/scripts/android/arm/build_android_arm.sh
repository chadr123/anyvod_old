#!/bin/bash
NDK=/Users/chadr/Downloads/android-ndk-r10e
SYSROOT=$NDK/platforms/android-16/arch-arm/
TOOLCHAIN=$NDK/toolchains/arm-linux-androideabi-4.8/prebuilt/darwin-x86_64

ln -s /Users/chadr/Downloads/tools/bin/pkg-config $TOOLCHAIN/bin/arm-linux-androideabi-pkg-config

function build_one
{
CFLAGS="-Os -mfloat-abi=softfp -mfpu=neon -march=armv7-a -mtune=cortex-a8" \
LDFLAGS="-mfloat-abi=softfp -mfpu=neon -march=armv7-a -mtune=cortex-a8 -L$SYSROOT/usr/lib" \
 ./configure \
    --prefix=$PREFIX \
    --enable-shared \
    --disable-static \
    --disable-doc \
    --disable-ffmpeg \
    --disable-ffplay \
    --disable-ffprobe \
    --disable-ffserver \
    --disable-avdevice \
    --disable-postproc \
    --disable-encoders \
    --enable-encoder="ac3,dca" \
    --disable-muxers \
    --enable-gpl \
    --enable-runtime-cpudetect \
    --enable-memalign-hack \
    --enable-openssl \
    --enable-libspeex \
    --enable-libilbc \
    --disable-doc \
    --disable-symver \
    --cross-prefix=$TOOLCHAIN/bin/arm-linux-androideabi- \
    --target-os=linux \
    --arch=arm \
    --cpu=cortex-a8 \
    --enable-cross-compile \
    --enable-pic \
    --sysroot=$SYSROOT \
    --extra-cflags="$ADDI_CFLAGS" \
    --extra-ldflags="$ADDI_LDFLAGS" \
    $ADDITIONAL_CONFIGURE_FLAG
make clean
make -j4
make install
}
CPU=arm
PREFIX=$(pwd)/android/$CPU 
ADDI_CFLAGS="-Os -mfloat-abi=softfp -mfpu=neon -march=armv7-a -mtune=cortex-a8"
ADDI_LDFLAGS="-mfloat-abi=softfp -mfpu=neon -march=armv7-a -mtune=cortex-a8"
build_one

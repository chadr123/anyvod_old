#! /bin/sh

PATH=/Users/chadr/Downloads/toolchain/arm/bin:$PATH

cd MediaInfoLib/Project/GNU/Library
cp AddThisToRoot_DLL_compile.sh ../../../../
chmod 775 autogen.sh
./autogen.sh
cd ../../../../
cd ZenLib/Project/GNU/Library
chmod 775 autogen.sh
./autogen.sh
cd ../../../../
chmod 775 AddThisToRoot_DLL_compile.sh
CPPFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8" CXXFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8" CFLAGS="-mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8" LDFLAGS="../../../../glob.o ../../../../wctomb.o -mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a -mtune=cortex-a8" ./AddThisToRoot_DLL_compile.sh --host=arm-linux-androideabi --disable-archive --disable-image
rm -rf ../build
mkdir ../build
cp -a MediaInfoLib/Project/GNU/Library/.libs/libmediainfo*.so* ../build
cd ../build

@echo off

rem %1 : start rev
rem %2 : rev chunk size
rem %3 : target rev
rem %4 : URL

set /a rev = %1

:loop

if %rev% gtr %3 goto end

:retry
echo pushing to %rev%

hg push -f --new-branch -r %rev% %4

if %errorlevel% gtr 1 goto retry

set /a rev = %rev% + %2
goto loop

:end
echo push completed
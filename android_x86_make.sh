#! /bin/sh

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 PASSWORD(signing)"
  exit 1
fi

cd ./installer/android
if [ $? -ne 0 ]; then
  exit $?
fi

./make.sh android_x86 $1 02
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

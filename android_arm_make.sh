#! /bin/sh

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 PASSWORD(signing)"
  exit 1
fi

cd ./installer/android
if [ $? -ne 0 ]; then
  exit $?
fi

./make.sh android_armv7 $1 01
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

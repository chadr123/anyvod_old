/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#include "stdafx.h"
#include "AsyncWinSocket.h"
#include "packets.h"

AsyncWinSocket::AsyncWinSocket()
:m_bTermSig(false), m_hEvent(NULL), m_hMain(NULL),
m_hSocket(NULL), m_bConnected(false), m_bSendPacket(true),
m_bIsShutDown(false), m_bStartConnect(false)
{
  InitializeCriticalSection(&this->m_Mutex);

}

AsyncWinSocket::~AsyncWinSocket()
{
  this->Clear();
  this->m_hRBuf.Destroy();

  ADDRESS_CACH_VECTOR_ITERATOR i = this->m_vAddress.begin();
  ADDRESS_CACH_VECTOR_ITERATOR j = this->m_vAddress.end();

  for (; i != j; i++)
  {
    freeaddrinfo(i->pdata);

  }

  this->m_bTermSig = true;

  WaitForSingleObject(this->m_hMain, INFINITE);

  DeleteCriticalSection(&this->m_Mutex);

}

bool
AsyncWinSocket::createInherit()
{
  return this->m_hRBuf.Create(1024 * 1024);

}

bool
AsyncWinSocket::Create()
{
  static bool bInit = false;

  if (!bInit)
  {
    WSADATA wsaData;

    if (WSAStartup(MAKEWORD(2,2), &wsaData))
      return false;

    bInit = true;
  }

  this->m_hMain = CreateThread(NULL, 0, thrMain, this, 0, NULL);

  if (this->m_hMain == INVALID_HANDLE_VALUE)
    return false;

  return this->createInherit();

}

bool
AsyncWinSocket::IsConnected()
{
  return this->m_bConnected;
}

bool
AsyncWinSocket::PreTranslateAddress(int nPort, const char* szIP)
{
  addrinfo hints, *res = NULL;
  char szPort[50] = {0, };
  int rc;

  sprintf(szPort, "%d", nPort);

  if (this->getAddress(string(szIP), nPort))
    return true;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  rc = getaddrinfo(szIP, szPort, &hints, &res);

  if (rc == WSANO_DATA)
    return false;

  ANYVOD_ADDRESS_CACH cach;

  cach.nPort = nPort;
  cach.strIP = szIP;
  cach.pdata = res;

  this->m_vAddress.push_back(cach);

  return true;
}

addrinfo*
AsyncWinSocket::getAddress(string &strAddress, int nPort)
{
  ADDRESS_CACH_VECTOR_ITERATOR i = this->m_vAddress.begin();
  ADDRESS_CACH_VECTOR_ITERATOR j = this->m_vAddress.end();

  for (; i != j; i++)
  {
    ANYVOD_ADDRESS_CACH &cach = *i;

    if (cach.nPort == nPort && cach.strIP == strAddress)
      return cach.pdata;

  }

  return NULL;

}

bool
AsyncWinSocket::Connect(int nPort, const char* szIP)
{
  addrinfo *res = NULL;

  //주소번역
  res = this->getAddress(string(szIP), nPort);

  if (!res)
  {
    if (!this->PreTranslateAddress(nPort, szIP))
      return false;

    res = this->getAddress(string(szIP), nPort);

    if (!res)
      return false;
  }

  //이벤트 생성
  this->m_hEvent = WSACreateEvent();

  if (this->m_hEvent == WSA_INVALID_EVENT)
    return false;

  //소켓 생성
  this->m_hSocket = socket(res->ai_family, res->ai_socktype, res->ai_protocol);

  if (this->m_hSocket == INVALID_SOCKET)
    return false;

  //이벤트 할당
  if (WSAEventSelect(this->m_hSocket, this->m_hEvent, FD_CONNECT | FD_READ | FD_WRITE | FD_CLOSE) != S_OK)
    return false;

  BOOL bBuf = TRUE;

  //지연하여 보내기를 끈다.
  if (setsockopt(this->m_hSocket, IPPROTO_TCP, TCP_NODELAY, (char*)&bBuf, sizeof(BOOL)))
    return false;

  //연결
  connect(this->m_hSocket, res->ai_addr, res->ai_addrlen);

  this->m_bStartConnect = true;

  return true;

}

void
AsyncWinSocket::Clear()
{
  EnterCriticalSection(&this->m_Mutex);

  this->m_bConnected = false;
  this->m_bStartConnect = false;

  closesocket(this->m_hSocket);
  WSACloseEvent(this->m_hEvent);

  this->m_hEvent = NULL;
  this->m_hSocket = NULL;

  this->m_hRBuf.Clear();

  while (!this->m_SendQueue.empty())
  {
    SOCKET_SEND_DATA data = this->m_SendQueue.front();

    delete[] data.p;

    this->m_SendQueue.pop_front();

  }

  LeaveCriticalSection(&this->m_Mutex);
}

void
AsyncWinSocket::Disconnect()
{
  while (true)
  {
    EnterCriticalSection(&this->m_Mutex);

    if (this->m_SendQueue.empty() || this->IsConnected() == false)
    {
      shutdown(this->m_hSocket, SD_SEND);

      LeaveCriticalSection(&this->m_Mutex);

      break;

    }

    LeaveCriticalSection(&this->m_Mutex);

  }

}

void
AsyncWinSocket::sendRemainedPackets()
{
  EnterCriticalSection(&this->m_Mutex);

  while (!this->m_SendQueue.empty())
  {
    SOCKET_SEND_DATA data = this->m_SendQueue.front();

    //패킷을 보냈으면..
    int nRest = this->sendPacket(data);

    this->m_SendQueue.pop_front();

    if (nRest > 0)
    {
      SOCKET_SEND_DATA remain;

      remain.p = new char[nRest];
      remain.size = nRest;

      memcpy(remain.p, &data.p[data.size - nRest] , nRest);

      this->m_SendQueue.push_front(remain);

    }
    else if (nRest == -1)
    {
      break;

    }

    delete[] data.p;

  }

  LeaveCriticalSection(&this->m_Mutex);

}

int
AsyncWinSocket::sendPacket(SOCKET_SEND_DATA &Data)
{
  size_t nRest;
  int nRet;
  char* pData = Data.p;

  nRest = Data.size;

  while (nRest)
  {
    nRet = send(this->m_hSocket, pData, nRest, 0);

    if (nRet == SOCKET_ERROR)
    {
      //보내기가 취소 됬으면 한바이트도 못보낸거다. 다음에 다시 보낸다
      if (WSAGetLastError() == WSAEWOULDBLOCK)
      {
        this->m_bSendPacket = false;

      }
      else
      {
        //접속이 끊긴것이다.

        //
        this->Clear();
        this->OnDisconnect();

        nRest = -1;

      }

      return nRest;

    }

    nRest -= nRet;
    pData += nRet;

  }

  return nRest;

}

void
AsyncWinSocket::cleanUp()
{
  if (this->m_bIsShutDown)
  {
    this->sendRemainedPackets();

    shutdown(this->m_hSocket, SD_SEND);

  }

  this->Clear();
  this->OnDisconnect();

}

void
AsyncWinSocket::recvData()
{
  if (!this->recvPacket())
  {
    this->cleanUp();

  }

}

bool
AsyncWinSocket::SendPacket(ANYVOD_PACKET &packet)
{
  if (!this->m_bConnected)
    return false;

  SOCKET_SEND_DATA data;
  int nLen = packet.c2s_header.header.size;

  packet.c2s_header.header.size = htonl(nLen);
  packet.c2s_header.header.type = htonl(packet.c2s_header.header.type);

  data.p = new char[nLen];

  if (data.p)
  {
    memcpy(data.p, &packet, nLen);
    data.size = nLen;

    EnterCriticalSection(&this->m_Mutex);

    this->m_SendQueue.push_back(data);

    LeaveCriticalSection(&this->m_Mutex);

    return true;

  }
  else
  {
    return false;

  }

}

DWORD WINAPI
AsyncWinSocket::thrMain(LPVOID lpArgs)
{
  AsyncWinSocket* pThis = (AsyncWinSocket*)lpArgs;

  pThis->m_bTermSig = false;
  pThis->MainLoop();

  return 0;

}

void
AsyncWinSocket::MainLoop()
{
  WSANETWORKEVENTS wne;

  while (!this->m_bTermSig)
  {
    if (!this->m_bStartConnect)
    {
      Sleep(1);

      continue;

    }

    if (this->m_bSendPacket)
    {
      this->sendRemainedPackets();

    }

    //이벤트를 얻자.
    WSAWaitForMultipleEvents(1, &this->m_hEvent, false, 10, false);
    WSAEnumNetworkEvents(this->m_hSocket, this->m_hEvent, &wne);

    if (wne.lNetworkEvents & FD_CONNECT)
    {
      if (wne.iErrorCode[FD_CONNECT_BIT] != 0)
      {
        this->m_bConnected = false;

      }
      else
      {
        this->m_bConnected = true;

      }

      this->OnConnect(this->m_bConnected);
    }

    if (wne.lNetworkEvents & FD_READ)
    {
      this->recvData();

    }

    if (wne.lNetworkEvents & FD_WRITE)
    {
      this->m_bSendPacket = true;

    }

    if (wne.lNetworkEvents & FD_CLOSE)
    {
      this->recvData();

    }

  }//whlie

}

void
AsyncWinSocket::assemblePacket()
{
  ANYVOD_PACKET packet;
  int nBufSize;

  while (true)
  {
    // 읽을 꺼리가 기본 헤더보다도 작으면 루프 끝냄
    nBufSize = this->m_hRBuf.GetReadableSize();

    if (nBufSize < S2C_HEADER_SIZE)
      break;

    // 헤더만 살짝이 읽어봄;;
    this->m_hRBuf.Read((char*)&packet, S2C_HEADER_SIZE, true);

    int nTotalSize = ntohl(packet.s2c_header.header.size);

    // 읽을 꺼리가 있으면 패킷을 조합한다.
    if (nBufSize >= nTotalSize)
    {
      ANYVOD_PACKET *packetBuf = NULL;
      ANYVOD_PACKET_TYPE type = (ANYVOD_PACKET_TYPE)ntohl(packet.s2c_header.header.type);

      if (type == PT_S2C_FILELIST)
      {
        char *buf = new char[nTotalSize];

        packetBuf = (ANYVOD_PACKET*)buf;

      }
      else
      {
        packetBuf = &packet;

      }

      this->m_hRBuf.Read((char*)packetBuf, nTotalSize, false);

      this->OnReceive(*packetBuf);

      if (type == PT_S2C_FILELIST)
      {
        delete[] packetBuf;

      }

    }
    else//헤더보다는 패킷이 더 왔지만 완전히 오지 않았으므로 루프 끝냄
    {
      break;

    }

  }

}

bool
AsyncWinSocket::recvPacket()
{
  unsigned long ulSize = 0;

  ioctlsocket(this->m_hSocket, FIONREAD, &ulSize);

  if (ulSize == 0)
  {
    this->m_bIsShutDown = true;

    return false;

  }

  size_t nBufSize = this->m_hRBuf.GetWritableSize();

  // 버퍼 용량이 적으면 버퍼 용량까지만 받자
  if (nBufSize < ulSize)
    ulSize = nBufSize;

  if (ulSize > 0)
  {// Sub routine
    char *szBuf = new char[ulSize];

    ulSize = recv(this->m_hSocket, szBuf, ulSize, 0);

    if (ulSize == 0 || ulSize == SOCKET_ERROR)
    {
      delete[] szBuf;

      if (ulSize == 0)
      {
        this->m_bIsShutDown = true;

      }

      return false;

    }

    this->m_hRBuf.Write(szBuf, ulSize, false);

    delete[] szBuf;

  }

  //받은 버퍼를 가지고 패킷을 조합하자.

  this->assemblePacket();

  return true;

}

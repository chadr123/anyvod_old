/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "packets.h"

class MemBuffer
{
public:
  enum
  {
    BUFFER_SIZE = TOTAL_PACKET_SIZE * 2

  };

  MemBuffer();
  MemBuffer(size_t nSize);
  ~MemBuffer();

  bool Create(size_t nSize = BUFFER_SIZE);
  void Destroy(void);
  void Clear(void);

  size_t GetReadableSize(void) const;
  size_t GetWritableSize(void) const;
  size_t GetSize(void) const;
  bool IsEmpty(void) const;
  bool IsFull(void) const;

  size_t GetFrontPos(void) const;
  size_t GetRearPos(void) const;

  bool Resize(size_t nSize);

  size_t Write(const char* pBuf, size_t nSize, bool bNoMove = false);
  size_t Read(char* pBuf, size_t nSize, bool bNoMove = false);

private:
  bool pushBack(char c);
  bool popFront(char& c);

private:
  char* m_Buf;
  size_t m_Size;
  size_t m_Front;
  size_t m_Rear;

};

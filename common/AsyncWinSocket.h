/*************************************************************************
Copyright (c) 2011-2016, DongRyeol Cha (chadr@dcple.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*************************************************************************/

#pragma once

#include "MemBuffer.h"

union ANYVOD_PACKET;

class AsyncWinSocket
{
  struct SOCKET_SEND_DATA
  {
    char *p;
    size_t size;

    SOCKET_SEND_DATA()
    {
      p = NULL;
      size = 0;
    }

  };

  typedef struct tagANYVOD_ADDRESS_CACH
  {
    string strIP;
    int nPort;

    addrinfo *pdata;

  }ANYVOD_ADDRESS_CACH;

  typedef deque<SOCKET_SEND_DATA> SEND_QUEUE;

  typedef vector<ANYVOD_ADDRESS_CACH> ADDRESS_CACH_VECTOR;
  typedef ADDRESS_CACH_VECTOR::iterator ADDRESS_CACH_VECTOR_ITERATOR;

public:
  AsyncWinSocket();
  virtual ~AsyncWinSocket();

  bool Create();
  void Clear();
  bool Connect(int nPort, const char* szIP = NULL);
  bool PreTranslateAddress(int nPort, const char* szIP = NULL);

  void Disconnect();
  bool SendPacket(ANYVOD_PACKET &packet);
  bool IsConnected();

private:
  ADDRESS_CACH_VECTOR m_vAddress;
  CRITICAL_SECTION m_Mutex;
  SOCKET m_hSocket;
  WSAEVENT m_hEvent;
  HANDLE m_hMain;
  volatile bool m_bTermSig;
  volatile bool m_bConnected;
  volatile bool m_bStartConnect;
  bool m_bSendPacket;
  bool m_bIsShutDown;

  static DWORD WINAPI thrMain(LPVOID lpArgs);
  void MainLoop();

  virtual bool createInherit();
  virtual void assemblePacket();

  int sendPacket(SOCKET_SEND_DATA &Data);
  void sendRemainedPackets();

  addrinfo* getAddress(string &strAddress, int nPort);

  void cleanUp();
  void recvData();

protected:
  SEND_QUEUE m_SendQueue;
  MemBuffer  m_hRBuf;  //!< Memory buffer for receiving...

  virtual bool recvPacket();

  virtual void OnReceive(const ANYVOD_PACKET& packet){}
  virtual void OnDisconnect(){}
  virtual void OnConnect(bool bRet){}

};

#! /bin/sh

version=`cat version`
fileName="AnyVOD-${version}_linux_amd64.deb"
installRoot="usr/bin"
installDirectory="AnyVOD"
installPath="${installRoot}/${installDirectory}"

cd ../../package
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -d AnyVOD ] ; then
  rm -rf AnyVOD
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir -p AnyVOD/${installPath}
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir -p AnyVOD/usr/share/applications
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../installer/linux/dcple-com-AnyVOD.desktop AnyVOD/usr/share/applications
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir -p AnyVOD/etc/ld.so.conf.d
if [ $? -ne 0 ]; then
  exit $?
fi

echo "/${installPath}/client" > AnyVOD/etc/ld.so.conf.d/AnyVOD.conf
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a client licenses AnyVOD/${installPath}
if [ $? -ne 0 ]; then
  exit $?
fi

cd AnyVOD/${installRoot}
if [ $? -ne 0 ]; then
  exit $?
fi

strip "${installDirectory}/client/AnyVODClient_bin"
if [ $? -ne 0 ]; then
  exit $?
fi

ln -s "${installDirectory}/client/AnyVODClient_bin" AnyVODClient
if [ $? -ne 0 ]; then
  exit $?
fi

cd -
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../images/orgs/icon_64x64.png "AnyVOD/${installPath}/client/icon.png"
if [ $? -ne 0 ]; then
  exit $?
fi

touch "AnyVOD/${installPath}/client/lastplay.ini"
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ../installer/linux/deb/* AnyVOD
if [ $? -ne 0 ]; then
  exit $?
fi

sed "s/%VERSION%/${version}/g" AnyVOD/DEBIAN/control > AnyVOD/DEBIAN/control_
if [ $? -ne 0 ]; then
  exit $?
fi

mv -f AnyVOD/DEBIAN/control_ AnyVOD/DEBIAN/control
if [ $? -ne 0 ]; then
  exit $?
fi

dpkg-deb --build AnyVOD ${fileName}
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

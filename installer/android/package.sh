#! /bin/sh

version=`cat ../linux/version`
version_qt=`cat ../../client/AnyVODClient/scripts/linux/version`
qt_path=`../../client/AnyVODClient/scripts/linux/qt_path.sh`
arch=$1

cd ../../package
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../client/AnyVODMobileClient-build-android/android-build/build/outputs/apk/android-build-release-signed.apk ./AnyVODMobile-${version}_${arch}.apk
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

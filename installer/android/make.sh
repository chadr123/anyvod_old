#! /bin/sh

if [ -d ../../package ] ; then
  rm -rf ../../package
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

cd ../../client/AnyVODMobileClient/scripts/android
if [ $? -ne 0 ]; then
  exit $?
fi

./build.sh $1 $2 $3
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../../../../installer/android
if [ $? -ne 0 ]; then
  exit $?
fi

./package.sh $1
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

#! /bin/sh

version=`cat ../linux/version`
fileName="AnyVODMobile-${version}_raspi_armhf.deb"
installRoot="usr/bin"
installDirectory="AnyVODMobile"
installPath="${installRoot}/${installDirectory}"

cd ../../package
if [ $? -ne 0 ]; then
  exit $?
fi

if [ -d AnyVODMobile ] ; then
  rm -rf AnyVODMobile
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

mkdir -p AnyVODMobile/${installPath}
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir -p AnyVODMobile/usr/share/applications
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../installer/raspi/dcple-com-AnyVODMobile.desktop AnyVODMobile/usr/share/applications
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir -p AnyVODMobile/etc/ld.so.conf.d
if [ $? -ne 0 ]; then
  exit $?
fi

echo "/${installPath}" > AnyVODMobile/etc/ld.so.conf.d/AnyVODMobile.conf
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a client/* AnyVODMobile/${installPath}
if [ $? -ne 0 ]; then
  exit $?
fi

cd AnyVODMobile/${installRoot}
if [ $? -ne 0 ]; then
  exit $?
fi

strip "${installDirectory}/AnyVODMobileClient_bin"
if [ $? -ne 0 ]; then
  exit $?
fi

ln -s "${installDirectory}/AnyVODMobileClient_bin" AnyVODMobileClient
if [ $? -ne 0 ]; then
  exit $?
fi

cd -
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../images/orgs/icon_64x64.png "AnyVODMobile/${installPath}/icon.png"
if [ $? -ne 0 ]; then
  exit $?
fi

cp -a ../installer/raspi/deb/* AnyVODMobile
if [ $? -ne 0 ]; then
  exit $?
fi

sed "s/%VERSION%/${version}/g" AnyVODMobile/DEBIAN/control > AnyVODMobile/DEBIAN/control_
if [ $? -ne 0 ]; then
  exit $?
fi

mv -f AnyVODMobile/DEBIAN/control_ AnyVODMobile/DEBIAN/control
if [ $? -ne 0 ]; then
  exit $?
fi

dpkg-deb --build AnyVODMobile ${fileName}
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

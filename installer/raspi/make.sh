#! /bin/sh

if [ -d ../../package ] ; then
  rm -rf ../../package
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

cd ../../client/AnyVODMobileClient/scripts/raspi
if [ $? -ne 0 ]; then
  exit $?
fi

./build.sh
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../../../../installer/raspi
if [ $? -ne 0 ]; then
  exit $?
fi

./package.sh
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

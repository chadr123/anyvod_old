#! /bin/sh

version=`cat ../linux/version`
version_qt=`cat ../../client/AnyVODClient/scripts/linux/version`
qt_path=`../../client/AnyVODClient/scripts/linux/qt_path.sh`

./change_bin_lib_path.sh
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../../package
if [ $? -ne 0 ]; then
  exit $?
fi

$HOME/$qt_path/$version_qt/clang_64/bin/macdeployqt client/AnyVODClient.app -verbose=2
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir resources
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../licenses/anyvod.txt resources
if [ $? -ne 0 ]; then
  exit $?
fi

mkdir scripts
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../installer/mac/postinstall ../installer/mac/preinstall scripts
if [ $? -ne 0 ]; then
  exit $?
fi

pkgbuild --component client/AnyVODClient.app --scripts scripts --identifier com.dcple.anyvod --version ${version} --install-location /Applications AnyVOD.pkg
if [ $? -ne 0 ]; then
  exit $?
fi

productbuild --distribution ../installer/mac/distribution.xml --resources resources --package-path AnyVOD.pkg AnyVOD-${version}_mac.pkg
if [ $? -ne 0 ]; then
  exit $?
fi

rm -rf client resources scripts AnyVOD.pkg
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

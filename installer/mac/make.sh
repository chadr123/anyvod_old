#! /bin/sh

if [ -d ../../package ] ; then
  rm -rf ../../package
  if [ $? -ne 0 ]; then
    exit $?
  fi
fi

cd ../../client/AnyVODClient/scripts/mac
if [ $? -ne 0 ]; then
  exit $?
fi

./build.sh
if [ $? -ne 0 ]; then
  exit $?
fi

cd ../../../../installer/mac
if [ $? -ne 0 ]; then
  exit $?
fi

./package.sh
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

#! /bin/sh

version=`cat ../linux/version`
version_qt=`cat ../../client/AnyVODClient/scripts/linux/version`
qt_path=`../../client/AnyVODClient/scripts/linux/qt_path.sh`

cd ../../package
if [ $? -ne 0 ]; then
  exit $?
fi

cp ../client/AnyVODMobileClient-build-ios/Release-iphoneos/AnyVODMobileClient.ipa ./AnyVODMobile-${version}_ios.ipa
if [ $? -ne 0 ]; then
  exit $?
fi

exit 0

if exist "..\..\package" (
rmdir ..\..\package /S /Q
if %errorlevel% neq 0 goto end
)

cd ..\..\client\AnyVODClient\scripts\win
call build.bat
if %errorlevel% neq 0 goto end
cd ..\..\..\..\installer\win

cd ..\..\server\AnyVODServer
call build.bat
if %errorlevel% neq 0 goto end
cd ..\..\installer\win

cd ..\..\server\AnyVODServerStarter
call build.bat
if %errorlevel% neq 0 goto end
cd ..\..\installer\win

cd ..\..\server\AnyVODServerTool\
call build.bat
if %errorlevel% neq 0 goto end
cd ..\..\installer\win

call package.bat

:end
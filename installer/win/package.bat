del ..\..\package\*.* /f /q /a-h-s-r
if %errorlevel% neq 0 goto end

xcopy AnyVOD.nsi ..\..\package /y
if %errorlevel% neq 0 goto end

xcopy *.exe ..\..\package /y
if %errorlevel% neq 0 goto end

xcopy ..\..\notes\*.txt ..\..\package /y
if %errorlevel% neq 0 goto end

cd ..\..\package

if exist "C:\Program Files (x86)\NSIS\makensis.exe" (
  "C:\Program Files (x86)\NSIS\makensis.exe" AnyVOD.nsi
) else (
  "C:\Program Files\NSIS\makensis.exe" AnyVOD.nsi
)

:end
